<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
<!-- Bootstrap -->
<link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/global.css"/>" rel="stylesheet">

<!-- jQuery -->
<script src="<c:url value="/resources/js/jquery-1.12.3.min.js"/>"></script>
<!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="resources/css/ie8-and-down.css" />
    <![endif]-->
<!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="resources/css/ie8-and-down.css" />
    <![endif]-->

<script type="text/javascript">
	$(function() {
		var uploadForm = $("#uploadForm");
		uploadForm.find("[name='uploadBtn']").click(function() {
			uploadForm.attr("action", "<c:url value='/upload/doUpload'/>");
			uploadForm.attr("target", "iframeUpload");
			uploadForm.submit();
		});
	})

	function uploadNotify() {
		var jframe = $("[name='iframeUpload']").contents();

		var code = jframe.find("#code").val();
		var msg = jframe.find("#msg").val();

		if (code == '0') {
			alert('success!');
		} else if (code == '-1') {
			//失敗
			alert(msg);
		}
		
		jframe.empty();
	}
</script>
</head>

<body>
	<iframe name="iframeUpload" style="display: none;" onload='uploadNotify()'></iframe>
	<div>
		<form id="uploadForm" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<input type="file" name="file">
			</div>
			<br>
			<div class="form-group">
				<button name="uploadBtn" type="button" class="btn btn-primary">Upload</button>
			</div>
		</form>
	</div>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
</body>

</html>