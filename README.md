國泰 "易call保" TPIGateway

由 https://bitbucket.org/tpowerapp/cathaybot/src/master/cathaybot_tpigateway/ 的 dev_life_OTP1017 分支
刪除大部份和開發 "易call保" 無關的程式

以下為 "易call保" TPIGateway 開發的程式
com.tp.tpigateway.web.life.cb.CBVController
com.tp.tpigateway.service.life.flow.cb.CBV10001Service
com.tp.tpigateway.service.life.flow.impl.cb.CBV10001ServiceImpl
com.tp.tpigateway.model.life.CBV10001VO
com.tp.tpigateway.model.life.CBV10001Enum
com.tp.tpigateway.service.life.flow.impl.cb.FakeCBA10001DataService

國泰 API URL 設定
tpigateway-lifelocal.properties
    cathaylife.CBV10001.website.url.prefix=https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1
    cathaylife.CBV10001.api.x-ibm-client-id=3d035f85-2d0f-4979-8219-744a174f992d