package com.tp.tpigateway.configuration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import com.tp.tpigateway.model.common.BotCardImageTextTemplateId;
import com.tp.tpigateway.model.common.BotCardTextTemplateId;

/**
 * 設定一般牌卡的欄位是否要顯示（預設為顯示）
 * 
 * @author 楊宗翰
 * @since 2018-08-23
 */
public class BotCardShowData {
	/** class name */
	public static final String name = BotCardShowData.class.getName();

	private Map<String, Object> tmp = new HashMap<String, Object>();

	private Map<String, Map<String, Boolean>> botCardShowData = new HashMap<String, Map<String, Boolean>>();
	private Set<BotCardImageTextTemplateId> botCardImageTexts_hide = new HashSet<BotCardImageTextTemplateId>();
	private Set<BotCardTextTemplateId> botCardTexts_hide = new HashSet<BotCardTextTemplateId>();

	public BotCardShowData() {
		tmp.put("botCardShowData", botCardShowData);
		tmp.put("botCardImageTexts_hide", botCardImageTexts_hide);
		tmp.put("botCardTexts_hide", botCardTexts_hide);

		Map<String, Boolean> bot_card_template = new HashMap<String, Boolean>();
		botCardShowData.put(_bot_card_template.bot_card_template.name(), bot_card_template);

		bot_card_template.put(_bot_card_template.message.name(), true);
		bot_card_template.put(_bot_card_template.title.name(), true);
		bot_card_template.put(_bot_card_template.break_line.name(), true);

		Map<String, Boolean> bot_card_image_template = new HashMap<String, Boolean>();
		botCardShowData.put(_bot_card_image_template.bot_card_image_template.name(), bot_card_image_template);

		bot_card_image_template.put(_bot_card_image_template.image_tag.name(), true);

		Map<String, Boolean> bot_card_image_text_template = new HashMap<String, Boolean>();
		botCardShowData.put(_bot_card_image_text_template.bot_card_image_text_template.name(), bot_card_image_text_template);

		bot_card_image_text_template.put(_bot_card_image_text_template.image_text_title.name(), true);
		bot_card_image_text_template.put(_bot_card_image_text_template.image_text.name(), true);

		Map<String, Boolean> bot_card_text_template = new HashMap<String, Boolean>();
		botCardShowData.put(_bot_card_text_template.bot_card_text_template.name(), bot_card_text_template);

		bot_card_text_template.put(_bot_card_text_template.card_text_label.name(), true);
		bot_card_text_template.put(_bot_card_text_template.text.name(), true);
		bot_card_text_template.put(_bot_card_text_template.unit.name(), true);
		bot_card_text_template.put(_bot_card_text_template.amount.name(), true);
	}

	/**
	 * @return 是否顯示牌卡重點提示訊息
	 */
	public boolean showBotCardTemplate_message() {
		return botCardShowData.get(_bot_card_template.bot_card_template.name()).get(_bot_card_template.message.name());
	}

	/**
	 * 是否顯示牌卡重點提示訊息
	 * 
	 * @param show
	 */
	public void setBotCardTemplate_message(boolean show) {
		botCardShowData.get(_bot_card_template.bot_card_template.name()).put(_bot_card_template.message.name(), show);
	}

	/**
	 * @return 是否顯示牌卡內容標題
	 */
	public boolean showBotCardTemplate_title() {
		return botCardShowData.get(_bot_card_template.bot_card_template.name()).get(_bot_card_template.title.name());
	}

	/**
	 * 是否顯示牌卡內容標題
	 * 
	 * @param show
	 */
	public void setBotCardTemplate_title(boolean show) {
		botCardShowData.get(_bot_card_template.bot_card_template.name()).put(_bot_card_template.title.name(), show);
	}

	/**
	 * @return 是否顯示分隔線
	 */
	public boolean showBotCardTemplate_breakLine() {
		return botCardShowData.get(_bot_card_template.bot_card_template.name()).get(_bot_card_template.break_line.name());
	}

	/**
	 * 是否顯示分隔線
	 * 
	 * @param show
	 */
	public void setBotCardTemplate_breakLine(boolean show) {
		botCardShowData.get(_bot_card_template.bot_card_template.name()).put(_bot_card_template.break_line.name(), show);
	}

	/**
	 * @return 是否顯示圖片標籤
	 */
	public boolean showBotCardImageTemplate_imageTag() {
		return botCardShowData.get(_bot_card_image_template.bot_card_image_template.name())
				.get(_bot_card_image_template.image_tag.name());
	}

	/**
	 * 是否顯示圖片標籤
	 * 
	 * @param show
	 */
	public void setBotCardImageTemplate_imageTag(boolean show) {
		botCardShowData.get(_bot_card_image_template.bot_card_image_template.name()).put(_bot_card_image_template.image_tag.name(),
				show);
	}

	/**
	 * @return 是否顯示標題
	 */
	public boolean showBotCardImageTextTemplate_imageTextTitle() {
		return botCardShowData.get(_bot_card_image_text_template.bot_card_image_text_template.name())
				.get(_bot_card_image_text_template.image_text_title.name());
	}

	/**
	 * 是否顯示標題
	 * 
	 * @param show
	 */
	public void setBotCardImageTextTemplate_imageTextTitle(boolean show) {
		botCardShowData.get(_bot_card_image_text_template.bot_card_image_text_template.name())
				.put(_bot_card_image_text_template.image_text_title.name(), show);
	}

	/**
	 * @return 是否顯示內容
	 */
	public boolean showBotCardImageTextTemplate_imageText() {
		return botCardShowData.get(_bot_card_image_text_template.bot_card_image_text_template.name())
				.get(_bot_card_image_text_template.image_text.name());
	}

	/**
	 * 是否顯示內容
	 * 
	 * @param show
	 */
	public void setBotCardImageTextTemplate_imageText(boolean show) {
		botCardShowData.get(_bot_card_image_text_template.bot_card_image_text_template.name())
				.put(_bot_card_image_text_template.image_text.name(), show);
	}

	/**
	 * @return 是否顯示牌卡內容標籤
	 */
	public boolean showBotCardTextTemplate_cardTextLabel() {
		return botCardShowData.get(_bot_card_text_template.bot_card_text_template.name())
				.get(_bot_card_text_template.card_text_label.name());
	}

	/**
	 * 是否顯示牌卡內容標籤
	 * 
	 * @param show
	 */
	public void setBotCardTextTemplate_cardTextLabel(boolean show) {
		botCardShowData.get(_bot_card_text_template.bot_card_text_template.name()).put(_bot_card_text_template.card_text_label.name(),
				show);
	}

	/**
	 * @return 是否顯示牌卡內容文字
	 */
	public boolean showBotCardTextTemplate_text() {
		return botCardShowData.get(_bot_card_text_template.bot_card_text_template.name()).get(_bot_card_text_template.text.name());
	}

	/**
	 * 是否顯示牌卡內容文字
	 * 
	 * @param show
	 */
	public void setBotCardTextTemplate_text(boolean show) {
		botCardShowData.get(_bot_card_text_template.bot_card_text_template.name()).put(_bot_card_text_template.text.name(), show);
	}

	/**
	 * @return 是否顯示(金額牌卡)牌卡單位/幣別
	 */
	public boolean showBotCardTextTemplate_unit() {
		return botCardShowData.get(_bot_card_text_template.bot_card_text_template.name()).get(_bot_card_text_template.unit.name());
	}

	/**
	 * 是否顯示(金額牌卡)牌卡單位/幣別
	 * 
	 * @param show
	 */
	public void setBotCardTextTemplate_unit(boolean show) {
		botCardShowData.get(_bot_card_text_template.bot_card_text_template.name()).put(_bot_card_text_template.unit.name(), show);
	}

	/**
	 * @return 是否顯示(金額牌卡)牌卡金額
	 */
	public boolean showBotCardTextTemplate_amount() {
		return botCardShowData.get(_bot_card_text_template.bot_card_text_template.name()).get(_bot_card_text_template.amount.name());
	}

	/**
	 * 是否顯示(金額牌卡)牌卡金額
	 * 
	 * @param show
	 */
	public void setBotCardTextTemplate_amount(boolean show) {
		botCardShowData.get(_bot_card_text_template.bot_card_text_template.name()).put(_bot_card_text_template.amount.name(), show);
	}

	/**
	 * 設定要隱藏的牌卡圖片文字模板
	 * 
	 * @param botCardImageTextTemplateId
	 */
	public void addToBotCardImageText_hide(BotCardImageTextTemplateId botCardImageTextTemplateId) {
		botCardImageTexts_hide.add(botCardImageTextTemplateId);
	}

	/**
	 * 取得所有要隱藏的牌卡圖片文字模板
	 * 
	 * @param botCardImageTextTemplateId
	 */
	public Set<BotCardImageTextTemplateId> getBotCardImageText_hide() {
		return botCardImageTexts_hide;
	}

	/**
	 * 刪除要隱藏的牌卡圖片文字模板
	 * 
	 * @param botCardImageTextTemplateId
	 */
	public void removeFromBotCardImageText_hide(BotCardImageTextTemplateId botCardImageTextTemplateId) {
		botCardImageTexts_hide.remove(botCardImageTextTemplateId);
	}

	/**
	 * 設定要隱藏的牌卡內容模板
	 * 
	 * @param botCardTextTemplateId
	 */
	public void addToBotCardText_hide(BotCardTextTemplateId botCardTextTemplateId) {
		botCardTexts_hide.add(botCardTextTemplateId);
	}

	/**
	 * 取得所有要隱藏的牌卡內容模板
	 * 
	 * @param botCardTextTemplateId
	 */
	public Set<BotCardTextTemplateId> getBotCardText_hide() {
		return botCardTexts_hide;
	}

	/**
	 * 刪除要隱藏的牌卡內容模板
	 * 
	 * @param botCardTextTemplateId
	 */
	public void removeFromBotCardText_hide(BotCardTextTemplateId botCardTextTemplateId) {
		botCardTexts_hide.remove(botCardTextTemplateId);
	}

	@Override
	public String toString() {
		return new JSONObject(tmp).toString(2);
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		}

		if (obj != null && obj instanceof BotCardShowData) {
			return toString().equals(obj.toString());
		}

		return false;
	}

	private enum _bot_card_template {
		bot_card_template, message, title, break_line
	}

	private enum _bot_card_image_template {
		bot_card_image_template, image_tag
	}

	private enum _bot_card_image_text_template {
		bot_card_image_text_template, image_text_title, image_text
	}

	private enum _bot_card_text_template {
		bot_card_text_template, card_text_label, text, unit, amount
	}
}
