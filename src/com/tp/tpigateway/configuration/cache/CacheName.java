package com.tp.tpigateway.configuration.cache;

public class CacheName {

    private CacheName() {
    }

    public static final String SESSION_STAGE_KEY = "session_stage";

    public static final String CUST_STAGE_REC_KEY = "cust_session_rec";

    public static String genCustStageRecKey(String customerId, String date) {
        return customerId + "_" + date;
    }
}
