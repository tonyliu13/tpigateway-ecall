package com.tp.tpigateway.configuration.cache;

import com.tp.tpigateway.model.common.CustSessionRec;
import com.tp.tpigateway.model.common.SessionStage;
import com.tp.tpigateway.repository.CustSessionRecRepository;
import com.tp.tpigateway.repository.SessionStageRepository;
import com.tp.tpigateway.util.DateUtil;
import com.tp.tpigateway.util.PropUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class InitCacheProcessor implements ApplicationListener {

    @Autowired
    private SessionStageRepository sessionStageRepository;

    @Autowired
    private CustSessionRecRepository custSessionRecRepository;

    @Autowired
    private CacheManager cacheManager;

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        // 上下文刷新事件
        if (applicationEvent instanceof ContextRefreshedEvent) {
            initSessionStageCache();
            initCustSessionRecCache();
        }
    }

    private void initSessionStageCache() {
        Cache sessionStageCache = cacheManager.getCache(CacheName.SESSION_STAGE_KEY);
        int min = PropUtils.DEFAULT_SESSION_UPD_TIME;

        Date date = DateUtil.addMinute(new Date(), -min);

        List<SessionStage> sessionStages = sessionStageRepository.findBySessionUpdTimeGreaterThanEqual(date);

        sessionStages.forEach(sessionStage ->
                sessionStageCache.put(sessionStage.getSessionId(), sessionStage)
        );
    }

    private void initCustSessionRecCache() {
        Cache custSessionRecCache = cacheManager.getCache(CacheName.CUST_STAGE_REC_KEY);

        //放當天的資料即可
        String todayStr = DateUtil.formatDate(new Date(), DateUtil.PATTERN_DATE);
        List<CustSessionRec> custSessionRecs = custSessionRecRepository.findByDate(todayStr);

        custSessionRecs.forEach(custSessionRec -> {
            String key = CacheName.genCustStageRecKey(custSessionRec.getCustomerId(), custSessionRec.getDate());
            custSessionRecCache.put(key, custSessionRec);
        });
    }

}
