package com.tp.tpigateway.configuration;

/**
 * 定義BotMessage設定相關常數
 * @author 李思穎
 * @建立日期：2018-08-21
 *
 */
public class BotMessageSetting {
	/**
	 * 將一般牌卡的資料存到APIResult(by bot_reply_content.data_key)時，應使用此值當key存入Map，再用bot_reply_content.data_key存入APIResult
	 */
	public static final String cardTemplateData = "_cardTemplateData";
	
	/**
	 * 是否顯示對話的key值
	 * botMessageService是否執行
	 * value值傳入布林boolean值，說明：true：顯示(預設)、false：不顯示
	 * 沒設定預設為true
	 */
	public final static String IS_SHOW = "botMsg_isShow";
	
	/**
	 * 更換Link Option選項的key值
	 * value值傳入說明：String[]，裡面放link_id
	 */
	public final static String REPLACE_OPTION_WITH_LINKID = "botMsg_replace_option";
	
}
