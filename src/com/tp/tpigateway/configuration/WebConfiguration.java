package com.tp.tpigateway.configuration;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.tp.tpigateway.util.PropUtils;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

@Configuration
@EnableWebMvc
public class WebConfiguration extends WebMvcConfigurerAdapter {

	private static Logger log = LoggerFactory.getLogger(WebConfiguration.class);

	@Bean
	public ViewResolver viewResolver() {
		log.error("WebConfiguration");
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/jsp/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(PropUtils.MULTIPART_MAX_FILE_SIZE);
		return multipartResolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/WEB-INF/resources/");
	}

//	@Bean
//	public RestTemplate okhttp3Template() {
//	    RestTemplate restTemplate = new RestTemplate();
//
//	    // create the okhttp client builder
//	    OkHttpClient.Builder builder = new OkHttpClient.Builder();
//	    ConnectionPool okHttpConnectionPool = new ConnectionPool(50, 30, TimeUnit.SECONDS);
//	    builder.connectionPool(okHttpConnectionPool);
//	    builder.connectTimeout(20, TimeUnit.SECONDS);
//	    builder.retryOnConnectionFailure(false);
//
//	    // embed the created okhttp client to a spring rest template
//	    restTemplate.setRequestFactory(new OkHttp3ClientHttpRequestFactory(builder.build()));
//	    return restTemplate;
//	}
}
