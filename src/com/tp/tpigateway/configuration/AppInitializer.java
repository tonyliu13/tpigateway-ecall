package com.tp.tpigateway.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;

public class AppInitializer implements WebApplicationInitializer {
 
	private static Logger log = LoggerFactory.getLogger(AppInitializer.class);
	
    public void onStartup(ServletContext container) throws ServletException {
    	log.error("AppInitializer");
    	//定義webAppRootKey 防止多個工程衝突,主要由log4j引發
    	container.setInitParameter("webAppRootKey","TPIGATEWAY_PATH");
    }
 
}
