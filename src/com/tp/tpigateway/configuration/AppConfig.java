package com.tp.tpigateway.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.tp.tpigateway")
public class AppConfig {
	
}

