package com.tp.tpigateway.model.life;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 定義 CB 10001 使用到的 VO
 *
 *   1. ChatFlow request VO
 *
 *   2. Cathay API request & response VO
 *
 *   3. TPIGateway return to ChatFlow VO
 */
public class CBV10001VO {

    /***************************************************************************
     * Cathay eCall API VO
     **************************************************************************/
    /**
     * 國泰 易call保 API 的 JSON 回傳結構如下，各 API 差異為 detail > data 的部份
     *
     * CathayBaseResponse 為 JSON 相同部份，將會變動的 data 使用泛型的方式，達到動態產生 JSON
     *
     *
     * {
     *     "returnCode": "0",
     *     "returnMsg": "API success",
     *     "UUID": "CB_API1331_20200221_DEC228F4C2_T01",
     *     "hostname": "cxlcsxat01"
     *     "detail": {
     *         "resultMsg": "傳入參數:會員生日必須為西元日期",
     *         "shortMsg": "未通過輸入檢核",
     *         "resultCode": "097",
     *         "data": {
     *             ...
     *         },
     *         "returnMessage": {
     *             "m_desc": [
     *                 "傳入參數: 會員生日必須為西元日期"
     *             ],
     *             "m_msgid": "",
     *             "m_sysid": "",
     *             "m_type": "",
     *             "m_url": "",
     *             "m_idx": 0,
     *             "m_code": -12
     *         }
     *     },
     * }
     *
     * @param <T>
     */
    public static class CathayBaseResponse<T> {
        private String returnCode;
        private String returnMsg;

        @JsonProperty("UUID")
        private String uuid;
        private String hostname;
        private CathayResponseDetail<T> detail;

        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getReturnMsg() {
            return returnMsg;
        }
        public void setReturnMsg(String returnMsg) {
            this.returnMsg = returnMsg;
        }
        public String getUuid() {
            return uuid;
        }
        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
        public String getHostname() {
            return hostname;
        }
        public void setHostname(String hostname) {
            this.hostname = hostname;
        }
        public CathayResponseDetail<T> getDetail() {
            return detail;
        }
        public void setDetail(CathayResponseDetail<T> detail) {
            this.detail = detail;
        }
    }

    /**
     * 國泰 易call保 API response JSON 的 detail 物件
     * @author Wayne Tsai
     *
     * @param <T>
     */
    public static class CathayResponseDetail<T> {
        private String resultMsg;
        private String shortMsg;
        private String resultCode;
        private T data;
        private CathayResponseReturnMessage returnMessage;

        public String getResultMsg() {
            return resultMsg;
        }
        public void setResultMsg(String resultMsg) {
            this.resultMsg = resultMsg;
        }
        public String getShortMsg() {
            return shortMsg;
        }
        public void setShortMsg(String shortMsg) {
            this.shortMsg = shortMsg;
        }
        public String getResultCode() {
            return resultCode;
        }
        public void setResultCode(String resultCode) {
            this.resultCode = resultCode;
        }
        public T getData() {
            return data;
        }
        public void setData(T data) {
            this.data = data;
        }
        public CathayResponseReturnMessage getReturnMessage() {
            return returnMessage;
        }
        public void setReturnMessage(CathayResponseReturnMessage returnMessage) {
            this.returnMessage = returnMessage;
        }
    }

    /**
     * 國泰 易call保 API response JSON 的 returnMessage 物件
     * @author Wayne Tsai
     */
    public static class CathayResponseReturnMessage {
        @JsonProperty("m_desc")
        private List<String> mDesc;
        @JsonProperty("m_msgid")
        private String mMsgid;
        @JsonProperty("m_sysid")
        private String mSysid;
        @JsonProperty("m_type")
        private String mType;
        @JsonProperty("m_url")
        private String mUrl;
        @JsonProperty("m_idx")
        private String mIdx;
        @JsonProperty("m_code")
        private String mCode;

        public List<String> getmDesc() {
            return mDesc;
        }
        public void setmDesc(List<String> mDesc) {
            this.mDesc = mDesc;
        }
        public String getmMsgid() {
            return mMsgid;
        }
        public void setmMsgid(String mMsgid) {
            this.mMsgid = mMsgid;
        }
        public String getmSysid() {
            return mSysid;
        }
        public void setmSysid(String mSysid) {
            this.mSysid = mSysid;
        }
        public String getmType() {
            return mType;
        }
        public void setmType(String mType) {
            this.mType = mType;
        }
        public String getmUrl() {
            return mUrl;
        }
        public void setmUrl(String mUrl) {
            this.mUrl = mUrl;
        }
        public String getmIdx() {
            return mIdx;
        }
        public void setmIdx(String mIdx) {
            this.mIdx = mIdx;
        }
        public String getmCode() {
            return mCode;
        }
        public void setmCode(String mCode) {
            this.mCode = mCode;
        }
    }

    /**
     * == 取得會員法定年齡 ==
     * 國泰 易call保 API response JSON 的 data 物件
     *
     * "data": {
     *     "AGE": "50",
     *     "VIP_BRDY": "1970-01-0",
     *     "ISSUE_DATE": "2020-02-20"
     * }
     */
    @JsonInclude(Include.NON_NULL)
    public static class LawAgeData {
        @JsonProperty("AGE")
        private String age;
        @JsonProperty("VIP_BRDY")
        private String vipBirthday;
        @JsonProperty("ISSUE_DATE")
        private String issueDate;

        public String getAge() {
            return age;
        }
        public void setAge(String age) {
            this.age = age;
        }
        public String getVipBirthday() {
            return vipBirthday;
        }
        public void setVipBirthday(String vipBirthday) {
            this.vipBirthday = vipBirthday;
        }
        public String getIssueDate() {
            return issueDate;
        }
        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }
    }

    /**
     * == 取得上一次投保紀錄  ==
     * 國泰 易call保 API response JSON 的 data 物件
     *
     * "data": {
     *     "BIRTHDAY": "0741222",
     *     "LOCAL_MEDICAL": "0",
     *     "AGE": "34",
     *     "IS_EMPLOYEE": "Y",
     *     "AGENT_QUALIFICATION": "Y",
     *     "OVERSEA_MEDICAL": "10",
     *     "OVERSEA_ILLNESS_TYPE": "3",
     *     "IS_MEMBER": "Y",
     *     "CREDITCARD_VALIDDATE": "202010",
     *     "LOCAL_MAIN": "100",
     *     "MEMBER_TYPE": "MAIN",
     *     "OVERSEA_ILLNESS": "20",
     *     "OVERSEA_MAIN": "100"
     * }
     */
    public static class LastIssueData {
        @JsonProperty("BIRTHDAY")
        private String birthday;
        @JsonProperty("LOCAL_MEDICAL")
        private String localMedical;
        @JsonProperty("AGE")
        private String age;
        @JsonProperty("IS_EMPLOYEE")
        private String isEmployee;
        @JsonProperty("AGENT_QUALIFICATION")
        private String agentQualification;
        @JsonProperty("OVERSEA_MEDICAL")
        private String overseaMedical;
        @JsonProperty("OVERSEA_ILLNESS_TYPE")
        private String overseaIllnessType;
        @JsonProperty("IS_MEMBER")
        private String isMember;
        @JsonProperty("CREDITCARD_VALIDDATE")
        private String creditcardValiddate;
        @JsonProperty("LOCAL_MAIN")
        private String localMain;
        @JsonProperty("MEMBER_TYPE")
        private String memberType;
        @JsonProperty("OVERSEA_ILLNESS")
        private String overseaIllness;
        @JsonProperty("OVERSEA_MAIN")
        private String overseaMain;

        public String getBirthday() {
            return birthday;
        }
        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }
        public String getLocalMedical() {
            return localMedical;
        }
        public void setLocalMedical(String localMedical) {
            this.localMedical = localMedical;
        }
        public String getAge() {
            return age;
        }
        public void setAge(String age) {
            this.age = age;
        }
        public String getIsEmployee() {
            return isEmployee;
        }
        public void setIsEmployee(String isEmployee) {
            this.isEmployee = isEmployee;
        }
        public String getAgentQualification() {
            return agentQualification;
        }
        public void setAgentQualification(String agentQualification) {
            this.agentQualification = agentQualification;
        }
        public String getOverseaMedical() {
            return overseaMedical;
        }
        public void setOverseaMedical(String overseaMedical) {
            this.overseaMedical = overseaMedical;
        }
        public String getOverseaIllnessType() {
            return overseaIllnessType;
        }
        public void setOverseaIllnessType(String overseaIllnessType) {
            this.overseaIllnessType = overseaIllnessType;
        }
        public String getIsMember() {
            return isMember;
        }
        public void setIsMember(String isMember) {
            this.isMember = isMember;
        }
        public String getCreditcardValiddate() {
            return creditcardValiddate;
        }
        public void setCreditcardValiddate(String creditcardValiddate) {
            this.creditcardValiddate = creditcardValiddate;
        }
        public String getLocalMain() {
            return localMain;
        }
        public void setLocalMain(String localMain) {
            this.localMain = localMain;
        }
        public String getMemberType() {
            return memberType;
        }
        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }
        public String getOverseaIllness() {
            return overseaIllness;
        }
        public void setOverseaIllness(String overseaIllness) {
            this.overseaIllness = overseaIllness;
        }
        public String getOverseaMain() {
            return overseaMain;
        }
        public void setOverseaMain(String overseaMain) {
            this.overseaMain = overseaMain;
        }
    }

    /**
     * == 整戶資料查詢  ==
     * 國泰 易call保 API response JSON 的 data 物件
     * "data":{
     *   "checkAGNT_ID":"00",
     *   "onlyVIP":"N",
     *   "newCASE_NO":"N",
     *   "a191_boList":[從屬會員陣列，見下方 SubMember物件
     *   ],
     *   "a190_bo":{主會員，見下方 MainMember物件
     *   },
     *   "PROD_ID":"Y",
     *   "notWelcome":"00",
     *   "newCASE_NO_INS":"N",
     *   "TRVL_PROD_ID":"N"
     * }
     * @see MainMember
     * @see SubMember
     */
    public static class AllVipData {
        @JsonProperty("checkAGNT_ID")
        private String checkAgntId;
        @JsonProperty("onlyVIP")
        private String onlyVip;
        @JsonProperty("a191_boList")
        private List<SubMember> subMemberList;
        @JsonProperty("newCASE_NO")
        private String newCaseNo;
        @JsonProperty("a190_bo")
        private MainMember mainMember;
        @JsonProperty("VIP_ID")
        private String vipId;
        @JsonProperty("PROD_ID")
        private String prodId;
        @JsonProperty("notWelcome")
        private String notWelcome;
        @JsonProperty("newCASE_NO_INS")
        private String newCaseNoIns;
        @JsonProperty("TRVL_PROD_ID")
        private String trvlProdId;

        public String getCheckAgntId() {
            return checkAgntId;
        }
        public void setCheckAgntId(String checkAgntId) {
            this.checkAgntId = checkAgntId;
        }
        public String getOnlyVip() {
            return onlyVip;
        }
        public void setOnlyVip(String onlyVip) {
            this.onlyVip = onlyVip;
        }
        public List<SubMember> getSubMemberList() {
            return subMemberList;
        }
        public void setSubMemberList(List<SubMember> subMemberList) {
            this.subMemberList = subMemberList;
        }
        public String getNewCaseNo() {
            return newCaseNo;
        }
        public void setNewCaseNo(String newCaseNo) {
            this.newCaseNo = newCaseNo;
        }
        public MainMember getMainMember() {
            return mainMember;
        }
        public void setMainMember(MainMember mainMember) {
            this.mainMember = mainMember;
        }
        public String getVipId() {
            return vipId;
        }
        public void setVipId(String vipId) {
            this.vipId = vipId;
        }
        public String getProdId() {
            return prodId;
        }
        public void setProdId(String prodId) {
            this.prodId = prodId;
        }
        public String getNotWelcome() {
            return notWelcome;
        }
        public void setNotWelcome(String notWelcome) {
            this.notWelcome = notWelcome;
        }
        public String getNewCaseNoIns() {
            return newCaseNoIns;
        }
        public void setNewCaseNoIns(String newCaseNoIns) {
            this.newCaseNoIns = newCaseNoIns;
        }
        public String getTrvlProdId() {
            return trvlProdId;
        }
        public void setTrvlProdId(String trvlProdId) {
            this.trvlProdId = trvlProdId;
        }

        /**
         * == 整戶資料查詢中的主會員物件 ==
         *   "a190_bo":{
         *     "case_no":"A030748397",
         *     "vip_id":"H29376308F",
         *     "vip_name":"謝○玲",
         *     "vip_brdy":"1974-05-02",
         *     "mobile":"0922123456",
         *     "zip_code":"116",
         *     "addr":"興隆路二段 203 巷 52 號 1",
         *     "tel_area":"02",
         *     "tel":"0227551399",
         *     "email":"XXXXX@cathlife.com.tw",
         *     "aply_date":"2019-01-16",
         *     "card_no":"4284306866737233",
         *     "card_name_ym":"202206",
         *     "card_kind":"1",
         *     "agnt_id":"A17224802A",
         *     "agnt_name":"謝○玲",
         *     "agnt_mobile":"0922123456",
         *     "agnt_div_no":"A819213",
         *     "input_id":"T25969399A",
         *     "input_div_no":"9P06201",
         *     "input_name":"顏○庭",
         *     "vip_type":"1",
         *     "input_date":"2019-01-16 14:43:24.25",
         *     "aply_sts":"1",
         *     "agnt_licence":"0111100932",
         *     "case_no_ins":"E0810236241",
         *     "agnt_license_ins":"A15E583160",
         *     "national":"7",
         *     "national_memo":"中華民國",
         *     "risk_occu_code":"J800",
         *     "risk_jobtitle_code":"T010",
         *     "m_retMsg":{
         *       "m_desc":[
         *         ""
         *       ],
         *       "m_msgid":"",
         *       "m_sysid":"",
         *       "m_type":"",
         *       "m_url":"",
         *       "m_idx":0,
         *       "m_code":0
         *     }
         *   }
         */
        @JsonIgnoreProperties(ignoreUnknown = true,
                value = {"CASE_NO", "zip_code", "addr", "tel_area", "tel",
                        "tel_ext", "aply_date", "card_kind", "agnt_id", "agnt_name",
                        "agnt_mobile", "agnt_div_no", "input_id", "input_div_no", "input_name",
                        "vip_type", "input_date", "agnt_tel_area", "agnt_tel", "agnt_tel_ext",
                        "aply_sts", "agnt_licence", "NATIONAL", "NATIONAL_MEMO", "RISK_OCCU_CODE",
                        "RISK_JOBTITLE_CODE", "m_retMsg", "msgDesc", "type", "url",
                        "sysid", "returnCode", "msgid", "RISK_JOBTITLE_CODE"})
        @JsonInclude(Include.NON_NULL)
        public static class MainMember {
            @JsonProperty("CASE_NO")
            private String caseNo;
            @JsonProperty("VIP_ID")
            private String vipId;
            @JsonProperty("VIP_NAME")
            private String vipName;
            @JsonProperty("VIP_BRDY")
            private String vipBirthday;
            @JsonProperty("MOBILE")
            private String mobile;
            @JsonProperty("zip_code")
            private String zipCode;
            @JsonProperty("addr")
            private String address;
            @JsonProperty("tel_area")
            private String telArea;
            @JsonProperty("tel")
            private String tel;
            @JsonProperty("tel_ext")
            private String telExt;
            @JsonProperty("EMAIL")
            private String email;
            @JsonProperty("aply_date")
            private String applyDate;
            @JsonProperty("CARD_NO")
            private String cardNo;
            @JsonProperty("CARD_NAME_YM")
            private String cardNameYm;
            @JsonProperty("card_kind")
            private String cardKind;
            @JsonProperty("agnt_id")
            private String agntId;
            @JsonProperty("agnt_name")
            private String agntName;
            @JsonProperty("agnt_mobile")
            private String agntMobile;
            @JsonProperty("agnt_div_no")
            private String agntDivNo;
            @JsonProperty("input_id")
            private String inputId;
            @JsonProperty("input_div_no")
            private String inputDivNo;
            @JsonProperty("input_name")
            private String inputName;
            @JsonProperty("vip_type")
            private String vipType;
            @JsonProperty("input_date")
            private String inputDate;
            @JsonProperty("agnt_tel_area")
            private String agntTelArea;
            @JsonProperty("agnt_tel")
            private String agntTel;
            @JsonProperty("agnt_tel_ext")
            private String agntTelExt;
            @JsonProperty("aply_sts")
            private String aplySts;
            @JsonProperty("agnt_licence")
            private String agntLicence;
            @JsonProperty("NATIONAL")
            private String national;
            @JsonProperty("CASE_NO_INS")
            private String caseNoIns;
            @JsonProperty("NATIONAL_MEMO")
            private String nationalMemo;
            @JsonProperty("RISK_OCCU_CODE")
            private String riskOccuCode;
            @JsonProperty("RISK_JOBTITLE_CODE")
            private String riskJobtitleCode;
            @JsonProperty("msgDesc")
            private String msgDesc;
            @JsonProperty("type")
            private String type;
            @JsonProperty("SEX")
            private String sex;
            @JsonProperty("sysid")
            private String sysid;
            @JsonProperty("msgid")
            private String msgid;
            @JsonProperty("url")
            private String url;
            @JsonProperty("returnCode")
            private String returnCode;
            @JsonProperty("m_retMsg")
            private CathayResponseReturnMessage mRetMsg;

            public String getCaseNo() {
                return caseNo;
            }
            public void setCaseNo(String caseNo) {
                this.caseNo = caseNo;
            }
            public String getVipId() {
                return vipId;
            }
            public void setVipId(String vipId) {
                this.vipId = vipId;
            }
            public String getVipName() {
                return vipName;
            }
            public void setVipName(String vipName) {
                this.vipName = vipName;
            }
            public String getVipBirthday() {
                return vipBirthday;
            }
            public void setVipBirthday(String vipBirthday) {
                this.vipBirthday = vipBirthday;
            }
            public String getMobile() {
                return mobile;
            }
            public void setMobile(String mobile) {
                this.mobile = mobile;
            }
            public String getZipCode() {
                return zipCode;
            }
            public void setZipCode(String zipCode) {
                this.zipCode = zipCode;
            }
            public String getAddress() {
                return address;
            }
            public void setAddress(String address) {
                this.address = address;
            }
            public String getTelArea() {
                return telArea;
            }
            public void setTelArea(String telArea) {
                this.telArea = telArea;
            }
            public String getTel() {
                return tel;
            }
            public void setTel(String tel) {
                this.tel = tel;
            }
            public String getTelExt() {
                return telExt;
            }
            public void setTelExt(String telExt) {
                this.telExt = telExt;
            }
            public String getEmail() {
                return email;
            }
            public void setEmail(String email) {
                this.email = email;
            }
            public String getApplyDate() {
                return applyDate;
            }
            public void setApplyDate(String applyDate) {
                this.applyDate = applyDate;
            }
            public String getCardNo() {
                return cardNo;
            }
            public void setCardNo(String cardNo) {
                this.cardNo = cardNo;
            }
            public String getCardNameYm() {
                return cardNameYm;
            }
            public void setCardNameYm(String cardNameYm) {
                this.cardNameYm = cardNameYm;
            }
            public String getCardKind() {
                return cardKind;
            }
            public CathayResponseReturnMessage getmRetMsg() {
                return mRetMsg;
            }
            public void setmRetMsg(CathayResponseReturnMessage mRetMsg) {
                this.mRetMsg = mRetMsg;
            }
            public void setCardKind(String cardKind) {
                this.cardKind = cardKind;
            }
            public String getAgntId() {
                return agntId;
            }
            public void setAgntId(String agntId) {
                this.agntId = agntId;
            }
            public String getAgntName() {
                return agntName;
            }
            public void setAgntName(String agntName) {
                this.agntName = agntName;
            }
            public String getAgntMobile() {
                return agntMobile;
            }
            public void setAgntMobile(String agntMobile) {
                this.agntMobile = agntMobile;
            }
            public String getAgntDivNo() {
                return agntDivNo;
            }
            public void setAgntDivNo(String agntDivNo) {
                this.agntDivNo = agntDivNo;
            }
            public String getInputId() {
                return inputId;
            }
            public void setInputId(String inputId) {
                this.inputId = inputId;
            }
            public String getInputDivNo() {
                return inputDivNo;
            }
            public void setInputDivNo(String inputDivNo) {
                this.inputDivNo = inputDivNo;
            }
            public String getInputName() {
                return inputName;
            }
            public void setInputName(String inputName) {
                this.inputName = inputName;
            }
            public String getVipType() {
                return vipType;
            }
            public void setVipType(String vipType) {
                this.vipType = vipType;
            }
            public String getInputDate() {
                return inputDate;
            }
            public void setInputDate(String inputDate) {
                this.inputDate = inputDate;
            }
            public String getAgntTelArea() {
                return agntTelArea;
            }
            public void setAgntTelArea(String agntTelArea) {
                this.agntTelArea = agntTelArea;
            }
            public String getAgntTel() {
                return agntTel;
            }
            public void setAgntTel(String agntTel) {
                this.agntTel = agntTel;
            }
            public String getAgntTelExt() {
                return agntTelExt;
            }
            public void setAgntTelExt(String agntTelExt) {
                this.agntTelExt = agntTelExt;
            }
            public String getAplySts() {
                return aplySts;
            }
            public void setAplySts(String aplySts) {
                this.aplySts = aplySts;
            }
            public String getAgntLicence() {
                return agntLicence;
            }
            public void setAgntLicence(String agntLicence) {
                this.agntLicence = agntLicence;
            }
            public String getNational() {
                return national;
            }
            public void setNational(String national) {
                this.national = national;
            }
            public String getCaseNoIns() {
                return caseNoIns;
            }
            public void setCaseNoIns(String caseNoIns) {
                this.caseNoIns = caseNoIns;
            }
            public String getNationalMemo() {
                return nationalMemo;
            }
            public void setNationalMemo(String nationalMemo) {
                this.nationalMemo = nationalMemo;
            }
            public String getRiskOccuCode() {
                return riskOccuCode;
            }
            public void setRiskOccuCode(String riskOccuCode) {
                this.riskOccuCode = riskOccuCode;
            }
            public String getRiskJobtitleCode() {
                return riskJobtitleCode;
            }
            public void setRiskJobtitleCode(String riskJobtitleCode) {
                this.riskJobtitleCode = riskJobtitleCode;
            }
            public String getMsgDesc() {
                return msgDesc;
            }
            public void setMsgDesc(String msgDesc) {
                this.msgDesc = msgDesc;
            }
            public String getType() {
                return type;
            }
            public void setType(String type) {
                this.type = type;
            }
            public String getSex() {
                String sexType = sex;
                switch(sex) {
                    case "1":
                        sexType = "先生";
                        break;
                    case "2":
                        sexType = "小姐";
                        break;
                    default:
                }
                return sexType;
            }
            public void setSex(String sex) {
                this.sex = sex;
            }
            public String getSysid() {
                return sysid;
            }
            public void setSysid(String sysid) {
                this.sysid = sysid;
            }
            public String getMsgid() {
                return msgid;
            }
            public void setMsgid(String msgid) {
                this.msgid = msgid;
            }
            public String getUrl() {
                return url;
            }
            public void setUrl(String url) {
                this.url = url;
            }
        }

        /**
         * == 整戶資料查詢中的從屬會員物件 ==
         *     {
         *       "case_no":"A030748397",
         *       "vip_id":"H29376308F",
         *       "rela_id":"A13042355K",
         *       "rela_name":"傅○熙",
         *       "rela_brdy":"2008-05-28",
         *       "mobile":"0922123456",
         *       "rela_kind":"4",
         *       "law_name":"",
         *       "law_rela":"",
         *       "rela_type":"1",
         *       "national":"TW",
         *       "case_no_ins":"E0810236241",
         *       "national_memo":"中華民國",
         *       "risk_occu_code":"J800",
         *       "risk_jobtitle_code":"T010",
         *       "m_retMsg":{
         *         "m_desc":[
         *           ""
         *         ],
         *         "m_msgid":"",
         *         "m_sysid":"",
         *         "m_type":"",
         *         "m_url":"",
         *         "m_idx":0,
         *         "m_code":0
         *       }
         *     }
         */
        @JsonIgnoreProperties(ignoreUnknown = true,
                value = {"CASE_NO", "mobile", "RELA_KIND", "LAW_NAME", "LAW_RELA",
                        "RELA_TYPE", "NATIONAL", "NATIONAL_MEMO", "RISK_OCCU_CODE", "risk_jobtitle_code",
                        "msgDesc", "type", "MOBILE", "sysid", "returnCode",
                        "msgid", "url", "RISK_JOBTITLE_CODE"})
        @JsonInclude(Include.NON_NULL)
        public static class SubMember {
            @JsonProperty("CASE_NO")
            private String caseNo;
            @JsonProperty("VIP_ID")
            private String vipId;
            @JsonProperty("RELA_ID")
            private String relaId;
            @JsonProperty("RELA_NAME")
            private String relaName;
            @JsonProperty("RELA_BRDY")
            private String relaBrdy;
            @JsonProperty("MOBILE")
            private String mobile;
            @JsonProperty("RELA_KIND")
            private String relKind;
            @JsonProperty("LAW_NAME")
            private String lawName;
            @JsonProperty("LAW_RELA")
            private String laRela;
            @JsonProperty("RELA_TYPE")
            private String relaType;
            @JsonProperty("NATIONAL")
            private String national;
            @JsonProperty("CASE_NO_INS")
            private String caseNoIns;
            @JsonProperty("NATIONAL_MEMO")
            private String nationalMemo;
            @JsonProperty("RISK_OCCU_CODE")
            private String riskOccuCode;
            @JsonProperty("RISK_JOBTITLE_CODE")
            private String riskJobtitleCode;
            @JsonProperty("msgDesc")
            private String msgDesc;
            @JsonProperty("type")
            private String type;
            @JsonProperty("SEX")
            private String sex;
            @JsonProperty("sysid")
            private String sysid;
            @JsonProperty("msgid")
            private String msgid;
            @JsonProperty("url")
            private String url;
            @JsonProperty("returnCode")
            private String returnCode;

            public String getCaseNo() {
                return caseNo;
            }
            public void setCaseNo(String caseNo) {
                this.caseNo = caseNo;
            }
            public String getVipId() {
                return vipId;
            }
            public void setVipId(String vipId) {
                this.vipId = vipId;
            }
            public String getRelaId() {
                return relaId;
            }
            public void setRelaId(String relaId) {
                this.relaId = relaId;
            }
            public String getRelaName() {
                return relaName;
            }
            public void setRelaName(String relaName) {
                this.relaName = relaName;
            }
            public String getRelaBrdy() {
                return relaBrdy;
            }
            public void setRelaBrdy(String relaBrdy) {
                this.relaBrdy = relaBrdy;
            }
            public String getMobile() {
                return mobile;
            }
            public void setMobile(String mobile) {
                this.mobile = mobile;
            }
            public String getRelKind() {
                return relKind;
            }
            public void setRelKind(String relKind) {
                this.relKind = relKind;
            }
            public String getLawName() {
                return lawName;
            }
            public void setLawName(String lawName) {
                this.lawName = lawName;
            }
            public String getLaRela() {
                return laRela;
            }
            public void setLaRela(String laRela) {
                this.laRela = laRela;
            }
            public String getRelaType() {
                return relaType;
            }
            public void setRelaType(String relaType) {
                this.relaType = relaType;
            }
            public String getNational() {
                return national;
            }
            public void setNational(String national) {
                this.national = national;
            }
            public String getCaseNoIns() {
                return caseNoIns;
            }
            public void setCaseNoIns(String caseNoIns) {
                this.caseNoIns = caseNoIns;
            }
            public String getNationalMemo() {
                return nationalMemo;
            }
            public void setNationalMemo(String nationalMemo) {
                this.nationalMemo = nationalMemo;
            }
            public String getRiskOccuCode() {
                return riskOccuCode;
            }
            public void setRiskOccuCode(String riskOccuCode) {
                this.riskOccuCode = riskOccuCode;
            }
            public String getRiskJobtitleCode() {
                return riskJobtitleCode;
            }
            public void setRiskJobtitleCode(String riskJobtitleCode) {
                this.riskJobtitleCode = riskJobtitleCode;
            }
            public String getMsgDesc() {
                return msgDesc;
            }
            public void setMsgDesc(String msgDesc) {
                this.msgDesc = msgDesc;
            }
            public String getType() {
                return type;
            }
            public void setType(String type) {
                this.type = type;
            }
            public String getSex() {
                String sexType = sex;
                switch(sex) {
                    case "1":
                        sexType = "先生";
                        break;
                    case "2":
                        sexType = "小姐";
                        break;
                    default:
                }
                return sexType;
            }
            public void setSex(String sex) {
                this.sex = sex;
            }
            public String getSysid() {
                return sysid;
            }
            public void setSysid(String sysid) {
                this.sysid = sysid;
            }
            public String getMsgid() {
                return msgid;
            }
            public void setMsgid(String msgid) {
                this.msgid = msgid;
            }
            public String getUrl() {
                return url;
            }
            public void setUrl(String url) {
                this.url = url;
            }
        }
    }

    /**
     * == 契約成立 ==
     * 國泰 易call保 API response JSON 的 data 物件
     * "data": {
     *     "APLY_NO": "0010700001"
     * }
     */
    public static class ConfirmVoiceData {
        @JsonProperty("APLY_NO")
        private String aplyNo;

        public String getAplyNo() {
            return aplyNo;
        }
        public void setAplyNo(String aplyNo) {
            this.aplyNo = aplyNo;
        }
    }

    /**
     * == 暫存 ==
     * 國泰 易call保 API response JSON 的 data 物件
     *
     * "data": {
     *     "RGN_CODE": "10",
     *     "ISSUE_DATE": "2020-03-20",
     *     "ISSUE_DAYS": "3",
     *     "APLY_NO": "0010700001",
     *     "SRC": "G",
     *     "BGN_TIME": "08:00:00"
     *     "INSD_LIST": [
     *         {
     *             "INSD_ID": "N26648942K",
     *             "INSD_BRDY": "1990-07-29",
     *             "HD_AMT": "100",
     *             "HP_AMT": "10",
     *             "HK_TYPE": "3",
     *             "HK_AMT": "10",
     *             "OMTP_AMT": "1",
     *             "TRVL_NOT_CONV": "N"
     *         },
     *         {
     *             "INSD_ID": "L22246387P",
     *             "INSD_BRDY": "1990-07-29",
     *             "HD_AMT": "100",
     *             "HP_AMT": "10",
     *             "HK_TYPE": "3",
     *             "HK_AMT": "10",
     *             "OMTP_AMT": "1",
     *             "TRVL_NOT_CONV": "N"
     *         }
     *     ],
     * }
     */
    public static class TempVoiceData {
        @JsonProperty("RGN_CODE")
        private String rgnCode;
        @JsonProperty("ISSUE_DATE")
        private String issueDate;
        @JsonProperty("ISSUE_DAYS")
        private String issueDays;
        @JsonProperty("APLY_NO")
        private String aplyNo;
        @JsonProperty("SRC")
        private String src;
        @JsonProperty("BGN_TIME")
        private String bgnTime;
        @JsonProperty("INSD_LIST")
        private List<InsdList> insdList;

        public String getRgnCode() {
            return rgnCode;
        }
        public void setRgnCode(String rgnCode) {
            this.rgnCode = rgnCode;
        }
        public String getIssueDate() {
            return issueDate;
        }
        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }
        public String getIssueDays() {
            return issueDays;
        }
        public void setIssueDays(String issueDays) {
            this.issueDays = issueDays;
        }
        public String getAplyNo() {
            return aplyNo;
        }
        public void setAplyNo(String aplyNo) {
            this.aplyNo = aplyNo;
        }
        public String getSrc() {
            return src;
        }
        public void setSrc(String src) {
            this.src = src;
        }
        public String getBgnTime() {
            return bgnTime;
        }
        public void setBgnTime(String bgnTime) {
            this.bgnTime = bgnTime;
        }
        public List<InsdList> getInsdList() {
            return insdList;
        }
        public void setInsdList(List<InsdList> insdList) {
            this.insdList = insdList;
        }

        public static class InsdList {
            @JsonProperty("INSD_ID")
            private String insdId;
            @JsonProperty("INSD_BRDY")
            private String insdBrdy;
            @JsonProperty("HD_AMT")
            private String hdAmt;
            @JsonProperty("HP_AMT")
            private String hpAmt;
            @JsonProperty("HK_TYPE")
            private String hkType;
            @JsonProperty("HK_AMT")
            private String hkAmt;
            @JsonProperty("OMTP_AMT")
            private String omtpAmt;
            @JsonProperty("TRVL_NOT_CONV")
            private String trvlNotConv;

            public String getInsdId() {
                return insdId;
            }
            public void setInsdId(String insdId) {
                this.insdId = insdId;
            }
            public String getInsdBrdy() {
                return insdBrdy;
            }
            public void setInsdBrdy(String insdBrdy) {
                this.insdBrdy = insdBrdy;
            }
            public String getHdAmt() {
                return hdAmt;
            }
            public void setHdAmt(String hdAmt) {
                this.hdAmt = hdAmt;
            }
            public String getHpAmt() {
                return hpAmt;
            }
            public void setHpAmt(String hpAmt) {
                this.hpAmt = hpAmt;
            }
            public String getHkType() {
                return hkType;
            }
            public void setHkType(String hkType) {
                this.hkType = hkType;
            }
            public String getHkAmt() {
                return hkAmt;
            }
            public void setHkAmt(String hkAmt) {
                this.hkAmt = hkAmt;
            }
            public String getOmtpAmt() {
                return omtpAmt;
            }
            public void setOmtpAmt(String omtpAmt) {
                this.omtpAmt = omtpAmt;
            }
            public String getTrvlNotConv() {
                return trvlNotConv;
            }
            public void setTrvlNotConv(String trvlNotConv) {
                this.trvlNotConv = trvlNotConv;
            }
        }
    }

    /**
     * == 暫存 ==
     * 國泰 易call保 API response JSON 的 data 物件
     * "data": {
     *     "RGN_CODE":"10",
     *     "ISSUE_DATE":"2020-03-20",
     *     "ISSUE_DAYS":"3",
     *     "APLY_NO":"0010700001",
     *     "INSD_LIST":[
     *         {
     *             "INSD_ID":"N26648942K",
     *             "INSD_BRDY":"1990-07-29",
     *             "HD_AMT":"100",
     *             "HP_AMT":"10",
     *             "HK_TYPE":"3",
     *             "HK_AMT":"10",
     *             "OMTP_AMT":"1",
     *             "TRVL_NOT_CONV":"N",
     *             "SELF_PREM":"1000"
     *         },
     *         ...
     *     ],
     *     "SRC":"G",
     *     "BGN_TIME":"08:00:00",
     *     "TOT_PREM":"2000"
     * }
     */
    public static class CheckAmtVoiceData {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("TOT_PREM")
        private String totPrem;
        @JsonProperty("APLY_NO")
        private String aplyNo;
        @JsonProperty("ISSUE_DATE")
        private String issueDate;
        @JsonProperty("BGN_TIME")
        private String bgnTime;
        @JsonProperty("ISSUE_DAYS")
        private String issueDays;
        @JsonProperty("RGN_CODE")
        private String rgnCode;
        @JsonProperty("INSD_LIST")
        private List<InsdList> insdList;

        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getTotPrem() {
			return totPrem;
		}
		public void setTotPrem(String totPrem) {
			this.totPrem = totPrem;
		}
		public String getAplyNo() {
            return aplyNo;
        }
        public void setAplyNo(String aplyNo) {
            this.aplyNo = aplyNo;
        }
        public String getIssueDate() {
            return issueDate;
        }
        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }
        public String getBgnTime() {
            return bgnTime;
        }
        public void setBgnTime(String bgnTime) {
            this.bgnTime = bgnTime;
        }
        public String getIssueDays() {
            return issueDays;
        }
        public void setIssueDays(String issueDays) {
            this.issueDays = issueDays;
        }
        public String getRgnCode() {
            return rgnCode;
        }
        public void setRgnCode(String rgnCode) {
            this.rgnCode = rgnCode;
        }
        public List<InsdList> getInsdList() {
            return insdList;
        }
        public void setInsdList(List<InsdList> insdList) {
            this.insdList = insdList;
        }

        public static class InsdList {
            @JsonProperty("INSD_ID")
            private String insdId;
            @JsonProperty("INSD_BRDY")
            private String insdBrdy;
            @JsonProperty("HD_AMT")
            private String hdAmt;
            @JsonProperty("HP_AMT")
            private String hpAmt;
            @JsonProperty("HK_TYPE")
            private String hkType;
            @JsonProperty("HK_AMT")
            private String hkAmt;
            @JsonProperty("OMTP_AMT")
            private String omtpAmt;
            @JsonProperty("TRVL_NOT_CONV")
            private String trvlNotConv;
            @JsonProperty("SELF_PREM")
            private String selfPrem;

            public String getInsdId() {
                return insdId;
            }
            public void setInsdId(String insdId) {
                this.insdId = insdId;
            }
            public String getInsdBrdy() {
                return insdBrdy;
            }
            public void setInsdBrdy(String insdBrdy) {
                this.insdBrdy = insdBrdy;
            }
            public String getHdAmt() {
                return hdAmt;
            }
            public void setHdAmt(String hdAmt) {
                this.hdAmt = hdAmt;
            }
            public String getHpAmt() {
                return hpAmt;
            }
            public void setHpAmt(String hpAmt) {
                this.hpAmt = hpAmt;
            }
            public String getHkType() {
                return hkType;
            }
            public void setHkType(String hkType) {
                this.hkType = hkType;
            }
            public String getHkAmt() {
                return hkAmt;
            }
            public void setHkAmt(String hkAmt) {
                this.hkAmt = hkAmt;
            }
            public String getOmtpAmt() {
                return omtpAmt;
            }
            public void setOmtpAmt(String omtpAmt) {
                this.omtpAmt = omtpAmt;
            }
            public String getTrvlNotConv() {
                return trvlNotConv;
            }
            public void setTrvlNotConv(String trvlNotConv) {
                this.trvlNotConv = trvlNotConv;
            }
            public String getSelfPrem() {
                return selfPrem;
            }
            public void setSelfPrem(String selfPrem) {
                this.selfPrem = selfPrem;
            }
        }
    }


    /***************************************************************************
     * TPIGateway VO
     **************************************************************************/
    /**
     * TPIGateway 回傳至 Flow 的 JSON 結構，各 API 的差異部份為 "content" 物件的內容
     *
     * ResponseBase 為 JSON 相同部份，將會變動的 content 使用泛型的方式，達到動態產生 JSON
     *
     * {
     *     "msg": "success",
     *     "code": 0,
     *     "data": {
     *         "from": "bot",
     *         "showtime": "2020-03-05 11:40:20",
     *         "content": {
     *             ...
     *         },
     *         "channel": "cathaylife",
     *         "source": "cathaylife",
     *         "role": "cathaylife",
     *         "fb": false,
     *         "fbIntent": null,
     *         "sbIntent": null
     *     }
     * }
     *
     * @param <T>
     */
    public static class GatewayResponse<T> {
        private static final int CODE_SUCCESS = 0;
        private static final int CODE_FAIL = -1;
        private static final int CODE_TIMEOUT = -2;
        
        private String msg;
        private int code;
        private GatewayResponseData<T> data;

        private GatewayResponse() {
        }
        private GatewayResponse(int code) {
        	this.code =  code;
        }
        private GatewayResponse(int code, String msg) {
            this.code =  code;
            this.msg = msg;
        }

        public static <T> GatewayResponse<T> buildSuccess(Class<T> clazz) {
            return new GatewayResponse<T>(CODE_SUCCESS);
        }
        public static <T> GatewayResponse<T> buildSuccess(Class<T> clazz, String msg) {
            return new GatewayResponse<T>(CODE_SUCCESS, msg);
        }
        public static <T> GatewayResponse<T> buildFail(Class<T> clazz) {
            return new GatewayResponse<T>(CODE_FAIL);
        }
        public static GatewayResponse<?> buildFail(String msg) {
            return new GatewayResponse<>(CODE_FAIL, msg);
        }
        public static <T> GatewayResponse<T> buildFail(Class<T> clazz, String msg) {
            return new GatewayResponse<T>(CODE_FAIL, msg);
        }
        public static GatewayResponse<?> buildTimeout() {
            return new GatewayResponse<>(CODE_TIMEOUT);
        }
        public static <T> GatewayResponseData<T> buildDefaultData() {
            return new GatewayResponseData<T>();
        }
        public static <T> GatewayResponseData<T> buildData(String channel, String source, String role, String from) {
            return new GatewayResponseData<T>(channel, source, role, from);
        }

        public String getMsg() {
            return msg;
        }
        public void setMsg(String msg) {
            this.msg = msg;
        }
        public int getCode() {
            return code;
        }
        public void setCode(int code) {
            this.code = code;
        }
        public GatewayResponseData<T> getData() {
            return data;
        }
        public void setData(GatewayResponseData<T> data) {
            this.data = data;
        }
    }

    /**
     * TPIGateway 回傳至 ChatFlow 的 JSON 的 data 物件
     *
     * @see GatewayResponse
     */
    public static class GatewayResponseData<T> {
        private static final String CATHAY_LIFE = "cathaylife";
        private static final String FROM_BOT = "bot";
        private static final DateTimeFormatter DT_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        private String channel;
        private String source;
        private String role;
        private boolean fb;
        private String fbIntent;
        private String sbIntent;
        private String from;
        private String showtime;
        private String resultMsg;
        private String shortMsg;
        private T content;

        private GatewayResponseData() {
            this(CATHAY_LIFE, CATHAY_LIFE, CATHAY_LIFE, FROM_BOT);
        }
        private GatewayResponseData(String channel, String source, String role, String from) {
            this.channel = channel;
            this.source = source;
            this.role = role;
            this.from = from;
            this.fb = false;
            this.showtime = LocalDateTime.now().format(DT_FORMAT);
        }
        public String getChannel() {
            return channel;
        }
        public void setChannel(String channel) {
            this.channel = channel;
        }
        public String getSource() {
            return source;
        }
        public void setSource(String source) {
            this.source = source;
        }
        public String getRole() {
            return role;
        }
        public void setRole(String role) {
            this.role = role;
        }
        public boolean isFb() {
            return fb;
        }
        public void setFb(boolean fb) {
            this.fb = fb;
        }
        public String getFbIntent() {
            return fbIntent;
        }
        public void setFbIntent(String fbIntent) {
            this.fbIntent = fbIntent;
        }
        public String getSbIntent() {
            return sbIntent;
        }
        public void setSbIntent(String sbIntent) {
            this.sbIntent = sbIntent;
        }
        public String getFrom() {
            return from;
        }
        public void setFrom(String from) {
            this.from = from;
        }
        public String getShowtime() {
            return showtime;
        }
        public void setShowtime(String showtime) {
            this.showtime = showtime;
        }
        public String getResultMsg() {
			return resultMsg;
		}
		public void setResultMsg(String resultMsg) {
			this.resultMsg = resultMsg;
		}
		public String getShortMsg() {
			return shortMsg;
		}
		public void setShortMsg(String shortMsg) {
			this.shortMsg = shortMsg;
		}
		public T getContent() {
            return content;
        }
        public void setContent(T content) {
            this.content = content;
        }
    }

    /**
     * == 取得會員法定年齡 ==
     * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
     *
     * "content": {
     *     "RTN_CODE": "000",
     *     "AGE": [12, 50],
     *     "ISSUE_DATE": "2020-02-20",
     *     "VIP_BRDY": ["1970-01-01", "1980-08-08"]
     * }
     */
    public static class ContentLawAge {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("AGE")
        private List<String> age;
        @JsonProperty("ISSUE_DATE")
        private String issueDate;
        @JsonProperty("VIP_BRDY")
        private List<String> vipBirthday;

        public ContentLawAge() {
        }
        public ContentLawAge(String returnCode, List<String> age, String issueDate, List<String> vipBirthday) {
            this.returnCode = returnCode;
            this.age = age;
            this.issueDate = issueDate;
            this.vipBirthday = vipBirthday;
        }
        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public List<String> getAge() {
            return age;
        }
        public void setAge(List<String> age) {
            this.age = age;
        }
        public String getIssueDate() {
            return issueDate;
        }
        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }
        public List<String> getVipBirthday() {
            return vipBirthday;
        }
        public void setVipBirthday(List<String> vipBirthday) {
            this.vipBirthday = vipBirthday;
        }
    }

    /**
     * == 取得上一次投保紀錄 ==
     * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
     * "content": {
     *     "RTN_CODE": "000",
     *     "LOCAL_MAIN": "100",
     *     "LOCAL_MEDICAL": "0",
     *     "OVERSEA_MAIN": "100",
     *     "OVERSEA_MEDICAL": "10",
     *     "OVERSEA_ILLNESS": "20",
     *     "OVERSEA_ILLNESS_TYPE": "3"
     * }
     *
     * @see GatewayResponse
     */
    public static class ContentLastIssueData {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("LOCAL_MAIN")
        private String localMain;
        @JsonProperty("LOCAL_MEDICAL")
        private String localMedical;
        @JsonProperty("OVERSEA_MAIN")
        private String overseaMain;
        @JsonProperty("OVERSEA_MEDICAL")
        private String overseaMedical;
        @JsonProperty("OVERSEA_ILLNESS")
        private String overseaIllness;
        @JsonProperty("OVERSEA_ILLNESS_TYPE")
        private String overseaIllnessType;

        public ContentLastIssueData() {
        }
        public ContentLastIssueData(String rtnCode, String localMain, String localMedical, String overseaMain,
                String overseaMedical, String overseaIllness, String overseaIllnessType) {
            super();
            this.returnCode = rtnCode;
            this.localMain = localMain;
            this.localMedical = localMedical;
            this.overseaMain = overseaMain;
            this.overseaMedical = overseaMedical;
            this.overseaIllness = overseaIllness;
            this.overseaIllnessType = overseaIllnessType;
        }
        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getLocalMain() {
            return localMain;
        }
        public void setLocalMain(String localMain) {
            this.localMain = localMain;
        }
        public String getLocalMedical() {
            return localMedical;
        }
        public void setLocalMedical(String localMedical) {
            this.localMedical = localMedical;
        }
        public String getOverseaMain() {
            return overseaMain;
        }
        public void setOverseaMain(String overseaMain) {
            this.overseaMain = overseaMain;
        }
        public String getOverseaMedical() {
            return overseaMedical;
        }
        public void setOverseaMedical(String overseaMedical) {
            this.overseaMedical = overseaMedical;
        }
        public String getOverseaIllness() {
            return overseaIllness;
        }
        public void setOverseaIllness(String overseaIllness) {
            this.overseaIllness = overseaIllness;
        }
        public String getOverseaIllnessType() {
            return overseaIllnessType;
        }
        public void setOverseaIllnessType(String overseaIllnessType) {
            this.overseaIllnessType = overseaIllnessType;
        }
    }

    /**
     * == 整戶資料查詢 ==
     * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
     *
     * "content": {
     *   "RTN_CODE": "000",
     *   "checkAGNT_ID": "00",
     *   "PROD_ID": "Y",
     *   "TRVL_PROD_ID": "M",
     *   "onlyVIP": "N",
     *   "newCASE_NO": "Y",
     *   "newCASE_NO_INS": "Y",
     *   "notWelcome": "00",
     *   "a190_bo": {主會員 Mainmember
     *   },
     *   "a191_boList": [從屬會員 SubMember
     *   ]
     * }
     */
    @JsonInclude(Include.NON_NULL)
    public static class ContentAllVipData {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("checkAGNT_ID")
        private String checkAgntId;
        @JsonProperty("PROD_ID")
        private String prodId;
        @JsonProperty("TRVL_PROD_ID")
        private String trvlProdId;
        @JsonProperty("onlyVIP")
        private String onlyVip;
        @JsonProperty("newCASE_NO")
        private String newCaseNo;
        @JsonProperty("newCASE_NO_INS")
        private String newCaseNoIns;
        @JsonProperty("notWelcome")
        private String notWelcome;
        @JsonProperty("a190_bo")
        private com.tp.tpigateway.model.life.CBV10001VO.AllVipData.MainMember mainMember;
        @JsonProperty("a191_boList")
        private List<com.tp.tpigateway.model.life.CBV10001VO.AllVipData.SubMember> subMemberList;

        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getCheckAgntId() {
            return checkAgntId;
        }
        public void setCheckAgntId(String checkAgntId) {
            this.checkAgntId = checkAgntId;
        }
        public String getProdId() {
            return prodId;
        }
        public void setProdId(String prodId) {
            this.prodId = prodId;
        }
        public String getTrvlProdId() {
            return trvlProdId;
        }
        public void setTrvlProdId(String trvlProdId) {
            this.trvlProdId = trvlProdId;
        }
        public String getOnlyVip() {
            return onlyVip;
        }
        public void setOnlyVip(String onlyVip) {
            this.onlyVip = onlyVip;
        }
        public String getNewCaseNo() {
            return newCaseNo;
        }
        public void setNewCaseNo(String newCaseNo) {
            this.newCaseNo = newCaseNo;
        }
        public String getNewCaseNoIns() {
            return newCaseNoIns;
        }
        public void setNewCaseNoIns(String newCaseNoIns) {
            this.newCaseNoIns = newCaseNoIns;
        }
        public String getNotWelcome() {
            return notWelcome;
        }
        public void setNotWelcome(String notWelcome) {
            this.notWelcome = notWelcome;
        }
        public com.tp.tpigateway.model.life.CBV10001VO.AllVipData.MainMember getMainMember() {
            return mainMember;
        }
        public void setMainMember(com.tp.tpigateway.model.life.CBV10001VO.AllVipData.MainMember mainMember) {
            this.mainMember = mainMember;
        }
        public List<com.tp.tpigateway.model.life.CBV10001VO.AllVipData.SubMember> getSubMemberList() {
            return subMemberList;
        }
        public void setSubMemberList(List<com.tp.tpigateway.model.life.CBV10001VO.AllVipData.SubMember> subMemberList) {
            this.subMemberList = subMemberList;
        }
    }

    /**
     * == 契約成立 ==
     * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
     *
     * "content": {
     *     "RTN_CODE":"000",
     *     "APLY_NO":"0010220330"
     * }
     */
    public static class ContentConfirmVoice {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("APLY_NO")
        private String aplyNo;

        public ContentConfirmVoice() {
        }
        public ContentConfirmVoice(String returnCode, ConfirmVoiceData data) {
            this.returnCode = returnCode;
            this.aplyNo = data.getAplyNo();
        }
        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getAplyNo() {
            return aplyNo;
        }
        public void setAplyNo(String aplyNo) {
            this.aplyNo = aplyNo;
        }
    }

    /**
     * == 暫存 ==
     * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
     *
     * "content": {
     *     "RTN_CODE": "111",
     *     "APLY_NO": "",
     *     "ISSUE_DATE" : "2020-03-20",
     *     "BGN_TIME" : "08:00:00",
     *     "ISSUE_DAYS" : "3",
     *     "RGN_CODE" : "3",
     *     "INSD_LIST" : [
     *         {
     *             "INSD_ID": "A123456789",
     *             "INSD_BRDY": "1990-07-29",
     *             "HD_AMT": "1000",
     *             "HP_AMT": "100",
     *             "HK_TYPE": "1",
     *             "HK_AMT": "10",
     *             "OMTP_AMT": "2",
     *             "TRVL_NOT_CONV": "C"
     *         }
     *     ]
     * }
     */
    public static class ContentTempVoice {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("APLY_NO")
        private String aplyNo;
        @JsonProperty("ISSUE_DATE")
        private String issueDate;
        @JsonProperty("BGN_TIME")
        private String bgnTime;
        @JsonProperty("ISSUE_DAYS")
        private String issueDays;
        @JsonProperty("RGN_CODE")
        private String rgnCode;
        @JsonProperty("INSD_LIST")
        private List<com.tp.tpigateway.model.life.CBV10001VO.TempVoiceData.InsdList> insdList;

        public ContentTempVoice() {
        }
        public ContentTempVoice(String rtnCode, TempVoiceData data) {
            this.returnCode = rtnCode;
            this.aplyNo = data.getAplyNo();
            this.issueDate = data.getIssueDate();
            this.bgnTime = data.getBgnTime();
            this.issueDays = data.getIssueDays();
            this.rgnCode = data.getRgnCode();
            this.insdList = data.getInsdList();
        }

        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getAplyNo() {
            return aplyNo;
        }
        public void setAplyNo(String aplyNo) {
            this.aplyNo = aplyNo;
        }
        public String getIssueDate() {
            return issueDate;
        }
        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }
        public String getBgnTime() {
            return bgnTime;
        }
        public void setBgnTime(String bgnTime) {
            this.bgnTime = bgnTime;
        }
        public String getIssueDays() {
            return issueDays;
        }
        public void setIssueDays(String issueDays) {
            this.issueDays = issueDays;
        }
        public String getRgnCode() {
            return rgnCode;
        }
        public void setRgnCode(String rgnCode) {
            this.rgnCode = rgnCode;
        }
        public List<com.tp.tpigateway.model.life.CBV10001VO.TempVoiceData.InsdList> getInsdList() {
            return insdList;
        }
        public void setInsdList(List<com.tp.tpigateway.model.life.CBV10001VO.TempVoiceData.InsdList> insdList) {
            this.insdList = insdList;
        }
    }

    /**
     * == 核保試算 ==
     * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
     *
     * "content": {
     *     "RTN_CODE": "000",
     *     "RGN_CODE": "3",
     *     "ISSUE_DATE" : "2020-02-20",
     *     "BGN_TIME" : "08:00:00"，
     *     "ISSUE_DAYS": "3",
     *     "APLY_NO": "0010700001",
     *     "TOT_PREM": "2000",
     *     "INSD_LIST": [
     *         {
     *             "INSD_ID": "A123456789",
     *             "INSD_BRDY": "1990-07-29",
     *             "HD_AMT": "1000",
     *             "HP_AMT": "100",
     *             "HK_TYPE": "1",
     *             "HK_AMT": "10",
     *             "OMTP_AMT": "2",
     *             "TRVL_NOT_CONV": "C",
     *             "SELF_PREM": "1000"
     *         }
     *     ]
     * }
     */
    public static class ContentCheckAmtVoice {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("RGN_CODE")
        private String rgnCode;
        @JsonProperty("ISSUE_DATE")
        private String issueDate;
        @JsonProperty("BGN_TIME")
        private String bgnTime;
        @JsonProperty("ISSUE_DAYS")
        private String issueDays;
        @JsonProperty("APLY_NO")
        private String aplyNo;
        @JsonProperty("TOT_PREM")
        private String totPrem;
        @JsonProperty("INSD_LIST")
        private List<com.tp.tpigateway.model.life.CBV10001VO.CheckAmtVoiceData.InsdList> insdList;

        public ContentCheckAmtVoice() {
        }
        public ContentCheckAmtVoice(String rtnCode, CheckAmtVoiceData data) {
            this.returnCode = rtnCode;
            this.aplyNo = data.getAplyNo();
            this.issueDate = data.getIssueDate();
            this.bgnTime = data.getBgnTime();
            this.issueDays = data.getIssueDays();
            this.rgnCode = data.getRgnCode();
            this.insdList = data.getInsdList();
            this.totPrem = data.getTotPrem();
        }

        public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getAplyNo() {
            return aplyNo;
        }
        public void setAplyNo(String aplyNo) {
            this.aplyNo = aplyNo;
        }
        public String getIssueDate() {
            return issueDate;
        }
        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }
        public String getBgnTime() {
            return bgnTime;
        }
        public void setBgnTime(String bgnTime) {
            this.bgnTime = bgnTime;
        }
        public String getIssueDays() {
            return issueDays;
        }
        public void setIssueDays(String issueDays) {
            this.issueDays = issueDays;
        }
        public String getRgnCode() {
            return rgnCode;
        }
        public void setRgnCode(String rgnCode) {
            this.rgnCode = rgnCode;
        }
        public List<com.tp.tpigateway.model.life.CBV10001VO.CheckAmtVoiceData.InsdList> getInsdList() {
            return insdList;
        }
        public void setInsdList(List<com.tp.tpigateway.model.life.CBV10001VO.CheckAmtVoiceData.InsdList> insdList) {
            this.insdList = insdList;
        }
    }

    /**
     * == 核保試算 ==
     * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
     *
     * content: {
     *     "RTN_CODE": "000",
     *     "RGN_PLACE" : "長灘島",
     *     "RGN_NAME": "菲律賓"
     * }
     *
     * content: {
     *     "RTN_CODE": "999",
     *     "RGN_PLACE" : "",
     *     "RGN_NAME": ""
     * }
     */
    public static class ContentGetRgnCode {
        @JsonProperty("RTN_CODE")
        private String returnCode;
        @JsonProperty("RGN_PLACE")
        private String rgnPlace;
        @JsonProperty("RGN_NAME")
        private String rgnName;
        @JsonProperty("RGN_CODE")
        private String rgnCode;

        public String getRgnCode() {
			return rgnCode;
		}
		public void setRgnCode(String rgnCode) {
			this.rgnCode = rgnCode;
		}
		public String getReturnCode() {
            return returnCode;
        }
        public void setReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }
        public String getRgnPlace() {
            return rgnPlace;
        }
        public void setRgnPlace(String rgnPlace) {
            this.rgnPlace = rgnPlace;
        }
        public String getRgnName() {
            return rgnName;
        }
        public void setRgnName(String rgnName) {
            this.rgnName = rgnName;
        }
    }


    /***************************************************************************
     * ChatFlow request VO
     **************************************************************************/
    /**
     * Flow to TPIGateway
     *
     * /ecall/law-age
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "issueDate": "2020-02-20",
     *       "vipBrdy": ["1970-01-01", "1980-08-08"]
     *   }
     *
     * /ecall/last-issue-data
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A29091346D"
     *   }
     *
     * ...
     *
     * /ecall/get-rgn-code
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "origStr": "泰國",
     *       "places": [
     *           {
     *               "match_user_phrase": false,
     *               "start": 0,
     *               "entity_id": 3685,
     *               "info": "東南亞-泰國;Thailand",
     *               "end": 2,
     *               "tokenized_text": "泰 國",
     *               "text": "泰國",
     *               "is_builtin": true,
     *               "key_user_phrase": null,
     *               "entity": "sys.世界國家城市"
     *           }
     *       ]
     *   }
     *
     * @author Wayne Tsai
     * @author Tony Liu
     */
    public static class FlowRequest {
        private String channel;
        private String source;
        private String role;
        private String fbIntent;
        private String sbIntent;
        private String sessionId;
        private String connectionId;
        
        private String issueDate;
        private List<String> vipBrdy;
        private String vipId;
        private String aplyNo;
        private String bgnTime;
        private String issueDays;
        private String rgnCode;
        private String origStr;
        private List<InsdList> insdList;
        private List<Places> places;

        public String getChannel() {
            return channel;
        }
        public void setChannel(String channel) {
            this.channel = channel;
        }
        public String getSource() {
            return source;
        }
        public void setSource(String source) {
            this.source = source;
        }
        public String getRole() {
            return role;
        }
        public void setRole(String role) {
            this.role = role;
        }
        public String getFbIntent() {
            return fbIntent;
        }
        public void setFbIntent(String fbIntent) {
            this.fbIntent = fbIntent;
        }
        public String getSbIntent() {
            return sbIntent;
        }
        public void setSbIntent(String sbIntent) {
            this.sbIntent = sbIntent;
        }
        public String getSessionId() {
			return sessionId;
		}
		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}
		public String getConnectionId() {
			return connectionId;
		}
		public void setConnectionId(String connectionId) {
			this.connectionId = connectionId;
		}
		public String getIssueDate() {
            return issueDate;
        }
        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }
        public List<String> getVipBrdy() {
            return vipBrdy;
        }
        public void setVipBrdy(List<String> vipBrdy) {
            this.vipBrdy = vipBrdy;
        }
        public String getVipId() {
            return vipId;
        }
        public void setVipId(String vipId) {
            this.vipId = vipId;
        }
        public String getAplyNo() {
            return aplyNo;
        }
        public void setAplyNo(String aplyNo) {
            this.aplyNo = aplyNo;
        }
        public String getBgnTime() {
            return bgnTime;
        }
        public void setBgnTime(String bgnTime) {
            this.bgnTime = bgnTime;
        }
        public String getIssueDays() {
            return issueDays;
        }
        public void setIssueDays(String issueDays) {
            this.issueDays = issueDays;
        }
        public String getRgnCode() {
            return rgnCode;
        }
        public void setRgnCode(String rgnCode) {
            this.rgnCode = rgnCode;
        }
        public String getOrigStr() {
            return origStr;
        }
        public void setOrigStr(String origStr) {
            this.origStr = origStr;
        }
        public List<InsdList> getInsdList() {
            return insdList;
        }
        public void setInsdList(List<InsdList> insdList) {
            this.insdList = insdList;
        }
        public List<Places> getPlaces() {
            return places;
        }
        public void setPlaces(List<Places> places) {
            this.places = places;
        }

        public static class Places {
            @JsonProperty(value = "match_user_phrase")
            private String matchUserPhrase;
            private String start;
            @JsonProperty(value = "entity_id")
            private String entityId;
            private String info;
            private String end;
            @JsonProperty(value = "tokenized_text")
            private String tokenizedText;
            private String text;
            @JsonProperty(value = "is_builtin")
            private String isBuiltin;
            @JsonProperty(value = "key_user_phrase")
            private String keyUserPhrase;
            private String entity;

            public String getMatchUserPhrase() {
                return matchUserPhrase;
            }
            public void setMatchUserPhrase(String matchUserPhrase) {
                this.matchUserPhrase = matchUserPhrase;
            }
            public String getStart() {
                return start;
            }
            public void setStart(String start) {
                this.start = start;
            }
            public String getEntityId() {
                return entityId;
            }
            public void setEntityId(String entityId) {
                this.entityId = entityId;
            }
            public String getInfo() {
                return info;
            }
            public void setInfo(String info) {
                this.info = info;
            }
            public String getEnd() {
                return end;
            }
            public void setEnd(String end) {
                this.end = end;
            }
            public String getTokenizedText() {
                return tokenizedText;
            }
            public void setTokenizedText(String tokenizedText) {
                this.tokenizedText = tokenizedText;
            }
            public String getText() {
                return text;
            }
            public void setText(String text) {
                this.text = text;
            }
            public String getIsBuiltin() {
                return isBuiltin;
            }
            public void setIsBuiltin(String isBuiltin) {
                this.isBuiltin = isBuiltin;
            }
            public String getKeyUserPhrase() {
                return keyUserPhrase;
            }
            public void setKeyUserPhrase(String keyUserPhrase) {
                this.keyUserPhrase = keyUserPhrase;
            }
            public String getEntity() {
                return entity;
            }
            public void setEntity(String entity) {
                this.entity = entity;
            }
        }

        public static class InsdList {
            private String insdId;
            private String insdBrdy;
            private String hdAmt;
            private String hpAmt;
            private String hkType;
            private String hkAmt;
            private String omtpAmt;
            private String trvlNotConv;

            public String getInsdId() {
                return insdId;
            }
            public void setInsdId(String insdId) {
                this.insdId = insdId;
            }
            public String getInsdBrdy() {
                return insdBrdy;
            }
            public void setInsdBrdy(String insdBrdy) {
                this.insdBrdy = insdBrdy;
            }
            public String getHdAmt() {
                return hdAmt;
            }
            public void setHdAmt(String hdAmt) {
                this.hdAmt = hdAmt;
            }
            public String getHpAmt() {
                return hpAmt;
            }
            public void setHpAmt(String hpAmt) {
                this.hpAmt = hpAmt;
            }
            public String getHkType() {
                return hkType;
            }
            public void setHkType(String hkType) {
                this.hkType = hkType;
            }
            public String getHkAmt() {
                return hkAmt;
            }
            public void setHkAmt(String hkAmt) {
                this.hkAmt = hkAmt;
            }
            public String getOmtpAmt() {
                return omtpAmt;
            }
            public void setOmtpAmt(String omtpAmt) {
                this.omtpAmt = omtpAmt;
            }
            public String getTrvlNotConv() {
                return trvlNotConv;
            }
            public void setTrvlNotConv(String trvlNotConv) {
                this.trvlNotConv = trvlNotConv;
            }
        }
    }

    /*
     * 在核保試算後，若成功呼叫，將取得的APLY_NO 與 connection_id 及 sesseion_id 做對應
     * 存進表格 id_mapping-easycall 中
     */
    @Entity
    @Table(name = "id_mapping_easycall", schema = "public")
    public static class IdMappingEasycall implements Serializable {

		private static final long serialVersionUID = 1L;
    	
	    @Id
	    @Column(name = "session_id", length = 50, nullable = false, unique = true)
		private String sessionId;
		
		@Column(name = "connection_id", length = 50, nullable = false)
		private String connectionId;
		
		@Column(name = "cust_id", length = 12, nullable = false)
		private String custId;
		
		@Column(name = "aply_no", length = 20)
		private String aplyNo;

		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public String getConnectionId() {
			return connectionId;
		}

		public void setConnectionId(String connectionId) {
			this.connectionId = connectionId;
		}

		public String getCustId() {
			return custId;
		}

		public void setCustId(String custId) {
			this.custId = custId;
		}

		public String getAplyNo() {
			return aplyNo;
		}

		public void setAplyNo(String aplyNo) {
			this.aplyNo = aplyNo;
		}
		
		@Override
	    public String toString() {
	        return new ToStringBuilder(this)
	                .append("sessionId", sessionId)
	                .append("connectionId", connectionId)
	                .append("custId", custId)
	                .append("aplyNo", aplyNo)
	                .toString();
	    }
    }
    
    @Entity
    @Table(name = "flow_call_log_3D", schema = "public")
    public static class FlowCallLog3D implements Serializable {

		private static final long serialVersionUID = 1L;
		
		@Id
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idGen")
		@SequenceGenerator(name = "idGen", sequenceName = "flow_call_log_3D_id_seq", initialValue = 1, allocationSize = 1)
		@Column(name = "log_id")
		private Long logId;

		@Column(name = "session_id", length = 50, nullable = false)
		private String sessionId;
		
		@Column(name = "cust_id", length = 12)
		private String custId;
		
		@Column(name = "ip", length = 50)
		private String ip;
		
		@Column(name = "input_type", length = 1, nullable = false)
		private String inputType;
		
		@Column(name = "channel", length = 10)
		private String channel;
		
		@Column(name = "connection_id", length = 50, nullable = false)
		private String connectionId;
		
		@Column(name = "msg", columnDefinition = "TEXT")
		private String msg;
		
		@Column(name = "msg_id", length = 50)
		private String msgId;
		
		@Column(name = "msg_type", length = 50)
		private String msgType;
		
		@Column(name = "msg_time")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		private Date msgTime;
		
		@Column(name = "assign_intent", length = 100)
		private String assignIntent;
		
		@Column(name = "assign_second_bot_intent", length = 100)
		private String assignSecondBotIntent;
		
		@Column(name = "assign_parameter", length = 2000)
		private String assignParameter;
		
		@Column(name = "first_nlu_intent", length = 100)
		private String firstNluIntent;
		
		@Column(name = "first_nlu_score", length = 30)
		private String firstNluScore;
		
		@Column(name = "first_nlu_support_intent", length = 100)
		private String firstNluSupportIntent;
		
		@Column(name = "first_nlu_support_score", length = 100)
		private String firstNluSupportScore;
		
		@Column(name = "first_nlu_cost_time")
		private Integer firstNluCostTime;
		
		@Column(name = "faq_answer", length = 100)
		private String faqAnswer;
		
		@Column(name = "faq_score", length = 100)
		private String faqScore;
		
		@Column(name = "faq_cost_time")
		private Integer faqCostTime;
		
		@Column(name = "second_nlu_entities", length = 500)
		private String secondNluEntities;             
		
		@Column(name = "second_nlu_add1_entities", length = 500)
		private String secondNluAdd1Entities;    
		
		@Column(name = "second_nlu_add2_entities", length = 500)
		private String secondNluAdd2Entities;
		
		@Column(name = "second_nlu_intent", length = 100)
		private String secondNluIntent;
		
		@Column(name = "second_nlu_score", length = 100)
		private String secondNluScore;      
		
		@Column(name = "second_nlu_cost_time")
		private Integer secondNluCostTime;     
		
		@Column(name = "bot_cost_time")
		private Integer botCostTime;  
		
		/*
		 * @DateTimeFormat: 前端傳給後端用，預設 iso = ISO.NONE，也就是傳毫秒過來
		 * @JsonFormat: 前後端互相轉換
		 */
		@Column(name = "flow_time", nullable = false)
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		private Date flowTime;		
		
		@Column(name = "flow_cost_time", nullable = false)
		private Integer flowCostTime;               
		
		@Column(name = "flow_host_name", length = 50)
		private String flowHostName;       
		
		@Column(name = "flow_first_app_id", length = 100)
		private String flowFirstAppId;     
		
		@Column(name = "memo", length = 2000)
		private String memo;    
		
		@Column(name = "state", length = 10, nullable = false)
		private String state;
		
		@Transient
		private FlowCallLogTimeLog timeLog;
		
		public Long getLogId() {
			return logId;
		}
		public void setLogId(Long logId) {
			this.logId = logId;
		}
		public String getSessionId() {
			return sessionId;
		}
		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}
		public String getCustId() {
			return custId;
		}
		public void setCustId(String custId) {
			this.custId = custId;
		}
		public String getIp() {
			return ip;
		}
		public void setIp(String ip) {
			this.ip = ip;
		}
		public String getInputType() {
			return inputType;
		}
		public void setInputType(String inputType) {
			this.inputType = inputType;
		}
		public String getChannel() {
			return channel;
		}
		public void setChannel(String channel) {
			this.channel = channel;
		}
		public String getConnectionId() {
			return connectionId;
		}
		public void setConnectionId(String connectionId) {
			this.connectionId = connectionId;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public String getMsgId() {
			return msgId;
		}
		public void setMsgId(String msgId) {
			this.msgId = msgId;
		}
		public String getMsgType() {
			return msgType;
		}
		public void setMsgType(String msgType) {
			this.msgType = msgType;
		}
		public Date getMsgTime() {
			return msgTime;
		}
		public void setMsgTime(Date msgTime) {
			this.msgTime = msgTime;
		}
		public String getAssignIntent() {
			return assignIntent;
		}
		public void setAssignIntent(String assignIntent) {
			this.assignIntent = assignIntent;
		}
		public String getAssignSecondBotIntent() {
			return assignSecondBotIntent;
		}
		public void setAssignSecondBotIntent(String assignSecondBotIntent) {
			this.assignSecondBotIntent = assignSecondBotIntent;
		}
		public String getAssignParameter() {
			return assignParameter;
		}
		public void setAssignParameter(String assignParameter) {
			this.assignParameter = assignParameter;
		}
		public String getFirstNluIntent() {
			return firstNluIntent;
		}
		public void setFirstNluIntent(String firstNluIntent) {
			this.firstNluIntent = firstNluIntent;
		}
		public String getFirstNluScore() {
			return firstNluScore;
		}
		public void setFirstNluScore(String firstNluScore) {
			this.firstNluScore = firstNluScore;
		}
		public String getFirstNluSupportIntent() {
			return firstNluSupportIntent;
		}
		public void setFirstNluSupportIntent(String firstNluSupportIntent) {
			this.firstNluSupportIntent = firstNluSupportIntent;
		}
		public String getFirstNluSupportScore() {
			return firstNluSupportScore;
		}
		public void setFirstNluSupportScore(String firstNluSupportScore) {
			this.firstNluSupportScore = firstNluSupportScore;
		}
		public Integer getFirstNluCostTime() {
			return firstNluCostTime;
		}
		public void setFirstNluCostTime(Integer firstNluCostTime) {
			this.firstNluCostTime = firstNluCostTime;
		}
		public String getFaqAnswer() {
			return faqAnswer;
		}
		public void setFaqAnswer(String faqAnswer) {
			this.faqAnswer = faqAnswer;
		}
		public String getFaqScore() {
			return faqScore;
		}
		public void setFaqScore(String faqScore) {
			this.faqScore = faqScore;
		}
		public Integer getFaqCostTime() {
			return faqCostTime;
		}
		public void setFaqCostTime(Integer faqCostTime) {
			this.faqCostTime = faqCostTime;
		}
		public String getSecondNluEntities() {
			return secondNluEntities;
		}
		public void setSecondNluEntities(String secondNluEntities) {
			this.secondNluEntities = secondNluEntities;
		}
		public String getSecondNluAdd1Entities() {
			return secondNluAdd1Entities;
		}
		public void setSecondNluAdd1Entities(String secondNluAdd1Entities) {
			this.secondNluAdd1Entities = secondNluAdd1Entities;
		}
		public String getSecondNluAdd2Entities() {
			return secondNluAdd2Entities;
		}
		public void setSecondNluAdd2Entities(String secondNluAdd2Entities) {
			this.secondNluAdd2Entities = secondNluAdd2Entities;
		}
		public String getSecondNluIntent() {
			return secondNluIntent;
		}
		public void setSecondNluIntent(String secondNluIntent) {
			this.secondNluIntent = secondNluIntent;
		}
		public String getSecondNluScore() {
			return secondNluScore;
		}
		public void setSecondNluScore(String secondNluScore) {
			this.secondNluScore = secondNluScore;
		}
		public Integer getSecondNluCostTime() {
			return secondNluCostTime;
		}
		public void setSecondNluCostTime(Integer secondNluCostTime) {
			this.secondNluCostTime = secondNluCostTime;
		}
		public Integer getBotCostTime() {
			return botCostTime;
		}
		public void setBotCostTime(Integer botCostTime) {
			this.botCostTime = botCostTime;
		}
		public Date getFlowTime() {
			return flowTime;
		}
		public void setFlowTime(Date flowTime) {
			this.flowTime = flowTime;
		}
		public Integer getFlowCostTime() {
			return flowCostTime;
		}
		public void setFlowCostTime(Integer flowCostTime) {
			this.flowCostTime = flowCostTime;
		}
		public String getFlowHostName() {
			return flowHostName;
		}
		public void setFlowHostName(String flowHostName) {
			this.flowHostName = flowHostName;
		}
		public String getFlowFirstAppId() {
			return flowFirstAppId;
		}
		public void setFlowFirstAppId(String flowFirstAppId) {
			this.flowFirstAppId = flowFirstAppId;
		}
		public String getMemo() {
			return memo;
		}
		public void setMemo(String memo) {
			this.memo = memo;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public FlowCallLogTimeLog getTimeLog() {
			return timeLog;
		}
		public void setTimeLog(FlowCallLogTimeLog timeLog) {
			this.timeLog = timeLog;
		}                 
    }
    
    
    @Entity
    @Table(name = "bot_tpi_call_log_3D", schema = "public")
    public static class BotTpiCallLog3D implements Serializable {

		private static final long serialVersionUID = 1L;
    	
		@Id
//		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idGen")
		@SequenceGenerator(name = "idGen", sequenceName = "bot_tpi_call_log_3D_id_seq", initialValue = 1, allocationSize = 1)
		@Column(name = "id")
		private Long id;
		
		@Column(name = "session_id", length = 50, nullable = false)
		private String sessionId;
		
		@Column(name = "params", columnDefinition = "TEXT")
		private String params;
		
		@Column(name = "method", length = 200)
		private String method;
		
		@Column(name = "result", columnDefinition = "TEXT")
		private String result;
		
		@Column(name = "connection_id", length = 50)
		private String connectionId;
		
		@Column(name = "remark", length = 200)
		private String remark;
		
		@Column(name = "api_cost_time")
		private Integer apiCostTime;
		
		@Column(name = "tpigateway_cost_time")
		private Integer tpigatewayCostTime;
		
		@Column(name = "tpigateway_start_time")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		private Date tpigatewayStartTime;
		
		@Column(name = "tpigateway_end_time")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		private Date tpigatewayEndTime;
		
		@Column(name = "api_start_time")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		private Date apiStartTime;
		
		@Column(name = "api_end_time")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		private Date apiEndTime;
		
		@Column(name = "api_url", length = 200)
		private String apiUrl;
		
		@Column(name = "input", columnDefinition = "TEXT")
		private String input;
		
		@Column(name = "output", columnDefinition = "TEXT")
		private String output;
		
		@Column(name = "resp_status", length = 20)
		private String respStatus;

		@Column(name = "Tpi_host_name", length = 200)
		private String tpiHostName;
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public String getParams() {
			return params;
		}

		public void setParams(String params) {
			this.params = params;
		}

		public String getMethod() {
			return method;
		}

		public void setMethod(String method) {
			this.method = method;
		}

		public String getResult() {
			return result;
		}

		public void setResult(String result) {
			this.result = result;
		}

		public String getConnectionId() {
			return connectionId;
		}

		public void setConnectionId(String connectionId) {
			this.connectionId = connectionId;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}

		public Integer getApiCostTime() {
			return apiCostTime;
		}

		public void setApiCostTime(Integer apiCostTime) {
			this.apiCostTime = apiCostTime;
		}

		public Integer getTpigatewayCostTime() {
			return tpigatewayCostTime;
		}

		public void setTpigatewayCostTime(Integer tpigatewayCostTime) {
			this.tpigatewayCostTime = tpigatewayCostTime;
		}

		public Date getTpigatewayStartTime() {
			return tpigatewayStartTime;
		}

		public void setTpigatewayStartTime(Date tpigatewayStartTime) {
			this.tpigatewayStartTime = tpigatewayStartTime;
		}

		public Date getTpigatewayEndTime() {
			return tpigatewayEndTime;
		}

		public void setTpigatewayEndTime(Date tpigatewayEndTime) {
			this.tpigatewayEndTime = tpigatewayEndTime;
		}

		public Date getApiStartTime() {
			return apiStartTime;
		}

		public void setApiStartTime(Date apiStartTime) {
			this.apiStartTime = apiStartTime;
		}

		public Date getApiEndTime() {
			return apiEndTime;
		}

		public void setApiEndTime(Date apiEndTime) {
			this.apiEndTime = apiEndTime;
		}

		public String getApiUrl() {
			return apiUrl;
		}

		public void setApiUrl(String apiUrl) {
			this.apiUrl = apiUrl;
		}

		public String getInput() {
			return input;
		}

		public void setInput(String input) {
			this.input = input;
		}

		public String getOutput() {
			return output;
		}

		public void setOutput(String output) {
			this.output = output;
		}

		public String getRespStatus() {
			return respStatus;
		}

		public void setRespStatus(String respStatus) {
			this.respStatus = respStatus;
		}

		public String getTpiHostName() {
			return tpiHostName;
		}

		public void setTpiHostName(String tpiHostName) {
			this.tpiHostName = tpiHostName;
		}
    }
    
    public static class FlowCallLogTimeLog {
    	private String text;
    	private String sessionId;
    	private int totalCostTime;
    	private FirstCostTime firstCostTime;
    	private SecondCostTime secondCostTime;
    	
    	public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public int getTotalCostTime() {
			return totalCostTime;
		}

		public void setTotalCostTime(int totalCostTime) {
			this.totalCostTime = totalCostTime;
		}

		public FirstCostTime getFirstCostTime() {
			return firstCostTime;
		}

		public void setFirstCostTime(FirstCostTime firstCostTime) {
			this.firstCostTime = firstCostTime;
		}

		public SecondCostTime getSecondCostTime() {
			return secondCostTime;
		}

		public void setSecondCostTime(SecondCostTime secondCostTime) {
			this.secondCostTime = secondCostTime;
		}

		public static class FirstCostTime {
    		private int firstNluCostTime;
    		private int firstNLUBehaviorCostTime;
    		private int faqCostTime;
			public int getFirstNluCostTime() {
				return firstNluCostTime;
			}
			public void setFirstNluCostTime(int firstNluCostTime) {
				this.firstNluCostTime = firstNluCostTime;
			}
			public int getFirstNLUBehaviorCostTime() {
				return firstNLUBehaviorCostTime;
			}
			public void setFirstNLUBehaviorCostTime(int firstNLUBehaviorCostTime) {
				this.firstNLUBehaviorCostTime = firstNLUBehaviorCostTime;
			}
			public int getFaqCostTime() {
				return faqCostTime;
			}
			public void setFaqCostTime(int faqCostTime) {
				this.faqCostTime = faqCostTime;
			}
    	}
    	
    	public static class SecondCostTime {
    		private int tpiLastIssueData;
    		private int tpiLawAge;
    		private int tpiDoConfirmVoice;
    		private int tpiRgnCode;
    		private int tpiDoCheckamtVoice;
    		private int tpiVipData;
    		private int nluCurrent;
    		private int nluInsured;
    		private int nluYesNo;
			public int getNluCurrent() {
				return nluCurrent;
			}
			public void setNluCurrent(int nluCurrent) {
				this.nluCurrent = nluCurrent;
			}
			public int getNluInsured() {
				return nluInsured;
			}
			public void setNluInsured(int nluInsured) {
				this.nluInsured = nluInsured;
			}
			public int getTpiRgnCode() {
				return tpiRgnCode;
			}
			public void setTpiRgnCode(int tpiRgnCode) {
				this.tpiRgnCode = tpiRgnCode;
			}
			public int getNluYesNo() {
				return nluYesNo;
			}
			public void setNluYesNo(int nluYesNo) {
				this.nluYesNo = nluYesNo;
			}
			public int getTpiLastIssueData() {
				return tpiLastIssueData;
			}
			public void setTpiLastIssueData(int tpiLastIssueData) {
				this.tpiLastIssueData = tpiLastIssueData;
			}
			public int getTpiLawAge() {
				return tpiLawAge;
			}
			public void setTpiLawAge(int tpiLawAge) {
				this.tpiLawAge = tpiLawAge;
			}
			public int getTpiDoConfirmVoice() {
				return tpiDoConfirmVoice;
			}
			public void setTpiDoConfirmVoice(int tpiDoConfirmVoice) {
				this.tpiDoConfirmVoice = tpiDoConfirmVoice;
			}
			public int getTpiDoCheckamtVoice() {
				return tpiDoCheckamtVoice;
			}
			public void setTpiDoCheckamtVoice(int tpiDoCheckamtVoice) {
				this.tpiDoCheckamtVoice = tpiDoCheckamtVoice;
			}
			public int getTpiVipData() {
				return tpiVipData;
			}
			public void setTpiVipData(int tpiVipData) {
				this.tpiVipData = tpiVipData;
			}
    	}
    }
    
    
}
