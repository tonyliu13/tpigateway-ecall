package com.tp.tpigateway.model.life;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class BotLifeRequestVO {

	private final static String FB = "FB";
	private final static String FACEBOOK = "FaceBook";
	private final static String LINE = "Line";
	private final static String CHATWEB = "ChatWeb";
	
	private String Channel;
	private String Role;
	private String NodeID;
	private String IDNo;
	//由前端決定是否傳入值
	private String ScenarioCode;
	private String PolicyNo;
	//TODO:查詢來源是否有固定值可由TPI帶過去
	private String QryFrm = "FROMB2E1";
	private String rcptSerNo; //單據序號
	private String payTimes; //繳次3
	
	private String longitude; //經度
	private String latitude; //緯度
	private String addr; //地址
	
	private String ChangeCardNoNote; //I2_12  扣款信用卡卡號是不是有變 -> 是:Y 否:N
	
	private String loginNote; // Y:已驗身   N:未驗身
	private String loginType; // (default)type1:登入+保單驗證(無需指定loginType), 
	                          // type2:登入(不接續完成保單驗證), 
	                          // turnToReal:登入+保單驗證(轉真人前預先登入), 
	                          // loginToAgent:登入(轉真人後真人請求客戶登入，不接續完成保單驗證)
	                          // requestRights:登入+保單驗證(請求權益懶人包登入，視為已點擊六宮格)
	
	private String jsonParameter; //額外的json參數
	
	private String amt;
	
	private String loanId; //貸款帳號
	
	/*
		人壽 BotController.i0 增參數非必要參數 showType=1 時只回覆顯示牌卡
		銀行 CathayBankController.getMessage 當 NodeID=B0 時增參數非必要參數 showType=1(這支的參數要加在 paramString 中) 時只回覆顯示牌卡
	*/
	private String showType;
	
	private String selectedPolicy;
	
	public String getChannel() {
		return Channel;
	}
	public void setChannel(String channel) {
		Channel = channel;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}
	public String getNodeID() {
		return NodeID;
	}
	public void setNodeID(String nodeID) {
		NodeID = nodeID;
	}
	public String getIDNo() {
		return IDNo;
	}
	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
	public String getScenarioCode() {
		return ScenarioCode;
	}
	public void setScenarioCode(String scenarioCode) {
		ScenarioCode = scenarioCode;
	}
	public String getPolicyNo() {
		return PolicyNo;
	}
	public void setPolicyNo(String policyNo) {
		PolicyNo = policyNo;
	}
	public String getQryFrm() {
		return QryFrm;
	}
	public void setQryFrm(String qryFrm) {
		QryFrm = qryFrm;
	}
	public String getRcptSerNo() {
		return rcptSerNo;
	}
	public void setRcptSerNo(String rcptSerNo) {
		this.rcptSerNo = rcptSerNo;
	}
	public String getPayTimes() {
		return payTimes;
	}
	public void setPayTimes(String payTimes) {
		this.payTimes = payTimes;
	}
	public String getChangeCardNoNote() {
		return ChangeCardNoNote;
	}
	public void setChangeCardNoNote(String changeCardNoNote) {
		ChangeCardNoNote = changeCardNoNote;
	}
	public String getLoginNote() {
		return loginNote;
	}
	public void setLoginNote(String loginNote) {
		this.loginNote = loginNote;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	public boolean isLogin() {
		return "Y".equals(loginNote);
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getShowType() {
		return showType;
	}
	public void setShowType(String showType) {
		this.showType = showType;
	}
	public String getJsonParameter() {
		return jsonParameter;
	}
	public void setJsonParameter(String jsonParameter) {
		this.jsonParameter = jsonParameter;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	public String getSelectedPolicy() {
		return selectedPolicy;
	}
	public void setSelectedPolicy(String selectedPolicy) {
		this.selectedPolicy = selectedPolicy;
	}
	
	public static BotLifeRequestVO getParam(HttpServletRequest req) {
		BotLifeRequestVO vo = new BotLifeRequestVO();
		vo.setChannel(req.getParameter("Channel"));
		vo.setRole(req.getParameter("Role"));
		vo.setNodeID(req.getParameter("NodeID"));
		vo.setIDNo(req.getParameter("IDNo"));
		vo.setScenarioCode(req.getParameter("ScenarioCode"));
		vo.setPolicyNo(req.getParameter("PolicyNo"));
		//vo.setQryFrm(req.getParameter("QryFrm"));
		vo.setRcptSerNo(req.getParameter("RcptSerNo"));
		vo.setPayTimes(req.getParameter("PayTimes"));
		vo.setChangeCardNoNote(req.getParameter("ChangeCardNoNote"));
		vo.setLoginNote(req.getParameter("loginNote"));
		vo.setLoginType(req.getParameter("loginType"));
		vo.setLongitude(req.getParameter("longitude"));
		vo.setLatitude(req.getParameter("latitude"));
		vo.setAddr(req.getParameter("addr"));
		vo.setShowType(req.getParameter("showType"));
		vo.setJsonParameter(req.getParameter("jsonParameter"));
		vo.setAmt(req.getParameter("amt"));
		vo.setLoanId(req.getParameter("loanId"));
		vo.setSelectedPolicy(req.getParameter("selectedPolicy"));
		return vo;
	}
	
	public boolean isChatWeb() {
		return StringUtils.equalsIgnoreCase(CHATWEB, getChannel()) || !(isFaceBook() || isLine());
	}
	
	public boolean isFaceBook() {
		return StringUtils.equalsIgnoreCase(FB, getChannel()) || StringUtils.equalsIgnoreCase(FACEBOOK, getChannel());
	}
	
	public boolean isLine() {
		return StringUtils.equalsIgnoreCase(LINE, getChannel());
	}
}
