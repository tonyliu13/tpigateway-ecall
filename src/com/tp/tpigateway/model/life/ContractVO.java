package com.tp.tpigateway.model.life;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 保單資訊 VO
 */
public class ContractVO {

	private String proposerID;				// 要保人ID
	private String contractNumber;			// 保單號碼
	private String insuredName;				// 被保人姓名
	private String productName;				// 商品名稱
	private String payment;					// 繳別
	private String insuranceSolicitorName;	// 服務人員姓名
	private String insuranceSolicitorPhone;	// 服務人員聯絡電話
	
	public ContractVO() { }
	
	public ContractVO(String contractNumber, String insuredName, String productName, String payment, String insuranceSolicitorName, String setInsuranceSolicitorPhone) {
		setContractNumber(contractNumber);
		setInsuredName(insuredName);
		setProductName(productName);
		setPayment(payment);
		setInsuranceSolicitorName(insuranceSolicitorName);
		setInsuranceSolicitorPhone(setInsuranceSolicitorPhone);
	}
	
	public String getProposerID() {
		return proposerID;
	}
	public void setProposerID(String proposerID) {
		this.proposerID = proposerID;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getInsuranceSolicitorName() {
		return insuranceSolicitorName;
	}
	public void setInsuranceSolicitorName(String insuranceSolicitorName) {
		this.insuranceSolicitorName = insuranceSolicitorName;
	}
	public String getInsuranceSolicitorPhone() {
		return insuranceSolicitorPhone;
	}
	public void setInsuranceSolicitorPhone(String insuranceSolicitorPhone) {
		this.insuranceSolicitorPhone = insuranceSolicitorPhone;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
