package com.tp.tpigateway.model.life;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bot_chat_log")
public class BotChatLog {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "session_id")
	private String sessionId;

	@Column(name = "chat_log", columnDefinition = "text")
	private String chatLog;

	@Column(name = "insert_time")
	private Date insertTime;
	
	public BotChatLog() {
		
	}
	
	public BotChatLog(String sessionId, String chatLog) {
		super();
		this.sessionId = sessionId;
		this.chatLog = chatLog;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getChatLog() {
		return chatLog;
	}

	public void setChatLog(String chatLog) {
		this.chatLog = chatLog;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

}
