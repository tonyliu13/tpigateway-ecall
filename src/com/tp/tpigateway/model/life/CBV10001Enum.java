//package com.tp.tpigateway.model.life;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * <p>國泰狀態碼與問問狀態碼的對應，(國泰, 問問)</p>
// * @author Wayne Tsai
// *
// */
//public class CBV10001Enum {
//
//    /**
//     * <p>取得法定年齡，國泰狀態碼與問問狀態碼的對應</p>
//     *
//     * 問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
//     * -----------------------------------------------------------------------
//     * 000       000          投保成功
//     * 131       097          傳入參數有誤
//     * 132       099          執行錯誤
//     */
//    public static enum LawAgeCodeMappingEnum {
//        SUCCESS("000", "000"),
//        PARAMETER_ERROR("097", "131"),
//        SYSTEM_ERROR("099", "132");
//
//        private static Map<String, LawAgeCodeMappingEnum> codeMap = new HashMap<>();
//        static {
//            codeMap.put("000", SUCCESS);
//            codeMap.put("097", PARAMETER_ERROR);
//            codeMap.put("099", SYSTEM_ERROR);
//        }
//
//        private String cathayCode;
//        private String askaskCode;
//
//        private LawAgeCodeMappingEnum(String cathayCode, String askaskCode) {
//            this.cathayCode = cathayCode;
//            this.askaskCode = askaskCode;
//        }
//
//        public String getCathayCode() {
//            return cathayCode;
//        }
//
//        public void setCathayCode(String cathayCode) {
//            this.cathayCode = cathayCode;
//        }
//
//        public String getAskaskCode() {
//            return askaskCode;
//        }
//
//        public void setAskaskCode(String askaskCode) {
//            this.askaskCode = askaskCode;
//        }
//
//        public static LawAgeCodeMappingEnum fromCathayCode(String cathayCode) {
//            return codeMap.get(cathayCode);
//        }
//    }
//
//    /**
//     * <p>取得最後投保日期，國泰狀態碼與問問狀態碼的對應</p>
//     *
//     * 問問代碼        國泰代碼        說明
//     * --------------------------------------------------
//     * 000         000         查詢成功
//     * 121         097         未通過輸入檢核
//     * 098         098         查無最近一次投保資料
//     * 122         099         系統有誤
//     *
//     * @author Wayne Tsai
//     */
//    public static enum LastIssueDataCodeMappingEnum {
//
//        SUCCESS("000", "000"),
//        PARAMETER_ERROR("097", "121"),
//        NO_DATA("098", "098"),
//        SYSTEM_ERROR("099", "122")
//        ;
//
//        private static Map<String, LastIssueDataCodeMappingEnum> codeMap;
//
//        static {
//            codeMap = new HashMap<>();
//
//            codeMap.put("000", SUCCESS);
//            codeMap.put("097", PARAMETER_ERROR);
//            codeMap.put("098", NO_DATA);
//            codeMap.put("099", SYSTEM_ERROR);
//        }
//
//        private String cathayCode;
//        private String askaskCode;
//
//        private LastIssueDataCodeMappingEnum(String cathayCode, String askaskCode) {
//            this.cathayCode = cathayCode;
//            this.askaskCode = askaskCode;
//        }
//
//        public String getCathayCode() {
//            return cathayCode;
//        }
//
//        public void setCathayCode(String cathayCode) {
//            this.cathayCode = cathayCode;
//        }
//
//        public String getAskaskCode() {
//            return askaskCode;
//        }
//
//        public void setAskaskCode(String askaskCode) {
//            this.askaskCode = askaskCode;
//        }
//
//        public static LastIssueDataCodeMappingEnum fromCathayCode(String cathayCode) {
//            return codeMap.get(cathayCode);
//        }
//    }
//
//    /**
//     * <p>整戶資料查詢，國泰狀態碼與問問狀態碼的對應</p>
//     *
//     * 問問代碼        國泰代碼        說明
//     * --------------------------------------------------
//     * 000         000         查詢成功
//     * 001         001         檢核經手人有誤
//     * 002         002         壽險/產險特殊名單
//     * 096         096         舊件會員
//     * 101         097         傳入參數有誤
//     * 098         098         查無會員資料
//     * 102         099         系統有誤
//     *
//     * @author Wayne Tsai
//     */
//    public static enum AllVipDataCodeMappingEnum {
//
//        SUCCESS("000", "000"),
//        CHECKER_ERROR("001", "001"),
//        SPECIAL_LIST("002", "002"),
//        OLD_MEMBER("096", "096"),
//        PARAMETER_ERROR("097", "101"),
//        NO_DATA("098", "098"),
//        SYSTEM_ERROR("099", "102")
//        ;
//
//        private static Map<String, AllVipDataCodeMappingEnum> codeMap;
//
//        static {
//            codeMap = new HashMap<>();
//
//            codeMap.put("000", SUCCESS);
//            codeMap.put("001", CHECKER_ERROR);
//            codeMap.put("002", SPECIAL_LIST);
//            codeMap.put("096", OLD_MEMBER);
//            codeMap.put("097", PARAMETER_ERROR);
//            codeMap.put("098", NO_DATA);
//            codeMap.put("099", SYSTEM_ERROR);
//        }
//
//        private String cathayCode;
//        private String askaskCode;
//
//        private AllVipDataCodeMappingEnum(String cathayCode, String askaskCode) {
//            this.cathayCode = cathayCode;
//            this.askaskCode = askaskCode;
//        }
//
//        public String getCathayCode() {
//            return cathayCode;
//        }
//
//        public void setCathayCode(String cathayCode) {
//            this.cathayCode = cathayCode;
//        }
//
//        public String getAskaskCode() {
//            return askaskCode;
//        }
//
//        public void setAskaskCode(String askaskCode) {
//            this.askaskCode = askaskCode;
//        }
//
//        public static AllVipDataCodeMappingEnum fromCathayCode(String cathayCode) {
//            return codeMap.get(cathayCode);
//        }
//
//    }
//
//    /**
//     * <p>契約成立，國泰狀態碼與問問狀態碼的對應</p>
//     *
//     * 問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
//     * -----------------------------------------------------------------------
//     * 000       000          投保成功
//     * 151       097          傳入參數有誤
//     * 152       098          查無核保試算資料
//     * 153       099          執行錯誤
//     */
//    public static enum ConfirmVoiceCodeMappingEnum {
//        SUCCESS("000", "000"),
//        PARAMETER_ERROR("097", "151"),
//        NO_DATA("098", "152"),
//        SYSTEM_ERROR("099", "153");
//
//        private static Map<String, ConfirmVoiceCodeMappingEnum> codeMap = new HashMap<>();
//        static {
//            codeMap.put(SUCCESS.getCathayCode(), SUCCESS);
//            codeMap.put(PARAMETER_ERROR.getCathayCode(), PARAMETER_ERROR);
//            codeMap.put(NO_DATA.getCathayCode(), NO_DATA);
//            codeMap.put(SYSTEM_ERROR.getCathayCode(), SYSTEM_ERROR);
//        }
//
//        private String cathayCode;
//        private String askaskCode;
//
//        private ConfirmVoiceCodeMappingEnum(String cathayCode, String askaskCode) {
//            this.cathayCode = cathayCode;
//            this.askaskCode = askaskCode;
//        }
//
//        public String getCathayCode() {
//            return cathayCode;
//        }
//        public void setCathayCode(String cathayCode) {
//            this.cathayCode = cathayCode;
//        }
//        public String getAskaskCode() {
//            return askaskCode;
//        }
//        public void setAskaskCode(String askaskCode) {
//            this.askaskCode = askaskCode;
//        }
//
//        public static ConfirmVoiceCodeMappingEnum fromCathayCode(String cathayCode) {
//            return codeMap.get(cathayCode);
//        }
//    }
//
//    /**
//     * <p>暫存，國泰狀態碼與問問狀態碼的對應</p>
//     *
//     * 問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
//     * -----------------------------------------------------------------------
//     * 000       000          暫存成功
//     * 110       097          未通過輸入檢核
//     * 111       099          系統有誤
//     */
//    public static enum TempVoiceCodeMappingEnum {
//        SUCCESS("000", "000"),
//        PARAMETER_ERROR("097", "110"),
//        SYSTEM_ERROR("099", "111");
//
//        private static Map<String, TempVoiceCodeMappingEnum> codeMap = new HashMap<>();
//        static {
//            codeMap.put(SUCCESS.getCathayCode(), SUCCESS);
//            codeMap.put(PARAMETER_ERROR.getCathayCode(), PARAMETER_ERROR);
//            codeMap.put(SYSTEM_ERROR.getCathayCode(), SYSTEM_ERROR);
//        }
//
//        private String cathayCode;
//        private String askaskCode;
//
//        private TempVoiceCodeMappingEnum(String cathayCode, String askaskCode) {
//            this.cathayCode = cathayCode;
//            this.askaskCode = askaskCode;
//        }
//
//        public String getCathayCode() {
//            return cathayCode;
//        }
//        public void setCathayCode(String cathayCode) {
//            this.cathayCode = cathayCode;
//        }
//        public String getAskaskCode() {
//            return askaskCode;
//        }
//        public void setAskaskCode(String askaskCode) {
//            this.askaskCode = askaskCode;
//        }
//
//        public static TempVoiceCodeMappingEnum fromCathayCode(String cathayCode) {
//            return codeMap.get(cathayCode);
//        }
//    }
//
//    /**
//     * <p>核保試算，國泰狀態碼與問問狀態碼的對應</p>
//     *
//     * 問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
//     * -----------------------------------------------------------------------
//     * 000       000          核保試算成功
//     * 141       011          未通過洗錢檢核
//     * 012       012          內外部通算未通過
//     * 142       097          未通過輸入檢核
//     * 143       099          系統有誤
//     */
//    public static enum CheckAmtVoiceCodeMappingEnum {
//        SUCCESS("000", "000"),
//        LAUNDERING_ERROR("011", "141"),
//        AUDIT_ERROR("012", "012"),
//        PARAMETER_ERROR("097", "142"),
//        SYSTEM_ERROR("099", "143");
//
//        private static Map<String, CheckAmtVoiceCodeMappingEnum> codeMap = new HashMap<>();
//        static {
//            codeMap.put(SUCCESS.getCathayCode(), SUCCESS);
//            codeMap.put(PARAMETER_ERROR.getCathayCode(), PARAMETER_ERROR);
//            codeMap.put(SYSTEM_ERROR.getCathayCode(), SYSTEM_ERROR);
//            codeMap.put(LAUNDERING_ERROR.getCathayCode(), LAUNDERING_ERROR);
//            codeMap.put(AUDIT_ERROR.getCathayCode(), AUDIT_ERROR);
//        }
//
//        private String cathayCode;
//        private String askaskCode;
//
//        private CheckAmtVoiceCodeMappingEnum(String cathayCode, String askaskCode) {
//            this.cathayCode = cathayCode;
//            this.askaskCode = askaskCode;
//        }
//
//        public String getCathayCode() {
//            return cathayCode;
//        }
//        public void setCathayCode(String cathayCode) {
//            this.cathayCode = cathayCode;
//        }
//        public String getAskaskCode() {
//            return askaskCode;
//        }
//        public void setAskaskCode(String askaskCode) {
//            this.askaskCode = askaskCode;
//        }
//
//        public static CheckAmtVoiceCodeMappingEnum fromCathayCode(String cathayCode) {
//            return codeMap.get(cathayCode);
//        }
//    }
//
//    /**
//     * <p>旅遊地點代碼，國泰狀態碼與問問狀態碼的對應</p>
//     *
//     * 問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
//     * -----------------------------------------------------------------------
//     * 000       000         投保成功
//     * 161       097         傳入參數有誤
//     * 162       099         執行錯誤
//     */
//    public static enum RgnCodeCodeMappingEnum {
//        SUCCESS("000", "000"),
//        PARAMETER_ERROR("097", "161"),
//        SYSTEM_ERROR("099", "162");
//
//        private static Map<String, RgnCodeCodeMappingEnum> codeMap = new HashMap<>();
//        static {
//            codeMap.put(SUCCESS.getCathayCode(), SUCCESS);
//            codeMap.put(PARAMETER_ERROR.getCathayCode(), PARAMETER_ERROR);
//            codeMap.put(SYSTEM_ERROR.getCathayCode(), SYSTEM_ERROR);
//        }
//
//        private String cathayCode;
//        private String askaskCode;
//
//        private RgnCodeCodeMappingEnum(String cathayCode, String askaskCode) {
//            this.cathayCode = cathayCode;
//            this.askaskCode = askaskCode;
//        }
//
//        public String getCathayCode() {
//            return cathayCode;
//        }
//        public void setCathayCode(String cathayCode) {
//            this.cathayCode = cathayCode;
//        }
//        public String getAskaskCode() {
//            return askaskCode;
//        }
//        public void setAskaskCode(String askaskCode) {
//            this.askaskCode = askaskCode;
//        }
//
//        public static RgnCodeCodeMappingEnum fromCathayCode(String cathayCode) {
//            return codeMap.get(cathayCode);
//        }
//    }
//
//}
