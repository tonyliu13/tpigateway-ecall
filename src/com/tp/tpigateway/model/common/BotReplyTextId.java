package com.tp.tpigateway.model.common;
// default package
// Generated 2018/5/24 �U�� 12:56:13 by Hibernate Tools 5.2.8.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BotReplyTextId generated by hbm2java
 */
@Embeddable
public class BotReplyTextId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6895430005706332461L;
	private String roleId;
	private String textId;

	public BotReplyTextId() {
	}

	public BotReplyTextId(String roleId, String textId) {
		this.roleId = roleId;
		this.textId = textId;
	}

	@Column(name = "role_id", nullable = false, length = 20)
	public String getRoleId() {
		return this.roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	@Column(name = "text_id", nullable = false, length = 30)
	public String getTextId() {
		return this.textId;
	}

	public void setTextId(String textId) {
		this.textId = textId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BotReplyTextId))
			return false;
		BotReplyTextId castOther = (BotReplyTextId) other;

		return ((this.getRoleId() == castOther.getRoleId()) || (this.getRoleId() != null
				&& castOther.getRoleId() != null && this.getRoleId().equals(castOther.getRoleId())))
				&& ((this.getTextId() == castOther.getTextId()) || (this.getTextId() != null
						&& castOther.getTextId() != null && this.getTextId().equals(castOther.getTextId())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getRoleId() == null ? 0 : this.getRoleId().hashCode());
		result = 37 * result + (getTextId() == null ? 0 : this.getTextId().hashCode());
		return result;
	}

}
