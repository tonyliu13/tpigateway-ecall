package com.tp.tpigateway.model.common;

public class DailySession {

    private SessionStage sessionStage;

    //要加的進修次數
    private int addNoResTime = 0;

    //要加的狀態分數
    private int addStageScore = 0;

    private CustSessionRec custSessionRec;

    //要加的對話次數
    private int addSessionTime = 0;

    private String source = "1"; //0:ChatWeb呼叫, 1:TPIGateway呼叫
    
    private String systemID;

    public DailySession() {

    }

    public DailySession(SessionStage sessionStage, CustSessionRec custSessionRec) {
        this.sessionStage = sessionStage;
        this.custSessionRec = custSessionRec;
    }

    public SessionStage getSessionStage() {
        return sessionStage;
    }

    public void setSessionStage(SessionStage sessionStage) {
        this.sessionStage = sessionStage;
    }

    public CustSessionRec getCustSessionRec() {
        return custSessionRec;
    }

    public void setCustSessionRec(CustSessionRec custSessionRec) {
        this.custSessionRec = custSessionRec;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getAddNoResTime() {
        return addNoResTime;
    }

    public void setAddNoResTime(int addNoResTime) {
        this.addNoResTime = addNoResTime;
    }

    public int getAddStageScore() {
        return addStageScore;
    }

    public void setAddStageScore(int addStageScore) {
        this.addStageScore = addStageScore;
    }

    public int getAddSessionTime() {
        return addSessionTime;
    }

    public void setAddSessionTime(int addSessionTime) {
        this.addSessionTime = addSessionTime;
    }

    public String getSystemID() {
		return systemID;
	}

	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}
}
