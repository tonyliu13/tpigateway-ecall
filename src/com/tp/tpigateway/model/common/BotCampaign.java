package com.tp.tpigateway.model.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "bot_campaign", schema = "public")
public class BotCampaign implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4620526693854665773L;

    @Id
    @Column(name = "campaign_id")
    private String campaignId;
    
    @Column(name = "campaign_nm")
    private String campaignNm;
    
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    
    @Column(name = "upd_id")
    private String updId;
    
    @Column(name = "upd_name")
    private String updName;
    
    @Column(name = "upd_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updDate;
    
    @Column(name = "campaign_type")
    private String campaignType;
    
    @Column(name = "cust_nm_enable")
    private String custNmEnable;
    
    @Column(name = "cust_sex_enable")
    private String custSexEnable;
    
    @Column(name = "cust_birth_enable")
    private String custBirthEnable;
    
    @Column(name = "cust_phone_enable")
    private String custPhoneEnable;
    
    @Column(name = "cust_email_enable")
    private String custEmailEnable;
    
    @Column(name = "cust_addr_enable")
    private String custAddrEnable;
    
    @Column(name = "is_insd_enable")
    private String checkInsdEnable;

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignNm() {
        return campaignNm;
    }

    public void setCampaignNm(String campaignNm) {
        this.campaignNm = campaignNm;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdName() {
        return updName;
    }

    public void setUpdName(String updName) {
        this.updName = updName;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public String getCustNmEnable() {
        return custNmEnable;
    }

    public void setCustNmEnable(String custNmEnable) {
        this.custNmEnable = custNmEnable;
    }

    public String getCustSexEnable() {
        return custSexEnable;
    }

    public void setCustSexEnable(String custSexEnable) {
        this.custSexEnable = custSexEnable;
    }

    public String getCustBirthEnable() {
        return custBirthEnable;
    }

    public void setCustBirthEnable(String custBirthEnable) {
        this.custBirthEnable = custBirthEnable;
    }

    public String getCustPhoneEnable() {
        return custPhoneEnable;
    }

    public void setCustPhoneEnable(String custPhoneEnable) {
        this.custPhoneEnable = custPhoneEnable;
    }

    public String getCustEmailEnable() {
        return custEmailEnable;
    }

    public void setCustEmailEnable(String custEmailEnable) {
        this.custEmailEnable = custEmailEnable;
    }

    public String getCustAddrEnable() {
        return custAddrEnable;
    }

    public void setCustAddrEnable(String custAddrEnable) {
        this.custAddrEnable = custAddrEnable;
    }

    public String getCheckInsdEnable() {
        return checkInsdEnable;
    }

    public void setCheckInsdEnable(String checkInsdEnable) {
        this.checkInsdEnable = checkInsdEnable;
    }

    @Override
    public String toString() {
        StringBuffer sbf = new StringBuffer();
        sbf.append('{');
        sbf.append("campaign_id:").append(this.campaignId);
        sbf.append(", ").append("campaign_nm:").append(this.campaignNm);
        sbf.append(", ").append("start_date:").append(this.startDate);
        sbf.append(", ").append("end_date:").append(this.endDate);
        sbf.append(", ").append("upd_id:").append(this.updId);
        sbf.append(", ").append("upd_name:").append(this.updName);
        sbf.append(", ").append("upd_date:").append(this.updDate);
        sbf.append(", ").append("campaign_type:").append(this.campaignType);
        sbf.append(", ").append("cust_nm_enable:").append(this.custNmEnable);
        sbf.append(", ").append("cust_sex_enable:").append(this.custSexEnable);
        sbf.append(", ").append("cust_birth_enable:").append(this.custBirthEnable);
        sbf.append(", ").append("cust_phone_enable:").append(this.custPhoneEnable);
        sbf.append(", ").append("cust_email_enable:").append(this.custEmailEnable);
        sbf.append(", ").append("cust_addr_enable:").append(this.custAddrEnable);
        sbf.append(", ").append("is_insd_enable:").append(this.checkInsdEnable);
        sbf.append('}');
        return sbf.toString();
    }
}
