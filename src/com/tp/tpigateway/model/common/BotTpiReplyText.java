package com.tp.tpigateway.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Bot呼叫TPI回覆內容
 */
@Entity
@Table(name = "bot_tpi_reply_text")
public class BotTpiReplyText implements Serializable {

	private static final long serialVersionUID = -7175208349588079745L;
	
	@Transient public static final String COLUMN_NAME_NODE_ID = "node_id";
	@Transient public static final String COLUMN_NAME_ROLE_ID = "role_id";
	@Transient public static final String COLUMN_NAME_TEXT_ID = "text_id";
	@Transient public static final String COLUMN_NAME_TEXT = "text";
	
	@Id
    private Integer id;

    @Column(name = COLUMN_NAME_NODE_ID, length = 20)
	private String nodeId;
    
    @Column(name = COLUMN_NAME_ROLE_ID, length = 20)
	private String roleId;
    
    @Column(name = COLUMN_NAME_TEXT_ID, length = 20)
    private String textId;
    
    @Column(name = COLUMN_NAME_TEXT, length = 4000)
    private String text;

	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
	    if (nodeId == null) {
            this.nodeId = "";
        } else {
            this.nodeId = nodeId;
        }
    }
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getTextId() {
		return textId;
	}
	public void setTextId(String textId) {
		this.textId = textId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
