package com.tp.tpigateway.model.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.tp.tpigateway.util.DateUtil;

@Entity
@Table(name = "bot_tourist_place", schema = "public")
public class BotTouristPlace implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6739469861318816989L;

    @EmbeddedId

    @AttributeOverrides({
            @AttributeOverride(name = "placeId", column = @Column(name = "place_id", nullable = false)),
            @AttributeOverride(name = "typeId", column = @Column(name = "type_id", nullable = false)) })
    private BotTouristPlaceId id;

    @Column(name = "place")
    private String place;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "m_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date mTime;
    
    @Column(name = "m_user_id")
    private String mUserId;
    
    @Column(name = "m_user_nm")
    private String mUserNm;

    public BotTouristPlaceId getId() {
        return id;
    }

    public void setId(BotTouristPlaceId id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getmTime() {
        return mTime;
    }

    public void setmTime(Date mTime) {
        this.mTime = mTime;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmUserNm() {
        return mUserNm;
    }

    public void setmUserNm(String mUserNm) {
        this.mUserNm = mUserNm;
    }
}
