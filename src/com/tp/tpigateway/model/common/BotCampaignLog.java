package com.tp.tpigateway.model.common;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.tp.tpigateway.util.DateUtil;

@Entity
@Table(name = "bot_campaign_log", schema = "public")
public class BotCampaignLog implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3016597334608194650L;

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "sessionId", column = @Column(name = "session_id", nullable = false)),
        @AttributeOverride(name = "campaignId", column = @Column(name = "campaign_id", nullable = false)) })
    private BotCampaignLogId id;
    
    @Column(name = "cust_nm")
    private String custNm;
    
    @Column(name = "cust_id")
    private String custId;
    
    @Column(name = "cust_sex")
    private String custSex;
    
    @Column(name = "cust_birth")
    private String custBirth;
    
    @Column(name = "cust_phone")
    private String custPhone;
    
    @Column(name = "cust_email")
    private String custEmail;
    
    @Column(name = "is_insd")
    private String checkInsd;
    
    @Column(name = "cust_addr")
    private String custAddr;
    
    @Column(name = "campaign_type")
    private String campaignType;
    
    @Column(name = "message")
    private String message;

    @Column(name = "add_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addTime;

    public BotCampaignLog() { }
    
    public BotCampaignLog(BotCampaignLogId id, String custNm, String custId, String custSex, String custBirth, String custPhone, String custEmail, String checkInsd,
            String custAddr, String campaignType, String message) {
        this.id = id;
        this.custNm = custNm;
        this.custId = custId;
        this.custSex = custSex;
        this.custBirth = custBirth;
        this.custPhone = custPhone;
        this.custEmail = custEmail;
        this.checkInsd = checkInsd;
        this.custAddr = custAddr;
        this.campaignType = campaignType;
        this.message = message;
        this.addTime = DateUtil.getNow();
    }
    
    public BotCampaignLogId getId() {
        return id;
    }

    public void setId(BotCampaignLogId id) {
        this.id = id;
    }

    public String getCustNm() {
        return custNm;
    }

    public void setCustNm(String custNm) {
        this.custNm = custNm;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustSex() {
        return custSex;
    }

    public void setCustSex(String custSex) {
        this.custSex = custSex;
    }

    public String getCustBirth() {
        return custBirth;
    }

    public void setCustBirth(String custBirth) {
        this.custBirth = custBirth;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCheckInsd() {
        return checkInsd;
    }

    public void setCheckInsd(String checkInsd) {
        this.checkInsd = checkInsd;
    }
    
    public String getCustAddr() {
        return custAddr;
    }

    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        StringBuffer sbf = new StringBuffer();
        sbf.append('{');
        sbf.append("session_id:").append(this.id.getSessionId());
        sbf.append(", ").append("campaign_id:").append(this.id.getCampaignId());
        sbf.append(", ").append("cust_nm:").append(this.custNm);
        sbf.append(", ").append("cust_id:").append(this.custId);
        sbf.append(", ").append("cust_sex:").append(this.custSex);
        sbf.append(", ").append("cust_birth:").append(this.custBirth);
        sbf.append(", ").append("cust_phone:").append(this.custPhone);
        sbf.append(", ").append("cust_email:").append(this.custEmail);
        sbf.append(", ").append("is_insd:").append(this.checkInsd);
        sbf.append(", ").append("custAddr:").append(this.custAddr);
        sbf.append(", ").append("campaignType:").append(this.campaignType);
        sbf.append(", ").append("message:").append(this.message);
        sbf.append(", ").append("add_time:").append(this.addTime);
        sbf.append('}');
        return sbf.toString();
    }
}
