package com.tp.tpigateway.model.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BotCampaignLogId implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2181342909394863642L;

    @Column(name = "session_id", nullable = false)
    private String sessionId;
    
    @Column(name = "campaign_id", nullable = false)
    private String campaignId;

    public BotCampaignLogId() { }
    
    public BotCampaignLogId(String sessionId, String campaignId) {
        this.sessionId = sessionId;
        this.campaignId = campaignId;
    }
    
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof BotCampaignLogId))
            return false;
        BotCampaignLogId castOther = (BotCampaignLogId) other;

        return ((this.getSessionId() == castOther.getSessionId()) || (this.getSessionId() != null
                && castOther.getSessionId() != null && this.getSessionId().equals(castOther.getSessionId())))
                && ((this.getCampaignId() == castOther.getCampaignId()) || (this.getCampaignId() != null
                        && castOther.getCampaignId() != null && this.getCampaignId().equals(castOther.getCampaignId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getSessionId() == null ? 0 : this.getSessionId().hashCode());
        result = 37 * result + (getCampaignId() == null ? 0 : this.getCampaignId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sbf = new StringBuffer();
        sbf.append('{');
        sbf.append("session_id:").append(this.getSessionId());
        sbf.append(", ").append("campaign_id:").append(this.getCampaignId());
        sbf.append('}');
        return sbf.toString();
    }
}
