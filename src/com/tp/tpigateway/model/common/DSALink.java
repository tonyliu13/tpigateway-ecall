package com.tp.tpigateway.model.common;

import com.tp.tpigateway.util.PropUtils;

public class DSALink extends LifeLink {

	public DSALink(int action, String text, String alt, String reply, String url) {
		super(action, text, alt, reply, url, "");
	}

	/**
	 * @author 李思穎
	 */
	public static class CathayLife {
		
		//https://www.cathaylife.com.tw/OCWeb/servlet/HttpDispatcher/OCTxBean/execute?ACTION_NAME=OCA1_0500&METHOD_NAME=initData
		public final static String DSA10001_WEBSITE = PropUtils.getProperty("cathaylife.DSA10001.website.url");
	}
}
