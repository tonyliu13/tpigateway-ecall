package com.tp.tpigateway.model.common;

import com.tp.tpigateway.util.PropUtils;

public class AAALink extends LifeLink {

	public AAALink(int action, String text, String alt, String reply, String url) {
		super(action, text, alt, reply, url, "");
	}
	
	/**
	 * @author 黃登俊
	 */
	public static class CathayLife {
		//https://www.cathaylife.com.tw/ocauth/OCWeb/servlet/HttpDispatcher/OCBX_0300/prompt?TRAN_ID=ABRJ_1002
		public final static String AAA10001URL = PropUtils.getProperty("cathaylife.ABA10001.website.url");	
		public final static String AAA00001_2_3URL = PropUtils.getProperty("cathaylife.AAA00001_2_3.website.url");
		public final static String AAA00001_2_3URL2 = PropUtils.getProperty("cathaylife.AAA00001_2_3.website.url2");
	}
	
	// 20190111 by Todd, 常用功能牌卡
    /**
	 * 理賠
	 * AAA00001
	 * @author Todd
	 */
    public static class AAA00001 {
    	public static LifeLink 如何申請理賠 = new LifeLink(15, "如何申請理賠", "如何申請理賠", "如何申請理賠", "");
    }
}
