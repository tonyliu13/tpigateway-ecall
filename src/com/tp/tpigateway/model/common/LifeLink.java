package com.tp.tpigateway.model.common;

import org.apache.commons.lang3.StringUtils;

import com.tp.tpigateway.util.PropUtils;

public class LifeLink {

	private final int action;
	private final String text;
	private final String alt;
	private final String reply;
	private final String url;
	private final String customerAction;

	// I8:(下載)保險費自動轉帳付款授權書 PDF
	public final static String PDF_01 = PropUtils.getProperty("cathaylife.download.pdf.01.url");
	// I8:(下載)信用卡繳交保費付款授權書 PDF
	public final static String PDF_02 = PropUtils.getProperty("cathaylife.download.pdf.02.url");
	// I8:(範例)保險費自動轉帳付款授權書 PDF
	public final static String PDF_example_01 = PropUtils.getProperty("cathaylife.example.pdf.01.url");
	// I8:(範例)信用卡繳交保費付款授權書 PDF
	public final static String PDF_example_02 = PropUtils.getProperty("cathaylife.example.pdf.02.url");
	// I8:廣告回郵信封 PDF
	public final static String PDF_03 = PropUtils.getProperty("cathaylife.download.pdf.03.url");
	// I11.1.1:國泰世華自動櫃員機 (ATM)
	public final static String PDF_04 = PropUtils.getProperty("cathaylife.download.pdf.04.url");
	// I11.1.2:國泰世華銀行MYATM
	public final static String PDF_05 = PropUtils.getProperty("cathaylife.download.pdf.05.url");
	// I12:信用卡繳費MYBILL
	public final static String PDF_06 = PropUtils.getProperty("cathaylife.download.pdf.06.url");
	// I13:MYBANK
	public final static String PDF_07 = PropUtils.getProperty("cathaylife.download.pdf.07.url");
	// I15:711
	public final static String PDF_08 = PropUtils.getProperty("cathaylife.download.pdf.08.url");
	// I16:全家
	public final static String PDF_09 = PropUtils.getProperty("cathaylife.download.pdf.09.url");
	// I19:會員網站
	public final static String PDF_10 = PropUtils.getProperty("cathaylife.download.pdf.10.url");
	// I22:下載 復效申請書 ?(未知格式)
	public final static String PDF_11 = PropUtils.getProperty("cathaylife.download.pdf.11.url");
	// I17:下載 劃撥單填寫範本 image(現行外聯中華郵政網址)
	public final static String PDF_12 = PropUtils.getProperty("cathaylife.download.pdf.12.url");
	// I29:附約終止申請書/I35:附約縮小申請書
	public final static String PDF_13 = PropUtils.getProperty("cathaylife.download.pdf.13.url");
	// I30:自然人憑證網路申請操作流程
	public final static String PDF_14 = PropUtils.getProperty("cathaylife.download.pdf.14.url");
	// I14:APP操作說明
	public final static String PDF_15 = PropUtils.getProperty("cathaylife.download.pdf.15.url");
	// I50:雲端繳款單操作說明
	public final static String PDF_16 = PropUtils.getProperty("cathaylife.download.pdf.16.url");
	// ABB10011_a 查詢繳款單操作方式
	public final static String PDF_17 = PropUtils.getProperty("cathaylife.download.pdf.17.url");

	public final static String I24_PDF_1 = PropUtils.getProperty("cathaylife.I24.pdf.01.url");

	public final static String GOOGLE_MAP_SEARCH_URL = PropUtils.getProperty("google.map.search.url");

	public final static String MEMBER_CENTER_URL = PropUtils.getProperty("cathaylife.member.center.url");

	public final static String CUSTOMER_SERVICE_PUHOE = PropUtils.getProperty("cathaylife.customer.service.phone");
	// I35:前往官網
	public final static String CATHAYLIFE_OFFICIAL_WEBSITE = PropUtils.getProperty("cathaylife.official.website");
	
	//I22 前往官網TRAN_ID=OCB2_0600
	public final static String CATHAYLIFE_WEBSITE_I22 = PropUtils.getProperty("cathaylife.I22.website.url");
	
	public static LifeLink 下載保險費自動轉帳付款授權書PDF = new LifeLink(4, "開啟", "下載", "", PDF_01, "");

	public static LifeLink 下載信用卡繳交保費付款授權書PDF = new LifeLink(4, "開啟", "下載", "", PDF_02, "");
	
	// 20190315 by Todd, 腳本優化I8
	public static LifeLink 範例保險費自動轉帳付款授權書PDF = new LifeLink(4, "範例", "下載", "", PDF_example_01, "");
	
	public static LifeLink 範例信用卡繳交保費付款授權書PDF = new LifeLink(4, "範例", "下載", "", PDF_example_02, "");

	public static LifeLink 下載廣告回郵信封PDF = new LifeLink(4, "開啟", "下載", "", PDF_03, "");

	public static LifeLink 下載操作流程圖PDF_ATM = new LifeLink(4, "開啟", "下載", "", PDF_04, "");

	public static LifeLink 下載操作流程圖PDF_MYATM = new LifeLink(4, "開啟", "下載", "", PDF_05, "");

	public static LifeLink 下載操作流程圖PDF_信用卡繳費MYBILL = new LifeLink(4, "開啟", "下載", "", PDF_06, "");

	public static LifeLink 下載操作流程圖PDF_MYBANK = new LifeLink(4, "開啟", "下載", "", PDF_07, "");

	public static LifeLink 下載操作流程圖PDF_711 = new LifeLink(4, "開啟", "下載", "", PDF_08, "");

	public static LifeLink 下載操作流程圖PDF_全家 = new LifeLink(4, "開啟", "下載", "", PDF_09, "");

	public static LifeLink 下載操作流程圖PDF_會員網站 = new LifeLink(4, "開啟", "下載", "", PDF_10, "");

	public static LifeLink 下載復效申請書 = new LifeLink(4, "開啟", "下載", "", PDF_11, "");

	public static LifeLink 劃撥單填寫範本 = new LifeLink(4, "開啟", "下載", "", PDF_12, "");

	public static LifeLink 附約終止申請書 = new LifeLink(4, "開啟", "下載", "", PDF_13, "");

	public static LifeLink 自然人憑證網路申請操作流程 = new LifeLink(4, "自然人憑證網路申請操作流程", "下載", "", PDF_14, "");

	public static LifeLink 下載操作流程圖PDF_APP = new LifeLink(4, "開啟", "下載", "", PDF_15, "");

	public static LifeLink 下載操作流程圖PDF_雲端繳款單 = new LifeLink(4, "開啟", "下載", "", PDF_16, "");

	public static LifeLink 查詢繳款單操作方式_雲端繳款單 = new LifeLink(4, "查詢繳款單操作方式", "下載", "", PDF_16, "");

	public static LifeLink 查詢繳款單操作方式 = new LifeLink(4, "查詢繳款單操作方式", "下載", "", PDF_17, "");

	public static LifeLink 匯款行庫資訊 = new LifeLink(4, "開啓", "開啓", "", I24_PDF_1, "");

	public static LifeLink goole_map_711 = new LifeLink(4, "查看GOOGLE地圖", "查看GOOGLE地圖", "", GOOGLE_MAP_SEARCH_URL + "7-11", "");

	public static LifeLink goole_map_全家 = new LifeLink(4, "查看GOOGLE地圖", "查看GOOGLE地圖", "", GOOGLE_MAP_SEARCH_URL + "全家", "");

	public static LifeLink 以要保人身分登入會員網站 = new LifeLink(4, "前往網站", "前往網站", "", MEMBER_CENTER_URL, "");

	public static LifeLink 撥號_來電客服中心 = new LifeLink(12, "撥號", CUSTOMER_SERVICE_PUHOE, "", "", "");

	// I21
	// 20181011 by Todd, keep保單號碼
	public static LifeLink 查保單業務員(String policyNo) {
		return LifeLink.buildLink(15, "查保單業務員", "我的保單服務人員是誰?", "派員收費" + policyNo, "", "");
	}

	public static LifeLink 看其他保單 = new LifeLink(2, "看其他保單", "看其他保單", "", "", "");

	// public static LifeLink 什麼是墊繳 = new LifeLink(11, "什麼是墊繳", "167", "什麼是墊繳", "", ""); // QA
	public static LifeLink 什麼是墊繳 = new LifeLink(11, "什麼是墊繳", "2042", "什麼是墊繳", "", ""); // QA by 0515 FAQ題庫調整

	// I8
	public static LifeLink 查辦理自動扣款方式 = new LifeLink(1, "辦自動扣款", "如何辦理自動扣款?", "", "", "");

	// I3
	public static LifeLink 最近的繳費紀錄 = new LifeLink(2, "最近的繳費紀錄", "最近的繳費紀錄", "", "", "");

	public static LifeLink 不了_結束對話 = new LifeLink(15, "我問完了", "我問完了", "看看阿發能幫什麼忙|{\"code\":\"I0.2\"}", "", "");

	// 20181011 by Todd, keep保單號碼
	public static LifeLink 看繳費管道(String policyNo) {
		return LifeLink.buildLink(15, "看繳費管道", "我可以用什麼方式繳保費?", "我可以用什麼方式繳保費?" + policyNo, "", "");
	}

	public static LifeLink 前往繳費 = new LifeLink(15, "前往繳費", "我可以用什麼方式繳保費?", "我可以用什麼方式繳保費?|{\"code\":\"I7Login\"}", "", "");

	public static LifeLink 甚麼是扣款基準日 = new LifeLink(1, "扣款基準日", "什麼是扣款基準日?", "", "", "");

	public static LifeLink 是 = new LifeLink(1, "是", "是的，我有換過信用卡", "", "", "");

	public static LifeLink 否 = new LifeLink(1, "否", "不，我卡號沒有變", "", "", "");

	public static LifeLink 總繳保費概況 = new LifeLink(2, "總繳保費概況", "總繳保費概況", "", "", "");

	// I9
	public static LifeLink 各繳費管道入帳時間 = new LifeLink(2, "保費入帳要多久時間", "保費入帳要多久時間", "", "", "");
	
	public static LifeLink 看其他繳交方式 = new LifeLink(15, "看其他繳交方式", "看其他繳交方式", "保費入帳要多久時間", "", "");

	public static LifeLink 下期應繳保費 = new LifeLink(2, "下期應繳保費", "下期應繳保費", "", "", "");

	public static LifeLink 看其他繳次 = new LifeLink(1, "看其他期", "看其他期附約繳費金額", "", "", "");

	public static LifeLink 如何延長信用卡效期 = new LifeLink(2, "如何延長信用卡效期", "如何延長信用卡效期", "", "", "");

	public static LifeLink 國泰人壽服務據點 = new LifeLink(2, "查服務據點", "國泰人壽服務據點?", "", "", "");

	public static LifeLink 服務據點 = new LifeLink(2, "服務據點", "國泰人壽服務據點?", "", "", "");

	public static LifeLink 有 = new LifeLink(15, "看阿發還能幫什麼忙", "看阿發還能幫什麼忙", "看看阿發能幫什麼忙", "", "");

	public static LifeLink 沒有了 = new LifeLink(15, "沒有了(清空對話)", "我沒有其他問題了", "滿意度調查", "", "");

	public static LifeLink ATM金融卡轉帳 = new LifeLink(2, "ATM金融卡轉帳", "ATM金融卡轉帳", "", "", "");

	public static LifeLink 信用卡線上繳費 = new LifeLink(2, "信用卡線上繳費", "信用卡線上繳費", "", "", "");

	public static LifeLink 看所有繳費管道 = new LifeLink(1, "看所有繳費管道", "我可以用什麼方式繳保費?", "", "", "");

	public static LifeLink 看保單適用的繳費管道 = new LifeLink(2, "看保單適用的繳費管道", "看保單適用的繳費管道", "", "", "");

	// public static LifeLink 如何申辦網路服務 = new LifeLink(11, "如何申辦網路服務", "1354", "", "", ""); // QA
	public static LifeLink 如何申辦網路服務 = new LifeLink(15, "如何申辦網路服務", "如何申辦網路服務", "網路服務資格辦理方式", ""); // FAQ 改成情境流程

	// public static LifeLink 超商多媒體機台 = new LifeLink(11, "超商繳費", "1120", "超商繳費", "", ""); // QA
	// public static LifeLink 超商多媒體機台 = new LifeLink(11, "超商繳費", "2075", "超商繳費", "", ""); // QA by 0515 FAQ題庫調整
	// 20190130 by Todd, 腳本優化-I9
	public static LifeLink 超商多媒體機台 = new LifeLink(15, "超商繳費", "超商繳費", "超商繳費I9.1.1", "", "");

	// public static LifeLink 郵政劃撥_QA = new LifeLink(11, "郵政劃撥", "1121", "郵政劃撥", "", ""); // QA
	// public static LifeLink 郵政劃撥_QA = new LifeLink(11, "郵政劃撥", "2076", "郵政劃撥", "", ""); // QA by 0515 FAQ題庫調整
	// 20190130 by Todd, 腳本優化-I9
	public static LifeLink 郵政劃撥_QA = new LifeLink(15, "郵政劃撥", "郵政劃撥", "郵政劃撥I9.1.2", "", "");

	// public static LifeLink 銀行匯款 = new LifeLink(11, "銀行匯款", "1154", "銀行匯款", "", ""); // QA

	// public static LifeLink 自動轉帳 = new LifeLink(11, "自動轉帳", "1700", "自動轉帳", "", ""); // QA
	// public static LifeLink 自動轉帳 = new LifeLink(11, "自動轉帳", "2142", "自動轉帳", "", ""); // QA by 0515 FAQ題庫調整
	// 20190130 by Todd, 腳本優化-I9
	public static LifeLink 自動轉帳 = new LifeLink(15, "自動轉帳", "自動轉帳", "自動轉帳I9.1.3", "", "");

	// public static LifeLink 銀行轉帳扣款基準日 = new LifeLink(11, "銀行轉帳扣款基準日", "1185", "銀行轉帳扣款基準日", "", ""); // QA

	// public static LifeLink 信用卡扣款基準日 = new LifeLink(11, "信用卡扣款基準日", "1186", "信用卡扣款基準日", "", ""); // QA

	// public static LifeLink 保單墊繳怎麼辦 = new LifeLink(11, "保單墊繳怎麼辦", "1701", "保單墊繳如何清償?", "", ""); // QA
	public static LifeLink 保單墊繳怎麼辦 = ABBLink.S027_保單墊繳怎麼辦("");

	public static LifeLink 國泰世華自動櫃員機 = new LifeLink(2, "國泰世華自動櫃員機", "國泰世華自動櫃員機", "", "", "");

	public static LifeLink 國泰世華網路提款機 = new LifeLink(2, "國泰世華網路提款機", "國泰世華網路提款機", "", "", "");

	public static LifeLink 我要自行輸入地址 = new LifeLink(2, "我要自行輸入地址", "我要自行輸入地址", "", "", "");

	public static LifeLink 我要重新輸入地址 = new LifeLink(2, "我要重新輸入地址", "我要重新輸入地址", "", "", "");

	public static LifeLink 最近有沒有要繳的保單 = new LifeLink(15, "最近要繳的保單", "最近有要繳的保單嗎?", "最近有沒有要繳的保單", "", "");

	// 20181015 by Todd, keep保單號碼
	public static LifeLink 我的保費有扣款嗎(String policyNo) {
		return LifeLink.buildLink(15, "保單是否繳費", "我的保單繳費了嗎?", "我的保費有扣款嗎" + policyNo, "", "");
	}

	// 20181011 by Todd, keep保單號碼
	public static LifeLink 保費扣款方式(String policyNo) {
		return LifeLink.buildLink(15, "保費扣款方式", "我的保單是從哪裡扣款?", "保單目前設定的繳費方式" + policyNo, "", "");
	}

	public static LifeLink 保單繳費方式 = new LifeLink(15, "保單繳費方式", "我的保單目前設定的繳費方式?", "保費從哪扣", "", "");

	// 20181011 by Todd, keep保單號碼
	public static LifeLink ibon(String policyNo) {
		return LifeLink.buildLink(15, "7-11統一超商", "如何在7-11超商繳保費?", "如何在7-11超商繳保費?" + policyNo, "", "");
	}

	// 20181011 by Todd, keep保單號碼
	public static LifeLink 全家FamiPort(String policyNo) {
		return LifeLink.buildLink(15, "全家便利商店", "如何在全家超商繳保費?", "如何在全家超商繳保費?" + policyNo, "", "");
	}

	public static LifeLink 信用卡繳費MyBill = new LifeLink(1, "MyBill線上輕鬆繳", "如何用信用卡線上繳保費?", "", "", "");

	public static LifeLink 網銀轉帳繳費MyBank = new LifeLink(1, "MyBank個人網路銀行", "如何用網路轉帳繳保費?", "", "", "");

	public static LifeLink 國泰世華ATM = new LifeLink(1, "國泰世華銀行ATM", "如何用ATM繳保費?", "", "", "");

	public static LifeLink 國泰人壽線上櫃檯 = new LifeLink(1, "國泰人壽線上櫃檯", "如何在國泰人壽線上櫃檯繳保費?", "", "", "");

	public static LifeLink 查詢繳費紀錄 = new LifeLink(2, "查詢繳費紀錄", "查詢繳費紀錄", "", "", "");

	public static LifeLink 保費從哪扣 = new LifeLink(1, "保單繳費方式", "我的保單是從哪裡扣款?", "", "", "");

	public static LifeLink 查詢契約狀況 = new LifeLink(2, "查詢契約狀況", "查詢契約狀況", "", "", "");

	public static LifeLink 查詢目前保單繳費管道 = new LifeLink(2, "查詢目前保單繳費管道", "查詢目前保單繳費管道", "", "", "");

	public static LifeLink 馬上去看 = new LifeLink(2, "馬上去看", "馬上去看", "", "", "");

	// I22
	public static LifeLink 申請復效辦理方式 = new LifeLink(1, "保單停效怎麼辦", "保單停效如何辦理復效?", "", "", "");
	// 20190111 by Todd, 常用功能牌卡
	public static LifeLink 申請保單復效 = new LifeLink(15, "申請保單復效", "申請保單復效", "保單停效如何辦理復效", "", "");

	public static LifeLink 到服務據點辦理自動扣款 = new LifeLink(1, "查看詳情", "到服務據點辦理自動扣款", "", "", "");

	public static LifeLink 到服務據點辦理保單復效 = new LifeLink(15, "查看詳情", "到服務據點辦理保單復效", "到服務據點辦理保單復效!_!{\"flow_prefix\":\"pay\"}", "", "");

	// 20190315 by Todd, 腳本優化I8
	// public static LifeLink 保單所屬業務人員 = new LifeLink(1, "查看詳情", "找業務員協助辦理自動扣款", "", "", "");
	public static LifeLink 保單所屬業務人員 = new LifeLink(1, "查看詳情", "業務人員辦理自動扣款", "", "", "");

	public static LifeLink 自行郵寄辦理自動扣款 = new LifeLink(1, "查看詳情", "自行郵寄辦理自動扣款", "", "", "");

	public static LifeLink 自行郵寄辦理保單復效 = new LifeLink(15, "查看詳情", "自行郵寄辦理保單復效", "自行郵寄辦理保單復效!_!{\"flow_prefix\":\"pay\"}", "", "");

	public static LifeLink 國泰人壽外幣帳戶 = new LifeLink(2, "國泰人壽外幣帳戶", "國泰人壽外幣帳戶", "", "", "");
	
	public static LifeLink 登入會員網站辦理保單復效 = new LifeLink(15, "查看詳情", "登入會員網站辦理保單復效", "登入會員網站辦理保單復效!_!{\"flow_prefix\":\"pay\"}", "", "");

	public static LifeLink 保單業務員 = new LifeLink(15, "查看詳情", "業務人員辦理保單復效", "保單業務員!_!{\"flow_prefix\":\"pay\"}", "", "");

	// 20190429 by Todd, I22腳本優化
	// public static LifeLink 保單能不能恢復效力 = new LifeLink(15, "保單能不能恢復效力", "保單能不能恢復效力", "保單能不能恢復效力!_!{\"flow_prefix\":\"pay\"}", "", "");
	public static LifeLink 恢復契約效力金額 = new LifeLink(15, "恢復契約效力金額", "恢復契約效力金額", "恢復契約效力金額!_!{\"flow_prefix\":\"pay\"}", "", "");
	//20181108優化
	public static LifeLink 看其他辦理方式_I22 = new LifeLink(2,"看其他辦理方式","申請復效辦理方式", "", "", "");
	
	// 20190805 by Todd, 流程優化
	public static LifeLink 保單復效操作步驟_開啟 = new LifeLink(4, "開啟", "開啟", "", PropUtils.getProperty("cathaylife.I22.edm.01.url"), "");
	
	public static LifeLink 前往官網_I22 = new LifeLink(4, "前往官網", "前往官網", "", CATHAYLIFE_WEBSITE_I22, "");

	// I20
	public static LifeLink 國泰人壽服務櫃台 = new LifeLink(1, "查服務據點", "國泰人壽服務據點?", "", "", "");

	public static LifeLink 郵政劃撥 = new LifeLink(1, "郵政劃撥", "如何去郵局繳保費?", "", "", "");

	public static LifeLink 我附近的櫃台 = new LifeLink(2, "我附近的櫃台", "我附近的櫃台", "", "", "");

	// 20190315 by Todd, 腳本優化I8
	// public static LifeLink 查看郵件地址 = new LifeLink(2, "查看郵件地址", "查看郵件地址", "", "", "");
	public static LifeLink 查看郵件地址 = new LifeLink(2, "查看郵寄地址", "查看郵寄地址!_!{\"flow_prefix\":\"pay\"}", "", "", "");

	// 20181015 by Todd, keep保單號碼
	public static LifeLink 查詢目前墊繳金額(String policyNo) {
		return LifeLink.buildLink(15, "查詢目前墊繳金額", "查詢目前墊繳金額", "查詢目前墊繳金額" + policyNo, "", "");
	}

	// S006
	public static LifeLink 看附約資訊 = new LifeLink(2, "看附約資訊", "我想查詢保單目前的附約的狀況", "", "", "");

	public static LifeLink 重新輸入金額 = new LifeLink(2, "重新輸入金額", "重新輸入金額", "", "", "");

	public static LifeLink 轉真人客服 = new LifeLink(2, "轉真人客服", "轉真人客服", "", "", "");

	public static LifeLink 辦理附約終止 = new LifeLink(2, "辦理附約終止", "辦理附約終止", "", "", "");

	// S053
	public static LifeLink 叫你們經理出來 = new LifeLink(2, "轉接真人文字客服", "已有考量由真人服務的項目", "", "", "");

	// I25
	public static LifeLink 附約終止辦理方式 = new LifeLink(1, "看附約能不能終止", "附約終止辦理方式", "", "", "");

	// I26
	public static LifeLink 重新選擇附約 = new LifeLink(2, "重新選擇附約", "重新選擇附約", "", "", "");

	// I28
	public static LifeLink 前往會員網站辦理 = new LifeLink(2, "前往會員網站辦理", "前往會員網站辦理", "", "", "");

	// I29.1
	public static LifeLink 看其他辦理方式 = new LifeLink(15, "看其他辦理方式", "看其他辦理方式", "看其他終止方式", "", "");
	public static LifeLink 如何辦理附約終止 = new LifeLink(2, "如何辦理附約終止", "如何辦理附約終止", "", "", "");

	// I29.1.1.1
	public static LifeLink I29_查看詳情1 = new LifeLink(2, "查看詳情29111", "查看詳情29111", "", "", "");
	// I29.1.1.2
	public static LifeLink I29_查看詳情2 = new LifeLink(2, "查看詳情29121", "查看詳情29121", "", "", "");
	// I29.1.1.3
	public static LifeLink I29_查看詳情3 = new LifeLink(2, "查看詳情29131", "查看詳情29131", "", "", "");
	// I29.1.1.4
	public static LifeLink I29_查看詳情4 = new LifeLink(2, "查看詳情29141", "查看詳情29141", "", "", "");

	// I30
	public static LifeLink 網路服務資格辦理方式 = new LifeLink(2, "網路服務資格辦理方式", "網路服務資格辦理方式", "", "", "");
	// I30.1
	public static LifeLink I30_查看詳情1 = new LifeLink(2, "查看詳情301", "查看詳情301", "", "", "");
	// I30.2
	public static LifeLink I30_查看詳情2 = new LifeLink(2, "查看詳情302", "查看詳情302", "", "", "");

	// I34.1.2
	public static LifeLink 查其他縮小方式 = new LifeLink(2, "查其他縮小方式", "查其他縮小方式", "", "", "");
	// I34.2.1
	public static LifeLink 申辦網路服務方式 = new LifeLink(1, "申辦網路服務方式", "網路服務資格辦理方式", "", "", "");

	// I35
	public static LifeLink 如何辦理附約縮小 = new LifeLink(1, "如何辦理附約縮小", "如何辦理附約縮小", "", "", "");

	public static LifeLink 前往官網 = new LifeLink(4, "前往官網", "前往官網", "", CATHAYLIFE_OFFICIAL_WEBSITE, "");

	// 聯絡客服中心(A10)
	public static LifeLink 聯絡客服中心 = new LifeLink(1, "聯絡客服中心", "聯絡客服中心", "", "", "");

	// 查所有業務員(S042)
	public static LifeLink 查所有業務員 = new LifeLink(15, "查所有業務員", "我的服務人員是誰？", "查所有業務員", "", "");

	//雲端繳款單(I50) NLU:如何在雲端繳款單繳保費?
	// 20181011 by Todd, keep保單號碼
	public static LifeLink 雲端繳款單(String policyNo) {
		return LifeLink.buildLink(15, "雲端繳款單", "如何在雲端繳款單繳保費", "如何在雲端繳款單繳保費" + policyNo, "", "");
	}

	// 國泰人壽APP(I14)NLU:如何在國泰人壽APP繳保費?
	public static LifeLink 國泰人壽APP = new LifeLink(1, "國泰人壽APP", "如何在國泰人壽APP繳保費", "", "", "");

	// 好的，請專業的來(S050.a)
	public static LifeLink 好的_請專業的來 = new LifeLink(1, "好的，請專業的來", "好的，請專業的來", "", "", "");
	// 不了，我思考一下(S050.b)
	public static LifeLink 不了_我思考一下 = new LifeLink(1, "不了，我思考一下", "不了，我思考一下", "", "", "");
	// 好的(S050.a.1.1)
	public static LifeLink 好的 = new LifeLink(2, "好的", "好的", "好的", "", "");
	// 不了(S050.a.1.2)
	public static LifeLink 不了 = new LifeLink(15, "不了", "不了", "不了S050.a.1.2", "", "");

	public LifeLink(int action, String text, String alt, String reply, String url, String customerAction) {
		super();
		this.action = action;
		this.text = text;
		this.alt = alt;
		this.reply = reply;
		this.url = url;
		this.customerAction = customerAction;
	}

	public LifeLink(int action, String text, String alt, String reply, String url) {
		super();
		this.action = action;
		this.text = text;
		this.alt = alt;
		this.reply = reply;
		this.url = url;
		this.customerAction = "";
	}

	public static LifeLink buildLink(int action, String text, String alt, String reply, String url, String customerAction) {
		return new LifeLink(action, text, alt, reply, url, customerAction);
	}

	public static LifeLink downloadFileLink(String downloadFileFullPath, String downloadFileName, String text) {
		return downloadFileLink(downloadFileFullPath, downloadFileName, null, text);
	}

	public static LifeLink downloadFileLink(String downloadFileFullPath, String downloadFileName, String lts, String text) {
		StringBuilder sb = new StringBuilder(PropUtils.CATHAYLIFE_DOWNLOAD_FILE_URL + "?");
		if (StringUtils.isNotBlank(lts)) {
			sb.append("lts=").append(lts).append("&");
		}
		sb.append("downloadFileFullPath=").append(downloadFileFullPath).append("&").append("downloadFileName=")
				.append(downloadFileName);

		return new LifeLink(4, text, "", "", sb.toString(), "");
	}

	public static LifeLink openUrlLink(String url, String text) {
		return LifeLink.buildLink(4, text, text, "", url, "");
	}

	// 20181011 by Todd, keep保單號碼
	// public static LifeLink 查附約繳費金額(String PAY_TIMES, String RCPT_SER_NO) {
	public static LifeLink 查附約繳費金額(String PAY_TIMES, String RCPT_SER_NO, String POLICY_NO) {
		// String alt = "查附約繳費金額," + PAY_TIMES + "," + RCPT_SER_NO;
		String alt = "查附約繳費金額," + PAY_TIMES + "," + RCPT_SER_NO + "," + POLICY_NO;
		return LifeLink.buildLink(2, "查附約繳費金額", alt, "", "", "");
	}

	public static LifeLink 最近的繳費紀錄(String polciyNo) {
		return LifeLink.buildLink(2, "最近的繳費紀錄", "最近的繳費紀錄" + polciyNo, "", "", "");
	}

	public static LifeLink 我要查保單(String prodName, String polciyNo) {
		String text = "我要查保單" + prodName + "(" + polciyNo + ")";
		return LifeLink.buildLink(2, text, polciyNo, "", "", "");
	}

	public static LifeLink 下載操作流程圖PDF(String url) {
		return LifeLink.buildLink(4, "開啟", "下載", "", url, "");
	}

	public static LifeLink 撥號(String tel) {
		return LifeLink.撥號("撥號", tel);
	}

	public static LifeLink 撥號(String text, String tel) {
		return LifeLink.buildLink(12, text, tel, "", "", "");
	}

	public static LifeLink 查google地圖(String addr) {
		String url = PropUtils.GOOGLE_MAP_SEARCH_PLACE_URL + addr;
		return LifeLink.buildLink(4, "查google地圖", "查google地圖", "", url, "");
	}

	public static LifeLink 寄信(String mail, String subject, String body) {
		return LifeLink.mailLink("寄信", mail, subject, body);
	}

	public static LifeLink 寄信(String mail) {
		return LifeLink.寄信(mail, PropUtils.MAIL_SUBJECT, PropUtils.MAIL_BODY);
	}

	public static LifeLink mailLink(String text, String mail) {
		return LifeLink.mailLink(text, mail, PropUtils.MAIL_SUBJECT, PropUtils.MAIL_BODY);
	}

	public static String getDefaultMailAlt(String mail) {
		return LifeLink.getMailAlt(mail, PropUtils.MAIL_SUBJECT, PropUtils.MAIL_BODY);
	}

	public static String getMailAlt(String mail, String subject, String body) {
		subject = subject == null ? "" : subject;
		body = body == null ? "" : body.replaceAll("!_!", "%0d%0a");
		String alt = mail + "?subject=" + subject + "&body=" + body;
		return alt;
	}

	/** 不帶預設主旨及內文 */
	public static LifeLink mailLink(String text, String mail, String subject, String body) {
		String alt = mail;// LifeLink.getMailAlt(mail, subject, body);
		return LifeLink.buildLink(13, text, alt, "", "", "");
	}

	public static LifeLink 保費從哪裡扣(String polciyNo) {
		return LifeLink.buildLink(15, "保單繳費方式", "我的保單目前設定的繳費方式?", polciyNo, "", "");
	}

	public int getAction() {
		return action;
	}

	public String getText() {
		return text;
	}

	public String getAlt() {
		// return PropLifeUtils.addIntent(alt);
		return alt;
	}

	public String getReply() {
		return reply;
	}

	public String getUrl() {
		return url;
	}

	public String getCustomerAction() {
		return customerAction;
	}

}
