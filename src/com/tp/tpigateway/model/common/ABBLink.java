package com.tp.tpigateway.model.common;

import com.tp.tpigateway.util.PropUtils;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ABBLink extends LifeLink {




	public static LifeLink 查看詳情 = new LifeLink(2, "查看詳情", "查看詳情", "", "");






	// I30:自然人憑證網路申請操作流程
	public final static String PDF_14 = PropUtils.getProperty("cathaylife.download.pdf.14.url");

	public static LifeLink 自然人憑證網路申請操作流程 = new LifeLink(4, "自然人憑證操作流程", "下載", "", PDF_14);
	
	// 20191204 by Todd, 申請網路服務優化
	public final static String PDF_18 = PropUtils.getProperty("cathaylife.download.pdf.18.url");
	
	public static LifeLink 網銀申辦操作流程 = new LifeLink(4, "網銀申辦操作流程", "下載", "", PDF_18);

	// public static LifeLink I30_看其他方式 = new LifeLink(1, "看其他辦理方式", "網路服務資格辦理方式", "", "");
	public static LifeLink I30_看其他方式 = new LifeLink(15, "看其他辦理方式", "網路服務資格辦理方式", "網路服務資格辦理方式!_!{\"flow_prefix\":\"abb\"}", "", "");
	
	public static LifeLink 申辦網路服務方式 = new LifeLink(1, "申辦網路服務方式", "網路服務資格辦理方式", "", "");
	// 20191108 by Todd, 調整選項點擊顯示文字，及後送埋入flow_prefix
	// I30.1
	// public static LifeLink I30_查看詳情1 = new LifeLink(2, "查看詳情", "查看詳情-臨櫃申辦", "", "");
	public static LifeLink I30_查看詳情1 = new LifeLink(15, "查看詳情", "到服務據點辦理網路服務", "查看詳情-臨櫃申辦!_!{\"flow_prefix\":\"abb\"}", "");
	// I30.2
	// public static LifeLink I30_查看詳情2 = new LifeLink(2, "查看詳情", "查看詳情-網路申辦", "", "");
	public static LifeLink I30_查看詳情2 = new LifeLink(15, "查看詳情", "到國泰官方網站辦理網路服務", "查看詳情-網路申辦!_!{\"flow_prefix\":\"abb\"}", "");
	public static LifeLink I30_查看詳情3 = new LifeLink(15, "查看詳情", "到國泰世華網銀開通網路服務", "查看詳情-網銀申辦!_!{\"flow_prefix\":\"abb\"}", "");
    
	public static LifeLink 沒有了_滿意度調查 = new LifeLink(2, "沒有了(清空對話)", "滿意度調查", "", "", "");


	// I44 ABB10005 看ATM可借款金額
	public static class ABB10005 {
		public static LifeLink 看ATM可借金額 = new LifeLink(2, "看ATM可借金額", "看ATM可借金額", "", "");		

		public static LifeLink 是否申辦ATM借款 = new LifeLink(2, "是否申辦ATM借款", "看ATM可借款金額", "", "");	
		
		// 20190213  腳本優化-ABB10005.3
		public static LifeLink 已開通哪些保單可借款 = new LifeLink(15, "已開通哪些保單可借款", "已開通哪些保單可借款", "已開通哪些保單可借款ABB10005.3.1", "");
		
	}


	// S27
	// 20181015 by Todd, keep保單號碼
	public static LifeLink S027_保單墊繳怎麼辦(String policyNo) {
		return LifeLink.buildLink(15, "保單墊繳怎麼辦", "保單墊繳如何清償?", "保單墊繳如何清償?" + policyNo, "", "");
	}
	// I39
	public static LifeLink I39_會員網站 = new LifeLink(15, "會員網站", "進入國泰人壽會員網站查詢", "國泰人壽會員網站查詢", "");

	// S019 ABB10011
	// 20181015 by Todd, keep保單號碼
	// 20190917 by Felity,牌卡送出顯示文字調整
	public static LifeLink 查看詳情_S019_a(String policyNo) {
		//return LifeLink.buildLink(2, "查看詳情", "查看詳情-雲端繳款ABB10011_4_1," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "", "");
		return LifeLink.buildLink(15, "查看詳情", "透過雲端繳款辦理保單還款", "查看詳情-雲端繳款ABB10011_4_1," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
	public static LifeLink 查看詳情_S019_b(String policyNo) {
		//return LifeLink.buildLink(2, "查看詳情", "查看詳情-繳款單ABB10011_4_2," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "", "");
		return LifeLink.buildLink(15, "查看詳情", "透過繳款單辦理保單還款", "查看詳情-繳款單ABB10011_4_2," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
	//public static LifeLink 查看詳情_I45_2 = new LifeLink(2, "查看詳情", "如何用ATM保單還款", "", "");
	public static LifeLink 查看詳情_I45_2 = new LifeLink(15, "查看詳情", "如何用ATM保單還款", "如何用ATM保單還款", "", "");
	public static LifeLink 查看詳情_S019_c(String policyNo) {
		//return LifeLink.buildLink(2, "查看詳情", "查看詳情-現金ABB10011_4_3," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "", "");
		return LifeLink.buildLink(15, "查看詳情", "透過服務據點辦理保單還款", "查看詳情-現金ABB10011_4_3," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
	
	
	public static LifeLink 查外幣帳戶 = new LifeLink(1, "查外幣帳戶", "國泰人壽專用外幣匯款帳號", "", "", "");
	//S019
	public static LifeLink 看其他方式_S019 = new LifeLink(2, "看其他方式", "我的保單貸款還款金額", "", "");

	//查所有業務員(S042)
	public static LifeLink 查所有業務員 = new LifeLink(15,"查看業務員","我的服務人員是誰？","查所有業務員","", "");					   
	public ABBLink(int action, String text, String alt, String reply, String url) {
		super(action, text, alt, reply, url);
	}
	
	
    public static class GoogleMap {
    	public static LifeLink 最近ATM = new LifeLink(4, "查看GOOGLE地圖", "查看GOOGLE地圖", "",
    			GOOGLE_MAP_SEARCH_URL + "ATM", ""); 
    	
        public static LifeLink 查最近ATM = new LifeLink(4, "查最近ATM", "查看GOOGLE地圖", "", GOOGLE_MAP_SEARCH_URL + "ATM", "");
    }

    
    public static class CathayLife {
		
		// 保單貸款前往國泰人壽官網 https://www.cathaylife.com.tw/OCWeb/servlet/HttpDispatcher/OCBX_1200/understand?TRAN_ID=ABRJ_A00000
		public final static String ABB10007_WEB_URL = PropUtils.getProperty("cathaylife.ABB10007.website.url");
		
		// https://www.cathaylife.com.tw/OCWeb/servlet/HttpDispatcher/OCTxBean/execute?ACTION_NAME=OCA1_0100&METHOD_NAME=initData&MENU_ID=M1_102
		public final static String ABB10007_2_URL = PropUtils.getProperty("cathaylife.ABB10007.2.website.url");
		
		public static LifeLink 貸款國泰人壽官網 = new LifeLink(4,"前往國泰人壽官網","前往國泰人壽官網","",ABB10007_WEB_URL, "");
		
		//https://swww.cathaylife.com.tw/OCWeb/servlet/HttpDispatcher/OCBX_1200/understand?TRAN_ID=ABRJ_A00000    	
    }
	
	

	// ABB00003 I47  如何申辦ATM借款(A08)
	public static class ABB00003 {
		public static LifeLink 申辦ATM借還款 = new LifeLink(2, "申辦ATM借還款服務", "如何申辦ATM借款", "", "");
		public static LifeLink 如何申辦ATM借款 = new LifeLink(2, "如何申辦ATM借款", "如何申辦ATM借款", "", "");
		public static LifeLink 查看其他ATM借款方式 = new LifeLink(2, "查看其他方式", "如何申辦ATM借款", "", "");

		// ABB00003.1.1
		public static LifeLink ATM借款臨櫃申辦 = new LifeLink(2, "臨櫃申辦", "臨櫃申辦ATM借款", "", "");

		// ABB00003.1.2
		public static LifeLink ATM借款業務人員申辦 = new LifeLink(2, "業務人員申辦", "業務人員申辦ATM借款", "", "");
	}
	//I45   如何用ATM保單借還款 (BA05ATM借還款各管道操作路徑)
	public static class ABB00004 {

		// I45
		public static LifeLink 如何用ATM保單借還款 = new LifeLink(2, "如何ATM保單借還款", "如何用ATM保單借還款", "", "");
		
		// ABB00004.1
		public static LifeLink ABB00004_1 = new LifeLink(2, "ATM保單借款", "ABB00004_1", "", "");

		// ABB00004.1
		public static LifeLink ABB00004_1_ATM借款操作 = new LifeLink(2, "ATM保單借款", "ATM借款操作", "", "");
		
		public static LifeLink 如何ATM保單借款 = new LifeLink(2, "如何ATM保單借款", "ATM借款操作", "", "");

		// ABB00004.2
		public static LifeLink ABB00004_2_ATM還款操作 = new LifeLink(2, "ATM保單還款", "ATM還款操作", "", "");
		// ABB00004.1.1 實體ATM借款操作
		public static LifeLink ABB00004_1_1 = new LifeLink(2, "查看詳情", "ABB00004_1_1", "", "");
		// ABB00004.1.1.1 國泰世華實體ATM借款操作
		public static LifeLink ABB00004_1_1_1 = new LifeLink(2, "國泰世華銀行", "ABB00004_1_1_1", "", "");
		// ABB00004.1.1.2 非國泰世華實體ATM借款操作
		public static LifeLink ABB00004_1_1_2 = new LifeLink(2, "其他銀行", "ABB00004_1_1_2", "", "");
		// ABB00004.1.2 國泰世華MyATM借款操作
		public static LifeLink ABB00004_1_2 = new LifeLink(2, "查看詳情", "ABB00004_1_2", "", "");
		// ABB00004.1.3 國泰世華MyBANK借款操作
		public static LifeLink ABB00004_1_3 = new LifeLink(2, "查看詳情", "ABB00004_1_3", "", "");
		// ABB00004.2 ATM還款操作
		public static LifeLink ABB00004_2 = new LifeLink(2, "ATM保單還款", "ABB00004_2", "", "");
		// ABB00004.2.1 國泰世華ATM還款操作
		public static LifeLink ABB00004_2_1 = new LifeLink(2, "查看詳情", "ABB00004_2_1", "", "");
		// ABB00004.2.2 國泰世華MyATM還款操作
		public static LifeLink ABB00004_2_2 = new LifeLink(2, "查看詳情", "ABB00004_2_2", "", "");
		// ABB00004.2.3 國泰世華MyBANK還款操作
		public static LifeLink ABB00004_2_3 = new LifeLink(2, "查看詳情", "ABB00004_2_3", "", "");

		// ABB00004.1
		public static LifeLink ABB00004_1_ATM其他借款方式 = new LifeLink(2, "看其他方式", "ATM借款操作", "", "");

		// ABB00004.2
		public static LifeLink ABB00004_2_ATM其他還款方式 = new LifeLink(2, "看其他方式", "ATM還款操作", "", "");

		// ABB00004.1.1.1:提款機ATM保單借款操作說明
		public final static String ABB00004_PDF_URL_01 = PropUtils.getProperty("cathaylife.ABB00004.pdf.01.url");
		// ABB00004.1.2 MyATM保單借款操作說明
		public final static String ABB00004_PDF_URL_02 = PropUtils.getProperty("cathaylife.ABB00004.pdf.02.url");
		// ABB00004.1.3 MyBANK保單借款操作說明
		public final static String ABB00004_PDF_URL_03 = PropUtils.getProperty("cathaylife.ABB00004.pdf.03.url");
		// ABB00004.2.1 提款機ATM保單轉帳還款說明
		public final static String ABB00004_PDF_URL_04 = PropUtils.getProperty("cathaylife.ABB00004.pdf.04.url");
		// ABB00004.2.2 MyATM保單轉帳還款說明
		public final static String ABB00004_PDF_URL_05 = PropUtils.getProperty("cathaylife.ABB00004.pdf.05.url");
		// ABB00004.2.3 :MyBANK保單轉帳還款說明
		public final static String ABB00004_PDF_URL_06 = PropUtils.getProperty("cathaylife.ABB00004.pdf.06.url");

		public static LifeLink PDF_01_ATM借款操作 = new LifeLink(4, "開啟", "下載", "", ABB00004_PDF_URL_01);

		public static LifeLink PDF_02_MyATM借款操作 = new LifeLink(4, "開啟", "下載", "", ABB00004_PDF_URL_02);

		public static LifeLink PDF_03_MyBANK借款操作 = new LifeLink(4, "開啟", "下載", "", ABB00004_PDF_URL_03);

		public static LifeLink PDF_04_ATM還款操作 = new LifeLink(4, "開啟", "下載", "", ABB00004_PDF_URL_04);

		public static LifeLink PDF_05_MyATM還款操作 = new LifeLink(4, "開啟", "下載", "", ABB00004_PDF_URL_05);

		public static LifeLink PDF_06_MyBANK款操作 = new LifeLink(4, "開啟", "下載", "", ABB00004_PDF_URL_06);		
	}

	/**
	 * I41  S022ID歸戶可借金額
	 * 看我的保單可借款金額
	 * 
	 * @author 楊宗翰
	 */
	public static class ABB10007 {
		// ABB10007
		public static LifeLink 看保單可借款金額 = new LifeLink(2, "看總可借款金額", "看我的保單可借款金額", "", "");
		
		public static LifeLink 看全部可借保單 = new LifeLink(2, "看全部可借保單", "看全部可借保單", "", "");
		
		public static LifeLink 保單是否可以借款 = new LifeLink(15, "保單是否可以借款", "保單是否可以借款", "看保單可借款金額", "");

		public static LifeLink 看各幣別可借保單 = new LifeLink(2, "看各幣別可借保單", "ABB10007_1_2", "", "");

		public static LifeLink 立即借款(String policyNo) {
			Map<String, String> tmp = new HashMap<String, String>();
			tmp.put("POLICY_NO", policyNo);
			String alt = "ABB10007_1_1_Web|" + new JSONObject(tmp).toString();
			return new LifeLink(2, "立即借款", alt, "", "");
		}

		public static LifeLink 查看保單(String curr) {
			Map<String, String> tmp = new HashMap<String, String>();
			tmp.put("CURR", curr);
			String alt = "查看可借保單|" + new JSONObject(tmp).toString();
			return new LifeLink(2, "查看保單", alt, "", "");
		}
		

	}
	
	/**
	 * 查詢借款方式  I46.1
	 * ABB00006
	 * @author 楊宗翰
	 */
	public static class ABB00006 {
		public static LifeLink 看借款辦理方式 = new LifeLink(2, "借款辦理方式", "如何申辦保單借款", "", "");

		public static LifeLink 透過服務據點辦理 = new LifeLink(1, "查看詳情", "服務據點保單借款", "", "");

		public static LifeLink 透過業務人員辦理 = new LifeLink(1, "查看詳情", "業務人員申辦保單借款", "", "");

		public static LifeLink 透過ATM借款 = new LifeLink(1, "查看詳情", "ATM借款詳情", "", "");

		public static LifeLink 透過線上借款 = new LifeLink(1, "查看詳情", "線上借款詳情", "", "");

		public static LifeLink 看其他方式 = new LifeLink(2, "看其他方式", "如何申辦保單借款", "", "");
	}

	// ABB10004 (S014)
	public static class ABB10004 {
		// 20190211 by Todd, 腳本優化-I9
		// public static LifeLink 查借還款紀錄 = new LifeLink(2, "過去借還款紀錄", "查借還款紀錄", "", "");
		public static LifeLink 查借還款紀錄 = new LifeLink(1, "查借還款紀錄", "我的保單借還款紀錄", "", "");
	}	
	
	/**
	 * 紅利給付
	 * 
	 * @author 黃登俊
	 */
	public static class ABB10010 {		
		public static LifeLink S021 = new LifeLink(2, "我想看我的保單有沒有領過紅利？", "我想看我的保單有沒有領過紅利？", "", "");
		
		public static LifeLink 保單能不能領取紅利() {		
			String alt = LifeLink.叫你們經理出來.getAlt();
			return new LifeLink(2, "保單能不能領取紅利", alt, "", "");
		}
		
		public static LifeLink ABB10010_1_2	 = new LifeLink(15, "保單能不能領取紅利", "保單能不能領取紅利", "ABB10010_1_2保單能不能領取紅利", "");
	}	
	
	/**
	 * 我想要查紅利
	 * 
	 * @author 黃登俊
	 */
	public static class ABB10013 {		
		public static LifeLink S021 = new LifeLink(2, "我想看我的保單有沒有領過紅利？", "我想看我的保單有沒有領過紅利？", "", "");
		public static LifeLink 申請紅利的方式() {		
			String alt = LifeLink.叫你們經理出來.getAlt();
			return new LifeLink(2, "申請紅利的方式", alt, "", "");
		}
		
		public static LifeLink 保單能不能領取紅利() {		
			String alt = LifeLink.叫你們經理出來.getAlt();
			return new LifeLink(2, "保單能不能領取紅利", alt, "", "");
		}
		
		public static LifeLink 已經領取的紀錄() {		
			String alt = ABBLink.ABB10010.S021.getAlt();
			return new LifeLink(2, "保單能不能領取紅利", alt, "", "");
		}
	}	
	
	//I38  S018  ATM借(還)款記錄
	public static class ABB10006{
		// I38.1
		public static LifeLink 查ATM已貸金額 = new LifeLink(2, "查ATM已貸金額", "查ATM已貸金額", "", "");
		// TODO 連結GOOGLE地圖
		public static LifeLink 查最近ATM = GoogleMap.最近ATM;
		// I38.2 查ATM借還款紀錄可傳入保單號碼
		//public static LifeLink 查ATM借還款紀錄 = new LifeLink(2, "查ATM借還款紀錄", "查ATM借還款紀錄", "", "");				
		public static LifeLink 查ATM借還款紀錄(String policyNo) {
			return LifeLink.buildLink(15, "查ATM借還款紀錄", "查ATM借還款紀錄", "查ATM借還款紀錄" + policyNo, "", "");
		}
		// 20190130 by Todd, 腳本優化-ABB10006.1
		public static LifeLink 已開通哪些保單可還款 = new LifeLink(15, "已開通哪些保單可還款", "已開通哪些保單可還款", "已開通哪些保單可還款ABB10006.1.3!_!{\"flow_prefix\":\"abb\"}", "");
	}	
	
	/**
	 * 查詢解約辦理方式
	 * 
	 * @author 黃登俊
	 */
	public static class ABB00001 {
		public static LifeLink BA04 = new LifeLink(2, "我想要做解約", "我想要做解約", "", "");
		
		public static LifeLink 告訴阿發 = new LifeLink(2, "好，告訴阿發", "好，告訴阿發ABB00001", "", "");
		public static LifeLink 不想告訴阿發 = new LifeLink(2, "不想告訴阿發", "不好ABB00001" + "!_!{\"flow_prefix\":\"abb\"}", "", "");	
		
		public static LifeLink 不好 = new LifeLink(2, "不好", "不好ABB00001", "", "");	
		public static LifeLink 我想算算解約金() {		
			String alt = ABBLink.ABB10008.S033_a.getAlt();
			return new LifeLink(2, "我想算算解約金", alt, "", "");
		}
		
		public static LifeLink 想投保其他新契約 = new LifeLink(2, "想投保其他新契約", "想投保其他新契約ABB00001_1_1" + "!_!{\"flow_prefix\":\"abb\"}", "", "");		
		public static LifeLink 無法負擔保費 = new LifeLink(2, "無法負擔保費", "無法負擔保費ABB00001_1_2" + "!_!{\"flow_prefix\":\"abb\"}", "", "");
		public static LifeLink 保單能不能繳別變更() {		
			String alt = LifeLink.叫你們經理出來.getAlt();
			return new LifeLink(2, "保單能不能繳別變更", alt, "", "");
		}
		public static LifeLink 保單能不能減額繳清() {		
			String alt = LifeLink.叫你們經理出來.getAlt();
			return new LifeLink(2, "保單能不能減額繳清", alt, "", "");
		}
		public static LifeLink 有資金需求 = new LifeLink(2, "有資金需求", "有資金需求ABB00001_1_3"+"!_!{\"flow_prefix\":\"abb\"}", "", "");
		
		public static LifeLink BA04_d = new LifeLink(2, "我想知道辦理方式", "我想知道辦理方式ABB00001", "", "");//我想知道辦理方式
		public static LifeLink 查看業務員() {		
			String alt = LifeLink.查所有業務員.getAlt();
			return new LifeLink(2, "查看業務員", alt, "", "");
		}
		
		public static LifeLink 看可借金額() {		
			String alt = ABBLink.ABB10007.看保單可借款金額.getAlt();
			return new LifeLink(2, "看可借金額", alt, "", "");
		}
		
		// 20191108 by Todd, 埋入flow_prefix代碼
		// public static LifeLink 查看詳情_服務據點 = new LifeLink(2, "查看詳情", "查看詳情ABB00001_2_1_1", "", "");
		// public static LifeLink 查看詳情_業務人員 = new LifeLink(2, "查看詳情", "查看詳情ABB00001_2_1_2", "", "");
		// public static LifeLink 查看詳情_線上 = new LifeLink(2, "查看詳情", "查看詳情ABB00001_2_1_3", "", "");
		public static LifeLink 查看詳情_服務據點 = new LifeLink(2, "查看詳情", "查看詳情ABB00001_2_1_1!_!{\"flow_prefix\":\"abb\"}", "", "");
		public static LifeLink 查看詳情_業務人員 = new LifeLink(2, "查看詳情", "查看詳情ABB00001_2_1_2!_!{\"flow_prefix\":\"abb\"}", "", "");
		public static LifeLink 查看詳情_線上 = new LifeLink(2, "查看詳情", "查看詳情ABB00001_2_1_3!_!{\"flow_prefix\":\"abb\"}", "", "");
		
		public static LifeLink 看其他方式 = new LifeLink(2, "看其他方式", "我想知道辦理方式ABB00001", "", "");
		public static LifeLink 申辦網路服務方式() {		
			String alt = LifeLink.網路服務資格辦理方式.getAlt();
			return new LifeLink(2, "申辦網路服務方式", alt, "", "");
		}
	}
	
	/**
	 * 前往會員網站辦理 S033
	 * ABB00008
	 * @author 劉冠廷
	 */
	public static class ABB10008 {

		public static LifeLink S033_a = new LifeLink(2, "看解約金額", "查詢保單解約金", "", "");
		
		public static LifeLink S033_b = new LifeLink(2, "前往會員網站辦理", "ABB10008_b", "", "");
		
	}
	
	/**
	 * 年金
	 * ABB10001
	 * @author Todd
	 */
	public static class ABB10001 {
		public static LifeLink 查詢年金領取狀況 = new LifeLink(15, "查詢年金領取狀況", "查詢年金領取狀況", "查詢年金給付紀錄", "");
		public static LifeLink 保單能不能領取年金() {		
			String alt = LifeLink.叫你們經理出來.getAlt();
			return new LifeLink(2, "保單能不能領取年金", alt, "", "");
		}
		//20181213
		public static LifeLink ABB10001_1_2	 = new LifeLink(2, "保單能不能領取年金", "ABB10001_1_2", "", "");
	}
	
	/**
	 * 滿期金
	 * ABB10009
	 * @author Todd
	 */
	public static class ABB10009 {
		public static LifeLink 查詢滿期金領取狀況 = new LifeLink(15, "查詢滿期金領取狀況", "查詢滿期金領取狀況", "滿期金給付紀錄", "");
		public static LifeLink 保單能不能領滿期金() {		
			String alt = LifeLink.叫你們經理出來.getAlt();
			return new LifeLink(2, "保單能不能領滿期金", alt, "", "");
		}
		//20181211
		public static LifeLink ABB10009_1_2	 = new LifeLink(15, "保單能不能領滿期金", "保單能不能領滿期金", "ABB10009_1_2保單能不能領滿期金", "");
	}
	
	// 20190111 by Todd, 常用功能牌卡
	/**
	 * 查詢年金
	 * ABB10014
	 * @author Todd
	 */
	public static class ABB10014 {
		public static LifeLink 查詢年金相關內容 = new LifeLink(15, "查詢年金相關內容", "查詢年金相關內容", "我想要查年金", "");
	}
	
	/**
	 * 查詢滿期金
	 * ABB10015
	 * @author Todd
	 */
	public static class ABB10015 {
		public static LifeLink 查詢滿期金相關內容 = new LifeLink(15, "查詢滿期金相關內容", "查詢滿期金相關內容", "我想要查滿期金", "");
	}
}


