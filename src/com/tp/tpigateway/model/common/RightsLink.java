package com.tp.tpigateway.model.common;

public class RightsLink extends LifeLink {

	public RightsLink(int action, String text, String alt, String reply, String url) {
		super(action, text, alt, reply, url);
	}

	/**
	 * 生日禮
	 * Birthday
	 * @author Todd
	 */
    public static class Birthday {
    	public static LifeLink 快來下載 = new LifeLink(15, "快來下載", "生日優禮", "生日優禮", "");
    }
    
    /**
     * 契約停效
     * TermPolicy
     * @author Todd
     */
    public static class TermPolicy {
        public static LifeLink 查看保單明細 = new LifeLink(15, "查看保單明細", "查詢停效保單明細", "查詢停效保單明細", "");
    }
    
    /**
     * 滿期金
     * Maturity
     * @author Todd
     */
    public static class Maturity {
        public static LifeLink 查看保單明細 = new LifeLink(15, "查看保單明細", "看未申請滿期金保單明細", "看未申請滿期金保單明細", "");
        public static LifeLink 申請滿期金的方式 = new LifeLink(15, "申請滿期金的方式", "申請滿期金的方式", "申請滿期金的方式", "");
    }
    
    /**
     * 年金
     * Annutiy
     * @author Todd
     */
    public static class Annutiy {
        public static LifeLink 查看保單明細 = new LifeLink(15, "查看保單明細", "看未申請年金保單明細", "看未申請年金保單明細", "");
        public static LifeLink 申請年金的方式 = new LifeLink(15, "申請年金的方式", "申請年金的方式", "申請年金的方式", "");
    }
    
    /**
     * 滿期金將到期
     * MaturityDue
     * @author Todd
     */
    public static class MaturityDue {
        public static LifeLink 查看保單明細 = new LifeLink(15, "查看保單明細", "看即將到期滿期金保單明細", "看即將到期滿期金保單明細", "");
        public static LifeLink 申請滿期金的方式 = new LifeLink(15, "申請滿期金的方式", "申請滿期金的方式", "申請滿期金的方式", "");
    }
    
    /**
     * 年金將到期
     * AnnutiyDue
     * @author Todd
     */
    public static class AnnutiyDue {
        public static LifeLink 查看保單明細 = new LifeLink(15, "查看保單明細", "看即將到期年金保單明細", "看即將到期年金保單明細", "");
        public static LifeLink 申請年金的方式 = new LifeLink(15, "申請年金的方式", "申請年金的方式", "申請年金的方式", "");
    }
}
