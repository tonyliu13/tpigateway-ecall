package com.tp.tpigateway.model.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "all_autocomplete")
public class AllAutoComplete {

	@Id
	@Column(name = "id", length = 10)
	private Integer id;
	
	@Column(name = "display_string")
	private String displayString;
	
	@Column(name = "hidden_string")
	private String hiddenString;
	
	@Column(name = "string_order", length = 10)
	private Integer stringOrder;
	
	public Integer getId () {
		return id;
	}
	public void setId (Integer id) {
		this.id = id;
	}
	public String getDisplayString () {
		return displayString;
	}
	public void setDisplayString (String displayString) {
		this.displayString = displayString;
	}
	public String getHiddenString () {
		return hiddenString;
	}
	public void setHiddenString (String hiddenString) {
		this.hiddenString = hiddenString;
	}
	public Integer getStringOrder () {
		return stringOrder;
	}
	public void setStringOrder (Integer stringOrder) {
		this.stringOrder = stringOrder;
	}	
}
