package com.tp.tpigateway.model.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.tp.tpigateway.util.DateUtil;

@Entity
@Table(name = "bot_tpi_cache", schema = "public")
public class BotTpiCache implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3953270435528495980L;
    
    @Id
    @Column(name = "cache_key")
    private String cacheKey;
    
    @Column(name = "cache_value")
    private String cacheValue;
    
    @Column(name = "cache_type")
    private String cacheType;
    
    @Column(name = "expired_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredTime;
    
    @Column(name = "insert_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertTime;
    
    @Column(name = "update_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;
    
    public BotTpiCache() { }
    
    public BotTpiCache(String cacheKey, String cacheValue, String cacheType, Date expiredTime) {
        this.cacheKey = cacheKey;
        this.cacheValue = cacheValue;
        this.cacheType = cacheType;
        this.expiredTime = expiredTime;
        this.insertTime = DateUtil.getNow();
        this.updateTime = DateUtil.getNow();
    }
    
    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public String getCacheValue() {
        return cacheValue;
    }

    public void setCacheValue(String cacheValue) {
        this.cacheValue = cacheValue;
    }
    
    public String getCacheType() {
        return cacheType;
    }

    public void setCacheType(String cacheType) {
        this.cacheType = cacheType;
    }

    public Date getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(Date expiredTime) {
        this.expiredTime = expiredTime;
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuffer sbf = new StringBuffer();
        sbf.append('{');
        sbf.append("cache_key:").append(this.cacheKey);
        if (this.cacheValue != null) {
            sbf.append(", ").append("cache_value:").append(this.cacheValue);
            sbf.append(", ").append("expired_time:").append(this.expiredTime);
        }
        sbf.append('}');
        return sbf.toString();
    }
}
