package com.tp.tpigateway.model.common;

import com.tp.tpigateway.util.DateUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "session_stage")
public class SessionStage {

    @Id
    private String id;

    @Column(name = "agent_id")
    private String agentId;

    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "session_str_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date sessionStrTime;

    @Column(name = "session_upd_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date sessionUpdTime;

    @Column(name = "chk_turn_to_real")
    private String chkTurnToReal;

    @Column(name = "chk_agent_stage")
    private String chkAgentStage;
    
    @Column(name = "chk_agent_pickup")
    private String chkAgentPickup;

	@Column(name = "chk_disconnect")
    private String chkDisconnect;

    @Column(name = "no_res_time")
    private Integer noResTime = 0;

    @Column(name = "chk_pass_bot")
    private String chkPassBot;

    @Column(name = "serious_ask")
    private String seriousAsk;

    @Column(name = "stage_score")
    private Integer stageScore = 0;

    @Column(name = "chk_sel_to_real")
    private String chkSelToReal;
    
    @Column(name = "chk_other_contact_way")
    private String chkOtherContactWay;
    
    @Column(name = "chk_in_survey")
    private String chkInSurvey;
    
    // 20190215 by todd, for session stage新增記錄滿意度調查分數以及開放題內容
    @Column(name = "bot_score")
    private String botScore;
    
    @Column(name = "agent_score")
    private String agentScore;
    
    @Column(name = "customer_comment")
    private String customerComment;

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Date getSessionStrTime() {
        return sessionStrTime;
    }

    public void setSessionStrTime(Date sessionStrTime) {
        this.sessionStrTime = sessionStrTime;
    }

    public Date getSessionUpdTime() {
        return sessionUpdTime;
    }

    public void setSessionUpdTime(Date sessionUpdTime) {
        this.sessionUpdTime = sessionUpdTime;
    }

    public String getChkTurnToReal() {
        return chkTurnToReal;
    }

    public void setChkTurnToReal(String chkTurnToReal) {
        this.chkTurnToReal = chkTurnToReal;
    }

    public String getChkAgentStage() {
        return chkAgentStage;
    }

    public void setChkAgentStage(String chkAgentStage) {
        this.chkAgentStage = chkAgentStage;
    }
    
    public String getChkAgentPickup() {
		return chkAgentPickup;
	}

	public void setChkAgentPickup(String chkAgentPickup) {
		this.chkAgentPickup = chkAgentPickup;
	}

    public String getChkDisconnect() {
        return chkDisconnect;
    }

    public void setChkDisconnect(String chkDisconnect) {
        this.chkDisconnect = chkDisconnect;
    }

    public Integer getNoResTime() {
        return noResTime;
    }

    public void setNoResTime(Integer noResTime) {
        this.noResTime = noResTime;
    }

    public String getChkPassBot() {
        return chkPassBot;
    }

    public void setChkPassBot(String chkPassBot) {
        this.chkPassBot = chkPassBot;
    }

    public String getSeriousAsk() {
        return seriousAsk;
    }

    // 2018/10/1 Todd，嚴重發言記錄以append方式留存
    /*public void setSeriousAsk(String seriousAsk) {
        this.seriousAsk = seriousAsk;
    }*/
    public void setSeriousAsk(String seriousAsk) {
    	if (this.seriousAsk == null) {
    		this.seriousAsk = "";
    	}
    	if (StringUtils.isBlank(seriousAsk)) {
    		return;
    	}
    	// 在舊seriousAsk已有資料時若需append新語句才加上分隔符號 |
    	if (!StringUtils.contains(this.seriousAsk + "|", seriousAsk + "|")) {
    		this.seriousAsk = StringUtils.substring((this.seriousAsk.length() > 0) ? this.seriousAsk + "|" + seriousAsk : seriousAsk, 0, 300);
    	}
    }

    public Integer getStageScore() {
        return stageScore;
    }

    public void setStageScore(Integer stageScore) {
        this.stageScore = stageScore;
    }

    public String getChkSelToReal() {
        return chkSelToReal;
    }

    public void setChkSelToReal(String chkSelToReal) {
        this.chkSelToReal = chkSelToReal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getChkOtherContactWay() {
		return chkOtherContactWay;
	}

	public void setChkOtherContactWay(String chkOtherContactWay) {
		this.chkOtherContactWay = chkOtherContactWay;
	}
	
	public String getChkInSurvey() {
		return chkInSurvey;
	}

	public void setChkInSurvey(String chkInSurvey) {
		this.chkInSurvey = chkInSurvey;
	}

	public String getBotScore() {
		return botScore;
	}

	public void setBotScore(String botScore) {
		this.botScore = botScore;
	}

	public String getAgentScore() {
		return agentScore;
	}

	public void setAgentScore(String agentScore) {
		this.agentScore = agentScore;
	}

	public String getCustomerComment() {
		return customerComment;
	}

	public void setCustomerComment(String customerComment) {
		this.customerComment = customerComment;
	}

}
