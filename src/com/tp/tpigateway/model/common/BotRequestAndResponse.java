package com.tp.tpigateway.model.common;

import java.util.ArrayList;
import java.util.List;

public class BotRequestAndResponse {
	
	private List<Intent> intentList;
	
	private List<Entity> entityList;
	
	private List<BotTpiCallLog> callLogList;
	
	private String requestContent;
	
	private String responseContent;
	
	public BotRequestAndResponse() {
		this.intentList = new ArrayList<Intent>();
		this.entityList = new ArrayList<Entity>();
		this.requestContent = "";
		this.responseContent = "";
		this.callLogList = new ArrayList<BotTpiCallLog>();
	}

	public List<Intent> getIntentList() {
		return intentList;
	}

	public void setIntentList(List<Intent> intentList) {
		this.intentList = intentList;
	}

	public List<Entity> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Entity> entityList) {
		this.entityList = entityList;
	}

	public String getResponseContent() {
		return responseContent;
	}

	public void setResponseContent(String responseContent) {
		this.responseContent = responseContent;
	}

	public String getRequestContent() {
		return requestContent;
	}

	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}

	public List<BotTpiCallLog> getCallLogList() {
		return callLogList;
	}

	public void setCallLogList(List<BotTpiCallLog> callLogList) {
		this.callLogList = callLogList;
	}
	

}
