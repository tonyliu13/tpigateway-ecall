package com.tp.tpigateway.model.common;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.tp.tpigateway.util.PropUtils;

/**
 * TPIGatewayResult
 */
@Component
@JsonInclude(Include.NON_NULL)
@SuppressWarnings("unchecked")
public class TPIGatewayResult implements Serializable {

	private static final long serialVersionUID = -1617208040603504135L;
	
	public static final String RETURNCODE_SUCCESS = PropUtils.RESULT_CODE_SUCCESS;
	public static final String RETURNDESC_SUCCESS = PropUtils.RESULT_DESCRIPTION_SUCCESS;
	public static final String RETURNCODE_FAILED = PropUtils.RESULT_CODE_FAIL;
	
	private String returnCode;				// 回傳代碼
	private String returnDesc;				// 結果描述
	private Object info;					// 結果資訊（回傳參數值）
	private String result;					// 結果狀態（Y:成功 N:失敗）
	private Object data;					// 回傳資料
	
	public TPIGatewayResult() { }
	
	public TPIGatewayResult(String code_, String description_, Object info_) {
		returnCode = code_;
		returnDesc = description_;
		info = info_;
	}
	
	public TPIGatewayResult(String code_, String description_, Object info_, String result_) {
		returnCode = code_;
		returnDesc = description_;
		info = info_;
		result = result_;
	}
	
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnDesc() {
		return returnDesc;
	}
	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc;
	}
	public Object getInfo() {
		return info;
	}
	public void setInfo(Object Info) {
		this.info = Info;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String Result) {
		this.result = Result;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * 重新組合輸出結果Map
	 * 
	 * @param breakUpInfo true:打散info中內容(若info是map)
	 * @return
	 */
	@JsonIgnore
	public Map<String, Object> getRegroupResult(boolean breakUpInfo) {
		
		Map<String, Object> mapResult = new LinkedHashMap<String, Object>();
		mapResult.put("ReturnCode", getReturnCode());
		mapResult.put("ReturnDesc", getReturnDesc());
		
		if (info != null) {
			if (breakUpInfo && info instanceof Map) {
				mapResult.putAll((Map<String,Object>) info);
			} else {
				mapResult.put("ReturnData", getInfo());
			}
		}
		
		if (data != null) {
			mapResult.put("DataInfo", data);
		}
		
		if (StringUtils.isNotBlank(getResult())) {
			mapResult.put("Result", getResult());
		}
		
		if (StringUtils.equals(PropUtils.RESULT_CODE_FAIL, getReturnCode())) {
			if (null == mapResult.get("ReturnData") || mapResult.get("ReturnData").toString().trim().length() == 0) {
				
				if (PropUtils.CALL_API_ERROR_SHOW_INFO) {
					mapResult.put("ReturnData", PropUtils.CALL_LIFE_API_ERROR + "(TPIGateway api error)");
				} else {
					mapResult.put("ReturnData", PropUtils.CALL_LIFE_API_ERROR);
				}
				
				mapResult.put("ReturnData", getInfo());
			}
		}
		
		return mapResult;
	}
	
}
