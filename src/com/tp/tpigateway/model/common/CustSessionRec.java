package com.tp.tpigateway.model.common;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cust_session_rec")
public class CustSessionRec {

    @Id
    private String id;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "session_time")
    private Integer sessionTime = 0;

    @Column(name = "date")
    private String date;

    @Column(name = "chk_turned_to_real")
    private String chkTurnedToReal;

    @Column(name = "chk_disconnected")
    private String chkDisconnected;

    @Column(name = "chk_sel_to_real")
    private String chkSelToReal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(Integer sessionTime) {
        this.sessionTime = sessionTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChkTurnedToReal() {
        return chkTurnedToReal;
    }

    public void setChkTurnedToReal(String chkTurnedToReal) {
        this.chkTurnedToReal = chkTurnedToReal;
    }

    public String getChkDisconnected() {
        return chkDisconnected;
    }

    public void setChkDisconnected(String chkDisconnected) {
        this.chkDisconnected = chkDisconnected;
    }

    public String getChkSelToReal() {
        return chkSelToReal;
    }

    public void setChkSelToReal(String chkSelToReal) {
        this.chkSelToReal = chkSelToReal;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("customerId", customerId)
                .append("sessionTime", sessionTime)
                .append("date", date)
                .append("chkTurnedToReal", chkTurnedToReal)
                .append("chkDisconnected", chkDisconnected)
                .append("chkSelToReal", chkSelToReal)
                .toString();
    }
}
