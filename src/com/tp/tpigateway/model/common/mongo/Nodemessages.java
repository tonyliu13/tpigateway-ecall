package com.tp.tpigateway.model.common.mongo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;

/*
{
    "_id" : ObjectId("5ab4ab88ae08f06877e30982"),
    "session" : ObjectId("5ab4ab89025ab15b1d9452ba"),
    "id" : "95885dd1.71eef",
    "info" : {
        "appId" : "f5d2bac1.1bd6c8",
        "kitt" : {
            "_session_id" : "default session",
            "_passed_enters" : [

            ],
            "_routerId" : "f458a7ee.384528"
        },
        "_path" : [
            "f21ee016.c9406",
            "f458a7ee.384528"
        ],
        "payload" : {
            "pair" : null,
            "customer_comment" : null,
            "rank_score_agent" : null,
            "rank_score_bot" : null,
            "agent_comment" : null,
            "comment_type" : null,
            "customer_id" : null,
            "customerData" : {
                "cookie_id" : "",
                "latitude" : "25.072947",
                "ip" : "88.252.199.118"
            },
            "agent_id" : null,
            "agent_engaging" : false,
            "intent" : null,
            "msg" : "%E6%88%91%E8%A6%81%E7%B9%B3%E4%BF%A1%E7%94%A8%E5%8D%A1%E8%B2%BB",
            "channel" : "ChatWeb",
            "query_id" : null,
            "session_id" : "tp-jxc5rq5wa-ss6zl0uz9",
            "customer_msg_id" : "681843026",
            "type" : "customerMsg"
        },
        "_msgid" : "95885dd1.71eef"
    },
    "type" : "input",
    "registered_at" : ISODate("2018-03-23T07:23:52.941+0000"),
    "__v" : NumberInt(0)
}
 */
@Document(collection = "nodemessages")
public class Nodemessages implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private ObjectId id;

    private ObjectId session;

    private Info info;

    private String type;

    @Field("registered_at")
    @JsonProperty(value = "registered_at")
    private Date registeredDateTime;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getSession() {
        return session;
    }

    public void setSession(ObjectId session) {
        this.session = session;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getRegisteredDateTime() {
        return registeredDateTime;
    }

    public void setRegisteredDateTime(Date registeredDateTime) {
        this.registeredDateTime = registeredDateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("session", session)
                .append("info", info)
                .append("type", type)
                .append("registeredDateTime", registeredDateTime)
                .toString();
    }

    //#######################################################################
    public static class Info {

        //DB格式不確定，有些是object有些是string
        private Object payload;

        public Object getPayload() {
            return payload;
        }

        public void setPayload(Payload payload) {
            this.payload = payload;
        }

        @Override
        public String toString() {
            return "Info{" +
                    "payload=" + payload +
                    '}';
        }
    }

    public static class Payload {
        private String pair;

        @Field("NLU")
        @JsonProperty(value = "NLU")
        private NLU nlu;

        @Field("rank_score_agent")
        @JsonProperty(value = "rank_score_agent")
        private String rankScoreAgent;

        @Field("rank_score_bot")
        @JsonProperty(value = "rank_score_bot")
        private String rankScoreBot;

        @Field("agent_comment")
        @JsonProperty(value = "agent_comment")
        private String agentComment;

        @Field("comment_type")
        @JsonProperty(value = "comment_type")
        private String commentType;

        @Field("customer_id")
        @JsonProperty(value = "customer_id")
        private String customerId;

        private CustomerData customerData;

        @Field("agent_id")
        @JsonProperty(value = "agent_id")
        private String agentId;

        @Field("agent_engaging")
        @JsonProperty(value = "agent_engaging")
        private Boolean agentEngaging;

        private String intent;

        private String msg;

        private String showMsg;
        
        private String showTime;

		private String channel;

        @Field("query_id")
        @JsonProperty(value = "query_id")
        private String queryId;

        @Field("session_id")
        @JsonProperty(value = "session_id")
        private String sessionId;

        @Field("customer_msg_id")
        @JsonProperty(value = "customer_msg_id")
        private String customerMsgId;

        private String type;

        public NLU getNlu() {
            return nlu;
        }

        public void setNlu(NLU nlu) {
            this.nlu = nlu;
        }

        public String getPair() {
            return pair;
        }

        public void setPair(String pair) {
            this.pair = pair;
        }

        public String getRankScoreAgent() {
            return rankScoreAgent;
        }

        public void setRankScoreAgent(String rankScoreAgent) {
            this.rankScoreAgent = rankScoreAgent;
        }

        public String getRankScoreBot() {
            return rankScoreBot;
        }

        public void setRankScoreBot(String rankScoreBot) {
            this.rankScoreBot = rankScoreBot;
        }

        public String getAgentComment() {
            return agentComment;
        }

        public void setAgentComment(String agentComment) {
            this.agentComment = agentComment;
        }

        public String getCommentType() {
            return commentType;
        }

        public void setCommentType(String commentType) {
            this.commentType = commentType;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public CustomerData getCustomerData() {
            return customerData;
        }

        public void setCustomerData(CustomerData customerData) {
            this.customerData = customerData;
        }

        public String getAgentId() {
            return agentId;
        }

        public void setAgentId(String agentId) {
            this.agentId = agentId;
        }

        public Boolean getAgentEngaging() {
            return agentEngaging;
        }

        public void setAgentEngaging(Boolean agentEngaging) {
            this.agentEngaging = agentEngaging;
        }

        public String getIntent() {
            return intent;
        }

        public void setIntent(String intent) {
            this.intent = intent;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getShowMsg() {
            return showMsg;
        }

        public void setShowMsg(String showMsg) {
            this.showMsg = showMsg;
        }
        
        public String getShowTime() {
			return showTime;
		}

		public void setShowTime(String showTime) {
			this.showTime = showTime;
		}

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getQueryId() {
            return queryId;
        }

        public void setQueryId(String queryId) {
            this.queryId = queryId;
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getCustomerMsgId() {
            return customerMsgId;
        }

        public void setCustomerMsgId(String customerMsgId) {
            this.customerMsgId = customerMsgId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("pair", pair)
                    .append("nlu", nlu)
                    .append("rankScoreAgent", rankScoreAgent)
                    .append("rankScoreBot", rankScoreBot)
                    .append("agentComment", agentComment)
                    .append("commentType", commentType)
                    .append("customerId", customerId)
                    .append("customerData", customerData)
                    .append("agentId", agentId)
                    .append("agentEngaging", agentEngaging)
                    .append("intent", intent)
                    .append("msg", msg)
                    .append("showMsg", showMsg)
                    .append("showTime", showTime)
                    .append("channel", channel)
                    .append("queryId", queryId)
                    .append("sessionId", sessionId)
                    .append("customerMsgId", customerMsgId)
                    .append("type", type)
                    .toString();
        }
    }

    public static class NLU {
        private String sentence;
        private double intent_score;
        private String intent;

        public String getSentence() {
            return sentence;
        }

        public void setSentence(String sentence) {
            this.sentence = sentence;
        }

        public double getIntent_score() {
            return intent_score;
        }

        public void setIntent_score(double intent_score) {
            this.intent_score = intent_score;
        }

        public String getIntent() {
            return intent;
        }

        public void setIntent(String intent) {
            this.intent = intent;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("sentence", sentence)
                    .append("intent_score", intent_score)
                    .append("intent", intent)
                    .toString();
        }
    }

    public static class CustomerData {
        @Field("cookie_id")
        @JsonProperty(value = "cookie_id")
        private String cookieId;
        private String latitude;
        private String ip;
        private String subChannel;

		public String getCookieId() {
            return cookieId;
        }

        public void setCookieId(String cookieId) {
            this.cookieId = cookieId;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }
        
        public String getSubChannel() {
			return subChannel;
		}

		public void setSubChannel(String subChannel) {
			this.subChannel = subChannel;
		}

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("cookieId", cookieId)
                    .append("latitude", latitude)
                    .append("ip", ip)
                    .append("subChannel", subChannel)
                    .toString();
        }
    }

}