package com.tp.tpigateway.model.common;

import java.util.HashMap;
import java.util.Map;

public class APIResult {
	private String respNodeID;
	
	private Map<String, Object> apiResult = new HashMap<String, Object>();

	public String getRespNodeID() {
		return respNodeID;
	}

	public void setRespNodID(String respNodeID) {
		this.respNodeID = respNodeID;
	}

	
	public Object getApiResult(String key) {
		return apiResult.get(key);
	}

	public void putApiResult(String key, Object value) {
		this.apiResult.put(key, value);
	}
	

}
