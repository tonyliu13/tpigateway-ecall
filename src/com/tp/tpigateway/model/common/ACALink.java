package com.tp.tpigateway.model.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.tp.tpigateway.util.PropUtils;

public class ACALink extends LifeLink {

    //我們提供的保費優惠
    public final static String DISCOUNT_URL = PropUtils.getProperty("cathaylife.discount.url");

    // A02
    public static LifeLink 我們提供的保費優惠 = new LifeLink(4, "我們提供的保費優惠", "", "", DISCOUNT_URL);

    // I5.1
    public static LifeLink 看保單優惠 = new LifeLink(15, "看保單優惠", "看保單優惠", "看我的保費折扣|{\"code\":\"ACA10001.1\"}", "");

    //看主約優惠(I5.a)
    public static LifeLink 看主約優惠 = new LifeLink(2, "看主約優惠", "看主約折扣優惠", "", "");
    // 20181015 by Todd, keep保單號碼
    public static LifeLink 看主約折扣優惠(String policyNo) {
		return LifeLink.buildLink(15, "看主約折扣優惠", "看主約折扣優惠", "看主約折扣優惠" + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}

    //看附約優惠(I5.b)
    public static LifeLink 看附約優惠 = new LifeLink(2, "看附約優惠", "看附約折扣優惠", "", "");
    // 20181015 by Todd, keep保單號碼
    public static LifeLink 看附約折扣優惠(String policyNo) {
		return LifeLink.buildLink(15, "看附約折扣優惠", "看附約折扣優惠", "看附約折扣優惠" + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}

    //查看詳情(S040.a)
    // 20181029 by Todd, 牌卡文字比對項目增加flow_prefix設定
    public static LifeLink 查看詳情_S040_A = new LifeLink(2, "查看詳情", "查看詳情ACA10004_3_1" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    
    // 前往官網(ACA10004_3_1_3)
    public static LifeLink 前往官網_S040_A_3 = new LifeLink(4, "前往官網", "前往官網", "", PropUtils.getProperty("cathaylife.ACA10004_3_1.website.url"), "");

    // 查詢附約資訊 S006
    // 20181015 by Todd, keep保單號碼
    public static LifeLink 查詢附約資訊(String policyNo) {
		return LifeLink.buildLink(2, "查詢附約資訊", "我想查詢保單目前的附約的狀況" + policyNo, "", "", "");
	}

    //清償墊繳方式(ACA00001.0.2)
    public static LifeLink 清償墊繳方式 = new LifeLink(2, "清償墊繳方式", "清償墊繳方式", "", "");
    public static LifeLink 看清償墊繳方式(String curr) {
    	Map<String, Object> jsonParameter = new HashMap<String, Object>();
    	jsonParameter.put("code", "ACA00001");
		if (StringUtils.isNotBlank(curr))
			jsonParameter.put("curr", curr);
    	return LifeLink.buildLink(15, "看清償墊繳方式", "看清償墊繳方式", "看墊繳金額|" + (new JSONObject(jsonParameter)).toString(), "", "");
    }
    // 20181015 by Todd, keep保單號碼
    public static LifeLink 看墊繳金額(String policyNo) {
		return LifeLink.buildLink(15, "看墊繳金額", "看墊繳金額", "看墊繳金額" + policyNo + "|{\"code\":\"ACA10003\"}", "", "");
	}

    // 20191126 by Todd, 還款、墊繳連結拆分
    // 貸款還款方式(ACA00002.1.2)
    // 20190528 by Todd, 牌卡文字比對項目增加flow_prefix設定
    // public static LifeLink 台幣保單= new LifeLink(2, "台幣保單", "台幣保單" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    // public static LifeLink 外幣保單 = new LifeLink(2, "外幣保單", "外幣保單" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 台幣保單ACA00002 = new LifeLink(15, "台幣保單", "台幣保單", "台幣保單ACA00002" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 外幣保單ACA00002 = new LifeLink(15, "外幣保單", "外幣保單", "外幣保單ACA00002" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    // 清償墊繳方式(ACA00001.1.2)
    public static LifeLink 台幣保單ACA00001 = new LifeLink(15, "台幣保單", "台幣保單", "台幣保單ACA00001" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 外幣保單ACA00001 = new LifeLink(15, "外幣保單", "外幣保單", "外幣保單ACA00001" + "!_!{\"flow_prefix\":\"aca\"}", "", "");

    // 清償墊繳方式(ACA00001.1.2.4)、貸款還款方式(ACA00002.1.2.4)
    public static LifeLink 外幣帳戶 = new LifeLink(2, "外幣帳戶", "國泰人壽專用外幣匯款帳號", "", "");

    // 貸款還款方式(ACA10002.1.1.1.3、ACA10002.1.1.2.3、ACA10002.1.1.3.2、ACA10002.1.1.4.3)
    // public static LifeLink 看其他方式_台幣保單= new LifeLink(1, "看其他方式", "台幣保單", "", "");
    public static LifeLink 看其他方式_台幣保單ACA00002 = new LifeLink(15, "看其他方式", "台幣保單", "台幣保單ACA00002" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    // 清償墊繳方式(ACA10001.1.1.1.3、ACA10001.1.1.2.3、ACA10001.1.1.3.3)
    public static LifeLink 看其他方式_台幣保單ACA00001 = new LifeLink(15, "看其他方式", "台幣保單", "台幣保單ACA00001" + "!_!{\"flow_prefix\":\"aca\"}", "", "");

    //貸款還款方式(ACA10001.1.1.2)
    // 20181029 by Todd, 牌卡文字比對項目增加flow_prefix設定
    // 20190917 by Felity,牌卡送出顯示文字調整 
    //public static LifeLink 查看詳情_A06_A_1 = new LifeLink(2, "查看詳情", "查看詳情-雲端繳款ACA00001_1_1_1" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 查看詳情_A06_A_1 = new LifeLink(15, "查看詳情", "透過雲端繳款辦理清償墊繳保險費", "查看詳情-雲端繳款ACA00001_1_1_1" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    //public static LifeLink 查看詳情_A06_A_2 = new LifeLink(2, "查看詳情", "查看詳情-繳款單ACA00001_1_1_2" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 查看詳情_A06_A_2 = new LifeLink(15, "查看詳情", "透過繳款單辦理清償墊繳保險費", "查看詳情-繳款單ACA00001_1_1_2" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    //public static LifeLink 查看詳情_A06_A_3 = new LifeLink(2, "查看詳情", "查看詳情-現金ACA00001_1_1_3" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 查看詳情_A06_A_3 = new LifeLink(15, "查看詳情", "透過服務據點辦理清償墊繳保險費", "查看詳情-現金ACA00001_1_1_3" + "!_!{\"flow_prefix\":\"aca\"}", "", "");

    //貸款還款方式(ACA00002.0.2)
    public static LifeLink 看還款方式= new LifeLink(1, "看還款方式", "查還款方式", "", "");
    public static LifeLink 看還款方式_BA07_1_1(String curr) {
    	Map<String, Object> jsonParameter = new HashMap<String, Object>();
    	jsonParameter.put("code", "ACA00002");
    	if (StringUtils.isNotBlank(curr))
			jsonParameter.put("curr", curr);
    	return LifeLink.buildLink(15, "看還款方式", "看還款方式", "看還款方式|" + (new JSONObject(jsonParameter)).toString(), "", "");
    }
    // 20181015 by Todd, keep保單號碼
    public static LifeLink 看還款金額(String policyNo) {
		return LifeLink.buildLink(15, "看還款金額", "看還款金額", "看還款金額" + policyNo + "|{\"code\":\"ABB10011\"}", "", "");
	}
    public static LifeLink 外幣專用戶 = new LifeLink(15, "外幣專用戶", "國泰人壽外幣帳戶", "國泰人壽外幣帳戶", "", "");

    //貸款還款方式(ACA00002.1.1.2)
    // 20181029 by Todd, 牌卡文字比對項目增加flow_prefix設定
    // 20190917 by Felity,牌卡送出顯示文字調整 
    //public static LifeLink 查看詳情_BA07_A_1 = new LifeLink(2, "查看詳情", "查看詳情-雲端繳款ACA00002_1_1_1" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    //public static LifeLink 查看詳情_BA07_A_2 = new LifeLink(2, "查看詳情", "查看詳情-繳款單ACA00002_1_1_2" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    //public static LifeLink 查看詳情_BA07_A_3 = new LifeLink(2, "查看詳情", "查看詳情-ATM借還款ACA00002_1_1_3" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    //public static LifeLink 查看詳情_BA07_A_4 = new LifeLink(2, "查看詳情", "查看詳情-現金ACA00002_1_1_4" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 查看詳情_BA07_A_1 = new LifeLink(15, "查看詳情", "透過雲端繳款辦理保單還款", "查看詳情-雲端繳款ACA00002_1_1_1" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 查看詳情_BA07_A_2 = new LifeLink(15, "查看詳情", "透過繳款單辦理保單還款", "查看詳情-繳款單ACA00002_1_1_2" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 查看詳情_BA07_A_3 = new LifeLink(15, "查看詳情", "透過ATM辦理保單還款", "查看詳情-ATM借還款ACA00002_1_1_3" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    public static LifeLink 查看詳情_BA07_A_4 = new LifeLink(15, "查看詳情", "透過服務據點辦理保單還款", "查看詳情-現金ACA00002_1_1_4" + "!_!{\"flow_prefix\":\"aca\"}", "", "");
    
    //貸款還款方式(ACA00002.1.1.3.2)
    public static LifeLink 如何申辦ATM借還款= new LifeLink(2, "如何申辦ATM借還款", "如何申辦ATM借還款", "", "");
    public static LifeLink 如何操作ATM還款 = new LifeLink(2, "如何操作ATM還款", "如何操作ATM還款", "", "");

    // 20181015 by Todd, keep保單號碼
    //查詢保單是否有催繳紀錄(ACA10002.3.1)
    public static LifeLink 保險費催繳紀錄(String policyNo) {
    	// 20181029 by Todd, 牌卡文字比對項目增加flow_prefix設定
		return LifeLink.buildLink(15, "保險費催繳紀錄", "保險費催繳紀錄", "保險費催繳紀錄" + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
    public static LifeLink 保單貸款催繳紀錄(String policyNo) {
    	// 20181029 by Todd, 牌卡文字比對項目增加flow_prefix設定
		return LifeLink.buildLink(15, "保單貸款催繳紀錄", "保單貸款催繳紀錄", "保單貸款催繳紀錄" + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}

    public static LifeLink ACA10003_保單墊繳怎麼辦 = new LifeLink(15, "保單墊繳怎麼辦", "保單墊繳如何清償", "保單墊繳如何清償", "");
    //S027(ACA10003.3.1)
    public static LifeLink 查最近催繳紀錄(String policyNo) {
		return LifeLink.buildLink(15, "查最近催繳紀錄", "查最近催繳紀錄", "查最近催繳紀錄" + policyNo, "", "");
	}
    public static LifeLink 查最近墊繳紀錄(String policyNo) {
    	// 20181029 by Todd, 牌卡文字比對項目增加flow_prefix設定
		return LifeLink.buildLink(15, "查最近墊繳紀錄", "查最近墊繳紀錄", "查最近墊繳紀錄" + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
    //S027(ACA10003.3.4)
    // 20190917 by Felity,牌卡送出顯示文字調整     
    //public static LifeLink 查看詳情_S027_a(String policyNo) {
	//	return LifeLink.buildLink(2, "查看詳情", "查看詳情-雲端繳款ACA10003_3_1," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "", "");
	//}
    //public static LifeLink 查看詳情_S027_b(String policyNo) {
	//	return LifeLink.buildLink(2, "查看詳情", "查看詳情-繳款單ACA10003_3_2," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "", "");
	//}
    //public static LifeLink 查看詳情_S027_c(String policyNo) {
	//	return LifeLink.buildLink(2, "查看詳情", "查看詳情-現金ACA10003_3_3," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "", "");
	//}
    public static LifeLink 查看詳情_S027_a(String policyNo) {
		return LifeLink.buildLink(15, "查看詳情", "透過雲端繳款辦理清償墊繳保險費", "查看詳情-雲端繳款ACA10003_3_1," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
    public static LifeLink 查看詳情_S027_b(String policyNo) {
		return LifeLink.buildLink(15, "查看詳情", "透過繳款單辦理清償墊繳保險費", "查看詳情-繳款單ACA10003_3_2," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
    public static LifeLink 查看詳情_S027_c(String policyNo) {
		return LifeLink.buildLink(15, "查看詳情", "透過服務據點辦理清償墊繳保險費", "查看詳情-現金ACA10003_3_3," + policyNo + "!_!{\"flow_prefix\":\"aca\"}", "", "");
	}
    
    
    public static LifeLink 查外幣帳戶 = new LifeLink(1, "查外幣帳戶", "國泰人壽專用外幣匯款帳號", "", "", "");
    public static LifeLink 看其他方式_S027 = new LifeLink(1, "看其他方式", "清償墊繳辦理方式", "", "");
    public static LifeLink 聯繫業務人員 = new LifeLink(1, "聯繫業務人員", "查保單業務員", "", "");
    public static LifeLink 查服務據點 = new LifeLink(1, "查服務據點", "國泰人壽服務據點?", "", "");

    //I7
    public static LifeLink 前往繳費 = new LifeLink(1, "前往繳費", "我可以用什麼方式繳保費?", "", "", "");

    public ACALink(int action, String text, String alt, String reply, String url) {
        super(action, text, alt, reply, url);
    }
    
    public static LifeLink 前往繳費(String polciyNo) {
        return LifeLink.buildLink(15, "前往繳費", "我可以用什麼方式繳保費?", "保單號碼"+polciyNo+"我可以用什麼方式繳保費?", "", "");
    }

}
