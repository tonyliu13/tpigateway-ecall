package com.tp.tpigateway.model.common;

public class AIALink extends LifeLink {

    //所有保單的保障內容(S007)
	public static LifeLink 我的保障內容有哪些 = new LifeLink(15, "我的保障內容有哪些", "我的保障內容有哪些", "我的保障內容有哪些", "", "");
    public static LifeLink 所有保單的保障內容 = new LifeLink(2, "所有保單的保障內容", "所有保單的保障內容_S007", "", "");

    //選保單看保障內容(S008)
    public static LifeLink 選保單看保障內容 = new LifeLink(2, "選保單看保障內容", "選保單看保障內容_S008", "", "");

    public static LifeLink 看其他保障_S007 = new LifeLink(2, "看其他保障項目", "所有保單的保障內容_S007", "", "", "");

    public static LifeLink 看其他保障_S008 = new LifeLink(2, "看其他保障項目", "選保單看保障內容_S008", "", "", "");

    // 20181015 by Todd, keep保單號碼
    public static LifeLink 看保障內容_S008(String policyNo) {
		return LifeLink.buildLink(2, "看保障內容", "選保單看保障內容_S008," + policyNo + "!_!{\"flow_prefix\":\"aia\"}", "", "", "");
	}

    //看保單內容(S003)
    public static LifeLink 確認我的保單狀況 = new LifeLink(15, "確認我的保單狀況", "確認我的保單狀況", "確認我的保單狀況", "");
    public static LifeLink 看保單內容 = new LifeLink(2, "看保單內容", "我想查詢保單目前契約狀況", "", "");
	public static LifeLink 查保單內容 = new LifeLink(2, "查保單內容", "我想查詢保單目前契約狀況", "", "");
    public static LifeLink 查主約資訊 = new LifeLink(2, "查主約資訊", "查詢保單目前契約狀況", "查詢保單目前契約狀況", "");
    //20181108優化
    public static LifeLink 查詢保單停效日 = new LifeLink(2, "查詢保單停效日", "查詢保單目前契約狀況", "", "");
    
    public static LifeLink 沒有了_滿意度調查 = new LifeLink(2, "沒有了(清空對話)", "滿意度調查", "", "", "");
    
    public AIALink(int action, String text, String alt, String reply, String url) {
        super(action, text, alt, reply, url);
    }

    public static class AIA10001 {
    	public static LifeLink 看保單績效(String policyNo) {
    		return LifeLink.buildLink(15, "看保單績效", "我要查詢保單績效", "我要查詢保單績效AIA10001.4.1," + policyNo + "!_!{\"flow_prefix\":\"aia\"}", "", "");
    	}
    	// 20190111 by Todd, 常用功能牌卡
    	public static LifeLink 推薦現售商品 = new LifeLink(15, "推薦現售商品", "推薦現售商品", "國泰有賣什麼商品", "");
    }
    
    // 20190111 by Todd, 常用功能牌卡
    /**
	 * 契約狀況
	 * AIA10005
	 * @author Todd
	 */
    public static class AIA10005 {
    	public static LifeLink 申請契約狀況一覽表 = new LifeLink(15, "申請契約狀況一覽表", "申請契約狀況一覽表", "我想申請契約狀況一覽表", "");
    }
    
    public static LifeLink 網路投保專區 = new LifeLink(11, "網路投保專區", "2627", "網路投保專區", "", "");
    // public static LifeLink 投保旅平險 = new LifeLink(11, "投保旅平險", "3618", "投保旅平險", "", "");
    public static LifeLink 投保旅平險 = new LifeLink(15, "投保旅平險", "投保旅平險", "投保旅平險", "", "");
}
