package com.tp.tpigateway.model.common;

import java.sql.Timestamp;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.tp.tpigateway.util.TPIStringUtil;

/**
 * TPIGateway呼叫API log紀錄
 */
@Entity
@Table(name = "bot_tpi_call_log")
public class BotTpiCallLog {

	public static enum LogType {
		QUERY, 
		INSERT, 
		UPDATE,
		DELETE
	}
	public BotTpiCallLog(){

	}

	public BotTpiCallLog(BotTpiCallLog botTpiCallLog) {
		this.cookieId = botTpiCallLog.getCookieId();
		this.chatId = botTpiCallLog.getChatId();
		this.apiUrl = botTpiCallLog.getApiUrl();
		this.type =  botTpiCallLog.getType();
		this.input = botTpiCallLog.getInput();
		this.output = botTpiCallLog.getOutput();
		this.result = botTpiCallLog.getResult();
		this.custId = botTpiCallLog.getCustId();
		this.cardNo = botTpiCallLog.getCardNo();
		this.account = botTpiCallLog.getAccount();
		this.insertTime = botTpiCallLog.getInsertTime();
		this.respStatus = botTpiCallLog.getRespStatus();
		this.costTime = botTpiCallLog.getCostTime();
	}

	@Transient
	public static final String COLUMN_NAME_LOG_ID = "log_id";
	@Transient 
	public static final String COLUMN_NAME_COOKIE_ID = "cookie_id";
	@Transient 
	public static final String COLUMN_NAME_IP = "ip";
	@Transient 
	public static final String COLUMN_NAME_CHAT_ID = "chat_id";
	@Transient 
	public static final String COLUMN_NAME_API_URL = "api_url";
	@Transient 
	public static final String COLUMN_NAME_TYPE = "type";
	@Transient 
	public static final String COLUMN_NAME_INPUT = "input";
	@Transient 
	public static final String COLUMN_NAME_OUTPUT = "output";
	@Transient 
	public static final String COLUMN_NAME_RESULT = "result";
	@Transient 
	public static final String COLUMN_NAME_CUST_ID = "cust_id";
	@Transient 
	public static final String COLUMN_NAME_CARD_NO = "card_no";
	@Transient 
	public static final String COLUMN_NAME_ACCOUNT = "account";
	@Transient 
	public static final String COLUMN_NAME_INSERT_TIME = "insert_time";
	@Transient 
	public static final String COLUMN_NAME_COST_TIME = "cost_time";
	@Transient 
	public static final String COLUMN_NAME_RESP_STATUS = "resp_status";
	@Transient 
	public static final String COLUMN_NAME_API_ID = "api_id";
	@Transient 
	public static final String COLUMN_NAME_TPI_HOST_NAME = "tpi_host_name";
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = COLUMN_NAME_LOG_ID)
	private Integer logId;
    
    @Column(name = COLUMN_NAME_COOKIE_ID, length = 4100)
	private String cookieId;
    
    @Column(name = COLUMN_NAME_IP, length = 50)
	private String ip;
    
	@Column(name = COLUMN_NAME_CHAT_ID, length = 50)
    private String chatId;
    
    @Column(name = COLUMN_NAME_API_URL, length = 4000)
	private String apiUrl;
    
    @Column(name = COLUMN_NAME_TYPE, length = 10)
	private String type;
    
    @Column(name = COLUMN_NAME_INPUT, length = 30000)
	private String input;
    
    @Column(name = COLUMN_NAME_OUTPUT, length = 30000)
	private String output;
    
    @Column(name = COLUMN_NAME_RESULT, length = 10)
	private String result;
    
    @Column(name = COLUMN_NAME_CUST_ID, length = 12)
	private String custId;
    
    @Column(name = COLUMN_NAME_CARD_NO, length = 20)
	private String cardNo;
    
    @Column(name = COLUMN_NAME_ACCOUNT, length = 20)
    private String account;
    
    @Column(name = COLUMN_NAME_INSERT_TIME)
	private Timestamp insertTime;
    
    @Column(name = COLUMN_NAME_COST_TIME)
    private Long costTime;

    @Column(name = COLUMN_NAME_RESP_STATUS, length = 5)
    private String respStatus;
    
    @Column(name = COLUMN_NAME_API_ID, length = 50)
    private String apiId;
    
    @Column(name = "tpi_host_name", length = 20)
    private String tpiHostName;

	public Integer getLogId() {
		return logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public String getCookieId() {
		return cookieId;
	}

	public void setCookieId(String cookieId) {
		this.cookieId = StringUtils.trim(cookieId);
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = StringUtils.trim(chatId);
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = StringUtils.trim(apiUrl);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = StringUtils.trim(type);
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = StringUtils.trim(input);
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = StringUtils.trim(output);
	}

	public String getResult() {
		return result;
	}
	
	public void setResult(boolean result) {
		if (result) {
			setResult("Success");
		} else {
			setResult("Fail");
		}
	}

	private void setResult(String result) {
		this.result = StringUtils.trim(result);
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = StringUtils.trim(custId);
	}
	
	/**
	 * 如果原本有值就不塞入
	 * @param custId
	 * @param isOverride
	 */
	public void setCustId(String custId, boolean isOverride) {
		if (isOverride) {
			setCustId(custId);
		} else {
			if (StringUtils.isEmpty(custId)) {
				setCustId(custId);
			}
		}
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = TPIStringUtil.cutString(StringUtils.trim(cardNo), 20);
	}
	
	public String getAccount() {
		return account;
	}
	
	public void setAccount(String account) {
		this.account = TPIStringUtil.cutString(StringUtils.trim(account), 20);
	}

	public Timestamp getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Timestamp insertTime) {
		this.insertTime = insertTime;
	}

	public Long getCostTime() {
		return costTime;
	}

	public void setCostTime(Long costTime) {
		this.costTime = costTime;
	}

	public String getRespStatus() {
		return respStatus;
	}

	public void setRespStatus(String respStatus) {
		this.respStatus = StringUtils.trim(respStatus);
	}
	
	public String getApiId() {
		return apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}
	
	public String getTpiHostName() {
		return tpiHostName;
	}

	public void setTpiHostName(String tpiHostName) {
		this.tpiHostName = tpiHostName;
	}

	@SuppressWarnings("rawtypes")
	public void insertMap(Map apiLogMap) {
		setType(MapUtils.getString(apiLogMap, COLUMN_NAME_TYPE, ""));
		setAccount(MapUtils.getString(apiLogMap, COLUMN_NAME_ACCOUNT, ""));
		setCardNo(MapUtils.getString(apiLogMap, COLUMN_NAME_CARD_NO, ""));
		setCustId(MapUtils.getString(apiLogMap, COLUMN_NAME_CUST_ID, ""), false);
	}
}
