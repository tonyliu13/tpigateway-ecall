package com.tp.tpigateway.model.common.monitor;

public class SessionLightData {

    public enum Light {
        RED, YELLOW, GREEN
    }

    private int redLight;
    private int yellowLight;
    private int greenLight;

    public SessionLightData() {
        this.redLight = 0;
        this.yellowLight = 0;
        this.greenLight = 0;
    }

    public void countLight(int stageScore) {
        Light light = getLight(stageScore);
        switch (light) {
            case GREEN:
                this.greenLight++;
                break;
            case YELLOW:
                this.yellowLight++;
                break;
            case RED:
                this.redLight++;
                break;
            default:
                break;
        }
    }

    public static Light getLight(int stageScore) {
        if (stageScore < 50) {
            return Light.GREEN;
        } else if (stageScore < 80 && stageScore >= 50) {
            return Light.YELLOW;
        } else {
            return Light.RED;
        }
    }

    public int getRedLight() {
        return redLight;
    }

    public void setRedLight(int redLight) {
        this.redLight = redLight;
    }

    public int getYellowLight() {
        return yellowLight;
    }

    public void setYellowLight(int yellowLight) {
        this.yellowLight = yellowLight;
    }

    public int getGreenLight() {
        return greenLight;
    }

    public void setGreenLight(int greenLight) {
        this.greenLight = greenLight;
    }
}