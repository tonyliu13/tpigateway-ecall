package com.tp.tpigateway.model.common.monitor;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class MonitorData {

    private String sessionId;
    private String customerId;
    private Long startDateTime;
    private String channel;
    private String subChannel;
	private Boolean isAppeal;
    private List<String> intent;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Long getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Long startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
    
    public String getSubChannel() {
		return subChannel;
	}

	public void setSubChannel(String subChannel) {
		this.subChannel = subChannel;
	}

    public Boolean getAppeal() {
        return isAppeal;
    }

    public void setAppeal(Boolean appeal) {
        isAppeal = appeal;
    }

    public List<String> getIntent() {
        return intent;
    }

    public void setIntent(List<String> intent) {
        this.intent = intent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("sessionId", sessionId)
                .append("customerId", customerId)
                .append("startDateTime", startDateTime)
                .append("channel", channel)
                .append("subChannel", subChannel)
                .append("isAppeal", isAppeal)
                .append("intent", intent)
                .toString();
    }
}
