package com.tp.tpigateway.model.common.monitor;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

public class SessionStatusData {

    //根據狀態分數判斷燈號
    private String sessionLight;

    private String sessionId;

    private String customerId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date sessionStrTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date sessionUpdTime;

    private String chkTurnToReal;

    private String chkAgentStage;

    private String chkSelToReal;

    private String chkDisconnect;

    private Integer noResTime;

    private String chkPassBot;

    private String seriousAsk;

    private Integer stageScore;

    //若customer_id有值時提供查詢日當日結果
    private Integer sessionTime;

    //若customer_id有值時提供查詢日當日結果
    private String chkTurnedToReal;

    //若customer_id有值時提供查詢日當日結果
    private String chkDisconnected;

    public String getSessionLight() {
        return sessionLight;
    }

    public SessionStatusData setSessionLight(String sessionLight) {
        this.sessionLight = sessionLight;
        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public SessionStatusData setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public String getCustomerId() {
        return customerId;
    }

    public SessionStatusData setCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public Date getSessionStrTime() {
        return sessionStrTime;
    }

    public SessionStatusData setSessionStrTime(Date sessionStrTime) {
        this.sessionStrTime = sessionStrTime;
        return this;
    }

    public Date getSessionUpdTime() {
        return sessionUpdTime;
    }

    public SessionStatusData setSessionUpdTime(Date sessionUpdTime) {
        this.sessionUpdTime = sessionUpdTime;
        return this;
    }

    public String getChkTurnToReal() {
        return chkTurnToReal;
    }

    public SessionStatusData setChkTurnToReal(String chkTurnToReal) {
        this.chkTurnToReal = chkTurnToReal;
        return this;
    }

    public String getChkAgentStage() {
        return chkAgentStage;
    }

    public SessionStatusData setChkAgentStage(String chkAgentStage) {
        this.chkAgentStage = chkAgentStage;
        return this;
    }

    public String getChkSelToReal() {
        return chkSelToReal;
    }

    public SessionStatusData setChkSelToReal(String chkSelToReal) {
        this.chkSelToReal = chkSelToReal;
        return this;
    }

    public String getChkDisconnect() {
        return chkDisconnect;
    }

    public SessionStatusData setChkDisconnect(String chkDisconnect) {
        this.chkDisconnect = chkDisconnect;
        return this;
    }

    public Integer getNoResTime() {
        return noResTime;
    }

    public SessionStatusData setNoResTime(Integer noResTime) {
        this.noResTime = noResTime;
        return this;
    }

    public String getChkPassBot() {
        return chkPassBot;
    }

    public SessionStatusData setChkPassBot(String chkPassBot) {
        this.chkPassBot = chkPassBot;
        return this;
    }

    public String getSeriousAsk() {
        return seriousAsk;
    }

    public SessionStatusData setSeriousAsk(String seriousAsk) {
        this.seriousAsk = seriousAsk;
        return this;
    }

    public Integer getStageScore() {
        return stageScore;
    }

    public SessionStatusData setStageScore(Integer stageScore) {
        this.stageScore = stageScore;
        return this;
    }

    public Integer getSessionTime() {
        return sessionTime;
    }

    public SessionStatusData setSessionTime(Integer sessionTime) {
        this.sessionTime = sessionTime;
        return this;
    }

    public String getChkTurnedToReal() {
        return chkTurnedToReal;
    }

    public SessionStatusData setChkTurnedToReal(String chkTurnedToReal) {
        this.chkTurnedToReal = chkTurnedToReal;
        return this;
    }

    public String getChkDisconnected() {
        return chkDisconnected;
    }

    public SessionStatusData setChkDisconnected(String chkDisconnected) {
        this.chkDisconnected = chkDisconnected;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("sessionLight", sessionLight)
                .append("sessionId", sessionId)
                .append("customerId", customerId)
                .append("sessionStrTime", sessionStrTime)
                .append("sessionUpdTime", sessionUpdTime)
                .append("chkTurnToReal", chkTurnToReal)
                .append("chkAgentStage", chkAgentStage)
                .append("chkSelToReal", chkSelToReal)
                .append("chkDisconnect", chkDisconnect)
                .append("noResTime", noResTime)
                .append("chkPassBot", chkPassBot)
                .append("seriousAsk", seriousAsk)
                .append("stageScore", stageScore)
                .append("sessionTime", sessionTime)
                .append("chkTurnedToReal", chkTurnedToReal)
                .append("chkDisconnected", chkDisconnected)
                .toString();
    }
}