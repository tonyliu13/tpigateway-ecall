package com.tp.tpigateway.model.common.monitor;

public class CacheData {

    private String key;
    private String value;
    private int expiredTime;
    private String type;
    
    public CacheData() { }
    
    public CacheData(String key, String value, int expiredTime, String type) {
        this.key = key;
        this.value = value;
        this.expiredTime = expiredTime;
        this.type = type;
    }
    
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public int getExpiredTime() {
        return expiredTime;
    }
    public void setExpiredTime(int expiredTime) {
        this.expiredTime = expiredTime;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
