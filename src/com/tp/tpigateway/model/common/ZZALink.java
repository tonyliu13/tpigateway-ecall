package com.tp.tpigateway.model.common;

import com.tp.tpigateway.util.PropUtils;

public class ZZALink extends LifeLink {

	// 20190327 by Todd, 腳本優化S042
	// S042(ZZA10001):業務人員更換申請網址
	public final static String ZZA10001_1_1_WEBSITE_URL = PropUtils.getProperty("cathaylife.ZZA10001_1_1.website.url");
		
    //貸款還款方式(ACA00002.1.1.3.2)
//    public static LifeLink 如何申辦ATM借還款= new LifeLink(2, "如何申辦ATM借還款", "如何申辦ATM借還款", "", "");
//    public static LifeLink 如何操作ATM還款 = new LifeLink(2, "如何操作ATM還款", "如何操作ATM還款", "", "");

    public ZZALink(int action, String text, String alt, String reply, String url) {
        super(action, text, alt, reply, url);
    }

    public static LifeLink 沒有了_滿意度調查 = new LifeLink(15, "沒有了(清空對話)", "我沒有其他問題了", "滿意度調查", "", "");
    
	public static LifeLink 立即前往_國泰人壽官網 = new LifeLink(4, "立即前往", "", "", PropUtils.CUSTOMER_URL);
    
    public static LifeLink 轉真人文字客服 = new LifeLink(2, "轉真人文字客服", "轉真人文字客服", "", "", "");
    public static LifeLink LIVE_TEXT_CHAT = new LifeLink(2, "Live chat", "ZZA00005EngServerS050|{\"IS_ENG\":true}!_!{\"flow_prefix\":\"zza\"}", "", "", ""); //轉真人文字客服
    
    public static LifeLink 立即撥號 = new LifeLink(12, "立即撥號", CUSTOMER_SERVICE_PUHOE, "", "", "");
    public static LifeLink CALL_US = new LifeLink(12, "Call us", CUSTOMER_SERVICE_PUHOE, "", "", ""); //立即撥號
    
    // 20190524 by Todd, 投保證明PDF牌卡數若可為動態多張，暫時改用不以DB設定的舊寫法
    public static LifeLink 轉接真人客服 = new LifeLink(15, "轉接真人客服", "轉真人客服", "轉真人客服", "", "");
	//The Offshore Insurance Unit (OIU) products //保險OIU單元
	public static LifeLink OIU = new LifeLink(11, "See more", "1005", "Offshore Insurance Unit", "", "");
	//The travel insurance products //FAQ 旅綜險產品
	public static LifeLink travelInsProducts = new LifeLink(11, "See more", "1010", "travel accident insurance", "", "");
	//Contact us //英文真人客服
	public static LifeLink CHAT_SERVICE = new LifeLink(6, "See more", "ZZA00004A10|!_!{\"flow_prefix\":\"zza\"}", "Contact us", "", "");
    
    public static LifeLink 查看業務員() {		
    	String alt = LifeLink.查所有業務員.getAlt();
    	return new LifeLink(2, "查看業務員", alt, "", "");
    }
    
    /**
	 * 更換業務員
	 * ZZA10001 (S042)
	 * @author Todd
	 */
	public static class ZZA10001 {
		
		public static LifeLink 更換業務員 = new LifeLink(2, "更換業務員", "更換業務員ZZA10001_1!_!{\"flow_prefix\":\"zza\"}", "", "");
		
		public static LifeLink 前往會員網站 = new LifeLink(4, "前往會員網站", "前往會員網站", "", ZZA10001_1_1_WEBSITE_URL, "");
		
	}
}
