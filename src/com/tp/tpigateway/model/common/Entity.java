package com.tp.tpigateway.model.common;

import java.math.BigDecimal;

public class Entity {
	private String name;
	private BigDecimal score; // -------------------

	public Entity() {
		this.name = "";
		this.score = BigDecimal.ZERO;
	}

	public Entity(String name, BigDecimal score) {
		this.name = name;
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}
}
