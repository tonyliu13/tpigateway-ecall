package com.tp.tpigateway.model.common;

import java.sql.Timestamp;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * TPIGateway呼叫API chatflow 走向代碼對照
 */
@Entity
@Table(name = "bot_chatflow_mapping")
public class BotChatflowMapping {

	public static enum LogType {
		QUERY, 
		INSERT, 
		UPDATE,
		DELETE
	}
	
	public BotChatflowMapping(){

	}

	public BotChatflowMapping(Map<String, String> chatflowMap) {
		this.mappingId = Integer.getInteger(chatflowMap.get(COLUMN_NAME_MAPPING_ID));
		this.chatflowId = chatflowMap.get(COLUMN_NAME_CHATFLOW_ID);
		this.chatflowName = chatflowMap.get(COLUMN_NAME_CHATFLOW_NAME);
		this.comments = chatflowMap.get(COLUMN_NAME_COMMENTS);
	}
	
	public BotChatflowMapping(BotChatflowMapping chatflowMap) {
		this.mappingId = chatflowMap.getMappingId();
		this.chatflowId = chatflowMap.getChatflowId();
		this.chatflowName = chatflowMap.getChatflowName();
		this.comments = chatflowMap.getComments();
	}

	@Transient
	public static final String COLUMN_NAME_MAPPING_ID = "mapping_id";
	@Transient 
	public static final String COLUMN_NAME_CHATFLOW_ID = "chatflow_id";
	@Transient 
	public static final String COLUMN_NAME_CHATFLOW_NAME = "chatflow_name";
	@Transient 
	public static final String COLUMN_NAME_COMMENTS = "comments";
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = COLUMN_NAME_MAPPING_ID)
	private Integer mappingId;
    
    @Column(name = COLUMN_NAME_CHATFLOW_ID, length = 20)
	private String chatflowId;
    
    @Column(name = COLUMN_NAME_CHATFLOW_NAME, length = 20)
	private String chatflowName;
    
	@Column(name = COLUMN_NAME_COMMENTS, length = 4000)
    private String comments;
    
	public Integer getMappingId() {
		return mappingId;
	}

	public void setMappingId(Integer mappingId) {
		this.mappingId = mappingId;
	}

	public String getChatflowId() {
		return chatflowId;
	}

	public void setChatflowId(String chatflowId) {
		this.chatflowId = chatflowId;
	}

	public String getChatflowName() {
		return chatflowName;
	}

	public void setChatflowName(String chatflowName) {
		this.chatflowName = chatflowName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
