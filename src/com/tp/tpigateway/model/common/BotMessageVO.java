package com.tp.tpigateway.model.common;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.tp.tpigateway.util.ApiLogUtils;
import com.tp.tpigateway.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class BotMessageVO {

	/** 200 */
	public static final String CWIDTH_200 = "segment secard";
	/** 250 */
	public static final String CWIDTH_250 = "segment";

	public static final String IMG_PDF = "card13.png";

	public static final String IMG_GENERIC = "img-credbank01";

	public static final String IMG_LIFE = "card03.png";

	public static final String IMG_CC = "img-creditcard";
	public static final String IMG_CB = "img-credbank";
	public static final String IMG_CB00 = "img-credbank00";
	public static final String IMG_CB00_GENERIC = "img-credbank00 generic";
	public static final String IMG_CB03 = "img-credbank03";
	public static final String IMG_CB03_ROUND = "img-credbank03 segment-round";
	public static final String IMG_CB04 = "img-credbank04";
	public static final String IMG_CB06 = "img-credbank06";
	public static final String IMG_EDM = "img-edm";

	public static final String CTYPE_CAROUSEL = "carousel";
	public static final String CTYPE_GENERIC = "generic";
	public static final String CTYPE_CARD = "card";

	// 2個選項，字數<=9(以中字字算1個，英數2個字元算1個字)
	public static final String LTYPE_01 = "select-tag01";
	// 3個選項，字數<=5(以中字字算1個，英數2個字元算1個字)
	public static final String LTYPE_02 = "select-tag02";
	// 4個選項，字數<=9(以中字字算1個，英數2個字元算1個字)
	public static final String LTYPE_03 = "select-tag03";
	// 其它
	public static final String LTYPE_04 = "select-tag04";
	// 其它
	public static final String LTYPE_05 = "select-tag04 search_all";
	// 自動完成
	public static final String LTYPE_AUTO_COMPLETE = "select-tag04 search_all";
	//打招呼
	public static final String HELLO_PIC = "sticker_hello.gif";
	//打招呼
	public static final String BYE_PIC = "sticker_bye.gif";
	//讚
	public static final String CONGRATS_PIC = "sticker_praise.gif";
	//灑花
	public static final String HAPPY_PIC = "sticker_hurrah.gif";
	//感謝
	public static final String THANKS_PIC = "sticker_thanks.gif";
	//OK!
	public static final String OK_PIC = "sticker_ok.gif";
	//震驚
	public static final String SHOCK_PIC = "sticker_shock.gif";
	//崩潰(操作中任務失敗)
	public static final String FAIL_PIC = "sticker_crumble.gif";
	//尷尬
	public static final String EMBARRASSED_PIC = "sticker_embarrassed.gif";
	//道歉
	public static final String APOLOGIZE_PIC = "sticker_sorry.gif";
	//精明(推眼鏡)
	public static final String CLEVER_PIC = "sticker_glasses.gif";
	//竊笑
	public static final String CHUCKLE_PIC = "sticker_hee.gif";
	//思考中
	public static final String THINKING_PIC = "sticker_thinking.gif";
	//探頭出來(再次呼叫)
	public static final String HI_PIC = "sticker_recall.gif";
	//掏文件(查找保單資訊)
	public static final String TAKE_DOC_PIC = "sticker_papers.gif";


	// level 1
	Map<String, Object> botMessage = new LinkedHashMap<String, Object>();
	String channel;
	String source;
	String role;
	String from = "bot";
	String displayChange = "Y";
	List<Map<String, Object>> content;

	public Map<String, Object> getVO() {
		botMessage.put("from", from);
		botMessage.put("displayChange", displayChange);
		botMessage.put("node_Id", ApiLogUtils.getNodeID());
		botMessage.put("showTime", DateUtil.formatDate(new Date(), DateUtil.PATTERN_DASH_DATE_TIME));
		return botMessage;
	}

	public String getChannel() {
		return MapUtils.getString(botMessage, "channel");
	}

	public void setChannel(String channel) {
		botMessage.put("channel", channel);
	}

	public String getSource() {
		return MapUtils.getString(botMessage, "source");
	}

	public void setSource(String source) {
		botMessage.put("source", source);
	}

	public String getRole() {
		return MapUtils.getString(botMessage, "role");
	}

	public void setRole(String role) {
		botMessage.put("role", role);
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	
	public String setDisplayChange() {
		return displayChange;
	}

	public void setDisplayChange(String displayChange) {
		this.displayChange = displayChange;
	}

	public List<Map<String, Object>> getContent() {
		return (List<Map<String, Object>>) botMessage.get("content");
	}

	public void setContent(List<Map<String, Object>> content) {
		botMessage.put("content", content);
	}

	// level 2 for content
	public class ContentItem {

		Map<String, Object> content = new LinkedHashMap<String, Object>();

		public Map<String, Object> getContent() {
			return content;
		}
        public String getLoginType() {
            return MapUtils.getString(content, "loginType");
        }
        public void setLoginType(String loginType) {
            content.put("loginType", loginType);
        }
		public Integer getType() {
			return MapUtils.getInteger(content, "type");
		}
		public void setType(Integer type) {
			content.put("type", type);
		}
		public String getText() {
			return MapUtils.getString(content, "text");
		}
		public void setText(String text) {
			content.put("text", text);
		}
		public String getCType() {
			return MapUtils.getString(content, "cType");
		}
		public void setCType(String cType) {
			content.put("cType", cType);
		}
		public List<Map<String, Object>> getCards() {
			return (List<Map<String, Object>>) content.get("cards");
		}
		public void setCards(List<Map<String, Object>> cards) {
			content.put("cards", cards);
		}
		public Integer getAction() {
			return MapUtils.getInteger(content, "action");
		}
		public void setAction(Integer action) {
			content.put("action", action);
		}
		public String getLType() {
			return MapUtils.getString(content, "lType");
		}
		public void setLType(String lType) {
			content.put("lType", lType);
		}
		public List<Map<String, Object>> getLinkList() {
			return (List<Map<String, Object>>) content.get("linkList");
		}
		public void setLinkList(List<Map<String, Object>> linkList) {
			content.put("linkList", linkList);
		}
		public String getUrl() {
			return MapUtils.getString(content, "url");
		}
		public void setUrl(String url) {
			content.put("url", url);
		}
		public String getImage() {
			return MapUtils.getString(content, "image");
		}
		public void setImage(String image) {
			content.put("image", image);
		}
		public String getImageUrl() {
			return MapUtils.getString(content, "imageUrl");
		}
		public void setImageUrl(String imageUrl) {
			content.put("imageUrl", imageUrl);
		}
		public List<Map<String, Object>> getDataList() {
			return (List<Map<String, Object>>) content.get("dataList");
		}
		public void setDataList(List<Map<String, Object>> dataList) {
			content.put("dataList", dataList);
		}
		public List<Map<String, Object>> getMoneyCards() {
			return (List<Map<String, Object>>) content.get("moneyCards");
		}
		public void setMoneyCards(List<Map<String, Object>> moneyCards) {
			content.put("moneyCards", moneyCards);
		}
		public List<Map<String, Object>> getDateCards() {
			return (List<Map<String, Object>>) content.get("dateCards");
		}
		public void setDateCards(List<Map<String, Object>> dateCards) {
			content.put("dateCards", dateCards);
		}
		public List<Map<String, Object>> getCountdownCards() {
			return (List<Map<String, Object>>) content.get("countdownCards");
		}
		public void setCountdownCards(List<Map<String, Object>> countdownCards) {
			content.put("countdownCards", countdownCards);
		}
		public String getAnnoType() {
			return MapUtils.getString(content, "annoType");
		}
		public void setAnnoType(String annoType) {
			content.put("annoType", annoType);
		}
		public String getAnnoTitle() {
			return MapUtils.getString(content, "annoTitle");
		}
		public void setAnnoTitle(String annoTitle) {
			content.put("annoTitle", annoTitle);
		}

		public String getAnnoText() {
			return MapUtils.getString(content, "annoText");
		}
		public void setAnnoText(String annoText) {
			content.put("annoText", annoText);
		}
		public String getWidget() {
			return MapUtils.getString(content, "widget");
		}
		public void setWidget(String widget) {
			content.put("widget", widget);
		}
		public boolean isMultiple(String widget) {
			return MapUtils.getBoolean(content, "isMultiple");
		}
		public void setMultiple(boolean isMultiple) {
			content.put("isMultiple", isMultiple);
		}
		public List<Map<String, Object>> getLVDataList() {
			return (List<Map<String, Object>>) content.get("lvDataList");
		}
		public void setLVDataList(List<Map<String, Object>> lvDataList) {
			content.put("lvDataList", lvDataList);
		}
		public String getLVTitle() {
			return MapUtils.getString(content, "lvTitle");
		}
		public void setLVTitle(String title) {
			content.put("lvTitle", title);
		}
		public String getLVType() {
			return MapUtils.getString(content, "lvType");
		}
		public void setLVType(String lvType) {
			content.put("lvType", lvType);
		}
	}

	// level 3 for lvDataList
	public class LVDataItem {
		Map<String, Object> lvData = new LinkedHashMap<String, Object>();

		public Map<String, Object> getLVData() {
			return lvData;
		}

		public String getProdName() {
			return MapUtils.getString(lvData, "prodName");
		}
		public void setProdName(String prodName) {
			lvData.put("prodName", prodName);
		}
		public String getRoleName() {
			return MapUtils.getString(lvData, "roleName");
		}
		public void setRoleName(String roleName) {
			lvData.put("roleName", roleName);
		}
		public String getRdId() {
			return MapUtils.getString(lvData, "rdId");
		}
		public void setRdId(String rdId) {
			lvData.put("rdId", rdId);
		}
		public String getID() {
			return MapUtils.getString(lvData, "id");
		}
		public void setID(String id) {
			lvData.put("id", id);
		}
		public String getFaceAmt() {
			return MapUtils.getString(lvData, "faceAmt");
		}
		public void setFaceAmt(String faceAmt) {
			lvData.put("faceAmt", faceAmt);
		}
		public String getAftRdAmt() {
			return MapUtils.getString(lvData, "aftRdAmt");
		}
		public void setAftRdAmt(String aftRdAmt) {
			lvData.put("aftRdAmt", aftRdAmt);
		}
	}

	// level 3 for dataList
	public class DataListItem {

		Map<String, Object> data = new LinkedHashMap<String, Object>();

		public Map<String, Object> getData() {
			return data;
		}

		public Integer getDlAction() {
			return MapUtils.getInteger(data, "dlAction");
		}
		public void setDlAction(Integer dlAction) {
			data.put("dlAction", dlAction);
		}
		public String getDlAlt() {
			return MapUtils.getString(data, "dlAlt");
		}
		public void setDlAlt(String dlAlt) {
			data.put("dlAlt", dlAlt);
		}
		public String getDlText() {
			return MapUtils.getString(data, "dlText");
		}
		public void setDlText(String dlText) {
			data.put("dlText", dlText);
		}
		public Map<String, Object> getPData() {
			return (Map<String, Object>) data.get("pData");
		}
		public void setPData(Map<String, Object> pData) {
			data.put("pData", pData);
		}
	}

	// level 4 for pData
	public class PDataItem {
		Map<String, Object> pData = new LinkedHashMap<String, Object>();

		public Map<String, Object> getpData() {
			return pData;
		}

		public String getPolicyNo() {
			return MapUtils.getString(pData, "policyNo");
		}
		public void setPolicyNo(String policyNo) {
			pData.put("policyNo", policyNo);
		}
		public String getProdName() {
			return MapUtils.getString(pData, "prodName");
		}
		public void setProdName(String prodName) {
			pData.put("prodName", prodName);
		}
		public String getApcNm() {
			return MapUtils.getString(pData, "apcNm");
		}
		public void setApcNm(String apcNm) {
			pData.put("apcNm", apcNm);
		}
		public String getInsdNm() {
			return MapUtils.getString(pData, "insdNm");
		}
		public void setInsdNm(String insdNm) {
			pData.put("insdNm", insdNm);
		}
	}

	// level 3 for cards
	public class CardItem {

		Map<String, Object> card = new LinkedHashMap<String, Object>();

		public Map<String, Object> getCards() {

			try {
				if (StringUtils.isBlank(MapUtils.getString(card, "cTextType")) && CollectionUtils.isNotEmpty((List<Map<String, Object>>) card.get("cTexts"))) {
					boolean hasCLabel = false;
					boolean hasCText = false;
					boolean hasCAmount = false;
					for (Map<String, Object> cTexts : (List<Map<String, Object>>) card.get("cTexts")) {
						if (StringUtils.isNotBlank(MapUtils.getString(cTexts, "cLabel"))) {
							hasCLabel = true;
						}
						if (StringUtils.isNotBlank(MapUtils.getString(cTexts, "cText"))) {
							hasCText = true;
						}
						if (StringUtils.isNotBlank(MapUtils.getString(cTexts, "cAmount"))) {
							hasCAmount = true;
						}
					}
					if (hasCLabel && hasCText && !hasCAmount) {
						setCTextType("1");
						//setCTextType("3"); // 目前3跟1無法識別, default放1
					} else if ((hasCLabel || hasCText) && hasCAmount) {
						setCTextType("4");
					} else if (!hasCLabel && hasCText) {
						setCTextType("5");
					} else {
						setCTextType("2");
					}
				}
			} catch (Exception e) { }

			return card;
		}

		public String getCName() {
			return MapUtils.getString(card, "cName");
		}
		public void setCName(String cName) {
			card.put("cName", cName);
		}
		public String getCWidth() {
			return MapUtils.getString(card, "cWidth");
		}
		public void setCWidth(String cWidth) {
			card.put("cWidth", cWidth);
		}
		public Map<String, Object> getCImageData() {
			return (Map<String, Object>) card.get("cImageData");
		}
		public void setCImageData(Map<String, Object> cImageData) {
			card.put("cImageData", cImageData);
		}
		public String getCMessage() {
			return MapUtils.getString(card, "cMessage");
		}
		public void setCMessage(String cMessage) {
			card.put("cMessage", cMessage);
		}
		public String getCTextType() {
			return MapUtils.getString(card, "cTextType");
		}
		public void setCTextType(String cTextType) {
			card.put("cTextType", cTextType);
		}
		public String getCTitle() {
			return MapUtils.getString(card, "cTitle");
		}
		public void setCTitle(String cTitle) {
			card.put("cTitle", cTitle);
		}
		public List<Map<String, Object>> getCTexts() {
			return (List<Map<String, Object>>) card.get("cTexts");
		}
		public void setCTexts(List<Map<String, Object>> cTexts) {
			card.put("cTexts", cTexts);
		}
		public String getCBreakLine() {
			return MapUtils.getString(card, "cBreakLine");
		}
		public void setCBreakLine(String cBreakLine) {
			card.put("cBreakLine", cBreakLine);
		}
		public List<Map<String, Object>> getCTexts2() {
			return (List<Map<String, Object>>) card.get("cTexts2");
		}
		public void setCTexts2(List<Map<String, Object>> cTexts2) {
			card.put("cTexts2", cTexts2);
		}
		public Integer getCLinkType() {
			return MapUtils.getInteger(card, "cLinkType");
		}
		public void setCLinkType(Integer cLinkType) {
			card.put("cLinkType", cLinkType);
		}
		public List<Map<String, Object>> getCLinkList() {
			return (List<Map<String, Object>>) card.get("cLinkList");
		}
		public void setCLinkList(List<Map<String, Object>> cLinkList) {
			card.put("cLinkList", cLinkList);
		}
	}

	// level 4 for cImageData
	public class CImageDataItem {

		Map<String, Object> cImageDatas = new LinkedHashMap<String, Object>();

		public Map<String, Object> getCImageDatas() {
			return cImageDatas;
		}

		public String getCImage() {
			return MapUtils.getString(cImageDatas, "cImage");
		}
		public void setCImage(String cImage) {
			cImageDatas.put("cImage", cImage);
		}
		public String getCImageUrl() {
			return MapUtils.getString(cImageDatas, "cImageUrl");
		}
		public void setCImageUrl(String cImageUrl) {
			cImageDatas.put("cImageUrl", cImageUrl);
		}
		public Integer getCImageTextType() {
			return MapUtils.getInteger(cImageDatas, "cImageTextType");
		}
		public void setCImageTextType(Integer cImageTextType) {
			cImageDatas.put("cImageTextType", cImageTextType);
		}
		public List<Map<String, Object>> getCImageTexts() {
			return (List<Map<String, Object>>) cImageDatas.get("cImageTexts");
		}
		public void setCImageTexts(List<Map<String, Object>> cImageTexts) {
			cImageDatas.put("cImageTexts", cImageTexts);
		}
		public String getCImageTag() {
			return MapUtils.getString(cImageDatas, "cImageTag");
		}
		public void setCImageTag(String cImageTag) {
			cImageDatas.put("cImageTag", cImageTag);
		}
	}

	// level 5 for cImageTexts
	public class CImageTextItem {

		Map<String, Object> cImageTexts = new LinkedHashMap<String, Object>();

		public Map<String, Object> getCImageTexts() {
			return cImageTexts;
		}

		public String getCImageTextTitle() {
			return MapUtils.getString(cImageTexts, "cImageTextTitle");
		}
		public void setCImageTextTitle(String cImageTextTitle) {
			cImageTexts.put("cImageTextTitle", cImageTextTitle);
		}
		public String getCImageText() {
			return MapUtils.getString(cImageTexts, "cImageText");
		}
		public void setCImageText(String cImageText) {
			cImageTexts.put("cImageText", cImageText);
		}
	}

	// level 4 for cTexts
	public class CTextItem {

		Map<String, Object> cTexts = new LinkedHashMap<String, Object>();

		public Map<String, Object> getCTexts() {
			return cTexts;
		}

		public String getCLabel() {
			return MapUtils.getString(cTexts, "cLabel");
		}
		public void setCLabel(String cLabel) {
			cTexts.put("cLabel", cLabel);
		}
		public String getCText() {
			return MapUtils.getString(cTexts, "cText");
		}
		public void setCText(String cText) {
			cTexts.put("cText", cText);
		}
		public String getCUnit() {
			return MapUtils.getString(cTexts, "cUnit");
		}
		public void setCUnit(String cUnit) {
			cTexts.put("cUnit", cUnit);
		}
		public String getCAmount() {
			return MapUtils.getString(cTexts, "cAmount");
		}
		public void setCAmount(String cAmount) {
			cTexts.put("cAmount", cAmount);
		}
		public String getCTextClass() {
			return MapUtils.getString(cTexts, "cTextClass");
		}
		public void setCTextClass(String cTextClass) {
			cTexts.put("cTextClass", cTextClass);
		}
	}

	// level 4 for cLinkContent
	public class CLinkListItem {

		Map<String, Object> cLinkList = new LinkedHashMap<String, Object>();

		public Map<String, Object> getCLinkList() {
			return cLinkList;
		}

		public Integer getClAction() {
			return MapUtils.getInteger(cLinkList, "clAction");
		}
		public void setClAction(Integer clAction) {
			cLinkList.put("clAction", clAction);
		}
		public String getClText() {
			return MapUtils.getString(cLinkList, "clText");
		}
		public void setClText(String clText) {
			cLinkList.put("clText", clText);
		}
		public String getClAlt() {
			return MapUtils.getString(cLinkList, "clAlt");
		}
		public void setClAlt(String clAlt) {
			cLinkList.put("clAlt", clAlt);
		}
		public String getClReply() {
			return MapUtils.getString(cLinkList, "clReply");
		}
		public void setClReply(String clReply) {
			cLinkList.put("clReply", clReply);
		}
		public String getClUrl() {
			return MapUtils.getString(cLinkList, "clUrl");
		}
		public void setClUrl(String clUrl) {
			cLinkList.put("clUrl", clUrl);
		}

		public String getClClick() {
			return MapUtils.getString(cLinkList, "clClick");
		}
		public void setClClick(String clClick) {
			cLinkList.put("clClick", clClick);
		}

		public String getClCustomerAction() {
			return MapUtils.getString(cLinkList, "clCustomerAction");
		}
		public void setClCustomerAction(String clCustomerAction) {
			cLinkList.put("clCustomerAction", clCustomerAction);
		}
	}

	// level 3 for linkList
	public class LinkListItem {

		Map<String, Object> linkList = new LinkedHashMap<String, Object>();

		public Map<String, Object> getLinkList() {
			return linkList;
		}

		public Integer getLAction() {
			return MapUtils.getInteger(linkList, "lAction");
		}
		public void setLAction(Integer lAction) {
			linkList.put("lAction", lAction);
		}
		public String getLText() {
			return MapUtils.getString(linkList, "lText");
		}
		public void setLText(String lText) {
			linkList.put("lText", lText);
		}
		public String getLAlt() {
			return MapUtils.getString(linkList, "lAlt");
		}
		public void setLAlt(String lAlt) {
			linkList.put("lAlt", lAlt);
		}
		public String getLReply() {
			return MapUtils.getString(linkList, "lReply");
		}
		public void setLReply(String lReply) {
			linkList.put("lReply", lReply);
		}
		public String getLUrl() {
			return MapUtils.getString(linkList, "lUrl");
		}
		public void setLUrl(String lUrl) {
			linkList.put("lUrl", lUrl);
		}
		public String getLCustomerAction() {
			return MapUtils.getString(linkList, "lCustomerAction");
		}
		public void setLCustomerAction(String lCustomerAction) {
			linkList.put("lCustomerAction", lCustomerAction);
		}
	}

	// level 3 for moneyCards
	public class MoneyCardItem {

		Map<String, Object> moneyCard = new LinkedHashMap<String, Object>();

		public Map<String, Object> getMoneyCard() {
			return moneyCard;
		}

		public String getMcTitle() {
			return MapUtils.getString(moneyCard, "mcTitle");
		}
		public void setMcTitle(String mcTitle) {
			moneyCard.put("mcTitle", mcTitle);
		}
		public String getMcUnit() {
			return MapUtils.getString(moneyCard, "mcUnit");
		}
		public void setMcUnit(String mcUnit) {
			moneyCard.put("mcUnit", mcUnit);
		}
		public String getMcAmount() {
			return MapUtils.getString(moneyCard, "mcAmount");
		}
		public void setMcAmount(String mcAmount) {
			moneyCard.put("mcAmount", mcAmount);
		}
		public List<Map<String, Object>> getMcLinkList() {
			return (List<Map<String, Object>>) moneyCard.get("mcLinkList");
		}
		public void setMcLinkList(List<Map<String, Object>> mcLinkList) {
			moneyCard.put("mcLinkList", mcLinkList);
		}
	}

	// level 4 for mcLinkList
	public class McLinkItem {

		Map<String, Object> mcLink = new LinkedHashMap<String, Object>();

		public Map<String, Object> getMcLink() {
			return mcLink;
		}

		public Integer getMclAction() {
			return MapUtils.getInteger(mcLink, "mclAction");
		}
		public void setMclAction(Integer mclAction) {
			mcLink.put("mclAction", mclAction);
		}
		public String getMclText() {
			return MapUtils.getString(mcLink, "mclText");
		}
		public void setMclText(String mclText) {
			mcLink.put("mclText", mclText);
		}
		public String getMclAlt() {
			return MapUtils.getString(mcLink, "mclAlt");
		}
		public void setMclAlt(String mclAlt) {
			mcLink.put("mclAlt", mclAlt);
		}
		public String getMclUrl() {
			return MapUtils.getString(mcLink, "mclUrl");
		}
		public void setMclUrl(String mclUrl) {
			mcLink.put("mclUrl", mclUrl);
		}
	}

	// level 3 for dateCards
	public class DateCardItem {

		Map<String, Object> dateCard = new LinkedHashMap<String, Object>();

		public Map<String, Object> getDateCard() {
			return dateCard;
		}

		public String getDcTitle() {
			return MapUtils.getString(dateCard, "dcTitle");
		}
		public void setDcTitle(String dcTitle) {
			dateCard.put("dcTitle", dcTitle);
		}
		public String getDcDate() {
			return MapUtils.getString(dateCard, "dcDate");
		}
		public void setDcDate(String dcDate) {
			dateCard.put("dcDate", dcDate);
		}
	}

	// level 3 for countdownCards
	public class CountdownCardItem {

		Map<String, Object> countdownCard = new LinkedHashMap<String, Object>();

		public Map<String, Object> getCountdownCard() {
			return countdownCard;
		}

		public String getCdcType() {
			return MapUtils.getString(countdownCard, "cdcType");
		}
		public void setCdcType(String cdcType) {
			countdownCard.put("cdcType", cdcType);
		}
		public String getCdcTitle() {
			return MapUtils.getString(countdownCard, "cdcTitle");
		}
		public void setCdcTitle(String cdcTitle) {
			countdownCard.put("cdcTitle", cdcTitle);
		}
		public String getCdcImage() {
			return MapUtils.getString(countdownCard, "cdcImage");
		}
		public void setCdcImage(String cdcImage) {
			countdownCard.put("cdcImage", cdcImage);
		}
		public String getCdcDigit() {
			return MapUtils.getString(countdownCard, "cdcDigit");
		}
		public void setCdcDigit(String cdcDigit) {
			countdownCard.put("cdcDigit", cdcDigit);
		}
	}

	public static void main(String args[]) {
		try {
			BotMessageVO vo = new BotMessageVO();

			CardItem cards = vo.new CardItem();
			cards.setCName("終身壽險");
			cards.setCTitle("新添采終身壽險9100245087");

			CTextItem cText = vo.new CTextItem();
			cText.setCText("最高可貸NT$25,000利率 2%");
			System.out.println("cards=" + (new JSONObject(cText.getCTexts())).toString());

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
			cTextList.add(cText.getCTexts());

			cards.setCTexts(cTextList);
			cards.setCLinkType(1);

			CLinkListItem cLinkList = vo.new CLinkListItem();
			cLinkList.setClAction(2);
			cLinkList.setClText("了解明細");
			cLinkList.setClAlt("我要查新添采終身壽險9100245087");

			List<Map<String, Object>> cLinkListList = new ArrayList<Map<String, Object>>();
			cLinkListList.add(cLinkList.getCLinkList());

			cards.setCLinkList(cLinkListList);

			System.out.println("cards=" + new JSONObject(cards).toString());
		} catch (Exception e) {
		}
	}
}
