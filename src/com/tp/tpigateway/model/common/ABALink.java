package com.tp.tpigateway.model.common;

import com.tp.tpigateway.util.PropUtils;

public class ABALink extends LifeLink {

	// I22
	public static LifeLink 申請復效辦理方式 = new LifeLink(1, "保單停效怎麼辦", "保單停效如何辦理復效?", "", "");

	// I25
	public static LifeLink 附約終止辦理方式 = new LifeLink(15, "看附約能不能終止", "確認保單附約能不能終止", "確認保單附約能不能終止，代碼：ABA10001(I25)", "", "");

	// I26
	// 20190318 by Todd, 腳本優化S029
	// public static LifeLink 查附約明細 = new LifeLink(15, "查附約明細", "查附約明細", "查看附約明細", "", "");
	public static LifeLink 查附約明細 = new LifeLink(15, "查附約明細", "查附約明細", "查附約明細", "", "");
	public static LifeLink 查看附約明細 = new LifeLink(2, "查看附約明細", "查看附約明細", "", "", "");

	// I26
	public static LifeLink 重新選擇附約 = new LifeLink(2, "重新選擇附約", "重新選擇附約", "", "", "");
	
	// I28
	public static LifeLink 前往會員網站辦理 = new LifeLink(2, "前往會員網站辦理", "前往會員網站辦理", "", "", "");

	// I29
	public static LifeLink 看其他終止方式 = new LifeLink(2, "看其他終止方式", "看其他終止方式", "", "", "");

	// I31
	public static LifeLink 看附約能不能縮小 = new LifeLink(15,"看附約能不能縮小","確認保單附約能不能縮小","確認保單附約能不能縮小，代碼：ABA10002(I31)","", "");

	// I35.1
	public static LifeLink I35_看其他方式 = new LifeLink(15,"看其他辦理方式","看其他辦理方式","如何辦理附約縮小","", "");
	public static LifeLink 看其他縮小方式 = new LifeLink(2, "看其他縮小方式", "如何辦理附約縮小", "", "", "");

	// 20190424 by Todd, 修正對話回捲點選牌卡對應問題
//	public static LifeLink ABA_查看詳情1 = new LifeLink(2, "查看詳情", "查看詳情-業務員!_!{\"flow_prefix\":\"aba\"}", "", "");
//	public static LifeLink ABA_查看詳情2 = new LifeLink(2, "查看詳情", "查看詳情-會員網站!_!{\"flow_prefix\":\"aba\"}", "", "");
//	public static LifeLink ABA_查看詳情3 = new LifeLink(2, "查看詳情", "查看詳情-郵寄辦理!_!{\"flow_prefix\":\"aba\"}", "", "");
//	public static LifeLink ABA_查看詳情4 = new LifeLink(2, "查看詳情", "查看詳情-臨櫃申辦!_!{\"flow_prefix\":\"aba\"}", "", "");
	
	// 20190805 by Todd, 流程優化
	// public static LifeLink ABA_查看詳情_業務員ABA00001_1 = new LifeLink(2, "查看詳情", "查看詳情-業務員ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_業務員ABA00001_1 = new LifeLink(15, "查看詳情", "業務人員辦理附約終止", "查看詳情-業務員ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	// public static LifeLink ABA_查看詳情_會員網站ABA00001_1 = new LifeLink(2, "查看詳情", "查看詳情-會員網站ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_會員網站ABA00001_1 = new LifeLink(15, "查看詳情", "登入會員網站辦理附約終止", "查看詳情-會員網站ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	// public static LifeLink ABA_查看詳情_郵寄辦理ABA00001_1 = new LifeLink(2, "查看詳情", "查看詳情-郵寄辦理ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_郵寄辦理ABA00001_1 = new LifeLink(15, "查看詳情", "自行郵寄辦理附約終止", "查看詳情-郵寄辦理ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	// public static LifeLink ABA_查看詳情_臨櫃申辦ABA00001_1 = new LifeLink(2, "查看詳情", "查看詳情-臨櫃申辦ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_臨櫃申辦ABA00001_1 = new LifeLink(15, "查看詳情", "到服務據點辦理附約終止", "查看詳情-臨櫃申辦ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
    
    public static LifeLink ABA_附約終止的權利影響 = new LifeLink(15, "附約終止的權利影響", "附約終止的權利影響", "附約終止的權利影響ABA00001_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	
	// public static LifeLink ABA_查看詳情_業務員ABA00002_1 = new LifeLink(2, "查看詳情", "查看詳情-業務員ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_業務員ABA00002_1 = new LifeLink(15, "查看詳情", "業務人員辦理附約縮小", "查看詳情-業務員ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	// public static LifeLink ABA_查看詳情_會員網站ABA00002_1 = new LifeLink(2, "查看詳情", "查看詳情-會員網站ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_會員網站ABA00002_1 = new LifeLink(15, "查看詳情", "登入會員網站辦理附約縮小", "查看詳情-會員網站ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	// public static LifeLink ABA_查看詳情_郵寄辦理ABA00002_1 = new LifeLink(2, "查看詳情", "查看詳情-郵寄辦理ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_郵寄辦理ABA00002_1 = new LifeLink(15, "查看詳情", "自行郵寄辦理附約縮小", "查看詳情-郵寄辦理ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	// public static LifeLink ABA_查看詳情_臨櫃申辦ABA00002_1 = new LifeLink(2, "查看詳情", "查看詳情-臨櫃申辦ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	public static LifeLink ABA_查看詳情_臨櫃申辦ABA00002_1 = new LifeLink(15, "查看詳情", "到服務據點辦理附約縮小", "查看詳情-臨櫃申辦ABA00002_1!_!{\"flow_prefix\":\"aba\"}", "", "");

	public ABALink(int action, String text, String alt, String reply, String url) {
		super(action, text, alt, reply, url, "");
	}
	
	/**
	 * 繳別變更方式
	 * ABA00003 (BB05)
	 * @author 黃鵬蒨
	 */
	public static class ABA00003 {
	    
	    public static LifeLink 繳別變更辦理方式 = new LifeLink(1, "繳別變更辦理方式", "繳別變更辦理方式", "", "");
	    
	    public static LifeLink 看其他辦理方式 = new LifeLink(1, "看其他辦理方式", "繳別變更辦理方式", "", "");
	    
	    public static LifeLink 前往官網 = new LifeLink(4, "前往官網", "前往官網", "", PropUtils.getProperty("cathaylife.ABA00003.url"), "");
	    
	    // 20191211 by Todd, 腳本優化調整牌卡連結
	    // public static LifeLink 繳別變更申請書 = new LifeLink(4, "開啟", "下載", "", PropUtils.getProperty("cathaylife.ABA00003.pdf.01.url"), "");
	    public static LifeLink 繳別變更申請書_開啟 = new LifeLink(4, "開啟", "下載", "", PropUtils.getProperty("cathaylife.ABA00003.pdf.01.url"), "");
	    public static LifeLink 繳別變更申請書_範本 = new LifeLink(4, "範本", "下載", "", PropUtils.getProperty("cathaylife.ABA00003.pdf.02.url"), "");
	    
	    // 20190805 by Todd, 流程優化
	    // public static LifeLink 查看詳情_業務員 = new LifeLink(2, "查看詳情", "查看詳情-業務員ABA00003_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	    public static LifeLink 查看詳情_業務員 = new LifeLink(15, "查看詳情", "業務人員辦理繳別變更", "查看詳情-業務員ABA00003_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");
	    
	    // public static LifeLink 查看詳情_會員網站 = new LifeLink(2, "查看詳情", "查看詳情-會員網站ABA00003_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");
	    public static LifeLink 查看詳情_會員網站 = new LifeLink(15, "查看詳情", "登入會員網站辦理繳別變更", "查看詳情-會員網站ABA00003_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");
	    
	    // public static LifeLink 查看詳情_郵寄辦理 = new LifeLink(2, "查看詳情", "查看詳情-郵寄辦理ABA00003_1_3!_!{\"flow_prefix\":\"aba\"}", "", "");
	    public static LifeLink 查看詳情_郵寄辦理 = new LifeLink(15, "查看詳情", "自行郵寄辦理繳別變更", "查看詳情-郵寄辦理ABA00003_1_3!_!{\"flow_prefix\":\"aba\"}", "", "");
	    
	    // public static LifeLink 查看詳情_臨櫃申辦 = new LifeLink(2, "查看詳情", "查看詳情-臨櫃申辦ABA00003_1_4!_!{\"flow_prefix\":\"aba\"}", "", "");
	    public static LifeLink 查看詳情_臨櫃申辦 = new LifeLink(15, "查看詳情", "到服務據點辦理繳別變更", "查看詳情-臨櫃申辦ABA00003_1_4!_!{\"flow_prefix\":\"aba\"}", "", "");
	    
	    public static LifeLink 繳別變更操作步驟_開啟 = new LifeLink(4, "開啟", "開啟", "", PropUtils.getProperty("cathaylife.ABA00003.edm.01.url"), "");
	}

	/**
	 * 減額繳清辦理方式
	 * ABA00004 (BB10)
	 * @author 黃鵬蒨
	 */
    public static class ABA00004 {

        public static LifeLink 減額繳清辦理方式 = new LifeLink(1, "減額繳清辦理方式", "減額繳清辦理方式", "", "");

        public static LifeLink 看其他辦理方式 = new LifeLink(1, "看其他辦理方式", "減額繳清辦理方式", "", "");

        // 20190805 by Todd, 流程優化
        public static LifeLink 減額繳清操作步驟_開啟 = new LifeLink(4, "開啟", "開啟", "", PropUtils.getProperty("cathaylife.ABA00004.edm.01.url"), "");
        
        public static LifeLink 前往官網 = new LifeLink(4, "前往官網", "前往官網", "", PropUtils.getProperty("cathaylife.ABA00004.url"), "");

        public static LifeLink 減額繳清申請書 = new LifeLink(4, "開啟", "下載", "", PropUtils.getProperty("cathaylife.ABA00004.pdf.01.url"), "");

        //20190220 優化  新增減額繳清申請書範本        
        public static LifeLink 減額繳清申請書_範本 = new LifeLink(4, "範本", "下載", "", PropUtils.getProperty("cathaylife.ABA00004.pdf.02.url"), "");
        //20190220 優化 新增繳清保險確認書
        public static LifeLink 繳清保險確認書 = new LifeLink(4, "開啟", "下載", "", PropUtils.getProperty("cathaylife.ABA00004.pdf.03.url"), "");


        public static LifeLink 查看詳情_業務員 = new LifeLink(15, "查看詳情", "業務人員辦理減額繳清", "查看詳情-業務員ABA00004_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");

        public static LifeLink 查看詳情_會員網站 = new LifeLink(15, "查看詳情", "登入會員網站辦理減額繳清", "查看詳情-會員網站ABA00004_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");

        public static LifeLink 查看詳情_郵寄辦理 = new LifeLink(15, "查看詳情", "自行郵寄辦理減額繳清", "查看詳情-郵寄辦理ABA00004_1_3!_!{\"flow_prefix\":\"aba\"}", "", "");

        public static LifeLink 查看詳情_臨櫃申辦 = new LifeLink(15, "查看詳情", "到服務據點辦理減額繳清", "查看詳情-臨櫃申辦ABA00004_1_4!_!{\"flow_prefix\":\"aba\"}", "", "");
        
        // 20190111 by Todd, 常用功能牌卡
        public static LifeLink 申請減額繳清 = new LifeLink(15, "申請減額繳清", "申請減額繳清", "如何辦理減額繳清", "");
    }

    /**
     * 縮小保額辦理方式
     * ABA00005 (BB03)
     * @author 黃鵬蒨
     */
    public static class ABA00005 {

        public static LifeLink 縮小保額辦理方式 = new LifeLink(1, "縮小保額辦理方式", "縮小保額辦理方式", "", "");

        public static LifeLink 看其他辦理方式 = new LifeLink(1, "看其他辦理方式", "縮小保額辦理方式", "", "");

        // 20190805 by Todd, 流程優化
        public static LifeLink 縮小保額操作步驟_開啟 = new LifeLink(4, "開啟", "開啟", "", PropUtils.getProperty("cathaylife.ABA00005.edm.01.url"), "");
        
        public static LifeLink 前往官網 = new LifeLink(4, "前往官網", "前往官網", "", PropUtils.getProperty("cathaylife.ABA00005.url"), "");

        public static LifeLink 縮小保額申請書 = new LifeLink(4, "開啟", "下載", "", PropUtils.getProperty("cathaylife.ABA00005.pdf.01.url"), "");
        
        //20190220 優化  增加縮小保額申請書範本  cathaylife.ABA00005.pdf.02.url
        public static LifeLink 縮小保額申請書_範本 = new LifeLink(4, "範本", "下載", "", PropUtils.getProperty("cathaylife.ABA00005.pdf.02.url"), "");

        public static LifeLink 查看詳情_業務員 = new LifeLink(15, "查看詳情", "業務人員辦理縮小保額", "查看詳情-業務員ABA00005_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");

        public static LifeLink 查看詳情_會員網站 = new LifeLink(15, "查看詳情", "登入會員網站辦理縮小保額", "查看詳情-會員網站ABA00005_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");

        public static LifeLink 查看詳情_郵寄辦理 = new LifeLink(15, "查看詳情", "自行郵寄辦理縮小保額", "查看詳情-郵寄辦理ABA00005_1_3!_!{\"flow_prefix\":\"aba\"}", "", "");

        public static LifeLink 查看詳情_臨櫃申辦 = new LifeLink(15, "查看詳情", "到服務據點辦理縮小保額", "查看詳情-臨櫃申辦ABA00005_1_4!_!{\"flow_prefix\":\"aba\"}", "", "");
    }

    /**
     * 增購保額辦理方式
     * ABA00006 (BB13)
     * @author 黃鵬蒨
     */
    public static class ABA00006 {

        public static LifeLink 看其他辦理方式 = new LifeLink(1, "看其他辦理方式", "增購保額辦理方式", "", "");

        public static LifeLink 查看詳情_業務員 = new LifeLink(2, "業務人員辦理增購保額", "查看詳情-業務員ABA00006_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");

        public static LifeLink 查看詳情_臨櫃申辦 = new LifeLink(2, "到服務據點辦理增購保額", "查看詳情-臨櫃申辦ABA00006_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");

    }
	
	/**
	 * 姓名變更辦理方式
	 * ABA00011 (BB06)
	 * @author 陳則宏
	 */
	public static class ABA00011 {
		
	    // 20190805 by Todd, 流程優化
		// public static LifeLink 查看詳情_1 = new LifeLink(2, "查看詳情", "查看詳情ABA00011_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_1 = new LifeLink(15, "查看詳情", "業務人員辦理姓名變更", "查看詳情ABA00011_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		// public static LifeLink 查看詳情_2 = new LifeLink(2, "查看詳情", "查看詳情ABA00011_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_2 = new LifeLink(15, "查看詳情", "到服務據點辦理姓名變更", "查看詳情ABA00011_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		public static LifeLink 看其他辦理方式 = new LifeLink(2, "看其他辦理方式", "姓名變更辦理方式", "", "");
	}
	
	/**
	 * 減額繳清辦理方式
	 * ABA00012 (BB11)
	 * @author 黃登俊
	 */
	public static class ABA00012 {
		
		public static LifeLink ABA00012 = new LifeLink(2, "保單補發辦理方式", "保單補發辦理方式", "", "");

		// 20190805 by Todd, 流程優化
		// public static LifeLink 查看詳情_1 = new LifeLink(2, "查看詳情", "查看詳情ABA00012_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_1 = new LifeLink(15, "查看詳情", "業務人員辦理保單補發", "查看詳情ABA00012_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		// public static LifeLink 查看詳情_2 = new LifeLink(2, "查看詳情", "查看詳情ABA00012_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_2 = new LifeLink(15, "查看詳情", "登入會員網站辦理保單補發", "查看詳情ABA00012_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		// public static LifeLink 查看詳情_3 = new LifeLink(2, "查看詳情", "查看詳情ABA00012_3!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_3 = new LifeLink(15, "查看詳情", "自行郵寄辦理保單補發", "查看詳情ABA00012_3!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		// public static LifeLink 查看詳情_4 = new LifeLink(2, "查看詳情", "查看詳情ABA00012_4!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_4 = new LifeLink(15, "查看詳情", "到服務據點辦理保單補發", "查看詳情ABA00012_4!_!{\"flow_prefix\":\"aba\"}", "", "");
		
        public static LifeLink 保單補發操作步驟_開啟 = new LifeLink(4, "開啟", "開啟", "", PropUtils.getProperty("cathaylife.ABA00012.edm.01.url"), "");
		
		public static LifeLink 看其他辦理方式 = new LifeLink(2, "看其他辦理方式", "保單補發辦理方式", "", "");
		
		public static LifeLink 查保單內容() {		
			String alt = AIALink.看保單內容.getAlt();
			return new LifeLink(2, "查保單內容", alt, "", "");
		}
		
		public static LifeLink 前往官網 = new LifeLink(4, "前往官網", "前往官網", "", PropUtils.getProperty("cathaylife.ABA00012.url"), "");
		
		public static LifeLink 保單補發申請書_開啟= new LifeLink(4, "開啟", "下載", "", PropUtils.getProperty("cathaylife.ABA00012.pdf.01.url"), "");
		
		public static LifeLink 保單補發申請書_範本= new LifeLink(4, "範本", "下載", "", PropUtils.getProperty("cathaylife.ABA00012.pdf.02.url"), "");
		//20181217優化
		// 20190827 by Todd, 流程優化
		// public static LifeLink 查看詳情_電子保單 = new LifeLink(2, "查看詳情", "電子保單ABA00012_0_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_電子保單 = new LifeLink(15, "查看詳情", "電子保單", "電子保單ABA00012_0_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		// public static LifeLink 查看詳情_紙本保單 = new LifeLink(2, "查看詳情", "紙本保單ABA00012_0_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_紙本保單 = new LifeLink(15, "查看詳情", "紙本保單", "紙本保單ABA00012_0_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		public static LifeLink 新保單要多久才下來_1= new LifeLink(2, "新保單要多久才下來", "新保單要多久才下來ABA00012_1_1", "", "");
		
		public static LifeLink 新保單要多久才下來_2= new LifeLink(2, "新保單要多久才下來", "新保單要多久才下來ABA00012_2_1", "", "");
		
		public static LifeLink 新保單要多久才下來_3= new LifeLink(2, "新保單要多久才下來", "新保單要多久才下來ABA00012_3_1", "", "");
		
		public static LifeLink 新保單要多久才下來_4= new LifeLink(2, "新保單要多久才下來", "新保單要多久才下來ABA00012_4_1", "", "");
		
		public static LifeLink 新保單要多久才下來_5= new LifeLink(2, "新保單要多久才下來", "新保單要多久才下來ABA00012_5", "", "");
		
		public static LifeLink 前往官網new = new LifeLink(4, "前往官網", "前往官網", "", PropUtils.getProperty("cathaylife.ABA00012_web.url"), "");
		
		public static LifeLink 查保單地址 = new LifeLink(2, "查保單地址", "確認保單地址ABA10006", "", "");
	}
	
	/**
	 * 要保人變更方式
	 * ABA00009 (BB07)
	 * @author 劉冠廷
	 */
	public static class ABA00009 {
		
		public static LifeLink ABA00009 = new LifeLink(2, "該怎麼變要保人", "該怎麼變要保人", "", "");
		
		public static LifeLink 家庭因素 = new LifeLink(2, "家庭因素", "家庭因素ABA00009!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 要保人身故 = new LifeLink(2, "要保人身故", "要保人身故ABA00009!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 其他原因 = new LifeLink(2, "其他原因", "其他原因ABA00009!_!{\"flow_prefix\":\"aba\"}", "", "");
		// 20190429 by Todd, BB07腳本優化
		public static LifeLink 新要保人權利義務 = new LifeLink(2, "新要保人權利義務", "新要保人權利義務ABA00009!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 變更的稅法問題 = new LifeLink(2, "變更的稅法問題", "變更的稅法問題ABA00009!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		public static LifeLink 查看業務員() {		
			String alt = LifeLink.查所有業務員.getAlt();
			return new LifeLink(2, "查看業務員", alt, "", "");
		}
		
		// BB07:財政部稅務入口網
		public final static String WEBSITE_1_5 = PropUtils.getProperty("cathaylife.ABA00009_1_5.website.url");
		public static LifeLink 前往稅務入口網 = new LifeLink(4, "前往稅務入口網", "前往稅務入口網", "", WEBSITE_1_5, "");
		
		// 20190805 by Todd, 流程優化
		// public static LifeLink 查看詳情_1_1_1 = new LifeLink(2, "查看詳情", "查看詳情ABA00009_1_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_1_1_1 = new LifeLink(15, "查看詳情", "業務人員辦理要保人變更", "查看詳情ABA00009_1_1_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		// public static LifeLink 查看詳情_1_1_2 = new LifeLink(2, "查看詳情", "查看詳情ABA00009_1_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_1_1_2 = new LifeLink(15, "查看詳情", "到服務據點辦理要保人變更", "查看詳情ABA00009_1_1_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		
		// public static LifeLink 查看詳情_1_2_1 = new LifeLink(2, "查看詳情", "查看詳情ABA00009_1_2_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_1_2_1 = new LifeLink(15, "查看詳情", "業務人員辦理要保人變更", "查看詳情ABA00009_1_2_1!_!{\"flow_prefix\":\"aba\"}", "", "");
		// public static LifeLink 查看詳情_1_2_2 = new LifeLink(2, "查看詳情", "查看詳情ABA00009_1_2_2!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 查看詳情_1_2_2 = new LifeLink(15, "查看詳情", "到服務據點辦理要保人變更", "查看詳情ABA00009_1_2_2!_!{\"flow_prefix\":\"aba\"}", "", "");
	
		public static LifeLink 看其他辦理方式_1_1 = new LifeLink(2, "看其他辦理方式", "家庭因素ABA00009!_!{\"flow_prefix\":\"aba\"}", "", "");
		public static LifeLink 看其他辦理方式_1_2 = new LifeLink(2, "看其他辦理方式", "要保人身故ABA00009!_!{\"flow_prefix\":\"aba\"}", "", "");
	}
	
	/**
	 * @author 黃登俊
	 */
	public static class CathayLife {
		//https://www.cathaylife.com.tw/ocauth/OCWeb/servlet/HttpDispatcher/OCBX_0300/prompt?TRAN_ID=ABRJ_1002
		public final static String ABA10006_2_3URL = PropUtils.getProperty("cathaylife.ABA10006_2_3.website.url");
		public final static String ABA10007_2_3URL = PropUtils.getProperty("cathaylife.ABA10007_2_3.website.url");
		public final static String ABA10005_3_3URL = PropUtils.getProperty("cathaylife.ABA10005_3_3.website.url");
	}
	
	/**
	 * @author 李思穎
	 */
	public static class ABA10003 {
		public static LifeLink 月繳 = new LifeLink(2, "月繳", "1ABA10003_1_5_a月繳", "", "");
		public static LifeLink 季繳 = new LifeLink(2, "季繳", "2ABA10003_1_5_a季繳", "", "");
		public static LifeLink 半年繳 = new LifeLink(2, "半年繳", "3ABA10003_1_5_a半年繳", "", "");
		public static LifeLink 年繳 = new LifeLink(2, "年繳", "4ABA10003_1_5_a年繳", "", "");
		public static LifeLink 躉繳 = new LifeLink(2, "躉繳", "5ABA10003_1_5_a躉繳", "", "");
		public static LifeLink 彈性繳 = new LifeLink(2, "彈性繳", "6ABA10003_1_5_a彈性繳", "", "");
		public static LifeLink 投資月繳 = new LifeLink(2, "月繳", "1ABA10003_1_5_a投資月繳", "", "");
		public static LifeLink 投資季繳 = new LifeLink(2, "季繳", "2ABA10003_1_5_a投資季繳", "", "");
		public static LifeLink 投資半年繳 = new LifeLink(2, "半年繳", "3ABA10003_1_5_a投資半年繳", "", "");
		public static LifeLink 投資年繳 = new LifeLink(2, "年繳", "4ABA10003_1_5_a投資年繳", "", "");
		public static LifeLink 投資躉繳 = new LifeLink(2, "躉繳", "5ABA10003_1_5_a投資躉繳", "", "");
		public static LifeLink 投資彈性繳 = new LifeLink(2, "彈性繳", "6ABA10003_1_5_a投資彈性繳", "", "");
		public static LifeLink 前往會員網站辦理 = new LifeLink(2, "前往會員網站辦理", "前往會員網站辦理ABA10003_1_5_b", "", "");
		public static LifeLink 重新輸入繳別 = new LifeLink(2, "重新輸入繳別", "重新輸入繳別ABA10003_1_5_a", "", "");
		public static LifeLink 重新輸入金額 = new LifeLink(2, "好，重新輸入金額", "重新輸入金額ABA10003_1_5_2", "", "");
		public static LifeLink 保單能不能繳別變更 = new LifeLink(2, "保單能不能繳別變更", "保單能不能繳別變更ABA10003_1", "", "");
	}
	
	public static class ABA10004{
		public static LifeLink 保單能不能減額繳清 = new LifeLink(2, "保單能不能減額繳清", "保單能不能減額繳清ABA10004", "", "");
	}
	
	public static class ABA10005{
		public static LifeLink 保單能不能縮小保額 = new LifeLink(2, "保單能不能縮小保額", "保單能不能縮小保額ABA10005", "", "");
	}
	
	// 20190111 by Todd, 常用功能牌卡
	public static LifeLink 申請附約變更 = new LifeLink(15, "申請附約變更", "申請附約變更", "申請附約變更ABA!_!{\"flow_prefix\":\"aba\"}", "", "");
	
    /**
	 * 附約終止
	 * ABA00001
	 * @author Todd
	 */
    public static class ABA00001 {
    	public static LifeLink 申請附約終止 = new LifeLink(15, "查看詳情", "如何辦理附約終止", "如何辦理附約終止", "");
        
        // 20190805 by Todd, 流程優化
        public static LifeLink 附約終止操作步驟_開啟 = new LifeLink(4, "開啟", "開啟", "", PropUtils.getProperty("cathaylife.ABA00001.edm.01.url"), "");
    }
    
    /**
     * 附約縮小
     * ABA00002
     * @author Todd
     */
    public static class ABA00002 {
    	public static LifeLink 申請附約縮小 = new LifeLink(15, "查看詳情", "如何辦理附約縮小", "如何辦理附約縮小", "");
        
        // 20190805 by Todd, 流程優化
        public static LifeLink 附約縮小操作步驟_開啟 = new LifeLink(4, "開啟", "開啟", "", PropUtils.getProperty("cathaylife.ABA00002.edm.01.url"), "");
    }
    
    /**
     * 附約追加
     * ABA10008
     * @author Todd
     */
    public static class ABA10008 {
    	public static LifeLink 申請附約追加 = new LifeLink(15, "查看詳情", "如何辦理附約追加", "如何辦理附約追加", "");
    }
}
