package com.tp.tpigateway.model.common;

import java.math.BigDecimal;

public class Intent {
	private String intent;
	private BigDecimal score;
	
	public Intent() {
		this.intent = "";
		this.score = BigDecimal.ZERO;
	}
	
	public Intent(String name, BigDecimal score) {
		this.intent = name;
		this.score = score;
	}

	public String getName() {
		return intent;
	}

	public void setName(String name) {
		this.intent = name;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}
}
