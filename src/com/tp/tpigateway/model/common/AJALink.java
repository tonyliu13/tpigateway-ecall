package com.tp.tpigateway.model.common;

public class AJALink extends LifeLink {

    public AJALink(int action, String text, String alt, String reply, String url) {
        super(action, text, alt, reply, url);
    }
    
    /**
	 * 中英文投保證明
	 * AJA10003
	 * @author Todd
	 */
	public static class AJA10003 {
		public static LifeLink AJA10003_1_1_1_1_1_1重新輸入資料 = new LifeLink(15, "重新輸入資料", "重新輸入資料", "AJA10003_1_1_1_1_1_1重新輸入資料", "");
		public static LifeLink AJA10003_1_1_2_1_1重新輸入資料 = new LifeLink(15, "重新輸入資料", "重新輸入資料", "AJA10003_1_1_2_1_1重新輸入資料", "");
	}
}
