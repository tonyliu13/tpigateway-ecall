package com.tp.tpigateway.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BotTouristPlaceId implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 9147231642338846221L;
    private Integer placeId;
    private Integer typeId;

    public BotTouristPlaceId() {
    }

    public BotTouristPlaceId(Integer placeId, Integer typeId) {
        this.placeId = placeId;
        this.typeId = typeId;
    }
    
    @Column(name = "place_id", nullable = false)
    public Integer getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Integer placeId) {
        this.placeId = placeId;
    }

    @Column(name = "type_id", nullable = false)
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }
    
    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof BotTouristPlaceId))
            return false;
        BotTouristPlaceId castOther = (BotTouristPlaceId) other;

        return ((this.getPlaceId() == castOther.getPlaceId()) || (this.getPlaceId() != null
                && castOther.getPlaceId() != null && this.getPlaceId().intValue() == castOther.getPlaceId().intValue()))
                && ((this.getTypeId() == castOther.getTypeId()) || (this.getTypeId() != null
                        && castOther.getTypeId() != null && this.getTypeId().intValue() == castOther.getTypeId().intValue()));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getPlaceId() == null ? 0 : this.getPlaceId().hashCode());
        result = 37 * result + (getTypeId() == null ? 0 : this.getTypeId().hashCode());
        return result;
    }
    
}
