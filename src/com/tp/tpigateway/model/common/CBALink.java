package com.tp.tpigateway.model.common;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;

import com.tp.tpigateway.util.PropUtils;

public class CBALink extends LifeLink {

    public CBALink(int action, String text, String alt, String reply, String url) {
        super(action, text, alt, reply, url);
    }

    // CBA10001:前往網頁預約電話投保(URL)
    public final static String CBA10001_1_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_1.website.url");
    // CBA10001:前往網頁預約機場投保(URL)
    public final static String CBA10001_2_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_2.website.url");
    // CBA10001:查看機場櫃檯資訊(URL)
    public final static String CBA10001_3_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_3.website.url");
    // CBA10001:前往網頁線上傳真投保(URL)
    public final static String CBA10001_4_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_4.website.url");
    // CBA10001:投保相關申請書及範例(URL)
    public final static String CBA10001_5_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_5.website.url");
    // CBA10001:加入國泰人壽會員(URL)
    public final static String CBA10001_6_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_6.website.url");
    // CBA10001:前往網頁投保(URL)
    public final static String CBA10001_7_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_7.website.url");
    // CBA10001:前往網頁變更受益人資料(URL)
    public final static String CBA10001_8_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_8.website.url");
    // CBA10001:前往網頁申請(網路投保資格)開通(URL)
    public final static String CBA10001_9_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_9.website.url");
    // CBA10001:Line分享投保(URL)
    public final static String CBA10001_10_WEBSITE_URL = PropUtils.getProperty("cathaylife.CBA10001_10.website.url");
    
    // CBA10001:LINE分享
    public final static String LINE_SHARE = PropUtils.getProperty("line.share");

    /**
     * 旅平險投保
     * CBA10001
     * @author Todd
     */
    public static class CBA10001 {

        public static LifeLink 我要試算投保 = new LifeLink(15, "我要試算投保", "我要試算投保", "CBA10001_1_1_1試算投保旅平險!_!{\"flow_prefix\":\"cbalogin\"}", "", "");

        public static LifeLink 申請易CALL保會員 = new LifeLink(11, "申請易CALL保會員", "3710", "申請易CALL保會員", "", "");

        public static LifeLink 前往網頁預約電話投保 = new LifeLink(4, "前往網頁預約電話投保", "前往網頁預約電話投保", "", CBA10001_1_WEBSITE_URL, "");
        public static LifeLink 前往網頁預約機場投保 = new LifeLink(4, "前往網頁預約機場投保", "前往網頁預約機場投保", "", CBA10001_2_WEBSITE_URL, "");
        public static LifeLink 查看機場櫃檯資訊 = new LifeLink(4, "查看機場櫃檯資訊", "查看機場櫃檯資訊", "", CBA10001_3_WEBSITE_URL, "");
        public static LifeLink 前往網頁線上傳真投保 = new LifeLink(4, "前往網頁試算傳真投保", "前往網頁試算傳真投保", "", CBA10001_4_WEBSITE_URL, "");
        public static LifeLink 投保相關申請書及範例 = new LifeLink(4, "投保相關申請書及範例", "投保相關申請書及範例", "", CBA10001_5_WEBSITE_URL, "");
        public static LifeLink 加入國泰人壽會員 = new LifeLink(4, "加入國泰人壽會員", "加入國泰人壽會員", "", CBA10001_6_WEBSITE_URL, "");
        public static LifeLink 前往網頁申請開通 = new LifeLink(4, "前往網頁申請開通", "前往網頁申請開通", "", CBA10001_9_WEBSITE_URL, "");
        public static LifeLink 前往網頁投保 = new LifeLink(4, "前往網頁投保", "前往網頁投保", "", CBA10001_7_WEBSITE_URL + "#anchorPoint", "");
        
        public static LifeLink 前往網頁投保(String IDNo, String RGN_CODE, String ISSUE_DAYS, String ISSUE_DATE, String MAIN_AMT, String HP_AMT, String HK_AMT) {
            String url = getCalculatorUrl(IDNo, RGN_CODE, ISSUE_DAYS, ISSUE_DATE, MAIN_AMT, HP_AMT, HK_AMT);
            return LifeLink.buildLink(4, "前往網頁投保", "前往網頁投保", "", url, "");
        }
        
        public static LifeLink 立即撥打電話投保(String phone) {
            return LifeLink.撥號("立即撥打電話投保", phone);
        }
        
        public static LifeLink 前往網頁投保其他方案(String IDNo, String RGN_CODE, String ISSUE_DAYS, String ISSUE_DATE, String MAIN_AMT, String HP_AMT, String HK_AMT) {
            String url = getCalculatorUrl(IDNo, RGN_CODE, ISSUE_DAYS, ISSUE_DATE, MAIN_AMT, HP_AMT, HK_AMT);
            return LifeLink.buildLink(4, "前往網頁投保其他方案", "前往網頁投保其他方案", "", url, "");
        }
        
        /**
         * 取得網頁投保頁面URL
         * 
         * @param IDNo
         * @param RGN_CODE
         * @param ISSUE_DAYS
         * @param ISSUE_DATE
         * @param MAIN_AMT
         * @param HP_AMT
         * @param HK_AMT
         * @return
         */
        public static String getCalculatorUrl(String IDNo, String RGN_CODE, String ISSUE_DAYS, String ISSUE_DATE, String MAIN_AMT, String HP_AMT, String HK_AMT) {
            StringBuilder url = new StringBuilder();
            String andFlag = null;
            // if (StringUtils.isBlank(IDNo)) {
                url.append(CBA10001_7_WEBSITE_URL);
                url.append("?page=calculator");
                andFlag = "&";
            // } else {
            //     url.append(CBA10001_8_WEBSITE_URL);
            //     andFlag = "%26";
            // }
            if (StringUtils.isNoneBlank(RGN_CODE)) {
                url.append(andFlag).append("RGN_CODE=").append(RGN_CODE);
            }
            if (StringUtils.isNoneBlank(ISSUE_DAYS)) {
                url.append(andFlag).append("ISSUE_DAYS=").append(ISSUE_DAYS);
            }
            if (StringUtils.isNoneBlank(ISSUE_DATE)) {
                url.append(andFlag).append("ISSUE_DATE=").append(ISSUE_DATE);
            }
            if (StringUtils.isNoneBlank(MAIN_AMT)) {
                url.append(andFlag).append("MAIN_AMT=").append(MAIN_AMT);
            }
            if (StringUtils.isNoneBlank(HP_AMT)) {
                url.append(andFlag).append("HP_AMT=").append(HP_AMT);
            }
            if (!"1".equals(RGN_CODE) && StringUtils.isNoneBlank(HK_AMT)) {
                url.append(andFlag).append("HK_AMT=").append(HK_AMT);
            }
            url.append("&utm_source=Alpha&utm_medium=alpha_clickToRevisePolicy&utm_campaign=alpha_travelInsurance");
            return url.toString();
        }

        public static LifeLink 看自己適合的管道 = new LifeLink(2, "看自己適合的管道", "CBA10001_1_1看自己適合的管道!_!{\"flow_prefix\":\"cbalogin\"}", "", "");

        public static LifeLink 確認方案(String PLAN_NAME, String CALL_ID, boolean IS_OMTP) {
            String[] plans = StringUtils.split(PLAN_NAME, "-");
            String confirmOMTP = "0";
            if(IS_OMTP) confirmOMTP = "1";
            return LifeLink.buildLink(15, "確認方案", "確認投保" + (plans.length > 1 ? plans[1] : PLAN_NAME), "CBA10001_5X1_5_1確認方案|{\"CALL_ID\":\"" + CALL_ID + "\",\"confirmNotice\" : \"" + confirmOMTP + "\"}!_!{\"flow_prefix\":\"cbalogin\"}", "", "");
        }

        public static LifeLink 修改地點或期間 = new LifeLink(2, "修改地點或期間", "CBA10001_1_1_1修改地點或期間!_!{\"flow_prefix\":\"cbalogin\"}", "", "");

        public static LifeLink 同意投保資訊並前往繳費(String CALL_ID, String TOURIST_ID) {
            return LifeLink.buildLink(15, "同意投保資訊並前往繳費", "同意投保資訊並前往繳費", "CBA10001_5X1_5_1_1_1同意投保資訊並前往繳費|{\"CALL_ID\":\"" + CALL_ID + "\",\"TOURIST_ID\":\"" + TOURIST_ID + "\"}!_!{\"flow_prefix\":\"cbalogin\"}", "", "");
        }

        public static LifeLink 修改投保資訊(String CALL_ID, String TOURIST_ID) {
            return LifeLink.buildLink(15, "修改投保資訊", "修改投保資訊", "CBA10001_5X1_5_1_1_2修改投保資訊|{\"CALL_ID\":\"" + CALL_ID + "\",\"TOURIST_ID\":\"" + TOURIST_ID + "\"}!_!{\"flow_prefix\":\"cbalogin\"}", "", "");
        }
        
        /**
         * 傳入編碼過的內容進行分享
         * 
         * @param text
         * @return
         */
        public static LifeLink LINE分享給親友(String text) throws UnsupportedEncodingException {
            return LifeLink.buildLink(4, "LINE分享給親友", "LINE分享給親友", "", LINE_SHARE + text, "");
        }
    }
}
