package com.tp.tpigateway.model.common;

import com.tp.tpigateway.util.DateUtil;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "flow_call_log")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlowCallLog {

    @Id
    @Column(name = "log_id")
    private String logId;

    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "msg_id")
    private String msgId;

    @Column(name = "cust_id")
    private String custId;
    
    @Column(name = "member_id")
    private String memberId;

	@Column(name = "ip")
    private String ip;

    @Column(name = "msg")
    private String msg;

    @Column(name = "node_Id")
    private String nodeId;

    @Column(name = "msg_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date msgTime;

    @Column(name = "nlu_result")
    private String nluResult;

    @Column(name = "nlu_intent")
    private String nluIntent;

    @Column(name = "nlu_score")
    private String nluScore;

    @Column(name = "nlu_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date nluTime;
    
    @Column(name = "nlu_cost_time")
    private Long nluCostTime;

    @Column(name = "faq_result")
    private String faqResult;

    @Column(name = "faq_answer")
    private String faqAnswer;

    @Column(name = "faq_score")
    private String faqScore;

    @Column(name = "faq_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date faqTime;

    @Column(name = "faq_cost_time")
    private Long faqCostTime;

    @Column(name = "bot_node_id")
    private String botNodeId;

    @Column(name = "bot_node_num")
    private String botNodeNum;

    @Column(name = "bot_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date botTime;

    @Column(name = "bot_cost_time")
    private Long botCostTime;

    @Column(name = "flow_time")
    @DateTimeFormat(pattern = DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS)
    private Date flowTime;

    @Column(name = "flow_cost_time")
    private Long flowCostTime;

    @Column(name = "flow_host_name", length = 20)
    private String flowHostName;
    
    @Column(name = "sub_channel", length = 10)
    private String subChannel;
    
	@Column(name = "input_type", length = 1)
    private String inputType;
	
	@Column(name = "flow_app_id", length = 30)
	private String flowAppId;

	@Column(name = "cookie", length = 75)
	private String celebrusCookie;

	@Column(name = "keyin_time")
    private Date keyinTime;

    @Column(name = "keyin_upd_time")
    private Date keyinUpdTime;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
    
    public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public Date getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(Date msgTime) {
        this.msgTime = msgTime;
    }

    public String getNluResult() {
        return nluResult;
    }

    public void setNluResult(String nluResult) {
        this.nluResult = nluResult;
    }

    public String getNluIntent() {
        return nluIntent;
    }

    public void setNluIntent(String nluIntent) {
        this.nluIntent = nluIntent;
    }

    public String getNluScore() {
        return nluScore;
    }

    public void setNluScore(String nluScore) {
        this.nluScore = nluScore;
    }

    public Date getNluTime() {
        return nluTime;
    }

    public void setNluTime(Date nluTime) {
        this.nluTime = nluTime;
    }
    
    public Long getNluCostTime() {
		return nluCostTime;
	}

	public void setNluCostTime(Long nluCostTime) {
		this.nluCostTime = nluCostTime;
	}

    public String getFaqResult() {
        return faqResult;
    }

    public void setFaqResult(String faqResult) {
        this.faqResult = faqResult;
    }

    public String getFaqAnswer() {
        return faqAnswer;
    }

    public void setFaqAnswer(String faqAnswer) {
        this.faqAnswer = faqAnswer;
    }

    public String getFaqScore() {
        return faqScore;
    }

    public void setFaqScore(String faqScore) {
        this.faqScore = faqScore;
    }

    public Date getFaqTime() {
        return faqTime;
    }

    public void setFaqTime(Date faqTime) {
        this.faqTime = faqTime;
    }
    
    public Long getFaqCostTime() {
		return faqCostTime;
	}

	public void setFaqCostTime(Long faqCostTime) {
		this.faqCostTime = faqCostTime;
	}

    public String getBotNodeId() {
        return botNodeId;
    }

    public void setBotNodeId(String botNodeId) {
        this.botNodeId = botNodeId;
    }

    public String getBotNodeNum() {
        return botNodeNum;
    }

    public void setBotNodeNum(String botNodeNum) {
        this.botNodeNum = botNodeNum;
    }

    public Date getBotTime() {
        return botTime;
    }

    public void setBotTime(Date botTime) {
        this.botTime = botTime;
    }
    
    public Long getBotCostTime() {
		return botCostTime;
	}

	public void setBotCostTime(Long botCostTime) {
		this.botCostTime = botCostTime;
	}

    public Date getFlowTime() {
        return flowTime;
    }

    public void setFlowTime(Date flowTime) {
        this.flowTime = flowTime;
    }
    
    public Long getFlowCostTime() {
		return flowCostTime;
	}

	public void setFlowCostTime(Long flowCostTime) {
		this.flowCostTime = flowCostTime;
	}
	
	public String getFlowHostName() {
		return flowHostName;
	}

	public void setFlowHostName(String flowHostName) {
		this.flowHostName = flowHostName;
	}
	
	public String getSubChannel() {
		return subChannel;
	}

	public void setSubChannel(String subChannel) {
		this.subChannel = subChannel;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	
	public String getFlowAppId() {
		return flowAppId;
	}

	public void setFlowAppId(String flowAppId) {
		this.flowAppId = flowAppId;
	}
	
	public String getCelebrusCookie() {
		return celebrusCookie;
	}

	public void setCelebrusCookie(String celebrusCookie) {
		this.celebrusCookie = celebrusCookie;
	}

    public Date getKeyinTime() {
        return keyinTime;
    }

    public void setKeyinTime(Date keyinTime) {
        this.keyinTime = keyinTime;
    }

    public Date getKeyinUpdTime() {
        return keyinUpdTime;
    }

    public void setKeyinUpdTime(Date keyinUpdTime) {
        this.keyinUpdTime = keyinUpdTime;
    }
}
