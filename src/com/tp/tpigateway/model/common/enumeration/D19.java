package com.tp.tpigateway.model.common.enumeration;

// 信用卡卡號檢核
public enum D19 {

    // input
    CARD_NO, // 信用卡卡號
    CARD_END_YM, // 信用卡到期年月
    USER_ID, // 使用者ID
    TRANS_NO, // 訂單編號
    TRANS_UUID, // 電商交易LOG編號
    ZS_UUID, // ZS LOG編號

    // output
    resultCode,

    data,
}
