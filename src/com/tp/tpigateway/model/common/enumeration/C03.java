package com.tp.tpigateway.model.common.enumeration;

// 網投旅平險-契約成立
public enum C03 {

    // input
    TRANS_NO, TRANS_UUID,

    contractJSON,
    /*** contractJSON START ****/
    CLIENT_ID, // 使用者
    // TRANS_NO, 交易序號
    AUTH_CODE, // 授權碼
    PAY_WAY_CODE, // 繳費方式
    IS_NCCC, // NCC
    IS_EXTENDED, // 延保件
    /*** contractJSON END ****/

    // output
    resultCode,

    data,
    /*** data START ****/
    // IS_EXTENDED, 
    // TRANS_NO,
    // CLIENT_ID,
    // PAY_WAY_CODE,
    // AUTH_CODE,
    // IS_NCCC,

    CONTRACT_DATA,
    /*** CONTRACT_DATA START ****/
    POLICY_NO,
    /*** CONTRACT_DATA END ****/
    /*** data END ****/
}
