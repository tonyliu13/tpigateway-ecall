package com.tp.tpigateway.model.common.enumeration;
/**
 * 單一房貸繳息紀錄
 * @author i9300627
 *
 */
public enum S02 {
	// input
	LOAN_ID,
	BORROW_ID,
	
	// output
	CURR,
	PAY_TERM,
	PAY_TYPE,
	DUTY_PAYDT,
	DUTY_PAYMT,
	ACCOUNT,
	SALARY_DATE,
	DEDUCT_DATE,
	IS_DELAY,
	PAY_MEMO,
	TRANS_NO,
	PRINCIPAL,
	INTEREST,
	DELAY_INT,
	RENEGE,
	LAWSUIT_FEE
}