package com.tp.tpigateway.model.common.enumeration;

// 驗證一次性密碼(驗證OTP)
public enum Z02 {

    // input
    trnId, // 交易代號
    s, // 序號
    otp, // 密碼

    // output
    RTN_CODE, // 回傳代碼
    RTN_MSG, // 回傳訊息
    CHK_OVER, // 對於錯誤次數已達上限的特別註記
}
