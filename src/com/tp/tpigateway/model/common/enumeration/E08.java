package com.tp.tpigateway.model.common.enumeration;

//官網保單清單查詢
public enum E08 {
	// input
	ID, 			// 輸入ID (要保人)
	NATION_CODE, 	// 國籍 TW
	PROD_KIND, 		// "0"：傳統型、"1"：投資型、"2"：全部、"3":利變
	//APC_IN, 		// 身分別  "0"：被保人、"1"：要保人、"2"：要被保人
	//POLICY_NO, 	// 顯示POLICY_NO之後之保單(分頁使用，參數帶本頁最後一筆保單號碼，下一頁就會從這個保單號碼之後開始抓，一次全抓這欄給空字串)

	// output
	//ID, 			// 身份證字號
	AUTH_NO, 		// 授權碼
	POLICY_NO, 		// 保單號碼
	APC_IN, 		// 身份別
	PROD_ID, 		// 險別代碼
	PROD_NAME, 		// 險別中文簡稱
	EFT_CODE, 		// 契約效力
	ISSUE_DATE, 	// 投保日期
	INSD_ID, 		// 被保人ID
	INSD_NAME, 		// 被保人姓名
	INSD_BRDY, 		// 被保人生日
	APC_ID, 		// 要保人ID
	APC_NAME, 		// 要保人姓名
	APC_BRDY, 		// 要保人生日
	CLC_ID, 		// 收費人員ID
	SCRT_IND, 		// 密戶
	//PROD_KIND, 	// 商品別
	IS_CHG_ANTY, 	// 是否為利變年金類商品
	MsgDesc, 		// 回覆訊息
	EFT_CODE_CH, 	// 效力中文
	EXT_END_DATE, 	// 展期終期
	MAT_DATE, 		// 滿期日
	LPS_DATE, 		// 停效日
	PAY_FREQ, 		// 繳別
	NXT_PAY_DATE, 	// 應繳日期 
	SUM_ROI, 		// 參考總投資報酬率
	IF_PAY, 		// 是否可繳費
	PRINT_TYPE, 	// 保單型式
	EPOLICY_PATH, 	// 電子保單路徑
	//NATION_CODE, 	// 國籍
	AUTO_PREM_NAME, // 墊繳選擇中文
	PAY_FREQ_NAME, 	// 繳費種類中文
	UN_CASH_TMS, 	// 票據未兌現次數
	ACNT_BANK_NO, 	// 扣款帳號
	CURR, 			// 幣別
	CURR_CH, 		// 幣別中文
	SIGN_TYPE, 		// 簽收方式
	IS_ONLINE_SIGN, // 是否線上簽收
	ROW_NUMBER 		// 序號
}
