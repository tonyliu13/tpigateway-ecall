package com.tp.tpigateway.model.common.enumeration;

//查詢最近服務中心據點
public enum J02 {
    //input
    POLICY_NO,    //保單號碼
    APC_ID,    //要保人ID
    DOWNLOAD_NAME,    //姓名
    IP_ADDRS,    //登入IP

    //output
    downloadFileFullPath,    //檔案完整路徑
    downloadFileName,    //檔案名稱
    PWD,    //0:無密碼;1:要保人ID
}

