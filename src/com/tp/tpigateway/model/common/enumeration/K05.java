package com.tp.tpigateway.model.common.enumeration;

/**
 * 保戶卡申請資格查詢
 * 
 */
public enum K05 {
	// input
	/** 要保人ID */
	ID,

	// output
	QUALIFY,
	/**e保戶卡  Y：保戶符合資格，即可下載e保戶卡。
			 N：不符資格，即無法下載e保戶卡。*/
	downloadFileFullPath,	/** 紙本繳款單檔案路徑-PDF */
	downloadFileName		/** 紙本繳款單檔案名稱-PDF */
}
