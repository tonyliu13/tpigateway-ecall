package com.tp.tpigateway.model.common.enumeration;

//單一保單保貸催繳紀錄
public enum D07 {
	// input
	POLICY_NO,		// 保單號碼，查詢該保單號碼之保貸催繳記錄
	URGE_STR_DATE,	// 催繳日期(起)，預設值(查詢前二月(T-2)月1號)
	URGE_END_DATE,	// 催繳日期(迄)，預設值(查詢當月月底)

	// output
	//POLICY_NO,	// 保單號碼
	MAIL_DATE,		// 交寄日(民國年月日)
	PAY_DATE_POL,	// 保價不足日(民國年月日)
	URGE_KIND,		// 催繳種類，如:保貸息逾準備金
	DCT_BANK_DATE,	// 民國年月日
	RTN_CHK_AMT,	// 借款本金
	AUTO_PREM_TOT,	// 應繳利息
	URGE_ADDR		// 催繳地址

}
