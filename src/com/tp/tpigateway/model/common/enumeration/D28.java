package com.tp.tpigateway.model.common.enumeration;

// 信用卡卡號儲存
public enum D28 {

    // input
	PAY_WAY_CODE, //繳費管道
	IS_SAVE, //是否儲存
	USER_ID, // 使用者ID
	TRANS_NO, // 訂單編號
    CARD_NO, // 信用卡卡號
    CARD_END_YM, // 信用卡到期年月
    TRANS_UUID, // 電商交易LOG編號

    // output
    resultCode,
    resultMsg,
    
    data,
}
