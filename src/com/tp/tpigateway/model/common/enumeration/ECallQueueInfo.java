package com.tp.tpigateway.model.common.enumeration;

// ECallQueueInfo
public enum ECallQueueInfo {

    // input
    
    // output
    result,
    func_show,
    total_waiting,
    error_message,
    login_agent,
    ready_agent
}
