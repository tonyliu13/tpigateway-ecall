package com.tp.tpigateway.model.common.enumeration;

/**
 * 單一保單紅利提領紀錄
 * i9400120
 * 2018/08/13
 * {
 * 		IS_RED_POINT:Y/N
 * 		rtnList[
 * 			{......}
 * 		]
 * }
 * 
 * */
public enum A23 {
	// input
	POLICY_NO_QRY, /**保單號碼*/
	PROD_ID,/**險別代號*/

	// output
	IS_RED_POINT,/**該險別是否有紅利*/
	rtnList,/**紅利記錄*/	
	POLICY_NO, /**保單號碼(分類)*/
	PAY_DATE, /**帳務日期*/
	PAY_NET, /**實支金額*/
	PAY_TYPE, /**給付方式*/
	ACPT_ACNT_NO, /**帳號*/
	ACPT_BANK_NO /**銀行行庫中文名稱*/

}
