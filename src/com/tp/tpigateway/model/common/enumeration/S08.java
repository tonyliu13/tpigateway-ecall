package com.tp.tpigateway.model.common.enumeration;
/**
 * ID歸戶房貸繳息證明
 */
public enum S08 {
	// input
	/**貸款帳號     <br> output:房貸繳息證明清單(PRINT_LIST)-貸款帳號(LOAN_ID)*/
	LOAN_ID,
	/**借款人ID*/
	BORROW_ID,
	
	// output
	/**非房貸客戶        Y是：無房貸資料或S01[契約效力]>16;  N否*/
	NOT_CUS,
	/**是否有繳息證明書    Y是：有list;  N否*/
	HAS_DATA,
	
	/**房貸繳息證明清單*/
	PRINT_LIST,
	
	/**檔案路徑-PDF*/
	downloadFileFullPath,
	/**檔案名稱-PDF*/
	downloadFileName
}