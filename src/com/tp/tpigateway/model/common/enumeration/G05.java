package com.tp.tpigateway.model.common.enumeration;

// 取得網投旅平險訂單編號
public enum G05 {
    // input
    SYS_CODE, // 系統代號
    START_WITH, // 訂單編號開頭

    // output
    resultCode, 
    data,
    /*** data START ****/
    TRANS_NO
    /*** data END ****/
}
