package com.tp.tpigateway.model.common.enumeration;

//取得服務人員聯絡
public enum D16 {
	// input
	//CLC_NO, // 收費代號 單一保單之收費代號

	// output
	 CLC_NO,
     CLC_ID,
     CLC_NAME,
     DIV_TEL,
     CLC_EMAIL 
}
