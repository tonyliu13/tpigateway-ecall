package com.tp.tpigateway.model.common.enumeration;

/**
 * 房貸火險資訊
 */
public enum S04 {
    // input
    /**借款人ID*/
    BORROW_ID,

    // output
    /**有非贈送件  */
    HAS_NOTFREE,
    /** 皆為贈送件 */
    ALL_FREE,
    /** 有失效件  */
    HAS_INVALID,
    /** 有商業火險或失效件 */
    HAS_BUS_INVALID,
    /** 有商業火險  */
    HAS_BUS,
    /** 有贈送火險*/
    HAS_FREE,
    /** 房貸餘額為0(皆清償) */
    NOT_VALID,
    /** 非房貸客戶 、FIRE_LIST*/
    NOT_CUS,

    /**房貸火險資訊清單 */
    FIRE_LIST,

    /** 貸款帳號 */
    LOAN_ID,
    /** 原貸帳號 */
    REFER_ID,
    /**火單狀況*/
    FIRE_STATUS,

    /** 幣別 */
    CURR,
    /** 是否贈送火險 */
    INSUFREE_CODE,
    /** 火單號碼 */
    FIREINSU_POLNO,
    /** 擔保品地址 */
    GAGE_ADDR,
    /** 火險種類 */
    NOTE_NM,
    /** 火險迄日 */
    FIREINSU_ENDDT,
    /** 扣款狀態異常 */
    DEDUCT_FAIL,
    /** 備註 */
    MEMO,
    /** 火單狀態 */
    ISSUE_CODE,
    /** 火險應繳金額 */
    PAYMENT,
    /** 火險繳款說明 */
    PAY_NOTE,
    /** 生效日期 */
    EFFECT_DATE,
    /** 續保年月 */
    PAYMENT_YM,
    /** 扣款狀態 */
    TRANS_STATUS,
    /** 沖銷與否 */
    PAY_DATE
}