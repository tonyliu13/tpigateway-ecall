package com.tp.tpigateway.model.common.enumeration;

import java.util.HashMap;
import java.util.Map;

//單一保單資訊
public enum E02 {
	//input
	POLICY_NO_QRY,	//保單號碼	
	QRY_FRM,	//查詢來源	定値：FROMB2E1
	QRY_ID,	//查詢人員ID	定値：ChatBot

	//output
	POLICY_NO,	//保單號碼	
	APC_ID,	//要保人ID	
	APC_NM,	//要保人姓名	
	INSD_ID,	//被保人ID	
	INSD_NM,	//被保人姓名	
	PROD_ID,	//險別代號
	PROD_NAME,	//險別中文名稱	
	POL_PRD,	//保險年期	
	PAY_PRD,	//繳費年期	
	FACE_AMT,	//主約保額	
	EFT_CODE,	/*契約效力	"比照官網保單查詢規則:
					一、保單類別為傳統型
					00(正常)、01(停效)、02(終身險滿期)、20(展期件)、21(展期滿期件)、23(繳清件)、24(繳清滿期件)、36(非終身險滿期)、53(長期看護)、54(重大疾病)、61(貸款停效)
					1.當效力01、61時，查詢日-停效日>2年則不顯示；
					2.當效力20、21時，查詢日>=展期終期則不顯示；
					3.當效力23、24、36時，查詢日>=滿期日則不顯示
					二、保單類別為投資型僅顯示00(正常)
					三、保單類別為利變年金與萬能壽險僅顯示00(正常)"
					*/
	EFT_CODE_NM,	//契約效力名稱	
	MAIN_PREM,	//主約保費	
	bdPREM_RD,	//下次特約保費	
	PAY_FREQ,	//繳別	1:月繳、2:季繳、3:半年繳、4:年繳、5:躉繳、6:彈性繳
	PAY_FREQ_NM,	//繳別中文	代碼中文：AB、PAY_FREQ
	PAY_TIMES,	//繳次	
	TOTAL_PAY_TIMES, //主約總應繳期數
	LST_PAY_DATE,	//最近繳費日	西元年月日
	PREM_RCPT,	//下次應繳保費	西元年月日
	MNXT_PAY_DATE,	//下次應繳日	西元年月日
	PREM_DISC,	//優待保費	
	PREM_TRN_CODE,	//保費轉帳	如:無轉帳／自繳、無轉帳、銀行轉帳／自繳、銀行轉帳、信用卡轉帳／自繳、信用卡轉帳、扣薪／自繳、扣薪等
	KIND_CODE,	//繳費方式	1.人工件;2.扣薪件;3.轉帳;4.信用卡;5.國泰卡;6.自繳;9.郵撥件;
	AUTO_PAY_CODE,	//自繳資格碼	Y/N
	AUTO_PREM_CODE,	//墊繳選擇	0：不自動墊繳、1：自動墊繳
	MLY_PAY_DATE,	//墊繳下次應繳日	墊繳時才會有值，西元年月日
	SCRT_IND,	//密戶表示	0:非密戶; 1:密戶 
	ISSUE_DATE,	//投保始期	西元年月日
	LPS_DATE,	//停效日期	西元年月日
	CURR,	//幣別	NTD、USD、AUD、NZD、CNY、ALL
	CURR_NM,	/*幣別中文	"中文顯示  台幣、美元、澳幣、紐幣、人民幣、綜合外幣
				代碼系統別：AB, 代碼：CURR"*/
	CLC_NO,	//收費代號	
	PREM_GRP_DISC,	//團體集彙優待比率	集體減費率
	UN_CASH_TMS,	//票據未兌現次數	
	CXL_PROD_KIND,	/*保單類別 (官網保單資料)	"00:傳統型
					01:投資型
					02:利變年金與萬能壽險"*/
	ADDR,	//收費地址	
	ANTY_PAY_KIND;	//年金給付方式

	private static Map<String,String> eftCodeMap;
	
	private static final String[] EFT_CODE_ARRAY = {
		"00:(正常)", "01:(停效)", "02:(終身險滿期)", "20:(展期件)", "21:(展期滿期件)", "23:(繳清件)",
		"24:(繳清滿期件)", "36:(非終身險滿期)", "53:(長期看護)", "54:(重大疾病)", "61:(貸款停效)" 
	};
	
	static {
		eftCodeMap = new HashMap<String,String>();
		for(String s : EFT_CODE_ARRAY) {
			String[] arr = s.split(":");
			String code = arr[0];
			String desc = arr[1];
			eftCodeMap.put(code, desc);
		}
	}
	
	public static String getEftCodeDesc(String EFT_CODE) {
		return eftCodeMap.get(EFT_CODE);
	}
	
}