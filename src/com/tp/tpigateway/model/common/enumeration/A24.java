package com.tp.tpigateway.model.common.enumeration;

/**
 * 保全	單一保單年金提領紀錄
 * 
 * @author i9300627
 */
public enum A24 {
	// input
	/** 保單號碼 */
	POLICY_NO,

	// output
	/** 年金次數 */
	ANTY_TIMES,
	/** 應領年金 */
	ANTY_RCBL,
	/** 帳務日期 */
	TACNT_DATE,
	/** 應付金額 */
	SUM_AMT,
	/** 償還借款金額 */
	LOAN_AMT,
	/** 償還借款利息 */
	LOAN_INT,
	/** 本次扣回理賠金 */
	ADV_OVER_AMT,
	/** 償還墊繳金額 */
	AUTO_PREM_AMT,
	/** 償還墊繳利息 */
	AUTO_PREM_INT,
	/** 償還墊繳延滯息 */
	AUTO_PREM_DLY,
	/** 應扣金額 */
	SUB_AMT,
	/** 實支金額 */
	PAY_NET,
	/** 給付方式 */
	PAY_TYPE,
	/** 受款人 */
	ACPT_NAME,
	/** 帳號 */
	ACPT_ACNT_NO,
	/** 銀行行庫中文名稱 */
	ACPT_BANK_NO,
	/** 個別受款人實支金額 */
	ACPT_PAY_NET
}
