package com.tp.tpigateway.model.common.enumeration;

/**
 * VIP資格查詢
 * 
 */
public enum K01 {
	// input
	/** 要保人ID */
	ID,

	// output
	/**VIP等級  0:非VIP, 5:黃金級,6:白金級,7:鑽石級,*/
	APC_VIP_LVL,
	/**家戶VIP等級  0:非VIP, 5:黃金級,6:白金級,7:鑽石級,*/ 
	FAM_VIP_LVL
}
