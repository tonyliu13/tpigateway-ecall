package com.tp.tpigateway.model.common.enumeration;

//單一保單查詢附約繳費記錄
public enum D05 {
	// input
	POLICY_NO,	// 保單號碼，僅顯示最近10期之墊繳紀錄
	QUERY_DATE, // 查詢日期，以查詢當日日期試算利息

	// output
	dataList,
	/*** showList ****/
	PAY_DATE_POL, 	// 應繳日期，該期應繳的日期
	PAY_TIMES, 		// 應繳次數，墊繳繳次、破月墊會從901開始標記至999後固定
	AUTO_PREM_AMT, 	// 墊繳金額
	TOT_RATE, 		// 墊繳利息
	COUNT_DAYS, 	// 墊繳日數
	RTN_PAY_DATE, 	// 歸還日期，無值
	AUTO_PREM_KIND, // 墊繳種類，如:正常墊繳、破月墊繳、整合復效、墊繳(國寶轉檔)等
	/*** showList ****/

	UNPAY_AUTO_PREM_AMT, // 待償墊繳餘額
	AUTO_PREM_INT, 		 // 墊繳餘額利息
	TOT_AUTO_AMT 		 // 墊繳本息合計，總計墊繳本金+總計墊繳利息

}
