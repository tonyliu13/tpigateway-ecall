package com.tp.tpigateway.model.common.enumeration;

/**
 * A15 險別查詢滿期金年金設定
 * 
 * @author i9300627
 */
public enum A15 {
	// input
	/** 險別 */
	prod_id,

	// output
	/** 是否有滿期金 */
	is_mat,
	/** 是否有年金 */
	is_anty
}
