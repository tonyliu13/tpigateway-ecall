package com.tp.tpigateway.model.common.enumeration;

// 給付款未領明細查詢
public enum A32 {

    // input
    DATA_KIND, // 資料種類 
    POLICYNO, // 保單號碼
    ID, // ID
    ROLE, // 角色

    // output
    INSD_NAME, // 被保人姓名 
    ACPT_ID, // 受款人ID 
    INSD_ID, // 被保人ID 
    PAY_AMT, // 應領金額 
    CURR, // 幣別 
    // DATA_KIND, // 資料種類 
    DATA_CONT, // 顯示內容 
    APC_NAME, // 要保人姓名 
    POLICY_NO, // 保單號碼 
    APC_ID, // 要保人ID 
    CLC_NO, // 收費代號  
    PROC_DATE, // 資料更新日期 
    PAY_DATE, // 應領日期 
    ACPT_NAME, // 受款人姓名 
    PROD_NAME, // 險別中文簡稱
    PROD_ID, // 險別代號

}
