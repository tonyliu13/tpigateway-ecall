package com.tp.tpigateway.model.common.enumeration;
/**
 * ID歸戶房貸業務人員
 */
public enum S07 {
	// input
	/**input:貸款帳號     <br> output:房貸業務人員清單(SERV_LIST)-貸款帳號(LOAN_ID)*/
	LOAN_ID,
	/**借款人ID*/
	BORROW_ID,
	
    // output
    /**非房貸客戶        Y是：無房貸資料或S01[契約效力]>16;  N否 */
    NOT_CUS,
    /**是否有業務人員     Y是 ;  N否：S07[業務人員姓名]無值 */
    HAS_SREV,
    
	/**房貸業務人員清單 */
    SERV_LIST,
    
	/**收費代號*/
    GETINT_CODE,
	/**經手人ID*/
    AGENT_ID,
	/**業務人員姓名*/
    SERV_NAME,
	/**業務人員聯絡電話*/
    TEL_OFFICE,
	/**業務人員email信箱*/
    EMAIL
}