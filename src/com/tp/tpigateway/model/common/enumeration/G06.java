package com.tp.tpigateway.model.common.enumeration;

// 查詢網投旅平險最新訂單資訊
public enum G06 {
    // input
    ID,

    // output
    resultCode,

    data,
    /*** data START ****/
    TRANS_NO, // 訂單編號
    SYS_NO, // 投保商品別(CB開頭為旅平險商品, AT開頭為壽險商品)
    APC_ID, // 要保人ID
    APC_NAME, // 要保人姓名
    BIRTHDAY, // 要保人生日
    APC_ID_TYPE, // 要保人證件來源(1:身分證,2:居留證)
    MOBIL_NO, // 要保人手機
    EMAIL, // 要保人EMAIL
    ZIP_CODE, // 要保人郵遞區號
    ADDR, // 要保人地址
    NATION_CODE, // 要保人國籍

    INSD_DATA, // 被保人資料(多筆)
    /*** INSD_DATA START ****/
    INSD_ID, // 被保人證件字號
    INSD_NAME, // 被保人姓名
    INSD_BIRTH, // 被保人生日
    INSD_SER, // 被保人序號
    RLTN, // 被保人與要保人關係(1:本人, 2:配偶, 3:子女, 4:父母, 5:祖孫)
    IS_GUARD, // 是否有監護宣告(Y:是,N:否)
    YEAR_INCOME, // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上)

    BENE_DATA, // 受益人資料(多筆)
    /*** BENE_DATA START ****/
    BENE_ID, // 受益人證件字號 [旅平險商品&法定繼承人,回傳空字串]
    BENE_NAME, // 受益人姓名
    BENE_BIRTH, // 受益人生日
    ADDR_AS_APC, // 受益人電話及地址同要保人(Y:是,N:否)
    BENE_ZIP, // 受益人郵遞區號
    BENE_ADDR, // 受益人地址
    BENE_RATO, // 獲得理賠方式_比例
    BENE_PRIO, // 獲得理賠方式_順位
    // RLTN, 受益人與被保人關係(1:法定繼承人, 2:配偶, 3:子女, 4:父母, 5:祖孫)
    BENE_PHONE, // 受益人電話
    BENE_TYPE, // 受益人種類(1:身故 2:祝壽金)
    BENE_SER, // 受益人序號
    ID_KIND, // 證件類別-OI記錄值(1:身分證,2:統一證號,3:護照,4:統一編號)
    BENE_ID_TYPE, // 證件類別-核心存放值(空字串:法定繼承人,1:身分證,2:統一證號,3:護照,4:統一編號)
    // NATION_CODE, 受益人國籍
    /*** BENE_DATA END ****/
    /*** INSD_DATA END ****/

    
    /*** data END ****/
}