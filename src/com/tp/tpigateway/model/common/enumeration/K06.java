package com.tp.tpigateway.model.common.enumeration;

/**
 * 檢核保戶是否有被保人之保單-投保證明
 * 
 */
public enum K06 {
	// input
	/** 要保人ID */
	ID,

	// output
	checkResult // 有被保人的保單 Y:有保單 N:無保單
}
