package com.tp.tpigateway.model.common.enumeration;

//取得服務人員聯絡(歸戶)
public enum D25 {
	// input
	//POLICY_NO,    // 保單號碼
    //AGNT_ID,      // 使用人員(ChatBot)
    //RTN_LOAN_AMT, // 償還貸款本金，輸入金額(若全部清償不需輸入該欄位)
    //RTN_ALL,      // 全部清償，1:全部清償  "":不全部清償
    //INT_END_DATE, // 繳息終期，預設為查詢日前一天日暦日
    //RCPT_KIND,    // 現金/匯款單，傳入定值"1"
    //PRT_TYPE,     // 繳款單類型，A:雲端繳款  B:紙本繳款單

	// output
    TOT_PREM,       // 合計金額，無金額以無值呈現
    LOAN_BAL,       // 貸款金額
    RTN_LOAN_INT,   // 償還貸款利息，利息需包含延滯息
    RTN_LOAN_AMT,   // 償還貸款本金
    RETURN_CODE,  // 償還貸款檢核，1.有金額：輸出A0
                    // 　　　　　　　2.跳出本件保單無保單貸款紀錄：輸出A1
                    // 　　　　　　　3.跳出壽四科不可還款錯誤訊息：輸出A2
                    // 　　　　　　　優先性A2->A1->A0（三者不併存）
    RCV_VALD_DATE, // 使用期限，單據開立出來的文件上使用期限
    PAY_DATA_URL,   // 雲端繳款單-URL
    downloadFileFullPath,   // 紙本繳款單檔案路徑-PDF
    downloadFileName        // 紙本繳款單檔案名稱-PDF

    }
