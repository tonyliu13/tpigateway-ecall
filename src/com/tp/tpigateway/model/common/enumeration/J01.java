package com.tp.tpigateway.model.common.enumeration;

//查詢最近服務中心據點
public enum J01 {
    //input
    POLICY_NO,    //保單號碼
    INSD_ID,    //被保人ID
    SRC,    //資料來源
    //TRY_CNT_DATE,    //試算日期
    ISSUE_DATE,    //投保始期
    PAY_KIND,    //繳別
    PROD_ID,    //險別

    //output
    SUM_PAY_PREM,    //總繳保險費
    SUM_PREM_FEE,    //累計保費費用
    SUM_CCZ,    //累計立即投資費用
    SUM_CC4,    //累計保險成本(危險保費)
    SUM_CC9,    //累計保單管理費
    cryAmtWithoutFee,    //部分提領金額
    BOU_DIV_L,    //現金配息
    NEW_POL_VAL,//保單帳戶價值
    PROFIT,    //投資損益
    TRY_CNT_DATE,    //試算日期
    INV_ROI,		//投資報酬率
    SUM_TOT_FEE,	//累計收取費用
    SUM_INV_COST	//總投資成本
}

