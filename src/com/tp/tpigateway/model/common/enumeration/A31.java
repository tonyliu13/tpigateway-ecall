package com.tp.tpigateway.model.common.enumeration;

// 查詢要保人停效保單
public enum A31 {
    // input
    APC_ID, // 要保人ID

    // output
    returnCode, // 
    
    detail, //
    /*** detail List START ****/
    SPC_KIND, // 是否有特殊紀錄
    INSD_NAME, // 被保人姓名
    EFT_CODE, // 契約效力
    IS_ABA008, // 是否利滾中
    IS_INV, // 是否投資型
    EXAM_DATE, // 需體檢日
    IS_ABA001, // 是否貸款
    POLICY_NO, // 保單號碼
    SUR_DATE, // 失效日
    IS_AUTO_PREM, // 是否墊繳中
    PROD_ID, // 險別代號
    PROD_NAME, // 險別中文名稱
    CLC_NO, // 收費代號
    LPS_DATE, // 停效日
    APC_NAME, // 要保人姓名
    /*** detail List END ****/
    
    returnMsg, //
    UUID, // 
    hostname, //
}
