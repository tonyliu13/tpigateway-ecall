package com.tp.tpigateway.model.common.enumeration;

//單一保單保費催繳紀錄
public enum D08 {
	// input
	POLICY_NO,		// 保單號碼，查詢該保單號碼之保費催繳記錄
	URGE_STR_DATE,	// 催繳日期(起)，預設值(查詢前二月(T-2)月1號)
	URGE_END_DATE,	// 催繳日期(迄)，預設值(查詢當月月底)

	// output
	//POLICY_NO,	// 保單號碼
	PAY_DATE_POL,	// 應繳日期(民國年月日)
	MAIL_DATE,		// 催繳交寄日(民國年月日)
	URGE_KIND,		// 催繳種類，如:投資型每月保價不足扣
	URGE_ADDR,		// 催繳地址
	URGE_AMT 		// 催繳金額，情境 (S026)-自主分析資料庫有該欄位

}
