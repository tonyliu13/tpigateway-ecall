package com.tp.tpigateway.model.common.enumeration;

//單一保單紅利提領紀錄
public enum A19 {
	// input
	POLICY_NO, // 保單號碼
	PREM, //保額
	PROD_KIND,// 保單類型
	INTENT, //INTENT代碼

	// output
	RTN_ADD_TOTAL_AMT, // 退還總金額 
	NEW_MAIN_PREM, // 新主約保費
	NEW_SPEC_PREM, // 新附約保費
	LOAN_AMT, // 部入借款
	LOAN_INT, // 借款利息
	LOAN_BAL, // 借款餘額
	EFT_DATE, // 生效日期
	BOTTOM_LIMIT, // 保額下限
	SHOW_ERROR_MSG, // 回覆訊息中文
	MSG_ID //回覆訊息代碼

}
