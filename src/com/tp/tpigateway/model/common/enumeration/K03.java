package com.tp.tpigateway.model.common.enumeration;

/**
 * 健檢醫院查詢
 * 
 */
public enum K03 {
	
	//input
	/** 地區 */
	AREA,
	/** 區域代碼*/
	CITY,
	
	//output
	/** 區域代碼 */
	ZIP_NAME,
	/** 建立人員 */
	INPUT_ID,
	/** 電話分機 */
	HOS_TELEXT,
	/** 醫院名稱 */
	HOS_NAME,
	/** 醫院電話 */
	HOS_TEL,
	/** 醫院地址 */
	HOS_ADDR
}
