package com.tp.tpigateway.model.common.enumeration;

//單一保單附約資訊查詢
public enum E07 {
	// input
	POLICY_NO, // 保單號碼 查詢該保單號碼下所有附約
	APLY_DATE, // 查詢日期

	// output
	RD_ID, // 特約種類 商品代號
	PROD_NAME, // 特約種類名稱 特約商品名稱
	ID, // 被保人ID
	ROLE_NAME, // 被保人姓名
	EFT_CODE, //有效表示(代碼)  有效表示 "只顯示有效表示為00(正常)、01(停效)、20(展期件)、23(繳清件) 1.當效力01時，查詢日-停效日>2年則不顯示"
	EFT_CODE_MEMO, //有效表示(中文)
	RD_AMT, // 表定保額 (=RD_AMT + RD_AMT_UNIT)
	RD_PREM, // 表定保費
	PREMIUM_ADD_DISC, // 下期應繳保費
	EFT_DATE, // 生效日期 民國年月日
	TERM_DATE, // 終止日期 民國年月日
	AMT_CEIL, //上限保額
	AMT_FLOOR //下限保額
}
