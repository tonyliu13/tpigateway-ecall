package com.tp.tpigateway.model.common.enumeration;

/**
 * VIP資格查詢
 * 
 */
public enum K02 {
	// input
	/** 要保人ID */
	ID,

	// output
	/**是否具健檢資格*/
	HealthType,
	/**是否補發過健檢單*/
	Reissue
}
