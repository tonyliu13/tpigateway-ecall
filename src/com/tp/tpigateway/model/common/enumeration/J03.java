package com.tp.tpigateway.model.common.enumeration;

//單一保單投資型提領明細
public enum J03 {
    //input
    POLICY_NO,  //保單號碼
    TRN_ITEM,   //交易種類

    //output
    APLY_DATE,	//受理日期
    RMT_DATE,	//匯款日期
    PAY_TYPE,	//給付方式
    PAY_TYPE_NM,//給付方式中文
    ACPT_NAME,	//受款人姓名
    ACPT_ID,	//受款人ID
    BANK_NO,	//行庫代號
    BANK_SN,	//行庫中文名稱
    ACNT_NO,	//匯款帳號
    NET_PAY_AMT	//實支金額
}

