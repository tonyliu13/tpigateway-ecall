package com.tp.tpigateway.model.common.enumeration;


//單一保單資訊
public enum A27 {
	//input
	POLICY_NO,	//保單號碼	

	//output
	BENE_TYPE_NM,	//受益人種類
	BENE_NAME,	//受益人姓名
	BENE_RATO,	//比例
	BENE_PRIO	//順位
}