package com.tp.tpigateway.model.common.enumeration;

// 地址檢核API
public enum D21 {
    // input
    ZIPCODE, // 3碼郵遞區號
    ADDRESS, // 地址
    USER_ID, // 使用者ID
    TRANS_NO, // 訂單編號
    TRANS_UUID, // 電商交易LOG編號
    ZS_UUID, // LOG編號(ZSweb會自己帶)

    // output
    resultCode,
    data,
    /*** data START ****/
    // ZIPCODE, 3碼郵遞區號
    // ADDRESS, 地址
    /*** data END ****/

}
