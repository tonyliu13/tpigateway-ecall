package com.tp.tpigateway.model.common.enumeration;

import java.util.HashMap;
import java.util.Map;

//單一保單資訊
public enum A17 {
	//input
	POLICY_NO,	//保單號碼	

	//output
	MESSAGE,	//回覆訊息中文	
	MESSAGE_ID,	//回覆訊息代碼
	SUR_TOTAL,	//解約金總額
	SUR_MAIN,	//主約解約金
	DVD_AMT,	//紅利金額
	MAIN_UN_PREM,	//主約未到期保費
	TOTAL_PREM,	//溢繳保費
	RD_UN_PREM,	//附約未到期保費
	DLY_PAY_INT,	//延付利息
	ADJ_PREM,		//調整保費
	SUR_RD_TOTAL,	//附約解約金總額
	INT_STR_DATE,	//計息起日
	INT_END_DATE,	//計息終日
	LOAN_AMT,	//貸款金額
	LOAN_INT,	//貸款利息
	LOAN_INT_REDUCE,	//貸款優惠利息
	LOAN_DLY_INT,	//貸款延滯息
	AUTO_PREM_AMT,	//墊繳金額
	AUTO_PREM_INT,	//墊繳利息
	AUTO_PREM_DLY,	//墊繳延滯息
	UN_CASH_PREM,	//未兌現保費
	IS_CASH,	//是否兌現
	TAX_AMT,	//印花稅
	DEB_AMT,	//聯名卡金額
	DVD_TAX,	//紅利所得稅
	DVD_INT,	//紅利利息
	BONUS_DEAD,	//死差紅利
	DLY_INT_TAX,	//延滯息所得稅
	DLY_INT_TAX_AMT,	//延滯息印花稅
	PAY_AMT,	//應付金額
	PAY_NET,	//實付金額
	RSV_MAIN,	//主約準備金
	RSV_TOTAL,	//準備金總額
	RSV_RD_TOTAL,	//附約準備金總額
	UN_CASH_LOAN,	//未兌現本金
	ACC_EXPENSE,	//帳戶費用款
	DVD_PAYUP_SUR,	//紅利增額繳清解約金
	TRANS_MSG,	//轉送訊息
	DVD_PAYUP_SUR_RD_TOTAL	//附約紅利增購保額解約金總額
	
}