package com.tp.tpigateway.model.common.enumeration;

//ID行銷資訊
public enum A04 {
	// input
	ID,

	// output
	ONLINE_CHANGE, // 網路服務 Y:有;N:無;X:無效
	IS_NETINSR, // 網路投保 Y:有;N:無;X:無效
	IS_RCPT, // 電子單據 Y:有;N:無;X:無效
	IS_MI, // 行動服務 Y:有;N:無;X:無效
	IS_BTWOC, // 會員網站 Y:有;N:無
	IS_EASY_CALL, // 易CALL保 Y:有;N:無
	FINGER, // 指定匯撥(一指通) 否/幣別中文，e.g. 否；台；澳、人、台、紐、美
	ATM, // ATM保貸 是/否/帳戶異常/X
}
