package com.tp.tpigateway.model.common.enumeration;

// 取得行政地區
public enum D27 {

    // output
    resultCode, //
    
    data,
    /*** data START ****/
    
    cityAreaList,
    /*** cityAreaList START ****/
    ZIPCODE, // 3碼郵遞區號
    AREA, // 縣市
    CITY, // 鄉鎮市區
    /*** cityAreaList END ****/
    /*** cityAreaList END ****/
}
