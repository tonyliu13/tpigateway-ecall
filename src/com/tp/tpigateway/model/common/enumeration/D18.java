package com.tp.tpigateway.model.common.enumeration;

//取得服務人員聯絡(歸戶)
public enum D18 {
	// input
	APC_ID, // 客戶ID(登入者)，查詢所有保單收費代號

	// output
    phone,  //服務人員聯絡電話
    email,   //服務人員email信箱
    channelId,
    name,  //服務人員姓名
    channelCat,
    policyNo,
    channel
}
