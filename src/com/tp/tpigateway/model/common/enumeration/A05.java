package com.tp.tpigateway.model.common.enumeration;

public enum A05 {
	// input
	// ID,

	// output
	POLICY_NO, // 保單號碼
	ROLE, // 契約角色 0:子女;A 要保人;C 配偶;I 被保人;P 父母;S 次被保人
	ID, // 客戶ID
	SEX, // 性別 1:男;2:女
	ROLE_NAME, // 姓名
	ROLE_BRDY, // 出生日期 西元年月日 yyyy-mm-dd
	NATION_CODE // 國籍 TW 中華民國;CN 中國大陸;JP 日本 ;VN 越南 ;US 美國

}
