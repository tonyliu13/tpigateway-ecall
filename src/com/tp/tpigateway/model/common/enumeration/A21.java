package com.tp.tpigateway.model.common.enumeration;

// 查詢ATM借還款記錄
public enum A21 {
	// input
	APC_ID, // 要保人ID
	S_DATE, // 查詢起始日
	E_DATE, // 查詢結束日
	
	// output
	/*** List START ****/
	TRN_CD_NAME, // 交易項目
	TRN_CD, // 交易項目代碼
	INT_AMT, // 利息
	TRN_AMT, // 交易金額
	TRN_DATE, // 交易日期
	TRN_TIME, // 交易時間
	TRN_NO, // 交易序號
	POLICY_NO, // 保單號碼
	PROD_NAME, // 險別中文名稱
	/*** List END ****/
}
