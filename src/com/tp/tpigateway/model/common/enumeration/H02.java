package com.tp.tpigateway.model.common.enumeration;

//查詢最近服務中心據點
public enum H02 {
	// input
//	LONGITUDE, // 經度
//	LATITUDE, // 緯度
	DATA_CNT, // 查詢筆數

	// output
	DIV_SHORT_NAME, // 服務中心名稱
	DIV_TEL1, // 單位電話1
	DIV_TEL2, // 單位電話2
	DIV_TEL3, // 單位電話3
	DIV_FAX1, // 單位傳真1
	DIV_FAX2, // 單位傳真2
	DIV_ITPHONE, // 網路電話
	DIV_ADDRESS, // 單位地址
	LONGITUDE, // 經度
	LATITUDE, // 緯度
}

