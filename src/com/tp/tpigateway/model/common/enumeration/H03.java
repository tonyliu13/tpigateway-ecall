package com.tp.tpigateway.model.common.enumeration;

import java.util.HashMap;
import java.util.Map;

//保費扣款狀態
public enum H03 {
	//input
	POLICY_NO,	//保單號碼

	//output
	CFM_CODE,	//授權書狀態	Y:有效件;N:受理中;O:歷史件;X:無效件
	DCT_EFT_DATE,	//扣款生效日	
	PAY_WAY_CODE,	//收費管道	1.人工件;2.扣薪件;3.轉帳;4.信用卡;5.國泰卡;6.自繳;7.自動墊繳;8.豁免件;9.郵撥件
	BANK_NAME_ACNT,	//行庫名稱	
	ACNT_NO,	//銀行帳號	末四碼
	BANK_NAME_CARD,	//發卡銀行	
	CARD_NO,	//信用卡號	末四碼
	AUTH_NAME,	//授權人姓名	
	AUTH_ID,	//授權人ＩＤ	
	D1,	//指定扣款日	日 (e.g. 25)
	ACNT_STS_CODE,	/*帳戶狀態	
						K0 待刷轉
						K1 刷轉不成功
						K2 授權被拒
						K3 過期卡
						K4 卡號錯誤
						K5 要求終止
						K6 超出限額
						K7 授權書審核中
						K8 授權書未寄達
						K9 個案處理中
						KA 未開卡
						KB 控管卡
						KC 終止確認中
						KD 終止書未寄達
						KE 停用卡／二扣不成功
						S0 待刷轉
						S1 存款不足
						S2 結清或靜止戶
						S3 銀行控管中
						S4 帳號錯誤或ＩＤ不符
						S5 要求終止
						S6 刷轉不成功
						S7 授權書審核中
						S8 授權書未寄達
						S9 委託代繳未建檔
						SA 終止確認中
						SB 個案處理中
						SC 其他副聯未用印
						SD 終止書未寄達
						SE 退件
						SF CSR系統新申請件但主機未入檔
						SG 失效件
						SS 新契約件等待保單成立
						ST 保全變更件等待保單成立*/
	MAIL_CODE,	//郵寄送金單	是否使用郵寄送金單
	NXT_TRN_DATE,	//預估下次扣款日	
	TRN_FAIL_TMS,	//失敗次數	
	FST_TRN_DATE,	//一扣日期	西元年月日 yyyy-mm-dd，無值給空白
	SEC_TRN_DATE,	//二扣日期	西元年月日 yyyy-mm-dd，無值給空白
	REC_STATUS,	//應收狀態	0:當期；1：落後批一；2：落後批二；3:超前有應收；4超前無應收
	NXT_PAY_DATE,	//下次應繳日	西元年月日 yyyy-mm-dd
	PREM_RCPT,	//下次應繳保費	
	TRN_TMS,	//已扣次數	
	PAY_DATE_CLC;	//繳費日期	
	
	private static Map<String,String> payWayCodeMap;
	
	private static final String[] PAY_WAY_CODE_ARRAY = {
		"1:人工件", "2:扣薪件", "3:轉帳", "4:信用卡", "5:國泰卡",
		"6:自繳", "7:自動墊繳", "8:豁免件", "9:郵撥件"
	};
	
	static {
		payWayCodeMap = new HashMap<String,String>();
		for(String s : PAY_WAY_CODE_ARRAY) {
			String[] arr = s.split(":");
			String code = arr[0];
			String desc = arr[1];
			payWayCodeMap.put(code, desc);
		}
	}
	
	public static String getPayWayCodeDesc(String PAY_WAY_CODE) {
		return payWayCodeMap.get(PAY_WAY_CODE);
	}

}
