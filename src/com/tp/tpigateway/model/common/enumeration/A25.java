package com.tp.tpigateway.model.common.enumeration;

import java.util.HashMap;
import java.util.Map;

//單一保單資訊
public enum A25 {
	//input
	POLICY_NO,	//保單號碼	

	//output
	MAT_AMT,	//滿期金	
	DVD_AMT,	//紅利金額	
	RTN_ADJ_PREM,	//退還調整保費	
	SUM_AMT,	//應付金額	
	ADV_OVER_AMT,	//本次扣回理賠金	
	LOAN_AMT,	//借款金額	
	LOAN_INT,	//借款利息	
	LOAN_DLY_INT,	//借款延滯息	
	AUTO_PREM_AMT,	//墊繳金額
	AUTO_PREM_INT,	//墊繳利息
	AUTO_PREM_DLY,	//墊繳延滯息
	UN_CASH_AMT,	//未兌現金額
	TAX_AMT,	//印花稅
	SUB_AMT,	//應扣金額
	PAY_NET,	//實支金額
	ACPT_NAME,	//受款人
	TACNT_DATE,	//帳務日期
	PAY_TYPE,	//給付方式
	ACPT_ACNT_NO,	//帳號
	ACPT_BANK_NO,	//銀行行庫中文名稱
	ACPT_PAY_NET	//個別受款人實支金額
	
}