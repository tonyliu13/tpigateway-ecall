package com.tp.tpigateway.model.common.enumeration;

//網投旅平險-不受歡迎旅遊地區
public enum C04 {
	// input
	ID,

	// output
	resultCode,
	data,
	/*** data START ****/
	isUNWELCOME, // 是否有被列入不受歡迎
	/*** data END ****/
}
