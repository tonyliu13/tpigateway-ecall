package com.tp.tpigateway.model.common.enumeration;

//查詢贖回受理進度
public enum J04 {
    //input
	APC_ID,    // 要保人ID
	TRN_ITEM,  // 交易種類
	Q_ALL,     // 是否要output全部正在提領保單

    //output
	POLICY_INFO_LIST,   // 受理進度資料列
	POLICY_NO_CNT, 		// 提領中保單件數
	POLICY_NO, 			// 保單號碼
	PROD_ID, 			// 險別
	PROD_SN, 			// 險別中文
	APLY_DATE, 			// 受理日期
	ESTIMATE_PAY_DATE, 	// 預估給付日期
	ESTIMATE_PAY_NET, 	// 預估給付金額
	CURR, 				// 幣別
	CURR_NM, 			// 幣別中文
	BANK_NO, 			// 行庫代號
	BANK_SN, 			// 行庫中文名稱
	ACNT_NO, 			// 匯款帳號
	COMPANY_IND  		// 商品原公司別 C:國泰人壽 G:國寶人壽 S:幸福人壽
}

