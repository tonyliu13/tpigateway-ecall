package com.tp.tpigateway.model.common.enumeration;

// 網投旅平險-核保試算(公會通算)
public enum C02 {

    // input
    TRANS_NO, // 網投專站訂單編號
    TRANS_UUID, // 阿發傳入session ID
    ZS_UUID,

    contractJSON,
    /*** contractJSON START ****/
    CLIENT_ID, // 使用者(SYS_NO:通路別 (阿發)  CB00)
    // TRANS_NO,  傳入網路投保訂單編號

    CONTRACT_DATA, // 契約內容單筆:JSONString(要保人及契約資料)
    /*** CONTRACT_DATA START ****/
    APC_ZIP_CODE, // 要保人地址
    B2C_APLY_NO, // 不須填
    ISSUE_DATE, // 投保始期
    POLICY_TYPE, // 保單型式 電子保單
    BGN_TIME, // 投保開始時間
    APC_NAME, // 要保人姓名
    APC_BRDY, // 要保人生日
    APC_ID, // 要保人ID
    APC_ADDR, // 要保人地址
    RGN_CODE, // 旅遊地區代碼,可call 旅遊地區API(getREG_CODE)取得全部
    APC_MOBILE, // 要保人電話
    ISSUE_DAYS, // 投保天數
    APC_ID_TYPE, // 要保人證件類型 1 身份證 2 統一證號  3 護照 
    APC_EMAIL, // 要保人email
    /*** CONTRACT_DATA END ****/

    INSD_DATA, // 投保內容多筆:JSONString(被保人清單  (阿發被保人資訊同要保人))
    /*** INSD_DATA START ****/
    INSD_NAME, // 被保人姓名
    ZIP_CODE, // 被保人郵遞區號
    HK_AMT, // 海外突發疾病保額
    HP_AMT, // 實支實付保額
    HD_AMT, // 死殘保額
    BENE_SHR, // 受益人受益分配 1 均分 2 比例 3 順位 (固定2.比例)
    MOBILE, // 被保人電話
    INSD_RLTN, // 與要保人關係 1 本人 (固定1.本人)
    INSD_ID, // 被保人ID
    INSD_BRDY, // 被保人生日
    EMAIL, // 被保人email 
    INSD_ID_TYPE, // 被保人證件類型 1 身份證 2 統一證號  3 護照 
    ADDR, // 被保人地址
    /*** INSD_DATA END ****/

    BENE_DATA, // 受益人內容多筆:JSONString(受益人資料)
    /*** BENE_DATA START ****/
    RELATION, // 與被保人關係 5 配偶 6 子女 7 父母 8 袓孫 2 法定繼承人 1 親屬 3 朋友 4 其他
    // ZIP_CODE,  郵遞區號(如為法繼，同要保人)
    BENE_ID, // 受益人ID (如為法繼，固定給0000000000)
    BENE_SER_NO, // 第N個受益人(固定給1)
    // INSD_ID,  對應之被保人(同要保人)
    TEL, // 電話(如為法繼，同要保人)
    BENE_ID_TYPE, // 證件類型 證件類型 1 身份證 2 統一證號  3 護照(如為法繼，給空字串) 
    BENE_RATO_BASE, // 百分比分母(固定給100)
    BENE_RATO_UP, // 百分比分子(固定給100)
    BENE_PRIO, // 選擇順位要填1,2,3(固定給0)
    // ADDR,  (如為法繼，同要保人)
    BENE_NAME, // 受益人姓名
    /*** BENE_DATA END ****/

    TRAL_TMP, // 業報書單筆:JSONString(財告)
    /*** TRAL_TMP START ****/
    INSR_ID, // 被保人ID
    APC_FAM_INCOME, // 被保人收入(固定給50)
    EXAMINE_A_SRC, // 是否為主要經濟來源 1 是,2否(固定給1)
    /*** TRAL_TMP END ****/
    /*** contractJSON END ****/
    
    // output
    resultCode, //
    
    data,
    /*** data START ****/
    // INSD_DATA, // 多筆
    /*** INSD_DATA START ****/
    // INSD_NAME, 
    // ZIP_CODE, 
    // HK_AMT, 
    // HP_AMT, 
    // HD_AMT, 
    // BENE_SHR, 
    // MOBILE, 
    // INSD_RLTN, 
    // INSD_ID, 
    // INSD_BRDY, 
    // EMAIL, 
    // INSD_ID_TYPE, 
    // ADDR, 
    OMTP_AMT, //專機   (0代表沒買，1代表有買)
    /*** INSD_DATA END ****/

    // TRANS_NO, 

    // BENE_DATA, // 多筆
    /*** BENE_DATA START ****/
    // RELATION, 
    // ZIP_CODE, 
    // BENE_ID, 
    // BENE_SER_NO, 
    // INSD_ID, 
    // TEL, 
    // BENE_ID_TYPE, 
    // BENE_RATO_BASE, 
    // BENE_RATO_UP, 
    // BENE_PRIO, 
    // ADDR, 
    // BENE_NAME, 
    /*** BENE_DATA END ****/

    // TRAL_TMP, // 單筆
    /*** TRAL_TMP START ****/
    // INSR_ID, 
    // APC_FAM_INCOME, 
    // EXAMINE_A_SRC, 
    /*** TRAL_TMP END ****/

    // CLIENT_ID, 

    // CONTRACT_DATA,  單筆
    /*** CONTRACT_DATA START ****/
    // APC_ZIP_CODE, 
    // B2C_APLY_NO, 
    // ISSUE_DATE, 
    // POLICY_TYPE, 
    // BGN_TIME, 
    // APC_NAME, 
    // APC_BRDY, 
    // APC_ID, 
    // APC_ADDR, 
    // RGN_CODE, 
    // APC_MOBILE, 
    // ISSUE_DAYS, 
    // APC_ID_TYPE, 
    // APC_EMAIL, 
    TOT_PREM, //
    APLY_NO,
    /*** CONTRACT_DATA END ****/
    /*** data END ****/
}
