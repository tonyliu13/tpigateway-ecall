package com.tp.tpigateway.model.common.enumeration;

public enum H05 {
	// input
    POLICY_NO_LIST, // 保單號碼清單

	// output
	POLICY_NO, // 保單號碼
    MNXT_PAY_DATE, // 下次應繳日 yyyy-mm-dd
    PREM_RCPT // 下次應繳保費
}
