package com.tp.tpigateway.model.common.enumeration;
/**
 * ID歸戶房貸資訊
 * @author i9300627
 *
 */
public enum S03 {
	// input
	/**貸款帳號*/
	LOAN_ID,
	/**借款人ID*/
	BORROW_ID,
	
	// output
	/**幣別*/
	CURR,
	/**期數*/
	PAY_TERM,
	/**應繳日期*/
	DUTY_PAYDT,
	/**繳款別*/
	PAY_KIND,
	/**實繳日期*/
	REAL_PAYDT,
	/**實繳金額*/
	REAL_PAYMT,
	/**繳款後本金餘額*/
	THIS_PRINCIPAL,
	/**繳款方式中文*/
	PAY_TYPE_NM,
	/**銀行帳號*/
	ACCOUNT,
	/**本金*/
	PRINCIPAL,
	/**利息*/
	INTEREST,
	/**延滯息*/
	DELAY_INT,
	/**違約金*/
	RENEGE,
	/**管理費*/
	LAWSUIT_FEE
}