package com.tp.tpigateway.model.common.enumeration;

public enum Rights {

    /** 生日 */
	BIRTHDAY,
	
	/** 契約停效 */
	TERM_POLICY,
	
	/** 滿期金 */
	MATURITY,
	
	/** 年金 */
	ANNUITY,
	
	/** 滿期金即將到期 */
	MATURITY_DUE,
	
	/** 年金即將到期 */
	ANNUITY_DUE,
}
