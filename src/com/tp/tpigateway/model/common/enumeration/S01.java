package com.tp.tpigateway.model.common.enumeration;
/**
 * ID歸戶房貸資訊
 * @author i9300627
 *
 */
public enum S01 {
	// input
	/**借款人ID*/
	BORROW_ID,

	// output
	IDList,
	/**非房貸客戶*/
	NOT_CUS,
	/**房貸餘額為0(皆清償)*/
	NOT_VALID,
	/**只有理財型*/
	ONLY_FIN,
	/**有理財型*/
	HAS_FIN,
	/**當月未繳息*/
	NOT_PAY,
	/**當月未繳息(收息控管)*/
	NOT_PAY_LIMIT,
	/**當月已繳息*/
	IS_PAID,
	/**至少1期已遲繳*/
	IS_DELAY,
	/**房貸有效*/
	IS_VALID,
	/**房貸催提*/
	IS_BAD,
	/**有分次動撥*/
	IS_SPR_REMIT,
	/**幣別*/
	CURR,
	/**分次動撥貸款帳號*/
	SPR_LOAN_ID,
	/**貸款帳號*/
	LOAN_ID,
	/**扣款日*/
	DEDUCT_DATE,
	/**契約效力*/
	LOAN_STATUS,
	/**貸款種類*/
	LOAN_KIND,
	/**下應繳日*/
	NEXT_PAYDT,
	/**繳款方式*/
	PAY_TYPE,
	/**擔保品地址*/
	GAGE_ADDR,
	/**收息控管*/
	GETINT_LIMIT,
	/**貸款餘額*/
	REMAIN_AMT,
	/**下期利率*/
	RATE_VAL
}