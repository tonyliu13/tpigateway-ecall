package com.tp.tpigateway.model.common.enumeration;

// 取得一次性碼(發OTP)
public enum Z01 {

    // input
    trnId, // 交易代號
    mobileNo, // 行動電話
    infmId, // 通知對象身分證字號
    infmName, // 通知對象姓名
    email, // 電子郵件地址

    // output
}
