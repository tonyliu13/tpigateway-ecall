package com.tp.tpigateway.model.common.enumeration;

// 記憶繳費(常用帳戶)查詢
public enum D22 {
    // input
    USER_ID, // 使用者ID
    SYS_NO, // 商品別
    TRANS_NO, // 訂單編號
    TRANS_UUID, // 電商交易LOG編號
    ZS_UUID, // ZS LOG編號

    // output
    resultCode,

    data,
    /*** data START ****/

    CARD_LIST,
    /*** CARD_LIST START ****/
    TYPE, // 資料種類
    BANK_NAME, // 銀行名稱
    CARD_NO, // 信用卡卡號
    CARD_VALD_YY, // 信用卡有效年
    CARD_VALD_MM, // 信用卡有效月
    /*** CARD_LIST END ****/

    ACNT_LIST,
    /*** ACNT_LIST START ****/
    // TYPE, 資料種類
    // BANK_NAME, 銀行名稱
    BANK_NO_TRN, // 行庫代號
    ACNT_NO_TRN, // 銀行帳號
    /*** ACNT_LIST END ****/
    /*** data END ****/
}
