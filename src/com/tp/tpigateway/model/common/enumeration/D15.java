package com.tp.tpigateway.model.common.enumeration;

//D15_自繳單網頁版
public enum D15 {
	// input
	POLICY_NO,	//保單號碼

	// output
	PAY_DATE_POL,	//應繳日期	 各繳次分開提供+檔名 
	ShortURL	//超連結	  各繳次分開提供

}
