package com.tp.tpigateway.model.common.enumeration;

//自繳單
public enum D14 {
	// input
	POLICY_NO,	//保單號碼

	// output
	downloadFileFullPath,	//檔案完整路徑	 path+檔名 
	downloadFileName,	//檔案名稱	下載到客戶端的顯示檔名
	rcptData,	//自繳單內容明細檔	List<AC_K0Z015_bo>
	PWD,	//0:無密碼;1:要保人ID	1

}
