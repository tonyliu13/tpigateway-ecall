package com.tp.tpigateway.model.common.enumeration;

/**
 * A14_險別查詢保全主約設定
 * 
 * @author i9300627
 */
public enum A14 {
	// input
	/** 險別 */
	PROD_ID,

	// output
	/** 可否辦理展期 */
	IS_EXIT,
	/** 可否辦理繳清 */
	IS_PAUP,
	/** 可否辦理縮小保額 */
	IS_DISC_AMT,
	/** 可否辦理增購保額 */
	IS_INCR_AMT,
	/** 可否辦理繳別變更 */
	IS_PAY_FREQ,
	/** 可否辦理延長年期 */
	IS_INCR_PRD,
	/** 可否辦理縮短年期 */
	IS_DISC_PRD,
	/** 是否可辦理紅利選擇變更 */
	IS_DVD_KIND,
	/** 可否辨理集體彙繳 */
	IS_GRUP_IND,
	/** 是否可辦理復效 */
	IS_ROCR,
	/*** resultList START ****/
	/** 繳別細項 */
	PAYFRQ_LIST,
	/** 月繳 */
	MONTH_PAY,
	/** 季繳 */
	SEASON_PAY,
	/** 半年繳 */
	HALFYEAR_PAY,
	/** 年繳 */
	ANNU_PAY,
	/** 躉繳 */
	SINGLE_PAY,
	/** 彈性繳 */
	FLEX_PAY
}
