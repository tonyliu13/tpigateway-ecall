package com.tp.tpigateway.model.common.enumeration;

// 網投旅平險-發送交易成功通知
public enum G08 {

    // input
    INFM_ID, // 核心會員ID
    MOBIL_NO, // 核心會員手機
    NAME, // 核心會員姓名
    EMAIL, // 核心mail         
    NATIONALITY, // 核心國籍
    IP, // client IP 
    SYS_NO, // 險種
    TOT_PREM, // 應繳保費
    POLICY_NO, // 保單號碼 
    RESPONSE_TM, // 扣款成立時間
    TRANS_NO, // 訂單編號
    PREM_NAME, // 商品名稱(可不傳) 

    // output
    resultCode,

    data,
}
