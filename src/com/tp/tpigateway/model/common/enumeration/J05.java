package com.tp.tpigateway.model.common.enumeration;

// 單一保單投資型商品分類
public enum J05 {
	// input
	PROD_ID, // 險別

	// output
	COMPANY_IND, // 商品原公司別
	POLICY_DUTY	// 產品保險責任
}
