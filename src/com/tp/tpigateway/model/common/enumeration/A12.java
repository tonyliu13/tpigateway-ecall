package com.tp.tpigateway.model.common.enumeration;

//單一保單附約試算終止.縮小
public enum A12 {
	// input
	POLICY_NO, 		// 保單號碼
	APLY_DATE, 		// 查詢日期
	mapList,
	/*** List START ****/
	RD_ID, 			// 商品代號
	ID, 			// 被保人ID
	AFT_RD_AMT, 	// 保額數值
	/*** List END ****/
	
	// output
	DTABL002_vo,
	/*** DTABL002_vo ***/
	CST_NAME   , // 被保人姓名
	/*** DTABL002_vo ***/
	showList,
	/*** showList ****/
	PROD_SNAME, // 特約種類
	RD_AMT_UNIT, //保額單位
	//AFT_RD_AMT+RD_AMT_UNIT, // 新附約保額
	AFT_RD_PREM, // 新附約表訂保費
	RTN_ADD_AMT, // 退(補)金額  / 退(補)總金額(不在showList內)
	/*** showList ****/
	
	MESSAGE, // 回覆訊息中文
	MESSAGE_ID, // 回覆訊息代碼	
	AMT_CEIL, // 上限保額
	AMT_FLOOR, // 下限保額
	
}

