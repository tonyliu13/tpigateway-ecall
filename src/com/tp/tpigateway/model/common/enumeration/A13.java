package com.tp.tpigateway.model.common.enumeration;

/**
 * A13_單一保單試算繳別變更
 * 
 * @author i9300627
 */
public enum A13 {
	// input
	/** 保單號碼 */
	POLICY_NO,
	/** 繳別 */
	PAY_FREQ,
	/** 保單類型 */
	PROD_KIND,
	/** 目標定期保費 */
	AFT_PREM,
	/** INTENT代碼 */
	INTENT,

	// output
	/** 新主約保費 */
	NEW_MAIN_PREM,
	/** 新附約保費 */
	NEW_SPEC_PREM,
	/** 補收總金額 */
	RTN_ADD_TOTAL_AMT,
	/** 生效日期 */
	EFT_DATE,
	/** 訊息中文 */
	SHOW_ERROR_MSG ,
	/** 保險費金額 */
	PERM,
	/** 保費上限 */
	TOP_LIMIT,
	/** 保費下限  */
	BOTTOM_LIMIT,
	/** 回覆訊息代碼 */
	MSG_ID
}
