package com.tp.tpigateway.model.common.enumeration;

// 查詢ATM借還款金額
public enum A29 {
	// input
	APC_ID, // 要保人ID
	
	// output
	LOAN_ABLE_AMT, // 合計可貸餘額
	LOAN_BAL_AMT, // 合計原貸金額
	STS_CODE, // 狀態表示
	MSG_TXT, // 回覆訊息中文
	MSG_CD, // 回覆訊息代碼

}
