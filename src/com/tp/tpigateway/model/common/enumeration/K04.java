package com.tp.tpigateway.model.common.enumeration;

/**
 * 中英文投保證明查詢
 * 
 */
public enum K04 {
	// input
	ID,						// 輸入ID (被保人)
	TYPE,					// 投保證明類別 1：壽險；2：意外/旅平險；3：歐盟申根版
	LAN,					// 版本 1：中文；2：英文
	CURRENT,				// 幣別 1：美金；2：歐元
	APC_NM,					// 被保人英文姓名 (保戶自行輸入)
	APC_PASSPORT,			// 被保人護照號碼 (保戶自行輸入)

	// output
	downloadFileFullPath,	// 檔案路徑
	downloadFileName		// 檔案名稱
}
