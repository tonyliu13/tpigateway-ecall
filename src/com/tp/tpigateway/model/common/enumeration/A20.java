package com.tp.tpigateway.model.common.enumeration;

/**
 * ID查詢可借金額
 * 
 * @author 楊宗翰
 */
public enum A20 {
	// input
	/** 要保人ID */
	ID,

	// output
	List,
	/** List:保單號碼 */
	POLICY_NO,
	/** List:險別名稱 */
	PROD_NAME,
	/** List:保單幣別 */
	CURR,
	/** List:原貸金額 */
	LOAN_BAL,
	/** List:借款利率 */
	LOAN_RATE,
	/** List:本次可貸金額 */
	LOAN_ABLE,
	/** List:要保人ID */
	APC_ID,
	/** List:要保人姓名 */
	APC_NAME,
	/** List:被保人姓名 */
	INSD_NAME,
	/** 尚可借金額換算為台幣金額 */
	LOAN_ABLE2NTD,
	/** 合計(約當台幣) */
	TOTAL_ABLE_AMT,
	/** 保單尚可貸金額 */
	LOAN_ABLE_,
	/** 保單張數 */
	_POL_CNT
}
