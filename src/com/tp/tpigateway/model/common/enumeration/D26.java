package com.tp.tpigateway.model.common.enumeration;

//單一保單試算墊繳清償繳款單
public enum D26 {
	// input
	//POLICY_NO,    // 保單號碼
    //AGNT_ID,      // 使用人員(ChatBot)
    //PAY_TYPE,     // 清償方式，預設全部清償； ("":全部清償、A:清償次數、B:清償金額 )
    //PAY_TIMES,    // 清償次數/金額，清償方式：清償次數(需輸入次數須小於墊繳次數)/清償金額(有最低墊繳金額清償限制)，若PAY_TYPE為全部清償，不需傳該欄位
    //PRT_TYPE,     // 繳款單類型，A:雲端繳款  B:紙本繳款單


    // output
    TOT_PREM,       // 合計金額
    CURR,           // 幣別
    RCV_VALD_DATE,  // 使用期限，單據開立出來的文件上使用期限
    PAY_DATA_URL,   // 雲端繳款單-URL
    downloadFileFullPath,   // 紙本繳款單檔案路徑-PDF
    downloadFileName        // 紙本繳款單檔案名稱-PDF
    }
