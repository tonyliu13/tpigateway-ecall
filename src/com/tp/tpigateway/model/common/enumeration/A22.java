package com.tp.tpigateway.model.common.enumeration;

//單一保單查詢借還款記錄
public enum A22 {
	// input
	POLICY_NO, // 保單號碼
	TYPE, // 分類

	// output
	resultList,
	/*** resultList START ****/
	TRN_KIND, // 交易種類(分類)
	ACNT_DATE, // 帳務日期
	TRN_AMT, // 交易金額
	PAY_KIND, // 給付/償還方式
	RTN_LOAN_AMT, // 收回金額
	/*** resultList END ****/

	LOAN_BAL, // 原借款金額
	LOAN_RATE // 借款利率
 
 
}
