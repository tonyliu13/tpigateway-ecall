package com.tp.tpigateway.model.common.enumeration;

/**
 * ID歸戶理財型房貸資訊
 */
public enum S06 {
    // input
    /**借款人ID*/
    BORROW_ID,

    // output
    /**有正常理財型                      Y是 ：以借款人ID比對理財型主檔資料，有一筆契約效力<10且S06[使用狀態]=正常;  N否*/
    IS_STATUS0,
    /**滾爆型                                  Y是 ：以借款人ID比對理財型主檔資料，每一筆S06[使用狀態]=滾爆 ;  N否 */
    IS_STATUS4,
    /**無理財型                               Y是：以借款人ID比對理財型主檔無資料，或無房貸資料，或每一筆S06[契約效力]>16 ;  N否*/
    NO_X,
    /**理財型房貸皆清償               Y是 ：以借款人ID比對理財型主檔資料，每一筆契約效力皆為11~16;  N否*/
    IS_PAYOFF,
    /**非正常件(不含滾爆)   Y是：以借款人ID比對理財型主檔資料，每一筆S06[使用狀態]=1或2或3或5 ;  N否*/
    NOT_STATUS04,

    /**理財型房貸資訊清單 */
    LOAN_X_LIST,

    /**幣別*/
    CURR,
    /**貸款帳號*/
    LOAN_ID,
    /**貸款種類*/
    LOAN_KIND,
    /**契約效力*/
    LOAN_STATUS,
    /**理財型額度*/
    MM_AMT,
    /**已動撥額度*/
    REMAIN_AMT,
    /**可動撥餘額*/
    USE_AMT,
    /**理財型連結帳號*/
    MM_ACCOUNT,
    /**下期利率*/
    RATE_VAL,
    /**使用狀態*/
    MM_STATUS

}
