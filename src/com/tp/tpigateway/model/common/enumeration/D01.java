package com.tp.tpigateway.model.common.enumeration;

//保單扣款不成狀況
public enum D01 {
	// input
	POLICY_NO, // 保單號碼

	// output
	BANK_NAME, // 轉帳銀行
	ACNT_NO, // 帳戶號碼
	AUTH_ID, // 授權人ID
	DCT_BANK_DATE, // 轉帳扣款日期
	FAIL_REASON, // 轉帳失敗原因
	FAIL_TMS, // 失敗次數
	CARD_NO // 卡號
}
