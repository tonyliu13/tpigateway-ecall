package com.tp.tpigateway.model.common.enumeration;

//單一保單最近一期保費優惠查詢
public enum D09 {
	// input
	POLICY_NO, 		// 保單號碼

	// output
	PAY_WAY_CODE, 	// 收費管道。1.人工件;2.扣薪件;5.國泰卡;6.自繳;9.郵撥件;
				  	// 			A.ATM轉帳;D.網路信用卡;E.網路金融卡;K.人員件虛擬帳號;L.線上投保_信用卡;P.郵局特戶存款;Q.QRcode_信用卡;
				 	// 			3.轉帳;4.信用卡;7.自動墊繳;8.豁免件;
				  	// 			B.滿期金回流;C.年金回流;I.保全_線上刷卡;J.保全MI_虛擬帳號;M.線上投保_本人帳戶扣款;N.線上投保_虛擬帳號;Z.國寶幸福_其他帳款源
	CLC_NO, 	  	// 收費代號
	GROUP_NO,		// 團體代號
	DISC_IDTY, 		// 優惠身分。0:一般;1:員工;2:員工扣薪_非本人;3:集團員工扣薪_本人;4:集團員工扣薪_非本人;5:業專;6:國泰卡;7:非國泰卡;8:集彙件
	dataList,
	/*****dataList***/
	PROD_CAT, 		// 商品型別，如:主約(PROD_CAT=1)、附約(PROD_CAT=2)
	RATE_DISC_W,	// 管道，管道優惠比率(查無資料為無值)
	RATE_DISC_H, 	// 高保費，高保費優待比率(查無資料為無值)
	PROD_SNAME, 	// 中文名稱，主約或附約險別中文名稱
	INSD_NAME, 		// 被保人姓名，身份證字號ID轉換為中文
	PREMIUM_ADD, 	// 表定保費，表定保費(含加費)
	PREM_DISC  		// 優待保費
	/****dataList***/

}
