package com.tp.tpigateway.model.common.enumeration;

// 公會資料取回(非同步)
public enum Z04 {

    // input
    ID, // 使用者ID
    TRANS_NO, // 訂單編號
    SRC, // 系統別

    // output
    resultCode, //
    data,
}
