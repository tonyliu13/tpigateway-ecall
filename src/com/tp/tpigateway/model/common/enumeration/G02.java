package com.tp.tpigateway.model.common.enumeration;

//保單驗證
public enum G02 {
	// input
	//TYPE,	//驗證類型	"0"  [0:一般壽險   3:旅平險/傷害險   2:團險  1:房貸戶]
	POLICY_NO,	//保單號碼	保單號碼 /(房貸戶) 房貸帳號 / (團險戶)保單號碼或公司統編 


	// output
	STATUS,	//是否成功保單驗證	[Y/N]
	TYPE	//驗證類型	(Array)[0:壽險 1:房貸 2:團險 3:旅平險 4:已有核心網路服務資格 5:自然人憑證開通]

}
