package com.tp.tpigateway.model.common.enumeration;

//單一保單查詢附約繳費記錄
public enum D06 {
	// input
	POLICY_NO, // 保單號碼
	RCPT_SER_NO, // 單據序號
	PAY_TIMES, // 繳次 可從D02取得

	// output
	PROD_ID, // 險別代號
	PROD_NAME, // 險別中文名稱
	RD_ID, // 特約種類 "特約商品名稱+商品代號 主約、附約代碼轉換成：特約商品名稱"
	RD_ID_NM, // 特約種類中文
	ID, // 被保人ID
	INSD_NM, // 被保人姓名 ID轉換成被保人姓名
	PREM_RCPT // 已繳金額 應繳保費

}
