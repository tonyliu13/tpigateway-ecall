package com.tp.tpigateway.model.common.enumeration;

public enum H01 {
	// input
	ID, // ID
	NATION_CODE, // 國籍 若國籍為空值，帶入空值(null)即可
	SCENARIO_CODE, // 情境代碼 如:S004

	// output欄位名稱
	POLICY_NO, // 保單號碼
	APC_ID, // 要保人ID
	APC_NM, // 要保人姓名
	INSD_ID, // 被保人ID
	INSD_NM, // 被保人姓名
	PROD_NAME, // 險別中文名稱
	MNXT_PAY_DATE, // 下次應繳日 西元年月日
	PREM_RCPT, // 下次應繳保費
	PAY_FREQ, // 繳別 1:月繳、2:季繳、3:半年繳、4:年繳、5:躉繳、6:彈性繳
	PAY_FREQ_NM, // 繳別中文 月繳、季繳、半年繳、年繳、躉繳、彈性繳
	CXL_PROD_KIND, // 保單類別 (官網保單資料)
	EFT_CODE, // 契約效力
	EFT_CODE_CH, // 契約效力中文 H02的EFT_CODE_NM
	IS_MLY, // 是否墊繳 Y/N
	IS_LAP, // 是否停效 Y/N
	//PREM_TRN_CODE, // 繳費管道
	MLY_PAY_DATE, // 墊繳下次應繳日
	LPS_DATE, // 停效日
	CURR,	  //幣別代碼
	CURR_CH,	 //幣別中文
	KIND_CODE,	//繳費方式	1.人工件;2.扣薪件;3.轉帳;4.信用卡;5.國泰卡;6.自繳;9.郵撥件;
	AUTO_PAY_CODE	//自繳資格碼	Y/N

}
