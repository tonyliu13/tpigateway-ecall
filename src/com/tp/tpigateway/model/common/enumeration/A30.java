package com.tp.tpigateway.model.common.enumeration;

// 查詢W001(會員資料)
public enum A30 {
    // input
    APC_ID, // 會員ID
    NATION_CODE, // 國籍
    TRANS_NO, // 電商訂單編號
    TRANS_UUID, // 電商交易LOG編號
    ZS_UUID, // ZS LOG編號

    // output
    resultCode, data,
    /*** data START ****/
    apc_id, // 申請人ID
    apc_name, //   申請人姓名
    apc_brdy, //    申請人出日
    htel_area, //   住家電話_區碼
    htel_num, //   住家電話_號碼
    htel_ext, //   住家電話_分機
    ctel_area, //   公司電話_區碼
    ctel_num, //   公司電話_號碼
    ctel_ext, //   公司電話_分機
    mobl_no, //行動電話
    email, // E_MAIL
    is_trn, // 是否同意保單線上服務
    is_rcpt, //是否同意電子單據服務
    is_acnt_no, // 是否指定匯撥帳號
    bank_no, //行庫代號
    acnt_no, //匯款帳號
    trn_pwd, //交易碼
    is_rset_pwd, // 交易碼是否已重設
    is_lock_user, //  帳號是否停用
    pwd_err_tms, // 交易碼錯誤次數
    pwd_upd_date, //  交易碼最後修改日期
    pwd_end_date, //   交易碼到期日期
    PWD_VRY_TIME, //   交易碼最後驗譣失敗時間
    is_callin, // 是否同意電話線上變更
    callin_pwd, // 電話變更交易碼
    is_call_reset, // 電話變更交易碼是否已重設
    call_err_tms, //  電話變更交易碼錯誤次數
    is_lock_call, //  電話變更是否停用
    CALL_UPD_DATE, //  電話交易碼最後修改日期
    CALL_END_DATE, //  電話交易碼到期日期
    CALLIN_VRY_TIME, //電話交易碼最後驗證失敗時間
    is_mi, // 是否同意行動櫃檯服務
    is_netinsr, // 是否同意網路投保
    netinsr_lv, // 網路投保等級
    email_cfm, //  EMAIL是否已驗證
    review_date, //網路投保審閱期
    is_wait_chk, //行動保全資格待確認
    hcountry_code, //  住家電話_國際碼
    ccountry_code, //  公司電話_國際碼
    mcountry_code, //  行動電話_國際碼
    nation_code, // 國籍
    ab_aply_no, // 保全變更受理編號
    /*** data END ****/

}
