package com.tp.tpigateway.model.common.enumeration;

// 滿期年金將到期查詢
public enum A33 {

    // input
    ID, //ID
    ROLE, //腳色(String[ ]，  [A要保人  I被保人    BENE受益人]     可傳多個身分)
    Period, //天數(EX. 30天後將到期)
    MAT_ANTY_CODE, //查詢類別(ALL全部、B滿期金、D年金)

    // output
    ACNT_NO, // 一指通帳號末四碼
    BANK_NO, // 行庫代號
    BENE_NAME, // 受益人姓名
    CLC_NO, // 收費代號
    IS_ABA001, // 是否保單貸款
    IS_ABA008, // 是否利滾中
    IS_AUTO_PREM, // 是否墊繳中
    IS_ONE_ACC, // 是否申辦一指通(N、IS_ID、IS_POLICYNO)
    PAY_AMT, // 應領金額
    PAY_DATE, // 應領日期
    PAY_KIND, // 給付種類(ABB002=滿期金   ABD002=年金)
    POLICY_NO, // 保單號碼
    SPC_KIND, // 特殊記錄
    PROD_ID, // 險別代號
    APC_ID, // 要保人ID
    INSD_ID, // 被保人ID
    CURR, // 幣別
    PROD_SNAME, // 險別中文簡稱
    APC_NAME, // 要保人姓名
    INSD_NAME, // 被保人姓名

}
