package com.tp.tpigateway.model.common.enumeration;


/*
 * 註:pay_way_code 目前只會回中文回來
 */

//保單近6期繳費記錄
public enum D02 {
	//input
	POLICY_NO,	//保單號碼

	//output
	BANK_NAME_TRN, // 行庫名稱
	ACNT_NO_TRN, // 銀行帳號 末四碼
	BANK_NAME_CARD, // 發卡銀行
	CARD_NO, // 信用卡號 末四碼
	AUTH_NAME, // 授權人姓名
	AUTH_ID, // 授權人ＩＤ
	PAY_TIMES, // 繳次
	PAY_DATE_POL, // 應繳日 西元年月日 yyyy-mm-dd
	TOT_PREM, // 繳費金額
	PAY_DATE_CLC, // 繳費日期 實際繳費日期(西元年月日 yyyy-mm-dd)
	acnt_date, // 作帳日期 公司入帳日期（西元年月日 yyyy-mm-dd）
	PAY_WAY_CODE, // 繳費管道 1.人工件;2.扣薪件;5.國泰卡;6.自繳;9.郵撥件;A.ATM
					// 轉帳;D.網路信用卡;E.網路金融卡;K.人員件虛擬帳號;L.線上投保_信用卡;P.郵局特戶存款;Q.QRcode_信用卡;3.轉帳;4.信用卡;7.自動墊繳;8.豁免件;B.滿期金回流;C.年金回流;I.保全_線上刷卡;J.保全MI_虛擬帳號;M.線上投保_本人帳戶扣款;N.線上投保_虛擬帳號;Z.國寶幸福_其他帳款源
	pay_way_NM, // 繳費管道中文 如信用卡、轉帳、人工件、自繳等
	RCPT_SER_NO // 單據序號
}
