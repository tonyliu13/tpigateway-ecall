package com.tp.tpigateway.model.common.enumeration;

// 扣款API
public enum D20 {

    // input
    PAY_WAY_CODE, // 繳費管道
    USER_ID, // 使用者ID
    AUTH_NAME, // 授權人姓名
    POLICY_NO, // 保單號碼
    TRN_AMT, // 金額
    CARD_NO, // 信用卡卡號
    CARD_END_YM, // 信用卡到期年月
    BANK_NO_TRN, // 行庫代號
    ACNT_NO_TRN, // 銀行帳號
    PROD_ID, // 險別
    APLY_NO, // 受理編號
    NEXT_PAY_AUTH, // 是否產生續期授權書
    SAME_FIRST_PAY, // 是否同首期繳費方式
    BANK_NO_Next, // 續期行庫代號
    ACCOUNT_NO_Next, // 續期銀行帳號
    SYS_NO, // 網路投保商品別
    TRANS_NO, // 訂單編號
    TRANS_UUID, // 電商交易LOG編號
    ZS_UUID, // ZS LOG編號

    // output
    resultCode, //
    resultMsg, //

    data,
    /*** data START ****/
    TRN_SER_NO, // 交易序號
    STORE_ID, // 特店代號或虛擬帳號前四碼
    RESPONSE_TM, // 銀行回覆時間
    RETURN_CODE, // 銀行原始回饋碼
    RETURN_MEMO, // 註記
    CARD_AUTH_CODE, // 信用卡授權碼
    IS_CATHAY, // 是否為國泰卡
    IS_NCCC, // 是否NCCC 收單
    AUTH_NO_Next, // 續期授權書編號
    PAY_WAY_CODE_Next, // 續期繳費方式
    SHOW_MSG, // 客戶顯示訊息
    ACNT_DATE, // 帳務日期
    /*** data END ****/
}
