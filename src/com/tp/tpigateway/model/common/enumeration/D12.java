package com.tp.tpigateway.model.common.enumeration;

//契約狀況一覽表
public enum D12 {
	// input
	ID, // 輸入ID (要保人)
	USER_ID,	// 列印者ID
	USER_NAME,	// 列印者姓名
	APC_ID,	// 要保人ID
	IP,	// IP

	// output
	downloadFileFullPath, // 檔案完整路徑
	downloadFileName, // 檔案名稱
	PWD, // 0:無密碼;1:要保人ID
	lts	// 檔案有效時間(ZR.FileDownload)
}
