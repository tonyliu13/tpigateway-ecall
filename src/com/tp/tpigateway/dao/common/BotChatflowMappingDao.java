package com.tp.tpigateway.dao.common;

import java.util.Map;



/**
 * 呼叫API log紀錄資料表介面
 */
public interface BotChatflowMappingDao {

	/**
	 * 新增一筆
	 * 
	 * @param log
	 */
	public Map<String, String> getChatflowMapping();

}
