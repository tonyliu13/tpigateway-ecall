package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotLink;

/**
 * BotLink連結設定
 */
public interface BotLinkDao {
	
	/**
	 * 查詢全部
	 * 
	 * @return
	 */
	public List<BotLink> findAll();
	
	/**
	 * 批次寫入
	 * 
	 * @param botLinks
	 */
	public void batchInsert(List<BotLink> botLinks);
	


	/**
	 * 查詢連結設定
	 * 
	 * @param linkID
	 */
	public BotLink findByLinkID(String linkID);

}
