package com.tp.tpigateway.dao.common;

import com.tp.tpigateway.model.common.BotCardStyle;

public interface BotCardStyleDAO {

	/**
	 * 查詢
	 * 
	 * @param styleId
	 *            樣式Id
	 * @return
	 */
	public BotCardStyle findByStyleId(int styleId);
}
