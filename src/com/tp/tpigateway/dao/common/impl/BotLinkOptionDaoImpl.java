package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotLinkOptionDao;
import com.tp.tpigateway.model.common.BotLinkOption;
import com.tp.tpigateway.model.common.BotLinkOptionId;

/**
 * 連結選項
 */
@Repository("botLinkOptionDao")
@Transactional
public class BotLinkOptionDaoImpl extends AbstractDAO<BotLinkOptionId, BotLinkOption> implements BotLinkOptionDao {
	private static Logger log = LoggerFactory.getLogger(BotLinkOptionDaoImpl.class);

	@Override
	public void batchInsert(List<BotLinkOption> botLinkOptions) {
		batchInsert(botLinkOptions);		
	}

	
	
	@Override
	public List<BotLinkOption> findByOptionID(String optionid) {
		String sql = "SELECT * FROM public.bot_link_option where option_id = '"+optionid+"'"+" ORDER BY option_seq";
		log.debug("SQL:"+ sql);
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(BotLinkOption.class);
		if(query.list()==null || query.list().isEmpty() || query.list().size()==0 ) {
			log.error("Table Name=[public.bot_link_option]中option_id="+optionid+"查無資料");
		}
		List<BotLinkOption> results = query.list();
		return results;
	}
	

	
}
