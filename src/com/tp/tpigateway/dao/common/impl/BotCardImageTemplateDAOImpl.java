package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCardImageTemplateDAO;
import com.tp.tpigateway.model.common.BotCardImageTemplate;

@Repository("botCardImageTemplateDAO")
@Transactional
public class BotCardImageTemplateDAOImpl extends AbstractDAO<String, BotCardImageTemplate>
		implements BotCardImageTemplateDAO {

	private static final Logger log = LoggerFactory.getLogger(BotCardImageTemplateDAOImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	private static final String sql_findByImageId = "SELECT * FROM public.bot_card_image_template WHERE image_id = :image_id";

	@Override
	public BotCardImageTemplate findByImageId(String imageId) {
		if (isInfo) {
			log.info("SQL:[" + sql_findByImageId + "]");
			log.info("parameter:[image_id=" + imageId + "]");
		}

		SQLQuery query = getSession().createSQLQuery(sql_findByImageId);
		query.setString("image_id", imageId);
		query.addEntity(BotCardImageTemplate.class);

		List<BotCardImageTemplate> botCardImageTemplates = query.list();
		if (botCardImageTemplates == null || botCardImageTemplates.isEmpty()) {
			return null;
		}
		return botCardImageTemplates.get(0);
	}
}
