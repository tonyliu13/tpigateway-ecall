package com.tp.tpigateway.dao.common.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotTpiCallLogDao;
import com.tp.tpigateway.model.common.BotRequestAndResponse;
import com.tp.tpigateway.model.common.BotTpiCallLog;
import com.tp.tpigateway.model.common.Entity;
import com.tp.tpigateway.model.common.Intent;

/**
 * 呼叫API log紀錄資料表貫作
 */
@Repository("botTpiCallLogDao")
public class BotTpiCallLogDaoImpl extends AbstractDAO<Integer, BotTpiCallLog> implements BotTpiCallLogDao {

	public void save(BotTpiCallLog log) {
		persist(log);
	}
	@Override
	public BotRequestAndResponse queryByChatID(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd) {
		
//		String hql = "FROM BotTpiCallLog l where l.insertTime >= '2012-12-12 01:01:01.000000' and l.insertTime <= '2018-12-12 01:01:01.000000'";
//		Query query = getSession().createQuery(hql);
		
		//記錄項目 :
		//	1. BOT 輸入
		//	2. INTENT 分析結果
		//	3. CHATFLOW 走向
		//	4. API 輸入參數
		//	5. API 輸出
		//	6. BOT 輸出(Response)		
		
		BotRequestAndResponse returnBotRequestAndResponse = new BotRequestAndResponse();
		returnBotRequestAndResponse.setRequestContent(queryRequest(customerID, chatID, insertTimeStart, insertTimeEnd));
		returnBotRequestAndResponse.setResponseContent(queryResponse(customerID, chatID, insertTimeStart, insertTimeEnd));
		returnBotRequestAndResponse.setCallLogList(queryCallLog(customerID, chatID, insertTimeStart, insertTimeEnd));
		returnBotRequestAndResponse.setIntentList(queryIntentList(customerID, chatID, insertTimeStart, insertTimeEnd));
		returnBotRequestAndResponse.setEntityList(queryEntityList(customerID, chatID, insertTimeStart, insertTimeEnd));
		return returnBotRequestAndResponse;
	}
	
	private static Logger log = LoggerFactory.getLogger(BotTpiCallLogDaoImpl.class);
	
	private List<BotTpiCallLog> queryCallLog(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd) {
		
		log.debug(" queryCallLog customerID ==>" + customerID);
		log.debug(" queryCallLog chatID ==>" + chatID);
		log.debug(" queryCallLog insertTimeStart ==>" + insertTimeStart);
		log.debug(" queryCallLog insertTimeEnd ==>" + insertTimeEnd);
		
		boolean isCustomerIdExists = false;
		boolean isChatIdExists = false;
		StringBuffer hql = new StringBuffer();
		hql.append("FROM BotTpiCallLog l where 1=1");
		
		if (customerID != null && "NULL".equals(customerID.trim().toUpperCase()) == false) {
			isCustomerIdExists = true;
			hql.append(" and l.custId = :custId");
		}
		if (chatID != null && "NULL".equals(chatID.trim().toUpperCase()) == false) {
			isChatIdExists = true;
			hql.append(" and l.chatId = :chatId");
		}
		if (insertTimeStart != null) {
			hql.append(" and l.insertTime >= :insertTimeStart"); 
		}
		if (insertTimeEnd != null) {
			hql.append(" and l.insertTime <= :insertTimeEnd");
		}
		if (insertTimeEnd != null) {
			hql.append(" and l.insertTime <= :insertTimeEnd");
		}
		hql.append(" order by l.insertTime desc");
		/*
		if(insertTimeStart != null && "2012-12-12 01:01:01.000000".equals(insertTimeStart) == false) {
			hql.append(" and l.insertTime >= :insertTimeStart");
		}
		if(insertTimeEnd != null && "2012-12-12 01:01:01.000000".equals(insertTimeEnd) == false) {
			hql.append(" and l.insertTime <= :insertTimeEnd");
		}
		*/
		
		log.info("hql=" + hql.toString());
		
		Query query = getSession().createQuery(hql.toString());
		
		if (isCustomerIdExists) {
			query.setString("custId", customerID);
			log.info("SET custId=" + customerID);
		}
		if (isChatIdExists) {
			query.setString("chatId", chatID);
			log.info("SETchatId" + chatID);
		}
		if (insertTimeStart != null) {
			query.setTimestamp("insertTimeStart", insertTimeStart);
		}
		if (insertTimeEnd != null) {
			query.setTimestamp("insertTimeEnd", insertTimeEnd);
		}
		/*
		if(insertTimeStart != null && "2012-12-12 01:01:01.000000".equals(insertTimeStart) == false) {
			query.setTimestamp("insertTimeStart", insertTimeStart);
			log.info("SET insertTimeStart=" + insertTimeStart);
		}
		if(insertTimeEnd != null && "2012-12-12 01:01:01.000000".equals(insertTimeEnd) == false) {
			query.setTimestamp("insertTimeEnd", insertTimeEnd);
			log.info("SET insertTimeEnd=" + insertTimeEnd);
		}
		*/
		@SuppressWarnings("unchecked")
		List<BotTpiCallLog> callLogList = query.list();		
		
		log.info("size=" + callLogList.size());
		
		return callLogList;
	}
	
	
	private String queryRequest(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd) {
		//TODO
		return "";
	}
	
	private String queryResponse(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd) {
		//TODO
		return "";
	}
	
	private List<Intent> queryIntentList(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd) {
		//TODO
		return new ArrayList<Intent>();
	}
	
	private List<Entity> queryEntityList(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd) {
		//TODO
		return new ArrayList<Entity>();
	}
	
}
