package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCardImageTextTemplateDAO;
import com.tp.tpigateway.model.common.BotCardImageTemplate;
import com.tp.tpigateway.model.common.BotCardImageTextTemplate;

@Repository("botCardImageTextTemplateDAO")
@Transactional
public class BotCardImageTextTemplateDAOImpl extends AbstractDAO<String, BotCardImageTextTemplate>
		implements BotCardImageTextTemplateDAO {

	private static final Logger log = LoggerFactory.getLogger(BotCardImageTextTemplateDAOImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	private static final String sql_findByImageTextId = "SELECT * FROM public.bot_card_image_text_template WHERE image_text_id = :image_text_id ORDER BY seq";

	@Override
	public List<BotCardImageTextTemplate> findByImageTextId(String imageTextId) {
		if (isInfo) {
			log.info("SQL:[" + sql_findByImageTextId + "]");
			log.info("parameter:[image_text_id=" + imageTextId + "]");
		}

		SQLQuery query = getSession().createSQLQuery(sql_findByImageTextId);
		query.setString("image_text_id", imageTextId);
		query.addEntity(BotCardImageTextTemplate.class);

		return query.list();
	}
}
