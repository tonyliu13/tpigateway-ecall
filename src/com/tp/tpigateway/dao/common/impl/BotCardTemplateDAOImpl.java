package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCardTemplateDAO;
import com.tp.tpigateway.model.common.BotCardTemplate;
import com.tp.tpigateway.model.common.BotCardTemplateId;

@Repository("botCardTemplateDAO")
@Transactional
public class BotCardTemplateDAOImpl extends AbstractDAO<BotCardTemplateId, BotCardTemplate>
		implements BotCardTemplateDAO {

	private static final Logger log = LoggerFactory.getLogger(BotCardTemplateDAOImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	private static final String sql_findByCardId = "SELECT * FROM public.bot_card_template WHERE card_id = :card_id ORDER BY seq";

	@Override
	public List<BotCardTemplate> findByCardId(String cardId) {
		if (isInfo) {
			log.info("SQL:[" + sql_findByCardId + "]");
			log.info("parameter:[card_id=" + cardId + "]");
		}

		SQLQuery query = getSession().createSQLQuery(sql_findByCardId);
		query.setString("card_id", cardId);
		query.addEntity(BotCardTemplate.class);

		return query.list();
	}
}
