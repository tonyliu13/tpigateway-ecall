package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotReplyContentDao;
import com.tp.tpigateway.model.common.BotReplyContent;
import com.tp.tpigateway.model.common.BotReplyContentId;
import com.tp.tpigateway.model.common.BotTpiReplyText;

/**
 * TPI回覆內容資料表實作
 */

@Repository("botReplyContentDao")
@Transactional
public class BotReplyContentDaoImpl extends AbstractDAO<BotReplyContentId, BotTpiReplyText> implements BotReplyContentDao {
	private static Logger log = LoggerFactory.getLogger(BotReplyContentDaoImpl.class);

	@Override
	public void batchInsert(List<BotReplyContent> botTpiReplyContents) {
		batchInsert(botTpiReplyContents);		
	}

	@Override
	public List<BotReplyContent> findByRespNodeId(String respnodeid) {
		String sql = "SELECT * FROM public.bot_reply_content where resp_node_id = '"+respnodeid+"'"+" ORDER BY resp_seq";
		log.debug("SQL:"+ sql);		
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(BotReplyContent.class);
		//query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		if(query.list()==null || query.list().isEmpty() || query.list().size()==0 ) {
			log.error("Table Name=[public.bot_reply_content]中resp_node_id="+respnodeid+"查無資料");
		}
		List<BotReplyContent> results = query.list();
		return results;
	}

	@Override
	public int removeAllByRespNodeId(String respnodeid) {
		String sql = "SELECT * FROM public.bot_reply_content where resp_node_id = '"+respnodeid+"'";
		SQLQuery query = getSession().createSQLQuery(sql);
		return query.executeUpdate();
	}
}
