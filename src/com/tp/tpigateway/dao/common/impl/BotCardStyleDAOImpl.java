package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCardStyleDAO;
import com.tp.tpigateway.model.common.BotCardStyle;
import com.tp.tpigateway.model.common.BotCardStyleId;

@Repository("botCardStyleDAO")
@Transactional
public class BotCardStyleDAOImpl extends AbstractDAO<BotCardStyleId, BotCardStyle> implements BotCardStyleDAO {

	private static final Logger log = LoggerFactory.getLogger(BotCardStyleDAOImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	private static final String sql_findByStyleId = "SELECT * FROM public.bot_card_style WHERE style_id = :style_id";

	@Override
	public BotCardStyle findByStyleId(int styleId) {
		if (isInfo) {
			log.info("SQL:[" + sql_findByStyleId + "]");
			log.info("parameter:[style_id=" + styleId + "]");
		}

		SQLQuery query = getSession().createSQLQuery(sql_findByStyleId);
		query.setInteger("style_id", styleId);
		query.addEntity(BotCardStyle.class);

		List<BotCardStyle> botCardStyles = query.list();
		if (botCardStyles == null || botCardStyles.isEmpty()) {
			return null;
		}
		return botCardStyles.get(0);
	}
}
