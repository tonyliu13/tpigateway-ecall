package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotTpiReplyTextDao;
import com.tp.tpigateway.model.common.BotTpiReplyText;

/**
 * TPI回覆內容資料表實作
 */
@Repository("botTpiReplyTextDao")
public class BotTpiReplyTextDaoImpl extends AbstractDAO<Integer, BotTpiReplyText> implements BotTpiReplyTextDao {

	private static Logger log = LoggerFactory.getLogger(BotTpiReplyTextDaoImpl.class);
	
	public void batchInsert(List<BotTpiReplyText> botTpiReplyTexts) {
//		bulkInsert(botTpiReplyTexts);
		doAction("batchInsert", botTpiReplyTexts);
	}
	
	public void removeAll() {
//		truncate(BotTpiReplyText.class.getAnnotation(Table.class).name());
		doAction("removeAll", null);
	}
	
	public List<BotTpiReplyText> findAll() {
//		return findAll(BotTpiReplyText.class);
		return doAction("findAll", null);
	}
	
	synchronized private List<BotTpiReplyText> doAction(String action, List<BotTpiReplyText> botTpiReplyTexts) {
		
		log.error("[BotTpiReplyTextDaoImpl] " + action + " start.");
		
		List<BotTpiReplyText> botTpiReplyTextsList = null;
		if (StringUtils.equals(action, "batchInsert")) {
			bulkInsert(botTpiReplyTexts);
		} else if (StringUtils.equals(action, "removeAll")) {
			truncate(BotTpiReplyText.class.getAnnotation(Table.class).name());
		} else {
			botTpiReplyTextsList = findAll(BotTpiReplyText.class);
		}
		
		log.error("[BotTpiReplyTextDaoImpl] " + action + " end.");
		
		return botTpiReplyTextsList;
	}
}
