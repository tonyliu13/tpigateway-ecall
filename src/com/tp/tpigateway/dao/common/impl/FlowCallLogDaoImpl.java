package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.FlowCallLogDao;
import com.tp.tpigateway.model.common.FlowCallLog;

/**
 * Flow紀錄資料表貫作
 */
@Repository("flowCallLogDao")
public class FlowCallLogDaoImpl extends AbstractDAO<Integer, FlowCallLog> implements FlowCallLogDao {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void save(FlowCallLog flowCallLog) {
        log.debug("[save] sessionId=" + flowCallLog.getSessionId() + ", msgId=" + flowCallLog.getMsgId());
        persist(flowCallLog);
    }

    @SuppressWarnings("unchecked")
    @Override
    public FlowCallLog findBySessionIdAndMsgId(String sessionId, String msgId) {

        FlowCallLog result = null;
        
        StringBuffer hql = new StringBuffer();
        hql.append("FROM FlowCallLog f where f.sessionId = :sessionId and f.msgId = :msgId");
        
        Query query = getSession().createQuery(hql.toString());
        query.setString("sessionId", sessionId);
        query.setString("msgId", msgId);
        
        List<FlowCallLog> lists = query.list();
        result = CollectionUtils.isNotEmpty(lists) ? lists.get(0) : null;

        return result;
    }

}
