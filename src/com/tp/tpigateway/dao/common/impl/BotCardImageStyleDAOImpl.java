package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCardImageStyleDAO;
import com.tp.tpigateway.model.common.BotCardImageStyle;
import com.tp.tpigateway.model.common.BotCardImageStyleId;
import com.tp.tpigateway.model.common.BotCardStyle;

@Repository("botCardImageStyleDAO")
@Transactional
public class BotCardImageStyleDAOImpl extends AbstractDAO<BotCardImageStyleId, BotCardImageStyle>
		implements BotCardImageStyleDAO {

	private static final Logger log = LoggerFactory.getLogger(BotCardImageStyleDAOImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	private static final String sql_findByStyleId = "SELECT * FROM public.bot_card_image_style WHERE style_id = :style_id";

	@Override
	public BotCardImageStyle findByStyleId(int styleId) {
		if (isInfo) {
			log.info("SQL:[" + sql_findByStyleId + "]");
			log.info("parameter:[style_id=" + styleId + "]");
		}

		SQLQuery query = getSession().createSQLQuery(sql_findByStyleId);
		query.setInteger("style_id", styleId);
		query.addEntity(BotCardImageStyle.class);

		List<BotCardImageStyle> botCardImageStyles = query.list();
		if (botCardImageStyles == null || botCardImageStyles.isEmpty()) {
			return null;
		}
		return botCardImageStyles.get(0);
	}
}
