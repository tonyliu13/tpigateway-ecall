package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCardTextStyleDAO;
import com.tp.tpigateway.model.common.BotCardStyle;
import com.tp.tpigateway.model.common.BotCardTextStyle;

@Repository("botCardTextStyleDAO")
@Transactional
public class BotCardTextStyleDAOImpl extends AbstractDAO<Integer, BotCardTextStyle> implements BotCardTextStyleDAO {

	private static final Logger log = LoggerFactory.getLogger(BotCardTextStyleDAOImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	private static final String sql_findByStyleId = "SELECT * FROM public.bot_card_text_style WHERE style_id = :style_id";

	@Override
	public BotCardTextStyle findByStyleId(int styleId) {
		if (isInfo) {
			log.info("SQL:[" + sql_findByStyleId + "]");
			log.info("parameter:[style_id=" + styleId + "]");
		}

		SQLQuery query = getSession().createSQLQuery(sql_findByStyleId);
		query.setInteger("style_id", styleId);
		query.addEntity(BotCardStyle.class);

		List<BotCardTextStyle> botCardTextStyles = query.list();
		if (botCardTextStyles == null || botCardTextStyles.isEmpty()) {
			return null;
		}
		return botCardTextStyles.get(0);
	}
}
