package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotLinkDao;
import com.tp.tpigateway.model.common.BotLink;

/**
 * 連結設定
 */
@Repository("botLinkDao")
@Transactional
public class BotLinkDaoImpl extends AbstractDAO<String, BotLink> implements BotLinkDao {
	private static Logger log = LoggerFactory.getLogger(BotLinkDaoImpl.class);

	@Override
	public void batchInsert(List<BotLink> botLinks) {
		batchInsert(botLinks);		
	}

	public List<BotLink> findAll() {
		return findAll(BotLink.class);
	}
	
	@Override
	public BotLink findByLinkID(String linkID) {
		String sql = "SELECT * FROM public.bot_link where link_id = '"+linkID+"'";
		log.debug("SQL:"+ sql);
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(BotLink.class);
		if(query.list()==null || query.list().isEmpty() || query.list().size()==0 ) {
			log.error("Table Name=[public.bot_link]中link_id="+linkID+"查無資料");
		}
		List<BotLink> results = query.list();
		return results.get(0);
	}

	
}
