package com.tp.tpigateway.dao.common.impl;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCampaignLogDao;
import com.tp.tpigateway.model.common.BotCampaignLog;
import com.tp.tpigateway.model.common.BotCampaignLogId;

@Repository("botCampaignLogDao")
public class BotCampaignLogDaoImpl extends AbstractDAO<BotCampaignLogId, BotCampaignLog> implements BotCampaignLogDao {

    @Override
    public BotCampaignLog findByBotCampaignLogId(BotCampaignLogId botCampaignLogId) {
        return super.getByKey(botCampaignLogId);
    }

    @Override
    public void save(BotCampaignLog campaignLog) {
        super.getSession().save(campaignLog);
    }

    @Override
    public void saveOrUpdate(BotCampaignLog campaignLog) {
        super.getSession().saveOrUpdate(campaignLog);
    }

    @Override
    public void update(BotCampaignLog campaignLog) {
        super.getSession().update(campaignLog);
    }
    
    @Override
    public void delete(BotCampaignLogId botCampaignLogId) {
        BotCampaignLog campaignLog = findByBotCampaignLogId(botCampaignLogId);
        super.getSession().delete(campaignLog);
    }

    @Override
    public BotCampaignLog findByCampaignIdAndCustId(String campaignId, String custId) {
        String sql = "SELECT * FROM public.bot_campaign_log where campaign_id = :campaignId and cust_id = :custId limit 1";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setString("campaignId", campaignId);
        query.setString("custId", custId);
        query.addEntity(BotCampaignLog.class);
        BotCampaignLog result = (BotCampaignLog) query.list().get(0);
        return result;
    }
}
