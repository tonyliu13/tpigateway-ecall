package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotReplyTextDao;
import com.tp.tpigateway.model.common.BotReplyText;
import com.tp.tpigateway.model.common.BotReplyTextId;


/**
 * TPI回覆內容資料表實作
 */
@Repository("botReplyTextDao")
public class BotReplyTextDaoImpl extends AbstractDAO<BotReplyTextId, BotReplyText> implements BotReplyTextDao {

	private static Logger log = LoggerFactory.getLogger(BotReplyTextDaoImpl.class);
	
	public void batchInsert(List<BotReplyText> botReplyTexts) {
//		bulkInsert(botReplyTexts);
		doAction("batchInsert", botReplyTexts);
	}
	
	public void removeAll() {
//		truncate(BotReplyText.class.getAnnotation(Table.class).name());
		doAction("removeAll", null);
	}
	
	public List<BotReplyText> findAll() {
//		return findAll(BotReplyText.class);
		return doAction("findAll", null);
	}
	
	synchronized private List<BotReplyText> doAction(String action, List<BotReplyText> botReplyTexts) {
		
		log.debug("[BotReplyTextDaoImpl] " + action + " start.");
		
		List<BotReplyText> botReplyTextsList = null;
		if (StringUtils.equals(action, "batchInsert")) {
			bulkInsert(botReplyTexts);
		} else if (StringUtils.equals(action, "removeAll")) {
			truncate(BotReplyText.class.getAnnotation(Table.class).name());
		} else {
			botReplyTextsList = findAll(BotReplyText.class);
		}
		
		log.debug("[BotReplyTextDaoImpl] " + action + " end.");
		
		return botReplyTextsList;
	}
}
