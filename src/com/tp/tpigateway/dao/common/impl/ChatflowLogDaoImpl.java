package com.tp.tpigateway.dao.common.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.tp.tpigateway.dao.common.ChatflowLogDao;
import com.tp.tpigateway.util.PropUtils;

@Repository("chatflowLogDao")
public class ChatflowLogDaoImpl implements ChatflowLogDao {
	
	private static Logger log = LoggerFactory.getLogger(ChatflowLogDaoImpl.class);
	
	@Override
	public List<Object> getMessageIntentList(String sessionId, String msgId) {
		
		MongoClientURI uri = new MongoClientURI(PropUtils.CATHAYLIFE_MONGODB_URL);
		MongoClient client = new MongoClient(uri);
		MongoDatabase db = client.getDatabase("admin");
		MongoCollection<Document> collection = db.getCollection("nodemessages");
		
		/*  At first I try to get the first record which meets the requirements, but its intent list may be empty.
		 *  So now I get all records that meet the requirements, and pick the first one of which 
		 *  intent list is not empty.
		 */ 
		// Document doc = collection.find(Filters.and(Filters.eq("info.payload.session_id", sessionId),
		//		Filters.eq("info.payload.customer_msg_id", msgId))).first();
		
		FindIterable<Document> iterDoc = collection.find(Filters.and(Filters.eq("info.payload.session_id", sessionId),
				Filters.eq("info.payload.customer_msg_id", msgId)));
		
		List<Object> intentList = pickNonEmptyIntentList(iterDoc);		
		client.close();
		
		return intentList;
	}
	
	@Override
	public List<Map<String, String>> getMessagePathNodeList(String sessionId, String msgId) {
		
		MongoClientURI uri = new MongoClientURI(PropUtils.CATHAYLIFE_MONGODB_URL);
		MongoClient client = new MongoClient(uri);
		MongoDatabase db = client.getDatabase("admin");
		MongoCollection<Document> collection = db.getCollection("nodemessages");
		
		FindIterable<Document> iterDoc = collection.find(Filters.and(Filters.eq("info.payload.session_id", sessionId),
				Filters.eq("info.payload.customer_msg_id", msgId)));
		
		List<String> pathNodeList = this.pickNonEmptyPathNodeList(iterDoc);		
		client.close();
		List<Map<String, String>> nameTypeMapList = getNameTypeOfPathList(pathNodeList);
		return nameTypeMapList;
	}
	
	@Override
	public List<Object> getSessionMessageList(String sessionId) {
		
		MongoClientURI uri = new MongoClientURI(PropUtils.CATHAYLIFE_MONGODB_URL);
		MongoClient client = new MongoClient(uri);
		MongoDatabase db = client.getDatabase("admin");
		MongoCollection<Document> collection = db.getCollection("nodemessages");
		final List<Object> messageList = new ArrayList<Object>();

		Block<Document> addBlock = new Block<Document>() {
			@Override
			public void apply(final Document doc) {
				Map<String, Object> map = convertDocToMap(doc);
				messageList.add(map);
			}
		};
		
		collection.find(Filters.eq("info.session_id", sessionId)).forEach(addBlock);
		client.close();
		return messageList;
	}
	
	private List<Map<String, String>> getNameTypeOfPathList(List<String> pathList) {
		MongoClientURI uri = new MongoClientURI(PropUtils.CATHAYLIFE_MONGODB_URL);
		MongoClient client = new MongoClient(uri);
		MongoDatabase db = client.getDatabase("admin");
		MongoCollection<Document> collection = db.getCollection("nodes");
		List<Map<String, String>> nameTypeList = new ArrayList<>();
		for (String path: pathList) {
			String type = "";
			String name = "";
			Map<String, String> nameTypeMap = new HashMap<>();
			Document doc = collection.find(Filters.eq("id", path)).first();
			if (doc != null) {
				type = (String) doc.get("type");
				name = (String) doc.get("name");
			}
			nameTypeMap.put("id", path);
			nameTypeMap.put("type", type);
			nameTypeMap.put("name", name);
			nameTypeList.add(nameTypeMap);
		}
		client.close();
		return nameTypeList;
	}

	private List<Object> pickNonEmptyIntentList(FindIterable<Document> iterDoc) {
		final List<List<Object>> intentListList = new ArrayList<>();
		List<Object> intentList = new ArrayList<>();
		
		Block<Document> addIntentListBlock = new Block<Document>() {
			@Override
			public void apply(final Document doc) {
				intentListList.add(getIntentList(doc));
			}
		};
		
		iterDoc.forEach(addIntentListBlock);
		for (List<Object> list: intentListList) {
			if (list.size() > 0) {
				intentList = list;
				break;
			}
		}
		return intentList;
	}
	
	private List<String> pickNonEmptyPathNodeList(FindIterable<Document> iterDoc) {
		final List<List<String>> pathListList = new ArrayList<>();
		List<String> pathNodeList = new ArrayList<>();
		
		Block<Document> addPathListBlock = new Block<Document>() {
			@Override
			public void apply(final Document doc) {
				pathListList.add(getPathList(doc));
			}
		};
		
		iterDoc.forEach(addPathListBlock);
		for (List<String> list: pathListList) {
			if (list.size() > 0) {
				pathNodeList = list;
				break;
			}
		}
		return pathNodeList;
	}
	
	@SuppressWarnings("unchecked")
	private List<Object> getIntentList(Document doc) {
		List<Object> retList = null;
		
		try {
			// It doesn't seem to work now. The schema may be changed cause I can not find this field in recent logs.
			retList = (List<Object>)((Document)((Document)((Document)doc.get("info")).get("NLU")).get("raw")).get("intents");
		} catch(Exception e1) {
			retList = new ArrayList<Object>();
			try {
				String intent = (String)((Document)((Document)doc.get("info")).get("NLU")).get("intent");
				Document tmpDoc = new Document();
				tmpDoc.put("intent", intent);
				tmpDoc.put("score", "");
				retList.add((Object)tmpDoc);
			} catch(Exception e2) {
				// do nothing
			}
		}
		return retList;
	}
	
	@SuppressWarnings("unchecked")
	private List<String> getPathList(Document doc) {
		List<String> retList;
		try {
			retList = (List<String>)((Document)doc.get("info")).get("_path");
		} catch(Exception e) {
			retList = new ArrayList<String>();
		}
		return retList;
	}
	
	private String getPayloadField(Document doc, String field) {
		String retMsg = "";
		try {
			retMsg = (String)((Document)((Document)doc.get("info")).get("payload")).get(field); 
		} catch(Exception e) {
			log.info("info.payload." + field + "不存在");
		}
		return retMsg;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> convertDocToMap(Document doc) {
		Map<String, Object> map = new HashMap<String, Object>();
		String objId = doc.getObjectId("_id").toString();
		String type = doc.getString("type");
		String msg = getPayloadField(doc, "msg");
		String showMsg = getPayloadField(doc, "showMsg");
		String msgId = getPayloadField(doc, "customer_msg_id");
		String msgType = getPayloadField(doc, "type");
		List<Object> intents = getIntentList(doc);
		List<String> pathList = (List<String>)((Document)doc.get("info")).get("_path");
		List<Map<String, String>> nameTypeList = getNameTypeOfPathList(pathList);
		Date registeredAt = doc.getDate("registered_at");
		map.put("objId", objId);
		map.put("type", type);
		map.put("msg", msg);
		map.put("showMsg", showMsg);
		map.put("intents", intents);
		map.put("customer_msg_id", msgId);
		map.put("message_type", msgType);
		map.put("registered_at", registeredAt);
		map.put("path", nameTypeList);
		return map;
	}

}
