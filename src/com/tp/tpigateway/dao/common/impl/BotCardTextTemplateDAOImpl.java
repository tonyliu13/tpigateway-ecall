package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCardTextTemplateDAO;
import com.tp.tpigateway.model.common.BotCardTextTemplate;
import com.tp.tpigateway.model.common.BotCardTextTemplateId;

@Repository("botCardTextTemplateDAO")
@Transactional
public class BotCardTextTemplateDAOImpl extends AbstractDAO<BotCardTextTemplateId, BotCardTextTemplate>
		implements BotCardTextTemplateDAO {

	private static final Logger log = LoggerFactory.getLogger(BotCardTextTemplateDAOImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	private static final String sql_findByCardTextId = "SELECT * FROM public.bot_card_text_template WHERE card_text_id = :card_text_id ORDER BY seq";

	@Override
	public List<BotCardTextTemplate> findByCardTextId(String cardTextId) {
		if (isInfo) {
			log.info("SQL:[" + sql_findByCardTextId + "]");
			log.info("parameter:[card_text_id=" + cardTextId + "]");
		}

		SQLQuery query = getSession().createSQLQuery(sql_findByCardTextId);
		query.setString("card_text_id", cardTextId);
		query.addEntity(BotCardTextTemplate.class);

		return query.list();
	}
}
