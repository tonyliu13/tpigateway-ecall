package com.tp.tpigateway.dao.common.impl;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.AllAutoCompleteDao;
import com.tp.tpigateway.model.common.AllAutoComplete;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Table;
import java.util.List;

@Repository("allAutoCompleteDao")
@SuppressWarnings("unchecked")
public class AllAutoCompleteDaoImpl extends AbstractDAO<Integer, AllAutoComplete> implements AllAutoCompleteDao {

	private static Logger log = LoggerFactory.getLogger(AllAutoCompleteDaoImpl.class);
	
	@Override
	public List<AllAutoComplete> findAll() {
//		List<AllAutoComplete> autoCompleteList = getSession().createQuery("FROM AllAutoComplete ORDER BY stringOrder").list();
//		return autoCompleteList;
		return doAction("findAll", null);
	}

	@Override
	public void removeAll() {
//		truncate(AllAutoComplete.class.getAnnotation(Table.class).name());
		doAction("removeAll", null);
	}

	@Override
	public void batchInsert(List<AllAutoComplete> allAutoCompleteList) {
//		bulkInsert(allAutoCompleteList);
		doAction("batchInsert", allAutoCompleteList);
	}
	
	synchronized private List<AllAutoComplete> doAction(String action, List<AllAutoComplete> allAutoCompleteList) {
		
		log.debug("[AllAutoCompleteDaoImpl] " + action + " start.");
		
		List<AllAutoComplete> autoCompleteList = null;
		if (StringUtils.equals(action, "batchInsert")) {
			bulkInsert(allAutoCompleteList);
		} else if (StringUtils.equals(action, "removeAll")) {
			truncate(AllAutoComplete.class.getAnnotation(Table.class).name());
		} else {
			autoCompleteList = getSession().createQuery("FROM AllAutoComplete ORDER BY stringOrder").list();
		}
		
		log.debug("[AllAutoCompleteDaoImpl] " + action + " end.");
		
		return autoCompleteList;
	}
}
