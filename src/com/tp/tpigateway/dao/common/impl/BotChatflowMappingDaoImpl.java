package com.tp.tpigateway.dao.common.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotChatflowMappingDao;
import com.tp.tpigateway.model.common.BotChatflowMapping;
import com.tp.tpigateway.model.common.BotTpiReplyText;

/**
 * 呼叫API log紀錄資料表貫作
 */
@Repository("botChatflowMappingDao")
@Transactional
public class BotChatflowMappingDaoImpl extends AbstractDAO<Integer, BotChatflowMapping> implements BotChatflowMappingDao {

	//private static Logger log = LoggerFactory.getLogger(BotChatflowMappingDaoImpl.class);
	
	public void save(BotChatflowMapping map) {
		persist(map);
	}
	
	@Override
	public Map<String, String> getChatflowMapping() {

		String hql = "FROM BotChatflowMapping";
		Query query = getSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<BotChatflowMapping> mappingList = query.list();
		Map<String, String> mappingMap = new HashMap<>();
		for (BotChatflowMapping map: mappingList ) {
			mappingMap.put(map.getChatflowId(), map.getChatflowName());
		}
		
		return mappingMap;
	}
	
	public List<BotChatflowMapping> findAll() {
		return findAll(BotChatflowMapping.class);
	}
}