package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotCampaignDao;
import com.tp.tpigateway.model.common.BotCampaign;

@Repository("botCampaignDao")
public class BotCampaignDaoImpl extends AbstractDAO<String, BotCampaign> implements BotCampaignDao {

    @SuppressWarnings("unchecked")
    @Override
    public List<BotCampaign> findAll() {
        return getSession().createQuery("FROM BotCampaign").list();
    }
    
    @Override
    public BotCampaign findByCampaignId(String campaignId) {
        return super.getByKey(campaignId);
    }

    @Override
    public void save(BotCampaign campaign) {
        super.getSession().save(campaign);
    }

    @Override
    public void saveOrUpdate(BotCampaign campaign) {
        super.getSession().saveOrUpdate(campaign);
    }

    @Override
    public void update(BotCampaign campaign) {
        super.getSession().update(campaign);
    }

    @Override
    public void delete(String campaignId) {
        BotCampaign campaign = findByCampaignId(campaignId);
        super.getSession().delete(campaign);
    }

}
