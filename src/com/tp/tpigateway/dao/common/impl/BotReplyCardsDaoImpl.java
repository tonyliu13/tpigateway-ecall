package com.tp.tpigateway.dao.common.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotReplyCardsDao;
import com.tp.tpigateway.model.common.BotReplyCards;
import com.tp.tpigateway.model.common.BotReplyCardsId;

/**
 * PDF/文字牌卡内容
 */
@Repository("botReplyCardsDao")
@Transactional
public class BotReplyCardsDaoImpl extends AbstractDAO<BotReplyCardsId, BotReplyCards> implements BotReplyCardsDao {
	private static Logger log = LoggerFactory.getLogger(BotReplyCardsDaoImpl.class);

	@Override
	public void batchInsert(List<BotReplyCards> botTpiReplyContents) {
		batchInsert(botTpiReplyContents);		
	}

	@Override
	public List<BotReplyCards> findByRespNodeSeq(String respnodeseq) {
		String sql = "SELECT * FROM public.bot_reply_cards where resp_node_seq = '"+respnodeseq+"'"+" ORDER BY card_seq";
		log.debug("SQL:"+ sql);
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(BotReplyCards.class);
		if(query.list()==null || query.list().isEmpty() || query.list().size()==0 ) {
			log.error("Table Name=[public.bot_reply_cards]中resp_node_seq="+respnodeseq+"查無資料");
		}
		List<BotReplyCards> results = query.list();
		return results;
	}

	@Override
	public int removeAllByRespNodeSeq(String respnodeseq) {
		String sql = "DELETE FROM public.bot_reply_cards where resp_node_seq = '"+respnodeseq+"'";
		SQLQuery query = getSession().createSQLQuery(sql);
		return query.executeUpdate();
	}
}
