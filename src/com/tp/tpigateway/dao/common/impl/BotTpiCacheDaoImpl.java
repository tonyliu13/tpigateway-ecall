package com.tp.tpigateway.dao.common.impl;

import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.common.BotTpiCacheDao;
import com.tp.tpigateway.model.common.BotTpiCache;

@Repository("botTpiCacheDaoImpl")
public class BotTpiCacheDaoImpl extends AbstractDAO<String, BotTpiCache> implements BotTpiCacheDao {

    @Override
    public BotTpiCache findByCacheKey(String cacheKey) {
        return super.getByKey(cacheKey);
    }

    @Override
    public void save(BotTpiCache cache) {
        super.getSession().save(cache);
    }
    
    @Override
    public void saveOrUpdate(BotTpiCache cache) {
        super.getSession().saveOrUpdate(cache);
    }
    
    @Override
    public void update(BotTpiCache cache) {
        super.getSession().update(cache);
    }
    
    @Override
    public void delete(String cacheKey) {
        BotTpiCache cache = findByCacheKey(cacheKey);
        super.getSession().delete(cache);
    }
}
