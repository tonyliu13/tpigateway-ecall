package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotCampaign;

public interface BotCampaignDao {

    public List<BotCampaign> findAll();
    
    public BotCampaign findByCampaignId(String campaignId);
    
    public void save(BotCampaign campaign);
    
    public void saveOrUpdate(BotCampaign campaign);
    
    public void update(BotCampaign campaign);
    
    public void delete(String campaignId);
}
