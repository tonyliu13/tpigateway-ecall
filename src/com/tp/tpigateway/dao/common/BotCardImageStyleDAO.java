package com.tp.tpigateway.dao.common;

import com.tp.tpigateway.model.common.BotCardImageStyle;

public interface BotCardImageStyleDAO {

	/**
	 * 查詢
	 * 
	 * @param styleId
	 *            樣式Id
	 * @return
	 */
	public BotCardImageStyle findByStyleId(int styleId);
}
