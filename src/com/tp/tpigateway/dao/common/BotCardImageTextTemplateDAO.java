package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotCardImageTextTemplate;

public interface BotCardImageTextTemplateDAO {

	/**
	 * 查詢
	 * 
	 * @param imageTextId
	 *            牌卡圖片文字Id
	 * @return
	 */
	public List<BotCardImageTextTemplate> findByImageTextId(String imageTextId);
}
