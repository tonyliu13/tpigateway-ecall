package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotLinkOption;

/**
 * 連結選項
 */
public interface BotLinkOptionDao {
	/**
	 * 批次寫入
	 * 
	 * @param botLinkOptions
	 */
	public void batchInsert(List<BotLinkOption> botLinkOptions);
	

	/**
	 * 查詢連結選項
	 * 
	 * @param optionid
	 */
	public List<BotLinkOption> findByOptionID(String optionid);
}
