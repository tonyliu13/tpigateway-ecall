package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotReplyCards;

/**
 * TPI回覆內容資料表介面
 */
public interface BotReplyCardsDao {
	/**
	 * 批次寫入
	 * 
	 * @param botReplyCards
	 */
	public void batchInsert(List<BotReplyCards> botReplyCards);
	

	
	/**
	 * 查詢節點的回覆内容設定
	 * 
	 * @return
	 */
	public List<BotReplyCards> findByRespNodeSeq(String respnodeseq);
	
	
	/**
	 * 清空(目前以truncate實現)
	 */
	public int removeAllByRespNodeSeq(String respnodeseq);



}
