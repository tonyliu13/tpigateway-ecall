package com.tp.tpigateway.dao.common;

import java.sql.Timestamp;

import com.tp.tpigateway.model.common.BotRequestAndResponse;
import com.tp.tpigateway.model.common.BotTpiCallLog;

/**
 * 呼叫API log紀錄資料表介面
 */
public interface BotTpiCallLogDao {

	/**
	 * 新增一筆
	 * 
	 * @param log
	 */
	public void save(BotTpiCallLog log);
	
	/**
	 * 查詢資料，依據chatID 及 insertTime
	 * @param log
	 */
	public BotRequestAndResponse queryByChatID(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd);

}
