package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotCardTextTemplate;

public interface BotCardTextTemplateDAO {

	/**
	 * 查詢
	 * 
	 * @param cardTextId
	 *            牌卡內容Id
	 * @return
	 */
	public List<BotCardTextTemplate> findByCardTextId(String cardTextId);
}
