package com.tp.tpigateway.dao.common;

import com.tp.tpigateway.model.common.BotCampaignLog;
import com.tp.tpigateway.model.common.BotCampaignLogId;

public interface BotCampaignLogDao {

    public BotCampaignLog findByBotCampaignLogId(BotCampaignLogId botCampaignLogId);
    
    public void save(BotCampaignLog campaignLog);
    
    public void saveOrUpdate(BotCampaignLog campaignLog);
    
    public void update(BotCampaignLog campaignLog);
    
    public void delete(BotCampaignLogId botCampaignLogId);
    
    public BotCampaignLog findByCampaignIdAndCustId(String campaignId, String custId);
}
