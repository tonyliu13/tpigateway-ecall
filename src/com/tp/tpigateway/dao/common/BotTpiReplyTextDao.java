package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotTpiReplyText;

/**
 * TPI回覆內容資料表介面
 */
public interface BotTpiReplyTextDao {

	/**
	 * 批次寫入
	 * 
	 * @param botTpiReplyTexts
	 */
	public void batchInsert(List<BotTpiReplyText> botTpiReplyTexts);
	
	/**
	 * 清空(目前以truncate實現)
	 */
	public void removeAll();
	
	/**
	 * 查詢全部
	 * 
	 * @return
	 */
	public List<BotTpiReplyText> findAll();
}
