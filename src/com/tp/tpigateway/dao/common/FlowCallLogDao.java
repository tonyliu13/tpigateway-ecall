package com.tp.tpigateway.dao.common;

import com.tp.tpigateway.model.common.FlowCallLog;

/**
 * Flow執行紀錄資料表介面
 */
public interface FlowCallLogDao {

    /**
     * 新增一筆
     * 
     * @param log
     */
    public void save(FlowCallLog log);
    
    /**
     * 查詢一筆
     * 
     * @param sessionId
     * @param msgId
     * @return
     */
    FlowCallLog findBySessionIdAndMsgId(String sessionId, String msgId);
}
