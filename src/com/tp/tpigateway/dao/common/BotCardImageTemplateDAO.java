package com.tp.tpigateway.dao.common;

import com.tp.tpigateway.model.common.BotCardImageTemplate;

public interface BotCardImageTemplateDAO {

	/**
	 * 查詢
	 * 
	 * @param imageId
	 *            牌卡圖片Id
	 * @return
	 */
	public BotCardImageTemplate findByImageId(String imageId);
}
