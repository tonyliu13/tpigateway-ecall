package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotReplyContent;

/**
 * TPI回覆內容資料表介面
 */
public interface BotReplyContentDao {
	/**
	 * 批次寫入
	 * 
	 * @param botTpiReplyTexts
	 */
	public void batchInsert(List<BotReplyContent> botTpiReplyContents);
	

	
	/**
	 * 查詢節點的回覆内容設定
	 * 
	 * @return
	 */
	public List<BotReplyContent> findByRespNodeId(String respnodeid);
	
	
	/**
	 * 清空(目前以truncate實現)
	 */
	public int removeAllByRespNodeId(String respnodeid);

	
}
