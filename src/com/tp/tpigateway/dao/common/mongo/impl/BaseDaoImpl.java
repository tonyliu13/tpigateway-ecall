package com.tp.tpigateway.dao.common.mongo.impl;

import com.tp.tpigateway.dao.common.mongo.BaseDao;
import com.tp.tpigateway.util.EmptyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

public abstract class BaseDaoImpl<T> implements BaseDao<T> {

    protected abstract Class<T> getEntityClass();

    @Autowired
    protected MongoTemplate mgt;

    @Override
    public T find(Serializable id) {
        return mgt.findById(id, getEntityClass());
    }

    @Override
    public List<T> findAll() {
        return mgt.findAll(getEntityClass());
    }

    @Override
    public List<T> findAll(String order) {
        List<Order> orderList = parseOrder(order);
        if (EmptyUtil.isEmpty(orderList)) {
            return findAll();
        }
        return mgt.find(new Query().with(new Sort(orderList)), getEntityClass());
    }

    @Override
    public List<T> findByProp(String propName, Object propValue) {
        return findByProp(propName, propValue, null);
    }

    @Override
    public T findOneByProp(String propName, Object propValue) {
        List<T> result = findByProp(propName, propValue);
        if (EmptyUtil.isNotEmpty(result)) {
            return result.get(0);
        }
        return null;
    }

    @Override
    public List<T> findByProp(String propName, Object propValue, String order) {
        Query query = new Query();

        query.addCriteria(where(propName).is(propValue));

        List<Order> orderList = parseOrder(order);
        if (EmptyUtil.isNotEmpty(orderList)) {
            query.with(new Sort(orderList));
        }
        return mgt.find(query, getEntityClass());
    }

    @Override
    public List<T> findByProps(String[] propName, Object[] propValue) {
        return findByProps(propName, propValue, null);
    }

    @Override
    public List<T> findByProps(String[] propName, Object[] propValue, String order) {
        Query query = createQuery(propName, propValue, order);
        return mgt.find(query, getEntityClass());
    }

    @Override
    public int countByCondition(String[] params, Object[] values) {
        Query query = createQuery(params, values, null);
        Long count = mgt.count(query, getEntityClass());
        return count.intValue();
    }

    protected Query createQuery(String[] params, Object[] values, String order) {
        Query query = new Query();

        if (EmptyUtil.isNotEmpty(params) && EmptyUtil.isNotEmpty(values)) {
            for (int i = 0; i < params.length; i++) {
                query.addCriteria(where(params[i]).is(values[i]));
            }
        }

        List<Order> orderList = parseOrder(order);
        if (EmptyUtil.isNotEmpty(orderList)) {
            query.with(new Sort(orderList));
        }

        return query;
    }

    protected List<Order> parseOrder(String order) {
        List<Order> list = null;
        if (EmptyUtil.isNotEmpty(order)) {
            list = new ArrayList<>();

            String[] fields = order.split(",");
            Order o = null;
            String[] item = null;
            for (int i = 0; i < fields.length; i++) {
                if (EmptyUtil.isEmpty(fields[i])) {
                    continue;
                }
                item = fields[i].split(" ");
                if (item.length == 1) {
                    o = new Order(Direction.ASC, item[0]);
                } else if (item.length == 2) {
                    o = new Order("desc".equalsIgnoreCase(item[1]) ? Direction.DESC : Direction.ASC, item[0]);
                } else {
                    throw new RuntimeException("參數order解析錯誤");
                }
                list.add(o);
            }
        }
        return list;
    }

}
