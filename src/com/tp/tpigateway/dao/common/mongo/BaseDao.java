package com.tp.tpigateway.dao.common.mongo;

import java.io.Serializable;
import java.util.List;

public interface BaseDao<T> {

    T find(Serializable id);

    List<T> findAll();

    List<T> findAll(String order);

    List<T> findByProp(String propName, Object propValue);

    T findOneByProp(String propName, Object propValue);

    List<T> findByProp(String propName, Object propValue, String order);

    List<T> findByProps(String[] propName, Object[] propValue);

    List<T> findByProps(String[] propName, Object[] propValue, String order);

    int countByCondition(String[] params, Object[] values);

}
