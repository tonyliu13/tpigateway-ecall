package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotCardTemplate;

public interface BotCardTemplateDAO {

	/**
	 * 查詢
	 * 
	 * @param cardId
	 *            牌卡id
	 * @return
	 */
	public List<BotCardTemplate> findByCardId(String cardId);
}
