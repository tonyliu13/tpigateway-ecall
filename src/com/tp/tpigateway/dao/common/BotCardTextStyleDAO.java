package com.tp.tpigateway.dao.common;

import com.tp.tpigateway.model.common.BotCardTextStyle;

public interface BotCardTextStyleDAO {

	/**
	 * 查詢
	 * 
	 * @param styleId
	 *            樣式Id
	 * @return
	 */
	public BotCardTextStyle findByStyleId(int styleId);
}
