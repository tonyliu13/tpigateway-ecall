package com.tp.tpigateway.dao.common;

import java.util.List;
import java.util.Map;

public interface ChatflowLogDao {
	
	public List<Object> getMessageIntentList(String sessionId, String msgId);
	
	public List<Object> getSessionMessageList(String sessionId);

	public List<Map<String, String>> getMessagePathNodeList(String sessionId, String msgId);

}
