package com.tp.tpigateway.dao.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotReplyText;

/**
 * TPI回覆內容資料表介面
 */
public interface BotReplyTextDao {

	/**
	 * 批次寫入
	 * 
	 * @param botReplyTexts
	 */
	public void batchInsert(List<BotReplyText> botReplyTexts);
	
	/**
	 * 清空(目前以truncate實現)
	 */
	public void removeAll();
	
	/**
	 * 查詢全部
	 * 
	 * @return
	 */
	public List<BotReplyText> findAll();
}
