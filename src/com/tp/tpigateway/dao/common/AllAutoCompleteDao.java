package com.tp.tpigateway.dao.common;

import com.tp.tpigateway.model.common.AllAutoComplete;

import java.util.List;

public interface AllAutoCompleteDao {

	public void removeAll();
	
	public void batchInsert (List<AllAutoComplete> allAutoCompleteList);
	
	/**
	 * 查詢全部
	 * 
	 * @return
	 */
	public List<AllAutoComplete> findAll();
}
