package com.tp.tpigateway.dao.flow;

import java.util.List;

import com.tp.tpigateway.model.life.CBV10001VO.FlowCallLog3D;

public interface FlowCallLog3DDAO {

	public void saveOrUpdate(FlowCallLog3D flowCallLog3D);
	
	//TODO 正式上線後須刪除
	public List<FlowCallLog3D> showFlowCallLog3D(int dataNumber);
	
}
