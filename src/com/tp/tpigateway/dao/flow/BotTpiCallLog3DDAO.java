package com.tp.tpigateway.dao.flow;

import java.util.List;

import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;

public interface BotTpiCallLog3DDAO {

	public void save(BotTpiCallLog3D botTpiCallLog3D);
	
	//TODO 正式上線後須刪除
	public List<BotTpiCallLog3D> showBotTpiCallLog3D(int dataNumber);
	
}
