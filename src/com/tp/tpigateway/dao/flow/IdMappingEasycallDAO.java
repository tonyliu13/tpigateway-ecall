package com.tp.tpigateway.dao.flow;

import com.tp.tpigateway.model.life.CBV10001VO.IdMappingEasycall;

public interface IdMappingEasycallDAO {

	public void save(IdMappingEasycall idMappingEasycall);
	
	public void update(IdMappingEasycall idMappingEasycall);
	
	public void saveOrUpdate(IdMappingEasycall idMappingEasycall);
	
	public IdMappingEasycall findBySessionId(String sessionId);
}
