package com.tp.tpigateway.dao.flow.impl;

import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.flow.IdMappingEasycallDAO;
import com.tp.tpigateway.model.life.CBV10001VO.IdMappingEasycall;

@Repository("idMappingEasycallDao")
public class IdMappingEasycallDAOImpl extends AbstractDAO<String, IdMappingEasycall> implements IdMappingEasycallDAO {

	@Override
	public void save(IdMappingEasycall idMappingEasycall) {
		persist(idMappingEasycall);
	}

	@Override
	public void update(IdMappingEasycall idMappingEasycall) {
		super.getSession().update(idMappingEasycall);
	}

	@Override
	public void saveOrUpdate(IdMappingEasycall idMappingEasycall) {
		super.getSession().saveOrUpdate(idMappingEasycall);
	}
	
	@Override
	public IdMappingEasycall findBySessionId(String sessionId) {
		return getByKey(sessionId);
	}
}
