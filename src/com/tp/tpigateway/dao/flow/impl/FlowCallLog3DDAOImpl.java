package com.tp.tpigateway.dao.flow.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.flow.FlowCallLog3DDAO;
import com.tp.tpigateway.model.life.CBV10001VO.FlowCallLog3D;

@Repository("flowCallLog3DDAO")
public class FlowCallLog3DDAOImpl extends AbstractDAO<String, FlowCallLog3D> implements FlowCallLog3DDAO {
	
	@Override
	public void saveOrUpdate(FlowCallLog3D flowCallLog3D) {
		super.getSession().saveOrUpdate(flowCallLog3D);
	}

	//TODO 正式上線後須刪除
	@SuppressWarnings("unchecked")
	@Override
	public List<FlowCallLog3D> showFlowCallLog3D(int dataNumber) {
		Query query = super.getSession()
				.createSQLQuery("select * from flow_call_log_3D order by log_id desc limit :dataNumber")
				.addEntity(FlowCallLog3D.class)
				.setInteger("dataNumber", dataNumber);
		return query.list();
	}

}
