package com.tp.tpigateway.dao.flow.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.dao.common.AbstractDAO;
import com.tp.tpigateway.dao.flow.BotTpiCallLog3DDAO;
import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;

@Repository("botTpiCallLog3DDAO")
public class BotTpiCallLog3DDAOImpl extends AbstractDAO<Long, BotTpiCallLog3D> implements BotTpiCallLog3DDAO {

	@Override
	public void save(BotTpiCallLog3D botTpiCallLog3D) {
		persist(botTpiCallLog3D);
	}

	//TODO 正式上線後須刪除
	@SuppressWarnings("unchecked")
	@Override
	public List<BotTpiCallLog3D> showBotTpiCallLog3D(int dataNumber) {
		Query query = super.getSession()
				.createSQLQuery("select * from bot_tpi_call_log_3D order by id desc limit :dataNumber")
				.addEntity(BotTpiCallLog3D.class)
				.setInteger("dataNumber", dataNumber);
		return query.list();
	}
}
