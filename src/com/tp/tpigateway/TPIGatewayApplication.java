package com.tp.tpigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TPIGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(TPIGatewayApplication.class, args);
	}
}
