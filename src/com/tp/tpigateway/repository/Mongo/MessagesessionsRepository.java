package com.tp.tpigateway.repository.Mongo;

import com.tp.tpigateway.model.common.mongo.Messagesessions;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MessagesessionsRepository extends MongoRepository<Messagesessions, String> {

    public List<Messagesessions> findBySessionIdAndAppIdInOrderByStartDateAsc(String sessiondId, List<String> appIds);

    public  List<Messagesessions> findBySessionIdAndAppIdInAndStartDateBetweenOrderByStartDateAsc(String sessionId, List<String> appIds, Date sDate, Date eDate);

    public List<Messagesessions> findByAppIdInAndStartDateBetweenOrderByStartDateAsc(List<String> appIds, Date sDate, Date eDate);
}
