package com.tp.tpigateway.repository;

import com.tp.tpigateway.model.common.FlowCallLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowTpiCallLogRepository extends JpaRepository<FlowCallLog, String> {

    FlowCallLog findBySessionIdAndMsgId(String sessionId, String msgId);
}
