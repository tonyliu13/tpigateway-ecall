package com.tp.tpigateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tp.tpigateway.model.common.BotTouristPlace;

@Repository
public interface BotTouristPlaceRepository extends JpaRepository<BotTouristPlace, Integer> {

    public List<BotTouristPlace> findAll();
}
