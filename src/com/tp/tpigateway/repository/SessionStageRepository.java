package com.tp.tpigateway.repository;

import com.tp.tpigateway.model.common.SessionStage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SessionStageRepository extends JpaRepository<SessionStage, String> {

    public SessionStage findFirstBySessionIdOrderBySessionStrTimeDesc(String sessionId);

    public List<SessionStage> findBySessionUpdTimeGreaterThanEqual(Date dateTime);

    public List<SessionStage> findBySessionUpdTimeGreaterThanEqualAndAgentIdIsNullOrderByStageScoreDescSessionUpdTimeAsc(Date dateTime);
}
