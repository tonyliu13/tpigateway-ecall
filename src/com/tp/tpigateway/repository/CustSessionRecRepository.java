package com.tp.tpigateway.repository;

import com.tp.tpigateway.model.common.CustSessionRec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustSessionRecRepository extends JpaRepository<CustSessionRec, String> {

    public CustSessionRec findByCustomerIdAndDate(String customerId, String date);

    public List<CustSessionRec> findByDate(String date);
}
