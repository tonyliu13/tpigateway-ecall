package com.tp.tpigateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.tp.tpigateway.model.life.BotChatLog;

@Repository
public interface BotChatLogRepository extends JpaRepository<BotChatLog, Integer> {

	public BotChatLog findFirstBySessionIdOrderByInsertTimeDesc(String sessionId);
	
}
