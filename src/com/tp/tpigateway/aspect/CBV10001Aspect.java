package com.tp.tpigateway.aspect;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.tpigateway.exception.CBV10001DataException;
import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.FlowRequest;
import com.tp.tpigateway.service.life.flow.cb.CBV10001DataService;
import com.tp.tpigateway.util.JSONUtils;

@Aspect
@Component
public class CBV10001Aspect {

	private ObjectMapper mapper = JSONUtils.getInstance();

	@Autowired
	private CBV10001DataService cbv10001DataService;

	@Around("execution(* com.tp.tpigateway.web.life.cb.CBVController..*(..))"
			+ " && !execution(* com.tp.tpigateway.web.life.cb.CBVController.saveFlowCallLog3D(..))"
			+ " && !execution(* com.tp.tpigateway.web.life.cb.CBVController.show*(..))"
			+ " && !execution(* com.tp.tpigateway.web.life.cb.CBVController.getRgnCode(..))")
	public Object handleCBVControllerMethod(ProceedingJoinPoint pjp) throws Throwable {

		long startTimeMillis = System.currentTimeMillis(); // 記錄方法開始執行的時間

		FlowRequest requestBody = (FlowRequest) pjp.getArgs()[0];

		BotTpiCallLog3D botTpiCallLog3D = (BotTpiCallLog3D) pjp.getArgs()[1];
		Date apiStartTime = botTpiCallLog3D.getApiStartTime();
		Date apiEndTime = botTpiCallLog3D.getApiEndTime();
		int apiCostTime = 0;
		if (apiStartTime != null && apiEndTime != null) {
			apiCostTime = (int) (apiEndTime.getTime() - apiStartTime.getTime());
		}

		botTpiCallLog3D.setSessionId((String) requestBody.getSessionId());
		botTpiCallLog3D.setParams(mapper.writeValueAsString(requestBody));
		botTpiCallLog3D.setMethod(pjp.getTarget().getClass().getName() + pjp.getSignature().getName());

		String connectionId = (String) requestBody.getConnectionId();
		if (connectionId == null) {
			connectionId = (String) requestBody.getSessionId();
		}
		botTpiCallLog3D.setConnectionId(connectionId);
		botTpiCallLog3D.setApiStartTime(apiStartTime);
		botTpiCallLog3D.setApiEndTime(apiEndTime);
		botTpiCallLog3D.setApiCostTime(apiCostTime);
		botTpiCallLog3D.setTpigatewayStartTime(new Date(startTimeMillis));

		botTpiCallLog3D.setTpiHostName("易call保流程");

		// 執行完方法的返回值：調用proceed()方法，就會觸發切入點方法執行
		Map<String, Object> outputParamMap = new HashMap<>();
		Object result;
		result = pjp.proceed();

		outputParamMap.put("result", result);

		long endTimeMillis = System.currentTimeMillis(); // 記錄方法執行完成的時間

		int apiCostTimeFinal = botTpiCallLog3D.getApiCostTime();
		botTpiCallLog3D.setResult(mapper.writeValueAsString(outputParamMap));
		botTpiCallLog3D.setTpigatewayEndTime(new Date(endTimeMillis));
		botTpiCallLog3D.setTpigatewayCostTime((int) (endTimeMillis - startTimeMillis - apiCostTimeFinal));

		try {
			cbv10001DataService.saveBotTpiCallLog3D(botTpiCallLog3D);
		} catch (Exception e) {
			throw new CBV10001DataException(e.getMessage());
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Around("cbv10001ApiServicePointcut()")
	public Object handleCBVApiServiceMethod(ProceedingJoinPoint pjp) throws Throwable {

		BotTpiCallLog3D botTpiCallLog3D = (BotTpiCallLog3D) pjp.getArgs()[0];
		String apiUrl = (String) pjp.getArgs()[1];
		HttpEntity<Map<String, Object>> apiRequestBody = (HttpEntity<Map<String, Object>>) pjp.getArgs()[3];

		if (botTpiCallLog3D.getApiStartTime() == null) {
			botTpiCallLog3D.setApiStartTime(new Date(System.currentTimeMillis()));
		}
		botTpiCallLog3D.setInput(mapper.writeValueAsString(apiRequestBody.getBody()));

		ResponseEntity<?> result = (ResponseEntity<?>) pjp.proceed();
		HttpStatus httpStatus = result.getStatusCode();

		botTpiCallLog3D.setApiEndTime(new Date(System.currentTimeMillis()));
		botTpiCallLog3D.setOutput(mapper.writeValueAsString(result));
		botTpiCallLog3D.setApiUrl(apiUrl);

		int apiCostTime = 0;
		Date apiStartTime = botTpiCallLog3D.getApiStartTime();
		Date apiEndTime = botTpiCallLog3D.getApiEndTime();
		if (apiStartTime!= null && apiEndTime!= null) {
			apiCostTime = (int) (apiEndTime.getTime() - apiStartTime.getTime());
			botTpiCallLog3D.setApiCostTime(apiCostTime);
		}
		if (httpStatus != null) {
			botTpiCallLog3D.setRespStatus(httpStatus.toString());
		}

		return result;
	}

	@AfterThrowing(pointcut = "cbv10001ApiServicePointcut()", throwing = "ex")
	public void handleCBVApiServiceThrowExcetion(JoinPoint jp, Throwable ex) {

		BotTpiCallLog3D botTpiCallLog3D = (BotTpiCallLog3D) jp.getArgs()[0];
		String apiUrl = (String) jp.getArgs()[1];

		botTpiCallLog3D.setApiEndTime(new Date(System.currentTimeMillis()));
		botTpiCallLog3D.setRemark(getTpiCallLogRemark(ex));
		botTpiCallLog3D.setApiUrl(apiUrl);

		if (ex instanceof ResourceAccessException) {
			try {
				cbv10001DataService.saveBotTpiCallLog3D(botTpiCallLog3D);
			} catch (Exception e) {
				throw new CBV10001DataException(e.getMessage());
			}
		}
	}

	private String getTpiCallLogRemark(Throwable ex) {
		String remark = "";
		if (ex.getCause() != null) {
			remark = ex.getCause().toString();
		} else {
			remark = ex.toString();
		}
		if (remark.length() > 200) {
			remark = remark.substring(0, 200);
		}
		return remark;
	}

	@Pointcut("execution(* com.tp.tpigateway.service.life.flow.impl.cb.CBV10001ApiServiceImpl..*(..))")
	private void cbv10001ApiServicePointcut() {
	}
}
