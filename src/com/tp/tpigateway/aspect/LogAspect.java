package com.tp.tpigateway.aspect;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.tpigateway.util.JSONUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//@Aspect
//@Component
public class LogAspect {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ObjectMapper mapper = JSONUtils.getInstance();

    private String requestPath = null; // 請求地址
    private String args = null; //方法裡的參數
    private Map<?, ?> inputParamMap = null; // 傳入參數
    private Map<String, Object> outputParamMap = null; // 存放輸出結果
    private long startTimeMillis = 0; // 開始時間
    private long endTimeMillis = 0; // 結束時間

    @Around("execution(* com.tp.tpigateway.web.life..*(..))")
    public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {

        startTimeMillis = System.currentTimeMillis(); // 記錄方法開始執行的時間

        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();

        // 獲取輸入參數
        inputParamMap = request.getParameterMap();
        // 獲取請求地址
        requestPath = request.getRequestURI();

        args = Arrays.toString(pjp.getArgs());

        // 執行完方法的返回值：調用proceed()方法，就會觸發切入點方法執行
        outputParamMap = new HashMap<>();
        Object result = pjp.proceed();// result的值就是被攔截方法的返回值
        outputParamMap.put("result", result);

        endTimeMillis = System.currentTimeMillis(); // 記錄方法執行完成的時間

        //String startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS").format(startTimeMillis);
        //String endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS").format(endTimeMillis);
        long costTime = endTimeMillis - startTimeMillis;

        try {
            logger.debug( "\n" +
                    "**********************************************************************************" + "\n" +
                    //"startTime：" + startTime + "\n" +
                    "signature : " + pjp.getSignature().toString() + "\n" +
                    "url：" + requestPath + "\n" +
                    "args：" + args + "\n" +
                    "param：" + mapper.writeValueAsString(inputParamMap) + "\n" +
                    "result：" + mapper.writeValueAsString(outputParamMap) + "\n" +
                    //"endTime : " + endTime + "\n" +
                    "costTime : " + costTime + "\n" +
                    "**********************************************************************************" + "\n"
            );
        } catch (Exception e) {
            //不處理
        }

        return result;
    }

    @Around("execution(* com.tp.tpigateway.service.life.lifeapi..*(..))")
    public Object handleServiceMethod(ProceedingJoinPoint pjp) throws Throwable {

        startTimeMillis = System.currentTimeMillis(); // 記錄方法開始執行的時間

        args = Arrays.toString(pjp.getArgs());

        // 執行完方法的返回值：調用proceed()方法，就會觸發切入點方法執行
        outputParamMap = new HashMap<>();
        Object result = pjp.proceed();// result的值就是被攔截方法的返回值
        outputParamMap.put("result", result);

        endTimeMillis = System.currentTimeMillis(); // 記錄方法執行完成的時間

        //String startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS").format(startTimeMillis);
        //String endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS").format(endTimeMillis);
        long costTime = endTimeMillis - startTimeMillis;

        try {
            logger.debug( "\n" +
                    "**********************************************************************************" + "\n" +
                    //"startTime：" + startTime + "\n" +
                    "signature : " + pjp.getSignature().toString() + "\n" +
                    "args：" + args + "\n" +
                    "result：" + mapper.writeValueAsString(outputParamMap) + "\n" +
                    //"endTime : " + endTime + "\n" +
                    "costTime : " + costTime + "\n" +
                    "**********************************************************************************" + "\n"
            );
        } catch (Exception e) {
            //不處理
        }

        return result;
    }

}
