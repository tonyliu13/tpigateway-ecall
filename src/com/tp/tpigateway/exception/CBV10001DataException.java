package com.tp.tpigateway.exception;

/**
 * 自訂異常
 */
public class CBV10001DataException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private final String rootMessage;
	
	public CBV10001DataException(String rootMessage) {
		super(rootMessage);
		Throwable currentException = this;
		Throwable rootCause = getCause();
		while(rootCause != null) {
			currentException = rootCause;
			rootCause = currentException.getCause();
		};
		
		this.rootMessage = currentException.getMessage();
	}

	public String getRootMessage() {
		return rootMessage;
	}
}
