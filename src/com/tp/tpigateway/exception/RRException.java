package com.tp.tpigateway.exception;

import java.text.MessageFormat;
import java.util.Map;

import com.tp.tpigateway.util.exception.StatusCode;


/**
 * 自訂異常
 */
public class RRException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private StatusCode statusCode;
	private Map<String, ?> info;
	private Object[] arguments = {};

	public RRException(StatusCode statusCode, Object... arguments) {
        super(statusCode.getCode() + ":" + statusCode.getMessage());

        this.statusCode = statusCode;
        this.arguments = arguments;
    }

	public RRException(StatusCode statusCode, Map<String, ?> info, Object... arguments) {
        super(statusCode.getCode() + ":" + statusCode.getMessage());

        this.statusCode = statusCode;
        this.info = info;
        this.arguments = arguments;
    }

	public RRException(Throwable cause, StatusCode statusCode, Object... arguments) {
        super(statusCode.getCode() + ":" + statusCode.getMessage(), cause);

        this.statusCode = statusCode;
        this.arguments = arguments;
    }

	public RRException(Throwable cause, StatusCode statusCode,  Map<String, ?> info, Object... arguments) {
        super(statusCode.getCode() + ":" + statusCode.getMessage(), cause);

        this.statusCode = statusCode;
        this.arguments = arguments;
        this.info = info;
    }

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public Object[] getArguments() {
		return arguments;
	}

	public Map<String, ?> getInfo() {
		return info;
	}

	public String getErrorCode() {
		return statusCode.getCode();
	}

	public String getErrorMessage() {
		String message = null;

		if (arguments == null) {
			message = statusCode.getMessage();
		} else {
			message = MessageFormat.format(statusCode.getMessage(), arguments);
		}

		return message;
	}

	@Override
	public String getMessage() {
		String message = null;

		if (arguments == null) {
			message = statusCode.getMessage();
		} else {
			message = MessageFormat.format(statusCode.getMessage(), arguments);
		}

		return statusCode.getCode() + " : " + message;
	}

}
