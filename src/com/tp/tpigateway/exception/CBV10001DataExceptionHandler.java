package com.tp.tpigateway.exception;

import java.net.SocketTimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.ResourceAccessException;

import com.tp.tpigateway.model.life.CBV10001VO.GatewayResponse;

@RestControllerAdvice
public class CBV10001DataExceptionHandler {

	private static Logger log = LoggerFactory.getLogger(CBV10001DataExceptionHandler.class);
	
	@ExceptionHandler(CBV10001DataException.class)
	public GatewayResponse<?> handleException(CBV10001DataException e) {
		log.error(e.getRootMessage(), e);
		return GatewayResponse.buildFail(e.getRootMessage());
	}
	
	@ExceptionHandler(ResourceAccessException.class)
	public GatewayResponse<?> handleTimeoutException(ResourceAccessException e) {
		log.error(e.getMessage(), e);
		if(e.getCause() instanceof SocketTimeoutException) {
			return GatewayResponse.buildTimeout();
		}
		return GatewayResponse.buildFail(e.getMessage());
	}
}
