package com.tp.tpigateway.exception;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.TPIGatewayResult;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;
import com.tp.tpigateway.util.PropUtils;
import com.tp.tpigateway.util.exception.LifeApiStatusConstants;
import com.tp.tpigateway.util.exception.StatusCode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.Map;


@RestControllerAdvice
public class RRExceptionHandler {

	private static Logger log = LoggerFactory.getLogger(RRExceptionHandler.class);

	private static final String SOURCE = PropUtils.SYSTEM_SOURCE;

	@Autowired
	private BotMessageService botMessageService;
	@Autowired
	private BotTpiReplyTextService botTpiReplyTextService;

	/**
	 * 自訂異常
	 */
	@ExceptionHandler(RRException.class)
	public Map<String, Object> handleRRException(HttpServletRequest req, RRException re) {
		log.error("[handleRRException]" + re.getErrorMessage());
		log.debug("", re);

		StatusCode statusCode = re.getStatusCode();
		Map<String, Object> result = null;
//		if (statusCode == LifeApiStatusConstants.RETURN_CODE_ERROR || statusCode == LifeApiStatusConstants.RETURN_BODY_IS_EMPTY ||
//				statusCode == LifeApiStatusConstants.NO_DATA_FOUND || statusCode == LifeApiStatusConstants.HTTP_STATUS_ERROR) {
			BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);

			if (statusCode.equals(LifeApiStatusConstants.NOT_OWN_POLICYNO)) {
				BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), SOURCE, reqVO.getRole());
				// 回覆內容I2.0.2
				String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I1.0.1");
				botMessageService.setTextResult(vo, replyText);
				LifeLink[] links = { LifeLink.看其他保單, LifeLink.不了_結束對話 };
				botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
				return getSuccessResult(vo.getVO(), false);
			}
//		}
//		else {
//			return getErrorResult(re.getMessage());
//		}
		return result;
	}

	@ExceptionHandler(Exception.class)
	public Map<String, Object> handleException(Exception e) {
		log.error(e.getMessage(), e);
//		return getErrorResult(PropUtils.RESULT_DESCRIPTION_SYSTEM_ERROR);
		
		StringBuilder errorMsgBuilder = new StringBuilder();
		
		for(StackTraceElement stackTrace:e.getStackTrace()) {
			errorMsgBuilder.append(stackTrace.toString());
			errorMsgBuilder.append(System.getProperty("line.separator"));
		}
		
		return getErrorResult(errorMsgBuilder.toString(), e.getClass().getName() + ": " + e.getMessage());
	}

	public Map<String, Object> getSuccessResult(Object info, boolean breakUpInfo) {
		return new TPIGatewayResult(PropUtils.RESULT_CODE_SUCCESS, PropUtils.RESULT_DESCRIPTION_SUCCESS, info).getRegroupResult(breakUpInfo);
	}


	public Map<String, Object> getErrorResult(String error) {
		return new TPIGatewayResult(PropUtils.RESULT_CODE_FAIL, error, null).getRegroupResult(true);
	}
	
	public Map<String, Object> getErrorResult(String error, String data) {
		return new TPIGatewayResult(PropUtils.RESULT_CODE_FAIL, error, data).getRegroupResult(true);
	}

}
