package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.AllAutoComplete;
import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.life.BotLifeRequestVO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface BotMessageService extends CathayLifeBotMessageService {


	/*
	 * I0
	 */
	public List<Map<String, Object>> getCardForI0_1_1(BotMessageVO vo);	
	
	/*
	 * I1
	 */
	public List<Map<String, Object>> getCardsForI1_1_2(BotMessageVO vo, List<Object> h01Result, BotLifeRequestVO reqVO) throws Exception;
	
	public List<Map<String, Object>> getPolicyFilterForI1_1_2(BotMessageVO vo, List<Object> h01Result);

    /*
     * I2
     */
	public List<Map<String,Object>> getCardsForI2_0_1(BotMessageVO vo, Map<String, Object> e02Result);

	public List<Map<String,Object>> getCardsForI2_1(BotMessageVO vo, Map<String, Object> e02Result);

	public List<Map<String, Object>> getCardsForI2_2_1(BotMessageVO vo, Map<String, Object> e02Result);

	public List<Map<String,Object>> getCardsForI2_3(BotMessageVO vo, Map<String, Object> e02Result);

	public List<Map<String,Object>> getCardsForI2_3(BotMessageVO vo, Map<String, Object> e02Result, String cMessage);

	public List<Map<String, Object>> getCardsForI2_5_3(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI2_6_1(BotMessageVO vo, Map<String, Object> e02Result,
			Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI2_7_1(BotMessageVO vo, Map<String, Object> e02Result,
                                                       Map<String, Object> h03Result, Map<String, Object> d01Result);

	public List<Map<String, Object>> getCardsForI2_8_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result);

	public List<Map<String, Object>> getCardsForI2_9_2(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result);

	public List<Map<String, Object>> getCardsForI2_10_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI2_11_2(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result);

	public List<Map<String, Object>> getCardsForI2_12_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result);
	
	public List<Map<String, Object>> getCardsForI2_12_1_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> d01Result);
	
	public List<Map<String, Object>> getCardsForI2_12_2_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> d01Result);
	
	public List<Map<String, Object>> getCardsForI2_12_2_4(BotMessageVO vo);
	public List<Map<String, Object>> getCardsForI2_12_2_4(BotMessageVO vo, String policyNo);
	
	/*
	 * I3
	 */
	public Map<String, Object> getCardForI3_1_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result);

	public Map<String, Object> getCardForI3_2_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result);
	
	public Map<String, Object> getCardForI3_3_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result);
	
	public Map<String, Object> getCardForI3_4_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result);
	
	public Map<String, Object> getCardForI3_5_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result);
	
	public Map<String, Object> getCardForI3_6_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result);
	
	/*
	 * I4
	 */
	public List<Map<String, Object>> getCardsForI4_1_2(BotMessageVO vo, List<Object> d06Result, Map<String, Object> e02Result);

	/*
	 * I6
	 */
	public List<Map<String, Object>> getCardsForI6_1_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);
	
	public List<Map<String, Object>> getCardsForI6_2_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);
	
	public List<Map<String, Object>> getCardsForI6_3_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI6_4_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI6_5_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI6_6_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI6_7_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI6_8_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI6_9_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	public List<Map<String, Object>> getCardsForI6_10_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result);

	/*
	 * I7
	 */
	// public List<Map<String, Object>> getCardsForI7_1_2(BotMessageVO vo);
	public List<Map<String, Object>> getCardsForI7_1_2(BotMessageVO vo, Map<String, Object> e02Result);
	
	public List<Map<String, Object>> getCardsForI7_2_2(BotMessageVO vo);
	
	/*
	 * I8
	 */
	public List<Map<String, Object>> getCardsForI8_1_2(BotMessageVO vo);

	public List<Map<String, Object>> getCardsForI8_1_3_2(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardsForI8_1_3_4(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardsForI8_1_3_6(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardsForI8_1_3_8(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardsForI8_1_4_1(BotMessageVO vo);
	
	/*
	 * I11
	 */
	public List<Map<String, Object>> getCardsForI11_1_1_1_3(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardsForI11_1_2_1_3(BotMessageVO vo);
	
	/*
	 * I12
	 */
	public List<Map<String, Object>> getCardsForI12_1_3(BotMessageVO vo);
	
	/*
	 * I3
	 */
	public List<Map<String, Object>> getCardForI13_1_3(BotMessageVO vo);

	/*
	 * I19
	 */
	public List<Map<String, Object>> getCardsForI19_1_1_3(BotMessageVO vo);

	public List<Map<String, Object>> getCardsForI19_1_2_3(BotMessageVO vo);

	/*
	 * I20
	 */
	public List<Map<String, Object>> getCardsForI20_1_1_2(BotMessageVO vo, List<Object> h02Result);

	/*
	 * I21
	 */
	public List<Map<String, Object>> getCardsForI21_1_2(BotMessageVO vo, Map<String, Object> d16Result);
	
	/*
	 * I15
	 */
	public List<Map<String, Object>> getCardsForI15_1_3(BotMessageVO vo, boolean isLogin, Map<String,Object> d14Result);

	public List<Map<String, Object>> getCardsForI15_1_8(BotMessageVO vo);

	/*
	 * I16
	 */
	public List<Map<String, Object>> getCardsForI16_1_3(BotMessageVO vo, boolean isLogin, Map<String,Object> d14Result);
	
	public List<Map<String, Object>> getCardsForI16_1_8(BotMessageVO vo);

    /*
         * I22
         */
	public List<Map<String, Object>> getCardsForI22_1_1(BotMessageVO vo);

	public List<Map<String, Object>> getCardsForI22_1_1_2_3(BotMessageVO vo);
	//20181108優化
	public List<Map<String, Object>> getCardsForI22_1_1_2_4(BotMessageVO vo);

	/*
	 * I23
	 */
	public List<Map<String, Object>> getCardsForI23_1_2(BotMessageVO vo);
	
	/*
	 * I17
	 */
	public List<Map<String, Object>> getCardForI17_1_2(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardForI17_1_3(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardsForI7_3_2(BotMessageVO vo, Map<String, Object> e02Result);

	// public List<Map<String, Object>> getCardsForI7_4_2(BotMessageVO vo);
	public List<Map<String, Object>> getCardsForI7_4_2(BotMessageVO vo, Map<String, Object> e02Result);

	/*
	 * I18
	 */
	public List<Map<String, Object>> getCardForI18_1_2(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardForI18_1_3(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardForI18_1_4(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardForI18_1_5(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardForI18_1_6(BotMessageVO vo);
	
	public List<Map<String, Object>> getCardForI18_1_7(BotMessageVO vo);
	
	/*
	 * I24
	 */
	public List<Map<String, Object>> getCardForI24_1_2(BotMessageVO vo);

	
	public String getPremTrnCodeName(Map<String, Object> e02Result);
	
	public String getPremTrnCodeName_H01( Map<String, Object> h01ResultMap );

	public BotMessageVO getAutoCompleteBotMessageVO(List<AllAutoComplete> dataList);
}