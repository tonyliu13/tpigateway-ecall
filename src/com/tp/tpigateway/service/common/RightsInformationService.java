package com.tp.tpigateway.service.common;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import com.tp.tpigateway.model.life.BotLifeRequestVO;

public interface RightsInformationService {

	/**
	 * 客戶權益提醒：作法二-權益懶人包
	 * (非同步執行項目，主動回推訊息給ChatWeb)
	 * 
	 * @param chatId
	 * @param reqVO
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 */
	public void queryRightsInformationWithAsync(String chatId, BotLifeRequestVO reqVO) throws JsonGenerationException, JsonMappingException, IOException;
	
	
	/**
	 * 檢查是否符合生日禮/權益懶人包-壽星
	 * 
	 * @param chatId
	 * @param reqVO
	 * @return true: 符合, false: 不符合
	 */
	public boolean checkBirthday(String chatId, BotLifeRequestVO reqVO);
}
