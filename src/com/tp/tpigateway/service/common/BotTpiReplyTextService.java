package com.tp.tpigateway.service.common;

/**
 * bot tpi 回覆清單服務介面
 */
public interface BotTpiReplyTextService {

	/**
	 * 取得指定角色(roldId)、節點(nodeId)、回覆編號(textId)的回覆內容
	 * 
	 * @param roleId
	 * @param nodeId
	 * @param textId
	 * @return
	 */
	public String getReplyText(String roleId, String nodeId, String textId);

    String getReplyText(String roleId, String textId);

    /**
	 * 更新回覆清單
	 */
	public void refreshReplyText();
	
}
