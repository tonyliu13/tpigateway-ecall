package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.BotCampaignLog;
import com.tp.tpigateway.model.common.BotCampaignLogId;

public interface BotCampaignLogService {

    public BotCampaignLog findByBotCampaignLogId(BotCampaignLogId botCampaignLogId);
    
    public void save(BotCampaignLog campaignLog);
    
    public BotCampaignLog findByCampaignIdAndCustId(String campaignId, String custId);
}
