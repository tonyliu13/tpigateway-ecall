package com.tp.tpigateway.service.common;

import java.sql.Timestamp;

import com.tp.tpigateway.model.common.BotRequestAndResponse;
import com.tp.tpigateway.model.common.BotTpiCallLog;

/**
 * 呼叫API log紀錄服務介面
 */
public interface BotTpiCallLogService {

	/**
	 * 新增一筆
	 * 
	 * @param log
	 */
	public void save(BotTpiCallLog log);
	
	/**
	 * 查詢資料，依據chatID 及 insertTime
	 * @param chatID
	 * @param insertTimeStart
	 * @param insertTimeEnd
	 * @return List<BotTpiCallLog>
	 */
	public BotRequestAndResponse queryByChatID(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd);
	
}
