package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.BotTpiCache;

public interface BotTpiCacheService {

    public BotTpiCache findByCacheKey(String cacheKey);
    
    public void save(BotTpiCache cache);
    
    public void update(BotTpiCache cache);
    
    public void delete(String cacheKey);
}
