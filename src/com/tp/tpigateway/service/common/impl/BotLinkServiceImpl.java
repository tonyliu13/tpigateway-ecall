package com.tp.tpigateway.service.common.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.configuration.BotMessageSetting;
import com.tp.tpigateway.dao.common.BotLinkDao;
import com.tp.tpigateway.dao.common.BotLinkOptionDao;
import com.tp.tpigateway.model.common.APIResult;
import com.tp.tpigateway.model.common.BotLink;
import com.tp.tpigateway.model.common.BotLinkOption;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.service.common.BotLinkService;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.util.LocalCache;

/**
 * BotLink 服務實作
 */
@Service("botLinkService")
@Transactional
public class BotLinkServiceImpl implements BotLinkService {

	private static final Logger log = LoggerFactory.getLogger(BotLinkServiceImpl.class);

	private boolean isDebug = log.isDebugEnabled();

	private boolean isInfo = log.isInfoEnabled();

	@Autowired
	private BotLinkOptionDao botLinkOptionDao;

	@Autowired
	private BotLinkDao botLinkDao;

	@Autowired
	private BotMessageService botMessageService;

	private static final String cacheKey = BotLinkServiceImpl.class.getName() + "_";

	/**
	 * 取得連結，使用指定資料替換選項內容
	 * @param optionid
	 * @param data
	 * @return
	 */
	public LifeLink[] getOptionLinks(String optionid, Map data, String optionDataKey) {
		APIResult apiResult = new APIResult();
		apiResult.putApiResult(optionDataKey, data);
		LifeLink[] links = this.getOptionLinks(optionid, apiResult);
		return links;
	}
	
	public LifeLink[] getOptionLinks(String optionid, APIResult data) {

		List<BotLinkOption> options = botLinkOptionDao.findByOptionID(optionid);
		int linkOptionSize = options.size();

		if (isDebug) {
			log.debug("[option_id=" + optionid + "] bot_link_option size=" + linkOptionSize);
		}

		LifeLink[] links = new LifeLink[linkOptionSize];
		
		this.getOptions(options, data, links, optionid);

		return links;
	}

	public LifeLink getLink(String linkID) {
		return createLink(getBotLink(linkID));
	}
	
	@Override
	public LifeLink[] getReplaceOptionLinks(String optionID, Map data, String replayContentdataKey, String optionDataKey) {
		APIResult apiResult = new APIResult();
		apiResult.putApiResult(optionDataKey, data);
		LifeLink[] links = this.getReplaceOptionLinks(optionID, apiResult, replayContentdataKey);
		return links;
	}
	@Override
	public LifeLink[] getReplaceOptionLinks(String optionid, APIResult data, String replayContentdataKey) {
		
		List<BotLinkOption> options = botLinkOptionDao.findByOptionID(optionid);
		log.debug("OptionLink Size:"+ options.size());
		
		Map apiResultDataMap = null;
		String[] replaceLinks = null; 
		if(StringUtils.isNotBlank(replayContentdataKey)) {
			apiResultDataMap = (Map) data.getApiResult(replayContentdataKey);
			if(apiResultDataMap.get(BotMessageSetting.REPLACE_OPTION_WITH_LINKID) instanceof String[]){
				replaceLinks = (String[]) apiResultDataMap.get(BotMessageSetting.REPLACE_OPTION_WITH_LINKID);
			}else {
				log.error("傳入要更換的選項link_id，不為字串陣列格式");
			}
		}else {
			log.error("bot_replay_content資料表中無設定data_key值");
		}
		
		LifeLink[] links = new LifeLink[replaceLinks.length];
		List<BotLinkOption> filterOptions = this.getReplaceOptions(replaceLinks, options, data, links);
		this.getOptions(filterOptions, data, links, optionid);
		return links;
	}
	
	public LifeLink getDynamicLink(String linkID, Map<String, String> paramMap) {

		BotLink link = getBotLink(linkID);
		LifeLink thelink;
		if (StringUtils.isNotBlank(link.getReplyDynamic())) {
			Integer linkAction = link.getAction();
			String linkReplyDynamic = link.getReplyDynamic();

			if (isInfo) {
				log.info("Action=" + linkAction);
				log.info("linkReplyDynamic=" + linkReplyDynamic);
				log.info("paramMap=" + paramMap);
			}

			String dynamicText = botMessageService.replaceTextByParamMap(linkReplyDynamic, paramMap);
			if (isInfo) {
				log.info("dynamicText=" + dynamicText);
			}

			if (12 == linkAction || 13 == linkAction) {
				link.setAlt(dynamicText);
			} else {
				link.setReply(dynamicText);
			}

			// 動態替換 BotMessage
			thelink = createLink(link);
		} else {
			thelink = createLink(link);
		}

		return thelink;
	}

	public LifeLink createLink(BotLink link) {
		if (isDebug) {
			log.debug("bot_link=" + link);
		}

		LifeLink thelink = LifeLink.buildLink(link.getAction(), link.getText(), link.getAlt(), link.getReply(), link.getUrl(),
				link.getCustomerAction());

		if (isDebug) {
			log.debug("bot_link >>> LifeLink=" + thelink);
		}

		return thelink;
	}

	public BotLink getBotLink(String linkID) {
		String cacheKey_linkID = cacheKey + linkID;
		BotLink link = (BotLink) LocalCache.get(cacheKey_linkID);

		if (link == null) {
			try {
				link = LocalCache.register(cacheKey_linkID, botLinkDao.findByLinkID(linkID));
			} catch (Exception e) {
				log.error("[get bot_link error] link_id:" + linkID, e);
				throw e;
			}
		}

		return link;
	}
	
	private List<BotLinkOption> getReplaceOptions(String[] replaceLinks, List<BotLinkOption> options, APIResult data, LifeLink[] links) {
		List<BotLinkOption> replaceOptions = new ArrayList<BotLinkOption>();
		for(int i = 0; i < options.size(); i++) {
			BotLinkOption option = options.get(i);
			log.debug("[BotLinkOption]:" + option);
			for(String linkId : replaceLinks) {
				if(linkId.equals(option.getLinkId())) {
					log.debug("[ReplaceBotLinkOption]:" + linkId);
					replaceOptions.add(option);
					break;
				}
			}
		}
		return replaceOptions;
	}
	
	private void getOptions(List<BotLinkOption> options, APIResult data, LifeLink[] links, String optionid) {	
		for (int i = 0; i < options.size(); i++) {
			BotLinkOption option = options.get(i);

			if (isDebug) {
				log.debug("[option_id=" + optionid + ", option_seq=" + option.getId().getOptionSeq() + "] bot_link_option=" + option);
			}

			LifeLink thelink;

			if ('Y' == option.getDynamicFlag()) {
				String dataKey = option.getDataKey();
				Map<String, String> paramMap = (Map) data.getApiResult(dataKey);

				if (isInfo) {
					log.info("[dataKey=" + dataKey + "] APIResult=" + paramMap);
				}

				thelink = getDynamicLink(option.getLinkId(), paramMap);
			} else {
				thelink = getLink(option.getLinkId());
			}

			if (StringUtils.isNotBlank(option.getText())) {
				thelink = LifeLink.buildLink(thelink.getAction(), option.getText(), thelink.getAlt(), thelink.getReply(),
						thelink.getUrl(), thelink.getCustomerAction());
			}

			links[i] = thelink;
		}
	}
}
