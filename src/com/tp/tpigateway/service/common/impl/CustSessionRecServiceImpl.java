package com.tp.tpigateway.service.common.impl;

import com.tp.tpigateway.configuration.cache.CacheName;
import com.tp.tpigateway.model.common.CustSessionRec;
import com.tp.tpigateway.repository.CustSessionRecRepository;
import com.tp.tpigateway.service.common.CustSessionRecService;
import com.tp.tpigateway.util.DataUtils;
import com.tp.tpigateway.util.TPIStringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("custSessionRecService")
@Transactional
@CacheConfig(cacheNames = CacheName.CUST_STAGE_REC_KEY)
public class CustSessionRecServiceImpl implements CustSessionRecService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustSessionRecRepository custSessionRecRepository;

    @Autowired
    private CacheManager cacheManager;

    @Override
    public Cache getCustSessionRecCache() {
        return cacheManager.getCache(CacheName.CUST_STAGE_REC_KEY);
    }

    private void putCache(String key, CustSessionRec value) {
        getCustSessionRecCache().put(key, value);
    }

    private CustSessionRec getCache(String key) {
        return getCustSessionRecCache().get(key, CustSessionRec.class);
    }

    @Override
    @Transactional(readOnly = true)
    //spring aop同層內其他method調用此方法是不走代理的(所以不會用到@Cacheable)，這邊先不用註解方式
    //@Cacheable(key = "#customerId + '_' + #date", condition="#result != null")
    public CustSessionRec findByCustomerIdAndDate(String customerId, String date) {
        String key = CacheName.genCustStageRecKey(customerId, date);
        CustSessionRec custSessionRec = getCache(key);

        if (custSessionRec != null) {
            return custSessionRec;
        }

        custSessionRec = custSessionRecRepository.findByCustomerIdAndDate(customerId, date);
        if (custSessionRec != null) {
            putCache(key, custSessionRec);
        }

        return custSessionRec;
    }

    @Override
    @CachePut(key = "#result.customerId + '_' + #result.date")
    public CustSessionRec addCustSessionRec(CustSessionRec custSessionRec) {
        String uuid = TPIStringUtil.getUUID();
        custSessionRec.setId(uuid);

        if (custSessionRec.getSessionTime() == null) {
            custSessionRec.setSessionTime(1);
        }

        custSessionRecRepository.save(custSessionRec);

        return custSessionRec;
    }

    @Override
    @CachePut(key = "#custSessionRec.customerId + '_' + #custSessionRec.date", condition = "#result != null")
    public CustSessionRec updateCustSessionRec(CustSessionRec custSessionRec) {
        CustSessionRec oldCustSessionRec = findByCustomerIdAndDate(custSessionRec.getCustomerId(), custSessionRec.getDate());

        if (oldCustSessionRec == null) {
            return custSessionRec;
        }

        BeanUtils.copyProperties(custSessionRec, oldCustSessionRec, DataUtils.getNullPropertyNames(custSessionRec));

        custSessionRecRepository.save(oldCustSessionRec);

        return oldCustSessionRec;
    }

    @Override
    @CachePut(key = "#custSessionRec.customerId + '_' + #custSessionRec.date", condition = "#result != null")
    public CustSessionRec saveCustSessionRec(CustSessionRec custSessionRec) {
        CustSessionRec oldCustSessionRec = findByCustomerIdAndDate(custSessionRec.getCustomerId(), custSessionRec.getDate());

        if (oldCustSessionRec != null)
            return updateCustSessionRec(custSessionRec);
        else
            return addCustSessionRec(custSessionRec);
    }

}
