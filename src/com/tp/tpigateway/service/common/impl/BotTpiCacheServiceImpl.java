package com.tp.tpigateway.service.common.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.BotTpiCacheDao;
import com.tp.tpigateway.model.common.BotTpiCache;
import com.tp.tpigateway.service.common.BotTpiCacheService;

@Service("botTpiCacheService")
@Transactional
public class BotTpiCacheServiceImpl implements BotTpiCacheService {

    private static Logger log = LoggerFactory.getLogger(BotTpiCacheServiceImpl.class);
    
    @Autowired
    private BotTpiCacheDao botTpiCacheDao;

    @Override
    public BotTpiCache findByCacheKey(String cacheKey) {
        return botTpiCacheDao.findByCacheKey(cacheKey);
    }

    @Override
    public void save(BotTpiCache cache) {
        log.debug("[save] key=" + cache.getCacheKey());
        try {
            botTpiCacheDao.save(cache);
        } catch (Exception e) {
            log.error("[save] error:" + e.getMessage());
            try {
                botTpiCacheDao.update(cache);
            } catch (Exception updateEx) {
                log.error("[save/update] error:" +updateEx.getMessage());
            }
        }
    }
    
    @Override
    public void update(BotTpiCache cache) {
        log.debug("[update] key=" + cache.getCacheKey());
        try {
            botTpiCacheDao.update(cache);
        } catch (Exception e) {
            log.error("[update] error:" + e.getMessage());
        }
    }
    
    @Override
    public void delete(String cacheKey) {
        log.debug("[delete] key=" + cacheKey);
        try {
            botTpiCacheDao.delete(cacheKey);
        } catch (Exception e) {
            log.error("[delete] error:" + e.getMessage());
        }
    }
}
