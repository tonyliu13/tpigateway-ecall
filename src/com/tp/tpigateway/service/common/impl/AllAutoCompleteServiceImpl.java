package com.tp.tpigateway.service.common.impl;

import com.tp.tpigateway.dao.common.AllAutoCompleteDao;
import com.tp.tpigateway.model.common.AllAutoComplete;
import com.tp.tpigateway.service.common.AllAutoCompleteService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service("AllAutoCompleteService")
public class AllAutoCompleteServiceImpl implements AllAutoCompleteService {

	@Autowired
	private AllAutoCompleteDao allAutoCompleteDao;
	
	private final static List<AllAutoComplete> autoCompletes = new ArrayList<>();
	
	private static boolean isUpdate = false;

	@Override
	public List<AllAutoComplete> getAllAutoComplete() {
		List<AllAutoComplete> acList = new ArrayList<>();
		waittingUpdate ();	
		acList.addAll(autoCompletes);
		return acList;
	}

	@Override
	public void refreshAllAutoComplete () {
		log.debug("<refreshAllAutoComplete> start");
		List<AllAutoComplete> autoCompleteList = allAutoCompleteDao.findAll();
		if (CollectionUtils.isNotEmpty(autoCompleteList)) {
			isUpdate = true;
			autoCompletes.clear();
			autoCompletes.addAll(autoCompleteList);			
			isUpdate = false;
		}
		log.debug("<refreshAllAutoComplete> finish");
	}
	
	@Override
	public void waittingUpdate () {
		// 若正在更新, 則稍候再取得資料
		if (!isUpdate && CollectionUtils.isNotEmpty(autoCompletes)) {
			this.refreshAllAutoComplete();
		}
		try {
			while (isUpdate) {
				Thread.sleep(100);
			}
		} catch (Exception e) {
		}
	}
}
