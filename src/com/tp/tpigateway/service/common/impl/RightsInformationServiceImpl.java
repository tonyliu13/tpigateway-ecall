package com.tp.tpigateway.service.common.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CImageDataItem;
import com.tp.tpigateway.model.common.BotMessageVO.CTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;
import com.tp.tpigateway.model.common.BotMessageVO.ContentItem;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.RightsLink;
import com.tp.tpigateway.model.common.enumeration.A32;
import com.tp.tpigateway.model.common.enumeration.Rights;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BaseService;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;
import com.tp.tpigateway.service.common.RightsInformationService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeAService;
import com.tp.tpigateway.util.DateUtil;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

import okhttp3.RequestBody;

/**
 * 查詢權益懶人包的服務實作
 */
@Service("RightsInformationService")
public class RightsInformationServiceImpl extends BaseService implements RightsInformationService {

	private static Logger log = LoggerFactory.getLogger(RightsInformationServiceImpl.class);

	@Autowired
	private BotMessageService botMessageService;
	@Autowired
	private BotTpiReplyTextService botTpiReplyTextService;
	@Autowired
	private CathayLifeTypeAService cathayLifeTypeAService;

	private static final String SOURCE = PropUtils.SYSTEM_SOURCE;
	private static final String CHAT_REPLY_URL = PropUtils.getProperty("chat.reply.url");

	@Override
	@Async
	public void queryRightsInformationWithAsync(String chatId, BotLifeRequestVO reqVO)
			throws JsonGenerationException, JsonMappingException, IOException {

		BotMessageVO result = botMessageService.initBotMessage(reqVO.getChannel(), SOURCE, reqVO.getRole());

		BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), SOURCE, reqVO.getRole());

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		// 權益懶人包項目(生日、<滿期金、年金、未繳保費、墊繳、停效>?)
		StringBuilder infoList = new StringBuilder();
		
		// 保戶好康(沒有有效優惠權益提醒項目時提供)
		boolean memberService = true;
		
		// 生日
		boolean isBirth = checkBirthday(chatId, reqVO);
		memberService = memberService & !isBirth;
		// if (checkBirthday(chatId, reqVO)) {
        CardItem card1 = getBirthdayCard(reqVO, vo, isBirth);
        cardList.add(card1.getCards());
        if (isBirth)
            infoList.append(Rights.BIRTHDAY.name() + "|");
		// }
		
		// 契約停效
		int termPolicyCount = checkTermPolicy(chatId, reqVO);
		if (termPolicyCount >= 0) {
		    CardItem card2 = getTermPolicyCard(reqVO, vo, termPolicyCount);
		    cardList.add(card2.getCards());
		}
        if (termPolicyCount > 0)
            infoList.append(Rights.TERM_POLICY.name() + "|");
		// }
        
        // 滿期金未辦件
        int maturityCount = checkMaturity(chatId, reqVO);
        if (maturityCount >= 0) {
            CardItem card3 = getMaturityCard(reqVO, vo, maturityCount);
            cardList.add(card3.getCards());
        }
        if (maturityCount > 0)
            infoList.append(Rights.MATURITY.name() + "|");
        
        // 年金未辦件
        int annutiyCount = checkAnnutiy(chatId, reqVO);
        if (annutiyCount >= 0) {
            CardItem card4 = getAnnutiyCard(reqVO, vo, annutiyCount);
            cardList.add(card4.getCards());
        }
        if (annutiyCount > 0)
            infoList.append(Rights.ANNUITY.name() + "|");
        
        // 滿期金將到期
        int maturityDueCount = checkMaturityDue(chatId, reqVO);
        if (maturityDueCount >= 0) {
            CardItem card5 = getMaturityDueCard(reqVO, vo, maturityDueCount);
            cardList.add(card5.getCards());
        }
        if (maturityDueCount > 0)
            infoList.append(Rights.MATURITY_DUE.name() + "|");
        
        // 年金將到期
        int annutiyDueCount = checkAnnutiyDue(chatId, reqVO);
        if (annutiyDueCount >= 0) {
            CardItem card6 = getAnnutiyDueCard(reqVO, vo, annutiyDueCount);
            cardList.add(card6.getCards());
        }
        if (annutiyDueCount > 0)
            infoList.append(Rights.ANNUITY_DUE.name() + "|");

        // 20190717 by Todd, 後續都有牌卡，沒有機會走到else
		String hintInfo = infoList.toString();
		// if (StringUtils.isNotBlank(hintInfo)) {

			// 執行動作：作法二，給予權益懶人包提醒wording
			ContentItem contentH = result.new ContentItem();
			contentH.setType(12);
			contentH.setAction(5);

			String hint0, hint1, hint2;

			// 提醒內容{RIGHTS.HINT.0}
			hint0 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.0");

			if (hintInfo.equals(Rights.BIRTHDAY.name() + "|")) {
				contentH.setWidget("doWay2BirthdayGift");

				// 提醒內容{RIGHTS.HINT.BIRTHDAY}
				hint1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.BIRTHDAY");
            } else if (hintInfo.equals(Rights.TERM_POLICY.name() + "|")) {
                contentH.setWidget("doWay2Rights");

                // 提醒內容{RIGHTS.HINT.TERMPOLICY}
                hint1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.TERMPOLICY");
            } else if (hintInfo.equals(Rights.MATURITY.name() + "|")) {
                contentH.setWidget("doWay2Rights");
                
                // 提醒內容{RIGHTS.HINT.MATURITY}
                hint1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.MATURITY");
            } else if (hintInfo.equals(Rights.ANNUITY.name() + "|")) {
                contentH.setWidget("doWay2Rights");
                
                // 提醒內容{RIGHTS.HINT.ANNUITY}
                hint1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.ANNUITY");
            } else if (hintInfo.equals(Rights.MATURITY_DUE.name() + "|")) {
                contentH.setWidget("doWay2Rights");
                
                // 提醒內容{RIGHTS.HINT.MATURITY_DUE}
                hint1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.MATURITY_DUE");
            } else if (hintInfo.equals(Rights.ANNUITY_DUE.name() + "|")) {
                contentH.setWidget("doWay2Rights");
                
                // 提醒內容{RIGHTS.HINT.ANNUITY_DUE}
                hint1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.ANNUITY_DUE");
			} else if (StringUtils.isNotBlank(hintInfo)) {
				contentH.setWidget("doWay2Rights");

				// 提醒內容{RIGHTS.HINT.ALL}
				hint1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.ALL");
			} else {
			    contentH.setWidget("doWay2Rights");

                // 提醒內容{RIGHTS.HINT.ALL}
                hint1 = null;
			}
			
			hint2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.ALL");

			// 回覆內容{RIGHTS.CONTENT.1}
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.CONTENT.1");
			botMessageService.setTextResult(vo, replyText);
			
			botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
			
			// 保戶好康(沒有有效優惠權益提醒項目時提供)
			if (memberService) {
			    // 回覆內容{RIGHTS.CONTENT.2}
			    String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.CONTENT.2");
			    botMessageService.setTextResult(vo, replyText2);
			}

			ContentItem contentR = result.new ContentItem();
			contentR.setType(12);
			contentR.setAction(5);
			contentR.setWidget("doRightsInformation");
			
			ObjectMapper ow = new ObjectMapper();
			String rightsItem = StringUtils.replaceAll(ow.writeValueAsString(vo.getVO()), " ", "@@");
			contentR.setText(new String((new Base64()).encode(StringUtils.replaceAll(URLEncoder.encode(rightsItem, "UTF-8"), "%40%40", "%20").getBytes()), "UTF-8"));
			result.getContent().add(contentR.getContent());

            Map<String, Object> item = contentH.getContent();
            item.put("hint0", hint0);
            if (StringUtils.isNotBlank(hint1))
                item.put("hint1", hint1);
            item.put("hint2", hint2);

			result.getContent().add(item);
			result.setDisplayChange("N");
			
			sendMsg(CHAT_REPLY_URL, chatId, ow.writeValueAsString(result.getVO()));
		/*} else { // 無任何權益內容
			// 執行動作：作法二，給予權益懶人包提醒wording
			ContentItem contentH = result.new ContentItem();
			contentH.setType(12);
			contentH.setAction(5);
			
			contentH.setWidget("doWay2Rights");
			
			// 回覆內容{RIGHTS.HINT.NOTHING}
			String hintNothing = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.HINT.NOTHING");
			
			Map<String, Object> item = contentH.getContent();
			item.put("hintNothing", hintNothing);

			result.getContent().add(item);
			result.setDisplayChange("N");
			
			ObjectMapper ow = new ObjectMapper();
			sendMsg(CHAT_REPLY_URL, chatId, ow.writeValueAsString(result.getVO()));
		}*/
	}

	@Override
	public boolean checkBirthday(String chatId, BotLifeRequestVO reqVO) {

		boolean result = false;
		try {
			if (StringUtils.isNotBlank(reqVO.getIDNo())) {
				Map<String, Object> a05Result = cathayLifeTypeAService.a05(reqVO.getIDNo());
				String ROLE_BRDY = MapUtils.getString(a05Result, "ROLE_BRDY");
				if (StringUtils.isNotBlank(ROLE_BRDY)) {
					int birdMonth = DateUtil.getMonth(DateUtil.getDateByPattern(ROLE_BRDY, DateUtil.PATTERN_DASH_DATE));
					int thisMonth = DateUtil.getMonth(new Date());

					// 當月壽星
					if (birdMonth == thisMonth) {
						log.error("[checkBirthday][" + chatId + "] 符合生日禮");
						result = true;
					}
				}
			}
		} catch (Exception e) {
			log.error("[checkBirthday][" + chatId + "] 檢查是否生日提醒發生錯誤:" + e.getMessage(), e);
		}

		return result;
	}
	
    private int checkTermPolicy(String chatId, BotLifeRequestVO reqVO) {
	    
	    int result = -1;
	    try {
	        if (StringUtils.isNotBlank(reqVO.getIDNo())) {
	            List<Map<String,Object>> detail = cathayLifeTypeAService.a31(reqVO.getIDNo());
	            result = 0;
	            if (CollectionUtils.isNotEmpty(detail)) {
	                
	                // 停效保單筆數
	                if (detail.size() > 0) {
	                    log.error("[checkTermPolicy][" + chatId + "] 查詢要保人停效保單筆數：" + detail.size());
	                    result = detail.size();
	                }
	            }
	        }
	    } catch (Exception e) {
	        log.error("[checkTermPolicy][" + chatId + "] 檢查是否契約停效發生錯誤:" + e.getMessage(), e);
	    }
	    
	    return result;
	}
    
    private int checkMaturity(String chatId, BotLifeRequestVO reqVO) {
        
        int result = -1;
        try {
            if (StringUtils.isNotBlank(reqVO.getIDNo())) {
                List<Map<String,Object>> detail = cathayLifeTypeAService.a32("1", null, reqVO.getIDNo(), "0");
                result = 0;
                if (CollectionUtils.isNotEmpty(detail)) {
                    
                    List<Map<String,Object>> detail_filter = new ArrayList<Map<String,Object>>();
                    for (Map<String,Object> map : detail) {
                        String INSD_ID = MapUtils.getString(map, A32.INSD_ID.name()); // 被保人ID
                        String APC_ID = MapUtils.getString(map, A32.APC_ID.name()); // 要保人ID
                        
                        // 查詢結果[ID]等於要保人ID或被保人ID(目的是排除受款人ID)
                        if (StringUtils.equals(reqVO.getIDNo(), INSD_ID) || StringUtils.equals(reqVO.getIDNo(), APC_ID)) {
                            detail_filter.add(map);
                        }
                    }
                    
                    // 未申請滿期金保單筆數
                    // if (detail.size() > 0) {
                    if (detail_filter.size() > 0) {
                        log.error("[checkMaturity][" + chatId + "] 查詢未申請滿期金保單筆數：" + detail.size());
                        result = detail.size();
                    }
                }
            }
        } catch (Exception e) {
            log.error("[checkMaturity][" + chatId + "] 檢查未申請滿期金保單錯誤:" + e.getMessage(), e);
        }
        
        return result;
    }
    
    private int checkAnnutiy(String chatId, BotLifeRequestVO reqVO) {
        
        int result = -1;
        try {
            if (StringUtils.isNotBlank(reqVO.getIDNo())) {
                List<Map<String,Object>> detail = cathayLifeTypeAService.a32("2", null, reqVO.getIDNo(), "0");
                result = 0;
                if (CollectionUtils.isNotEmpty(detail)) {
                    
                    List<Map<String,Object>> detail_filter = new ArrayList<Map<String,Object>>();
                    List<String> policyNoList = new ArrayList<String>();
                    
                    for (Map<String,Object> map : detail) {
                        String INSD_ID = MapUtils.getString(map, A32.INSD_ID.name()); // 被保人ID
                        String APC_ID = MapUtils.getString(map, A32.APC_ID.name()); // 要保人ID
                        String POLICY_NO = MapUtils.getString(map, A32.POLICY_NO.name()); // 保單號碼
                        
                        // 查詢結果[ID]等於要保人ID或被保人ID(目的是排除受款人ID)，且[保單號碼]歸戶(年金會有多筆給付明細，需歸戶為單張數量計次)
                        if ((StringUtils.equals(reqVO.getIDNo(), INSD_ID) || StringUtils.equals(reqVO.getIDNo(), APC_ID)) && !policyNoList.contains(POLICY_NO)) {
                            detail_filter.add(map);
                            policyNoList.add(POLICY_NO);
                        }
                    }
                    
                    // 未申請年金保單筆數
                    // if (detail.size() > 0) {
                    if (detail_filter.size() > 0) {
                        log.error("[checkAnnutiy][" + chatId + "] 查詢未申請年金保單筆數：" + detail_filter.size());
                        result = detail_filter.size();
                    }
                }
            }
        } catch (Exception e) {
            log.error("[checkAnnutiy][" + chatId + "] 檢查未申請年金保單錯誤:" + e.getMessage(), e);
        }
        
        return result;
    }
    
    private int checkMaturityDue(String chatId, BotLifeRequestVO reqVO) {
        
        int result = -1;
        try {
            if (StringUtils.isNotBlank(reqVO.getIDNo())) {
                List<Map<String, Object>> detail = cathayLifeTypeAService.a33(reqVO.getIDNo(), new String[] { "A", "I" }, "30", "B");
                result = 0;
                if (CollectionUtils.isNotEmpty(detail)) {
                    // 將到期滿期金保單筆數
                    log.error("[checkMaturityDue][" + chatId + "] 查詢將到期滿期金保單筆數：" + detail.size());
                    result = detail.size();
                }
            }
        } catch (Exception e) {
            log.error("[checkMaturityDue][" + chatId + "] 檢查將到期滿期金保單錯誤:" + e.getMessage(), e);
        }
        
        return result;
    }
    
    private int checkAnnutiyDue(String chatId, BotLifeRequestVO reqVO) {
        
        int result = -1;
        try {
            if (StringUtils.isNotBlank(reqVO.getIDNo())) {
                List<Map<String,Object>> detail = cathayLifeTypeAService.a33(reqVO.getIDNo(), new String[] { "A", "I" }, "30", "D");
                result = 0;
                if (CollectionUtils.isNotEmpty(detail)) {
                    // 將到期年金保單筆數
                    log.error("[checkAnnutiyDue][" + chatId + "] 查詢將到期年金保單筆數：" + detail.size());
                    result = detail.size();
                }
            }
        } catch (Exception e) {
            log.error("[checkAnnutiyDue][" + chatId + "] 檢查將到期年金保單錯誤:" + e.getMessage(), e);
        }
        
        return result;
    }

	private CardItem getBirthdayCard(BotLifeRequestVO reqVO, BotMessageVO vo, boolean isBirth) {

		CardItem cards1 = vo.new CardItem();

		try {
			// card1
			cards1.setCName("");
			cards1.setCWidth(BotMessageVO.CWIDTH_200);

			CImageDataItem cImageData1 = vo.new CImageDataItem();
			cImageData1.setCImage(BotMessageVO.IMG_CC);
			cImageData1.setCImageUrl("card02.png");
			cards1.setCImageData(cImageData1.getCImageDatas());

			cards1.setCTextType("7");
			// cards1.setCTitle("當月壽星獨享");
			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
			CTextItem cText0 = vo.new CTextItem();
            cText0.setCTextClass("0");
            cText0.setCLabel("當月壽星獨享");
            cTextList.add(cText0.getCTexts());
			CTextItem cText1 = vo.new CTextItem();
			cText1.setCTextClass("0");
			if (isBirth) {
			    cText1.setCText("各式各樣的優惠卷 　　　　");
			} else {
			    cText1.setCText("還沒輪到你喔敬請期待 　　");
			}
			cTextList.add(cText1.getCTexts());
			cards1.setCTexts(cTextList);

			if (isBirth) {
			    cards1.setCLinkType(1);
			    LifeLink[] link1s = { RightsLink.Birthday.快來下載 };
			    cards1.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link1s)));
			}
		} catch (Exception e) {
			log.error("[getBirthdayCard] 取得生日牌卡發生錯誤:" + e.getMessage());
		}

		return cards1;
	}
	
	private CardItem getTermPolicyCard(BotLifeRequestVO reqVO, BotMessageVO vo, int termPolicyCount) {
	    
	    CardItem cards1 = vo.new CardItem();
	    
	    try {
	        // card1
	        cards1.setCName("");
	        cards1.setCWidth(BotMessageVO.CWIDTH_200);
	        
	        CImageDataItem cImageData1 = vo.new CImageDataItem();
	        cImageData1.setCImage(BotMessageVO.IMG_CC);
	        cImageData1.setCImageUrl("card_termPolicy.png");
	        cards1.setCImageData(cImageData1.getCImageDatas());
	        
	        cards1.setCTextType("7");
	        // cards1.setCTitle("契約停效中");
	        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
	        CTextItem cText0 = vo.new CTextItem();
            cText0.setCTextClass("0");
            cText0.setCLabel("契約停效中");
            cTextList.add(cText0.getCTexts());
	        CTextItem cText1 = vo.new CTextItem();
	        cText1.setCTextClass("0");
	        if (termPolicyCount > 0) {
	            cText1.setCText("你為要保人契約停效日在兩年內的保單共<font color=\"#E65100\" size=\"4\"><b>" + termPolicyCount + "</b></font>張");
	        } else {
	            cText1.setCText("你沒有為要保人契約停效日在兩年內的保單喔");
	        }
	        cTextList.add(cText1.getCTexts());
	        cards1.setCTexts(cTextList);
	        
	        if (termPolicyCount > 0) {
	            cards1.setCLinkType(1);
	            LifeLink[] link1s = { RightsLink.TermPolicy.查看保單明細, RightsLink.申請復效辦理方式 };
	            cards1.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link1s)));
	        }
	    } catch (Exception e) {
	        log.error("[getTermPolicyCard] 取得契約停效發生錯誤:" + e.getMessage());
	    }
	    
	    return cards1;
	}
    
    private CardItem getMaturityCard(BotLifeRequestVO reqVO, BotMessageVO vo, int maturityCount) {
        
        CardItem cards1 = vo.new CardItem();
        
        try {
            // card1
            cards1.setCName("");
            cards1.setCWidth(BotMessageVO.CWIDTH_200);
            
            CImageDataItem cImageData1 = vo.new CImageDataItem();
            cImageData1.setCImage(BotMessageVO.IMG_CC);
            cImageData1.setCImageUrl("card_maturity.png");
            cards1.setCImageData(cImageData1.getCImageDatas());
            
            cards1.setCTextType("7");
            // cards1.setCTitle("滿期金未辦件");
            List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
            CTextItem cText0 = vo.new CTextItem();
            cText0.setCTextClass("0");
            cText0.setCLabel("滿期金未辦件");
            cTextList.add(cText0.getCTexts());
            CTextItem cText1 = vo.new CTextItem();
            cText1.setCTextClass("0");
            if (maturityCount > 0) {
                cText1.setCText("你為要保人或被保人還沒申請的保單共<font color=\"#E65100\" size=\"4\"><b>" + maturityCount + "</b></font>張");
            } else {
                cText1.setCText("你沒有為要保人或被保人的保單還沒有申請喔");
            }
            cTextList.add(cText1.getCTexts());
            cards1.setCTexts(cTextList);
            
            if (maturityCount > 0) {
                cards1.setCLinkType(1);
                LifeLink[] link1s = { RightsLink.Maturity.查看保單明細, RightsLink.Maturity.申請滿期金的方式 };
                cards1.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link1s)));
            }
        } catch (Exception e) {
            log.error("[getMaturityCard] 取得滿期金未辦件發生錯誤:" + e.getMessage());
        }
        
        return cards1;
    }
    
    private CardItem getAnnutiyCard(BotLifeRequestVO reqVO, BotMessageVO vo, int annutiyCount) {
        
        CardItem cards1 = vo.new CardItem();
        
        try {
            // card1
            cards1.setCName("");
            cards1.setCWidth(BotMessageVO.CWIDTH_200);
            
            CImageDataItem cImageData1 = vo.new CImageDataItem();
            cImageData1.setCImage(BotMessageVO.IMG_CC);
            cImageData1.setCImageUrl("card_annutiy.png");
            cards1.setCImageData(cImageData1.getCImageDatas());
            
            cards1.setCTextType("7");
            // cards1.setCTitle("年金未辦件");
            List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
            CTextItem cText0 = vo.new CTextItem();
            cText0.setCTextClass("0");
            cText0.setCLabel("年金未辦件");
            cTextList.add(cText0.getCTexts());
            CTextItem cText1 = vo.new CTextItem();
            cText1.setCTextClass("0");
            if (annutiyCount > 0) {
                cText1.setCText("你為要保人或被保人還沒申請的保單共<font color=\"#E65100\" size=\"4\"><b>" + annutiyCount + "</b></font>張");
            } else {
                cText1.setCText("你沒有為要保人或被保人的保單還沒有申請喔");
            }
            cTextList.add(cText1.getCTexts());
            cards1.setCTexts(cTextList);
            
            if (annutiyCount > 0) {
                cards1.setCLinkType(1);
                LifeLink[] link1s = { RightsLink.Annutiy.查看保單明細, RightsLink.Annutiy.申請年金的方式 };
                cards1.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link1s)));
            }
        } catch (Exception e) {
            log.error("[getAnnutiyCard] 取得年金未辦件發生錯誤:" + e.getMessage());
        }
        
        return cards1;
    }
    
    private CardItem getMaturityDueCard(BotLifeRequestVO reqVO, BotMessageVO vo, int maturityDueCount) {
        
        CardItem cards1 = vo.new CardItem();
        
        try {
            // card1
            cards1.setCName("");
            cards1.setCWidth(BotMessageVO.CWIDTH_200);
            
            CImageDataItem cImageData1 = vo.new CImageDataItem();
            cImageData1.setCImage(BotMessageVO.IMG_CC);
            cImageData1.setCImageUrl("card_maturityDue.png");
            cards1.setCImageData(cImageData1.getCImageDatas());
            
            cards1.setCTextType("7");
            List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
            CTextItem cText0 = vo.new CTextItem();
            cText0.setCTextClass("0");
            cText0.setCLabel("滿期金即將到期");
            cTextList.add(cText0.getCTexts());
            CTextItem cText1 = vo.new CTextItem();
            cText1.setCTextClass("0");
            if (maturityDueCount > 0) {
                cText1.setCText("你為要保人或被保人即將到期的保單共<font color=\"#E65100\" size=\"4\"><b>" + maturityDueCount + "</b></font>張");
            } else {
                cText1.setCText("你沒有為要保人或被保人的保單即將到期喔");
            }
            cTextList.add(cText1.getCTexts());
            cards1.setCTexts(cTextList);
            
            if (maturityDueCount > 0) {
                cards1.setCLinkType(1);
                LifeLink[] link1s = { RightsLink.MaturityDue.查看保單明細, RightsLink.MaturityDue.申請滿期金的方式 };
                cards1.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link1s)));
            }
        } catch (Exception e) {
            log.error("[getMaturityDueCard] 取得滿期金將到期發生錯誤:" + e.getMessage());
        }
        
        return cards1;
    }
    
    private CardItem getAnnutiyDueCard(BotLifeRequestVO reqVO, BotMessageVO vo, int annutiyDueCount) {
        
        CardItem cards1 = vo.new CardItem();
        
        try {
            // card1
            cards1.setCName("");
            cards1.setCWidth(BotMessageVO.CWIDTH_200);
            
            CImageDataItem cImageData1 = vo.new CImageDataItem();
            cImageData1.setCImage(BotMessageVO.IMG_CC);
            cImageData1.setCImageUrl("card_annutiyDue.png");
            cards1.setCImageData(cImageData1.getCImageDatas());
            
            cards1.setCTextType("7");
            List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
            CTextItem cText0 = vo.new CTextItem();
            cText0.setCTextClass("0");
            cText0.setCLabel("年金即將到期");
            cTextList.add(cText0.getCTexts());
            CTextItem cText1 = vo.new CTextItem();
            cText1.setCTextClass("0");
            if (annutiyDueCount > 0) {
                cText1.setCText("你為要保人或被保人即將到期的保單共<font color=\"#E65100\" size=\"4\"><b>" + annutiyDueCount + "</b></font>張");
            } else {
                cText1.setCText("你沒有為要保人或被保人的保單即將到期喔");
            }
            cTextList.add(cText1.getCTexts());
            cards1.setCTexts(cTextList);
            
            if (annutiyDueCount > 0) {
                cards1.setCLinkType(1);
                LifeLink[] link1s = { RightsLink.AnnutiyDue.查看保單明細, RightsLink.AnnutiyDue.申請年金的方式 };
                cards1.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link1s)));
            }
        } catch (Exception e) {
            log.error("[getAnnutiyDueCard] 取得年金將到期發生錯誤:" + e.getMessage());
        }
        
        return cards1;
    }

	private void sendMsg(String url, String chatID, String msg) {
		if (StringUtils.isBlank(url)) {
			return;
		}

		try {
			Map<String, Object> requestMap = new HashMap<String, Object>(); 
			requestMap.put("chatID", chatID);
			requestMap.put("msg", msg);
			
			// JSON方式呼叫start
			String requestJSON = JSONUtils.obj2json(requestMap);
			log.debug("requestJSON=" + requestJSON);

			RequestBody body = RequestBody.create(super.getJsonType(), requestJSON);

			OkHttpUtils.requestPost(url, super.getHeaders(), body);
		} catch (Exception e) {
			log.error(e.getMessage());
			log.debug("", e);
		}
	}
}
