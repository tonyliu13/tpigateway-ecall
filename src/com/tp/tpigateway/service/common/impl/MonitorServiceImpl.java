package com.tp.tpigateway.service.common.impl;

import com.tp.tpigateway.configuration.cache.CacheName;
import com.tp.tpigateway.model.common.CustSessionRec;
import com.tp.tpigateway.model.common.DailySession;
import com.tp.tpigateway.model.common.SessionStage;
import com.tp.tpigateway.model.common.mongo.Messagesessions;
import com.tp.tpigateway.model.common.mongo.Nodemessages;
import com.tp.tpigateway.model.common.monitor.ChatLogData;
import com.tp.tpigateway.model.common.monitor.MonitorData;
import com.tp.tpigateway.model.common.monitor.SessionLightData;
import com.tp.tpigateway.model.common.monitor.SessionStatusData;
import com.tp.tpigateway.repository.Mongo.MessagesessionsRepository;
import com.tp.tpigateway.repository.Mongo.NodemessagesRepository;
import com.tp.tpigateway.service.common.CallApiService;
import com.tp.tpigateway.service.common.CustSessionRecService;
import com.tp.tpigateway.service.common.MonitorService;
import com.tp.tpigateway.service.common.SessionStageService;
import com.tp.tpigateway.util.*;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service("monitorService")
public class MonitorServiceImpl implements MonitorService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MessagesessionsRepository messagesessionsRepository;

    @Autowired
    private NodemessagesRepository nodemessagesRepository;

    @Autowired
    private SessionStageService sessionStageService;

    @Autowired
    private CustSessionRecService custSessionRecService;

    @Autowired
    private CallApiService callApiService;

    private static final String TURN_TO_REAL_INTENT = "baodan.S051.turnToreal";

    private int sessionUpdMin = PropUtils.DEFAULT_SESSION_UPD_TIME;

    private static final String IDLE_STATUS_PREFIX = "idle.status";

    private static final String IDLE_STATUS_SCORE = PropUtils.IDLE_STATUS_SCORE;

    private static final String UPDATE_CACHE_URLS = PropUtils.getProperty("chat.status.other.urls");

    public int getSessionUpdMin() {
        return sessionUpdMin;
    }

    public void setSessionUpdMin(int sessionUpdMin) {
        this.sessionUpdMin = sessionUpdMin;
    }

    @Override
    public ChatLogData findChatLogBySessionId(String sessionId) {
        return findChatLogBySessionIdAndStartDateTime(sessionId, null);
    }

    @Override
    public ChatLogData findChatLogBySessionIdAndStartDateTime(String sessionId, Long startDateTime) {
        ChatLogData chatLogData = new ChatLogData();

        List<Nodemessages.Payload> chatLogs = new ArrayList<>();

        List<Nodemessages> nodeMessages = findNodemessagesBySessionIdAndStartDateTimeOrderByRegisteredDateTimeAsc(sessionId, startDateTime);

        if (EmptyUtil.isNotEmpty(nodeMessages)) {
            for (Nodemessages nodeMessage : nodeMessages) {
                Nodemessages.Info info = nodeMessage.getInfo();
                if (info != null) {
                    Nodemessages.Payload payload = null;
                    try {
                        payload = JSONUtils.obj2pojo(info.getPayload(), Nodemessages.Payload.class);
                    } catch (Exception e) {
                        //轉不了的就當成垃圾資料
                    }
                    if (payload != null) {
                        chatLogs.add(payload);
                        Date registeredDateTime = nodeMessage.getRegisteredDateTime();
                        if (registeredDateTime != null)
                            chatLogData.setLastChatDateTime(registeredDateTime.getTime());
                    }
                }
            }
        }

        chatLogData.setChatLog(chatLogs);

        return chatLogData;
    }

    private List<Nodemessages> findNodemessagesBySessionIdAndStartDateTimeOrderByRegisteredDateTimeAsc(String sessionId, Long startDateTime) {

    	// 20181211 by todd, 支援多組ChatFlow appid
    	List<String> appIds = new ArrayList<String>(Arrays.asList(StringUtils.splitByWholeSeparator(PropUtils.APP_ID, "$$")));
    	
        List<Messagesessions> messagesessions = messagesessionsRepository.findBySessionIdAndAppIdInOrderByStartDateAsc(sessionId, appIds);
    	/*List<Messagesessions> messagesessions = null;
    	if (appIds != null) {
    		for (String appId : appIds) {
    			messagesessions = messagesessionsRepository.findBySessionIdAndAppIdOrderByStartDateAsc(sessionId, appId);
    			if (CollectionUtils.isNotEmpty(messagesessions)) {
    				break;
    			}
    		}
		}*/
    	
        if (EmptyUtil.isEmpty(messagesessions)) {
            return null;
        }

        List<Nodemessages> nodeMessagesResult = new ArrayList<>();

        for (Messagesessions messagesession : messagesessions) {
            ObjectId _id = messagesession.getId();
            List<Nodemessages> nodeMessages = null;

            if (startDateTime != null) {
                nodeMessages = nodemessagesRepository.findBySessionAndRegisteredDateTimeGreaterThanEqualOrderByRegisteredDateTimeAsc(_id, new Date(startDateTime));
            } else {
                nodeMessages = nodemessagesRepository.findBySessionOrderByRegisteredDateTimeAsc(_id);
            }

            if (EmptyUtil.isNotEmpty(nodeMessages)) {
                nodeMessagesResult.addAll(nodeMessages);
            }
        }

        return nodeMessagesResult;
    }

    /*
      .1「對話unique ID」{session_id}
      .2「登入ID」{customerData.customer_id}
      .3「時間 」{對話起始時間}
      .4「管道」{channel}
      .5「是否申訴」{intent包含<baodan.S051.turnToreal>或對話紀錄中(bot回覆)有isAppeal=true}
      .6「該客戶曾詢問哪些intent」{對話紀錄中NLU命中到的intent清單}
     */
    @Override
    public List<MonitorData> findChatLogBySessionIdAndDateTime(String sessionId, long startDateTime, long endDateTime) {
        List<MonitorData> monitorDatas = new ArrayList<>();

        Date sDate = new Date(startDateTime);
        Date eDate = new Date(endDateTime);

        List<Messagesessions> messagesessions = null;

        List<String> appIds = new ArrayList<String>(Arrays.asList(StringUtils.splitByWholeSeparator(PropUtils.APP_ID, "$$")));
        
        if (StringUtils.isNotBlank(sessionId)) {
            messagesessions = messagesessionsRepository.findBySessionIdAndAppIdInAndStartDateBetweenOrderByStartDateAsc(sessionId, appIds, sDate, eDate);
        } else {
            messagesessions = messagesessionsRepository.findByAppIdInAndStartDateBetweenOrderByStartDateAsc(appIds, sDate, eDate);
        }

        Map<String, List<Nodemessages>> nodeMessagesMap = new HashMap<>();

        for (Messagesessions messagesession : messagesessions) {
            String id = messagesession.getSessionId();
            ObjectId _id = messagesession.getId();

            if (_id != null) {
                List<Nodemessages> nodemessages = nodemessagesRepository.findBySessionOrderByRegisteredDateTimeAsc(_id);

                if (EmptyUtil.isNotEmpty(nodemessages)) {
                    if (!nodeMessagesMap.containsKey(id)) {
                        nodeMessagesMap.put(id, nodemessages);
                    } else {
                        nodeMessagesMap.get(id).addAll(nodemessages);
                    }
                }
            }
        }

        nodeMessagesMap.forEach(
            (k,v)-> {
                MonitorData monitorData = new MonitorData();
                monitorData.setSessionId(k);
                handleChatLogData(monitorData, v);
                monitorDatas.add(monitorData);
            }
        );

        return monitorDatas;
    }

    @SuppressWarnings("unchecked")
	private void handleChatLogData(MonitorData monitorData, List<Nodemessages> nodemessages) {
        Nodemessages firstNode = nodemessages.get(0);
        if (EmptyUtil.isNotEmpty(nodemessages)) {
            monitorData.setStartDateTime(firstNode.getRegisteredDateTime().getTime());
        }

        List<String> intents = new ArrayList<>();
        boolean isAppeal = false;

        for (Nodemessages nodemessage : nodemessages) {
            Nodemessages.Info info = nodemessage.getInfo();
            if (info != null) {
                Nodemessages.Payload payload = null;
                try {
                    payload = JSONUtils.obj2pojo(info.getPayload(), Nodemessages.Payload.class);
                } catch (Exception e) {
                    //轉不了的就當成垃圾資料
                }

                if (payload != null) {
                	// 20181203 by todd, 由mongodb中獲取intent改由原始資料取得nlu json string
                    // Nodemessages.NLU nlu = payload.getNlu();
                    Nodemessages.CustomerData customerData = payload.getCustomerData();

                    // 20190306 by todd, 有值才填入回傳參數中，避免被空值覆蓋
                    // monitorData.setCustomerId(StringUtils.equalsIgnoreCase("NOID", payload.getCustomerId()) ? "" : payload.getCustomerId());
                    // monitorData.setChannel(payload.getChannel());
                    if (StringUtils.isBlank(monitorData.getCustomerId()) && StringUtils.isNotBlank(payload.getCustomerId()) && !StringUtils.equalsIgnoreCase("NOID", payload.getCustomerId())) {
                    	monitorData.setCustomerId(payload.getCustomerId());
                    }
                    if (StringUtils.isBlank(monitorData.getChannel()) && StringUtils.isNotBlank(payload.getChannel())) {
                    	monitorData.setChannel(payload.getChannel());
                    }

                    /*if (nlu != null) {
                        String intent = nlu.getIntent();
                        if (StringUtils.isNotBlank(intent) && !intents.contains(intent)) {
                            intents.add(nlu.getIntent());
                            if (isAppeal(intent)) {
                                isAppeal = true;
                            }
                        }
                    }*/
                    if (StringUtils.isNotBlank(payload.getIntent()) 
            				&& StringUtils.contains(payload.getIntent(), "{")
            				&& StringUtils.contains(payload.getIntent(), "}")) {
            			try {
            				Map<Object, Object> intentMap = new ObjectMapper().readValue(payload.getIntent(), Map.class);
            				if (MapUtils.isNotEmpty(intentMap)) {
            					Map<Object, Object> nluMap = (Map<Object, Object>) MapUtils.getMap(intentMap, "NLU");
            					if (MapUtils.isNotEmpty(nluMap)) {
            						String intent = MapUtils.getString(nluMap, "intent");
            						if (StringUtils.isNotBlank(intent) && !intents.contains(intent)) {
                                		intents.add(intent);
                                		if (isAppeal(intent)) {
                                			isAppeal = true;
                                		}
                                	}
            					}
            				}
            			} catch (Exception e) {
            				log.error("(" + payload.getSessionId() + ") [handleChatLogData] intent parser error:" + e.getMessage());
            			}
            		}
                    
                    if (customerData != null) {
                    		// if (StringUtils.isNotBlank(customerData.getSubChannel())) {
                    		if (StringUtils.isBlank(monitorData.getSubChannel()) && StringUtils.isNotBlank(customerData.getSubChannel())) {
                    			monitorData.setSubChannel(customerData.getSubChannel());
                    		}
                    }
                }
            }
        }

        monitorData.setIntent(intents);
        monitorData.setAppeal(isAppeal);
    }

    //「是否申訴」{intent包含<baodan.S051.turnToreal>或對話紀錄中(bot回覆)有isAppeal=true}
    private boolean isAppeal(String intent) {
    	// 20181203 by todd, 由mongodb中獲取intent改由原始資料取得nlu json string
        // return StringUtils.equals(TURN_TO_REAL_INTENT, intent);
        return StringUtils.contains(intent, TURN_TO_REAL_INTENT);
    }

    @Override
    public void saveDailySession(DailySession dailySession) {
        SessionStage sessionStage = dailySession.getSessionStage();
        CustSessionRec custSessionRec = dailySession.getCustSessionRec();

        try {
	        	SessionStage newSessionStage = null;
	        	CustSessionRec newCustSessionRec = null;
	        	
	        	if (sessionStage != null) {
	        		String sessionId = sessionStage.getSessionId();
	        		SessionStage oldSessionStage = sessionStageService.findSessionStageBySessionId(sessionId);
	        		if (oldSessionStage != null) {
	        			if (dailySession.getAddNoResTime() > 0) {
	        				int oldNoResTime = oldSessionStage.getNoResTime() == null ? 0 : oldSessionStage.getNoResTime();
	        				sessionStage.setNoResTime(oldNoResTime + dailySession.getAddNoResTime());
	        			}
	        			
	        			if (dailySession.getAddStageScore() > 0) {
	        				int oldStageScore = oldSessionStage.getStageScore() == null ? 0 : oldSessionStage.getStageScore();
	        				sessionStage.setStageScore(oldStageScore + dailySession.getAddStageScore());
	        			}
	        			
	        			newSessionStage = sessionStageService.updateSessionStage(sessionStage);
	        		} else {
	        			sessionStage.setNoResTime(dailySession.getAddNoResTime());
	        			sessionStage.setStageScore(dailySession.getAddStageScore());

	        			newSessionStage = sessionStageService.addSessionStage(sessionStage);
	        		}
	        	}
	        	
	        	if (custSessionRec != null) {
	        		CustSessionRec oldCustSessionRec = custSessionRecService.findByCustomerIdAndDate(custSessionRec.getCustomerId(), custSessionRec.getDate());
	        		if (oldCustSessionRec != null) {
	        			if (dailySession.getAddSessionTime() > 0) {
	        				int oldSessionTime = oldCustSessionRec.getSessionTime() == null ? 0 : oldCustSessionRec.getSessionTime();
	        				custSessionRec.setSessionTime(oldSessionTime + dailySession.getAddSessionTime());
	        			}
	        			
	        			newCustSessionRec = custSessionRecService.updateCustSessionRec(custSessionRec);
	        		} else {
	        			custSessionRec.setSessionTime(dailySession.getAddSessionTime());
	        			newCustSessionRec = custSessionRecService.addCustSessionRec(custSessionRec);
	        		}
	        	}
	        	
	        	//更新另一台tpigateway的cache
	        	DailySession dailySessionForCache = new DailySession(newSessionStage, newCustSessionRec);
	        	callApiService.updateCache(UPDATE_CACHE_URLS, dailySessionForCache);
        } catch (Exception e) {
        		log.error("[saveDailySession] " + e.getMessage());
        		try {
        			log.error("[saveDailySession] sessionStage=" + sessionStage == null ? "null" : (new JSONObject(sessionStage)).toString());
        			log.error("[saveDailySession] custSessionRec=" + custSessionRec == null ? "null" : (new JSONObject(custSessionRec)).toString());
        		} catch (Exception je) {
        			log.error("[saveDailySession] sessionStage or custSessionRec cannot trans to jsonString.");
        		}
        }
    }


    @Override
    public SessionStatusData findSessionStatusData(String agentId) {
        /*
            1. 符合isByPass的不處理
            2. 紅燈 > 黃燈 > 綠燈  同燈號下，(對話更新時間)舊的先取.
            3. 取出後該筆sessionId壓上agentId
        */
        Cache cache = sessionStageService.getSessionStageRecCache();
        ConcurrentMap cacheMap = getSessionStageCache(cache);

        SessionStatusData ssd = null;
        SessionStage ssResult = null;

        if (!MapUtils.isEmpty(cacheMap)) {
            //存放 k:燈號  v:對話更新時間最舊的那筆
            Map<SessionLightData.Light, SessionStage> dataMap = new HashMap<>();

            for (Object k : cacheMap.keySet()) {
                SessionStage sessionStage = cache.get(k, SessionStage.class);

                //清一下過時的cache
                clearSessionStageCache(cache, sessionStage);

                if (isByPass(sessionStage)) {
                    continue;
                }

                int score = countStageScore(sessionStage);
                SessionLightData.Light light = SessionLightData.getLight(score);

                SessionStage ss = dataMap.get(light);
                if (ss == null) {
                    dataMap.put(light, sessionStage);
                } else {
                    Date oldTime = ss.getSessionUpdTime();
                    Date nowTime = sessionStage.getSessionUpdTime();

                    //舊的才放
                    if (nowTime.getTime() < oldTime.getTime()) {
                        dataMap.put(light, sessionStage);
                    }
                }
            }

            //紅燈 > 黃燈 > 綠燈
            ssResult = dataMap.get(SessionLightData.Light.RED);
            if (ssResult == null) {
                ssResult = dataMap.get(SessionLightData.Light.YELLOW);
            }
            if (ssResult == null) {
                ssResult = dataMap.get(SessionLightData.Light.GREEN);
            }

        } else {
            int min = PropUtils.DEFAULT_SESSION_UPD_TIME;
            Date date = DateUtil.addMinute(new Date(), -min);

            List<SessionStage> sessionStages = sessionStageService.findBySessionUpdTimeGreaterThanEqualAndAgentIdIsNullOrderByStageScoreDescSessionUpdTimeAsc(date);

            for (SessionStage sessionStage : sessionStages) {

                if (isByPass(sessionStage)) {
                    continue;
                }

                ssResult = sessionStage;

                //取到第一個符合條件的就跳出
                break;
            }
        }

        if (ssResult != null) {
            ssd = handleSessionStatusData(ssResult);

            //取出後該筆sessionId壓上agentId
            ssResult.setAgentId(agentId);
            SessionStage newSessionStage = sessionStageService.updateSessionStage(ssResult);

            DailySession dailySessionForCache = new DailySession(newSessionStage, null);
            callApiService.updateCache(UPDATE_CACHE_URLS, dailySessionForCache);
        }

        return ssd;
    }

    private SessionStatusData handleSessionStatusData(SessionStage sessionStage) {
        SessionStatusData ssd = new SessionStatusData();

        if (sessionStage == null) {
            return null;
        }

        BeanUtils.copyProperties(sessionStage, ssd, DataUtils.getNullPropertyNames(sessionStage));

        if (StringUtils.isNotBlank(ssd.getCustomerId())) {
            String date = DateUtil.formatDate(new Date(), DateUtil.PATTERN_DATE);
            CustSessionRec csr = custSessionRecService.findByCustomerIdAndDate(ssd.getCustomerId(), date);
            if (csr != null) {
                BeanUtils.copyProperties(csr, ssd, DataUtils.getNullPropertyNames(csr));
            }
        }

        int score = countStageScore(sessionStage);
        SessionLightData.Light light = SessionLightData.getLight(score);

        ssd.setSessionLight(light.name());
        //這邊秀重新算過的分數
        ssd.setStageScore(score);

        return ssd;
    }

    private ConcurrentMap getSessionStageCache(Cache cache) {
        Object nativeCache = cache.getNativeCache();

        if (nativeCache instanceof ConcurrentMap) {
            return (ConcurrentMap) nativeCache;
        }
        return new ConcurrentHashMap();
    }

    @Override
    public SessionLightData countSessionLight(Integer sessionUpdMin) {
        if (sessionUpdMin != null && sessionUpdMin < getSessionUpdMin()) {
            setSessionUpdMin(sessionUpdMin);
        }

        Cache cache = sessionStageService.getSessionStageRecCache();
        ConcurrentMap cacheMap = getSessionStageCache(cache);

        SessionLightData sessionLightData = new SessionLightData();

        if (!MapUtils.isEmpty(cacheMap)) {

            for (Object k : cacheMap.keySet()) {
                SessionStage sessionStage = cache.get(k, SessionStage.class);

                //清一下過時的cache
                clearSessionStageCache(cache, sessionStage);

                //符合isByPass的不統計
                if (isByPass(sessionStage)) {
                    continue;
                }

                int score = countStageScore(sessionStage);

                //統計燈號
                sessionLightData.countLight(score);
            }
        } else {
            int min = PropUtils.DEFAULT_SESSION_UPD_TIME;
            Date date = DateUtil.addMinute(new Date(), -min);
            List<SessionStage> sessionStages = sessionStageService.findBySessionUpdTimeGreaterThanEqual(date);

            for (SessionStage sessionStage : sessionStages) {

                if (isByPass(sessionStage)) {
                    continue;
                }

                int score = countStageScore(sessionStage);

                //統計燈號
                sessionLightData.countLight(score);
            }
        }

        return sessionLightData;
    }

    private int countStageScore(SessionStage sessionStage) {
        Date sessionStrTime = sessionStage.getSessionStrTime();
        Date sessionUpdTime = sessionStage.getSessionUpdTime();

        long min = (sessionUpdTime.getTime() - sessionStrTime.getTime()) / (1000 * 60);

        int stageScore = sessionStage.getStageScore().intValue();

        /*
            EX:
               idle.status.score=50,80
               idle.status.50=10
               idle.status.80=20
        */
        String[] scoreArray = IDLE_STATUS_SCORE.split(",");

        for (int i = 0; i < scoreArray.length; i++) {
            try {
                int score = Integer.parseInt(scoreArray[i].trim());
                int idelMin = Integer.parseInt(PropUtils.getProperty(IDLE_STATUS_PREFIX + "." + score));
                if (min > idelMin) {
                    stageScore = stageScore + score;
                }
            } catch (Exception e) {
                //轉換成int有錯誤的話不處理
            }

        }

        return stageScore;
    }

    //清除閒置時間超過(default.session.upd.min)設定的分鐘數
    private void clearSessionStageCache(Cache cache, SessionStage sessionStage) {
        Date sessionUpdTime = sessionStage.getSessionUpdTime();
        if (isOverUpdTime(sessionUpdTime)) {
            cache.evict(sessionStage.getSessionId());
        }
    }

    //updTime距離現在時間超過DEFAULT_SESSION_UPD_TIME(如果user有傳過sessionUpdMin就用user傳的值)
    private boolean isOverUpdTime(Date sessionUpdTime, int userDefiendSessionUpdMin) {
        int defaultMin = PropUtils.DEFAULT_SESSION_UPD_TIME;

        //大於預設值就不採用
        if (defaultMin > userDefiendSessionUpdMin) {
            defaultMin = userDefiendSessionUpdMin;
        }

        return DateUtil.addMinute(new Date(), -defaultMin).getTime() > sessionUpdTime.getTime();
    }

    /*
        (1)有壓agentId(被取過件)
        (2)轉過真人
        (3)休假過
        (4)updTime距離現在時間超過DEFAULT_SESSION_UPD_TIME(如果user有傳過sessionUpdMin就用user傳的值)
        (5)斷線過
        (6)session_str_time < 今天
        (7)進入滿意度調查
     */
    private boolean isByPass(SessionStage sessionStage) {
        //user最後呼叫sessionLight api時傳入的sessionUpdMin值
        int userDefinedUpdTime = getSessionUpdMin();
        Date today = DateUtil.getToday();
        Date strDate = sessionStage.getSessionStrTime();

        boolean isLessToday = (strDate.getTime() - today.getTime() < 0);

        return isLessToday ||
                StringUtils.isNotBlank(sessionStage.getAgentId()) ||
                StringUtils.equalsIgnoreCase("Y", sessionStage.getChkTurnToReal()) ||
                StringUtils.equalsIgnoreCase("Y", sessionStage.getChkPassBot())    ||
                isOverUpdTime(sessionStage.getSessionUpdTime(), userDefinedUpdTime)    ||
                StringUtils.equalsIgnoreCase("Y", sessionStage.getChkDisconnect())    ||
                StringUtils.equalsIgnoreCase("Y", sessionStage.getChkInSurvey()) ;
    }

    private boolean isOverUpdTime(Date sessionUpdTime) {
        int min = PropUtils.DEFAULT_SESSION_UPD_TIME;
        return DateUtil.addMinute(new Date(), -min).getTime() > sessionUpdTime.getTime();
    }

    @Override
    public void updateCache(DailySession dailySession) {
        SessionStage sessionStage = dailySession.getSessionStage();
        CustSessionRec custSessionRec = dailySession.getCustSessionRec();

        if (sessionStage != null) {
            Cache sessionStageRecCache = sessionStageService.getSessionStageRecCache();
            sessionStageRecCache.put(sessionStage.getSessionId(), sessionStage);
        }

        if (custSessionRec != null) {
            Cache custStageRecCache = custSessionRecService.getCustSessionRecCache();
            String key = CacheName.genCustStageRecKey(custSessionRec.getCustomerId(), custSessionRec.getDate());
            custStageRecCache.put(key, custSessionRec);
        }
    }

    /**
     * 查詢SessionStage by sessionId(no cache)
     */
	@Override
	public SessionStage findSessionStageBySessionId(String sessionId) {
		return sessionStageService.findSessionStageBySessionId(sessionId);
	}

}
