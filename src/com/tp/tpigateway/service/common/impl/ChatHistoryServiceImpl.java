package com.tp.tpigateway.service.common.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.dao.common.ChatflowLogDao;
import com.tp.tpigateway.service.common.ChatHistoryService;

@Service("ChatHistoryService")
public class ChatHistoryServiceImpl implements ChatHistoryService {
	
	private static Logger log = LoggerFactory.getLogger(ChatHistoryServiceImpl.class);
	
	@ Autowired
	private ChatflowLogDao chatflowLogDao;
	
	@Override
	public List<Object> getMessageIntentList(String sessionId, String msgId) {
		return chatflowLogDao.getMessageIntentList(sessionId, msgId);
	}

	@Override
	public List<Object> getSessionMessageList(String sessionId) {
		return chatflowLogDao.getSessionMessageList(sessionId);
	}
	
	@Override
	public List<Map<String, String>> getMessagePathNodeList(String sessionId, String msgId) {
		return chatflowLogDao.getMessagePathNodeList(sessionId, msgId);
	}
	
}
