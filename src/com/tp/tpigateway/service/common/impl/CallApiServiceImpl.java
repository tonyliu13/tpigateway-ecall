package com.tp.tpigateway.service.common.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.BotTpiCache;
import com.tp.tpigateway.model.common.DailySession;
import com.tp.tpigateway.service.common.BaseService;
import com.tp.tpigateway.service.common.BotTpiCacheService;
import com.tp.tpigateway.service.common.CallApiService;
import com.tp.tpigateway.util.DateUtil;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

import okhttp3.RequestBody;

@Service("callApiService")
public class CallApiServiceImpl extends BaseService implements CallApiService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BotTpiCacheService botTpiCacheService;

    private static final String LOCAL_CACHE_URLS = PropUtils.getProperty("chat.cache.other.urls");
    private static final String CHAT_SENDDATA_URL = PropUtils.getProperty("chat.sendData.url");
    private static final String CHAT_GETDATA_URL = PropUtils.getProperty("chat.getData.url");
    private static final String CHAT_COMPLETEDDATA_URL = PropUtils.getProperty("chat.completedData.url");

    @Override
    @Async
    public void updateCache(String urls, DailySession dailySession) {
        if (StringUtils.isBlank(urls)) {
            return;
        }

        try {
            String[] urlArray = urls.split(",");
            for (String url : urlArray) {
                dailySession.setSystemID(PropUtils.SYSTEM_ID);
                // JSON方式呼叫start
                String requestJSON = JSONUtils.obj2json(dailySession);
                log.debug("requestJSON=" + requestJSON);

                RequestBody body = RequestBody.create(super.getJsonType(), requestJSON);

                OkHttpUtils.requestPost(url, super.getHeaders(), body);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            log.debug("", e);
        }
    }

    @Override
    public void localCache(String key, Object value, int expiredTime) {
        localCache(key, value, expiredTime, false);
    }
    
    @Override
    public void localCache(String key, Object value, int expiredTime, boolean tpiCache) {
    
        if (StringUtils.isBlank(LOCAL_CACHE_URLS)) {
            return;
        }

        String type = null;
        Object tmp = null;
        try {
            String[] urlArray = LOCAL_CACHE_URLS.split(",");

            if (value instanceof String) {
                type = "1";
                tmp = value;
            } else if (value instanceof Collection) {
                type = "2";
                tmp = new ObjectMapper().writeValueAsString(value);
            } else if (value instanceof Map) {
                type = "3";
                tmp = new ObjectMapper().writeValueAsString(value);
            }

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("key", key);
            map.put("value", tmp);
            map.put("expiredTime", expiredTime);
            map.put("type", type);

            if (tpiCache) {
                try {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(DateUtil.getNow());
                    calendar.add(Calendar.MILLISECOND, expiredTime);
                    Date expiredDateTime = calendar.getTime();
                    BotTpiCache botTpiCache = new BotTpiCache(key, tmp.toString(), type, expiredDateTime);
                    log.debug("botTpiCache=" + botTpiCache.toString());
                    botTpiCacheService.save(botTpiCache);
                } catch (Exception saveEx) {
                    log.error(saveEx.getMessage());
                }
            }

            try {
                // JSON方式呼叫start
                String requestJSON = new ObjectMapper().writeValueAsString(map);
                log.debug("requestJSON=" + requestJSON);

                for (String url : urlArray) {
                    RequestBody body = RequestBody.create(super.getJsonType(), requestJSON);
                    OkHttpUtils.requestPost(url, super.getHeaders(), body);
                }
            } catch (Exception callEx) {
                log.error(callEx.getMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            log.debug("", e);
        }
    }

    @Override
    public void removeCache(String key) {
        removeCache(key, false);
    }
    
    @Override
    public void removeCache(String key, boolean tpiCache) {
        if (StringUtils.isBlank(LOCAL_CACHE_URLS)) {
            return;
        }

        try {
            if (tpiCache) {
                try {
                    botTpiCacheService.delete(key);
                } catch (Exception saveEx) {
                    log.error(saveEx.getMessage());
                }
            }
            
            try {
                String[] urlArray = LOCAL_CACHE_URLS.split(",");
                
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("key", key);
                map.put("type", "-1");
                
                // JSON方式呼叫start
                String requestJSON = new ObjectMapper().writeValueAsString(map);
                log.debug("requestJSON=" + requestJSON);
                
                for (String url : urlArray) {
                    RequestBody body = RequestBody.create(super.getJsonType(), requestJSON);
                    OkHttpUtils.requestPost(url, super.getHeaders(), body);
                }
            } catch (Exception callEx) {
                log.error(callEx.getMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            log.debug("", e);
        }
    }

    @Override
    public Object getTpiCache(String key) {
        return getTpiCache(key, false);
    }

    @Override
    public Object getTpiCache(String key, boolean checkExpiredTime) {

        try {
            BotTpiCache botTpiCache = botTpiCacheService.findByCacheKey(key);
            if (botTpiCache != null) {
                if (checkExpiredTime && botTpiCache.getExpiredTime().before(DateUtil.getNow())) {
                    log.error("[getTpiCache] key=" + key + " expired.");
                    return null;
                }
                
                if ("1".equals(botTpiCache.getCacheType())) {
                    return botTpiCache.getCacheValue();
                } else if ("2".equals(botTpiCache.getCacheType())) {
                    return new ObjectMapper().readValue(botTpiCache.getCacheValue(), Collection.class);
                } else if ("3".equals(botTpiCache.getCacheType())) {
                    return new ObjectMapper().readValue(botTpiCache.getCacheValue(), Map.class);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return null;
    }

    @Override
    @Async
    public boolean sendDataToQueueWithAsync(String chatID, String key, String value) {
        return sendDataToQueue(chatID, key, value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean sendDataToQueue(String chatID, String key, String value) {

        boolean result = false;
        if (StringUtils.isBlank(key)) {
            return result;
        }

        try {
            Map<String, Object> requestMap = new HashMap<String, Object>();
            requestMap.put("chatID", chatID);
            requestMap.put("key", key);
            requestMap.put("value", new String((new Base64()).encode(value.getBytes("UTF-8")), "UTF-8"));

            // JSON方式呼叫start
            String requestJSON = JSONUtils.obj2json(requestMap);
            log.debug("requestJSON=" + requestJSON);

            RequestBody body = RequestBody.create(super.getJsonType(), requestJSON);

            Map<String, Object> sendResult = OkHttpUtils.requestPost(CHAT_SENDDATA_URL, super.getHeaders(), body);

            String bodyResult = OkHttpUtils.getBody(sendResult);

            if (StringUtils.isNotBlank(bodyResult)) {
                Map<String, Object> mapResult = new ObjectMapper().readValue(bodyResult, HashMap.class);
                String rtnCode = MapUtils.getString(mapResult, "rtnCode");
                String rtnMsg = MapUtils.getString(mapResult, "rtnMsg");
                if ("1".equals(rtnCode)) {
                    result = true;
                } else {
                    log.error("(" + chatID + ") sendDataToQueue發生錯誤，key=" + key + "，value=" + value + "，error=" + rtnMsg);
                }
            }
        } catch (Exception e) {
            log.error("(" + chatID + ") sendDataToQueue發生錯誤，key=" + key + "，value=" + value + "，error=" + e.getMessage());
            log.debug("", e);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getDataFromQueue(String chatID, String key) {

        String result = null;
        if (StringUtils.isBlank(key)) {
            return result;
        }

        try {
            Map<String, Object> requestMap = new HashMap<String, Object>();
            requestMap.put("chatID", chatID);
            requestMap.put("key", key);
            requestMap.put("completed", false);

            // JSON方式呼叫start
            String requestJSON = JSONUtils.obj2json(requestMap);
            log.debug("requestJSON=" + requestJSON);

            RequestBody body = RequestBody.create(super.getJsonType(), requestJSON);

            Map<String, Object> sendResult = OkHttpUtils.requestPost(CHAT_GETDATA_URL, super.getHeaders(), body);

            String bodyResult = OkHttpUtils.getBody(sendResult);

            if (StringUtils.isNotBlank(bodyResult)) {
                Map<String, Object> mapResult = new ObjectMapper().readValue(bodyResult, HashMap.class);
                String rtnCode = MapUtils.getString(mapResult, "rtnCode");
                String rtnMsg = MapUtils.getString(mapResult, "rtnMsg");
                String value = MapUtils.getString(mapResult, "value");
                if ("1".equals(rtnCode)) {
                    result = new String((new Base64()).decode(value.getBytes("UTF-8")), "UTF-8");
                } else {
                    log.error("(" + chatID + ") getDataFromQueue發生錯誤，key=" + key + "，error=" + rtnMsg);
                }
            }
        } catch (Exception e) {
            log.error("(" + chatID + ") getDataFromQueue發生錯誤，key=" + key + "，error=" + e.getMessage());
            log.debug("", e);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean completedDataToQueue(String chatID, String key) {

        boolean result = false;
        if (StringUtils.isBlank(key)) {
            return result;
        }

        try {
            Map<String, Object> requestMap = new HashMap<String, Object>();
            requestMap.put("chatID", chatID);
            requestMap.put("key", key);

            // JSON方式呼叫start
            String requestJSON = JSONUtils.obj2json(requestMap);
            log.debug("requestJSON=" + requestJSON);

            RequestBody body = RequestBody.create(super.getJsonType(), requestJSON);

            Map<String, Object> sendResult = OkHttpUtils.requestPost(CHAT_COMPLETEDDATA_URL, super.getHeaders(), body);

            String bodyResult = OkHttpUtils.getBody(sendResult);

            if (StringUtils.isNotBlank(bodyResult)) {
                Map<String, Object> mapResult = new ObjectMapper().readValue(bodyResult, HashMap.class);
                String rtnCode = MapUtils.getString(mapResult, "rtnCode");
                String rtnMsg = MapUtils.getString(mapResult, "rtnMsg");
                if ("1".equals(rtnCode)) {
                    result = true;
                } else {
                    log.error("(" + chatID + ") completedDataToQueue發生錯誤，key=" + key + "，error=" + rtnMsg);
                }
            }
        } catch (Exception e) {
            log.error("(" + chatID + ") completedDataToQueue發生錯誤，key=" + key + "，error=" + e.getMessage());
            log.debug("", e);
        }

        return result;
    }

}
