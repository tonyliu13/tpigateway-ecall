package com.tp.tpigateway.service.common.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CImageDataItem;
import com.tp.tpigateway.model.common.BotMessageVO.CImageTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CLinkListItem;
import com.tp.tpigateway.model.common.BotMessageVO.CTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;
import com.tp.tpigateway.model.common.BotMessageVO.ContentItem;
import com.tp.tpigateway.model.common.BotMessageVO.LinkListItem;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.enumeration.D01;
import com.tp.tpigateway.model.common.enumeration.D02;
import com.tp.tpigateway.model.common.enumeration.E02;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;
import com.tp.tpigateway.service.common.CathayLifeBotMessageService;
import com.tp.tpigateway.service.common.impl.CathayLifeBotMessageServiceImpl.Card.CarouselCard;
import com.tp.tpigateway.util.DataUtils;


@Service("lifeBotMessageService")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class CathayLifeBotMessageServiceImpl implements CathayLifeBotMessageService {

    private static Logger log = LoggerFactory.getLogger(CathayLifeBotMessageServiceImpl.class);

    @Autowired
    protected BotTpiReplyTextService botTpiReplyTextService;
    
    protected static enum Widget { //for type=12用
    	doLogin, doBankCodeAutoComplete, doNotice, doGeolocation
    }
    
    @Override
    public BotMessageVO initBotMessage(String channel, String source, String role) {
        BotMessageVO vo = new BotMessageVO();
        vo.setChannel(channel);
        vo.setSource(source);
        vo.setRole(role);
        vo.setContent(new ArrayList<Map<String, Object>>());
        return vo;
    }

    @Override
    public void setTextResult(BotMessageVO vo, String text) {
        ContentItem contentH = this.getTextContentItem(vo, text);
        vo.getContent().add(contentH.getContent());
    }

    @Override
    public void setNoteResult(BotMessageVO vo, String annoTitle, String annoText) {
        ContentItem contentH = this.getNoteContentItem(vo, annoTitle, annoText);
        vo.getContent().add(contentH.getContent());
    }

    @Override
    public void setHNoteResult(BotMessageVO vo, String annoText) {
        ContentItem contentH = getHNoteContentItem(vo, annoText);
        vo.getContent().add(contentH.getContent());
    }

    @Override
    public void setHNoteResultWithReplace(BotMessageVO vo, String annoText, Map<String, String> replaceParams) {
        ContentItem contentH = this.getHNoteContentItemWithReplace(vo, annoText, replaceParams);
        vo.getContent().add(contentH.getContent());
    }

    @Override
    public ContentItem getHNoteContentItem(BotMessageVO vo, String annoText) {
        ContentItem contentH = vo.new ContentItem();
        contentH.setType(13);
        contentH.setAnnoText(annoText);
        return contentH;
    }

    @Override
    public ContentItem getHNoteContentItemWithReplace(BotMessageVO vo, String annoText, Map<String, String> replaceParams) {
        return getHNoteContentItem(vo, replaceTextByParamMap(annoText, replaceParams));
    }

    @Override
    public void setNoteResultWithReplace(BotMessageVO vo, String annoTitle, String annoText,
                                         Map<String, String> replaceParams) {
        ContentItem contentH = this.getNoteContentItemWithReplace(vo, annoTitle, annoText, replaceParams);
        vo.getContent().add(contentH.getContent());
    }

    @Override
    public void setTextResultWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap) {
        ContentItem contentH = this.getTextContentItemWithReplace(vo, text, paramMap);
        vo.getContent().add(contentH.getContent());
    }

    @Override
    public void setCardsResult(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
        ContentItem content = vo.new ContentItem();
        content.setType(2);
        content.setCType(ctype);
        content.setCards(cardList);
        vo.getContent().add(content.getContent());
    }

    @Override
    public void setPolicyFilterResult(BotMessageVO vo, List<Map<String, Object>> dataList) {
        ContentItem content = vo.new ContentItem();
        content.setType(7);
        content.setDataList(dataList);
        vo.getContent().add(content.getContent());
    }
    
    @Override
    public void setListViewResult(BotMessageVO vo, String lvType, List<Map<String, Object>> dataList) {
        ContentItem content = vo.new ContentItem();
        boolean isMultiple = false;
        content.setType(14);
        content.setLVType(lvType);
        if (StringUtils.equals("1", lvType)) {
        	isMultiple = true;
        	content.setLVTitle("點選欲終止的附約(可複選)");
        	content.setText("我要終止<%RD_NAME_LIST%>");//我要終止([特約種類])[特約種類名稱]附約、([特約種類])[特約種類名稱]附約
        }
        else if (StringUtils.equals("2", lvType)) {
            isMultiple = false;
            content.setLVTitle("點選欲縮小的附約");
            content.setText("我要縮小<%RD_NAME_LIST%>附約");//我要縮小([特約種類])[特約種類名稱]附約
        }
        else if (StringUtils.equals("3", lvType)) {
            isMultiple = true;
            content.setLVTitle("你為被保險人所擁有的保障項目");
        }
        else if (StringUtils.equals("4", lvType)) {
        	content.setLVType("3");
            isMultiple = true;
            content.setLVTitle("這張保單所擁有的保障項目");
        }
        content.setMultiple(isMultiple);
        content.setLVDataList(dataList);
        vo.getContent().add(content.getContent());
    }

    @Override
    public ContentItem getTextContentItem(BotMessageVO vo, String text) {
        ContentItem contentH = vo.new ContentItem();
        contentH.setType(1);
        contentH.setText(text);
        return contentH;
    }

    @Override
    public ContentItem getNoteContentItem(BotMessageVO vo, String annoTitle, String annoText) {
        ContentItem contentH = vo.new ContentItem();
        contentH.setType(11);
        contentH.setAnnoTitle(annoTitle);
        contentH.setAnnoText(annoText);
        return contentH;
    }

    @Override
    public ContentItem getNoteContentItemWithReplace(BotMessageVO vo, String annoTitle, String annoText, Map<String, String> paramMap) {
        return getNoteContentItem(vo, annoTitle, replaceTextByParamMap(annoText, paramMap));
    }

    public ContentItem getImageContentItem(BotMessageVO vo, String imageUrl) {
        ContentItem contentH = vo.new ContentItem();
        contentH.setType(5);
        contentH.setAction(6);
        contentH.setImageUrl(imageUrl);
        return contentH;
    }

    @Override
    public void setImageResult(BotMessageVO vo, String imageUrl) {
        ContentItem contentH = this.getImageContentItem(vo, imageUrl);
        vo.getContent().add(contentH.getContent());
    }

    
    @Override
    public void setGeolocationResult(BotMessageVO vo) {
    	ContentItem contentH = vo.new ContentItem();
	 	contentH.setType(12);
	 	contentH.setWidget(Widget.doGeolocation.name());
        vo.getContent().add(contentH.getContent());
    }
    
    @Override
    public void setLifeLinkListResult(BotMessageVO vo, List<LifeLink> links) {
        ContentItem contentLinkListH = vo.new ContentItem();
        contentLinkListH.setType(4);

        int[] wordLen = new int[links.size()];

        List<Map<String, Object>> linkLists = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < links.size(); i++) {
            LifeLink link = links.get(i);
            LinkListItem linkItem = this.getLinkListItem(vo, link.getAction(), link.getText(), link.getAlt(), link.getReply(), link.getUrl(), link.getCustomerAction());
            linkLists.add(linkItem.getLinkList());

            wordLen[i] = getWordLen(link.getText());
        }

        String lType = "";
        int len = links.size();
        if (wordLen.length == 2)
            lType = (wordLen[0] <= 9 && wordLen[1] <= 9) ? BotMessageVO.LTYPE_01 : BotMessageVO.LTYPE_04;
        else if (len == 3)
            lType = (wordLen[0] <= 5 && wordLen[1] <= 5 && wordLen[2] <= 5) ? BotMessageVO.LTYPE_02 : BotMessageVO.LTYPE_04;
        else if (len == 4)
            lType = (wordLen[0] <= 9 && wordLen[1] <= 9 && wordLen[2] <= 9 && wordLen[3] <= 9) ? BotMessageVO.LTYPE_03 : BotMessageVO.LTYPE_04;
        else
            lType = BotMessageVO.LTYPE_04;

        contentLinkListH.setLType(lType);
        contentLinkListH.setLinkList(linkLists);

        vo.getContent().add(contentLinkListH.getContent());
    }
    
    @Override
    public void setLifeLinkListResult(BotMessageVO vo, String linkType, List<LifeLink> links) {
        ContentItem contentLinkListH = vo.new ContentItem();
        contentLinkListH.setType(4);

        List<Map<String, Object>> linkLists = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < links.size(); i++) {
            LifeLink link = links.get(i);
            LinkListItem linkItem = this.getLinkListItem(vo, link.getAction(), link.getText(), link.getAlt(), link.getReply(), link.getUrl(), link.getCustomerAction());
            linkLists.add(linkItem.getLinkList());
        }

        contentLinkListH.setLType(linkType);
        contentLinkListH.setLinkList(linkLists);

        vo.getContent().add(contentLinkListH.getContent());
    }

    protected int getWordLen(String str) {
        int chinese = 0;
        int english = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if ((int) c < 256)
                english++;
            else
                chinese++;
        }
        int engLen = english / 2;

        return chinese * 1 + (english % 2 == 0 ? engLen : engLen + 1);
    }

    @Override
    public String replaceTextByParamMap(String text, Map<String, String> paramMap) {

    	if (StringUtils.isBlank(text) || !StringUtils.contains(text, "<%") || !StringUtils.contains(text, "%>")) return text;
        StringBuffer sbf = new StringBuffer();
        String[] col_ids = text.split("<%|%>");
        for (String col_id : col_ids) {
            if (col_id.length() > 0 && paramMap.get(col_id) != null) {
                sbf.append(MapUtils.getString(paramMap, col_id));
            } else {
                if (text.indexOf("<%" + col_id + "%>") != -1)
                    sbf.append("<%" + col_id + "%>");
                else
                    sbf.append(col_id);
            }
        }
        return sbf.toString();
    }

    @Override
    public ContentItem getTextContentItemWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap) {
        return getTextContentItem(vo, replaceTextByParamMap(text, paramMap));
    }

    @Override
    public LinkListItem getLinkListItem(BotMessageVO vo, Integer action, String text, String alt, String reply, String url, String customerAction) {
        LinkListItem linkList = vo.new LinkListItem();
        linkList.setLAction(action);
        linkList.setLText(text);
        linkList.setLAlt(alt);
        linkList.setLReply(reply);
        linkList.setLUrl(url);
        linkList.setLCustomerAction(customerAction);
        return linkList;
    }

    @Override
    public BotMessageVO getSimpleBotMessageVO(String channel, String source, String role, String text) {
        BotMessageVO botMsgVO = initBotMessage(channel, source, role);
        setTextResult(botMsgVO, text);
        return botMsgVO;
    }

    protected List<Map<String, Object>> getCommonCards1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, LifeLink[] links) {
		/*
			[險別中文名稱]
			[保單號碼]
			[下次應繳日]
			
			link:看繳費管道
		*/
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		cards.setCTextType("1");
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("下次應繳保費");
		cText2.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cards.setCTexts(cTextList);
		
		if (links != null && links.length > 0) {
			cards.setCLinkType(1);
			List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
			cards.setCLinkList(cLinkList);
		}
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}
	
    protected Map<String, Object> getCommonCards2(BotMessageVO vo, Map<String, Object> d02Result ,Map<String,Object> e02Result, LifeLink[] links) {
		/*
		 	應繳日：[應繳日]
			繳次：[繳次] 
			繳費日期：[繳費日期]
			繳費金額：[繳費金額] 
			繳費管道：[繳費管道中文]
			授權人姓名：[授權人姓名]
			扣款銀行：[行庫名稱]
			帳號末四碼：[銀行帳號]末四碼
		*/
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		cImageData.setCImageTag(MapUtils.getString(d02Result, D02.PAY_TIMES.name()) + "期");
	
		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);
	
		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("應繳日");
		cText1.setCText(MapUtils.getString(d02Result, D02.PAY_DATE_POL.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("繳費日期");
		cText3.setCText(MapUtils.getString(d02Result, D02.PAY_DATE_CLC.name()));
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("繳費金額");
		cText4.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + "\t" + DataUtils.formatDollar(MapUtils.getString(d02Result, D02.TOT_PREM.name())));
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("繳費管道");
		cText5.setCText(MapUtils.getString(d02Result, D02.PAY_WAY_CODE.name()));
		CTextItem cText8 = vo.new CTextItem();
		cText8.setCLabel("帳號末四碼"); // 扣款銀行資訊+帳號末四碼(ex.郵局6899)
		if (MapUtils.getString(d02Result, D02.BANK_NAME_TRN.name()) !=null && MapUtils.getString(d02Result, D02.ACNT_NO_TRN.name()) != null) {
			cText8.setCText(MapUtils.getString(d02Result, D02.BANK_NAME_TRN.name()) + MapUtils.getString(d02Result, D02.ACNT_NO_TRN.name()));
		} else {
			cText8.setCText("--");
		}		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		cTextList.add(cText8.getCTexts());
		
		cards.setCTexts(cTextList);
		
		if (links != null && links.length > 0) {
			cards.setCLinkType(1);
			List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
			cards.setCLinkList(cLinkList);
		}
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		return cards.getCards();
	}
	
    protected Map<String, Object> getCommonCards3(BotMessageVO vo, Map<String, Object> d02Result ,Map<String,Object> e02Result, LifeLink[] links) {
		/*
		 	應繳日：[應繳日]
			繳次：[繳次] 
			繳費日期：[繳費日期]
			繳費金額：[繳費金額] 
			繳費管道：[繳費管道中文]
			授權人姓名：[授權人姓名]
			發卡銀行：[發卡銀行]
			信用卡號末四碼：[信用卡號]末四碼
		 */
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		cImageData.setCImageTag(MapUtils.getString(d02Result, D02.PAY_TIMES.name()) + "期");
	
		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);
	
		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("應繳日");
		cText1.setCText(MapUtils.getString(d02Result, D02.PAY_DATE_POL.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("繳費日期");
		cText3.setCText(MapUtils.getString(d02Result, D02.PAY_DATE_CLC.name()));
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("繳費金額");
		cText4.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + "\t" + DataUtils.formatDollar(MapUtils.getString(d02Result, D02.TOT_PREM.name())));
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("繳費管道");
		cText5.setCText(MapUtils.getString(d02Result, D02.PAY_WAY_CODE.name()));
		CTextItem cText8 = vo.new CTextItem();
		cText8.setCLabel("卡號末四碼");
		if (MapUtils.getString(d02Result, D02.BANK_NAME_CARD.name()) != null && MapUtils.getString(d02Result, D02.CARD_NO.name()) != null) {
			cText8.setCText(MapUtils.getString(d02Result, D02.BANK_NAME_CARD.name()) + MapUtils.getString(d02Result, D02.CARD_NO.name()));
		} else {
			cText8.setCText("--");
		}		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		cTextList.add(cText8.getCTexts());
		
		cards.setCTexts(cTextList);
	
		if (links != null && links.length > 0) {
			cards.setCLinkType(1);
			List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
			cards.setCLinkList(cLinkList);
		}
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		return cards.getCards();
	}
	
    protected Map<String, Object> getCommonCards4(BotMessageVO vo, Map<String, Object> d02Result ,Map<String,Object> e02Result, LifeLink[] links) {
		/*
		 	保單名稱 : [保單名稱]
		 	保單號碼 : [保單號碼]
		 	應繳日：[應繳日]
			繳次：[繳次] 
			繳費日期：[繳費日期]
			繳費金額：[繳費金額] 
			繳費管道：[繳費管道中文]
			
			link:查附約繳費金額
		 */
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		cImageData.setCImageTag(MapUtils.getString(d02Result, D02.PAY_TIMES.name()) + "期");
	
		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());		
		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());
	
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("應繳日");
		cText1.setCText(MapUtils.getString(d02Result, D02.PAY_DATE_POL.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("繳費日期");
		cText3.setCText(MapUtils.getString(d02Result, D02.PAY_DATE_CLC.name()));
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("繳費金額");
		cText4.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + "\t" + DataUtils.formatDollar(MapUtils.getString(d02Result, D02.TOT_PREM.name())));
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("繳費管道");
		cText5.setCText(MapUtils.getString(d02Result, D02.PAY_WAY_CODE.name()));
		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		
		cards.setCTexts(cTextList);
	
		if (links != null && links.length > 0) {
			cards.setCLinkType(1);
			List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
			cards.setCLinkList(cLinkList);
		}
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		return cards.getCards();
	}
	
    protected List<Map<String, Object>> getCommonCards5(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result, LifeLink[] links) {
		/*
			[險別中文名稱]
			[保單號碼]
			[下次應繳日]
			[最近扣款日]
			
			link:看繳費管道
		*/
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		cards.setCTextType("1");
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("下次應繳保費");
		cText2.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("最近扣款日");
		cText3.setCText(MapUtils.getString(d01Result, D01.DCT_BANK_DATE.name()));
		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cards.setCTexts(cTextList);
		
		if (links != null && links.length > 0) {
			cards.setCLinkType(1);
			List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
			cards.setCLinkList(cLinkList);
		}
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}
	
	@Override
	public List<Map<String, Object>> getCLinkList(BotMessageVO vo, List<LifeLink> links) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
		for (LifeLink link : links)
			cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());
		return cLinkContentList;
	}
	
	protected  List<Map<String, Object>> getCLinkList(BotMessageVO vo, LifeLink[] lifeLinks) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();

		for (LifeLink lifeLink : lifeLinks) {
			cLinkContentList.add(getCLinkListItem(vo, lifeLink).getCLinkList());
		}
		return cLinkContentList;
	}	

    @Override
    public List<Map<String, Object>> getCLinkList(BotMessageVO vo, LifeLink link) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
		cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());
		return cLinkContentList;
	}
	
    protected CLinkListItem getCLinkListItem(BotMessageVO vo, LifeLink link) {
		CLinkListItem cLinkContent = vo.new CLinkListItem();
		cLinkContent.setClAction(link.getAction());
		cLinkContent.setClText(link.getText());
		cLinkContent.setClAlt(link.getAlt());
		cLinkContent.setClReply(link.getReply());
		cLinkContent.setClUrl(link.getUrl());
		cLinkContent.setClCustomerAction(link.getCustomerAction());
		return cLinkContent;
	}

	@Override
	public Map<String, Object> getCardForDownloadPDF(BotMessageVO vo, String imageText, List<LifeLink> links) {
		// 操作流程圖PDF
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB06);
		cImageData.setCImageTextType(3);

		List<Map<String, Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageText(imageText);
		cImageTextList.add(cImageText.getCImageTexts());
		cImageData.setCImageTexts(cImageTextList);

		cards.setCImageData(cImageData.getCImageDatas());

		// link
		cards.setCLinkType(1);
		List<Map<String, Object>> cLinkList = this.getCLinkList(vo, links);
		cards.setCLinkList(cLinkList);

		log.info("cards=" + new JSONObject(cards).toString());

		return cards.getCards();
	}
	
	@Override
	public Map<String, Object> getCardForDownloadEDM(BotMessageVO vo, String imageText, List<LifeLink> links) {
	    // EDM
	    CardItem cards = vo.new CardItem();
	    cards.setCWidth(BotMessageVO.CWIDTH_250);
	    
	    CImageDataItem cImageData = vo.new CImageDataItem();
	    cImageData.setCImage(BotMessageVO.IMG_EDM);
	    cImageData.setCImageTextType(3);
	    
	    List<Map<String, Object>> cImageTextList = new ArrayList<>();
	    CImageTextItem cImageText = vo.new CImageTextItem();
	    cImageText.setCImageText(imageText);
	    cImageTextList.add(cImageText.getCImageTexts());
	    cImageData.setCImageTexts(cImageTextList);
	    
	    cards.setCImageData(cImageData.getCImageDatas());
	    
	    // link
	    cards.setCLinkType(1);
	    List<Map<String, Object>> cLinkList = this.getCLinkList(vo, links);
	    cards.setCLinkList(cLinkList);
	    
	    log.info("cards=" + new JSONObject(cards).toString());
	    
	    return cards.getCards();
	}

	/**
	 * DB查詢回覆訊息並覆蓋參數
	 * 
	 * @param Role
	 * @param NodeID
	 * @param textId
	 * @param paramMap
	 * @return
	 */
	public String getReplaceTextByParamMap(String Role, String NodeID, String textId, Map paramMap) {
		String returnText = botTpiReplyTextService.getReplyText(Role, NodeID, textId);
		returnText = replaceTextByParamMap(returnText, paramMap);
		return returnText;
	}

	/**
	 * Quick Reply
	 * 
	 * @param botMessageService
	 * @param vo
	 * @param lifeLinks
	 * 
	 */
	@Override
	public void setQuickReply(BotMessageVO vo, LifeLink... lifeLinks) {
		this.setLifeLinkListResult(vo, Arrays.asList(lifeLinks));
	}

	/**
	 * 貼圖
	 * 
	 * @param botMessageService
	 * @param vo
	 * @param imageName
	 */
	@Override
	public void setSticker(BotMessageVO vo, String imageName) {
		this.setImageResult(vo, imageName);
	}
	
	/**
	 * 注意事項(使用預設標題)
	 * 
	 * @param botMessageService
	 * @param botTpiReplyTextService
	 * @param vo
	 * @param role
	 * @param textId
	 */
	@Override
	public void setNote(BotMessageVO vo, String role, String textId) {
		setNote(vo, role, null, textId);
	}	
	
	/**
	 * 注意事項
	 * 
	 * @param botMessageService
	 * @param botTpiReplyTextService
	 * @param vo
	 * @param role
	 * @param title
	 * @param textId
	 */
	@Override	
	public void setNote(BotMessageVO vo, String role, String title, String textId) {
		String replyText = botTpiReplyTextService.getReplyText(role, textId);
		this.setNoteResult(vo, title == null ? "注意事項" : title, replyText);
	}	
	
	@Override	
	public void setPDFCard(BotMessageVO vo, LifeLink link, String imageText) {
		Map<String, Object> pdfCard = this.createPDFCard(vo, link, imageText);
		setPDFCards(vo,  pdfCard);
	}
	
	//下載PDF牌卡
	protected Map<String, Object> createPDFCard(BotMessageVO vo, LifeLink link, String imageText) {
		//操作流程圖PDF
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB06);
		//
		cImageData.setCImageTextType(3);
		
		List<Map<String,Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageText(imageText);
		cImageTextList.add(cImageText.getCImageTexts());
		cImageData.setCImageTexts(cImageTextList);
		
		cards.setCImageData(cImageData.getCImageDatas());
		
		//link
		cards.setCLinkType(1);
		List<Map<String, Object>> cLinkList = getCLinkList(vo, link);
		cards.setCLinkList(cLinkList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		return cards.getCards();
	}	
	/**
	 * Carousel Card
	 * 
	 * @param botMessageService
	 * @param vo
	 * @param cardList
	 * @param title
	 * @param texts
	 * @param lifeLinks
	 * @return
	 */
	protected void setPDFCards(BotMessageVO vo,
			Map<String, Object>... carouselCards) {
		setPDFCards(vo, null, carouselCards);
	}	
	
	/**
	 * Carousel Card
	 * 
	 * @param botMessageService
	 * @param vo
	 * @param cardList
	 * @param title
	 * @param texts
	 * @param lifeLinks
	 * @return
	 */
	protected void setPDFCards(BotMessageVO vo,
			List<Map<String, Object>> cardList, Map<String, Object>... pdfCards) {

		boolean onlyOneData = cardList == null;

		if (onlyOneData) {
			cardList = new ArrayList<Map<String, Object>>();
		}
		for(Map<String, Object> pdfCard: pdfCards) {
			cardList.add(pdfCard);	
		}

		this.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
	}	
	
	/**
	 * 純文字
	 * 
	 * @param botMessageService
	 * @param botTpiReplyTextService
	 * @param vo
	 * @param role
	 * @param textIdsOrTexts
	 */
	@Override		
	public void setText(BotMessageVO vo, String role, String... textIdsOrTexts) {
		setText(vo, role, true, textIdsOrTexts);
	}

	/**
	 * 純文字
	 * 
	 * @param botMessageService
	 * @param botTpiReplyTextService
	 * @param vo
	 * @param role
	 * @param useWording
	 * @param textIdsOrTexts
	 */
	public  void setText(BotMessageVO vo, String role, boolean useWording, String... textIdsOrTexts) {
		for (String textIdOrText : textIdsOrTexts) {
			if (useWording) {
				textIdOrText = getText(botTpiReplyTextService, role, textIdOrText);
			}
			this.setTextResult(vo, textIdOrText);
		}
	}

	/**
	 * 純文字(有替代變數)
	 * 
	 * @param botMessageService
	 * @param botTpiReplyTextService
	 * @param vo
	 * @param role
	 * @param replaceParams
	 * @param textIds
	 */
	public void setTextWithReplace(BotMessageVO vo, String role,
			Map<String, String> replaceParams, String textId) {

		boolean doReplace = replaceParams != null && !replaceParams.isEmpty();

		if (doReplace) {
			String replyText = getText(botTpiReplyTextService, role, textId);
			this.setTextResultWithReplace(vo, replyText, replaceParams);
		} else {
			setText(vo, role, textId);
		}
	}


	/**
	 * 從wording取得文字
	 * 
	 * @param botTpiReplyTextService
	 * @param role
	 * @param textId
	 * @return
	 */
	private String getText(BotTpiReplyTextService botTpiReplyTextService, String role, String textId) {
		return botTpiReplyTextService.getReplyText(role, textId);
	}	

	/**
	 * 多組純文字(有替代變數)
	 * 
	 * @param botMessageService
	 * @param botTpiReplyTextService
	 * @param vo
	 * @param role
	 * @param multiReplaceParams
	 * @param textIds
	 */
	public void setTextWithReplace(BotMessageVO vo, String role,
			Map<String, Map<String, String>> multiReplaceParams, String... textIds) {

		boolean doReplace = multiReplaceParams != null && !multiReplaceParams.isEmpty();

		for (String textId : textIds) {
			setTextWithReplace(vo, role,
					doReplace ? multiReplaceParams.get(textId) : null, textId);
		}
	}
	
	
	/**
	 * Carousel Card
	 * 
	 * @param botMessageService
	 * @param vo
	 * @param cardList
	 * @param title
	 * @param texts
	 * @param lifeLinks
	 * @return
	 */
	@Override	
	public void setCarouselCards(BotMessageVO vo, CarouselCard... carouselCards) {
		setCarouselCards(vo, null, carouselCards);
	}

	/**
	 * Carousel Card
	 * 
	 * @param botMessageService
	 * @param vo
	 * @param cardList
	 * @param title
	 * @param texts
	 * @param lifeLinks
	 * @return
	 */
	@Override	
	public void setCarouselCards(BotMessageVO vo,
			List<Map<String, Object>> cardList, CarouselCard... carouselCards) {

		boolean onlyOneData = cardList == null;

		if (onlyOneData) {
			cardList = new ArrayList<Map<String, Object>>();
		}

		for (CarouselCard carouselCard : carouselCards) {
			CardItem cards = vo.new CardItem();
			cards.setCWidth(carouselCard.getWidth().getValue());
			cards.setCTextType(carouselCard.getTextType().getValue());

			String title = carouselCard.getTitle();
			if (StringUtils.isNotBlank(title)) {
				cards.setCTitle(title);
			}

			String[] texts = carouselCard.getTexts();
			if (texts != null && texts.length > 0) {
				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
				for (String text : texts) {
					CTextItem cText = vo.new CTextItem();
					cText.setCText(text);
					cTextList.add(cText.getCTexts());
				}
				cards.setCTexts(cTextList);
			}

			LifeLink[] lifeLinks = carouselCard.getLifeLinks();
			if (lifeLinks != null && lifeLinks.length > 0) {
				cards.setCLinkType(Card.LinkType.連結.getValue());
				cards.setCLinkList(getCLinkList(vo, lifeLinks));
			}

			cardList.add(cards.getCards());
		}

		if (onlyOneData) {
			this.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
		}
	}
	
	
	
	@Override	
	public CarouselCard createCarouselCard(String title, String[] texts, LifeLink[] lifeLinks) {
		return new Card().new CarouselCard(title, texts, lifeLinks);
	}
	
	/**
	 * Carousel Card
	 * 
	 * @param botMessageService
	 * @param vo
	 * @param titles
	 * @param texts
	 * @param lifeLinks
	 */
	@Override		
	public void setCarouselCards(BotMessageVO vo,
			List<CarouselCard> carouselCards) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		for (CarouselCard carouselCard : carouselCards) {
			setCarouselCards( vo, cardList, carouselCard);
		}

		setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
	}	
	
	protected List<Map<String, Object>> getCardsForDownloadPDF(BotMessageVO vo, String imageText, List<LifeLink> links) {
		//操作流程圖PDF
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		cardList.add(this.getCardForDownloadPDF(vo, imageText, links));
		return cardList;
	}

	

	/**
	 * 多組注意事項
	 * 
	 * @param botMessageService
	 * @param botTpiReplyTextService
	 * @param vo
	 * @param role
	 * @param titleAndTextIdArray
	 */
	@Override		
	public void setNote(BotMessageVO vo, String role, String[]... titleAndTextIdArray) {

		for (int i = 0; i < titleAndTextIdArray.length; i++) {
			String[] titleAndTextId = titleAndTextIdArray[i];
			String title = titleAndTextId[0];
			if (title == null) {
				title = "注意事項";
				if (i > 0) {
					title += (i + 1);
				}
			}
			String textId = titleAndTextId[1];

			setNote(vo, role, title, textId);
		}
	}
	
	/**
	 * 牌卡
	 */
	public static class Card {


		/**
		 * 文字牌卡
		 */
		public class CarouselCard {
			private String title;

			private String[] texts;

			private LifeLink[] lifeLinks;

			private CardWidth width = CardWidth._200;

			
			private TextType textType = TextType.一般牌卡2;

			public CarouselCard(String title, String[] texts, LifeLink[] lifeLinks) {
				this.title = title;
				/*若內文為空，則不設內文*/
				if(!(texts.length==1 && StringUtils.isBlank(texts[0]))){			
					this.texts = texts;
				}
				this.lifeLinks = lifeLinks;
			}

			public String getTitle() {
				return title;
			}

			public void setTitle(String title) {
				this.title = title;
			}

			public String[] getTexts() {
				return texts;
			}

			public void setTexts(String[] texts) {
				this.texts = texts;
			}

			public LifeLink[] getLifeLinks() {
				return lifeLinks;
			}

			public void setLifeLinks(LifeLink[] lifeLinks) {
				this.lifeLinks = lifeLinks;
			}

			public CardWidth getWidth() {
				return this.width;
			}

			public void setWidth(CardWidth width) {
				this.width = width;
			}
			public void setWidthByText() {
				boolean wideCard = false;
				for(String text: texts) {
					if(StringUtils.isNotBlank(text) && text.length()>10) {
						wideCard = true;
					}	
				}
				
				if(wideCard) {
					this.width = CardWidth._250;
				} else {
					this.width = CardWidth._200;
				} 				
			}

			public TextType getTextType() {
				return textType;
			}

			public void setTextType(TextType textType) {
				this.textType = textType;
			}
		}

		/**
		 * 連結顯示類型
		 */
		public enum LinkType {
			連結(1), 按鈕(2);

			private int value;

			private LinkType(int value) {
				this.value = value;
			}

			public int getValue() {
				return this.value;
			}
		}

		/**
		 * 牌卡內容顯示格式
		 */
		public enum TextType {
			/** 左標題cLabel+右項目值cText */
			一般牌卡1("1"), 金額牌卡("2"), 多標題牌卡("3"),
			/** (1+2)Highlight標題 */
			混合牌卡("4"),
			/** 大標題cTitle +內容cText */
			一般牌卡2("5");

			private String value;

			private TextType(String value) {
				this.value = value;
			}

			public String getValue() {
				return this.value;
			}
		}

		/**
		 * 牌卡寬度
		 */
		public enum CardWidth {
			_200(BotMessageVO.CWIDTH_200), _250(BotMessageVO.CWIDTH_250);

			private String value;

			private CardWidth(String value) {
				this.value = value;
			}

			public String getValue() {
				return this.value;
			}
		}

	}	
	
	protected String getFormatAmount(String amount) {
		if (StringUtils.isBlank(amount) || !NumberUtils.isNumber(amount)) {
			return "";
		}

		return DataUtils.formatDollar(amount);
	}	
}