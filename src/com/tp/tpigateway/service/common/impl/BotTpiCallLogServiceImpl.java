package com.tp.tpigateway.service.common.impl;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.BotTpiCallLogDao;
import com.tp.tpigateway.model.common.BotRequestAndResponse;
import com.tp.tpigateway.model.common.BotTpiCallLog;
import com.tp.tpigateway.service.common.BotTpiCallLogService;

/**
 * 呼叫API log紀錄服務實作
 */
@Service("botTpiCallLogService")
@Transactional
public class BotTpiCallLogServiceImpl implements BotTpiCallLogService {
	
	private static final Logger logger = LoggerFactory.getLogger(BotTpiCallLogServiceImpl.class);

	@Autowired
	private BotTpiCallLogDao botTpiCallLogDao;
	
	@Async
	public void save(BotTpiCallLog log) {
		logger.debug("[save](" + log.getChatId() + ") " + log.getApiUrl() + ":" + log.getResult());
		botTpiCallLogDao.save(log);
	}
	
	@Override
	public BotRequestAndResponse queryByChatID(String customerID, String chatID, Timestamp insertTimeStart, Timestamp insertTimeEnd) {
		return botTpiCallLogDao.queryByChatID(customerID, chatID, insertTimeStart, insertTimeEnd);
	}
	
}
