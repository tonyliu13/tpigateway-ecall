package com.tp.tpigateway.service.common.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.BotCampaignLogDao;
import com.tp.tpigateway.model.common.BotCampaignLog;
import com.tp.tpigateway.model.common.BotCampaignLogId;
import com.tp.tpigateway.service.common.BotCampaignLogService;

@Service("botCampaignLogService")
@Transactional
public class BotCampaignLogServiceImpl implements BotCampaignLogService {

    private static Logger log = LoggerFactory.getLogger(BotCampaignLogServiceImpl.class);
    
    @Autowired
    private BotCampaignLogDao botCampaignLogDao;
    
    @Override
    public BotCampaignLog findByBotCampaignLogId(BotCampaignLogId botCampaignLogId) {
        
        log.debug("[findByBotCampaignLogId]");
        
        BotCampaignLog result = null;
        
        try {
            result = botCampaignLogDao.findByBotCampaignLogId(botCampaignLogId);
        } catch (Exception e) {
            log.error("[findByBotCampaignLogId] error:" + e.getMessage());
        }
        
        return result;
    }
    
    @Override
    public void save(BotCampaignLog campaignLog) {
        
        log.debug("[save] campaignLogId=" + campaignLog.getId().toString());
        
        try {
            botCampaignLogDao.save(campaignLog);
        } catch (Exception e) {
            log.error("[save] error:" + e.getMessage());
        }
    }

    @Override
    public BotCampaignLog findByCampaignIdAndCustId(String campaignId, String custId) {
        
        log.debug("[findByCampaignIdAndCustId]");
        
        BotCampaignLog result = null;
        
        try {
            result = botCampaignLogDao.findByCampaignIdAndCustId(campaignId, custId);
        } catch (Exception e) {
            log.error("[findByCampaignIdAndCustId] error:" + e.getMessage());
        }
        
        return result;
    }
}
