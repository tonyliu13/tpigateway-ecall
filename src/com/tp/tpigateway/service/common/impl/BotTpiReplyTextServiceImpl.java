package com.tp.tpigateway.service.common.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.dao.common.BotReplyTextDao;
import com.tp.tpigateway.model.common.BotReplyText;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;

/**
 * bot tpi 回覆清單服務實作
 */
@Service("botTpiReplyTextService")
//@Transactional
@SuppressWarnings({ "rawtypes", "unchecked" })
public class BotTpiReplyTextServiceImpl implements BotTpiReplyTextService {

	private static Logger log = LoggerFactory.getLogger(BotTpiReplyTextServiceImpl.class);
	
	@Autowired
	private BotReplyTextDao botReplyTextDao;
	
	private final static MultiKeyMap<String, String> replyTextsForBank = new MultiKeyMap(); // 3 keys: nodeId, roleId, textId
	private final static MultiKeyMap<String, String> replyTextsForInsurance = new MultiKeyMap(); // 2 keys: roleId, textId

	private static boolean isUpdate = false;

	@Override
	public String getReplyText(String roleId, String nodeId, String textId) {
		log.info("roleId=" + roleId + ",nodeId=" + nodeId + ",textId=" + textId);

		if (replyTextsForBank.isEmpty())
			this.refreshReplyText();

		// 若正在更新, 則稍候再取得資料
		try {
			while(isUpdate) {
				Thread.sleep(100);
			}
		} catch (Exception e) { }

        String replyText =  replyTextsForBank.get(nodeId, roleId, textId);

        log.debug("[getReplyText] replyText="+replyText);
        return replyText;
	}

    @Override
    public String getReplyText(String roleId, String textId) {
        log.info("roleId=" + roleId + ",textId=" + textId);

        if (replyTextsForInsurance.isEmpty())
            this.refreshReplyText();

        // 若正在更新, 則稍候再取得資料
        try {
            while(isUpdate) {
                Thread.sleep(100);
            }
        } catch (Exception e) { }

        String replyText =  replyTextsForInsurance.get(roleId, textId);
        log.debug("[getReplyText] replyText="+replyText);
        return replyText;
    }

    @Override
	public void refreshReplyText() {
		
		if (isUpdate) {
			log.info("更新回覆清單中...");
			//return;
			try {
			    while(isUpdate) {
			        Thread.sleep(100);
			    }
			    return;
			} catch(Exception e) {
			    log.error("等候更新回覆清單發生錯誤：" + e.getMessage());
			}
		}
		
		isUpdate = true;
		
		try {
			log.info("開始更新回覆清單");
			
			List<BotReplyText> texts = botReplyTextDao.findAll();
			
			MultiKeyMap<String, String> replyTextsForBank = new MultiKeyMap();
			MultiKeyMap<String, String> replyTextsForInsurance = new MultiKeyMap();
			
			if (CollectionUtils.isNotEmpty(texts)) {
				for (BotReplyText text : texts) {
					if (StringUtils.isNotBlank(text.getNodeId())) {
						replyTextsForBank.put(text.getNodeId(), text.getId().getRoleId(), text.getId().getTextId(), text.getText());
					} else {
						replyTextsForInsurance.put(text.getId().getRoleId(), text.getId().getTextId(), text.getText());
					}
				}
			}
			
			if (MapUtils.isNotEmpty(replyTextsForBank)) {
			    BotTpiReplyTextServiceImpl.replyTextsForBank.clear();
			    BotTpiReplyTextServiceImpl.replyTextsForBank.putAll(replyTextsForBank);
			}
			if (MapUtils.isNotEmpty(replyTextsForInsurance)) {
			    BotTpiReplyTextServiceImpl.replyTextsForInsurance.clear();
			    BotTpiReplyTextServiceImpl.replyTextsForInsurance.putAll(replyTextsForInsurance);
			}
			
			log.info("更新回覆清單結束");
		} catch (Exception e) {
			log.error("更新回覆清單發生異常，error:" + e.getMessage());
		} finally {
			isUpdate = false;
		}
	}

}
