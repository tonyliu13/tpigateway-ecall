package com.tp.tpigateway.service.common.impl;

import com.tp.tpigateway.configuration.cache.CacheName;
import com.tp.tpigateway.model.common.SessionStage;
import com.tp.tpigateway.repository.SessionStageRepository;
import com.tp.tpigateway.service.common.SessionStageService;
import com.tp.tpigateway.util.DataUtils;
import com.tp.tpigateway.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service("sessionStageService")
@Transactional
@CacheConfig(cacheNames = CacheName.SESSION_STAGE_KEY)
public class SessionStageServiceImpl implements SessionStageService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SessionStageRepository sessionStageRepository;

    @Autowired
    private CacheManager cacheManager;

    @Override
    public Cache getSessionStageRecCache() {
        return cacheManager.getCache(CacheName.SESSION_STAGE_KEY);
    }

    private void putCache(String key, SessionStage value) {
        getSessionStageRecCache().put(key, value);
    }

    private SessionStage getCache(String key) {
        return getSessionStageRecCache().get(key, SessionStage.class);
    }

    @Override
    @Transactional(readOnly = true)
    //@Cacheable(key = "#sessionId")
    public SessionStage findSessionStageBySessionId(String sessionId) {
        String key = sessionId;
        SessionStage sessionStage = getCache(sessionId);

        if (sessionStage != null) {
            return sessionStage;
        }

        sessionStage = sessionStageRepository.findFirstBySessionIdOrderBySessionStrTimeDesc(sessionId);

        if (sessionStage != null) {
            putCache(key, sessionStage);
        }

        return sessionStage;
    }

    @Override
    @CachePut(key = "#result.sessionId", condition = "#result != null")
    public SessionStage addSessionStage(SessionStage sessionStage) throws Exception {
        Date nowDateTime = DateUtil.getNow();
        //String uuid = TPIStringUtil.getUUID();

        String sessionId = sessionStage.getSessionId();
        if (StringUtils.isBlank(sessionId)) {
            throw new IllegalArgumentException("sessionId is Blank");
        }

        String id = sessionId + "-" + DateUtil.formatDate(new Date(), DateUtil.PATTERN_DATE);

        sessionStage.setId(id);
        sessionStage.setSessionStrTime(nowDateTime);
        sessionStage.setSessionUpdTime(nowDateTime);

        if (sessionStage.getStageScore() == null) {
            sessionStage.setStageScore(0);
        }
        if (sessionStage.getNoResTime() == null) {
            sessionStage.setNoResTime(0);
        }

//        sessionStageRepository.save(sessionStage);
        sessionStageRepository.save(handleValueLength(sessionStage));

        return sessionStage;
    }

    @Override
    @CachePut(key = "#result.sessionId", condition = "#result != null")
    public SessionStage updateSessionStage(SessionStage sessionStage) {
        SessionStage oldSessionStage = findSessionStageBySessionId(sessionStage.getSessionId());

        if (oldSessionStage == null) {
            return sessionStage;
        }

        BeanUtils.copyProperties(sessionStage, oldSessionStage, DataUtils.getNullPropertyNames(sessionStage));

        Date nowDateTime = DateUtil.getNow();
        oldSessionStage.setSessionUpdTime(nowDateTime);
//        sessionStageRepository.save(oldSessionStage);
        sessionStageRepository.save(handleValueLength(oldSessionStage));

        return oldSessionStage;
    }

    @Override
    public List<SessionStage> findBySessionUpdTimeGreaterThanEqual(Date dateTime) {
        return sessionStageRepository.findBySessionUpdTimeGreaterThanEqual(dateTime);
    }

    @Override
    public List<SessionStage> findBySessionUpdTimeGreaterThanEqualAndAgentIdIsNullOrderByStageScoreDescSessionUpdTimeAsc(Date date) {
        return sessionStageRepository.findBySessionUpdTimeGreaterThanEqualAndAgentIdIsNullOrderByStageScoreDescSessionUpdTimeAsc(date);
    }

    @Override
    @CachePut(key = "#sessionStage.sessionId", condition = "#sessionStage.id != null")
    public SessionStage saveSessionStage(SessionStage sessionStage) throws Exception {
        SessionStage oldSessionStage = findSessionStageBySessionId(sessionStage.getSessionId());

        if (oldSessionStage != null)
            return updateSessionStage(sessionStage);
        else
            return addSessionStage(sessionStage);
    }

    private SessionStage handleValueLength(SessionStage sessionStage) {
    		if (sessionStage != null) {
    			sessionStage.setSeriousAsk(StringUtils.substring(sessionStage.getSeriousAsk(), 0, 300));
    			sessionStage.setAgentId(StringUtils.substring(sessionStage.getAgentId(), 0, 50));
    			sessionStage.setCustomerComment(StringUtils.substring(sessionStage.getCustomerComment(), 0, 500));
    		}
    		return sessionStage;
    }
}
