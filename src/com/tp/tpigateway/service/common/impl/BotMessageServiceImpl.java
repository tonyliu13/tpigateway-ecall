package com.tp.tpigateway.service.common.impl;

import com.tp.tpigateway.model.common.AllAutoComplete;
import com.tp.tpigateway.model.common.AAALink;
import com.tp.tpigateway.model.common.ABALink;
import com.tp.tpigateway.model.common.ABBLink;
import com.tp.tpigateway.model.common.ACALink;
import com.tp.tpigateway.model.common.AIALink;
import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.*;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.enumeration.*;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeDService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeHService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeJService;
import com.tp.tpigateway.service.life.lifeapi.impl.CathayLifeTypeDServiceImpl;
import com.tp.tpigateway.service.life.lifeapi.impl.CathayLifeTypeJServiceImpl;
import com.tp.tpigateway.util.DataUtils;
import com.tp.tpigateway.util.DateUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("botMessageService")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class BotMessageServiceImpl extends CathayLifeBotMessageServiceImpl implements BotMessageService {

    private static Logger log = LoggerFactory.getLogger(BotMessageServiceImpl.class);

    @Autowired
	private CathayLifeTypeHService cathayLifeTypeHService;
    
    @Override
    public List<Map<String, Object>> getCardsForI1_1_2(BotMessageVO vo, List<Object> h01Result, BotLifeRequestVO reqVO ) throws Exception {

    	List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        for( int i = 0 ; i < h01Result.size() ; i++ ) {
        	
        	String policy_no = MapUtils.getString((Map) h01Result.get(i), H01.POLICY_NO.name());
        	
            //E02 單一保單資訊(繳費方式)
			//Map<String, Object> e02Result = cathayLifeTypeEService.e02(policy_no, reqVO.getQryFrm(), "ChatBot");
        	
        	CardItem cards = vo.new CardItem();
            cards.setCName("保單");
            cards.setCWidth(BotMessageVO.CWIDTH_250);

            CImageDataItem cImageData = vo.new CImageDataItem();
            cImageData.setCImage(BotMessageVO.IMG_CB03);
            cImageData.setCImageTextType(1);

            List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
            
            CImageTextItem cImageText = vo.new CImageTextItem();
            cImageText.setCImageTextTitle("保單名稱");
            cImageText.setCImageText(MapUtils.getString((Map) h01Result.get(i), H01.PROD_NAME.name()));
            CImageTextItem cImageText2 = vo.new CImageTextItem();
            cImageText2.setCImageTextTitle("保單號碼");
            cImageText2.setCImageText(MapUtils.getString((Map) h01Result.get(i), H01.POLICY_NO.name()));
            cImageTexts.add(cImageText.getCImageTexts());
            cImageTexts.add(cImageText2.getCImageTexts());
            cImageData.setCImageTexts(cImageTexts);
            cards.setCImageData(cImageData.getCImageDatas());
            cards.setCTextType("1");
            
            CTextItem cText3 = vo.new CTextItem();
            cText3.setCLabel("下次應繳日");
            cText3.setCText(MapUtils.getString((Map) h01Result.get(i), H01.MNXT_PAY_DATE.name()));
            CTextItem cText4 = vo.new CTextItem();
            cText4.setCLabel("下次應繳保費");
            cText4.setCText(MapUtils.getString((Map) h01Result.get(i), H01.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString((Map) h01Result.get(i), H01.PREM_RCPT.name())));
            CTextItem cText5 = vo.new CTextItem();
            cText5.setCLabel("繳別");
            cText5.setCText(MapUtils.getString((Map) h01Result.get(i), H01.PAY_FREQ_NM.name()));
            CTextItem cText6 = vo.new CTextItem();
            cText6.setCLabel("目前繳費方式");
			cText6.setCText(this.getPremTrnCodeName_H01((Map<String, Object>) h01Result.get(i)));
			//cText6.setCText(MapUtils.getString(e02Array.get(i), E02.KIND_CODE.name()));
            
            List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
            cTextList.add(cText3.getCTexts());
            cTextList.add(cText4.getCTexts());
            cTextList.add(cText5.getCTexts());
			cTextList.add(cText6.getCTexts());
			
            //判斷繳費方式
            String kind_code = MapUtils.getString((Map) h01Result.get(i), H01.KIND_CODE.name());
            
            // 若繳費方式為"轉帳扣款"或"信用卡扣款"才呼叫H03
            if( "3".equals(kind_code) || "4".equals(kind_code) || "5".equals(kind_code) ) {
            	//H03  保費扣款狀態(行庫名稱、銀行帳戶、發卡銀行、信用卡卡號)
    			Map<String, Object> h03Result = cathayLifeTypeHService.h03(policy_no);
    			CTextItem cText7 = vo.new CTextItem();
    			
    			if( "3".equals(kind_code) ) {
    				cText7.setCLabel("帳號末四碼");
    				cText7.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name()) + " " + MapUtils.getString(h03Result, H03.ACNT_NO.name()));
    				cTextList.add(cText7.getCTexts());
    			} else if( "4".equals(kind_code) || "5".equals(kind_code) ) {
    				cText7.setCLabel("卡號末四碼");
    				cText7.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) + " " + MapUtils.getString(h03Result, H03.CARD_NO.name()));
    				cTextList.add(cText7.getCTexts());
    			}
            }

            cards.setCTexts(cTextList);
            cards.setCLinkType(1);
            
            LifeLink[] links = { ACALink.前往繳費(policy_no), LifeLink.最近的繳費紀錄(policy_no) };
            //LifeLink[] links = { LifeLink.看繳費管道, LifeLink.最近的繳費紀錄(MapUtils.getString((Map) h01Result.get(i), H01.POLICY_NO.name())) };
            //LifeLink[] links = {LifeLink.保費從哪裡扣(MapUtils.getString((Map) h01Result.get(i), H01.POLICY_NO.name()))};
            List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
            cards.setCLinkList(cLinkList);

            log.info("cards=" + new JSONObject(cards).toString());

            cardList.add(cards.getCards());
            
        }
        /*
        for (Object obj : h01Result) {

            Map<String, Object> map = (Map<String, Object>) obj;

            CardItem cards = vo.new CardItem();
            cards.setCName("保單");
            cards.setCWidth(BotMessageVO.CWIDTH_250);

            CImageDataItem cImageData = vo.new CImageDataItem();
            cImageData.setCImage(BotMessageVO.IMG_CB03);
            cImageData.setCImageTextType(1);

            List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();

            CImageTextItem cImageText = vo.new CImageTextItem();
            cImageText.setCImageTextTitle("保單名稱");
            cImageText.setCImageText(MapUtils.getString(map, H01.PROD_NAME.name()));
            CImageTextItem cImageText2 = vo.new CImageTextItem();
            cImageText2.setCImageTextTitle("保單號碼");
            cImageText2.setCImageText(MapUtils.getString(map, H01.POLICY_NO.name()));
            cImageTexts.add(cImageText.getCImageTexts());
            cImageTexts.add(cImageText2.getCImageTexts());
            cImageData.setCImageTexts(cImageTexts);
            cards.setCImageData(cImageData.getCImageDatas());

            cards.setCTextType("1");
            List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
            CTextItem cText3 = vo.new CTextItem();
            cText3.setCLabel("下次應繳日");
            cText3.setCText(MapUtils.getString(map, H01.MNXT_PAY_DATE.name()));
            CTextItem cText4 = vo.new CTextItem();
            cText4.setCLabel("下次應繳保費");
            cText4.setCText(MapUtils.getString(map, H01.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(map, H01.PREM_RCPT.name())));
            CTextItem cText5 = vo.new CTextItem();
            cText5.setCLabel("繳別");
            cText5.setCText(MapUtils.getString(map, H01.PAY_FREQ_NM.name()));
            cTextList.add(cText3.getCTexts());
            cTextList.add(cText4.getCTexts());
            cTextList.add(cText5.getCTexts());
            /*
            //判斷繳費方式
            String kind_code = MapUtils.getString(e02Result, E02.KIND_CODE.name());
            if( "3".equals(kind_code) || "4".equals(kind_code) || "5".equals(kind_code) ) {
            	CTextItem cText6 = vo.new CTextItem();
    			cText6.setCLabel("下次應繳日");
    			cText6.setCText(MapUtils.getString(map, H01.MNXT_PAY_DATE.name()));
    			CTextItem cText7 = vo.new CTextItem();
    			cText7.setCLabel("下次應繳保費");
    			cText7.setCText(MapUtils.getString(map, H01.PREM_RCPT.name()));
    			CTextItem cText8 = vo.new CTextItem();
    			cText8.setCLabel("繳別中文");
    			cText8.setCText(MapUtils.getString(map, H01.PAY_FREQ_NM.name()));
    			CTextItem cText9 = vo.new CTextItem();
    			cText9.setCLabel("目前繳費方式");
    			cText9.setCText(MapUtils.getString(e02Result, E02.KIND_CODE.name()));
    			
    			CTextItem cText10 = vo.new CTextItem();
    			if( "3".equals(kind_code) ) {
    				cText10.setCLabel("帳號末四碼");
    				cText10.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name())+MapUtils.getString(h03Result, H03.ACNT_NO.name()));
    			} else if( "4".equals(kind_code) || "5".equals(kind_code) ) {
    				cText10.setCLabel("卡號末四碼");
    				cText10.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name())+MapUtils.getString(h03Result, H03.CARD_NO.name()));
    			}
            }
            
            cards.setCTexts(cTextList);
            cards.setCLinkType(1);
            //String policyNo = MapUtils.getString(map, H01.POLICY_NO.name());
            
            LifeLink[] links = {LifeLink.保費從哪裡扣(MapUtils.getString(map, H01.POLICY_NO.name()))};
            List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
            cards.setCLinkList(cLinkList);

            log.info("cards=" + new JSONObject(cards).toString());

            cardList.add(cards.getCards());
        }*/

        return cardList;
    }

    @Override
    public List<Map<String, Object>> getPolicyFilterForI1_1_2(BotMessageVO vo, List<Object> h01Result) {
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();

        for (Object obj : h01Result) {

            Map<String, Object> map = (Map<String, Object>) obj;

            DataListItem data = vo.new DataListItem();

            String prodName = MapUtils.getString(map, H01.PROD_NAME.name());
            String policyNo = MapUtils.getString(map, H01.POLICY_NO.name());

            // 20180502 by 淑靜, 我要查保單H01[保單名稱](H01[保單號碼])
            LifeLink link = LifeLink.我要查保單(prodName, policyNo);

            data.setDlAction(link.getAction());
            data.setDlText(link.getText());
            data.setDlAlt(link.getAlt());

            PDataItem pData = vo.new PDataItem();
            pData.setPolicyNo(policyNo);
            pData.setProdName(MapUtils.getString(map, H01.PROD_NAME.name()));
            pData.setApcNm(MapUtils.getString(map, H01.APC_NM.name()));
            pData.setInsdNm(MapUtils.getString(map, H01.INSD_NM.name()));
            data.setPData(pData.getpData());

            log.info("data=" + new JSONObject(data).toString());

            dataList.add(data.getData());
        }

        return dataList;
    }
  

    @Override
    public List<Map<String, Object>> getCardsForI2_0_1(BotMessageVO vo, Map<String, Object> e02Result) {
        /*
            保單名稱
            [險別中文名稱]
            保單號碼
            [保單號碼]
            --------------------------------------------
            下次應繳日[下次應繳日]
            下次應繳保費[幣別][下次應繳保費]
            繳費方式[繳費方式]
         */
        List<Map<String, Object>> cardList = new ArrayList<>();

        CardItem cards = vo.new CardItem();
        cards.setCName("保單");
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB03);
        cImageData.setCImageTextType(1);

        List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();

        CImageTextItem cImageText = vo.new CImageTextItem();
        cImageText.setCImageTextTitle("保單名稱");
        cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));

        CImageTextItem cImageText2 = vo.new CImageTextItem();
        cImageText2.setCImageTextTitle("保單號碼");
        cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));

        cImageTexts.add(cImageText.getCImageTexts());
        cImageTexts.add(cImageText2.getCImageTexts());

        cImageData.setCImageTexts(cImageTexts);
        cards.setCImageData(cImageData.getCImageDatas());

        //text
        List<Map<String, Object>> cTextList = new ArrayList<>();
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCLabel("下次應繳日");
        cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
        CTextItem cText2 = vo.new CTextItem();
        cText2.setCLabel("下次應繳保費");
        cText2.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCLabel("繳費方式");
        cText3.setCText(getPremTrnCodeName(e02Result));

        cTextList.add(cText1.getCTexts());
        cTextList.add(cText2.getCTexts());
        cTextList.add(cText3.getCTexts());
        cards.setCTexts(cTextList);

        log.info("cards=" + new JSONObject(cards).toString());

        cardList.add(cards.getCards());
        return cardList;
    }

    @Override
    public List<Map<String, Object>> getCardsForI2_1(BotMessageVO vo, Map<String, Object> e02Result) {
        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        CardItem cards = vo.new CardItem();
        cards.setCName("保單");
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB03);
        cImageData.setCImageTextType(1);

        List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();

        CImageTextItem cImageText = vo.new CImageTextItem();
        cImageText.setCImageTextTitle("保單名稱");
        cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));

        CImageTextItem cImageText2 = vo.new CImageTextItem();
        cImageText2.setCImageTextTitle("保單號碼");
        String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
        cImageText2.setCImageText(policyNo);

        cImageTexts.add(cImageText.getCImageTexts());
        cImageTexts.add(cImageText2.getCImageTexts());

        cImageData.setCImageTexts(cImageTexts);
        cards.setCImageData(cImageData.getCImageDatas());
        //20180504 更新投資保單公版
        CathayLifeTypeJService cathayLifeTypeJService = new CathayLifeTypeJServiceImpl();
        try {
        	String TRY_CNT_DATE = DateUtil.formatDate(DateUtil.getToday(), DateUtil.PATTERN_DASH_DATE);
        	
			Map<String, Object> j01Result = cathayLifeTypeJService.j01(policyNo, 
					MapUtils.getString(e02Result, E02.INSD_ID.name()), "A", 
					TRY_CNT_DATE, 
					MapUtils.getString(e02Result, E02.ISSUE_DATE.name()), 
					MapUtils.getString(e02Result, E02.PAY_FREQ.name()),
					MapUtils.getString(e02Result, E02.PROD_ID.name()));
			
			
			
	        log.debug("j01Result=" + j01Result);
	        //墊繳本息合計 [幣別][合計金額]
	        cards.setCTextType("1");
	        List<Map<String, Object>> cTextList = new ArrayList<>();
	        CTextItem cText1 = vo.new CTextItem();
	        cText1.setCLabel("契約效力 ");
	        cText1.setCText(MapUtils.getString(e02Result, E02.EFT_CODE_NM.name()));
	        

	        CTextItem cText2 = vo.new CTextItem();
	        cText2.setCLabel("試算日期 ");
	        cText2.setCText(TRY_CNT_DATE);	        

	        CTextItem cText3 = vo.new CTextItem();
	        cText3.setCLabel("保單帳戶價值");
	        String polVal = MapUtils.getString(e02Result, E02.CURR.name())+ " " + DataUtils.formatDollar(MapUtils.getString(j01Result, J01.NEW_POL_VAL.name()));
	        cText3.setCText(polVal);
	        
	        cTextList.add(cText1.getCTexts());
	        cTextList.add(cText2.getCTexts());
	        cTextList.add(cText3.getCTexts());
	        cards.setCTexts(cTextList);
		} catch (Exception e) {
			log.error("呼叫 J01 API錯誤"+e.getMessage());
		}
        
      
        
        //link
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] link1s = { LifeLink.查保單業務員 , LifeLink.最近的繳費紀錄(policyNo)};
		LifeLink[] link1s = { LifeLink.查保單業務員(policyNo) , LifeLink.最近的繳費紀錄(policyNo)};
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));

        log.info("cards=" + new JSONObject(cards).toString());

        cardList.add(cards.getCards());
        
            
        return cardList;
    }
	
	@Override
	public List<Map<String, Object>> getCardsForI2_2_1(BotMessageVO vo, Map<String, Object> e02Result) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB04);
		cImageData.setCImageTextType(1);
		cImageData.setCImageTag("停效");
		
		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("停效日期");
		cText1.setCText(MapUtils.getString(e02Result, E02.LPS_DATE.name()));
		cTextList.add(cText1.getCTexts());
		cards.setCTexts(cTextList);
		
		//link
		cards.setCLinkType(1);
		LifeLink[] link1s = { LifeLink.申請復效辦理方式 };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

    @Override
    public List<Map<String, Object>> getCardsForI2_3(BotMessageVO vo, Map<String, Object> e02Result) {
        return getCardsForI2_3(vo, e02Result, "　保險費墊繳中　　　");
    }

    @Override
    public List<Map<String, Object>> getCardsForI2_3(BotMessageVO vo, Map<String, Object> e02Result, String cMessage) {
        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        CardItem cards = vo.new CardItem();
        cards.setCName("保單");
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB00);
        cImageData.setCImageTextType(1);

        List<Map<String, Object>> cImageTexts = new ArrayList<>();

        CImageTextItem cImageText = vo.new CImageTextItem();
        cImageText.setCImageTextTitle("保單名稱");
        cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
        String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
        CImageTextItem cImageText2 = vo.new CImageTextItem();
        cImageText2.setCImageTextTitle("保單號碼");
        cImageText2.setCImageText(policyNo);

        cImageTexts.add(cImageText.getCImageTexts());
        cImageTexts.add(cImageText2.getCImageTexts());

        cImageData.setCImageTexts(cImageTexts);
        cards.setCImageData(cImageData.getCImageDatas());

        if (StringUtils.isNotBlank(cMessage)) {
            cards.setCTextType("1");
            cards.setCMessage(cMessage);
        }
        //20180504 修改墊交公版
        try {
        	CathayLifeTypeDService cathayLifeTypeDService = new CathayLifeTypeDServiceImpl();
			Map<String, Object> d26Result = cathayLifeTypeDService.d26(policyNo, "ChatBot", "", "", "");
			
	        log.debug("d26Result=" + d26Result);
	        //墊繳本息合計 [幣別][合計金額]
	        cards.setCTextType("1");
	        List<Map<String, Object>> cTextList = new ArrayList<>();
	        CTextItem cText1 = vo.new CTextItem();
	        
	        cText1.setCLabel("墊繳本息合計");
	        cText1.setCText(MapUtils.getString(d26Result, D26.CURR.name()) +"　"+ DataUtils.formatDollar(MapUtils.getString(d26Result, D26.TOT_PREM.name())));

	        cTextList.add(cText1.getCTexts());
	        cards.setCTexts(cTextList);
		} catch (Exception e) {
			log.error("呼叫 D26 API錯誤"+e.getMessage());
		}

        
        //link
  		cards.setCLinkType(1);
  		LifeLink[] link1s = { ABBLink.S027_保單墊繳怎麼辦(policyNo) };
  		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));

        log.info("cards=" + new JSONObject(cards).toString());

        cardList.add(cards.getCards());
        return cardList;
    }	

	@Override
	public List<Map<String, Object>> getCardsForI2_5_3(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("最近繳費日");
		cText3.setCText(MapUtils.getString(e02Result, E02.LST_PAY_DATE.name()));
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("下次應繳日");
		cText4.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("下次應繳保費");
		cText5.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		cards.setCTexts(cTextList);
		
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// List<Map<String, Object>> cLinkList = this.getCLinkList(vo, LifeLink.看繳費管道);
		List<Map<String, Object>> cLinkList = this.getCLinkList(vo, LifeLink.看繳費管道(policyNo));
		cards.setCLinkList(cLinkList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI4_1_2(BotMessageVO vo, List<Object> d06Result, Map<String, Object> e02Result) {
		/*
		 	[附約編號]
			[特約種類]
			[被保人姓名]
			[已繳金額]
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		int subContNo = 1;
		for (Object obj : d06Result) {
			Map<String, Object> map = (Map<String, Object>) obj;

			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			cards.setCTextType("4");
			cards.setCTitle("附約" + subContNo);

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			CTextItem cText1 = vo.new CTextItem();
			cText1.setCTextClass("1");
			cText1.setCLabel("附約名稱");
			cText1.setCText(MapUtils.getString(map, D06.RD_ID_NM.name()));
			
			CTextItem cText2 = vo.new CTextItem();
			cText2.setCTextClass("1");
			cText2.setCLabel("被保人姓名");
			cText2.setCText(MapUtils.getString(map, D06.INSD_NM.name()));
			
			CTextItem cText3 = vo.new CTextItem();
			cText3.setCTextClass("2");
			cText3.setCLabel("已繳金額");
			cText3.setCUnit(MapUtils.getString(e02Result, E02.CURR.name()));
			cText3.setCAmount(DataUtils.formatDollar(MapUtils.getString(map, D06.PREM_RCPT.name())));

			cTextList.add(cText1.getCTexts());
			cTextList.add(cText2.getCTexts());
			cTextList.add(cText3.getCTexts());
			
			cards.setCTexts(cTextList);
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
			subContNo ++;
		}
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI2_6_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
			[險別中文名稱] 
			[保單號碼]
			[繳次]
			[最近繳費日]
			[繳費管道中文]
			[下次應繳日]
			[ 一扣日期 ]
	    */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		cards.setCTextType("1");		
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("最近繳費日");
		cText2.setCText(MapUtils.getString(e02Result, E02.LST_PAY_DATE.name()));
		
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("下次應繳日");
		cText4.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("下次應繳保費");
		cText5.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		
		CTextItem cText6 = vo.new CTextItem();
		cText6.setCLabel("下次扣款日");
		cText6.setCText(MapUtils.getString(h03Result, H03.FST_TRN_DATE.name()));
		
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		cTextList.add(cText6.getCTexts());
		cards.setCTexts(cTextList);
		
		//link 
  		cards.setCLinkType(1);
  		// 20181011 by Todd, keep保單號碼
  		// LifeLink[] link1s = { LifeLink.保費扣款方式, LifeLink.最近的繳費紀錄 };
  		LifeLink[] link1s = { LifeLink.保費扣款方式(policyNo), LifeLink.最近的繳費紀錄(policyNo) };
  		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI2_7_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result) {
		/*
			[險別中文名稱]
			[保單號碼]
			[下次應繳日] 
			[扣款日期] 
			[二扣日期]
	    */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		cards.setCTextType("1");
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("下次應繳保費");
		cText4.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("最近扣款日");
		cText2.setCText(MapUtils.getString(d01Result, D01.DCT_BANK_DATE.name()));
		
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("預計扣款日");
		cText3.setCText(MapUtils.getString(h03Result, H03.SEC_TRN_DATE.name()));
		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cards.setCTexts(cTextList);
		
		//link
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.保費扣款方式, LifeLink.看繳費管道 };
		LifeLink[] links = { LifeLink.保費扣款方式(policyNo), LifeLink.看繳費管道(policyNo) };
		List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
		cards.setCLinkList(cLinkList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI2_8_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result) {
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.保費扣款方式, LifeLink.看繳費管道 };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.保費扣款方式(policyNo), LifeLink.看繳費管道(policyNo) };
		return this.getCommonCards5(vo, e02Result, h03Result, d01Result, links);
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI2_9_2(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result) {
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.看繳費管道 };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.看繳費管道(policyNo) };
		return this.getCommonCards5(vo, e02Result, h03Result, d01Result, links);
	}

	@Override
	public List<Map<String, Object>> getCardsForI2_10_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.看繳費管道 };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.看繳費管道(policyNo) };
		return this.getCommonCards1(vo, e02Result, h03Result, links);
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI2_11_2(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result) {
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.看繳費管道 };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.看繳費管道(policyNo) };
		return this.getCommonCards5(vo, e02Result, h03Result, d01Result, links);
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI2_12_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result, Map<String, Object> d01Result) {
		return this.getCommonCards5(vo, e02Result, h03Result, d01Result, null);
	}
	
	@Override
	public Map<String, Object> getCardForI3_1_2(BotMessageVO vo, Map<String, Object> d02Result ,Map<String,Object> e02Result) {
		String PAY_TIMES = MapUtils.getString(d02Result, D02.PAY_TIMES.name());
		String RCPT_SER_NO = MapUtils.getString(d02Result, D02.RCPT_SER_NO.name());
		// for ChatFlow 需增加帶入繳次
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO), LifeLink.保單墊繳怎麼辦 };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO, policyNo), ABBLink.S027_保單墊繳怎麼辦(policyNo) };
		return this.getCommonCards2(vo, d02Result, e02Result, links);
	}

	@Override
	public Map<String, Object> getCardForI3_2_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result) {
		String PAY_TIMES = MapUtils.getString(d02Result, D02.PAY_TIMES.name());
		String RCPT_SER_NO = MapUtils.getString(d02Result, D02.RCPT_SER_NO.name());
		// for ChatFlow 需增加帶入繳次
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO), LifeLink.保單墊繳怎麼辦 };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO, policyNo), ABBLink.S027_保單墊繳怎麼辦(policyNo)};
		return this.getCommonCards3(vo, d02Result, e02Result, links);
	}

	@Override
	public Map<String, Object> getCardForI3_3_2(BotMessageVO vo, Map<String, Object> d02Result ,Map<String,Object> e02Result) {
		String PAY_TIMES = MapUtils.getString(d02Result, D02.PAY_TIMES.name());
		String RCPT_SER_NO = MapUtils.getString(d02Result, D02.RCPT_SER_NO.name());
		// for ChatFlow 需增加帶入繳次
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO), LifeLink.保單墊繳怎麼辦 };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO, policyNo), ABBLink.S027_保單墊繳怎麼辦(policyNo) };
		return this.getCommonCards4(vo, d02Result, e02Result, links);
	}

	@Override
	public Map<String, Object> getCardForI3_4_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result) {
		String PAY_TIMES = MapUtils.getString(d02Result, D02.PAY_TIMES.name());
		String RCPT_SER_NO = MapUtils.getString(d02Result, D02.RCPT_SER_NO.name());
		// for ChatFlow 需增加帶入繳次
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO) };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO, policyNo) };
		return this.getCommonCards2(vo, d02Result, e02Result, links);
	}

	@Override
	public Map<String, Object> getCardForI3_5_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result) {
		String PAY_TIMES = MapUtils.getString(d02Result, D02.PAY_TIMES.name());
		String RCPT_SER_NO = MapUtils.getString(d02Result, D02.RCPT_SER_NO.name());
		// for ChatFlow 需增加帶入繳次
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO) };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO, policyNo) };
		return this.getCommonCards3(vo, d02Result, e02Result, links);
	}

	@Override
	public Map<String, Object> getCardForI3_6_2(BotMessageVO vo, Map<String, Object> d02Result, Map<String,Object> e02Result) {
		String PAY_TIMES = MapUtils.getString(d02Result, D02.PAY_TIMES.name());
		String RCPT_SER_NO = MapUtils.getString(d02Result, D02.RCPT_SER_NO.name());
		// for ChatFlow 需增加帶入繳次
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO) };
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		LifeLink[] links = { LifeLink.查附約繳費金額(PAY_TIMES, RCPT_SER_NO, policyNo) };
		return this.getCommonCards4(vo, d02Result, e02Result, links);
	}

	@Override
	public List<Map<String, Object>> getCardsForI2_12_1_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> d01Result) {
		/*
		   [險別中文名稱]
		   [保單號碼]
		   [失敗Tag]
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		cards.setCMessage(MapUtils.getString(d01Result, D01.FAIL_REASON));
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI2_12_2_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> d01Result) {
		/*
		   [險別中文名稱]
		   [保單號碼]
		   [失敗Tag]
		*/
		return getCardsForI2_12_1_1(vo, e02Result, d01Result);
	}

	// 20181011 by Todd, keep保單號碼
	@Override
	public List<Map<String, Object>> getCardsForI2_12_2_4(BotMessageVO vo) {
		return getCardsForI2_12_2_4(vo, null);
	}
	
	// 20181011 by Todd, keep保單號碼
	@Override
	public List<Map<String, Object>> getCardsForI2_12_2_4(BotMessageVO vo, String policyNo) {
		/*
		    以要保人身分登入會員網站
			來電客服中心
			聯繫業務員
			
			link: 
				前往網站
			    撥號
			 	查看業務員
		 */
		String[] textInfo = {"線上櫃檯辦理:需為保單要保人且具有線上服務資格", "來電客服中心: ", "聯繫業務員: "};
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] linkInfo = { LifeLink.以要保人身分登入會員網站, LifeLink.撥號_來電客服中心, LifeLink.查保單業務員 };
		LifeLink[] linkInfo = { LifeLink.以要保人身分登入會員網站, LifeLink.撥號_來電客服中心, StringUtils.isNotBlank(policyNo) ? LifeLink.查保單業務員(policyNo) : LifeLink.查保單業務員("") };
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		for(int i=0; i<textInfo.length; i++) {
			String[] info = textInfo[i].split(":");
			String title = info[0];
			String desc = info[1];
			LifeLink link = linkInfo[i];
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CB03);
			cards.setCImageData(cImageData.getCImageDatas());
			
			cards.setCTextType("5");
			cards.setCTitle(title);
			
			if(StringUtils.isNotBlank(desc)) {
				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
				CTextItem cText1 = vo.new CTextItem();
				cText1.setCText(desc);
				cTextList.add(cText1.getCTexts());
				cards.setCTexts(cTextList);
			}
			
			//link
			cards.setCLinkType(1);
			cards.setCLinkList(this.getCLinkList(vo, link));
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_1_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
			[險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日] 
			目前繳費方式：[保費轉帳中文]
			扣款銀行：[行庫名稱]
			銀行帳號末四碼：{[銀行帳號]帳號末四碼}
		*/
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("下次應繳保費");
		cText3.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("目前繳費方式");
		cText2.setCText(getPremTrnCodeName(e02Result));		
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("銀行帳號末四碼");
		if (MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name()) != null && MapUtils.getString(h03Result, H03.ACNT_NO.name()) != null) {
			cText4.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name()) + MapUtils.getString(h03Result, H03.ACNT_NO.name()));
		} else {
			cText4.setCText("--");
		}		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText4.getCTexts());
		cards.setCTexts(cTextList);
		
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查保單業務員 };
		LifeLink[] links = { LifeLink.查保單業務員(policyNo) };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_2_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
			[險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日] 
			目前繳費方式：[保費轉帳中文]
			扣款信用卡：[發卡銀行]
			卡號末四碼：{[信用卡號]帳號末四碼}
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("下次應繳保費");
		cText3.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("目前繳費方式");
		cText2.setCText(getPremTrnCodeName(e02Result));
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("卡號末四碼");
		if (MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) != null && MapUtils.getString(h03Result, H03.CARD_NO.name()) != null) {			
			cText4.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) + MapUtils.getString(h03Result, H03.CARD_NO.name()));
		} else {			
			cText4.setCText("--");
		}	
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText4.getCTexts());
		
		cards.setCTexts(cTextList);
		
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查保單業務員 };
		LifeLink[] links = { LifeLink.查保單業務員(policyNo) };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_3_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
			[險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日] 
			目前繳費方式：[保費轉帳中文]
		*/
		
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		cards.setCTextType("1");
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("下次應繳保費");
		cText3.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("目前繳費方式");
		cText2.setCText(getPremTrnCodeName(e02Result));
		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText2.getCTexts());
		cards.setCTexts(cTextList);
		
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.查保單業務員 };
		LifeLink[] links = { LifeLink.查保單業務員(policyNo) };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_4_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
		 	[險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日] 
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		cards.setCTextType("1");
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("下次應繳保費");
		cText2.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cards.setCTexts(cTextList);
		
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.看繳費管道 };
		LifeLink[] links = { LifeLink.看繳費管道(policyNo) };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_5_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
			[險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日]
			目前繳費方式：[保費轉帳中文]
			扣款銀行：[行庫名稱]
			帳號末四碼：{[銀行帳號]帳號末四碼}
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("下次應繳保費");
		cText3.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("目前繳費方式");
		cText2.setCText(getPremTrnCodeName(e02Result));		
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("銀行帳號末四碼");
		if (MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name()) != null && MapUtils.getString(h03Result, H03.ACNT_NO.name()) != null) {
			cText4.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name()) + MapUtils.getString(h03Result, H03.ACNT_NO.name()));
		} else {
			cText4.setCText("--");
		}		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText4.getCTexts());
		cards.setCTexts(cTextList);		
		
		log.info("cards=" + new JSONObject(cards).toString());		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_6_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
		    [險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日]
			目前繳費方式：[保費轉帳中文]
			扣款信用卡：[發卡銀行]
			卡號末四碼：{[信用卡號]帳號末四碼}
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("下次應繳保費");
		cText3.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("目前繳費方式");
		cText2.setCText(getPremTrnCodeName(e02Result));
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("卡號末四碼");
		if (MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) != null && MapUtils.getString(h03Result, H03.CARD_NO.name()) != null) {
			cText4.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) + MapUtils.getString(h03Result, H03.CARD_NO.name()));
		} else {
			cText4.setCText("--");
		}		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText4.getCTexts());
		
		cards.setCTexts(cTextList);		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_7_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
		    [險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日]
			目前已授權：[保費轉帳中文]
			扣款銀行：[行庫名稱]
			帳號末四碼：{[銀行帳號]帳號末四碼}
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("下次應繳保費");
		cText3.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("目前繳費方式");
		cText2.setCText(getPremTrnCodeName(e02Result));		
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("銀行帳號末四碼");
		if (MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name()) != null && MapUtils.getString(h03Result, H03.ACNT_NO.name()) != null) {
			cText4.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_ACNT.name()) + MapUtils.getString(h03Result, H03.ACNT_NO.name()));
		} else {
			cText4.setCText("--");
		}		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText4.getCTexts());
		cards.setCTexts(cTextList);
		
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.看繳費管道 };
		LifeLink[] links = { LifeLink.看繳費管道(policyNo) };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_8_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
		    [險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日]
			目前已授權：[保費轉帳中文]
			扣款信用卡：[發卡銀行]
			卡號末四碼：{[信用卡號]帳號末四碼}
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181011 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("下次應繳保費");
		cText3.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("目前繳費方式");
		cText2.setCText(getPremTrnCodeName(e02Result));
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("卡號末四碼");
		if (MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) != null && MapUtils.getString(h03Result, H03.CARD_NO.name()) != null) {
			cText4.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) + MapUtils.getString(h03Result, H03.CARD_NO.name()));
		} else {
			cText4.setCText("--");
		}
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText4.getCTexts());
		
		cards.setCTexts(cTextList);
		cards.setCLinkType(1);
		// 20181011 by Todd, keep保單號碼
		// LifeLink[] links = { LifeLink.看繳費管道 };
		LifeLink[] links = { LifeLink.看繳費管道(policyNo) };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));
		
		log.info("cards=" + new JSONObject(cards).toString());		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_9_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
		    [險別中文名稱]
			[保單號碼]
			下次應繳日：[下次應繳日]
		 */
		return this.getCardsForI6_4_1(vo, e02Result, h03Result);
	}

	@Override
	public List<Map<String, Object>> getCardsForI6_10_1(BotMessageVO vo, Map<String, Object> e02Result, Map<String, Object> h03Result) {
		/*
			[險別中文名稱]
			[保單號碼]
			下次應繳日為：[下次應繳日]
			扣款信用卡：[發卡銀行]
			卡號末四碼：{[信用卡號]帳號末四碼}
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));
		
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		
		cImageData.setCImageTexts(cImageTexts);

		cards.setCImageData(cImageData.getCImageDatas());
		
		cards.setCTextType("1");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("下次應繳日");
		cText1.setCText(MapUtils.getString(e02Result, E02.MNXT_PAY_DATE.name()));
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("下次應繳保費");
		cText2.setCText(MapUtils.getString(e02Result, E02.CURR.name()) + " " + DataUtils.formatDollar(MapUtils.getString(e02Result, E02.PREM_RCPT.name())));
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("卡號末四碼");
		if (MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) != null && MapUtils.getString(h03Result, H03.CARD_NO.name()) != null) {
			cText3.setCText(MapUtils.getString(h03Result, H03.BANK_NAME_CARD.name()) + MapUtils.getString(h03Result, H03.CARD_NO.name()));
		} else {
			cText3.setCText("--");
		}
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		
		return cardList;
	}
	
	// 20181011 by Todd, keep保單號碼
	@Override
	// public List<Map<String, Object>> getCardsForI7_1_2(BotMessageVO vo) {
	public List<Map<String, Object>> getCardsForI7_1_2(BotMessageVO vo, Map<String, Object> e02Result) {
		
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		/*
		CardItem cards1 = vo.new CardItem();
		cards1.setCWidth(BotMessageVO.CWIDTH_200);
		cards1.setCTextType("5");
		cards1.setCTitle("超商現金繳費");
		cards1.setCLinkType(1);
		LifeLink[] links1 = {LifeLink.ibon, LifeLink.全家FamiPort};
		cards1.setCLinkList(this.getCLinkList(vo, Arrays.asList(links1)));
		*/
		
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		
		CardItem cards2 = vo.new CardItem();
		cards2.setCWidth(BotMessageVO.CWIDTH_200);
		cards2.setCTextType("5");
		cards2.setCTitle("信用卡繳費");
		cards2.setCLinkType(1);
		// LifeLink[] links2 = {LifeLink.雲端繳款單, LifeLink.信用卡繳費MyBill, LifeLink.國泰人壽線上櫃檯, LifeLink.國泰人壽APP};
		LifeLink[] links2 = {LifeLink.雲端繳款單(policyNo), LifeLink.信用卡繳費MyBill, LifeLink.國泰人壽線上櫃檯, LifeLink.國泰人壽APP};
		cards2.setCLinkList(this.getCLinkList(vo, Arrays.asList(links2)));
		
		CardItem cards3 = vo.new CardItem();
		cards3.setCWidth(BotMessageVO.CWIDTH_200);
		cards3.setCTextType("5");
		cards3.setCTitle("本人帳戶繳費");
		cards3.setCLinkType(1);
		// LifeLink[] links3 = {LifeLink.雲端繳款單, LifeLink.國泰人壽APP, LifeLink.網銀轉帳繳費MyBank, LifeLink.國泰世華ATM};
		LifeLink[] links3 = {LifeLink.雲端繳款單(policyNo), LifeLink.國泰人壽APP, LifeLink.網銀轉帳繳費MyBank, LifeLink.國泰世華ATM};
		cards3.setCLinkList(this.getCLinkList(vo, Arrays.asList(links3)));
		
		CardItem cards4 = vo.new CardItem();
		cards4.setCWidth(BotMessageVO.CWIDTH_200);
		cards4.setCTextType("5");
		cards4.setCTitle("現金繳費");
		cards4.setCLinkType(1);
		// LifeLink[] links4 = {LifeLink.雲端繳款單, LifeLink.ibon, LifeLink.全家FamiPort, LifeLink.國泰人壽服務櫃台};
		LifeLink[] links4 = {LifeLink.雲端繳款單(policyNo), LifeLink.ibon(policyNo), LifeLink.全家FamiPort(policyNo), LifeLink.國泰人壽服務櫃台};
		cards4.setCLinkList(this.getCLinkList(vo, Arrays.asList(links4)));
		
		//cardList.add(cards1.getCards());
		cardList.add(cards2.getCards());
		cardList.add(cards3.getCards());
		cardList.add(cards4.getCards());
		 
		/*List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		String[] textInfo = {"統一超商繳費: ", "全家超商繳費: ", "信用卡線上繳費:需具有世華卡", "網路銀行繳費: ", "服務據點繳費: ",
				"國泰世華ATM: ", "國泰人壽線上櫃檯: ", "郵政劃撥: " };
		LifeLink[] linkInfo = { LifeLink.ibon, LifeLink.全家FamiPort, LifeLink.信用卡繳費MyBill, LifeLink.網銀轉帳繳費MyBank,
				LifeLink.國泰人壽服務據點, LifeLink.國泰世華ATM, LifeLink.國泰人壽線上櫃檯, LifeLink.郵政劃撥 };
		
		for(int i=0; i<textInfo.length; i++) {
			String[] info = textInfo[i].split(":");
			String title = info[0];
			String desc = info[1];
			LifeLink link = linkInfo[i];
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			
			cards.setCTextType("5");
			cards.setCTitle(title);
			
			if(StringUtils.isNotBlank(desc)) {
				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
				CTextItem cText1 = vo.new CTextItem();
				cText1.setCText(desc);
				cTextList.add(cText1.getCTexts());
				cards.setCTexts(cTextList);
			}
			
			//link
			cards.setCLinkType(1);
			cards.setCLinkList(this.getCLinkList(vo, link));
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}*/
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI7_2_2(BotMessageVO vo) {
		/*
		  carousel 文字牌卡
		    國泰人壽專用外幣匯款帳號
			國泰人壽服務櫃台
			查保單業務員
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		//String[] textInfo = {"金融機構匯款: ", "服務據點繳費: ", "找業務員協助: "};
		//LifeLink[] linkInfo = { LifeLink.國泰人壽外幣帳戶, LifeLink.國泰人壽服務櫃台, LifeLink.查保單業務員 };

		String[] textInfo = {"金融機構匯款: "};
		LifeLink[] linkInfo = { LifeLink.國泰人壽外幣帳戶 };
		
		for(int i=0; i<textInfo.length; i++) {
			String[] info = textInfo[i].split(":");
			String title = info[0];
			String desc = info[1];
			LifeLink link = linkInfo[i];
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			
			cards.setCTextType("5");
			cards.setCTitle(title);
			
			if(StringUtils.isNotBlank(desc)) {
				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
				CTextItem cText1 = vo.new CTextItem();
				cText1.setCText(desc);
				cTextList.add(cText1.getCTexts());
				cards.setCTexts(cTextList);
			}
			
			//link
			cards.setCLinkType(1);
			cards.setCLinkList(this.getCLinkList(vo, link));
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		return cardList;
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI7_3_2(BotMessageVO vo, Map<String, Object> e02Result) {
		List<Map<String, Object>> cardList = new ArrayList<>();

		CardItem cards = vo.new CardItem();
		cards.setCName("保單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<>();

		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("保單名稱");
		cImageText.setCImageText(MapUtils.getString(e02Result, E02.PROD_NAME.name()));

		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("保單號碼");
		// 20181015 by Todd, keep保單號碼
		// cImageText2.setCImageText(MapUtils.getString(e02Result, E02.POLICY_NO.name()));
		String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
		cImageText2.setCImageText(policyNo);

		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());

		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());

		//link
		cards.setCLinkType(1);
		// 20181015 by Todd, keep保單號碼
		// LifeLink[] link1s = { LifeLink.查保單業務員 };
		LifeLink[] link1s = { LifeLink.查保單業務員(policyNo) };
		cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));

		log.info("cards=" + new JSONObject(cards).toString());

		cardList.add(cards.getCards());
		return cardList;
	}

    @Override
    // 20181015 by Todd, keep保單號碼
    // public List<Map<String, Object>> getCardsForI7_4_2(BotMessageVO vo) {
	public List<Map<String, Object>> getCardsForI7_4_2(BotMessageVO vo, Map<String, Object> e02Result) {

    	String policyNo = MapUtils.getString(e02Result, E02.POLICY_NO.name());
    	
        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        CardItem cards2 = vo.new CardItem();
        cards2.setCWidth(BotMessageVO.CWIDTH_200);
        cards2.setCTextType("5");
        cards2.setCTitle("信用卡繳費");
        cards2.setCLinkType(1);
        // LifeLink[] links2 = {LifeLink.雲端繳款單, LifeLink.信用卡繳費MyBill, LifeLink.國泰人壽線上櫃檯, LifeLink.國泰人壽APP};
        LifeLink[] links2 = {LifeLink.雲端繳款單(policyNo), LifeLink.信用卡繳費MyBill, LifeLink.國泰人壽線上櫃檯, LifeLink.國泰人壽APP};
        cards2.setCLinkList(this.getCLinkList(vo, Arrays.asList(links2)));

        CardItem cards3 = vo.new CardItem();
        cards3.setCWidth(BotMessageVO.CWIDTH_200);
        cards3.setCTextType("5");
        cards3.setCTitle("本人帳戶繳費");
        cards3.setCLinkType(1);
        // LifeLink[] links3 = {LifeLink.雲端繳款單, LifeLink.國泰人壽APP, LifeLink.網銀轉帳繳費MyBank, LifeLink.國泰世華ATM};
        LifeLink[] links3 = {LifeLink.雲端繳款單(policyNo), LifeLink.國泰人壽APP, LifeLink.網銀轉帳繳費MyBank, LifeLink.國泰世華ATM};
        cards3.setCLinkList(this.getCLinkList(vo, Arrays.asList(links3)));

        CardItem cards4 = vo.new CardItem();
        cards4.setCWidth(BotMessageVO.CWIDTH_200);
        cards4.setCTextType("5");
        cards4.setCTitle("現金繳費");
        cards4.setCLinkType(1);
        // LifeLink[] links4 = {LifeLink.雲端繳款單, LifeLink.國泰人壽線上櫃檯, LifeLink.國泰人壽服務櫃台};
        LifeLink[] links4 = {LifeLink.雲端繳款單(policyNo), LifeLink.國泰人壽線上櫃檯, LifeLink.國泰人壽服務櫃台};
        cards4.setCLinkList(this.getCLinkList(vo, Arrays.asList(links4)));

        cardList.add(cards2.getCards());
        cardList.add(cards3.getCards());
        cardList.add(cards4.getCards());

        return cardList;
    }

	@Override
	public List<Map<String, Object>> getCardsForI8_1_2(BotMessageVO vo) {
		/*
		  carousel 文字牌卡
		    透過服務中心辦理
			保單所屬業務人員
			郵寄辦理─申請書寄件地址
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		String[] textInfo = {"到服務據點辦理: ", "找業務員協助: ", "自行郵寄辦理: "};
		LifeLink[] linkInfo = { LifeLink.到服務據點辦理自動扣款, LifeLink.保單所屬業務人員, LifeLink.自行郵寄辦理自動扣款 };
		
		for(int i=0; i<textInfo.length; i++) {
			String[] info = textInfo[i].split(":");
			String title = info[0];
			String desc = info[1];
			LifeLink link = linkInfo[i];
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			
			cards.setCTextType("5");
			cards.setCTitle(title);
			
			if(StringUtils.isNotBlank(desc)) {
				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
				CTextItem cText1 = vo.new CTextItem();
				cText1.setCText(desc);
				cTextList.add(cText1.getCTexts());
				cards.setCTexts(cTextList);
			}
			
			//link
			cards.setCLinkType(1);
			List<Map<String, Object>> cLinkList = this.getCLinkList(vo, link);
			cards.setCLinkList(cLinkList);
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI8_1_3_8(BotMessageVO vo) {
		/*
		 * 106 台北市大安區仁愛路四段296號12樓－保費部轉帳服務科收
		 */
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCName("地址");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		cards.setCTextType("5");
		cards.setCTitle("地址");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCText("106 台北市大安區仁愛路四段296號12樓－保費部轉帳服務科收");
		
		cTextList.add(cText1.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		
		return cardList;
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI8_1_4_1(BotMessageVO vo) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		String[] data_recipient = { "世界服務中心", "桃園服務中心", "育仁服務中心", "逢甲服務中心", "高雄服務中心" };
		String[] data_image = { "service01.jpg", "service02.jpg", "service03.jpg", "service04.jpg", "service05.jpg" };
		String[] data_add = { 
				"105 台北市松山區南京東路四段１２６號４樓", "330 桃園市桃園區中山路８４５號４樓", "404 台中市北區進化路５８１號２樓", 
				"700 台南市中西區西門路一段４９６號４樓", "801 高雄市前金區中華三路１４６號３樓Ａ室" };
		
		for(int i=0 ; i < data_add.length ; i++) {
			
			CardItem cards = vo.new CardItem();//創一個牌卡物件
			//cards.setCName("地址");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			
			CImageDataItem cImageData = vo.new CImageDataItem();//設定牌卡圖片
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl(data_image[i]);
			cards.setCImageData(cImageData.getCImageDatas());
			cards.setCTextType("3");	//文字與邊框的間距
			
			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();//設定牌卡圖片區塊的文字
			CTextItem cText1 = vo.new CTextItem();
			cText1.setCLabel("收件人");
			cText1.setCText(data_recipient[i]);
			cTextList.add(cText1.getCTexts());
			
			CTextItem cText2 = vo.new CTextItem();
			cText2.setCLabel("收件地址");
			cText2.setCText(data_add[i]);
			cTextList.add(cText2.getCTexts());
			
			cards.setCTexts(cTextList);
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		/*List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();//設定牌卡圖片區塊的文字
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("收件地址");
		cText1.setCText("106 台北市大安區仁愛路四段296號12樓");
		cTextList.add(cText1.getCTexts());
				
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("收件人");
		cText2.setCText("保費部轉帳服務科 收");
		cTextList.add(cText2.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());*/
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI8_1_3_2(BotMessageVO vo) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		//保險費自動轉帳付款授權書PDF
		LifeLink links[] = { LifeLink.下載保險費自動轉帳付款授權書PDF, LifeLink.查看郵件地址 };
		Map<String, Object> pdf1 = this.getCardForDownloadPDF(vo, "帳戶扣款授權", Arrays.asList(links));
		//信用卡繳交保費付款授權書PDF
		LifeLink[] links2 = { LifeLink.下載信用卡繳交保費付款授權書PDF, LifeLink.查看郵件地址 };
		Map<String, Object> pdf2 = this.getCardForDownloadPDF(vo, "信用卡扣款授權", Arrays.asList(links2));
		
		cardList.add(pdf1);
		cardList.add(pdf2);
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI8_1_3_4(BotMessageVO vo) {
		//信用卡繳交保費付款授權書PDF
		//(投資型保險限國泰世華銀行信用卡)
		LifeLink[] links = { LifeLink.下載信用卡繳交保費付款授權書PDF };
		return this.getCardsForDownloadPDF(vo, "信用卡繳交保費付款授權書", Arrays.asList(links));		
	}

	@Override
	public List<Map<String, Object>> getCardsForI8_1_3_6(BotMessageVO vo) {
		//廣告回郵信封PDF
		LifeLink[] links = { LifeLink.下載廣告回郵信封PDF, LifeLink.查看郵件地址 };		
		return this.getCardsForDownloadPDF(vo, "回郵信封", Arrays.asList(links));		
	}

    @Override
    public Map<String, Object> getCardForDownloadPDF(BotMessageVO vo, String imageText, List<LifeLink> links) {
		//操作流程圖PDF
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB06);
		cImageData.setCImageTextType(3);
		
		List<Map<String,Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageText(imageText);
		cImageTextList.add(cImageText.getCImageTexts());
		cImageData.setCImageTexts(cImageTextList);
		
		cards.setCImageData(cImageData.getCImageDatas());
		
		//link
		cards.setCLinkType(1);
		List<Map<String, Object>> cLinkList = this.getCLinkList(vo, links);
		cards.setCLinkList(cLinkList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		return cards.getCards();
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI19_1_1_3(BotMessageVO vo) {
		//操作流程圖PDF
		List<LifeLink> links = new ArrayList<>();
		links.add(LifeLink.下載操作流程圖PDF_會員網站);
		return this.getCardsForDownloadPDF(vo, "操作流程圖PDF", links);
	}

	@Override
	public List<Map<String, Object>> getCardsForI19_1_2_3(BotMessageVO vo) {
		//操作流程圖PDF
		List<LifeLink> links = new ArrayList<>();
		links.add(LifeLink.下載操作流程圖PDF_會員網站);
		return this.getCardsForDownloadPDF(vo, "操作流程圖PDF", links);
	}

	@Override
	public List<Map<String, Object>> getCardsForI20_1_1_2(BotMessageVO vo, List<Object> h02Result) {
		/*
			［服務中心名稱］
			［地址］
			［電話］
		
			 link:
				查google map（開啟google map搜尋頁）
				撥號
		*/
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		for (Object obj : h02Result) {
			Map<String, Object> h02 = (Map<String, Object>) obj;
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_250);
			
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CB);
			cImageData.setCImageUrl("card18.png");
			
			cards.setCImageData(cImageData.getCImageDatas());
			
			//多標題牌卡
			cards.setCTextType("3");
			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
			CTextItem cText1 = vo.new CTextItem();
			cText1.setCLabel("服務中心名稱");
			cText1.setCText(MapUtils.getString(h02, H02.DIV_SHORT_NAME.name()));
			CTextItem cText2 = vo.new CTextItem();
			cText2.setCLabel("地址");
			cText2.setCText(MapUtils.getString(h02, H02.DIV_ADDRESS.name()));
			CTextItem cText3 = vo.new CTextItem();
			cText3.setCLabel("電話");
			cText3.setCText(MapUtils.getString(h02, H02.DIV_TEL1.name()));
			
			cTextList.add(cText1.getCTexts());
			cTextList.add(cText2.getCTexts());
			cTextList.add(cText3.getCTexts());
			
			cards.setCTexts(cTextList);
			
			//link
			String addr = MapUtils.getString(h02, H02.DIV_ADDRESS.name());
			String tel = MapUtils.getString(h02, H02.DIV_TEL1.name());
			
			cards.setCLinkType(1);
			LifeLink[] links = {LifeLink.查google地圖(addr), LifeLink.撥號(tel)};
			List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
			cards.setCLinkList(cLinkList);
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI21_1_2(BotMessageVO vo, Map<String, Object> d16Result) {
		/*
		    業務人員：[服務人員姓名]
			辦公室電話： [服務人員聯絡電話]
			Email信箱：[服務人員email信箱]
			
			link: 
				撥號
				寄信
				查所有業務員(S042)
		 */

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		String CLC_NAME = MapUtils.getString(d16Result, D16.CLC_NAME.name());
		String DIV_TEL = MapUtils.getString(d16Result, D16.DIV_TEL.name());
		String CLC_EMAIL = MapUtils.getString(d16Result, D16.CLC_EMAIL.name());
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB);
		cImageData.setCImageUrl("card20.png");
		
		cards.setCImageData(cImageData.getCImageDatas());
		
		//多標題牌卡
		cards.setCTextType("3");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("業務人員");
		cText1.setCText(StringUtils.isEmpty(CLC_NAME) ? "--" : CLC_NAME);
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("辦公室電話");
		cText2.setCText(StringUtils.isEmpty(DIV_TEL) ? "--" : DIV_TEL);
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("Email信箱");
		cText3.setCText(StringUtils.isEmpty(CLC_EMAIL) ? "--" : CLC_EMAIL);
		
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		
		cards.setCTexts(cTextList);
		
		//link
		cards.setCLinkType(1);
		LifeLink[] links = {LifeLink.撥號(DIV_TEL), LifeLink.寄信(CLC_EMAIL), LifeLink.查所有業務員};
		List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
		cards.setCLinkList(cLinkList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		
		return cardList;
	}
	
	/**
	 * DB查詢回覆訊息並覆蓋參數
	 * @param Role
	 * @param NodeID
	 * @param textId
	 * @param paramMap
	 * @return
	 */
	public String getReplaceTextByParamMap(String Role, String NodeID, String textId, Map paramMap) {
		String returnText = botTpiReplyTextService.getReplyText(Role, NodeID, textId);
		returnText = replaceTextByParamMap(returnText, paramMap);
		return returnText;
	}

	@Override
	public List<Map<String, Object>> getCardsForI15_1_3(BotMessageVO vo, boolean isLogin, Map<String,Object> d14Result) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		//操作流程圖PDF
		LifeLink links[] = { LifeLink.下載操作流程圖PDF_711 };
		Map<String, Object> pdf1 = this.getCardForDownloadPDF(vo, "操作流程圖PDF", Arrays.asList(links));
		cardList.add(pdf1);
		if (isLogin) {
			String downloadFileFullPath = MapUtils.getString(d14Result, D14.downloadFileFullPath.name());
			String downloadFileName = MapUtils.getString(d14Result, D14.downloadFileName.name());
			if (StringUtils.isBlank(downloadFileFullPath) || StringUtils.isBlank(downloadFileName)) {
				log.error("I15自繳單下載參數錯誤:" + d14Result);
			} else {
				//自繳單
				LifeLink[] links2 = { LifeLink.downloadFileLink(downloadFileFullPath, downloadFileName, "開啟") };
				Map<String, Object> pdf2 = this.getCardForDownloadPDF(vo, "自繳單", Arrays.asList(links2));
				cardList.add(pdf2);
			}
		}
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI15_1_8(BotMessageVO vo) {
		//generic template	據點牌卡	subview google map搜尋頁
		//7-11
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem card = vo.new CardItem();
		card.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB);
		cImageData.setCImageUrl("card16.png");
		card.setCImageData(cImageData.getCImageDatas());
		
		card.setCLinkType(1);
		LifeLink[] links = { LifeLink.goole_map_711 };
		card.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));
		cardList.add(card.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI16_1_3(BotMessageVO vo, boolean isLogin, Map<String,Object> d14Result) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		//操作流程圖PDF
		LifeLink links[] = { LifeLink.下載操作流程圖PDF_全家 };
		Map<String, Object> pdf1 = this.getCardForDownloadPDF(vo, "操作流程圖PDF", Arrays.asList(links));
		cardList.add(pdf1);
		
		if (isLogin) {
			String downloadFileFullPath = MapUtils.getString(d14Result, D14.downloadFileFullPath.name());
			String downloadFileName = MapUtils.getString(d14Result, D14.downloadFileName.name());
			if (StringUtils.isBlank(downloadFileFullPath) || StringUtils.isBlank(downloadFileName)) {
				log.error("I16自繳單下載參數錯誤:" + d14Result);
			} else {
				//自繳單
				LifeLink[] links2 = { LifeLink.downloadFileLink(downloadFileFullPath, downloadFileName, "開啟") };
				Map<String, Object> pdf2 = this.getCardForDownloadPDF(vo, "自繳單", Arrays.asList(links2));
				cardList.add(pdf2);
			}
		}
		
		return cardList;
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI16_1_8(BotMessageVO vo) {
		//generic template	據點牌卡	subview google map搜尋頁 
		//全家
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem card = vo.new CardItem();
		card.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB);
		cImageData.setCImageUrl("card09.png");
		card.setCImageData(cImageData.getCImageDatas());
		
		card.setCLinkType(1);
		LifeLink[] links = {LifeLink.goole_map_全家};
		card.setCLinkList(this.getCLinkList(vo, Arrays.asList(links)));
		cardList.add(card.getCards());
		return cardList;
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI11_1_1_1_3(BotMessageVO vo) {
		//操作流程圖PDF
		List<LifeLink> links = new ArrayList<>();
		links.add(LifeLink.下載操作流程圖PDF_ATM);
		return this.getCardsForDownloadPDF(vo, "操作流程圖PDF", links);
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI11_1_2_1_3(BotMessageVO vo) {
		//操作流程圖PDF
		List<LifeLink> links = new ArrayList<>();
		links.add(LifeLink.下載操作流程圖PDF_MYATM);
		return this.getCardsForDownloadPDF(vo, "操作流程圖PDF", links);
	}
	
	@Override
	public List<Map<String, Object>> getCardsForI12_1_3(BotMessageVO vo) {
		//操作流程圖PDF
		List<LifeLink> links = new ArrayList<>();
		links.add(LifeLink.下載操作流程圖PDF_信用卡繳費MYBILL);
		return this.getCardsForDownloadPDF(vo, "操作流程圖PDF", links);
	}
	
	@Override
	public List<Map<String, Object>> getCardForI13_1_3(BotMessageVO vo) {
		//操作流程圖PDF
		List<LifeLink> links = new ArrayList<>();
		links.add(LifeLink.下載操作流程圖PDF_MYBANK);
		return this.getCardsForDownloadPDF(vo, "操作流程圖PDF", links);
	}

    @Override
	public List<Map<String, Object>> getCLinkList(BotMessageVO vo, List<LifeLink> links) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
		for (LifeLink link : links)
			cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());
		return cLinkContentList;
	}

    @Override
    public List<Map<String, Object>> getCLinkList(BotMessageVO vo, LifeLink link) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
		cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());
		return cLinkContentList;
	}
	
	
	@Override
	public List<Map<String, Object>> getCardsForI22_1_1(BotMessageVO vo) {
		/*
		           保單停效辦理復效的方式如下：
			link:
				透過服務中心辦理
				郵寄辦理─申請書寄件地址
		*/
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		/*20181108優化
		String[] textInfo = {"到服務據點辦理: ", "自行郵寄辦理: " };			
		LifeLink[] linkInfo = { LifeLink.到服務據點辦理保單復效, LifeLink.自行郵寄辦理保單復效 };
		*/	
		String[] textInfo = {"服務據點:親自到服務據點辦理 ", "郵寄辦理:填寫完寄到服務據點 " , "要保人登入會員網站:需先開通網路交易服務 ", "保單業務員:業務員會幫你送件喔"};
		LifeLink[] linkInfo = { LifeLink.到服務據點辦理保單復效, LifeLink.自行郵寄辦理保單復效, LifeLink.登入會員網站辦理保單復效, LifeLink.保單業務員 };
		
		for(int i=0; i<textInfo.length; i++) {
			String[] info = textInfo[i].split(":");
			String title = info[0];
			String desc = info[1];
			LifeLink link = linkInfo[i];
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			
			cards.setCTextType("5");
			cards.setCTitle(title);
			
			if(StringUtils.isNotBlank(desc)) {
				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
				CTextItem cText1 = vo.new CTextItem();
				cText1.setCText(desc);
				cTextList.add(cText1.getCTexts());
				cards.setCTexts(cTextList);
			}
			
			//link
			cards.setCLinkType(1);
			cards.setCLinkList(this.getCLinkList(vo, link));
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardsForI22_1_1_2_3(BotMessageVO vo) {
		//（復效申請書）
		List<LifeLink> links = new ArrayList<>();
		links.add(LifeLink.下載復效申請書);
		return this.getCardsForDownloadPDF(vo, "復效申請書", links);
	}
	
	//20181108優化
	@Override
	public List<Map<String, Object>> getCardsForI22_1_1_2_4(BotMessageVO vo) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		String[] data_recipient = { "世界服務中心", "桃園服務中心", "育仁服務中心", "逢甲服務中心", "高雄服務中心" };
		String[] data_image = { "service01.jpg", "service02.jpg", "service03.jpg", "service04.jpg", "service05.jpg" };
		String[] data_add = { 
				"105 台北市松山區南京東路四段１２６號４樓", "330 桃園市桃園區中山路８４５號４樓", "404 台中市北區進化路５８１號２樓", 
				"700 台南市中西區西門路一段４９６號４樓", "801 高雄市前金區中華三路１４６號３樓Ａ室" };
		
		for(int i=0 ; i < data_add.length ; i++) {
			
			CardItem cards = vo.new CardItem();//創一個牌卡物件
			//cards.setCName("地址");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			
			CImageDataItem cImageData = vo.new CImageDataItem();//設定牌卡圖片
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl(data_image[i]);
			cards.setCImageData(cImageData.getCImageDatas());
			cards.setCTextType("3");	//文字與邊框的間距
			
			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();//設定牌卡圖片區塊的文字
			CTextItem cText1 = vo.new CTextItem();
			cText1.setCLabel("收件人");
			cText1.setCText(data_recipient[i]);
			cTextList.add(cText1.getCTexts());
			
			CTextItem cText2 = vo.new CTextItem();
			cText2.setCLabel("收件地址");
			cText2.setCText(data_add[i]);
			cTextList.add(cText2.getCTexts());
			
			cards.setCTexts(cTextList);
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		return cardList;
	}	
	
	@Override
	public List<Map<String, Object>> getCardsForI23_1_2(BotMessageVO vo) {
		return this.getCardsForI2_12_2_4(vo);
	}
	
	@Override
	public List<Map<String, Object>> getCardForI17_1_2(BotMessageVO vo) {
		
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("國泰人壽郵局專用劃撥帳號");
		cImageText.setCImageText("00041496");
		CImageTextItem cImageText2 = vo.new CImageTextItem();
		cImageText2.setCImageTextTitle("戶名");
		cImageText2.setCImageText("國泰人壽保險股份有限公司");
		cImageTexts.add(cImageText.getCImageTexts());
		cImageTexts.add(cImageText2.getCImageTexts());
		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI17_1_3(BotMessageVO vo) {
		
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCName("郵局劃撥單");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		
		CImageDataItem cImageData1 = vo.new CImageDataItem();
		cImageData1.setCImage(BotMessageVO.IMG_CB);
		cImageData1.setCImageUrl("card11.png");
		
		cards.setCImageData(cImageData1.getCImageDatas());	
		
		cards.setCLinkType(1);
		LifeLink[] links = { LifeLink.劃撥單填寫範本 };
		List<Map<String, Object>> cLinkList = this.getCLinkList(vo, Arrays.asList(links));
		cards.setCLinkList(cLinkList);
		
		cardList.add(cards.getCards());
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI18_1_2(BotMessageVO vo) {
		// 國泰世華銀行 仁愛分行		
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("匯款行庫");
		cText1.setCText("國泰世華銀行");
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("分行");
		cText2.setCText("仁愛分行");
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("通匯金融代號");
		cText3.setCText("0132011");
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("轉匯帳號");
		cText4.setCText("86880+匯款人身分證後9碼");
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("戶名");
		cText5.setCText("國泰人壽保險股份有限公司");
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI18_1_3(BotMessageVO vo) {
		// 第一銀行營業部
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("匯款行庫");
		cText1.setCText("第一銀行");
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("分行");
		cText2.setCText("營業部");
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("通匯金融代號");
		cText3.setCText("0070937");
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("轉匯帳號");
		cText4.setCText("0022800+匯款人身分證後9碼");
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("戶名");
		cText5.setCText("國泰人壽保險股份有限公司");
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI18_1_4(BotMessageVO vo) {
		// 華南銀行營業部
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("匯款行庫");
		cText1.setCText("華南銀行");
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("分行");
		cText2.setCText("營業部");
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("通匯金融代號");
		cText3.setCText("0081005");
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("轉匯帳號");
		cText4.setCText("98369+匯款人身分證後9碼");
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("戶名");
		cText5.setCText("國泰人壽保險股份有限公司");
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI18_1_5(BotMessageVO vo) {
		// 合作金庫營業部
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("匯款行庫");
		cText1.setCText("合作金庫");
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("分行");
		cText2.setCText("營業部");
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("通匯金融代號");
		cText3.setCText("0060567");
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("轉匯帳號");
		cText4.setCText("8993+匯款人身分證後9碼");
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("戶名");
		cText5.setCText("國泰人壽保險股份有限公司");
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI18_1_6(BotMessageVO vo) {
		// 彰化銀行北門分行
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("匯款行庫");
		cText1.setCText("彰化銀行");
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("分行");
		cText2.setCText("北門分行");
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("通匯金融代號");
		cText3.setCText("0095005");
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("轉匯帳號");
		cText4.setCText("5005-03-00053-900");
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("戶名");
		cText5.setCText("國泰人壽保險股份有限公司");
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI18_1_7(BotMessageVO vo) {
		// 台北北門郵局
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		CardItem cards = vo.new CardItem();
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCLabel("匯款行庫");
		cText1.setCText("中華郵政股份有限公司");
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCLabel("分行");
		cText2.setCText("台北北門郵局");
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCLabel("通匯金融代號");
		cText3.setCText("7000010");
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCLabel("轉匯帳號");
		cText4.setCText("18888575(劃撥)");
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCLabel("戶名");
		cText5.setCText("國泰人壽保險股份有限公司");
		cTextList.add(cText1.getCTexts());
		cTextList.add(cText2.getCTexts());
		cTextList.add(cText3.getCTexts());
		cTextList.add(cText4.getCTexts());
		cTextList.add(cText5.getCTexts());
		
		cards.setCTexts(cTextList);
		
		log.info("cards=" + new JSONObject(cards).toString());
		
		cardList.add(cards.getCards());
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI24_1_2(BotMessageVO vo) {
		// 國泰世華國外部
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		String[] datas = {
			"國泰世華銀行(013);國外部;UWCBTWTP;002080486068;國泰人壽保險股份有限公司/ Cathay Life Insurance Co., Ltd.",
			"中國信託(822);市府分行;CTCBTWTP;543131010141;國泰人壽保險股份有限公司/ Cathay Life Insurance Co., Ltd.",
			"兆豐銀行(017);忠孝分行;ICBCTWTP;00553050458;國泰人壽保險股份有限公司/ Cathay Life Insurance Co., Ltd."
		};
		
		for (String s : datas) {
			String[] data = s.split(";");
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_250);
			
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND);
			cImageData.setCImageTextType(1);
			
			List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
			CImageTextItem cImageText = vo.new CImageTextItem();
			cImageText.setCImageTextTitle("匯款行庫(代碼)");
			cImageText.setCImageText(data[0]);
			CImageTextItem cImageText2 = vo.new CImageTextItem();
			cImageText2.setCImageTextTitle("分行");
			cImageText2.setCImageText(data[1]);
			CImageTextItem cImageText3 = vo.new CImageTextItem();
			cImageText3.setCImageTextTitle("SWIFT/ BIC國際代碼");
			cImageText3.setCImageText(data[2]);
			CImageTextItem cImageText4 = vo.new CImageTextItem();
			cImageText4.setCImageTextTitle("轉匯帳號");
			cImageText4.setCImageText(data[3]);
			CImageTextItem cImageText5 = vo.new CImageTextItem();
			cImageText5.setCImageTextTitle("戶名");
			cImageText5.setCImageText(data[4]);
			
			cImageTexts.add(cImageText.getCImageTexts());
			cImageTexts.add(cImageText2.getCImageTexts());
			cImageTexts.add(cImageText3.getCImageTexts());
			cImageTexts.add(cImageText4.getCImageTexts());
			cImageTexts.add(cImageText5.getCImageTexts());
			
			cImageData.setCImageTexts(cImageTexts);
			cards.setCImageData(cImageData.getCImageDatas());
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
		}
		
		return cardList;
	}

	@Override
	public List<Map<String, Object>> getCardForI0_1_1(BotMessageVO vo) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		// card1
		CardItem cards1 = vo.new CardItem();
		cards1.setCName("");
		cards1.setCWidth(BotMessageVO.CWIDTH_200);
			
		CImageDataItem cImageData1 = vo.new CImageDataItem();
		cImageData1.setCImage(BotMessageVO.IMG_CC);
		cImageData1.setCImageUrl("policy_01.jpg");
		cards1.setCImageData(cImageData1.getCImageDatas());
		
		cards1.setCTextType("5");
		cards1.setCTitle("確認我的繳費狀況");
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText1 = vo.new CTextItem();
		cText1.setCText("這邊可查詢關於保單的繳費狀態。");
		cTextList.add(cText1.getCTexts());
		cards1.setCTexts(cTextList);
		
		cards1.setCLinkType(1);
		LifeLink[] link1s = { LifeLink.最近有沒有要繳的保單, LifeLink.我的保費有扣款嗎(""), LifeLink.保單繳費方式 };
		cards1.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));
		
		
		// card2
		CardItem cards2 = vo.new CardItem();
		cards2.setCName("");
		cards2.setCWidth(BotMessageVO.CWIDTH_200);
			
		CImageDataItem cImageData2 = vo.new CImageDataItem();
		cImageData2.setCImage(BotMessageVO.IMG_CC);
		cImageData2.setCImageUrl("policy_03.jpg");
		cards2.setCImageData(cImageData2.getCImageDatas());
		
		cards2.setCTitle("保險金領取&借款狀況");
		List<Map<String, Object>> cText2List = new ArrayList<Map<String, Object>>();
		CTextItem cText2 = vo.new CTextItem();
		cText2.setCText("這邊可查詢關於年金、滿期金和借款狀態。");
		cText2List.add(cText2.getCTexts());
		cards2.setCTexts(cText2List);
		
  
		cards2.setCLinkType(1);
		// 20190111 by Todd, 常用功能牌卡
		// LifeLink[] link2s = { ABBLink.ABB10007.保單是否可以借款, ABBLink.ABB10001.查詢年金領取狀況, ABBLink.ABB10009.查詢滿期金領取狀況 };
		LifeLink[] link2s = { ABBLink.ABB10007.保單是否可以借款, ABBLink.ABB10014.查詢年金相關內容, ABBLink.ABB10015.查詢滿期金相關內容 };
		cards2.setCLinkList(this.getCLinkList(vo, Arrays.asList(link2s)));
		
		// cards3
		CardItem cards3 = vo.new CardItem();
		cards3.setCName("");
		cards3.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData3 = vo.new CImageDataItem();
		cImageData3.setCImage(BotMessageVO.IMG_CC);
		cImageData3.setCImageUrl("policy_02.jpg");
		cards3.setCImageData(cImageData3.getCImageDatas());
		
		cards3.setCTitle("保單狀況&保障內容");
		List<Map<String, Object>> cText3List = new ArrayList<Map<String, Object>>();
		CTextItem cText3 = vo.new CTextItem();
		cText3.setCText("這邊可查詢保單效力、年期、保額等資訊。");
		cText3List.add(cText3.getCTexts());
		cards3.setCTexts(cText3List);
		
		cards3.setCLinkType(1);
		// LifeLink[] link3s = { AIALink.確認我的保單狀況, AIALink.我的保障內容有哪些 };
		LifeLink[] link3s = { AIALink.確認我的保單狀況, AIALink.我的保障內容有哪些, AIALink.AIA10005.申請契約狀況一覽表 };
		cards3.setCLinkList(this.getCLinkList(vo, Arrays.asList(link3s)));
		
		// cards4
		CardItem cards4 = vo.new CardItem();
		cards4.setCName("");
		cards4.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData4 = vo.new CImageDataItem();
		cImageData4.setCImage(BotMessageVO.IMG_CC);
		cImageData4.setCImageUrl("card-b39.jpg");
		cards4.setCImageData(cImageData4.getCImageDatas());
		
		cards4.setCTitle("推薦商品&旅平險投保");
		List<Map<String, Object>> cText4List = new ArrayList<Map<String, Object>>();
		CTextItem cText4 = vo.new CTextItem();
		cText4.setCText("這邊提供商品簡單介紹及投保管道。");
		cText4List.add(cText4.getCTexts());
		cards4.setCTexts(cText4List);
		
		cards4.setCLinkType(1);
		LifeLink[] link4s = { AIALink.AIA10001.推薦現售商品, AIALink.網路投保專區, AIALink.投保旅平險 };
		cards4.setCLinkList(this.getCLinkList(vo, Arrays.asList(link4s)));
		
		// cards5
		CardItem cards5 = vo.new CardItem();
		cards5.setCName("");
		cards5.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData5 = vo.new CImageDataItem();
		cImageData5.setCImage(BotMessageVO.IMG_CC);
		cImageData5.setCImageUrl("policy_04.jpg");
		cards5.setCImageData(cImageData5.getCImageDatas());
		
		cards5.setCTitle("保單內容變更");
		List<Map<String, Object>> cText5List = new ArrayList<Map<String, Object>>();
		CTextItem cText5 = vo.new CTextItem();
		cText5.setCText("這邊可查詢關於保單變更的辦理方式及試算。");
		cText5List.add(cText5.getCTexts());
		cards5.setCTexts(cText5List);
		
		cards5.setCLinkType(1);
		LifeLink[] link5s = { LifeLink.申請保單復效, ABALink.申請附約變更, ABALink.ABA00004.申請減額繳清 };
		cards5.setCLinkList(this.getCLinkList(vo, Arrays.asList(link5s)));
		
		// cards6
		CardItem cards6 = vo.new CardItem();
		cards6.setCName("");
		cards6.setCWidth(BotMessageVO.CWIDTH_200);
		
		CImageDataItem cImageData6 = vo.new CImageDataItem();
		cImageData6.setCImage(BotMessageVO.IMG_CC);
		cImageData6.setCImageUrl("policy_05.jpg");
		cards6.setCImageData(cImageData6.getCImageDatas());
		
		cards6.setCTitle("保單理賠項目");
		List<Map<String, Object>> cText6List = new ArrayList<Map<String, Object>>();
		CTextItem cText6 = vo.new CTextItem();
		cText6.setCText("這邊提供申請理賠管道。");
		cText6List.add(cText6.getCTexts());
		cards6.setCTexts(cText6List);
		
		cards6.setCLinkType(1);
		LifeLink[] link6s = { AAALink.AAA00001.如何申請理賠 };
		cards6.setCLinkList(this.getCLinkList(vo, Arrays.asList(link6s)));
		
		cardList.add(cards1.getCards());
		cardList.add(cards3.getCards());
		cardList.add(cards2.getCards());
		cardList.add(cards4.getCards());
		cardList.add(cards5.getCards());
		cardList.add(cards6.getCards());
		
		return cardList;
	}

	public String getPremTrnCodeName( Map<String, Object> e02Result ) {
		
		String kind_code = MapUtils.getString(e02Result, E02.KIND_CODE.name());
		String auto_pay_code = MapUtils.getString(e02Result, E02.AUTO_PAY_CODE.name());
		
		String MML_text = null;
		
		if( "2".equals(kind_code) ) {
			MML_text = "扣薪";
		} else if( "3".equals(kind_code) ) {
			MML_text = "轉帳扣款";
		} else if( "4".equals(kind_code) || "5".equals(kind_code) ) {
			MML_text = "信用卡扣款";
		} else if( "6".equals(kind_code) || ( !this.getKindCodeCompare(kind_code) && "Y".equals(auto_pay_code) ) ) {
			MML_text = "自行繳費";
		} else if( "1".equals(kind_code) && "N".equals(auto_pay_code) ) {
			MML_text = "匯款";
		} else if( !this.getKindCodeCompare(kind_code) && "N".equals(auto_pay_code) ) {
			MML_text = "匯款";
		}
		
		return MML_text;
	}
	
	public String getPremTrnCodeName_H01( Map<String, Object> h01ResultMap ) {
		
		String kind_code = MapUtils.getString(h01ResultMap, H01.KIND_CODE.name());
		String auto_pay_code = MapUtils.getString(h01ResultMap, H01.AUTO_PAY_CODE.name());
		
		String MML_text = null;
		
		if( "2".equals(kind_code) ) {
			MML_text = "扣薪";
		} else if( "3".equals(kind_code) ) {
			MML_text = "轉帳扣款";
		} else if( "4".equals(kind_code) || "5".equals(kind_code) ) {
			MML_text = "信用卡扣款";
		} else if( "6".equals(kind_code) || ( !this.getKindCodeCompare(kind_code) && "Y".equals(auto_pay_code) ) ) {
			MML_text = "自行繳費";
		} else if( "1".equals(kind_code) && "N".equals(auto_pay_code) ) {
			MML_text = "匯款";
		} else if( !this.getKindCodeCompare(kind_code) && "N".equals(auto_pay_code) ) {
			MML_text = "匯款";
		}
		
		return MML_text;
	}
	
	protected boolean getKindCodeCompare( String kind_code ) {
		//判斷KIND_CODE 是否 ≠ 1~6，若等於1~6，回傳false
		ArrayList<String> list = new ArrayList<String>(){{ 
	        add("1");
	        add("2");
	        add("3");
	        add("4");
	        add("5");
	        add("6");
	    }};
		
		if( list.indexOf(kind_code) != -1 ) {
			return true;	//不等於1~6
		} else {
			return false;	//等於1~6
		}
				
	}

	@Override
	public BotMessageVO getAutoCompleteBotMessageVO(List<AllAutoComplete> dataList) {
		BotMessageVO botMsgVO = initBotMessage(null, null, null);

		List<Map<String, Object>> linkList = new ArrayList<>();
		LinkListItem link;
		ContentItem content = botMsgVO.new ContentItem();

		content.setType(4);
		content.setWidget("autoComplete");
		content.setLType(BotMessageVO.LTYPE_AUTO_COMPLETE);

		for (AllAutoComplete allAutoComplete : dataList) {
			link = botMsgVO.new LinkListItem();
			link.setLAction(2);
			link.setLAlt(allAutoComplete.getHiddenString());
			link.setLText(allAutoComplete.getDisplayString());
			linkList.add(link.getLinkList());
		}
		content.setLinkList(linkList);

		List<Map<String, Object>> contentList = new ArrayList<>();
		contentList.add(content.getContent());
		botMsgVO.setContent(contentList);
		return botMsgVO;
	}
	
}