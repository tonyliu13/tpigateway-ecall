package com.tp.tpigateway.service.common.impl;

import java.sql.Timestamp;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.BotChatflowMappingDao;
import com.tp.tpigateway.dao.common.BotTpiCallLogDao;
import com.tp.tpigateway.model.common.BotRequestAndResponse;
import com.tp.tpigateway.model.common.BotTpiCallLog;
import com.tp.tpigateway.service.common.BotChatflowMappingService;
import com.tp.tpigateway.service.common.BotTpiCallLogService;

/**
 * 呼叫API log紀錄服務實作
 */
@Service("botChatflowMappingService")
@Transactional
public class BotChatflowMappingServiceImpl implements BotChatflowMappingService {

	@Autowired
	private BotChatflowMappingDao botChatflowMappingDao;
	
	/*
	public void save(BotTpiCallLog log) {
		botTpiCallLogDao.save(log);
	}
	*/
	
	@Override
	public Map<String, String> getChatflowMapping() {
		return botChatflowMappingDao.getChatflowMapping();
	}
}
