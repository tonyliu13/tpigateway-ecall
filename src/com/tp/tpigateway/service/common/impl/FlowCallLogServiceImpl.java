package com.tp.tpigateway.service.common.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.FlowCallLogDao;
import com.tp.tpigateway.exception.RRException;
import com.tp.tpigateway.model.common.FlowCallLog;
import com.tp.tpigateway.service.common.FlowCallLogService;
import com.tp.tpigateway.util.DataUtils;
import com.tp.tpigateway.util.DateUtil;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.TPIStringUtil;
import com.tp.tpigateway.util.exception.StatusCode;
import com.tp.tpigateway.util.exception.StatusType;

@Service("flowCallLogService")
@Transactional
public class FlowCallLogServiceImpl implements FlowCallLogService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	// @Autowired
	// private FlowTpiCallLogRepository flowTpiCallLogRepository;
	@Autowired
	private FlowCallLogDao flowCallLogDao;

	private static final StatusCode FLOW_CALL_LOG_NOT_FOUND = new StatusCode(StatusType.FLOW_CALL_LOG, "0001", "The flowCallLog {0} is not found.");

	@Override
	public String add(FlowCallLog flowCallLog) {
		handleFlowCallLogData(flowCallLog);

		Date nowDateTime = DateUtil.getNow();

		String logId = TPIStringUtil.getUUID();
		flowCallLog.setLogId(logId);
		flowCallLog.setKeyinTime(nowDateTime);
		flowCallLog.setKeyinUpdTime(nowDateTime);
		// flowTpiCallLogRepository.save(flowCallLog);
		flowCallLogDao.save(flowCallLog);
		return flowCallLog.getLogId();
	}

	@Override
	public void update(FlowCallLog flowCallLog) {
		// FlowCallLog oldFlowCallLog = flowTpiCallLogRepository.findBySessionIdAndMsgId(flowCallLog.getSessionId(), flowCallLog.getMsgId());
		FlowCallLog oldFlowCallLog = flowCallLogDao.findBySessionIdAndMsgId(flowCallLog.getSessionId(), flowCallLog.getMsgId());
		if (oldFlowCallLog == null) {
			throw new RRException(FLOW_CALL_LOG_NOT_FOUND, flowCallLog.getLogId());
		}

		update(oldFlowCallLog, flowCallLog);
	}

	private void update(FlowCallLog oldFlowCallLog, FlowCallLog flowCallLog) {
		handleFlowCallLogData(flowCallLog);

		BeanUtils.copyProperties(flowCallLog, oldFlowCallLog, DataUtils.getNullPropertyNames(flowCallLog));

		Date nowDateTime = DateUtil.getNow();
		oldFlowCallLog.setKeyinUpdTime(nowDateTime);
		// flowTpiCallLogRepository.save(oldFlowCallLog);
		flowCallLogDao.save(oldFlowCallLog);
	}

	private String obj2Str(Object object) {
		return object == null ? null : String.valueOf(object);
	}

	@SuppressWarnings("unchecked")
    private void handleFlowCallLogData(FlowCallLog flowCallLog) {

		String nluResult = flowCallLog.getNluResult();
		String faqResult = flowCallLog.getFaqResult();

		try {
            if (StringUtils.isNotBlank(nluResult) && StringUtils.contains(nluResult, "{") && StringUtils.contains(nluResult, "}")) {
                Map<String, Object> nluResultMap = JSONUtils.json2map(nluResult);
                List<Map<String, Object>> intents = (List<Map<String, Object>>) nluResultMap.get("intents");
                if (CollectionUtils.isNotEmpty(intents)) {
                    Map<String, Object> intentMap = intents.get(0);
                    String nluScore = obj2Str(intentMap.get("score"));
                    String nluIntent = obj2Str(intentMap.get("intent"));
                    flowCallLog.setNluScore(nluScore);
                    flowCallLog.setNluIntent(nluIntent);
                }
            }

            if (StringUtils.isNotBlank(faqResult) && StringUtils.contains(faqResult, "{") && StringUtils.contains(faqResult, "}")) {
                Map<String, Object> faqResultMap = JSONUtils.json2map(faqResult);
                List<Map<String, Object>> results = (List<Map<String, Object>>) faqResultMap.get("results");
                if (CollectionUtils.isNotEmpty(results)) {
                    Map<String, Object> resultMap = results.get(0);
                    String faqAnswer = JSONUtils.obj2json(resultMap);
                    String faqScore = obj2Str(resultMap.get("score"));
                    flowCallLog.setFaqAnswer(faqAnswer);
                    flowCallLog.setFaqScore(faqScore);
                }
            }

		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@Override
	@Async
	public String saveOrUpdate(FlowCallLog flowCallLog) {
		long startTime = System.currentTimeMillis();
		log.debug("[saveOrUpdate] sessionId=" + flowCallLog.getSessionId() + ", msgId=" + flowCallLog.getMsgId());

		// FlowCallLog oldFlowCallLog = flowTpiCallLogRepository.findBySessionIdAndMsgId(flowCallLog.getSessionId(), flowCallLog.getMsgId());
		FlowCallLog oldFlowCallLog = flowCallLogDao.findBySessionIdAndMsgId(flowCallLog.getSessionId(), flowCallLog.getMsgId());
		if (oldFlowCallLog != null) {
			update(oldFlowCallLog, flowCallLog);
		}
		else {
			add(flowCallLog);
		}

		log.error("FlowCallLogServiceImpl saveOrUpdate process time : " +(System.currentTimeMillis() - startTime) + "ms.");
		return flowCallLog.getLogId();
	}

}
