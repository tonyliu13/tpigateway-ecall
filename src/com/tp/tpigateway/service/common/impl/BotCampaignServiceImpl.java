package com.tp.tpigateway.service.common.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.common.BotCampaignDao;
import com.tp.tpigateway.model.common.BotCampaign;
import com.tp.tpigateway.service.common.BotCampaignService;

@Service("botCampaignService")
@Transactional
public class BotCampaignServiceImpl implements BotCampaignService {

    private static Logger log = LoggerFactory.getLogger(BotCampaignServiceImpl.class);
    
    @Autowired
    private BotCampaignDao botCampaignDao;
    
    @Override
    public List<BotCampaign> findAll() {
        
        log.debug("[findAll]");
        
        List<BotCampaign> result = null;
        
        try {
            result = botCampaignDao.findAll();
        } catch (Exception e) {
            log.error("[findAll] error:" + e.getMessage());
        } finally {
            if (CollectionUtils.isEmpty(result)) {
                result = new ArrayList<BotCampaign>();
            }
        }
        
        return result;
    }
}
