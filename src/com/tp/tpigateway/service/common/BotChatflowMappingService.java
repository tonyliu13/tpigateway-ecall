package com.tp.tpigateway.service.common;

import java.sql.Timestamp;
import java.util.Map;

import com.tp.tpigateway.model.common.BotRequestAndResponse;
import com.tp.tpigateway.model.common.BotTpiCallLog;

/**
 * 呼叫API log紀錄服務介面
 */
public interface BotChatflowMappingService {

	/**
	 * 新增一筆
	 * 
	 * @param log
	 */
	// public void save(BotTpiCallLog log);
	
	/**
	 * 查詢資料，依據chatID 及 insertTime
	 * @param chatID
	 * @param insertTimeStart
	 * @param insertTimeEnd
	 * @return List<BotTpiCallLog>
	 */
	public Map<String, String> getChatflowMapping();
	
}
