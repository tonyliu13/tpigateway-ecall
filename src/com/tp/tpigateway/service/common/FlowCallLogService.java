package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.FlowCallLog;

public interface FlowCallLogService {

	public String add(FlowCallLog flowCallLog);

	public void update(FlowCallLog flowCallLog);

	public String saveOrUpdate(FlowCallLog flowCallLog);
}
