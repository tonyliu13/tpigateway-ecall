package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.CustSessionRec;
import org.springframework.cache.Cache;

public interface CustSessionRecService {

    public Cache getCustSessionRecCache();

    public CustSessionRec findByCustomerIdAndDate(String customerId, String date);

    public CustSessionRec addCustSessionRec(CustSessionRec custSessionRec);

    public CustSessionRec updateCustSessionRec(CustSessionRec custSessionRec);

    public CustSessionRec saveCustSessionRec(CustSessionRec custSessionRec);

}
