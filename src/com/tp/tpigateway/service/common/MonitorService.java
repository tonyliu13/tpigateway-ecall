package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.DailySession;
import com.tp.tpigateway.model.common.SessionStage;
import com.tp.tpigateway.model.common.monitor.ChatLogData;
import com.tp.tpigateway.model.common.monitor.MonitorData;
import com.tp.tpigateway.model.common.monitor.SessionLightData;
import com.tp.tpigateway.model.common.monitor.SessionStatusData;

import java.util.List;

public interface MonitorService {

	public ChatLogData findChatLogBySessionId(String sessionId);

	public ChatLogData findChatLogBySessionIdAndStartDateTime(String sessionId, Long startDateTime);

	public List<MonitorData> findChatLogBySessionIdAndDateTime(String sessionId, long startDateTime, long endDateTime);

	public void saveDailySession(DailySession dailySession);

	public SessionStatusData findSessionStatusData(String agentId);

	public SessionLightData countSessionLight(Integer sessionUpdMin);

	public void updateCache(DailySession dailySession);
	
	public SessionStage findSessionStageBySessionId(String sessionId);
}
