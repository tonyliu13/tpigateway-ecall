package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.DailySession;

public interface CallApiService {

    public void updateCache(String urls, DailySession dailySession);
    
    public void localCache(String key, Object value, int expiredTime);
    public void localCache(String key, Object value, int expiredTime, boolean tpiCache);
    public void removeCache(String key);
    public void removeCache(String key, boolean tpiCache);
    public Object getTpiCache(String key);
    public Object getTpiCache(String key, boolean checkExpiredTime);
    
    /**
     * (非同步)Bot發送資料到Queue中暫存
     * 
     * @param chatID
     * @param key
     * @param value
     * @return
     */
    public boolean sendDataToQueueWithAsync(String chatID, String key, String value);
    
    /**
     * Bot發送資料到Queue中暫存
     * 
     * @param chatID
     * @param key
     * @param value
     * @return
     */
    public boolean sendDataToQueue(String chatID, String key, String value);
    
    /**
     * Bot由Queue中取回資料
     * 
     * @param chatID
     * @param key
     * @return
     */
    public String getDataFromQueue(String chatID, String key);
    
    /**
     * Bot由Queue中移出資料
     * 
     * @param chatID
     * @param key
     * @return
     */
    public boolean completedDataToQueue(String chatID, String key);
}
