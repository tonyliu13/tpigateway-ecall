package com.tp.tpigateway.service.common;

import okhttp3.MediaType;

import java.util.HashMap;
import java.util.Map;

/**
 * OkHttp用基礎服務設定
 */
public class BaseService {

	/**
	 * 取得並設定表頭(form)
	 * 
	 * @return header's map
	 */
	public Map<String, String> getAPIMHeaders() {
		Map<String, String> headers = new HashMap<String, String>();
		//headers.put("x-ibm-client-id", PropUtils.IBM_CLIENT_ID);
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		//headers.put("accept", "application/json");
		return headers;
	}

	/**
	 * 取得並設定表頭(json)
	 *
	 * @return header's map
	 */
	public Map<String, String> getHeaders() {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		return headers;
	}
	
	/**
	 * 取得並設定表身格式(json)
	 * 
	 * @return
	 */
	public MediaType getJsonType() {
		return MediaType.parse("application/json");
	}
}
