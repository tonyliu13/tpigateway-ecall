package com.tp.tpigateway.service.common;

import java.util.List;
import java.util.Map;

public interface ChatHistoryService {

	public List<Object> getMessageIntentList(String sessionId, String msgId);
	
	public List<Object> getSessionMessageList(String sessionId);

	public List<Map<String, String>> getMessagePathNodeList(String sessionId, String msgId);
}
