package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.ContentItem;
import com.tp.tpigateway.model.common.BotMessageVO.LinkListItem;
import com.tp.tpigateway.service.common.impl.CathayLifeBotMessageServiceImpl.Card.CarouselCard;
import com.tp.tpigateway.model.common.LifeLink;

import java.util.List;
import java.util.Map;

public interface CathayLifeBotMessageService {

	public String replaceTextByParamMap(String text, Map<String, String> paramMap);

	public BotMessageVO initBotMessage(String channel, String source, String role);

	public ContentItem getTextContentItem(BotMessageVO vo, String text);

	public ContentItem getTextContentItemWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap);
	
	public void setTextResult(BotMessageVO vo, String text);
	
	public void setNoteResult(BotMessageVO vo, String annoTitle, String annoText);
	
	public void setHNoteResult(BotMessageVO vo, String annoText);
	
	public void setNoteResultWithReplace(BotMessageVO vo, String annoTitle, String annoText, Map<String, String> replaceParams);
	
	public void setHNoteResultWithReplace(BotMessageVO vo, String annoText, Map<String, String> replaceParams);
	
	public void setTextResultWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap);
	
	public void setCardsResult(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList);
	
	public void setPolicyFilterResult(BotMessageVO vo, List<Map<String, Object>> dataList);
	
	public void setListViewResult(BotMessageVO vo, String lvType, List<Map<String, Object>> dataList);
	
	public ContentItem getHNoteContentItem(BotMessageVO vo, String annoText);

	public ContentItem getHNoteContentItemWithReplace(BotMessageVO vo, String annoText, Map<String, String> paramMap);
	
	public ContentItem getNoteContentItem(BotMessageVO vo, String annoTitle, String annoText);

	public ContentItem getNoteContentItemWithReplace(BotMessageVO vo, String annoTitle, String annoText, Map<String, String> paramMap);

	public void setLifeLinkListResult(BotMessageVO vo, List<LifeLink> asList);
	
	public void setLifeLinkListResult(BotMessageVO vo, String linkType, List<LifeLink> links);
	
	public LinkListItem getLinkListItem(BotMessageVO vo, Integer action, String text, String alt, String reply, String url, String lCustomerAction);

	public BotMessageVO getSimpleBotMessageVO(String channel, String source, String role, String text);
	
	public ContentItem getImageContentItem(BotMessageVO vo, String imageUrl);
	
	public void setImageResult(BotMessageVO vo, String string);

	public void setGeolocationResult(BotMessageVO vo);

	public List<Map<String, Object>> getCLinkList(BotMessageVO vo, List<LifeLink> links);

	public List<Map<String, Object>> getCLinkList(BotMessageVO vo, LifeLink link);

	public Map<String, Object> getCardForDownloadPDF(BotMessageVO vo, String imageText, List<LifeLink> links);
	
	public Map<String, Object> getCardForDownloadEDM(BotMessageVO vo, String imageText, List<LifeLink> links);

	@SuppressWarnings("rawtypes")
	public String getReplaceTextByParamMap(String Role, String NodeID, String textId, Map paramMap);
	
	public void setQuickReply(BotMessageVO vo, LifeLink... lifeLinks);
	
	public void setSticker(BotMessageVO vo, String imageName);
	public void setText(BotMessageVO vo, String role, String... textIdsOrTexts);
	public void setNote(BotMessageVO vo, String role, String textId);
	public void setNote(BotMessageVO vo, String role, String title, String textId);
	public void setNote(BotMessageVO vo, String role, String[]... titleAndTextIdArray);
	public void setPDFCard(BotMessageVO vo, LifeLink link, String imageText);
	
	public void setCarouselCards(BotMessageVO vo, CarouselCard... carouselCards);
	public void setCarouselCards(BotMessageVO vo,
			List<Map<String, Object>> cardList, CarouselCard... carouselCards);
	
	public CarouselCard createCarouselCard(String title, String[] texts, LifeLink[] lifeLinks);
	
	public void setCarouselCards(BotMessageVO vo,List<CarouselCard> carouselCards);

}