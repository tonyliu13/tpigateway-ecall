package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.AllAutoComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public interface AllAutoCompleteService {

	final Logger log = LoggerFactory.getLogger(AllAutoCompleteService.class);
			
	public void refreshAllAutoComplete();
	
	public void waittingUpdate();
	
	public List<AllAutoComplete> getAllAutoComplete();

}
