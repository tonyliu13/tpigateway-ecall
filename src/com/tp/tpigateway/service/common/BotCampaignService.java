package com.tp.tpigateway.service.common;

import java.util.List;

import com.tp.tpigateway.model.common.BotCampaign;

public interface BotCampaignService {

    public List<BotCampaign> findAll();
}
