package com.tp.tpigateway.service.common;

import java.util.Map;

import com.tp.tpigateway.model.common.APIResult;
import com.tp.tpigateway.model.common.LifeLink;

/**
 * bot Link 
 */
public interface BotLinkService {

	public LifeLink[] getOptionLinks(String optionID, APIResult apiResult);
	public LifeLink[] getOptionLinks(String optionID, Map data, String optionDataKey);
	public LifeLink[] getReplaceOptionLinks(String optionID, APIResult apiResult, String replayContentdataKey);
	public LifeLink[] getReplaceOptionLinks(String optionID, Map data, String replayContentdataKey, String optionDataKey);
	public LifeLink getLink(String linkID);
	public LifeLink getDynamicLink(String linkID, Map<String, String> paramMap);
}
