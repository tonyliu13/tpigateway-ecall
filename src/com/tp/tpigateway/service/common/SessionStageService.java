package com.tp.tpigateway.service.common;

import com.tp.tpigateway.model.common.SessionStage;
import org.springframework.cache.Cache;

import java.util.Date;
import java.util.List;

public interface SessionStageService {

    public Cache getSessionStageRecCache();

    public SessionStage findSessionStageBySessionId(String sessionId);

    public SessionStage addSessionStage(SessionStage sessionStage) throws Exception;

    public SessionStage saveSessionStage(SessionStage sessionStage) throws Exception;

    public SessionStage updateSessionStage(SessionStage sessionStage);

    public List<SessionStage> findBySessionUpdTimeGreaterThanEqual(Date dateTime);

    public List<SessionStage> findBySessionUpdTimeGreaterThanEqualAndAgentIdIsNullOrderByStageScoreDescSessionUpdTimeAsc(Date date);

}
