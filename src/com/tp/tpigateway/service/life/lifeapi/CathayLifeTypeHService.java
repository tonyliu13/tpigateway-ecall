package com.tp.tpigateway.service.life.lifeapi;

import java.util.List;
import java.util.Map;

/**
 * 中台相關服務介面
 */
public interface CathayLifeTypeHService {
	
	/**
	 * 依情境查詢保單清單
	 * 
	 * @param ID
	 * @param nationCode
	 * @param ScenarioCode
	 * @return
	 * @throws Exception
	 */
	public List<Object> h01(String ID, String nationCode, String ScenarioCode) throws Exception;
	
	/**
	 * 查詢最近服務中心據點
	 * 
	 * @param LONGITUDE
	 * @param LATITUDE
	 * @param DATA_CNT
	 * @return
	 * @throws Exception 
	 */
	public List<Object> h02(String LONGITUDE, String LATITUDE, int DATA_CNT) throws Exception;

	/**
	 * [H03] 保費扣款狀態
	 */
	public Map<String, Object> h03(String POLICY_NO) throws Exception;
	
	public List<Object> h05(List<String> queryPolicyNos) throws Exception;
	
	/**
	 * [H06] 判斷是否為上班時間
	 */
	public boolean h06() throws Exception;

	/**
	 * [H07] 時間加密
	 */
	public String h07() throws Exception;
}
