package com.tp.tpigateway.service.life.lifeapi;

import java.util.Map;

public interface CathayLifeTypeOTSService {

    public Map<String, Object> eCallQueueInfo() throws Exception;
}
