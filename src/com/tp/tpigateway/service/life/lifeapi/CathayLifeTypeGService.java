package com.tp.tpigateway.service.life.lifeapi;

import java.util.Map;

public interface CathayLifeTypeGService {

    /**
     * 查詢客戶是否已經保單驗證
     * 
     * @param ID 身分證字號
     * @param IP
     * @return
     * @throws Exception
     */
    Map<String, Object> g03(String ID, String IP) throws Exception;

    /**
     * OTP保單驗證
     *
     * @param ID 身分證字號
     * @param IP
     * @return
     * @throws Exception
     */
    Map<String, Object> g01(String ID, String IP) throws Exception;

    /**
     * 檢核OTP保單驗證結果
     * 
     * @param ID 身分證字號
     * @param OTP_SNO OTP序號
     * @param OTP_VNO OTP驗證碼(四碼)
     * @return
     * @throws Exception
     */
    Map<String, Object> g02(String ID, String OTP_SNO, String OTP_VNO) throws Exception;

    /**
     * 進行保單驗證(無手機資料)
     *
     * @param ID 身分證字號
     * @param POLICYNO 保單號碼
     * @param TYPE 驗證類型(0:一般壽險   3:旅平險/傷害險   2:團險  1:房貸戶)
     * @return
     * @throws Exception
     */
    Map<String, Object> g04(String ID, String POLICYNO, String TYPE) throws Exception;

    /**
     * 取得網投旅平險訂單編號
     *
     * @param SYS_CODE 系統代號
     * @param START_WITH 訂單編號開頭
     * @return
     * @throws Exception
     */
    Map<String, Object> g05(String SYS_CODE, String START_WITH) throws Exception;

    /**
     * 取得網投旅平險訂單編號
     *
     * @param SYS_CODE 系統代號
     * @param START_WITH 訂單編號開頭
     * @return
     */
    Map<String, Object> g05WithNoException(String SYS_CODE, String START_WITH);

    /**
     * 查詢網投旅平險最新訂單資訊
     *
     * @param ID 會員ID
     * @return
     * @throws Exception
     */
    Map<String, Object> g06(String ID) throws Exception;

    /**
     * 查詢網投旅平險最新訂單資訊
     *
     * @param ID 會員ID
     * @return
     */
    Map<String, Object> g06WithNoException(String ID);

    /**
     * 網投旅平險訂單狀態更新
     *
     * @param paymentJSON 訂單資訊
     * @return
     * @throws Exception
     */
    Map<String, Object> g07(String paymentJSON) throws Exception;

    /**
     * 網投旅平險-發送交易成功通知
     *
     * @param finishInfoJSON 發送通知資訊
     * @return
     * @throws Exception
     */
    Map<String, Object> g08(String finishInfoJSON) throws Exception;

}
