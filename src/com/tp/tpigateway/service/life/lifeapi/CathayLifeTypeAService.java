package com.tp.tpigateway.service.life.lifeapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 保全資訊相關服務介面
 */
public interface CathayLifeTypeAService {

	/**
	 * [A05] 客戶基本資料 (IVR)
	 * 
	 * @param ID
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a05(String ID) throws Exception;
	
	/**
	 * [A04] ID行銷資訊
	 * 
	 * @param ID
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> a04(String ID) throws Exception;
	
	/**
	 * [A04] ID行銷資訊
	 * 
	 * @param ID
	 * @param NATION_CODE
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> a04(String ID, String NATION_CODE) throws Exception;
	
	/**
	 * [A12] 單一保單附約試算終止.縮小
	 * 
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> a12(String POLICY_NO, String APLY_DATE, List<Map<String, Object>> List) throws Exception;
	
	/**
	 * [A21] 查詢ATM借還款記錄
	 * 
	 * @return
	 * @throws Exception 
	 */
	public List<Object> a21(String APC_ID, String S_DATE, String E_DATE) throws Exception;
	
	/**
	 * [A22] 單一保單查詢借還款記錄
	 * 
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> a22(String POLICY_NO, String TYPE) throws Exception;
	
	/**
	 * [A23] 單一保單紅利提領紀錄
	 * 
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> a23(String POLICY_NO , String PROD_ID) throws Exception;
	
	/**
	 * [A24] 單一保單年金提領紀錄
	 * 
	 * @return
	 * @throws Exception 
	 */
	public ArrayList<Map<String, Object>> a24(String POLICY_NO) throws Exception;
	
	/**
	 * [A29] 查詢ATM借還款金額
	 * 
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> a29(String APC_ID) throws Exception;
	
	/**
	 * [A20] ID查詢可借金額
	 * 
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> a20(String APC_ID) throws Exception;
	
	/**
	 * [A25] 單一保費滿期金紀錄
	 * 
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Map<String, Object>> a25(String POLICY_NO) throws Exception;
	
	/**
	 * [A27] 單一保單受益人查詢
	 * 
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Map<String, Object>> a27(String POLICY_NO) throws Exception;
	
	/**
	 * [A17] 保全系統/保全給付試算輸入/C解約試算輸入
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a17(String POLICY_NO) throws Exception;
	
	/**
	 * [A18] 單一保單試算繳清資訊
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a18(String POLICY_NO , String IS_RD_VALD , String INTENT) throws Exception;
	
	/**
	 * [A14] 險別查詢保全主約設定
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a14(String PROD_ID) throws Exception;
	
	/**
	 * [A19] 單一保單試算縮小保額
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a19(String POLICY_NO , String PREM , String PROD_KIND , String INTENT) throws Exception;
	
	/**
	 * [A13] 單一保單試算繳別變更
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a13(String POLICY_NO, String PAY_FREQ, String PROD_KIND, String AFT_PREM, String INTENT) throws Exception;
	public Map<String, Object> a13(String POLICY_NO, String PAY_FREQ, String PROD_KIND, String INTENT) throws Exception;
	
	/**
	 * [A11] 單一保單可附加附約
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a11(String PROD_ID , String INTENT) throws Exception;
	
	/**
	 * [A15] 險別查詢滿期金年金設定
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> a15(String PROD_ID) throws Exception;
	
	/**
	 * [A30] 查詢W001(會員資料)
	 * 
     * @param APC_ID
     * @param NATION_CODE
     * @return
     * @throws Exception
     */
    public Map<String, Object> a30(String APC_ID, String NATION_CODE) throws Exception;
    
    /**
     * [A31] 查詢要保人停效保單
     * 
     * @param APC_ID
     * @return
     * @throws Exception
     */
    public List<Map<String, Object>> a31(String APC_ID) throws Exception;
    
    /**
     * [A32] 給付款未領明細查詢
     * 
     * @param DATA_KIND
     * @param POLICYNO
     * @param ID
     * @param ROLE
     * @return
     * @throws Exception
     */
    public List<Map<String, Object>> a32(String DATA_KIND, String POLICYNO, String ID, String ROLE) throws Exception;
    
    /**
     * [A33] 滿期年金將到期查詢
     * 
     * @param ID
     * @param ROLE
     * @param Period
     * @param MAT_ANTY_CODE
     * @return
     * @throws Exception
     */
    public List<Map<String, Object>> a33(String ID, String[] ROLE, String Period, String MAT_ANTY_CODE) throws Exception;

}
