package com.tp.tpigateway.service.life.lifeapi;

import java.util.ArrayList;
import java.util.Map;

/**
 * 放款相關
 */
public interface CathayLifeTypeSService {

	/**
	 * [s01]ID歸戶房貸資訊<BR/>
	 * 
	 * @param BORROW_ID 借款人ID
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> s01(String BORROW_ID) throws Exception;
	
	/**
	 * [s02]試算單一房貸繳息<BR/>
	 * 
	 * @param LOAN_ID 貸款帳號
	 * @param BORROW_ID 借款人ID
	 * @return
	 * @throws Exception 
	 */
	public ArrayList<Map<String, Object>> s02(String LOAN_ID, String BORROW_ID) throws Exception;
	public ArrayList<Map<String, Object>> s02(String BORROW_ID) throws Exception;
	public ArrayList<Map<String, Object>> s02(String LOAN_ID, String BORROW_ID, Boolean IS_PAID) throws Exception;
	
	
	/**
	 * [s03]單一房貸繳息紀錄<BR/>
	 * 
	 * @param LOAN_ID 貸款帳號
	 * @param BORROW_ID 借款人ID
	 * @return
	 * @throws Exception 
	 */
	public ArrayList<Map<String, Object>> s03(String LOAN_ID, String BORROW_ID) throws Exception;
	public ArrayList<Map<String, Object>> s03(String BORROW_ID) throws Exception;
	
	
	/**
	 * [s04]房貸火險資訊<BR/>
	 * 
	 * @param BORROW_ID 借款人ID
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> s04(String BORROW_ID) throws Exception;
	
	/**
	 * [s06]ID歸戶理財型房貸資訊<BR/>
	 * 
	 * @param BORROW_ID 借款人ID
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> s06(String BORROW_ID) throws Exception;
	
	/**
	 * [s07]ID歸戶房貸業務人員<BR/>
	 * 
	 * @param BORROW_ID 借款人ID
	 * @param LOAN_ID 貸款帳號
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> s07(String BORROW_ID, String LOAN_ID) throws Exception;
	
	/**
	 * [s08]ID歸戶房貸繳息證明<BR/>
	 * 
	 * @param BORROW_ID 借款人ID
	 * @return
	 * @throws Exception 
	 */ 
	public Map<String, Object> s08(String BORROW_ID) throws Exception;
}
