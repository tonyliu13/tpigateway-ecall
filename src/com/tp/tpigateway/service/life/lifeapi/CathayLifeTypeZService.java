package com.tp.tpigateway.service.life.lifeapi;

import java.util.Map;

public interface CathayLifeTypeZService {

    /**
     * [Z01] 取得一次性碼(發OTP)
     *
     * @param trnId 交易代號
     * @param mobileNo    行動電話
     * @param infmId  通知對象身分證字號
     * @param infmName    通知對象姓名
     * @param email   電子郵件地址
     * @return
     * @throws Exception
     */
    public String z01(String trnId, String mobileNo, String infmId, String infmName, String email) throws Exception;
    
    /**
     * [Z02] 驗證一次性密碼(驗證OTP)
     * 
     * @param trnId 交易代號
     * @param s 序號
     * @param otp 密碼
     * @return
     * @throws Exception
     */
    public Map<String, Object> z02(String trnId, String s, String otp) throws Exception;
    
    /**
     * [Z04] 公會資料取回(非同步)
     * 
     * @param ID    使用者ID
     * @param TRANS_NO    訂單編號
     * @param SRC 系統別
     * @return
     * @throws Exception
     */
    public Map<String, Object> z04(String ID, String TRANS_NO, String SRC) throws Exception;
    
    /**
     * [Z05] 公會資料取回(同步)
     * 
     * @param ID    使用者ID
     * @param TRANS_NO    訂單編號
     * @param SRC 系統別
     * @return
     * @throws Exception
     */
    public Map<String, Object> z05(String ID, String TRANS_NO, String SRC) throws Exception;
}
