package com.tp.tpigateway.service.life.lifeapi.impl;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeEService;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("CathayLifeTypeEService")
public class CathayLifeTypeEServiceImpl implements CathayLifeTypeEService {

	private static Logger log = LoggerFactory.getLogger(CathayLifeTypeDServiceImpl.class);

	// 定值FROMB2E1
	private static final String E02_QRY_FRM = "FROMB2E1";
	// 定值ChatBot
	private static final String E02_QRY_ID = "ChatBot";

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> e02(String policyNoQry, String qryFrm, String qryId) throws Exception {
		long costTimeStart = System.currentTimeMillis();
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO_QRY", policyNoQry);
		param.put("QRY_FRM", E02_QRY_FRM);
		param.put("QRY_ID", E02_QRY_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_E02_URL, param, HashMap.class);
		log.info("[e02] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		return result == null ? new HashMap<String, Object>() : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> e01(String ID, String NATION_CODE) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		param.put("NATION_CODE", NATION_CODE);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_E01_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> e07(String POLICY_NO, String APLY_DATE) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("APLY_DATE", APLY_DATE);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_E07_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> e08(String ID, String NATION_CODE, String PROD_KIND, String APC_IN, String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		param.put("NATION_CODE", NATION_CODE);
		param.put("PROD_KIND", PROD_KIND);
		param.put("APC_IN", APC_IN);
		param.put("POLICY_NO", POLICY_NO);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_E08_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

}
