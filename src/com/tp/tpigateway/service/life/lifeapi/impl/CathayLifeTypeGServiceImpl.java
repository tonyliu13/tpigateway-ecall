package com.tp.tpigateway.service.life.lifeapi.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeAService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeGService;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

import edu.emory.mathcs.backport.java.util.Arrays;
import edu.emory.mathcs.backport.java.util.Collections;

/**
 * 官網登入相關服務實作
 */
@SuppressWarnings("unchecked")
@Service("cathayLifeTypeGService")
public class CathayLifeTypeGServiceImpl implements CathayLifeTypeGService {

    private static Logger log = LoggerFactory.getLogger(CathayLifeTypeGServiceImpl.class);

    @Autowired
    private CathayLifeTypeAService cathayLifeTypeAService;

    @Override
    public Map<String, Object> g03(String ID, String IP) throws Exception {
        if (PropUtils.CATHAYLIFE_PASS_G03) {
            Map<String, Object> result = new HashMap<>();
            result.put("STATUS", true);
            return result;
        }

        //20180928 by Todd，暫時調整 for 測試環境房貸戶資料驗證用，不可上版至正式環境
        Map<String, Object> a05Result = null;
        try {
            a05Result = cathayLifeTypeAService.a05(ID);
        } catch (Exception e) {
            /*Map<String, StringBuilder> infoMap = null;
            if (rre.getInfo() != null && rre.getInfo().get("detail") != null) {
            	infoMap = (Map<String, StringBuilder>) rre.getInfo();
            	infoMap.get("detail").append("(A05)");
            }
            throw new RRException(rre.getStatusCode(), infoMap, rre.getArguments());*/
        } finally {
            if (a05Result == null)
                a05Result = new HashMap<String, Object>();
            if (StringUtils.isBlank(MapUtils.getString(a05Result, "NATION_CODE"))) {
                a05Result.put("NATION_CODE", "TW");
            }
        }

        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("NATIONALITY", MapUtils.getString(a05Result, "NATION_CODE"));
        param.put("IP", IP);

        Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G03_URL, param, HashMap.class);
        return result == null ? Collections.emptyMap() : result;
    }

    @Override
    public Map<String, Object> g01(String ID, String IP) throws Exception {
        //20180928 by Todd，暫時調整 for 測試環境房貸戶資料驗證用，不可上版至正式環境
        Map<String, Object> a05Result = null;
        try {
            a05Result = cathayLifeTypeAService.a05(ID);
        } catch (Exception e) {
        } finally {
            if (a05Result == null)
                a05Result = new HashMap<String, Object>();
            if (StringUtils.isBlank(MapUtils.getString(a05Result, "NATION_CODE"))) {
                a05Result.put("NATION_CODE", "TW");
            }
        }

        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("NATIONALITY", MapUtils.getString(a05Result, "NATION_CODE"));
        param.put("NAME", (String) a05Result.get("ROLE_NAME"));
        param.put("IP", IP);
        Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G01_URL, param, HashMap.class);
        return result == null ? Collections.emptyMap() : result;
    }

    @Override
    public Map<String, Object> g02(String ID, String OTP_SNO, String OTP_VNO) throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("OTP_SNO", OTP_SNO);
        param.put("OTP_VNO", OTP_VNO);
        Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G02_URL, param, HashMap.class);
        return result == null ? Collections.emptyMap() : result;
    }

    @Override
    public Map<String, Object> g04(String ID, String POLICYNO, String TYPE) throws Exception {
        //20180928 by Todd，暫時調整 for 測試環境房貸戶資料驗證用，不可上版至正式環境
        Map<String, Object> a05Result = null;
        try {
            a05Result = cathayLifeTypeAService.a05(ID);
        } catch (Exception e) {
        } finally {
            if (a05Result == null)
                a05Result = new HashMap<String, Object>();
            if (StringUtils.isBlank(MapUtils.getString(a05Result, "NATION_CODE"))) {
                a05Result.put("NATION_CODE", "TW");
            }
        }

        // 20181214 by todd, 多元機制未完成前loop {0:一般壽險 1:房貸戶}
        Map<String, Object> result = null;
        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("POLICYNO", POLICYNO);
        param.put("NATIONALITY", MapUtils.getString(a05Result, "NATION_CODE"));

        if (StringUtils.isBlank(TYPE)) {
            String[] types = { "0", "1" }; // [0:一般壽險 3:旅平險/傷害險 2:團險 1:房貸戶]
            for (String verifyType : types) {
                param.put("TYPE", verifyType);
                Map<String, Object> g04result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G04_URL, param, HashMap.class);
                if (MapUtils.isNotEmpty(g04result) && StringUtils.equals("Y", MapUtils.getString(g04result, "status"))) {
                    result = g04result;
                    break;
                } else {
                    log.error("g04result=" + (g04result == null ? "null" : Arrays.toString(g04result.entrySet().toArray())));
                    if (result == null) {
                        result = g04result;
                    }
                }
            }
        } else {
            param.put("TYPE", TYPE);
            result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G04_URL, param, HashMap.class);
        }
        return result == null ? Collections.emptyMap() : result;
    }

    @Override
    public Map<String, Object> g05(String SYS_CODE, String START_WITH) throws Exception {

        Map<String, Object> result = null;

        Map<String, String> param = new HashMap<>();
        param.put("SYS_NO", SYS_CODE);
        param.put("START_WITH", START_WITH);

        // result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G05_URL, param, HashMap.class);
        result = OkHttpUtils.callLifeApi("cathaylife.g05", param, HashMap.class);

        return result == null ? Collections.emptyMap() : result;
    }

    @Override
    public Map<String, Object> g05WithNoException(String SYS_CODE, String START_WITH) {
        Map<String, Object> result = null;
        try {
            result = g05(SYS_CODE, START_WITH);
        } catch (Exception e) {
            log.error("[g05] error:" + e.getMessage());
        }
        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> g06(String ID) throws Exception {

        Map<String, Object> result = null;

        Map<String, String> param = new HashMap<>();
        param.put("memberID", ID);
        param.put("SYS_NO", "CB");

        // result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G06_URL, param, HashMap.class);
        result = OkHttpUtils.callLifeApi("cathaylife.g06", param, HashMap.class);

        return result == null ? Collections.emptyMap() : result;
    }

    @Override
    public Map<String, Object> g06WithNoException(String ID) {
        Map<String, Object> result = null;
        try {
            result = g06(ID);
        } catch (Exception e) {
            log.error("[g06] error:" + e.getMessage());
        }
        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> g07(String paymentJSON) throws Exception {

        Map<String, Object> result = null;

        Map<String, String> param = new HashMap<>();
        param.put("paymentJSON", paymentJSON);
        
        // result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G07_URL, null, paymentJSON, HashMap.class);
        result = OkHttpUtils.callLifeApi("cathaylife.g07", param, HashMap.class);

        return result == null ? Collections.emptyMap() : result;
    }

    @Override
    public Map<String, Object> g08(String finishInfoJSON) throws Exception {

        Map<String, Object> result = null;

        Map<String, String> param = new HashMap<>();
        param.put("finishInfoJSON", finishInfoJSON);
        
        // result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_G08_URL, null, finishInfoJSON, HashMap.class);
        result = OkHttpUtils.callLifeApi("cathaylife.g08", param, HashMap.class);

        return result == null ? Collections.emptyMap() : result;
    }
}
