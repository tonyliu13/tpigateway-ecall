package com.tp.tpigateway.service.life.lifeapi.impl;

import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeOTSService;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

@Service("cathayLifeTypeOTSService")
public class CathayLifeTypeOTSServiceImpl implements CathayLifeTypeOTSService {

    private static Logger log = LoggerFactory.getLogger(CathayLifeTypeOTSServiceImpl.class);
    
    @Override
    public Map<String, Object> eCallQueueInfo() throws Exception {

        long costTimeStart = System.currentTimeMillis();

        String resultStr = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_ECALLQUEUEINFO_URL, null, String.class, String.class);
        Map<String, Object> result = JSONUtils.json2map(resultStr);

        log.info("[eCallQueueInfo] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result == null ? Collections.emptyMap() : result;
    }
}
