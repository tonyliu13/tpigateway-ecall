package com.tp.tpigateway.service.life.lifeapi.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeZService;
import com.tp.tpigateway.util.OkHttpUtils;

import edu.emory.mathcs.backport.java.util.Collections;

@Service("cathayLifeTypeZService")
public class CathayLifeTypeZServiceImpl implements CathayLifeTypeZService {

    private static Logger log = LoggerFactory.getLogger(CathayLifeTypeZServiceImpl.class);
    
    @Override
    public String z01(String trnId, String mobileNo, String infmId, String infmName, String email) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, String> param = new HashMap<>();
        param.put("trnId", trnId);
        param.put("mobileNo", mobileNo);
        param.put("infmId", infmId);
        param.put("infmName", infmName);
        param.put("email", email);
        
        // String result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_Z01_URL, param, String.class);
        String result = OkHttpUtils.callLifeApi("cathaylife.z01", param, String.class);
        
        log.info("[z01] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> z02(String trnId, String s, String otp) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, String> param = new HashMap<>();
        param.put("trnId", trnId);
        param.put("key", s);
        param.put("otp", otp);
        
        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_Z02_URL, param, HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.z02", param, HashMap.class);
        
        log.info("[z01] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result == null ? Collections.emptyMap() : result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> z04(String ID, String TRANS_NO, String SRC) throws Exception {
        Map<String, Object> result = null;

        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("TRANS_NO", TRANS_NO);
        param.put("SRC", SRC);

        // result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_Z04_URL, param, HashMap.class);
        result = OkHttpUtils.callLifeApi("cathaylife.z04", param, HashMap.class);

        return result == null ? Collections.emptyMap() : result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> z05(String ID, String TRANS_NO, String SRC) throws Exception {
        Map<String, Object> result = null;

        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("TRANS_NO", TRANS_NO);
        param.put("SRC", SRC);

        // result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_Z05_URL, param, HashMap.class);
        result = OkHttpUtils.callLifeApi("cathaylife.z05", param, HashMap.class);

        return result == null ? Collections.emptyMap() : result;
    }
}
