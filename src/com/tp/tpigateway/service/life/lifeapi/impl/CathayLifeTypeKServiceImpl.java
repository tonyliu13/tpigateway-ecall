 package com.tp.tpigateway.service.life.lifeapi.impl;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeHService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeKService;
import com.tp.tpigateway.util.LocalCache;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

/**
 * 投商(投資型商品)資訊相關服務實作
 */
@Service("CathayLifeTypeKService")
public class CathayLifeTypeKServiceImpl implements CathayLifeTypeKService {

	private static Logger log = LoggerFactory.getLogger(CathayLifeTypeKServiceImpl.class);

	private boolean isDebug = log.isDebugEnabled();
	
	private boolean isInfo = log.isInfoEnabled();

	@Autowired
	private CathayLifeTypeHService cathayLifeTypeHService;

	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> k01(String ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_K01_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> k02(String ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_K02_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> k03(String AREA, String CITY) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("AREA", AREA);
		param.put("CITY", CITY);
		List<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_K03_URL, param, List.class);
		return result == null ? new ArrayList<Map<String,Object>>() : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> k04(String ID, String TYPE, String LAN, String CURRENT, String APC_NM, String APC_PASSPORT) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		param.put("TYPE", TYPE);
		param.put("LAN", LAN);
		param.put("CURRENT", CURRENT);
		param.put("APC_NM", StringUtils.isBlank(APC_NM) ? "" : URLEncoder.encode(APC_NM, "UTF-8"));
		param.put("APC_PASSPORT", StringUtils.isBlank(APC_PASSPORT) ? "" : URLEncoder.encode(APC_PASSPORT, "UTF-8"));
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_K04_URL, param, ArrayList.class);

		for (Object obj : result) {
			Map<String, Object> pdf = (Map<String, Object>) obj;
			if (!pdf.isEmpty()) {
				Calendar calendar = Calendar.getInstance();
				String currentDate = simpleDateFormat.format(calendar.getTime());
	
				StringBuffer sb = new StringBuffer();
				sb.append("H07(").append(currentDate).append(")");
				String key_H07 = sb.toString();
				if (isInfo) {
					log.info("key_H07:" + key_H07);
				}
	
				String encryptExpiredTime = (String) LocalCache.get(key_H07);
				if (isInfo) {
					log.info("(LocalCache) encryptExpiredTime:" + encryptExpiredTime);
				}
	
				if (StringUtils.isBlank(encryptExpiredTime)) {
					if (isDebug) {
						log.debug("cathayLifeTypeHService:" + cathayLifeTypeHService);
					}
					if (cathayLifeTypeHService == null) {
						synchronized (cathayLifeTypeHService) {
							if (cathayLifeTypeHService == null) {
								cathayLifeTypeHService = new CathayLifeTypeHServiceImpl();
							}
						}
					}
	
					encryptExpiredTime = cathayLifeTypeHService.h07();
					if (isInfo) {
						log.info("encryptExpiredTime:" + encryptExpiredTime);
					}
	
					if (StringUtils.isNotBlank(encryptExpiredTime)) {
						LocalCache.register(key_H07, encryptExpiredTime, LocalCache.ExpiredTime._1day);
						pdf.put("lts", encryptExpiredTime);
					} else {
						sb.setLength(0);
						sb.append("取得 ").append(currentDate).append(" 下載檔案有效時間失敗");
						log.error(sb.toString());
					}
				} else {
					pdf.put("lts", encryptExpiredTime);
				}
			}
		}

		return result == null ? new ArrayList<Object>() : result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> k05(String ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_K05_URL, param, HashMap.class);

		if (!result.isEmpty()) {
			Calendar calendar = Calendar.getInstance();
			String currentDate = simpleDateFormat.format(calendar.getTime());

			StringBuffer sb = new StringBuffer();
			sb.append("H07(").append(currentDate).append(")");
			String key_H07 = sb.toString();
			if (isInfo) {
				log.info("key_H07:" + key_H07);
			}

			String encryptExpiredTime = (String) LocalCache.get(key_H07);
			if (isInfo) {
				log.info("(LocalCache) encryptExpiredTime:" + encryptExpiredTime);
			}

			if (StringUtils.isBlank(encryptExpiredTime)) {
				if (isDebug) {
					log.debug("cathayLifeTypeHService:" + cathayLifeTypeHService);
				}
				if (cathayLifeTypeHService == null) {
					synchronized (cathayLifeTypeHService) {
						if (cathayLifeTypeHService == null) {
							cathayLifeTypeHService = new CathayLifeTypeHServiceImpl();
						}
					}
				}

				encryptExpiredTime = cathayLifeTypeHService.h07();
				if (isInfo) {
					log.info("encryptExpiredTime:" + encryptExpiredTime);
				}

				if (StringUtils.isNotBlank(encryptExpiredTime)) {
					LocalCache.register(key_H07, encryptExpiredTime, LocalCache.ExpiredTime._1day);
					result.put("lts", encryptExpiredTime);
				} else {
					sb.setLength(0);
					sb.append("取得 ").append(currentDate).append(" 下載檔案有效時間失敗");
					log.error(sb.toString());
				}
			} else {
				result.put("lts", encryptExpiredTime);
			}
		}

		return result == null ? new HashMap<String, Object> () : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String k06(String ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		String result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_K06_URL, param, String.class);
		return result == null ? "" : result;
	}
}
