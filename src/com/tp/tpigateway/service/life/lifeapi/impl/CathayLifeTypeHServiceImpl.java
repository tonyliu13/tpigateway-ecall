package com.tp.tpigateway.service.life.lifeapi.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeHService;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

@SuppressWarnings("unchecked")
@Service("CathayLifeTypeHService")
public class CathayLifeTypeHServiceImpl implements CathayLifeTypeHService {

	private static Logger log = LoggerFactory.getLogger(CathayLifeTypeDServiceImpl.class);

	@Override
	public List<Object> h01(String ID, String nationCode, String ScenarioCode) throws Exception {
		long costTimeStart = System.currentTimeMillis();
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		param.put("NATION_CODE", nationCode);
		param.put("SCENARIO_CODE", ScenarioCode);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_H01_URL, param, ArrayList.class);
		log.info("[h01] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public List<Object> h02(String LONGITUDE, String LATITUDE, int DATA_CNT) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("LONGITUDE", LONGITUDE);
		param.put("LATITUDE", LATITUDE);
		param.put("DATA_CNT", "" + DATA_CNT);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_H02_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public Map<String, Object> h03(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_H03_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public List<Object> h05(List<String> queryPolicyNos) throws Exception {
		// JSON方式呼叫start
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String requestJSON = ow.writeValueAsString(queryPolicyNos);
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO_LIST", requestJSON);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_H05_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public boolean h06() throws Exception {
		Map<String, String> param = new HashMap<>();
		Boolean result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_H06_URL, param, Boolean.class);
		return result == null ? false : result;
	}

	@Override
	public String h07() throws Exception {
		Map<String, String> param = new HashMap<>();
		
		return OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_H07_URL, param, String.class);
	}
}
