package com.tp.tpigateway.service.life.lifeapi.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeCService;
import com.tp.tpigateway.util.OkHttpUtils;

@SuppressWarnings("unchecked")
@Service("cathayLifeTypeCService")
public class CathayLifeTypeCServiceImpl implements CathayLifeTypeCService {

    private static Logger log = LoggerFactory.getLogger(CathayLifeTypeDServiceImpl.class);

    @Override
    @Async
    public void c01WithAsync(ConcurrentHashMap<String, Object> C01_map, String c01_CALL_ID, String CLIENT_ID, String TRANS_UUID, Map<String, String> CONTRACT_DATA,
            List<Map<String, String>> INSD_DATA) throws Exception {
        
        try {
            Map<String, Object> result = c01(CLIENT_ID, TRANS_UUID, CONTRACT_DATA, INSD_DATA, c01_CALL_ID);
            if (result == null) {
                C01_map.put(c01_CALL_ID, "NULL");
            } else {
                C01_map.put(c01_CALL_ID, result);
            }
        } catch (Exception e) {
            log.error("[" + c01_CALL_ID + "] call C01 error:" + e.getMessage());
            C01_map.put(c01_CALL_ID, "NULL");
        }
    }

    @Override
    public Map<String, Object> c01(String CLIENT_ID, String TRANS_UUID, Map<String, String> CONTRACT_DATA, List<Map<String, String>> INSD_DATA, String c01_CALL_ID) throws Exception {

        long costTimeStart = System.currentTimeMillis();

        Map<String, Object> param = new HashMap<>();
        param.put("TRANS_UUID", TRANS_UUID);
        
        Map<String, String> CONTRACT_DATA_C01 = new	HashMap<String, String>();
        CONTRACT_DATA_C01.putAll(CONTRACT_DATA);
        
        if("CBA10001_5X1_5_5".equals(c01_CALL_ID)) {
        	CONTRACT_DATA_C01.put("RGN_CODE", "11");
        }
        
        Map<String, Object> contract = new HashMap<>();
        contract.put("CLIENT_ID", CLIENT_ID);
        contract.put("CONTRACT_DATA", CONTRACT_DATA_C01);
        contract.put("INSD_DATA", INSD_DATA);

        param.put("contractJSON", contract);

        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_C01_URL, null, new JSONObject(param).toString(), HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.c01", null, new JSONObject(param).toString(), HashMap.class);

        log.info("[c01] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result;
    }

    @Override
    public Map<String, Object> c02(String TRANS_NO, String TRANS_UUID, String ZS_UUID, String CLIENT_ID, String contract_TRANS_NO, Map<String, Object> CONTRACT_DATA,
            List<Map<String, String>> INSD_DATA, List<Map<String, Object>> BENE_DATA, Map<String, Object> TRAL_TMP) throws Exception {

        long costTimeStart = System.currentTimeMillis();

        Map<String, Object> param = new HashMap<>();
        param.put("TRANS_NO", TRANS_NO);
        param.put("TRANS_UUID", TRANS_UUID);
        param.put("ZS_UUID", ZS_UUID);

        Map<String, Object> contract = new HashMap<>();
        contract.put("CLIENT_ID", CLIENT_ID);
        contract.put("TRANS_NO", contract_TRANS_NO);
        contract.put("CONTRACT_DATA", CONTRACT_DATA);
        contract.put("INSD_DATA", INSD_DATA);
        contract.put("BENE_DATA", BENE_DATA);
        contract.put("TRAL_TMP", TRAL_TMP);

        param.put("contractJSON", contract);

        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_C02_URL, null, new JSONObject(param).toString(), HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.c02", null, new JSONObject(param).toString(), HashMap.class);

        log.info("[c02] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result;
    }

    @Override
    public Map<String, Object> c02WithNoException(String TRANS_NO, String TRANS_UUID, String ZS_UUID, String CLIENT_ID, String contract_TRANS_NO, Map<String, Object> CONTRACT_DATA,
            List<Map<String, String>> INSD_DATA, List<Map<String, Object>> BENE_DATA, Map<String, Object> TRAL_TMP) {
        Map<String, Object> result = null;
        try {
            result = c02(TRANS_NO, TRANS_UUID, ZS_UUID, CLIENT_ID, contract_TRANS_NO, CONTRACT_DATA, INSD_DATA, BENE_DATA, TRAL_TMP);
        } catch (Exception e) {
            log.error("[c02] error:" + e.getMessage());
        }
        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> c03(String TRANS_NO, String TRANS_UUID, Map<String, Object> contract) throws Exception {

        long costTimeStart = System.currentTimeMillis();

        Map<String, Object> param = new HashMap<>();
        param.put("TRANS_NO", TRANS_NO);
        param.put("TRANS_UUID", TRANS_UUID);
        param.put("contractJSON", contract);

        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_C03_URL, null, new JSONObject(param).toString(), HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.c03", null, new JSONObject(param).toString(), HashMap.class);

        log.info("[c03] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> c04(String ID) throws Exception {

        long costTimeStart = System.currentTimeMillis();

        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);

        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_C04_URL, param, HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.c04", param, HashMap.class);

        log.info("[c04] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> c04WithNoException(String ID) {
        Map<String, Object> result = null;
        try {
            result = c04(ID);
        } catch (Exception e) {
            log.error("[c04] error:" + e.getMessage());
        }
        return result == null ? new HashMap<String, Object>() : result;
    }
}
