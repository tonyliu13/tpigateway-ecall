package com.tp.tpigateway.service.life.lifeapi.impl;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.enumeration.A05;
import com.tp.tpigateway.service.common.BaseService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeAService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeIService;
import com.tp.tpigateway.util.JaxbXmlUtil;
import com.tp.tpigateway.util.LocalCache;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

/**
 * 通路資訊相關服務實作
 */
@Service("cathayLifeTypeIService")
public class CathayLifeTypeIServiceImpl extends BaseService implements CathayLifeTypeIService {
	private static final Logger log = Logger.getLogger(CathayLifeTypeIServiceImpl.class);

	@Autowired
	private CathayLifeTypeAService cathayLifeTypeAService;

	public static class I01Data {
		private String latitude;
		private String longitude;

		public I01Data(String latitude, String longitude) {
			super();
			this.latitude = latitude;
			this.longitude = longitude;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
	}

	@Override
	public String i01(String addr) throws Exception {
		// Map<String, String> param = new HashMap<>();
		// param.put("addr", addr);
	    String url = PropUtils.getProperty(PropUtils.CATHAYLIFE_I01_URL + ".url") + URLEncoder.encode(addr, "UTF-8");
		// String result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_I01_URL, param, String.class, String.class);
	    
	    Map<String, Object> respResult = OkHttpUtils.requestGet(url, super.getHeaders());
	    
	    String result = null;
        if (OkHttpUtils.isSuccessHasBody(respResult)) {
            result = OkHttpUtils.getBody(respResult);
        } else {
            log.error("respResult=" + respResult);
        }
        
		return result;
	}

	/*
	 * [County], [City], [Village], [Road], [Lane], [Alley], [Number], [Level], [LATITUDE], [LONGITUDE]
	 */
	@Override
	public I01Data getI01Date(String addr) throws Exception {
		I01Data i01Data = null;
		String i01Result = this.i01(addr);
		if (StringUtils.isEmpty(i01Result)) {
			i01Data = new I01Data("", "");
		} else {
			try {
				String[] r = i01Result.split(",");
				String latitude = r[r.length - 2];
				String longitude = r[r.length - 1];
				i01Data = new I01Data(latitude, longitude);
			} catch (Exception e) {
				i01Data = new I01Data("", "");
			}
		}
		return i01Data;
	}

	@XmlRootElement(name = "CurrentYearCoverage")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class I02Data {

		@XmlElement(name = "CoverageItem")
		private List<CoverageItem> coverageItems;

		public I02Data() {
			this.coverageItems = new ArrayList<>();
		}

		public List<CoverageItem> getCoverageItems() {
			return coverageItems;
		}

		public void setCoverageItems(List<CoverageItem> coverageItems) {
			this.coverageItems = coverageItems;
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class CoverageItem {
		@XmlElement(name = "MainCoverage")
		private String mainCoverage;
		@XmlElement(name = "SubCoverageElem")
		private String subCoverageElem;
		@XmlElement(name = "Amount")
		private String amount;
		@XmlElement(name = "MainCoverageAverageAmount")
		private String mainCoverageAverageAmount;
		@XmlElement(name = "IsMainAvgCountIncluded")
		private String isMainAvgCountIncluded;

		public String getMainCoverage() {
			return mainCoverage;
		}

		public void setMainCoverage(String mainCoverage) {
			this.mainCoverage = mainCoverage;
		}

		public String getSubCoverageElem() {
			return subCoverageElem;
		}

		public void setSubCoverageElem(String subCoverageElem) {
			this.subCoverageElem = subCoverageElem;
		}

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public String getMainCoverageAverageAmount() {
			return mainCoverageAverageAmount;
		}

		public void setMainCoverageAverageAmount(String mainCoverageAverageAmount) {
			this.mainCoverageAverageAmount = mainCoverageAverageAmount;
		}

		public String getIsMainAvgCountIncluded() {
			return isMainAvgCountIncluded;
		}

		public void setIsMainAvgCountIncluded(String isMainAvgCountIncluded) {
			this.isMainAvgCountIncluded = isMainAvgCountIncluded;
		}

	}

	@Override
	public String i02(String Customer, String policyno) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("I02(Customer:");
		boolean customerExist = StringUtils.isNotBlank(Customer);
		if (customerExist) {
			sb.append(Customer);
		}

		sb.append(", policyno:");
		if (StringUtils.isNotBlank(policyno)) {
			sb.append(policyno);
		}
		sb.append(")");

		String key_I02 = sb.toString(); // I02(Customer, policyno)
		sb.setLength(0);

		String i02_result = (String) LocalCache.get(key_I02);

		if (StringUtils.isBlank(i02_result)) {
			sb.append("A05(Customer:");
			if (customerExist) {
				sb.append(Customer);
			}
			sb.append(").").append(A05.ROLE_BRDY.name());

			String key_A05_ROLE_BRDY = sb.toString(); // A05(Customer).ROLE_BRDY
			sb.setLength(0);

			String birthday = (String) LocalCache.get(key_A05_ROLE_BRDY);

			if (StringUtils.isBlank(birthday)) {
				Map<String, Object> a05_result = cathayLifeTypeAService.a05(Customer);
				if (a05_result != null && a05_result.containsKey(A05.ROLE_BRDY.name())) {
					birthday = MapUtils.getString(a05_result, A05.ROLE_BRDY.name());

					if (StringUtils.isNotBlank(birthday)) {
						LocalCache.register(key_A05_ROLE_BRDY, birthday, LocalCache.ExpiredTime._1day);
					}
				}
			}

			Map<String, String> param = new HashMap<>();
			param.put("Customer", Customer);
			param.put("Birthday", birthday);
			param.put("policyno", policyno);
			i02_result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_I02_URL, param, String.class, String.class);

			if (i02_result.indexOf("MainCoverageAverageAmount") < 0) {
				i02_result = null;
			} else {
				LocalCache.register(key_I02, i02_result, LocalCache.ExpiredTime._12hrs);
			}
		}

		return i02_result;
	}

	@Override
	public I02Data getI02Data(String Customer, String policyno) throws Exception {
		String i02Result = this.i02(Customer, policyno);

		I02Data i02Data;
		if (StringUtils.isEmpty(i02Result)) {
			i02Data = new I02Data();
		} else {
			try {
				i02Data = JaxbXmlUtil.xmlToBean(i02Result, I02Data.class);
			} catch (Exception e) {
				log.error("[xml轉換失敗] i02Result\n" + i02Result);
				i02Data = new I02Data();
			}
		}

		return i02Data;
	}
}
