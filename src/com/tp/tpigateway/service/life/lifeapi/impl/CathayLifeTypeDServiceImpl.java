package com.tp.tpigateway.service.life.lifeapi.impl;

import com.tp.tpigateway.model.common.enumeration.D15;
import com.tp.tpigateway.model.common.enumeration.D25;
import com.tp.tpigateway.model.common.enumeration.D26;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeDService;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({ "unchecked" })
@Service("cathayLifeTypeDService")
public class CathayLifeTypeDServiceImpl implements CathayLifeTypeDService {

	private static Logger log = LoggerFactory.getLogger(CathayLifeTypeDServiceImpl.class);

	@Override
	public List<Object> d02(String policyNo) throws Exception {
		long costTimeStart = System.currentTimeMillis();
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", policyNo);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D02_URL, param, ArrayList.class);
		log.info("[d02] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public Map<String, Object> d01(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D01_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> d05(String POLICY_NO, String QUERY_DATE) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("QUERY_DATE", QUERY_DATE);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D05_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public List<Object> d06(String POLICY_NO, String PAY_TIMES, String RCPT_SER_NO) throws Exception {
		log.debug("POLICY_NO=" + POLICY_NO + ", PAY_TIMES=" + PAY_TIMES + ", RCPT_SER_NO=" + RCPT_SER_NO);
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("PAY_TIMES", PAY_TIMES);
		param.put("RCPT_SER_NO", RCPT_SER_NO);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D06_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public List<Object> d07(String POLICY_NO, String URGE_STR_DATE, String URGE_END_DATE) throws Exception {
		log.debug("POLICY_NO=" + POLICY_NO + ", URGE_STR_DATE=" + URGE_STR_DATE + ", URGE_END_DATE=" + URGE_END_DATE);
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("URGE_STR_DATE", URGE_STR_DATE);
		param.put("URGE_END_DATE", URGE_END_DATE);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D07_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public List<Object> d08(String POLICY_NO, String URGE_STR_DATE, String URGE_END_DATE) throws Exception {
		log.debug("POLICY_NO=" + POLICY_NO + ", URGE_STR_DATE=" + URGE_STR_DATE + ", URGE_END_DATE=" + URGE_END_DATE);
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("URGE_STR_DATE", URGE_STR_DATE);
		param.put("URGE_END_DATE", URGE_END_DATE);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D08_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public Map<String, Object> d09(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D09_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> d11(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D11_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> d12(String APC_ID, String USER_ID, String USER_NMAE, String IP) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("APC_ID", APC_ID);
		param.put("USER_ID", USER_ID);
		param.put("USER_NMAE", USER_NMAE);
		param.put("IP", IP);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D12_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> d14(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D14_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public List<Object> d15(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D15_URL, param, ArrayList.class);

		formatShortUrl(result, D15.ShortURL.name());

		return result == null ? new ArrayList<>() : result;
	}

	// @Override
	// public String d13(String POLICY_NO) throws Exception {
	//
	// String result = null;
	//
	// try {
	// log.debug("POLICY_NO=" + POLICY_NO);
	//
	// RequestBody body = new FormBody.Builder().add("POLICY_NO", POLICY_NO).build();
	//
	// Map<String, Object> respResult = OkHttpUtils.requestPost(PropUtils.CATHAYLIFE_D13_URL,
	// new HashMap<String, String>(), body);
	// log.debug("respResult=" + Arrays.toString(respResult.entrySet().toArray()));
	//
	// if (OkHttpUtils.isSuccessHasBody(respResult)) {
	// String botResult = OkHttpUtils.getBody(respResult);
	// log.debug(botResult);
	//
	// Map<String, Object> resultMap = new ObjectMapper().readValue(botResult, HashMap.class);
	//
	// result = MapUtils.getString(resultMap, "PDF_FULL_PATH");
	// } else {
	// log.debug(OkHttpUtils.getMessage(respResult));
	// }
	// } catch (Exception e) {
	// String errorMsg = e.getMessage();
	// log.error(e.getMessage(), e);
	//
	// if ((e.getCause() != null && e.getCause().equals(SocketTimeoutException.class))
	// || StringUtils.contains(errorMsg.toLowerCase(), "timed out")
	// || StringUtils.contains(errorMsg.toLowerCase(), "timeout")) {
	// throw e;
	// }
	// } finally {
	// if (result == null) {
	// result = "";
	// }
	// BotTpiCallLog botTpiCallLog = ApiLogUtils.getBotTpiCallLog();
	// if (botTpiCallLog != null) {
	// botTpiCallLogDao.save(new BotTpiCallLog(botTpiCallLog));
	// }
	// }
	//
	// return result;
	// }

	// @Override
	// public String d14(String POLICY_NO) throws Exception {
	//
	// String result = null;
	//
	// try {
	// log.debug("POLICY_NO=" + POLICY_NO);
	//
	// RequestBody body = new FormBody.Builder().add("POLICY_NO", POLICY_NO).build();
	//
	// Map<String, Object> respResult = OkHttpUtils.requestPost(PropUtils.CATHAYLIFE_D14_URL,
	// new HashMap<String, String>(), body);
	// log.debug("respResult=" + Arrays.toString(respResult.entrySet().toArray()));
	//
	// if (OkHttpUtils.isSuccessHasBody(respResult)) {
	// String botResult = OkHttpUtils.getBody(respResult);
	// log.debug(botResult);
	//
	// Map<String, Object> resultMap = new ObjectMapper().readValue(botResult, HashMap.class);
	//
	// result = MapUtils.getString(resultMap, "PDF_FULL_PATH");
	// } else {
	// log.debug(OkHttpUtils.getMessage(respResult));
	// }
	// } catch (Exception e) {
	// String errorMsg = e.getMessage();
	// log.error(e.getMessage(), e);
	//
	// if ((e.getCause() != null && e.getCause().equals(SocketTimeoutException.class))
	// || StringUtils.contains(errorMsg.toLowerCase(), "timed out")
	// || StringUtils.contains(errorMsg.toLowerCase(), "timeout")) {
	// throw e;
	// }
	// } finally {
	// if (result == null) {
	// result = "";
	// }
	// BotTpiCallLog botTpiCallLog = ApiLogUtils.getBotTpiCallLog();
	// if (botTpiCallLog != null) {
	// botTpiCallLogDao.save(new BotTpiCallLog(botTpiCallLog));
	// }
	// }
	//
	// return result;
	// }

	// @Override
	// public String d15(String POLICY_NO) throws Exception {
	//
	// String result = null;
	//
	// try {
	// log.debug("POLICY_NO=" + POLICY_NO);
	//
	// RequestBody body = new FormBody.Builder().add("POLICY_NO", POLICY_NO).build();
	//
	// Map<String, Object> respResult = OkHttpUtils.requestPost(PropUtils.CATHAYLIFE_D15_URL,
	// new HashMap<String, String>(), body);
	// log.debug("respResult=" + Arrays.toString(respResult.entrySet().toArray()));
	//
	// if (OkHttpUtils.isSuccessHasBody(respResult)) {
	// String botResult = OkHttpUtils.getBody(respResult);
	// log.debug(botResult);
	//
	// Map<String, Object> resultMap = new ObjectMapper().readValue(botResult, HashMap.class);
	//
	// result = MapUtils.getString(resultMap, "PDF_FULL_PATH");
	// } else {
	// log.debug(OkHttpUtils.getMessage(respResult));
	// }
	// } catch (Exception e) {
	// String errorMsg = e.getMessage();
	// log.error(e.getMessage(), e);
	//
	// if ((e.getCause() != null && e.getCause().equals(SocketTimeoutException.class))
	// || StringUtils.contains(errorMsg.toLowerCase(), "timed out")
	// || StringUtils.contains(errorMsg.toLowerCase(), "timeout")) {
	// throw e;
	// }
	// } finally {
	// if (result == null) {
	// result = "";
	// }
	// BotTpiCallLog botTpiCallLog = ApiLogUtils.getBotTpiCallLog();
	// if (botTpiCallLog != null) {
	// botTpiCallLogDao.save(new BotTpiCallLog(botTpiCallLog));
	// }
	// }
	//
	// return result;
	// }

	@Override
	public Map<String, Object> d16(String CLC_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("CLC_NO", CLC_NO);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D16_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public List<Object> d18(String APC_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("APC_ID", APC_ID);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D18_URL, param, ArrayList.class);
		return result == null ? new ArrayList<>() : result;
	}

	@Override
	public Map<String, Object> d25(String POLICY_NO, String AGNT_ID, String RTN_LOAN_AMT, String RTN_ALL, String INT_END_DATE,
			String RCPT_KIND, String BAL_TYPE) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("AGNT_ID", AGNT_ID);
		param.put("RTN_LOAN_AMT", RTN_LOAN_AMT);
		param.put("RTN_ALL", RTN_ALL);
		param.put("INT_END_DATE", INT_END_DATE);
		param.put("RCPT_KIND", RCPT_KIND);
		param.put("BAL_TYPE", BAL_TYPE);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D25_URL, param, HashMap.class);

		formatShortUrl(result, D25.PAY_DATA_URL.name());

		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> d26(String POLICY_NO, String AGNT_ID, String PAY_TYPE, String PAY_TIMES, String BAL_TYPE)
			throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("AGNT_ID", AGNT_ID);
		param.put("PAY_TYPE", PAY_TYPE);
		param.put("PAY_TIMES", PAY_TIMES);
		param.put("BAL_TYPE", BAL_TYPE);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D26_URL, param, HashMap.class);

		formatShortUrl(result, D26.PAY_DATA_URL.name());

		return result == null ? new HashMap<String, Object>() : result;
	}

	/**
	 * 格式化短網址
	 * 
	 * @param result
	 * @param key
	 */
	private void formatShortUrl(List<Object> result, String key) {
		if (result != null && !result.isEmpty()) {
			for (Object obj : result) {
				Map<String, Object> tmp = (Map<String, Object>) obj;
				formatShortUrl(tmp, key);
			}
		}
	}

	/**
	 * 格式化短網址
	 * 
	 * @param result
	 * @param key
	 */
	private void formatShortUrl(Map<String, Object> result, String key) {
		if (result != null && result.containsKey(key)) {
			String shortUrl = MapUtils.getString(result, key);
			if (StringUtils.isNotBlank(shortUrl)) {
				shortUrl = shortUrl.trim();
				if (!shortUrl.startsWith("http://") && !shortUrl.startsWith("https://")) {
					result.put(key, "https://" + shortUrl);
				}
			}
		}
	}

    @Override
    public Map<String, Object> d19(String CARD_NO, String CARD_END_YM, String USER_ID, String TRANS_NO, String TRANS_UUID) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, String> param = new HashMap<>();
        param.put("CARD_NO", CARD_NO);
        param.put("CARD_END_YM", CARD_END_YM);
        param.put("USER_ID", USER_ID);
        param.put("TRANS_NO", TRANS_NO);
        param.put("TRANS_UUID", TRANS_UUID);
        
        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D19_URL, param, HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.d19", param, HashMap.class);
        
        log.info("[d19] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> d20(Map<String, String> reqMap) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D20_URL, reqMap, HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.d20", reqMap, HashMap.class);
        
        log.info("[d20] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> d21(String ZIPCODE, String ADDRESS, String USER_ID, String TRANS_NO, String TRANS_UUID, String ZS_UUID) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, String> param = new HashMap<>();
        // param.put("ZIPCODE", ZIPCODE);
        param.put("ADDRESS", ADDRESS);
        param.put("USER_ID", USER_ID);
        param.put("TRANS_NO", TRANS_NO);
        param.put("TRANS_UUID", TRANS_UUID);
        param.put("ZS_UUID", ZS_UUID);
        
        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D21_URL, param, HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.d21", param, HashMap.class);
        
        log.info("[d21] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> d22(String USER_ID, String SYS_NO, String TRANS_NO, String TRANS_UUID) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();

        Map<String, String> param = new HashMap<>();
        param.put("USER_ID", USER_ID);
        param.put("SYS_NO", SYS_NO);
        param.put("TRANS_NO", TRANS_NO);
        param.put("TRANS_UUID", TRANS_UUID);

        // Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_D22_URL, param, HashMap.class);
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.d22", param, HashMap.class);

        log.info("[d22] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public Map<String, Object> d27() throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.d27", null, HashMap.class);

        log.info("[d27] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result == null ? new HashMap<String, Object>() : result;
    }
    
    @Override
    public Map<String, Object> d28(String PAY_WAY_CODE, String IS_SAVE, String IS_DELETE, String USER_ID, String TRANS_NO,String CARD_NO,String CARD_END_YM,String TRANS_UUID) throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("PAY_WAY_CODE", PAY_WAY_CODE);
        param.put("IS_SAVE", IS_SAVE);
        param.put("IS_DELETE", IS_DELETE);
        
        param.put("USER_ID", USER_ID);
        param.put("TRANS_NO", TRANS_NO);
        param.put("CARD_NO", CARD_NO);
        param.put("CARD_END_YM", CARD_END_YM);
        param.put("TRANS_UUID", TRANS_UUID);
    	long costTimeStart = System.currentTimeMillis();
    	
    	Map<String, Object> result = OkHttpUtils.callLifeApi("cathaylife.d28", param, HashMap.class);
    	
    	log.info("[d27] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
    	
    	return result == null ? new HashMap<String, Object>() : result;
    }
}
