package com.tp.tpigateway.service.life.lifeapi.impl;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeAService;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 保全資訊相關服務實作
 */
@SuppressWarnings("unchecked")
@Service("cathayLifeSecurityService")
public class CathayLifeTypeAServiceImpl implements CathayLifeTypeAService {

	private static Logger log = LoggerFactory.getLogger(CathayLifeTypeAServiceImpl.class);

	@Override
	public Map<String, Object> a05(String ID) throws Exception {
		long costTimeStart = System.currentTimeMillis();
		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A05_URL, param, HashMap.class);
		log.info("[a05] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> a04(String ID) throws Exception {
		Map<String, Object> result = a05(ID);
		String NATION_CODE = MapUtils.getString(result, "NATION_CODE");

		Map<String, String> param = new HashMap<>();
		param.put("ID", ID);
		param.put("NATION_CODE", NATION_CODE);
		result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A04_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public Map<String, Object> a04(String ID, String NATION_CODE) throws Exception {
	    Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("NATION_CODE", NATION_CODE);
        Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A04_URL, param, HashMap.class);
        return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public Map<String, Object> a22(String POLICY_NO, String TYPE) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("TYPE", TYPE);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A22_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> a12(String POLICY_NO, String APLY_DATE, List<Map<String, Object>> List)
			throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("APLY_DATE", APLY_DATE);
		param.put("mapList", JSONUtils.obj2json(List));
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A12_URL, param, HashMap.class);		
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public List<Object> a21(String APC_ID, String S_DATE, String E_DATE) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("APC_ID", APC_ID);
		param.put("S_DATE", S_DATE);
		param.put("E_DATE", E_DATE);
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A21_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@Override
	public Map<String, Object> a23(String POLICY_NO ,String PROD_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("PROD_ID", PROD_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A23_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object> () : result;
	}

	@Override
	public ArrayList<Map<String, Object>> a24(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A24_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	
	@Override
	public Map<String, Object> a29(String APC_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("APC_ID", APC_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A29_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}	

	@Override
	public Map<String, Object> a20(String APC_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("APC_ID", APC_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A20_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public ArrayList<Map<String, Object>> a25(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A25_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	
	@Override
	public ArrayList<Map<String, Object>> a27(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A27_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	
	@Override
	public Map<String, Object> a17(String POLICY_NO) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A17_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public Map<String, Object> a18(String POLICY_NO , String IS_RD_VALD , String INTENT) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("IS_RD_VALD", IS_RD_VALD);
		param.put("INTENT", INTENT);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A18_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public Map<String, Object> a14(String PROD_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("PROD_ID", PROD_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A14_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public Map<String, Object> a19(String POLICY_NO , String PREM , String PROD_KIND , String INTENT) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("PREM", PREM);
		param.put("PROD_KIND", PROD_KIND);
		param.put("INTENT", INTENT);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A19_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public Map<String, Object> a11(String PROD_ID , String INTENT) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("PROD_ID", PROD_ID);
		param.put("INTENT", INTENT);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A11_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}
	
	@Override
	public Map<String, Object> a13(String POLICY_NO, String PAY_FREQ, String PROD_KIND, String INTENT) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("PAY_FREQ", PAY_FREQ);
		param.put("PROD_KIND", PROD_KIND);
		param.put("INTENT", INTENT);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A13_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> a13(String POLICY_NO, String PAY_FREQ, String PROD_KIND, String AFT_PREM, String INTENT) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("PAY_FREQ", PAY_FREQ);
		param.put("PROD_KIND", PROD_KIND);
		param.put("AFT_PREM", AFT_PREM);
		param.put("INTENT", INTENT);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A13_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

	@Override
	public Map<String, Object> a15(String PROD_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("PROD_ID", PROD_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A15_URL, param, HashMap.class);
		return result == null ? new HashMap<String, Object>() : result;
	}

    @Override
    public Map<String, Object> a30(String APC_ID, String NATION_CODE) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, String> param = new HashMap<>();
        param.put("APC_ID", APC_ID);
        param.put("NATION_CODE", NATION_CODE);
        
        Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A30_URL, param, HashMap.class);
        
        log.info("[a30] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result == null ? new HashMap<String, Object>() : result;
    }

    @Override
    public List<Map<String, Object>> a31(String APC_ID) throws Exception {

        long costTimeStart = System.currentTimeMillis();

        Map<String, String> param = new HashMap<>();
        param.put("APC_ID", APC_ID);

        List<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A31_URL, param, ArrayList.class);

        log.info("[a31] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");

        return result == null ? new ArrayList<Map<String, Object>>() : result;
    }
    
    @Override
    public List<Map<String, Object>> a32(String DATA_KIND, String POLICYNO, String ID, String ROLE) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, String> param = new HashMap<>();
        param.put("DATA_KIND", DATA_KIND);
        if (StringUtils.isNotBlank(POLICYNO)) {
            param.put("POLICYNO", POLICYNO);
        }
        if (StringUtils.isNotBlank(ID)) {
            param.put("ID", ID);
        }
        param.put("ROLE", ROLE);
        
        List<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A32_URL, param, ArrayList.class);
        
        log.info("[a32] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result == null ? new ArrayList<Map<String, Object>>() : result;
    }
    
    @Override
    public List<Map<String, Object>> a33(String ID, String[] ROLE, String Period, String MAT_ANTY_CODE) throws Exception {
        
        long costTimeStart = System.currentTimeMillis();
        
        Map<String, String> param = new HashMap<>();
        param.put("ID", ID);
        param.put("ROLE", JSONUtils.obj2json(ROLE));
        param.put("Period", Period);
        param.put("MAT_ANTY_CODE", MAT_ANTY_CODE);
        
        List<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_A33_URL, param, ArrayList.class);
        
        log.info("[a33] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        
        return result == null ? new ArrayList<Map<String, Object>>() : result;
    }
}
