package com.tp.tpigateway.service.life.lifeapi.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeHService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeJService;
import com.tp.tpigateway.util.LocalCache;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

/**
 * 投商(投資型商品)資訊相關服務實作
 */
@Service("CathayLifeTypeJService")
public class CathayLifeTypeJServiceImpl implements CathayLifeTypeJService {

	private static final Logger log = LoggerFactory.getLogger(CathayLifeTypeJServiceImpl.class);

	private boolean isDebug = log.isDebugEnabled();

	private boolean isInfo = log.isInfoEnabled();

	@Autowired
	private CathayLifeTypeHService cathayLifeTypeHService;

	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public Map<String, Object> j01(String POLICY_NO, String INSD_ID, String SRC, String TRY_CNT_DATE, String ISSUE_DATE,
			String PAY_KIND, String PROD_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("INSD_ID", INSD_ID);
		param.put("SRC", SRC);
		param.put("TRY_CNT_DATE", TRY_CNT_DATE);
		param.put("ISSUE_DATE", ISSUE_DATE);
		param.put("PAY_KIND", PAY_KIND);
		param.put("PROD_ID", PROD_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_J01_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	}

	@Override
	public Map<String, Object> j02(String POLICY_NO, String APC_ID, String DOWNLOAD_NAME, String IP_ADDRS) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("APC_ID", APC_ID);
		param.put("DOWNLOAD_NAME", DOWNLOAD_NAME);
		param.put("IP_ADDRS", IP_ADDRS);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_J02_URL, param, HashMap.class);

		if (result == null) {
			return new HashMap<>();
		}

		if (!result.isEmpty()) {
			Calendar calendar = Calendar.getInstance();
			String currentDate = simpleDateFormat.format(calendar.getTime());

			StringBuffer sb = new StringBuffer();
			sb.append("H07(").append(currentDate).append(")");
			String key_H07 = sb.toString();
			if (isInfo) {
				log.info("key_H07:" + key_H07);
			}

			String encryptExpiredTime = (String) LocalCache.get(key_H07);
			if (isInfo) {
				log.info("(LocalCache) encryptExpiredTime:" + encryptExpiredTime);
			}

			if (StringUtils.isBlank(encryptExpiredTime)) {
				if (isDebug) {
					log.debug("cathayLifeTypeHService:" + cathayLifeTypeHService);
				}

				encryptExpiredTime = cathayLifeTypeHService.h07();
				if (isInfo) {
					log.info("encryptExpiredTime:" + encryptExpiredTime);
				}

				if (StringUtils.isNotBlank(encryptExpiredTime)) {
					LocalCache.register(key_H07, encryptExpiredTime, LocalCache.ExpiredTime._1day);
					result.put("lts", encryptExpiredTime);
				} else {
					sb.setLength(0);
					sb.append("取得 ").append(currentDate).append(" 下載檔案有效時間失敗");
					log.error(sb.toString());
				}
			} else {
				result.put("lts", encryptExpiredTime);
			}
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> j03(String POLICY_NO, String[] TRN_ITEM) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("POLICY_NO", POLICY_NO);
		param.put("TRN_ITEM", "[\"CC1\", \"CC2\", \"CCC\"]"); // 部份提領需要參數
		List<Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_J03_URL, param, List.class);
		return result == null ? new ArrayList<Object>() : result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> j04(String APC_ID, String[] TRN_ITEM, Boolean Q_ALL) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("APC_ID", APC_ID);
		param.put("TRN_ITEM", "[\"CC1\", \"CC2\", \"CCC\"]"); // 部份提領需要參數
		param.put("Q_ALL", Q_ALL.toString());
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_J04_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	}

	@Override
	public Map<String, Object> j05(String PROD_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("PROD_ID", PROD_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_J05_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	}
}
