package com.tp.tpigateway.service.life.lifeapi.impl;


import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeSService;
import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 投商(投資型商品)資訊相關服務實作
 */
@Service("CathayLifeTypeSService")
@SuppressWarnings("unchecked")
public class CathayLifeTypeSServiceImpl implements CathayLifeTypeSService {

	//private static Logger log = LoggerFactory.getLogger(CathayLifeTypeSServiceImpl.class);
	
	@Override
	public Map<String, Object> s01(String BORROW_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S01_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	}
	@Override
	public ArrayList<Map<String, Object>> s02(String LOAN_ID, String BORROW_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		param.put("LOAN_ID", LOAN_ID);
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S02_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	@Override
	public ArrayList<Map<String, Object>> s02(String BORROW_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S02_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	@Override
	public ArrayList<Map<String, Object>> s02(String LOAN_ID, String BORROW_ID, Boolean IS_PAID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		param.put("LOAN_ID", LOAN_ID);
		if(IS_PAID) {
			param.put("IS_PAID", "true");
		}else {
			param.put("IS_PAID", "false");
		}
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S02_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	@Override
	public ArrayList<Map<String, Object>> s03(String LOAN_ID, String BORROW_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		param.put("LOAN_ID", LOAN_ID);
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S03_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	@Override
	public ArrayList<Map<String, Object>> s03(String BORROW_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		ArrayList<Map<String, Object>> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S03_URL, param, ArrayList.class);
		return result == null ? new ArrayList<Map<String, Object>>() : result;
	}
	
	@Override
	public Map<String, Object> s04(String BORROW_ID) throws Exception {
	    Map<String, String> param = new HashMap<>();
	    param.put("BORROW_ID", BORROW_ID);
	    Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S04_URL, param, HashMap.class);
	    return result == null ? new HashMap<>() : result;
	}
	
	@Override
	public Map<String, Object> s06(String BORROW_ID) throws Exception {
	    Map<String, String> param = new HashMap<>();
	    param.put("BORROW_ID", BORROW_ID);
	    Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S06_URL, param, HashMap.class);
	    return result == null ? new HashMap<>() : result;
	}
	 
	 @Override
	 public Map<String, Object> s07(String BORROW_ID, String LOAN_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		param.put("LOAN_ID", LOAN_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S07_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	}
	 
	 @Override
	 public Map<String, Object> s08(String BORROW_ID) throws Exception {
		Map<String, String> param = new HashMap<>();
		param.put("BORROW_ID", BORROW_ID);
		Map<String, Object> result = OkHttpUtils.callLifeApi(PropUtils.CATHAYLIFE_S08_URL, param, HashMap.class);
		return result == null ? new HashMap<>() : result;
	 }

}
