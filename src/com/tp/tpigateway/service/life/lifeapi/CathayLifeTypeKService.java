package com.tp.tpigateway.service.life.lifeapi;

import java.util.List;
import java.util.Map;

/**
 * 保戶服務
 */
public interface CathayLifeTypeKService {
	
	/**
	 * 查詢客戶契約及VIP等級
	 * 
	 * @param ID
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> k01(String ID) throws Exception;
	
	/**
	 * 查詢客戶是否具當年度健檢資格
	 * 
	 * @param ID
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> k02(String ID) throws Exception;

	/**
	 * 查詢健檢之配合醫院
	 * 
	 * @param AREA
	 * @param CITY
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> k03(String AREA, String CITY) throws Exception;

	/**
	 * 中英文投保證明查詢
	 * 
	 * @param ID
	 * @return
	 * @throws Exception
	 */
	public List<Object> k04(String ID, String TYPE, String LAN, String CURRENT, String APC_NM, String APC_PASSPORT) throws Exception;
	
	/**
	 * 保戶卡申請資格查詢
	 * 
	 * @param ID
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> k05(String ID) throws Exception;

	/**
	 * 檢核保戶是否有被保人之保單-投保證明
	 * 
	 * @param ID
	 * @return
	 * @throws Exception
	 */
	public String k06(String ID) throws Exception;
	
}
