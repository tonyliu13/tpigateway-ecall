package com.tp.tpigateway.service.life.lifeapi;

import java.util.List;
import java.util.Map;

public interface CathayLifeTypeEService {

	/**
	 * [E02] 單一保單資訊
	 */
	public Map<String, Object> e02(String policyNoQry, String qryFrm, String qryId) throws Exception;
	
	/**
	 * [E01] ID索引保單
	 * 
	 * @param ID
	 * @param NATION_CODE
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> e01(String ID, String NATION_CODE) throws Exception;
	
	
	/**
	 * [E07] 單一保單附約資訊查詢
	 * @param ID
	 * @param NATION_CODE
	 * @return
	 * @throws Exception
	 */
	public List<Object> e07(String POLICY_NO, String APLY_DATE) throws Exception;

	/**
	 * [E08] 官網保單清單
	 * @param ID
	 * @param NATION_CODE
	 * @param PROD_KIND
	 * @param APC_IN
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	public List<Object> e08(String ID, String NATION_CODE, String PROD_KIND, String APC_IN, String POLICY_NO) throws Exception;
}
