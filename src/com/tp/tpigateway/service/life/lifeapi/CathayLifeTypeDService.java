package com.tp.tpigateway.service.life.lifeapi;

import java.util.List;
import java.util.Map;

public interface CathayLifeTypeDService {

	/**
	 * 保單近6期繳費記錄
	 */
	public List<Object> d02(String policyNo) throws Exception;
	
	/**
	 * [D01] 保單扣款不成狀況
	 * 
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d01(String POLICY_NO) throws Exception;


	/**
	 * [D05] 單一保單墊繳記錄查詢
	 *
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d05(String POLICY_NO, String QUERY_DATE) throws Exception;
	
	/**
	 * [D06] 單一保單查詢附約繳費記錄
	 * 
	 * @param POLICY_NO
	 * @param PAY_TIMES
	 * @param RCPT_SER_NO
	 * @return
	 * @throws Exception
	 */
	public List<Object> d06(String POLICY_NO, String PAY_TIMES, String RCPT_SER_NO) throws Exception;

	/**
	 * [D07] 單一保單保貸催繳紀錄
	 *
	 * @param POLICY_NO
	 * @param URGE_STR_DATE
	 * @param URGE_END_DATE
	 * @return
	 * @throws Exception
	 */
	public List<Object> d07(String POLICY_NO, String URGE_STR_DATE, String URGE_END_DATE) throws Exception;

	/**
	 * [D08] 單一保單保費催繳紀錄
	 *
	 * @param POLICY_NO
	 * @param URGE_STR_DATE
	 * @param URGE_END_DATE
	 * @return
	 * @throws Exception
	 */
	public List<Object> d08(String POLICY_NO, String URGE_STR_DATE, String URGE_END_DATE) throws Exception;

	/**
	 * [D09] 單一保單最近一期保費優惠查詢
	 * 
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d09(String POLICY_NO) throws Exception;
	
	/**
	 * [D11] 保費扣款狀態
	 * 
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d11(String POLICY_NO) throws Exception;
	
	/**
	 * [D12] 契約狀況一覽表
	 * 
	 * @param APC_ID
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d12(String APC_ID, String USER_ID, String USER_NMAE, String IP) throws Exception;
	
	/**
	 * [D13] 保險費繳納證明
	 * 
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	//public String d13(String POLICY_NO) throws Exception;
	
	/**
	 * [D14] 自繳單
	 * 
	 * @param POLICY_NO
	 * @return	
	 * @throws Exception
	 */
	public Map<String, Object> d14(String POLICY_NO) throws Exception;
	
	/**
	 * [D15] 繳費條碼
	 * 
	 * @param POLICY_NO
	 * @return	
	 * @throws Exception
	 */
	public List<Object> d15(String POLICY_NO) throws Exception;
	
	/**
	 * [D16] 取得服務人員聯絡
	 * 
	 * @param CLC_NO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d16(String CLC_NO) throws Exception;

	/**
	 * [D18] 取得服務人員聯絡(歸戶)
	 *
	 * @param APC_ID
	 * @return
	 * @throws Exception
	 */
	public List<Object> d18(String APC_ID) throws Exception;

	/**
	 * [D25] 單一保單試算保貸清償繳款單
	 *
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d25(String POLICY_NO, String AGNT_ID, String RTN_LOAN_AMT, String RTN_ALL, String INT_END_DATE, String RCPT_KIND, String BAL_TYPE) throws Exception;

	/**
	 * [D26] 單一保單試算墊繳清償繳款單
	 *
	 * @param POLICY_NO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> d26(String POLICY_NO, String AGNT_ID, String PAY_TYPE, String PAY_TIMES, String BAL_TYPE) throws Exception;
	
	/**
	 * [D19] 信用卡卡號檢核API
	 *
     * @param CARD_NO   信用卡卡號
     * @param CARD_END_YM 信用卡到期年月
     * @param USER_ID 使用者ID
     * @param TRANS_NO    訂單編號
     * @param TRANS_UUID  電商交易LOG編號
     * @return
     * @throws Exception
     */
    public Map<String, Object> d19(String CARD_NO, String CARD_END_YM, String USER_ID, String TRANS_NO, String TRANS_UUID) throws Exception;
    
    /**
     * [D20] 扣款API
     *
     * @param reqMap 參數集
     * @return
     * @throws Exception
     */
    public Map<String, Object> d20(Map<String, String> reqMap) throws Exception;
    
    /**
     * [D21] 地址檢核API
     *
     * @param ZIPCODE 3碼郵遞區號
     * @param ADDRESS 地址
     * @param USER_ID 使用者ID
     * @param TRANS_NO 訂單編號
     * @param TRANS_UUID 電商交易LOG編號
     * @param ZS_UUID ZS LOG編號(ZSweb會自己帶)
     * @return
     * @throws Exception
     */
    public Map<String, Object> d21(String ZIPCODE, String ADDRESS, String USER_ID, String TRANS_NO, String TRANS_UUID, String ZS_UUID) throws Exception;
    
    /**
     * [D22] 記憶繳費(常用帳戶)查詢
     *
     * @param USER_ID 使用者ID
     * @param SYS_NO 商品別
     * @param TRANS_NO 訂單編號
     * @param TRANS_UUID 電商交易LOG編號
     * @return
     * @throws Exception
     */
    public Map<String, Object> d22(String USER_ID, String SYS_NO, String TRANS_NO, String TRANS_UUID) throws Exception;
    
    /**
     * [D27] 取得行政地區
     *
     * @return
     * @throws Exception
     */
    public Map<String, Object> d27() throws Exception;
    
    /**
     * [D28] 記憶信用卡號
     * 
     * @param PAY_WAY_CODE 繳費管道
     * @param IS_SAVE 是否儲存
     * @param IS_DELETE 是否刪除
     * @param USER_ID 使用者ID
     * @param TRANS_NO 訂單編號
     * @param CARD_NO 信用卡卡號
     * @param CARD_END_YM 信用卡到期年月
     * @param TRANS_UUID 電商交易LOG編號
     * @return
     * @throws Exception
     */
    public Map<String, Object> d28(String PAY_WAY_CODE, String IS_SAVE, String IS_DELETE, String USER_ID, String TRANS_NO, String CARD_NO, String CARD_END_YM, String TRANS_UUID) throws Exception;

}
