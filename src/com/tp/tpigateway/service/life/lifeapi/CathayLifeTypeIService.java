package com.tp.tpigateway.service.life.lifeapi;

import com.tp.tpigateway.service.life.lifeapi.impl.CathayLifeTypeIServiceImpl.I01Data;
import com.tp.tpigateway.service.life.lifeapi.impl.CathayLifeTypeIServiceImpl.I02Data;

/**
 * 通路資訊相關服務介面
 */
public interface CathayLifeTypeIService {

	/**
	 * 地址定位
	 * 
	 * @param addr
	 * @return
	 * @throws Exception 
	 */
	public String i01(String addr) throws Exception;

	public I01Data getI01Date(String addr) throws Exception;

	/**
	 * ID索引總保障狀況
	 * @param Customer  被保人ID
	 * @param policyno  可不傳入(若不傳入則以ID歸戶統計)
	 * @return
	 * @throws Exception
	 */
	public String i02(String Customer, String policyno) throws Exception;

	public I02Data getI02Data(String Customer, String policyno) throws Exception;

}
