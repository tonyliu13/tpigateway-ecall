package com.tp.tpigateway.service.life.lifeapi;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface CathayLifeTypeCService {

	/**
	 * [C01] (非同步)網投旅平險-保費試算<br>
	 * ps.將c01呼叫結果以CALL_ID為key置入map中
	 * 
	 * @param C01_map
	 * @param c01_CALL_ID
	 * @param CLIENT_ID
	 * @param TRANS_UUID
	 * @param CONTRACT_DATA
	 * @param INSD_DATA
	 * @throws Exception
	 */
    public void c01WithAsync(ConcurrentHashMap<String, Object> C01_map, String c01_CALL_ID, String CLIENT_ID, String TRANS_UUID, Map<String, String> CONTRACT_DATA,
            List<Map<String, String>> INSD_DATA) throws Exception;

	/**
	 * [C01] 網投旅平險-保費試算
	 * 
	 * @param CLIENT_ID 通路別 (阿發)
	 * @param TRANS_UUID 阿發傳入session ID
	 * @param CONTRACT_DATA 契約內容單筆:JSONString
	 * @param INSD_DATA 投保內容多筆:JSONString
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> c01(String CLIENT_ID, String TRANS_UUID, Map<String, String> CONTRACT_DATA, List<Map<String, String>> INSD_DATA, String c01_CALL_ID)
			throws Exception;

	/**
     * [C02] 網投旅平險-核保試算(公會通算)
     * 
     * @param TRANS_NO 網投專站訂單編號
     * @param TRANS_UUID 阿發傳入session ID
     * @param ZS_UUID
     * @param CLIENT_ID 使用者 SYS_NO
     * @param contract_TRANS_NO 傳入網路投保訂單編號
     * @param CONTRACT_DATA 契約內容單筆
     * @param INSD_DATA 投保內容多筆
     * @param BENE_DATA 受益人內容多筆
     * @param TRAL_TMP 業報書單筆
     * @return
     * @throws Exception
     */
    public Map<String, Object> c02(String TRANS_NO, String TRANS_UUID, String ZS_UUID, String CLIENT_ID, String contract_TRANS_NO, Map<String, Object> CONTRACT_DATA,
            List<Map<String, String>> INSD_DATA, List<Map<String, Object>> BENE_DATA, Map<String, Object> TRAL_TMP) throws Exception;
    
    /**
     * [C02] 網投旅平險-核保試算(公會通算)
     * 
     * @param TRANS_NO 網投專站訂單編號
     * @param TRANS_UUID 阿發傳入session ID
     * @param ZS_UUID
     * @param CLIENT_ID 使用者 SYS_NO
     * @param contract_TRANS_NO 傳入網路投保訂單編號
     * @param CONTRACT_DATA 契約內容單筆
     * @param INSD_DATA 投保內容多筆
     * @param BENE_DATA 受益人內容多筆
     * @param TRAL_TMP 業報書單筆
     * @return
     */
    public Map<String, Object> c02WithNoException(String TRANS_NO, String TRANS_UUID, String ZS_UUID, String CLIENT_ID, String contract_TRANS_NO, Map<String, Object> CONTRACT_DATA,
            List<Map<String, String>> INSD_DATA, List<Map<String, Object>> BENE_DATA, Map<String, Object> TRAL_TMP);
    
    /**
     * [C03] 網投旅平險-契約成立
     * 
     * @param ID
     * @return
     * @throws Exception
     */
    public Map<String, Object> c03(String TRANS_NO, String TRANS_UUID, Map<String, Object> contract) throws Exception;
    
    /**
     * [C04] 網投旅平險-不受歡迎旅遊地區API
     * 
     * @param ID
     * @return
     * @throws Exception
     */
    public Map<String, Object> c04(String ID) throws Exception;

    /**
     * [C04] 網投旅平險-不受歡迎旅遊地區API
     * 
     * @param ID
     * @return
     */
    public Map<String, Object> c04WithNoException(String ID);
}
