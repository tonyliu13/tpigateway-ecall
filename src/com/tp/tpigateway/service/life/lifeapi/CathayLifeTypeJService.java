package com.tp.tpigateway.service.life.lifeapi;


import java.util.List;
import java.util.Map;

/**
 * 投商(投資型商品)資訊相關服務介面
 */
public interface CathayLifeTypeJService {

	/**
	 * 單一保單投資型對帳單查詢<BR/>
	 * ({E02}單一保單資訊取得 [INSD_ID,ISSUE_DATE,PAY_KIND,PROD_ID])
	 * 
	 * @param POLICY_NO 保單號碼
	 * @param INSD_ID 被保人ID
	 * @param SRC 資料來源('A')
	 * @param TRY_CNT_DATE 試算日期(系統日期)
	 * @param ISSUE_DATE 投保始期
	 * @param PAY_KIND 繳別
	 * @param PROD_ID 險別
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> j01(String POLICY_NO, String INSD_ID, String SRC, String TRY_CNT_DATE, String ISSUE_DATE, String PAY_KIND, String PROD_ID) throws Exception;
	
	/**
	 * 投資型保單對帳單
	 * 
	 * @param POLICY_NO 保單號碼
	 * @param APC_ID 要保人ID
	 * @param DOWNLOAD_NAME 姓名
	 * @param IP_ADDRS 登入IP
	 * @return
	 * @throws Exception 
	 */
	public Map<String,Object> j02(String POLICY_NO, String APC_ID, String DOWNLOAD_NAME, String IP_ADDRS) throws Exception;

	/**
	 * 查詢單一保單投資型提領明細
	 * @param POLICY_NO
	 * @param TRN_ITEM
	 * @return
	 * @throws Exception
	 */
	public List<Object> j03(String POLICY_NO, String[] TRN_ITEM) throws Exception;
	
	/**
	 * 查詢贖回受理進度
	 * @param ID
	 * @param TYPE
	 * @param ALL
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> j04(String APC_ID, String[] TRN_ITEM, Boolean Q_ALL) throws Exception;
	
	/**
	 * 單一保單投資型商品分類<BR/>
	 * ({E02}單一保單資訊取得 [PROD_ID])
	 * 
	 * @param PROD_ID 險別
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> j05(String PROD_ID) throws Exception;
	
}
