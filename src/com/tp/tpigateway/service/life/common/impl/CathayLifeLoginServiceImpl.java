package com.tp.tpigateway.service.life.common.impl;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.ContentItem;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.life.common.CathayLifeLoginService;
import com.tp.tpigateway.service.life.flow.CathayLifeFlowI0Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 官網登入相關服務實作
 */
@Service("cathayLifeLoginService")
public class CathayLifeLoginServiceImpl implements CathayLifeLoginService {

	private static Logger log = LoggerFactory.getLogger(CathayLifeLoginServiceImpl.class);
	
	@Autowired
	private BotMessageService botMessageService;
	
	@Autowired
	private CathayLifeFlowI0Service cathayLifeFlowI0Service;
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> doI0(BotLifeRequestVO reqVO, String source) throws Exception {
		return (Map<String, Object>) cathayLifeFlowI0Service.queryI0(source, reqVO);
	}
	
	@Override
	public Map<String, Object> doLogin(BotLifeRequestVO reqVO, String source) throws Exception {
		
		Map<String, Object> result = null;
		
		try {
			BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

			List<Map<String, Object>> contentList = new ArrayList<Map<String, Object>>();
			
			/*
			// 回覆內容置換參數
			Map<String, String> replaceParams = new HashMap<String, String>();
			replaceParams.put("SOURCE", source);
			
			// 回覆內容{I1.1}
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "1");
						
			ContentItem contentH = botMessageService.getTextContentItemWithReplace(vo, replyText, replaceParams);
			contentList.add(contentH.getContent());
			vo.setContent(contentList);
			*/
			
			ContentItem contentH = vo.new ContentItem();
			contentH.setType(12);
			contentH.setAction(7);
			contentH.setWidget("doLogin");
			String loginType = reqVO.getLoginType() == null ? "" : reqVO.getLoginType();
			contentH.setLoginType(loginType);
			contentList.add(contentH.getContent());
			vo.setContent(contentList);
			
			/*
			ContentItem content = vo.new ContentItem();
			content.setType(2);
			content.setCType(BotMessageVO.CTYPE_CARD);
			
			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
			
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_250);
			
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CB);
			cImageData.setCImageUrl(BotMessageVO.IMG_LIFE);
			
			cards.setCImageData(cImageData.getCImageDatas());
			
			cards.setCTitle("請先告訴我你是誰唷");
			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
			cards.setCTexts(cTextList);
			
			//link
			String temp = "chatWidge(this,'doLogin',['1','<%SOURCE%>'])";
			String function = botMessageService.replaceTextByParamMap(temp, replaceParams);
			
			cards.setCLinkType(1);
			CLinkListItem cLinkContent = vo.new CLinkListItem();
			cLinkContent.setClAction(5);
			cLinkContent.setClText("驗證身份");
			cLinkContent.setClAlt(function);
			
			List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
			cLinkContentList.add(cLinkContent.getCLinkList());
			cards.setCLinkList(cLinkContentList);
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			cardList.add(cards.getCards());
			content.setCards(cardList);
			
			contentList.add(content.getContent());
			vo.setContent(contentList);
			*/
			
			result = vo.getVO();
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			if (result == null) {
				result = new HashMap<String, Object>();
			}
		}
		
		return result;
	}

	@Override
	public Map<String, Object> showSurvey(BotLifeRequestVO reqVO, String source) throws Exception {

		Map<String, Object> result = null;

		try {
			BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

			List<Map<String, Object>> contentList = new ArrayList<>();

			ContentItem contentH = vo.new ContentItem();
			contentH.setType(6);
			contentList.add(contentH.getContent());
			vo.setContent(contentList);

			result = vo.getVO();
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			if (result == null) {
				result = new HashMap<>();
			}
		}

		return result;
	}
	
}
