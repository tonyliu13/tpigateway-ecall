package com.tp.tpigateway.service.life.common.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.exception.RRException;
import com.tp.tpigateway.model.life.BotChatLog;
import com.tp.tpigateway.repository.BotChatLogRepository;
import com.tp.tpigateway.service.life.common.BotChatLogService;
import com.tp.tpigateway.util.DateUtil;
import com.tp.tpigateway.util.exception.StatusConstants;

@Service("botChatLogService")
@Transactional
public class BotChatLogServiceImpl implements BotChatLogService {

	@Autowired
	private BotChatLogRepository botChatLogRepository;

	@Override
	public Integer addBotChatLog(BotChatLog botChatLog) {
		Date currentDate = DateUtil.getNow();

		botChatLog.setInsertTime(currentDate);
		botChatLogRepository.save(botChatLog);

		return botChatLog.getId();
	}

	@Override
	@Transactional(readOnly = true)
	public BotChatLog getBotChatLogBySessionId(String sessionId) {
		BotChatLog botChatLog = botChatLogRepository.findFirstBySessionIdOrderByInsertTimeDesc(sessionId);
		if (botChatLog == null)
			throw new RRException(StatusConstants.BOT_CHAT_LOG_NOT_FOUND, sessionId);
		return botChatLog;
	}
}
