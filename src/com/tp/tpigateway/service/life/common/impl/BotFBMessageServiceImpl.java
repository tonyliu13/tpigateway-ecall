package com.tp.tpigateway.service.life.common.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;
import com.tp.tpigateway.service.life.common.BotFBMessageService;

@Service("botFBMessageService")
public class BotFBMessageServiceImpl implements BotFBMessageService {

	private static Logger log = LoggerFactory.getLogger(BotFBMessageServiceImpl.class);
	
	@Autowired
	private BotTpiReplyTextService botTpiReplyTextService;
	@Autowired
	private BotMessageService botMessageService;
	
	@SuppressWarnings("unchecked")
	public String getFBForQueryContractListByScenario(BotLifeRequestVO reqVO, List<Object> contractList, Map<String, String> replaceParams) {
		
		// 你2017年12月前尚未入帳的保單有這些，但不含墊繳件、停效件、投資型商品喔！
		// \n\n保單號碼：1005207269
		// \n險別中文名稱：國泰人壽漾安心住院醫療終身保險
		// \n下次應繳日：2017-11-02
		// \n下次應繳保費：NT$25,000
		// \n繳別：年繳
		
		StringBuffer sb = new StringBuffer();
		
		// 回覆內容{I1.2.1}
		String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I1.1.1");
		sb.append(botMessageService.replaceTextByParamMap(replyText, replaceParams));

		// 回覆牌卡
		for (Object obj : contractList) {
			
			Map<String, Object> map = (Map<String, Object>) obj;
			
			sb.append("\n\n保單號碼：");
			sb.append(MapUtils.getString(map, "POLICY_NO"));
			sb.append("\n險別中文名稱：");
			sb.append(MapUtils.getString(map, "PROD_NAME"));
			sb.append("\n下次應繳日：");
			sb.append(MapUtils.getString(map, "NXT_PAY_DATE"));
			sb.append("\n下次應繳保費：");
			sb.append(MapUtils.getString(map, "PREM_RCPT"));
			sb.append("\n繳別：");
			sb.append(MapUtils.getString(map, "PAY_FREQ_NM"));
		}
		
		log.debug("ContractList=" + sb.toString());
		
		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getFBForI1_4_2(BotLifeRequestVO reqVO, List<Object> contractPayList, Map<String, String> replaceParams) {

		// 意外險保單目前保費墊繳中，保單仍然有效力。最近一期到近六期繳費紀錄如下：
		// \n\n應繳日：2018-01-01 
		// \n繳次：1
		// \n繳費日期：2018-01-01 
		// \n繳費金額：NT$100
		// \n繳費管道：信用卡
		// \n授權人姓名：王大明
		// \n發卡銀行：台新銀行
		// \n信用卡號末四碼：1234
		// \n\n提醒☝️若你有收到催繳通知單，記得在催繳交寄日次日起30日內，清償墊繳金額，以免保單停效喔~
		StringBuffer sb = new StringBuffer();
		
		String replyText21 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I3.1.1");
		sb.append(botMessageService.replaceTextByParamMap(replyText21, replaceParams));

		// 回覆牌卡
		for (Object obj : contractPayList) {

			Map<String, Object> map = (Map<String, Object>) obj;

			sb.append("\n\n應繳日：");
			sb.append(MapUtils.getString(map, "pay_date_pol"));
			sb.append("\n繳次：");
			sb.append(MapUtils.getString(map, "pay_times"));
			sb.append("\n繳費日期：");
			sb.append(MapUtils.getString(map, "pay_date_clc"));
			sb.append("\n繳費金額：");
			sb.append(MapUtils.getString(map, "tot_prem"));
			sb.append("\n繳費管道：");
			sb.append(MapUtils.getString(map, "pay_way_NM"));
			sb.append("\n授權人姓名：");
			sb.append(MapUtils.getString(map, "AUTH_NAME"));
			sb.append("\n發卡銀行：");
			sb.append(MapUtils.getString(map, "BANK_NAME_CARD"));
			sb.append("\n信用卡號末四碼：");
			sb.append(MapUtils.getString(map, "CARD_NO"));
		}
		
		String replyText23 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I3.1.3");
		sb.append("\n\n" + replyText23);
		
		return sb.toString();
	}
}
