package com.tp.tpigateway.service.life.common;

import com.tp.tpigateway.model.life.BotChatLog;

public interface BotChatLogService {

	 public Integer addBotChatLog(BotChatLog botChatLog);

	 public BotChatLog getBotChatLogBySessionId(String sessionId);
}
