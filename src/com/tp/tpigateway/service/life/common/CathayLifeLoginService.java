package com.tp.tpigateway.service.life.common;

import com.tp.tpigateway.model.life.BotLifeRequestVO;

import java.util.Map;

/**
 * 官網登入相關服務介面
 */
public interface CathayLifeLoginService {

	public Map<String, Object> doLogin(BotLifeRequestVO reqVO, String source) throws Exception;


	public Map<String, Object> showSurvey(BotLifeRequestVO reqVO, String source) throws Exception;

	/**
	 * init(I0)
	 * 
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> doI0(BotLifeRequestVO reqVO, String source) throws Exception;
	
}
