package com.tp.tpigateway.service.life.common;

import java.util.List;
import java.util.Map;

import com.tp.tpigateway.model.life.BotLifeRequestVO;

public interface BotFBMessageService {

	public String getFBForQueryContractListByScenario(BotLifeRequestVO reqVO, List<Object> contractList, Map<String, String> replaceParams);
	
	public String getFBForI1_4_2(BotLifeRequestVO reqVO, List<Object> contractPayList, Map<String, String> replaceParams);
}
