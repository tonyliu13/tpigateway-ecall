package com.tp.tpigateway.service.life.flow.cb;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * CBA10001：旅平險投保資料處理服務介面
 */
public interface CBA10001DataService {

    /**
     * 產生C01契約內容單筆
     * 
     * @param ISSUE_DATE
     * @param ISSUE_DAYS
     * @param BGN_TIME
     * @param RGN_CODE
     * @return
     */
    public Map<String, String> genC01_CONTRACT_DATA(String ISSUE_DATE, String ISSUE_DAYS, String BGN_TIME, String RGN_CODE);
    
    /**
     * 產生C01投保內容多筆
     * 
     * @param RGN_CODE
     * @param HD_AMT 死殘保額 單位 萬  (意外身故、失能)
     * @param HP_AMT 實支實付保額 單位 萬  (傷害醫療保險金 )
     * @param HK_AMT 海外突發保額 單位 萬  (突發疾病醫療保險金 )
     * @return
     */
    public List<Map<String, String>> genC01_INSD_DATA(String RGN_CODE, String HD_AMT, String HP_AMT, String HK_AMT,boolean IS_OMTP);
    
    /**
     * 由c01Result取得總保費
     * 
     * @param c01Result
     * @return
     */
    public String getTOT_PREM(Map<String, Object> c01Result);
    
    /**
     * 檢查是否登入者年齡65歲以上
     * 
     * @param a30Result
     * @return
     * @throws Exception 
     */
    public boolean checkOver65(Map<String, Object> a30Result) throws Exception;
    
    /**
     * TODO: 取得旅遊地點(RGN_CODE)<br>
     * 旅遊地點代碼 - <br>
     * "1": "國內", "2": "中國大陸(含香港、澳門地區)", "4": "其他", "5": "日本", "6": "美加", 
     * "7": "歐洲非申根國家", "9": "紐澳", "11": "歐洲申根國家", "14": "韓國","15": "越南", 
     * "16": "新加坡", "17": "菲律賓", "18": "印尼", "19": "馬來西亞", "20": "緬甸", "21": "泰國", 
     * "22": "寮國", "23": "柬埔寨"
     * 
     * @param CBA_ORIG_STR
     * @param CBA_PLACEs
     * @return
     * @throws Exception 
     */
    public Map<String, Object> getRGN_CODE(String CBA_ORIG_STR, List<Object> CBA_PLACEs) throws Exception;
    
    /**
     * 取得旅遊地點中文(RGN_NAME)
     * 
     * @param RGN_CODE
     * @return
     */
    public String getRGN_NAME(String RGN_CODE);
    
    /**
     * 取得投保始期日期(ISSUE_DATE)、投保始期時間(BGN_TIME)、投保天數(ISSUE_DAYS)、起日(MIN_DATE)、迄日(MAX_DATE)<br>
     * 1. 起日(MIN_DATE)、迄日(MAX_DATE)為原始起迄日期<br>
     * 2. 保始期日期(ISSUE_DATE)、投保始期時間(BGN_TIME)須視需要重新產生
     * 
     * @param CBA_DATEs
     * @return
     */
    public Map<String, Object> getISSUE_DAY(String sessionId, List<Object> CBA_DATEs);
    
    /**
     * 取得投保開始時間
     * 若是今天-(1.2.可取跨日)
     * 1.若大於50分，取下一小時30分
     * 2.若大於20分，取即將到的整點
     * 3.否則取當小時30分
     * 若是他日-
     * 4.若現在距他日不足10分鐘，取他日零點30分
     * 5.否則-取零點整
     * 
     * @param ISSUE_DATE、BGN_TIME
     * @return
     */
    public Map<String, String> getBGN_TIME(Date MIN_DATE);
    
    /**
     * 取得投保期間日期時間字串<br>
     * ps. 迄止若時間為零點整點則回傳24:00
     * 
     * @param MIN_DATE
     * @param ISSUE_DAYS
     * @return
     */
    public Map<String, String> getDateTime(Date MIN_DATE, String ISSUE_DAYS);
    
    /**
     * 取得投保期間日期時間字串<br>
     * ps. 迄止若時間為零點整點則回傳24:00
     * 
     * @param MIN_DATE
     * @param ISSUE_DAYS
     * @param MIN_TIME
     * @return
     */
    public Map<String, String> getDateTime(Date MIN_DATE, String ISSUE_DAYS, String MIN_TIME);
    
    /**
     * TODO: 檢查投保期間
     * 
     * @param RGN_CODE
     * @param ISSUE_DATE
     * @param ISSUE_DAYS
     * @return true:可接受, false:不接受
     */
    public boolean checkISSUE_Period(String RGN_CODE, String ISSUE_DATE, String ISSUE_DAYS);
    
    /**
     * TODO: 產生契約內容單筆
     * 
     * @param a30Data
     * @param g06Data
     * @param ZIPCODE
     * @param ADDRESS
     * @param RGN_CODE
     * @param ISSUE_DATE
     * @param ISSUE_DAYS
     * @param BGN_TIME
     * @return
     */
    @SuppressWarnings("rawtypes")
    public Map<String, Object> genC02_CONTRACT_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS, String RGN_CODE, String ISSUE_DATE, String ISSUE_DAYS, String BGN_TIME);
    
    /**
     * TODO: 產生投保內容多筆
     * ps. 阿發投保旅平險「被保人」同「要保人」
     * 
     * @param a30Data
     * @param g06Data
     * @param ZIPCODE
     * @param ADDRESS
     * @param RGN_CODE
     * @param HD_AMT
     * @param HP_AMT
     * @param HK_AMT
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<Map<String, String>> genC02_INSD_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS, String RGN_CODE, String HD_AMT, String HP_AMT, String HK_AMT, boolean IS_OMTP);
    
    /**
     * TODO: 產生契約內容單筆
     * 
     * @param a30Data
     * @param g06Data
     * @param ZIPCODE
     * @param ADDRESS
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<Map<String, Object>> genC02_BENE_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS);
    
    /**
     * TODO: 產生業報書單筆
     * 
     * @param a30Data
     * @param g06Data
     * @return
     */
    @SuppressWarnings("rawtypes")
    public Map<String, Object> genC02_TRAL_TMP(Map a30Data, Map g06Data);
    
    /**
     * 檢查日期是否為今天
     * 
     * @param date
     * @return
     */
    public boolean checkToday(Date date);
    
    /**
     * 取得C04結果<br>
     * 1.先由cache中取出<br>
     * 2.由呼叫C04取回
     * 
     * @param ID
     * @return
     * @throws Exception
     */
    public Map<String, Object> getC04DataById(String ID) throws Exception;
    
    /**
     * 取得A04預設國籍TW結果<br>
     * 1.先由cache中取出<br>
     * 2.由呼叫A04取回
     * 
     * @param ID
     * @return
     * @throws Exception
     */
    public Map<String, Object> getA04WithDefaultTWDataById(String ID) throws Exception;
    
    /**
     * 取得G06結果<br>
     * 1.先由cache中取出<br>
     * 2.由呼叫G06取回
     * 
     * @param ID
     * @return
     * @throws Exception
     */
    public Map<String, Object> getG06DataById(String ID) throws Exception;
    
    /**
     * 取得A30結果<br>
     * 1.先由cache中取出<br>
     * 2.由呼叫A30取回
     * 
     * @param ID
     * @return
     * @throws Exception
     */
    public Map<String, Object> getA30WithDefaultTWDataById(String ID) throws Exception;
    
    /**
     * 取得旅遊地點結果中「地點名稱、地點類型代碼」
     * 
     * @return
     * @throws Exception
     */
    public Map<String, Object> getAllBotTouristPlace() throws Exception;
    
    
    /**
     * 取得旅遊地點比對Pattern(Regex用)
     * 
     * @return
     * @throws Exception
     */
    public String getPlacePattern() throws Exception;
    
    /**
     * 產生「網投旅平險訂單狀態更新」呼叫參數字串
     * 
     * @param tmpData
     * @param STATUS
     * @return
     */
    @SuppressWarnings({ "rawtypes" })
    public String genG07PaymentJSON(Map tmpData, String STATUS);
    
    /**
     * 產生「網投旅平險-發送交易成功通知」呼叫參數字串
     * 
     * @param a30Data
     * @param d20Data
     * @param c03Data
     * @param SYS_NO
     * @param TRANS_NO
     * @return
     */
    @SuppressWarnings("rawtypes")
    public String genG08FinishInfoJSON(Map a30Data, Map d20Data, Map c03Data, String SYS_NO, String TRANS_NO);
    
    /**
     * 產生「LINE分享給親友」字串
     * 
     * @param param
     * @param a30Data
     * @param c03Data
     * @return
     * @throws UnsupportedEncodingException
     */
    @SuppressWarnings("rawtypes")
    public String genLineShareText(Map param, Map a30Data, Map c03Data) throws UnsupportedEncodingException;
    
    public String getCacheTRANS_NO(String cacheKey, String ID) throws Exception;
    
    public void setCacheTRANS_NO(String cacheKey, String ID, String TRANS_NO, int expiredTime) throws Exception;
    
    public void removeCacheTRANS_NO(String cacheKey, String ID);
    
    public String getCacheCompletedTRANS_NO(String cacheKey, String ID);
    
    public void setCacheCompletedTRANS_NO(String cacheKey, String ID, String TRANS_NO, int expiredTime);
    
    public Map<String, Object> getCacheTRANS_DATA(String cacheKey, String ID);
    
    public void setCacheTRANS_DATA(String cacheKey, String ID, Map<String, Object> tmpData, int expiredTime);
    
    public Map<String, Object> getCacheCALL_ID_DATA(String cacheKey, String ID);
    
    public void setCacheCALL_ID_DATA(String cacheKey, String ID, Map<String, Object> callIdData, int expiredTime);
    
    public Map<String, Object> getCacheC02_DATA(String cacheKey, String ID);
    
    public void setCacheC02_DATA(String cacheKey, String ID, Map<String, Object> c02Data, int expiredTime);
    
    public Map<String, Object> getCacheD20_DATA(String cacheKey, String ID);
    
    public void setCacheD20_DATA(String cacheKey, String ID, Map<String, Object> d20Data, int expiredTime);
    
    public Map<String, Object> getCacheD21_DATA(String cacheKey);
    
    public void setCacheD21_DATA(String cacheKey, Map<String, Object> d21Data, int expiredTime);
    
    public String getCacheZ01_KEY(String cacheKey, String ID);
    
    public void setCacheZ01_KEY(String cacheKey, String ID, String Z01_KEY, int expiredTime);
    
    public Map<String, Object> getCacheTOURIST_DATA(String cacheKey);
    
    public void setCacheTOURIST_DATA(String cacheKey, Map<String, Object> touristData, int expiredTime);

	boolean getOMTP(String RGN_CODE) throws Exception;

}
