package com.tp.tpigateway.service.life.flow.cb;

import java.util.List;
import java.util.Map;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;

/**
 * CBA10001：旅平險投保牌卡組裝服務介面
 */
public interface CBA10001CardService {

    /**
     * CBA10001：怎麼投保旅平險<br>
     * CBA10001.1.1 已核身<br>
     * card {CBA10001.1.1.2}
     * 
     * @param vo
     * @param a04Result
     * @param c04Result
     * @return
     */
    public List<Map<String, Object>> getCardsForCBA10001_1_1_2(BotMessageVO vo, Map<String, Object> a04Result, Map<String, Object> c04Result);

    /**
     * CBA10001：怎麼投保旅平險<br>
     * CBA10001.1.2 未核身<br>
     * card {CBA10001.1.2.2}
     * 
     * @param vo
     * @return
     */
    public List<Map<String, Object>> getCardsForCBA10001_1_2_2(BotMessageVO vo);

    /**
     * CBA10001_5X1：輸入旅遊問句<br>
     * CBA10001.5X1.5 (地點及日期皆取得)<br>
     * 套餐-XX方案 牌卡
     * 
     * @param vo
     * @return
     */
    public CardItem getCardForCBA10001_5X1_5(BotMessageVO vo, String PLAN_NAME, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME, String MAX_DATE_TIME, String[] INSD_DATA,
            String TOT_PREM, String CALL_ID, boolean IS_OMTP);

    /**
     * CBA10001_5X1：輸入旅遊問句<br>
     * CBA10001.5X1.5 (地點及日期皆取得)<br>
     * 其他投保方案
     * 
     * @param vo
     * @param IDNo
     * @param RGN_CODE
     * @param ISSUE_DAYS
     * @param ISSUE_DATE
     * @param MAIN_AMT
     * @param HP_AMT
     * @param HK_AMT
     * @return
     */
    public CardItem getCard2ForCBA10001_5X1_5(BotMessageVO vo, String IDNo, String RGN_CODE, String ISSUE_DAYS, String ISSUE_DATE, String MAIN_AMT, String HP_AMT, String HK_AMT);

    /**
     * CBA10001_5X1_5_1：確認投保<br>
     * CBA10001.5X1.5.1.1 繳費前確認<br>
     * TODO: card {CBA10001.5X1.5.1.1.1}
     * 
     * @param vo
     * @param APC_NAME 要保人姓名
     * @param RGN_CODE 旅遊地點
     * @param ISSUE_DAYS 旅遊天數
     * @param MIN_DATE_TIME 旅遊開始日期時間
     * @param MAX_DATE_TIME 旅遊結束日期時間
     * @param BENE_NAME
     * @param INSD_DATA
     * @param TOT_PREM 總保費
     * @param CALL_ID
     * @param TOURIST_ID
     * @param APC_YEAR_INCOME 年收入
     * @return
     */
    public List<Map<String, Object>> getCardsForCBA10001_1_1_1_1_1_5_1_1_1(BotMessageVO vo, String APC_NAME, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME, String MAX_DATE_TIME,
            String BENE_NAME, String[] INSD_DATA, int TOT_PREM, String CALL_ID, String TOURIST_ID, String APC_YEAR_INCOME,boolean IS_OMTP);
    
    /**
     * TODO: card {CBA10001.5X1.5.1.3.1.1}
     * 
     * @param vo
     * @param APC_NAME
     * @param RGN_CODE 旅遊地點
     * @param ISSUE_DAYS 旅遊天數
     * @param MIN_DATE_TIME 旅遊開始日期時間
     * @param MAX_DATE_TIME 旅遊結束日期時間
     * @param BENE_NAME
     * @param INSD_DATA
     * @param TOT_PREM 總保費
     * @param CALL_ID
     * @param TOURIST_ID
     * @param APC_YEAR_INCOME 年收入
     * @return
     */
    public List<Map<String, Object>> getCardsForCBA10001_1_1_1_1_1_5_1_3_1_1(BotMessageVO vo, String APC_NAME, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME, String MAX_DATE_TIME,
            String BENE_NAME, String[] INSD_DATA, int TOT_PREM, String CALL_ID, String TOURIST_ID, String APC_YEAR_INCOME, boolean IS_OMTP);
    
    /**
     * CBA10001_5X1_5_5X1：繳費完成<br>
     * card {CBA10001.5X1.5.6X1.1.1}
     * 
     * @param vo
     * @param CARD4
     * @param TOT_PREM
     * @param a30Data
     * @param POLICY_NO
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<Map<String, Object>> getCardsForCBA10001_5X1_5_6X1_1_1(BotMessageVO vo, String CARD4, String TOT_PREM, Map a30Data, String POLICY_NO);
}
