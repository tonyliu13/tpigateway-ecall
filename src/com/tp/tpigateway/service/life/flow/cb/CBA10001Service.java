package com.tp.tpigateway.service.life.flow.cb;

import java.util.Map;

import com.tp.tpigateway.model.life.BotLifeRequestVO;

/**
 * CBA10001：旅平險投保服務介面
 */
public interface CBA10001Service {

	/**
	 * CBA10001：怎麼投保旅平險
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001(String source, BotLifeRequestVO reqVO) throws Exception;

	/**
	 * CBA10001_1_1_1：試算投保旅平險
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception;

	/**
	 * CBA10001_1_1_1_1_1：輸入旅遊問句<br>
	 * ps. 輸入「4/4~4/13 到日本玩」依照客戶輸入問句取得[旅遊地點]及[旅遊日期]，少一條件走不同檢核點。
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception;

	/**
	 * CBA10001_1_1_1_1_1_5_1：確認投保
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1_1_1_5_1(String source, BotLifeRequestVO reqVO) throws Exception;
	
	/**
	 * CBA10001_1_1_1_1_1_5_2：修改出發時間
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1_1_1_5_2(String source, BotLifeRequestVO reqVO) throws Exception;
	
	/**
	 * CBA10001_1_1_1_1_1_5_2_X：輸入出發時間
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1_1_1_5_2_X(String source, BotLifeRequestVO reqVO) throws Exception;

	/**
	 * CBA10001_1_1_1_1_1_5_1_3：輸入地址
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1_1_1_5_1_3(String source, BotLifeRequestVO reqVO) throws Exception;

	/**
	 * CBA10001_1_1_1_1_1_5_1_1_1_1：繳費步驟
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1_1_1_5_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception;

    /**
     * CBA10001_1_1_1_1_1_5_1_1_2：修改投保資訊
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    public Object queryCBA10001_1_1_1_1_1_5_1_1_2(String source, BotLifeRequestVO reqVO) throws Exception;
    
    /**
     * CBA10001_1_1_1_1_1_5_1_1_1_1：投保操作時間過久
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    public Object queryCBA10001_1_1_1_1_1_5_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception;

	/**
	 * CBA10001_1_1_1_1_1_5_1_1_1_1_1：繳費完成
	 * 
	 * @param source
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public Object queryCBA10001_1_1_1_1_1_5_1_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception;
	
	/**
	 * 執行D20扣款
	 * 
	 * @param reqVO
	 * @param TOURIST_ID
	 * @param CARD_NO
	 * @param CARD_END_YM
	 * @param TRANS_NO
     * @return
	 * @throws Exception
	 */
	public Map<String, String> doCombinePAY(BotLifeRequestVO reqVO, String TOURIST_ID, String CARD_NO, String CARD_END_YM, String TRANS_NO) throws Exception;

}
