package com.tp.tpigateway.service.life.flow.cb;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.CathayBaseResponse;
import com.tp.tpigateway.model.life.CBV10001VO.CheckAmtVoiceData;
import com.tp.tpigateway.model.life.CBV10001VO.ConfirmVoiceData;
import com.tp.tpigateway.model.life.CBV10001VO.LastIssueData;
import com.tp.tpigateway.model.life.CBV10001VO.LawAgeData;
import com.tp.tpigateway.model.life.CBV10001VO.TempVoiceData;

/**
 * 呼叫國泰Api服務介面
 * @author Wayne Tsai
 *
 */
public interface CBV10001ApiService {
	
	public ResponseEntity<CathayBaseResponse<LawAgeData>> lawAge(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request);
	
	public ResponseEntity<CathayBaseResponse<LastIssueData>> lastIssueData(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request);
	
	public ResponseEntity<CathayBaseResponse<?>> allVipData(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request);
	
	public ResponseEntity<CathayBaseResponse<ConfirmVoiceData>> doConfirmVoice(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request);
	
	public ResponseEntity<CathayBaseResponse<TempVoiceData>> insertTempVoice(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request);
	
	public ResponseEntity<CathayBaseResponse<CheckAmtVoiceData>> doCheckAmtVoice(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request);
}
