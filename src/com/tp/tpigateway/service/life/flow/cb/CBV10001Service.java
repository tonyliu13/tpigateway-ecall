package com.tp.tpigateway.service.life.flow.cb;

import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.ContentAllVipData;
import com.tp.tpigateway.model.life.CBV10001VO.ContentCheckAmtVoice;
import com.tp.tpigateway.model.life.CBV10001VO.ContentConfirmVoice;
import com.tp.tpigateway.model.life.CBV10001VO.ContentGetRgnCode;
import com.tp.tpigateway.model.life.CBV10001VO.ContentLastIssueData;
import com.tp.tpigateway.model.life.CBV10001VO.ContentLawAge;
import com.tp.tpigateway.model.life.CBV10001VO.ContentTempVoice;
import com.tp.tpigateway.model.life.CBV10001VO.FlowCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.FlowRequest;
import com.tp.tpigateway.model.life.CBV10001VO.GatewayResponse;

public interface CBV10001Service {

    public GatewayResponse<ContentLawAge> lawAge(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D);
    public GatewayResponse<ContentLastIssueData> lastIssueData(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D);
    public GatewayResponse<ContentAllVipData> allVipData(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D);
    public GatewayResponse<ContentConfirmVoice> doConfirmVoice(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D);
    public GatewayResponse<ContentTempVoice> insertTempVoice(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D);
    public GatewayResponse<ContentCheckAmtVoice> doCheckAmtVoice(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D);
    public GatewayResponse<ContentGetRgnCode> getRgnCode(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D);
    
	public GatewayResponse<?> saveFlowCallLog3D(FlowCallLog3D flowCallLog3D);
}
