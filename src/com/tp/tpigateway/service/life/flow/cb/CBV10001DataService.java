package com.tp.tpigateway.service.life.flow.cb;

import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.FlowCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.IdMappingEasycall;

/**
 * 處理CBV10001資料庫存取的介面
 */
public interface CBV10001DataService {

    public void saveFlowCallLog3D(FlowCallLog3D flowCallLog3D);
	
    public void saveIdMappingEasyCall(IdMappingEasycall idMappingEasyCall);
    
    public void updateIdMappingEasyCall(IdMappingEasycall idMappingEasyCall);
    
    public void saveOrUpdateIdMappingEasyCall(IdMappingEasycall idMappingEasyCall);
    
    public IdMappingEasycall findIdMappingEasycallBySessionId(String sessionId);
    
    public void saveBotTpiCallLog3D(BotTpiCallLog3D botTpiCallLog3D);
    
}
