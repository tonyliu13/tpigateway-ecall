package com.tp.tpigateway.service.life.flow;

import com.tp.tpigateway.model.life.BotLifeRequestVO;

public interface CathayLifeFlowI0Service {

	public Object queryI0(String source, BotLifeRequestVO reqVO) throws Exception;
	
}
