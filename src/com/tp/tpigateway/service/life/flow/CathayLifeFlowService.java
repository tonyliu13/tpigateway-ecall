package com.tp.tpigateway.service.life.flow;

import java.util.List;
import java.util.Map;

import com.tp.tpigateway.model.common.APIResult;
import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotReplyContent;
import com.tp.tpigateway.model.life.BotLifeRequestVO;

/**
 * 定義共用流程界面
 * 
 * @author i9301212
 *
 */
public interface CathayLifeFlowService {

	/**
	 * 依據NodeID產生ChatBot回應内容，一般回答内容可以透過讀取設定檔自動產生結果
	 * 
	 * @param source
	 * @param reqVO
	 * @param nodeID
	 * @return BotMessageVO
	 * @throws Exception
	 */
	public BotMessageVO genResponse(String source, BotLifeRequestVO reqVO, APIResult apiResult) throws Exception;

	/**
	 * 依據NodeID產生ChatBot設定必要輸入參數
	 * 
	 * @param reqVO
	 * @param nodeID
	 * @return 必要輸入參數
	 * @throws Exception
	 */
	public Map<String, String> getRequireParams(BotLifeRequestVO reqVO, String nodeID) throws Exception;

	/**
	 * 依據NodeID產生API結果
	 * 
	 * @param reqVO
	 * @param source
	 * @param nodeID
	 * @return 透過Key值設定不同的API結果，依據API結果有不同條件的回應内容， [RespNodeID]
	 * @throws Exception
	 */
	public APIResult getData(BotLifeRequestVO reqVO, String source, String nodeID) throws Exception;

	/**
	 * 較複雜的回應内容，自行定義如何產生客制化牌卡内容
	 * 
	 * @param vo
	 * @param source
	 * @param respNodeID
	 * @param apiResult
	 * @return
	 */
	public List<Map<String, Object>> getCustomCards(BotLifeRequestVO reqVO, String source, BotMessageVO vo, APIResult apiResult,
			String optionID);

	/**
	 * 使用DB設定的資料產生牌卡
	 * 
	 * @param vo
	 * @param apiResult
	 * @param botReplyContent
	 * @return
	 */
	public List<Map<String, Object>> getCardsByDatabaseConfig(BotMessageVO vo, APIResult apiResult, BotReplyContent botReplyContent);

}
