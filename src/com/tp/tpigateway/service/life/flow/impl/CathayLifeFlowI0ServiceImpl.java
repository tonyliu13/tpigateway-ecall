package com.tp.tpigateway.service.life.flow.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;
import com.tp.tpigateway.service.life.flow.CathayLifeFlowI0Service;

@Service("cathayLifeFlowI0Service")
public class CathayLifeFlowI0ServiceImpl implements CathayLifeFlowI0Service {
	
	@Autowired
	private BotTpiReplyTextService botTpiReplyTextService;
	@Autowired
	private BotMessageService botMessageService;

	private static Logger log = LoggerFactory.getLogger(CathayLifeFlowI0ServiceImpl.class);

	@Override
	public Object queryI0(String source, BotLifeRequestVO reqVO) throws Exception {
		// FaceBook's BotMessage
		if (reqVO.isFaceBook()) {
			return null;
			// ChatWeb's BotMessage
		} else {
			return getChatWebForI0(source, reqVO);
		}
	}
	
	private Map<String, Object> getChatWebForI0(String source, BotLifeRequestVO reqVO) {
		BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
		
		// showType=2 時顯示快速回覆
		if (StringUtils.equals("2", reqVO.getShowType())) {
			log.info("#############I0.2##############");
			
			// 回覆內容{I0.2.1}
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I0.2.1");
			botMessageService.setTextResult(vo, replyText);
			
			// I0.2.2
			LifeLink[] links = { LifeLink.有, LifeLink.沒有了 };
			botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
		        
		// showType=3 時「兩個流程交錯時會發生聽不懂」回覆
		} else if (StringUtils.equals("3", reqVO.getShowType())) {
			log.info("#############I0.2##############");
			
			// 回覆內容{I0.3.1}
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I0.3.1");
			botMessageService.setTextResult(vo, replyText);
			
			// I0.3.2
			LifeLink[] links = { LifeLink.有, LifeLink.沒有了 };
			botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
		        
		// 20191126 by Todd，增加參數不顯示打招呼牌卡
		// showType=4 時不顯示牌卡
		} else if (StringUtils.equals("4", reqVO.getShowType())) {
		    log.info("#############showType=4##############");
		    
		    // 回覆內容{I0.0.2}
            String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I0.0.2");
            botMessageService.setTextResult(vo, replyText);
		} else {
			log.info("#############I0##############");
			//人壽 BotController.i0 增參數非必要參數 showType=1 時只回覆顯示牌卡
			if (!"1".equals(reqVO.getShowType())) {
				// sticker 打招呼貼圖
				// botMessageService.setImageResult(vo, BotMessageVO.HELLO_PIC);
				// 20181211 by todd, 比照銀行作法，打招呼貼圖改由前端js輸出，對話紀錄也將不存在此貼圖
				// botMessageService.setImageResult(vo, "sticker_hello.gif"); // by todd, 2018/8/20由於怎麼改BotMessageVO中變數值都無法生效，decompiler也甚至是註解掉的，故hard code
				
				// 回覆內容{I0.0.2}
				String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I0.0.2");
				botMessageService.setTextResult(vo, replyText);
				log.debug("i0 ShowType=" + reqVO.getShowType() + ", imageUrl=" + vo.getContent().get(0).get("imageUrl"));
			} else {
				// 回覆內容{I0.0.3}
				String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "I0.0.3");
				botMessageService.setTextResult(vo, replyText);	
			}
			
			// 回覆牌卡
			List<Map<String, Object>> cardList = botMessageService.getCardForI0_1_1(vo);
			botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
		}
		
		return vo.getVO();
	}

}
