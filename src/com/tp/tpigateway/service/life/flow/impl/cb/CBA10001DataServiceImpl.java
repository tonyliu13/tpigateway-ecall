package com.tp.tpigateway.service.life.flow.impl.cb;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.BotTouristPlace;
import com.tp.tpigateway.model.common.CBALink;
import com.tp.tpigateway.model.common.enumeration.A30;
import com.tp.tpigateway.model.common.enumeration.C02;
import com.tp.tpigateway.model.common.enumeration.C03;
import com.tp.tpigateway.model.common.enumeration.C04;
import com.tp.tpigateway.model.common.enumeration.D20;
import com.tp.tpigateway.model.common.enumeration.G06;
import com.tp.tpigateway.repository.BotTouristPlaceRepository;
import com.tp.tpigateway.service.common.CallApiService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001DataService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeAService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeCService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeGService;
import com.tp.tpigateway.util.ApiLogUtils;
import com.tp.tpigateway.util.DateUtil;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.LocalCache;
import com.tp.tpigateway.util.LocalCache.ExpiredTime;

/**
 * CBA10001：旅平險投保資料處理服務實作
 */
//@Service("cBA10001DataService")
public class CBA10001DataServiceImpl implements CBA10001DataService {

    private static Logger log = LoggerFactory.getLogger(CBA10001DataServiceImpl.class);

    @Autowired
    private CallApiService callApiService;
    @Autowired
    private CathayLifeTypeAService cathayLifeTypeAService;
    @Autowired
    private CathayLifeTypeCService cathayLifeTypeCService;
    @Autowired
    private CathayLifeTypeGService cathayLifeTypeGService;
    @Autowired
    private BotTouristPlaceRepository botTouristPlaceRepository;

    @Override
    public Map<String, String> genC01_CONTRACT_DATA(String ISSUE_DATE, String ISSUE_DAYS, String BGN_TIME, String RGN_CODE) {
        Map<String, String> CONTRACT_DATA = new HashMap<String, String>();
        CONTRACT_DATA.put("ISSUE_DATE", ISSUE_DATE);
        CONTRACT_DATA.put("ISSUE_DAYS", ISSUE_DAYS);
        CONTRACT_DATA.put("BGN_TIME", BGN_TIME);
        CONTRACT_DATA.put("RGN_CODE", RGN_CODE);
        return CONTRACT_DATA;
    }

    @Override
    public List<Map<String, String>> genC01_INSD_DATA(String RGN_CODE, String HD_AMT, String HP_AMT, String HK_AMT,boolean IS_OMTP) {
        List<Map<String, String>> INSD_DATA = new ArrayList<Map<String, String>>();
        Map<String, String> map = new HashMap<String, String>();
        map.put("HD_AMT", HD_AMT);
        map.put("HP_AMT", HP_AMT);
        if (!"1".equals(RGN_CODE)) {
            map.put("HK_AMT", HK_AMT);
        }
        if(IS_OMTP) 
        	map.put("OMTP_AMT", "1");
        else 
        	map.put("OMTP_AMT", "0");
        
        INSD_DATA.add(map);
        return INSD_DATA;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public String getTOT_PREM(Map<String, Object> c01Result) {

        String TOT_PREM = null;

        try {
            if (c01Result != null) {
                String resultCode = MapUtils.getString(c01Result, "resultCode");
                if ("000".equals(resultCode)) { // 000:成功呼叫完成 
                    // 回傳資料 
                    Map data = MapUtils.getMap(c01Result, "data");
                    if (data != null) {
                        Map O_CONTRACT_DATA = MapUtils.getMap(data, "CONTRACT_DATA");
                        if (O_CONTRACT_DATA != null) { // 總保費 
                            String O_TOT_PREM = MapUtils.getString(O_CONTRACT_DATA, "TOT_PREM");
                            if (StringUtils.isNotBlank(O_TOT_PREM) && NumberUtils.isDigits(O_TOT_PREM)) {
                                TOT_PREM = O_TOT_PREM;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("由c01Result取得總保費發生錯誤：" + e.getMessage());
        }

        return TOT_PREM;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean checkOver65(Map<String, Object> a30Result) throws Exception {

        boolean result = false;

        if (MapUtils.isNotEmpty(a30Result)) {
            String a30ResultCode = MapUtils.getString(a30Result, A30.resultCode.name());
            if ("000".equals(a30ResultCode)) {
                Map a30Data = MapUtils.getMap(a30Result, A30.data.name());
                String APC_BRDY = MapUtils.getString(a30Data, A30.apc_brdy.name());
                Date brdy = DateUtil.getDateByPattern(APC_BRDY, DateUtil.PATTERN_DASH_DATE);
                int age = getAge(brdy);
                if (age >= 65) {
                    result = true;
                }
            }
        }

        return result;
    }

    private int getAge(Date birthDay) throws Exception {

        Calendar cal = Calendar.getInstance();

        if (cal.before(birthDay)) {
            return -1;
        }

        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);

        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;

        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth)
                    age--;
            } else {
                age--;
            }
        }
        return age;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, Object> getRGN_CODE(String CBA_ORIG_STR, List<Object> CBA_PLACEs) throws Exception {

        Map<String, Object> result = new HashMap<String, Object>();
        
        List<String> RGN_PLACEs = new ArrayList<String>();
        Map<String, String> RGN_CODEs = new HashMap<String, String>();
        Map<String, String> RGN_NAMEs = new HashMap<String, String>();

        // 1. CBA_PLACEs，由NLU回傳結果組成
        if (CollectionUtils.isNotEmpty(CBA_PLACEs)) {
            for (Object obj : CBA_PLACEs) {
                if (obj != null && obj instanceof Map) {
                    Map CBA_PLACE = (Map) obj;
                    String entity = MapUtils.getString(CBA_PLACE, "entity");
                    String text = MapUtils.getString(CBA_PLACE, "text");
                    String info = MapUtils.getString(CBA_PLACE, "info");
                    if (StringUtils.isNotBlank(entity) && StringUtils.isNotBlank(text) && StringUtils.isNotBlank(info)) {
                        String tmp = null;
                        /*if (StringUtils.endsWith(entity, "台灣行政區")) {
                            tmp = "1";
                        } else */
                        if (StringUtils.endsWith(entity, "世界國家城市")) {
                            tmp = getCBA_CODE(info);
                        }
                        if (StringUtils.isNotBlank(tmp)) {
                            String RGN_NAME = getRGN_NAME(tmp);
                            if (StringUtils.isNotBlank(tmp) && !RGN_PLACEs.contains(text)) {
                                RGN_PLACEs.add(text);
                            }
                            RGN_CODEs.put(text, tmp);
                            RGN_NAMEs.put(text, RGN_NAME);
                        } else {
                            log.error("判斷旅遊地區發生錯誤：entity=" + entity + ", text=" + text + ", info=" + info);
                        }
                    } else {
                        log.error("取得entity發生錯誤：entity=" + entity + ", text=" + text + ", info=" + info);
                    }
                }
            }
        }

        // 2. 由tourist_place table維護比對
        Map<String, Object> placeMap = getAllBotTouristPlace();

        Pattern p = null;
        Matcher m = null;
        
        if (MapUtils.isNotEmpty(placeMap)) {
            String placePattern = getPlacePattern();
            if (StringUtils.isNotBlank(placePattern)) {
                p = Pattern.compile(placePattern);
                m = p.matcher(StringUtils.isNotBlank(CBA_ORIG_STR) ? CBA_ORIG_STR.toLowerCase() : "");
                while (m.find()) {
                    String RGN_PLACE = m.group();
                    if (validEngPlaceName(CBA_ORIG_STR, RGN_PLACE)) {
                        String tmp = placeMap.get(RGN_PLACE).toString();
                        String RGN_NAME = getRGN_NAME(tmp);
                        if (StringUtils.isNotBlank(tmp) && !RGN_PLACEs.contains(RGN_PLACE)) {
                            RGN_PLACEs.add(RGN_PLACE);
                        }
                        RGN_CODEs.put(RGN_PLACE, tmp);
                        RGN_NAMEs.put(RGN_PLACE, RGN_NAME);
                    }
                }
            } else {
                log.error("取得旅遊地點Pattern發生錯誤，PlacePattern為空！");
            }
        } else {
            log.error("取得Table旅遊地點發生錯誤，TouristPlace為空！");
        }
        
        log.debug("NLU、Table比對合併地點：" + RGN_PLACEs);
        log.debug("NLU、Table比對合併國家：" + RGN_NAMEs.entrySet());
        
        // 3. 重整RGN_PLACEs中互相涵蓋重疊部分，取字串最長
        String RGN_CODE = null;
        List<String> newRGN_PLACEs = new ArrayList<String>();
        List<String> newRGN_NAMEs = new ArrayList<String>();
        
        if (CollectionUtils.isNotEmpty(RGN_PLACEs)) {
            
            List<String> tmpPlaces = new ArrayList<String>(RGN_PLACEs);
            Collections.sort(tmpPlaces, new Comparator<String>() {
                public int compare(String str1, String str2) {
                    return str1.length() < str2.length() ? 1 : (str1.length() == str2.length() ? 0 : -1);
                }
            });
            
            StringBuilder tmpPlace = new StringBuilder();
            for (String place : tmpPlaces) {
                if (tmpPlace.length() > 0) {
                    tmpPlace.append("|");
                }
                tmpPlace.append(place.trim());
            }
            
            p = Pattern.compile(tmpPlace.toString());
            m = p.matcher(CBA_ORIG_STR);
            while (m.find()) {
                String RGN_PLACE = m.group();
                String tmp = RGN_CODEs.get(RGN_PLACE);
                String RGN_NAME = RGN_NAMEs.get(RGN_PLACE);
                if (StringUtils.isBlank(RGN_CODE)) {
                    RGN_CODE = tmp;
                } else if (!StringUtils.equals(RGN_CODE, tmp)) {
                    RGN_CODE = "4";
                }
                if (StringUtils.isNotBlank(tmp) && !newRGN_PLACEs.contains(RGN_PLACE)) {
                    newRGN_PLACEs.add(RGN_PLACE);
                }
                if (!newRGN_NAMEs.contains(RGN_NAME)) {
                    newRGN_NAMEs.add(RGN_NAME);
                }
            }
        } else {
            log.error("無法辨視任何旅遊地點！");
        }
        
        result.put("RGN_CODE", RGN_CODE);
        result.put("RGN_PLACEs", newRGN_PLACEs);
        result.put("RGN_NAMEs", newRGN_NAMEs);

        return result;
    }
    
    /**
     * 檢核英文地點有效性(獨立的英文單字)<br>
     * 1.輸入參數為空，無效<br>
     * 2.PLACE不是純英文，有效(不需檢核)<br>
     * 3.獨立的英文單字，有效<br>
     * 4.其他，無效
     * 
     * @param STR
     * @param PLACE
     * @return true:有效, false:無效
     */
    private boolean validEngPlaceName(String STR, String PLACE) {

        if (StringUtils.isBlank(STR) || StringUtils.isBlank(PLACE))
            return false;
        
        if (!PLACE.matches("^[A-Za-z]+$"))
            return true;

        boolean result = false;

        try {
            Pattern p = Pattern.compile("(^|[^a-z])" + PLACE + "($|[^a-z])");
            Matcher m = p.matcher(StringUtils.isNotBlank(STR) ? STR.toLowerCase() : "");
            while (m.find()) {
                String RGN_PLACE = m.group();
                if (StringUtils.isNotBlank(RGN_PLACE)) {
                    result = true;
                }
            }
        } catch(Exception e) {
            
        }
        
        return result;
    }
    
    private String getCBA_CODE(String infos) {
        
        if (StringUtils.isBlank(infos)) {
            return "4";
        }
        
        String info = infos.split(";")[0];
        
        if (StringUtils.contains(info, "歐洲非申根-土耳其") || StringUtils.contains(info, "歐洲非申根-以色列") || StringUtils.contains(info, "歐洲非申根-白俄羅斯")
                || StringUtils.contains(info, "歐洲非申根-約旦") || StringUtils.contains(info, "歐洲非申根-烏克蘭") || StringUtils.contains(info, "歐洲非申根-喬治亞")
                || StringUtils.contains(info, "歐洲非申根-摩爾多瓦") || StringUtils.contains(info, "歐洲非申根-黎巴嫩")) {
            return "4";
        }

        if(StringUtils.contains(info, "國內")) {
        	return "1";
        }else if(StringUtils.contains(info, "中國大陸") || StringUtils.contains(info, "香港") || StringUtils.contains(info, "澳門")) {
        	return "2";
        }else if(StringUtils.contains(info, "日本")) {
        	return "5";
        } else if (StringUtils.contains(info, "美國") || StringUtils.contains(info, "加拿大")) {
        	return "6";
        }else if(StringUtils.contains(info, "歐洲")) {
        	return "7";// 預設7(歐洲非申根)
        } else if (StringUtils.contains(info, "紐西蘭") || StringUtils.contains(info, "澳洲")) {
        	return "9";
        }else if(StringUtils.contains(info, "韓國")) {
        	return "14";
        }else if(StringUtils.contains(info, "越南")) {
        	return "15";
        }else if(StringUtils.contains(info, "新加坡")) {
        	return "16";
        }else if(StringUtils.contains(info, "菲律賓")) {
        	return "17";
        }else if(StringUtils.contains(info, "印尼")) {
        	return "18";
        }else if(StringUtils.contains(info, "馬來西亞")) {
        	return "19";
        }else if(StringUtils.contains(info, "緬甸")) {
        	return "20";
        }else if(StringUtils.contains(info, "泰國")) {
        	return "21";
        }else if(StringUtils.contains(info, "寮國")) {
        	return "22";
        }else if(StringUtils.contains(info, "柬埔寨")) {
        	return "23";
        }else {
        	return "4";
        }
    }

    @Override
    public String getRGN_NAME(String RGN_CODE) {
        if ("1".equals(RGN_CODE)) {
            return "國內";
        } else if ("2".equals(RGN_CODE)) {
            return "中國.港澳";
        } else if ("5".equals(RGN_CODE)) {
            return "日本";
        } else if ("6".equals(RGN_CODE)) {
            return "美加";
        } else if ("7".equals(RGN_CODE)) {
            return "歐洲";
        } else if ("11".equals(RGN_CODE)) {
        	return "歐洲-申根";
        } else if ("9".equals(RGN_CODE)) {
            return "紐澳";
        } else if ("14".equals(RGN_CODE)) {
            return "韓國";
        } else if ("15".equals(RGN_CODE)) {
            return "越南";
        } else if ("16".equals(RGN_CODE)) {
        	return "新加坡";
        } else if ("17".equals(RGN_CODE)) {
        	return "菲律賓";
        } else if ("18".equals(RGN_CODE)) {
        	return "印尼";
        } else if ("19".equals(RGN_CODE)) {
        	return "馬來西亞";
        } else if ("20".equals(RGN_CODE)) {
        	return "緬甸";
        } else if ("21".equals(RGN_CODE)) {
        	return "泰國";
        } else if ("22".equals(RGN_CODE)) {
        	return "寮國";
        } else if ("23".equals(RGN_CODE)) {
        	return "柬埔寨";
        } else { // 4: 「其他」顯示暫時調整為「國外」
            // return "其他";
            return "國外";
        }
    }

    @Override
    public Map<String, Object> getISSUE_DAY(String sessionId, List<Object> CBA_DATEs) {

        Map<String, Object> ISSUE_DAY = new HashMap<String, Object>();

        try {
            if (CollectionUtils.isNotEmpty(CBA_DATEs)) {
                Collections.sort(CBA_DATEs, new Comparator<Object>() {
                    public int compare(Object str1, Object str2) {
                        String[] tmpDate1 = str1.toString().split(",");
                        Date theDay1 = DateUtil.getDateByPattern(tmpDate1[0], DateUtil.PATTERN_DASH_DATE);
                        String[] tmpDate2 = str2.toString().split(",");
                        Date theDay2 = DateUtil.getDateByPattern(tmpDate2[0], DateUtil.PATTERN_DASH_DATE);
                        return theDay1.compareTo(theDay2);
                    }
                });
            }
        } catch (Exception e) {
            log.error("[getISSUE_DAY] sort CBA_DATEs error:" + e.getMessage());
        }
        
        Date minDay = null, maxDay = null;
        if (CollectionUtils.isNotEmpty(CBA_DATEs)) {
            for (Object obj : CBA_DATEs) {
                try {
                    if (obj != null) {
                        String[] tmpDate = obj.toString().split(",");
                        String theDay = tmpDate[0];
                        boolean canNextYear = StringUtils.equals("0", tmpDate[1]);
                        if (StringUtils.isNotBlank(theDay)) {
                            
                            Date tmpDay = DateUtil.getDateOfStart(DateUtil.getDateByPattern(theDay, DateUtil.PATTERN_DASH_DATE));
                            
                            // 若原始用戶問句中取得日期沒有明確年份，則檢查是否可指定為跨到明年日期處理
                            if (canNextYear && tmpDay.before(DateUtil.getToday())) {
                                Date tmpMinDay = minDay, tmpMaxDay = maxDay;
                                Date tmpNextYearDay = DateUtil.addYear(tmpDay, 1);
                                if (tmpMinDay == null) {
                                    tmpMinDay = tmpNextYearDay;
                                } else if (tmpNextYearDay.compareTo(tmpMinDay) >= 0) { // tmpDay >= minDay
                                    if (tmpMaxDay == null || tmpNextYearDay.after(tmpMaxDay)) {
                                        tmpMaxDay = tmpNextYearDay;
                                    }
                                } else if (tmpNextYearDay.compareTo(tmpMinDay) < 0) { // tmpDay < minDay
                                    if (tmpMaxDay == null) {
                                        tmpMaxDay = tmpMinDay;
                                    }
                                    tmpMinDay = tmpNextYearDay;
                                }
                                
                                String tmpDays = null;
                                if (tmpMaxDay != null) {
                                    tmpDays = DateUtil.getDiffDays(DateUtil.addDay(tmpMaxDay, 1), tmpMinDay).toString();
                                } else {
                                    tmpDays = "1";
                                }
                                
                                if (checkISSUE_Period("4", DateUtil.formatDate(tmpMinDay, DateUtil.PATTERN_DASH_DATE), tmpDays)) {
                                    tmpDay = tmpNextYearDay;
                                }
                            }
                            
                            if (minDay == null) {
                                minDay = tmpDay;
                            } else if (tmpDay.compareTo(minDay) >= 0) { // tmpDay >= minDay
                                if (maxDay == null || tmpDay.after(maxDay)) {
                                    maxDay = tmpDay;
                                }
                            } else if (tmpDay.compareTo(minDay) < 0) { // tmpDay < minDay
                                if (maxDay == null) {
                                    maxDay = minDay;
                                }
                                minDay = tmpDay;
                            }
                        }
                    }
                } catch (Exception e) {
                    log.error("日期取得錯誤：" + obj);
                }
            }
        }

        if (minDay != null) {
            Map<String, String> bgn = getBGN_TIME(minDay);
            String ISSUE_DATE = MapUtils.getString(bgn, "ISSUE_DATE");
            String BGN_TIME = MapUtils.getString(bgn, "BGN_TIME");
            String IS_FIX_TIME = MapUtils.getString(bgn, "IS_FIX_TIME", "N");
            ISSUE_DAY.put("ISSUE_DATE", ISSUE_DATE);
            ISSUE_DAY.put("BGN_TIME", BGN_TIME);
            ISSUE_DAY.put("MIN_DATE", minDay);
            ISSUE_DAY.put("IS_FIX_TIME", IS_FIX_TIME);
            if (maxDay != null) {
                ISSUE_DAY.put("ISSUE_DAYS", DateUtil.getDiffDays(DateUtil.addDay(maxDay, 1), minDay).toString());
                ISSUE_DAY.put("MAX_DATE", maxDay);
            }
        }

        log.debug("(" + sessionId + ") ISSUE_DAY=" + ISSUE_DAY);

        return ISSUE_DAY;
    }

    
    /**
     * 判斷是否為有專機之地區OMTP_AMT
     * 專機地區
     * "2":"中國大陸（含香港、澳門地區）", "5":"日本", "14":"韓國", "15":"越南", "16":"新加坡",
     * "17":"菲律賓", "18":"印尼", "19":"馬來西亞", "20":"緬甸", "21":"泰國", "22":"寮國", "23":"柬埔寨"
     * @param STR
     * @param PLACE
     * @return true:有效, false:無效
     */
    @SuppressWarnings("rawtypes")
    @Override
    public boolean getOMTP(String RGN_CODE) throws Exception {
    	boolean IS_OMTP = false;
    	switch(RGN_CODE) {
	    	case "2":
	    	case "5":
	    	case "14": 
	    	case "15":
	    	case "16":
	    	case "17":
	    	case "18":
	    	case "19":
	    	case "20":
	    	case "21":
	    	case "22":
	    	case "23":
	    		IS_OMTP = true;
    	}
    	return IS_OMTP;
    }
    
    @Override
    public Map<String, String> getBGN_TIME(Date THE_DATE) {

        Map<String, String> theDate = new HashMap<String, String>();

        // if (this.checkToday(THE_DATE)) {

        if (THE_DATE.compareTo(DateUtil.getToday()) >= 0) {
            
            Date now = DateUtil.getNow();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(now);

            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            
            Date nowDate30 = DateUtil.addMinute(now, 30);
            
            if (THE_DATE.after(now)) {
                calendar = Calendar.getInstance();
                calendar.setTime(THE_DATE);
                
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH) + 1;
                day = calendar.get(Calendar.DAY_OF_MONTH);
                hour = calendar.get(Calendar.HOUR_OF_DAY);
            }
            
            log.debug("year=" + year + ", month=" + month + ", day=" + day + ", hour=" + hour);

            Date date20 = DateUtil.getDate(year, month, day, hour, 20, 0);
            Date date50 = DateUtil.getDate(year, month, day, hour, 50, 0);

            Date BGN_DATE = null;
            if (now.after(date50)) {
                BGN_DATE = DateUtil.addMinute(date50, 40);
            } else if (now.after(date20)) {
                BGN_DATE = DateUtil.addMinute(date20, 40);
            } else {
                if (nowDate30.before(THE_DATE)) {
                    BGN_DATE = THE_DATE;
                } else {
                    BGN_DATE = DateUtil.addMinute(date20, 10);
                }
            }

            theDate.put("ISSUE_DATE", DateUtil.formatDate(BGN_DATE, DateUtil.PATTERN_DASH_DATE));
            theDate.put("BGN_TIME", DateUtil.formatDate(BGN_DATE, DateUtil.PATTERN_TIME_2));
            
            if (BGN_DATE.equals(THE_DATE)) {
                theDate.put("IS_FIX_TIME", "N");
            } else {
                theDate.put("IS_FIX_TIME", "Y");
            }
        } else {
            theDate.put("ISSUE_DATE", DateUtil.formatDate(THE_DATE, DateUtil.PATTERN_DASH_DATE));
            theDate.put("BGN_TIME", "00:00");
            theDate.put("IS_FIX_TIME", "N");
        }

        return theDate;
    }
    
    @Override
    public Map<String, String> getDateTime(Date MIN_DATE, String ISSUE_DAYS) {
        return getDateTime(MIN_DATE, ISSUE_DAYS, null);
    }
    
    @Override
    public Map<String, String> getDateTime(Date MIN_DATE, String ISSUE_DAYS, String MIN_TIME) {
        
        Map<String, String> theDate = new HashMap<String, String>();
        
        String reg_Time = "^(20|21|22|23|[0-1]\\d):[0-5]\\d$";
        
        // 若傳入MIN_TIME，則以MIN_TIME中小時為基準判斷處理
        if (StringUtils.isNotBlank(MIN_TIME) && MIN_TIME.matches(reg_Time)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(MIN_DATE);
            MIN_DATE = DateUtil.addHour(MIN_DATE, -1 * calendar.get(Calendar.HOUR_OF_DAY));
            int hour = NumberUtils.toInt(MIN_TIME.split(":")[0]);
            MIN_DATE = DateUtil.addHour(MIN_DATE, hour);
        }
        
        Map<String, String> minDateTime = getBGN_TIME(MIN_DATE);
        String minDate = MapUtils.getString(minDateTime, "ISSUE_DATE");
        String minTime = MapUtils.getString(minDateTime, "BGN_TIME");
        String IS_FIX_TIME = MapUtils.getString(minDateTime, "IS_FIX_TIME");
        
        int days = NumberUtils.toInt(ISSUE_DAYS);
        Date tmpMAX_DATE = DateUtil.addDay(DateUtil.getDateByPattern(minDate + " " + minTime + ":00", DateUtil.PATTERN_DASH_DATE_TIME), days);
        String tmpMaxTim = DateUtil.formatDate(tmpMAX_DATE, DateUtil.PATTERN_TIME_2);
        
        String maxDate = null;
        String maxTime = null;
        if ("00:00".equals(tmpMaxTim)) {
            Date MAX_DATE = DateUtil.addDay(tmpMAX_DATE, -1);
            maxDate = DateUtil.formatDate(MAX_DATE, DateUtil.PATTERN_DASH_DATE);
            maxTime = "24:00";
        } else {
            maxDate = DateUtil.formatDate(tmpMAX_DATE, DateUtil.PATTERN_DASH_DATE);
            maxTime = DateUtil.formatDate(tmpMAX_DATE, DateUtil.PATTERN_TIME_2);
        }
        
        theDate.put("ISSUE_DATE", minDate);
        theDate.put("BGN_TIME", minTime);
        theDate.put("MIN_DATE_TIME", minDate + " " + minTime);
        theDate.put("MAX_DATE_TIME", maxDate + " " + maxTime);
        theDate.put("IS_FIX_TIME", IS_FIX_TIME);
        
        return theDate;
    }

    @Override
    public boolean checkISSUE_Period(String RGN_CODE, String ISSUE_DATE, String ISSUE_DAYS) {

        /**
         * 不接受的投保期間： <br>
         * 1. 地點國內且投保期間>30天<br>
         * 2. 地點國外且投保期間>180天<br>
         * 3. 開始時間為今天以前<br>
         * 4. 開始時間超過(今天+180天)
         */

        boolean result = true;
        
        int days = NumberUtils.toInt(ISSUE_DAYS, 0);
        Date startDate = DateUtil.getDateByPattern(ISSUE_DATE, DateUtil.PATTERN_DASH_DATE);
        
        Date today = DateUtil.getToday();
        Date finalDay = DateUtil.addDay(today, 180);
        
        if ("1".equals(RGN_CODE) && days > 30) {
            result = false;
        } else if (!"1".equals(RGN_CODE) && days > 180) {
            result = false;
        } else if (today.after(startDate)) {
            result = false;
        } else if (finalDay.before(startDate)) {
            result = false;
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, Object> genC02_CONTRACT_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS, String RGN_CODE, String ISSUE_DATE, String ISSUE_DAYS, String BGN_TIME) {

        String APC_ZIP_CODE = null;
        String APC_NAME = null;
        String APC_BRDY = null;
        String APC_ID = null;
        String APC_ADDR = null;
        String MOBIL_NO = null;
        String APC_ID_TYPE = null;
        String APC_EMAIL = null;
        
        if (MapUtils.isNotEmpty(g06Data)) {
            APC_NAME = MapUtils.getString(g06Data, G06.APC_NAME.name());
            APC_ID = MapUtils.getString(g06Data, G06.APC_ID.name());
            APC_BRDY = MapUtils.getString(g06Data, G06.BIRTHDAY.name());
            APC_ZIP_CODE = MapUtils.getString(g06Data, G06.ZIP_CODE.name());
            APC_ADDR = MapUtils.getString(g06Data, G06.ADDR.name());
            MOBIL_NO = MapUtils.getString(a30Data, A30.mobl_no.name());
            APC_EMAIL = MapUtils.getString(a30Data, A30.email.name());
            // TODO: 要保人證件來源(1:身分證,2:居留證) --> 要保人證件類型 1 身份證 2 統一證號  3 護照 
            APC_ID_TYPE = MapUtils.getString(g06Data, G06.APC_ID_TYPE.name());
        } else {
            APC_ID = MapUtils.getString(a30Data, A30.apc_id.name());
            APC_NAME = MapUtils.getString(a30Data, A30.apc_name.name());
            APC_BRDY = MapUtils.getString(a30Data, A30.apc_brdy.name());
            APC_ZIP_CODE = ZIPCODE;
            APC_ADDR = ADDRESS;
            MOBIL_NO = MapUtils.getString(a30Data, A30.mobl_no.name());
            APC_EMAIL = MapUtils.getString(a30Data, A30.email.name());
            // TODO: 要保人證件類型先預設帶身分證
            APC_ID_TYPE = "1";
        }

        /**
         * "CONTRACT_DATA": //要保人及契約資料{
         * "APC_ZIP_CODE":"114", //要保人地址
         * "B2C_APLY_NO":"", //不須填
         * "ISSUE_DATE":"2019-05-28", //投保始期
         * "POLICY_TYPE":"2", //保單型式 電子保單
         * "BGN_TIME":"02:30", //投保開始時間
         * "APC_NAME":"陳○文", //要保人姓名
         * "APC_BRDY":"1986-09-27", //要保人生日
         * "APC_ID":"K18637836J", //要保人ID
         * "APC_ADDR":"民權東路六段１５０號", //要保人地址
         * "RGN_CODE":"3", //旅遊地區代碼,可call 旅遊地區API(getREG_CODE)取得全部
         * "APC_MOBILE":"0937123456", //要保人電話
         * "ISSUE_DAYS":15, //投保天數
         * "APC_ID_TYPE":"1", //要保人證件類型 1 身份證 2 統一證號  3 護照 
         * "APC_EMAIL":"XXXXX@cathlife.com.tw" //要保人email}
         */

        Map<String, Object> CONTRACT_DATA = new HashMap<String, Object>();
        CONTRACT_DATA.put("APC_ZIP_CODE", APC_ZIP_CODE);
        CONTRACT_DATA.put("B2C_APLY_NO", "");
        CONTRACT_DATA.put("ISSUE_DATE", ISSUE_DATE);
        CONTRACT_DATA.put("POLICY_TYPE", "2");
        CONTRACT_DATA.put("BGN_TIME", BGN_TIME);
        CONTRACT_DATA.put("APC_NAME", APC_NAME);
        CONTRACT_DATA.put("APC_BRDY", APC_BRDY);
        CONTRACT_DATA.put("APC_ID", APC_ID);
        CONTRACT_DATA.put("APC_ADDR", APC_ADDR);
        CONTRACT_DATA.put("RGN_CODE", RGN_CODE);
        CONTRACT_DATA.put("APC_MOBILE", MOBIL_NO);
        CONTRACT_DATA.put("ISSUE_DAYS", ISSUE_DAYS);
        CONTRACT_DATA.put("APC_ID_TYPE", APC_ID_TYPE);
        CONTRACT_DATA.put("APC_EMAIL", APC_EMAIL);
        return CONTRACT_DATA;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<Map<String, String>> genC02_INSD_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS, String RGN_CODE, String HD_AMT, String HP_AMT, String HK_AMT, boolean IS_OMTP) {

        String INSD_NAME = null;
        String ZIP_CODE = null;
        String BENE_SHR = null;
        String MOBILE = null;
        String INSD_RLTN = null;
        String INSD_ID = null;
        String INSD_BRDY = null;
        String EMAIL = null;
        String INSD_ID_TYPE = null;
        String ADDR = null;
        String YEAR_INCOME = null;
        String YEAR_INCOME_NAME = null;
        
        boolean defaultIncome = true;
        if (MapUtils.isNotEmpty(g06Data)) {
            INSD_NAME = MapUtils.getString(g06Data, G06.APC_NAME.name());
            ZIP_CODE = MapUtils.getString(g06Data, G06.ZIP_CODE.name());
            INSD_ID = MapUtils.getString(g06Data, G06.APC_ID.name());
            INSD_BRDY = MapUtils.getString(g06Data, G06.BIRTHDAY.name());
            BENE_SHR = "2"; // 2:比例
            INSD_RLTN = "1"; // 1:本人
            ADDR = MapUtils.getString(g06Data, G06.ADDR.name());
            MOBILE = MapUtils.getString(a30Data, A30.mobl_no.name());
            EMAIL = MapUtils.getString(a30Data, A30.email.name());
            // G06要保人證件來源(1:身分證,2:居留證)
            // C02被保人證件類型 1 身份證 2 統一證號  3 護照
            INSD_ID_TYPE = MapUtils.getString(g06Data, G06.APC_ID_TYPE.name());
            
            Map INSD_DATA = new HashMap();
            try {
                List INSD_DATA_list = (List) MapUtils.getObject(g06Data, G06.INSD_DATA.name());
                if (CollectionUtils.isNotEmpty(INSD_DATA_list)) {
                    INSD_DATA = (Map) INSD_DATA_list.get(0);
                }
            } catch (Exception e) {
                log.error("genC02_BENE_DATA發生錯誤，error:" + e.getMessage(), e);
            }
            String tmpYearIncome = MapUtils.getString(INSD_DATA, G06.YEAR_INCOME.name());
            if (StringUtils.isNotBlank(tmpYearIncome)) {
                defaultIncome = false;
            }
            
            // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上)
            YEAR_INCOME = StringUtils.isNotBlank(tmpYearIncome) ? tmpYearIncome : "1";
        } else {
            INSD_NAME = MapUtils.getString(a30Data, A30.apc_name.name());
            ZIP_CODE = ZIPCODE;
            INSD_ID = MapUtils.getString(a30Data, A30.apc_id.name());
            INSD_BRDY = MapUtils.getString(a30Data, A30.apc_brdy.name());
            BENE_SHR = "2"; // 2:比例
            INSD_RLTN = "1"; // 1:本人
            ADDR = ADDRESS;
            MOBILE = MapUtils.getString(a30Data, A30.mobl_no.name());
            EMAIL = MapUtils.getString(a30Data, A30.email.name());
            // TODO: INSD_ID_TYPE先預設帶身分證
            INSD_ID_TYPE = "1";
            // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上)
            YEAR_INCOME = "1";
        }
        
        if ("2".equals(YEAR_INCOME)) {
            YEAR_INCOME_NAME = "50~100萬";
        } else if ("3".equals(YEAR_INCOME)) {
            YEAR_INCOME_NAME = "100~200萬";
        } else if ("4".equals(YEAR_INCOME)) {
            YEAR_INCOME_NAME = "200萬以上";
        } else {
            YEAR_INCOME_NAME = "50萬以下" + (defaultIncome ? "(預設)" : "");
        }

        /**
         * "INSD_DATA":[  //被保人清單
         * {"INSD_NAME":"陳○文", //被保人姓名
         * "ZIP_CODE":"114", //被保人郵遞區號
         * "HK_AMT":"90", //海外突發疾病保額
         * "HP_AMT":"90", //實支實付保額
         * "HD_AMT":"900", //死殘保額
         * "BENE_SHR":"2", //受益人受益分配 1 均分 2 比例 3 順位 
         * "MOBILE":"0937123456",//被保人電話
         * "INSD_RLTN":"1", //與要保人關係 1 本人
         * "INSD_ID":"K18637836J", //被保人ID
         * "INSD_BRDY":"1986-09-27", //被保人生日
         * "EMAIL":"XXXXX@cathlife.com.tw", //被保人email
         * "INSD_ID_TYPE":"1", //被保人證件類型 1 身份證 2 統一證號  3 護照 
         * "ADDR":"民權東路六段１５０號"} ],
         */

        List<Map<String, String>> INSD_DATAs = new ArrayList<Map<String, String>>();

        Map<String, String> INSD_DATA = new HashMap<String, String>();
        INSD_DATA.put("INSD_NAME", INSD_NAME);
        INSD_DATA.put("ZIP_CODE", ZIP_CODE);
        if (!"1".equals(RGN_CODE)) {
            INSD_DATA.put("HK_AMT", HK_AMT);
        }
        INSD_DATA.put("HP_AMT", HP_AMT);
        INSD_DATA.put("HD_AMT", HD_AMT);
        INSD_DATA.put("BENE_SHR", BENE_SHR);
        INSD_DATA.put("MOBILE", MOBILE);
        INSD_DATA.put("INSD_RLTN", INSD_RLTN);
        INSD_DATA.put("INSD_ID", INSD_ID);
        INSD_DATA.put("INSD_BRDY", INSD_BRDY);
        INSD_DATA.put("EMAIL", EMAIL);
        INSD_DATA.put("INSD_ID_TYPE", INSD_ID_TYPE);
        INSD_DATA.put("ADDR", ADDR);
        INSD_DATA.put("YEAR_INCOME", YEAR_INCOME);
        INSD_DATA.put("YEAR_INCOME_NAME", YEAR_INCOME_NAME);
    	INSD_DATA.put("OMTP_AMT", IS_OMTP ? "1" : "0");

        INSD_DATAs.add(INSD_DATA);

        return INSD_DATAs;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<Map<String, Object>> genC02_BENE_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS) {

        String RELATION = null;
        String ZIP_CODE = null;
        String BENE_ID = null;
        String INSD_ID = null;
        String TEL = null;
        String BENE_ID_TYPE = null;
        String ADDR = null;
        String BENE_NAME = null;
        String BENE_TYPE = null;
        String BENE_SER = null;
        String ID_KIND = null;
        String NATION_CODE = null;
        String BENE_BIRTH = null;
        String ADDR_AS_APC = null;
        
        if (MapUtils.isNotEmpty(g06Data)) {
            
            Map INSD_DATA = new HashMap();
            Map BENE_DATA = new HashMap();
            try {
                List INSD_DATA_list = (List) MapUtils.getObject(g06Data, G06.INSD_DATA.name());
                if (CollectionUtils.isNotEmpty(INSD_DATA_list)) {
                    INSD_DATA = (Map) INSD_DATA_list.get(0);
                }
                List BENE_DATA_list = (List) MapUtils.getObject(INSD_DATA, G06.BENE_DATA.name());
                if (CollectionUtils.isNotEmpty(BENE_DATA_list)) {
                    BENE_DATA = (Map) BENE_DATA_list.get(0);
                }
            } catch (Exception e) {
                log.error("genC02_BENE_DATA發生錯誤，error:" + e.getMessage(), e);
            }
            
            // 投保商品別    CB開頭為旅平險商品, AT開頭為壽險商品
            String SYS_NO = MapUtils.getString(g06Data, G06.SYS_NO.name());
            
            // G06受益人與被保人關係
            // G06[旅平險商品,回傳 2:法定繼承人, 5:配偶, 6:子女, 7:父母, 8:祖孫]
            // G06[壽險商品,回傳 2:子女, 3:配偶, 5:祖孫, 9:法定繼承人]
            // C02與被保人關係 5 配偶 6 子女 7 父母 8 袓孫 2 法定繼承人 1 親屬 3 朋友 4 其他
            String[] rltnMapping2 = { "", "", "6", "5", "", "8", "", "", "", "2" };
            String g06RLTN = MapUtils.getString(BENE_DATA, G06.RLTN.name());
            int tmpRLTN = NumberUtils.toInt(g06RLTN, 0);
            if (tmpRLTN >= 2 && tmpRLTN <= 9) {
                if (StringUtils.startsWith(SYS_NO, "CB")) {
                    RELATION = "" + tmpRLTN;
                } else if (StringUtils.startsWith(SYS_NO, "AT")) {
                    RELATION = rltnMapping2[tmpRLTN];
                }
            }
            
            if (StringUtils.isBlank(RELATION)) {
                log.error("genC02_BENE_DATA發生錯誤，無法取得受益人與被保人關係，G06 RLTN=" + g06RLTN);
                RELATION = "2";
            }
            
            // 受益人證件字號 [旅平險商品&法定繼承人,回傳空字串]
            BENE_ID = MapUtils.getString(BENE_DATA, G06.BENE_ID.name());
            if (StringUtils.isBlank(BENE_ID)) {
                BENE_ID = "0000000000";
            }
            
            INSD_ID = MapUtils.getString(g06Data, G06.APC_ID.name());
            
            // 受益人電話及地址同要保人 (Y:是, N:否)
            ADDR_AS_APC = MapUtils.getString(BENE_DATA, G06.ADDR_AS_APC.name());
            if ("Y".equals(ADDR_AS_APC)) {
                TEL = MapUtils.getString(a30Data, A30.mobl_no.name());
                ZIP_CODE = MapUtils.getString(g06Data, G06.ZIP_CODE.name());
                ADDR = MapUtils.getString(g06Data, G06.ADDR.name());
            } else {
                TEL = MapUtils.getString(BENE_DATA, G06.BENE_PHONE.name());
                ZIP_CODE = MapUtils.getString(BENE_DATA, G06.BENE_ZIP.name());
                ADDR = MapUtils.getString(BENE_DATA, G06.BENE_ADDR.name());
            }
            
            // TODO: 若取得4:統一編號，可能得整筆設為法定繼承人
            // G06證件類別
            // G06[旅平險商品,回傳 空字串:法定繼承人, 1:身分證, 2:統一證號, 3:護照, 4:統一編號]
            // G06[壽險商品,回傳 1:身分證, 2:護照, 3:統一證號, 4:統一編號]
            // C02證件類型 1 身份證 2 統一證號  3 護照(如為法繼，給空字串)
            String[] idTypeMapping2 = { "", "1", "3", "2", "" };
            int tmpIdType = NumberUtils.toInt(MapUtils.getString(BENE_DATA, G06.BENE_ID_TYPE.name()), 0);
            if (StringUtils.startsWith(SYS_NO, "CB")) {
                BENE_ID_TYPE = "" + tmpIdType;
            } else {
                BENE_ID_TYPE = idTypeMapping2[tmpIdType];
            }
            
            BENE_NAME = MapUtils.getString(BENE_DATA, G06.BENE_NAME.name());
            BENE_TYPE = MapUtils.getString(BENE_DATA, G06.BENE_TYPE.name());
            BENE_SER = MapUtils.getString(BENE_DATA, G06.BENE_SER.name());
            ID_KIND = MapUtils.getString(BENE_DATA, G06.ID_KIND.name());
            NATION_CODE = MapUtils.getString(BENE_DATA, G06.NATION_CODE.name());
            BENE_BIRTH = MapUtils.getString(BENE_DATA, G06.BENE_BIRTH.name());
        } else {
            RELATION = "2";
            ZIP_CODE = ZIPCODE;
            BENE_ID = "0000000000";
            INSD_ID = MapUtils.getString(a30Data, A30.apc_id.name());
            TEL = MapUtils.getString(a30Data, A30.mobl_no.name());
            BENE_ID_TYPE = "";
            ADDR = ADDRESS;
            BENE_NAME = "法定繼承人";
            BENE_TYPE = "1";
            BENE_SER = "1";
            ID_KIND = "1";
            ADDR_AS_APC = "Y";
        }

        /**
         * "BENE_DATA":[ //受益人資料
          * {"RELATION":"2", //與被保人關係 5 配偶 6 子女 7 父母 8 袓孫 2 法定繼承人 1 親屬 3 朋友 4 其他
          * "ZIP_CODE":"114", //郵遞區號
          * "BENE_ID":"", //受益人ID
          * "BENE_SER_NO":1, //第N個受益人
          * "INSD_ID":"K18637836J", //對應之被保人
          * "TEL":"0937123456", //電話
          * "BENE_ID_TYPE":"", //證件類型 證件類型 1 身份證 2 統一證號  3 護照(如為法繼，給空字串) 
          * "BENE_RATO_BASE":"100", //百分比分母
          * "BENE_RATO_UP":100, //百分比分子
          * "BENE_PRIO":"", //選擇順位要填1,2,3
          * "ADDR":"台北市內湖區民權東路六段１５０號",
          * "BENE_NAME":"法定繼承人"
          * }],
          */

        List<Map<String, Object>> BENE_DATAs = new ArrayList<Map<String, Object>>();

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("RELATION", RELATION);
        data.put("ZIP_CODE", ZIP_CODE);
        data.put("BENE_ID", BENE_ID);
        data.put("BENE_SER_NO", "1");
        data.put("INSD_ID", INSD_ID);
        data.put("TEL", TEL);
        data.put("BENE_ID_TYPE", BENE_ID_TYPE);
        data.put("BENE_RATO_BASE", "100");
        data.put("BENE_RATO_UP", "100");
        data.put("BENE_PRIO", "0");
        data.put("ADDR", ADDR);
        data.put("BENE_NAME", BENE_NAME);
        // for G07
        data.put("BENE_TYPE", BENE_TYPE);
        data.put("BENE_SER", BENE_SER);
        data.put("ID_KIND", ID_KIND);
        data.put("NATION_CODE", NATION_CODE);
        data.put("BENE_BIRTH", BENE_BIRTH);
        data.put("ADDR_AS_APC", ADDR_AS_APC);

        BENE_DATAs.add(data);

        return BENE_DATAs;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, Object> genC02_TRAL_TMP(Map a30Data, Map g06Data) {

        String INSR_ID = null;
        String APC_YEAR_INCOME = null;
        
        String YEAR_INCOME = null;
        if (MapUtils.isNotEmpty(g06Data)) {
            INSR_ID = MapUtils.getString(g06Data, G06.APC_ID.name());
            
            Map INSD_DATA = new HashMap();
            try {
                List INSD_DATA_list = (List) MapUtils.getObject(g06Data, G06.INSD_DATA.name());
                if (CollectionUtils.isNotEmpty(INSD_DATA_list)) {
                    INSD_DATA = (Map) INSD_DATA_list.get(0);
                }
            } catch (Exception e) {
                log.error("genC02_BENE_DATA發生錯誤，error:" + e.getMessage(), e);
            }
            // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上)
            YEAR_INCOME = MapUtils.getString(INSD_DATA, G06.YEAR_INCOME.name(), "1");
        } else {
            INSR_ID = MapUtils.getString(a30Data, A30.apc_id.name());
            // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上)
            YEAR_INCOME = "1";
        }
        
        if ("2".equals(YEAR_INCOME)) {
            APC_YEAR_INCOME = "100"; //50~100萬
        } else if ("3".equals(YEAR_INCOME)) {
            APC_YEAR_INCOME = "200"; //100~200萬
        } else if ("4".equals(YEAR_INCOME)) {
            APC_YEAR_INCOME = "201"; //200萬以上
        } else {
            APC_YEAR_INCOME = "50"; //50萬以下
        }

        /**
         * "TRAL_TMP": //財告
         * {"INSR_ID":"K18637836J", //被保人ID
         * "APC_FAM_INCOME":"50", //被保人收入
         * "EXAMINE_A_SRC":"1" //是否為主要經濟來源 1 是,2否},
         */

        Map<String, Object> TRAL_TMP = new HashMap<String, Object>();
        TRAL_TMP.put("INSR_ID", INSR_ID);
        // TRAL_TMP.put("APC_FAM_INCOME", "50"); // 預設為50萬元以下
        TRAL_TMP.put("APC_YEAR_INCOME", APC_YEAR_INCOME); // 預設為50萬元以下
        TRAL_TMP.put("EXAMINE_A_SRC", "1"); // 預設為是
        return TRAL_TMP;
    }

    @Override
    public boolean checkToday(Date date) {
        String param = DateUtil.formatDate(date, DateUtil.PATTERN_DASH_DATE); // 參數時間
        String now = DateUtil.formatDate(new Date(), DateUtil.PATTERN_DASH_DATE); // 當前時間
        if (param.equals(now)) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getC04DataById(String ID) throws Exception {

        String cacheKey = "C04_" + ID;

        Map<String, Object> tmp = (Map<String, Object>) LocalCache.get(cacheKey);
        if (tmp != null && !tmp.isEmpty()) {
            log.debug("[" + ID + "] C04DataById=" + tmp.entrySet());
            return tmp;
        }

        tmp = (Map<String, Object>) cathayLifeTypeCService.c04WithNoException(ID);
        String c04ResultCode = MapUtils.getString(tmp, C04.resultCode.name());
        if ("000".equals(c04ResultCode)) {
            LocalCache.register(cacheKey, tmp, ExpiredTime._10mins);
            callApiService.localCache(cacheKey, tmp, ExpiredTime._10mins);
            log.debug("[" + ID + "] C04DataById=" + tmp.entrySet());
        } else {
            log.error("[" + ID + "] getC04DataById resultCode:" + c04ResultCode);
        }
        
        return tmp;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getA04WithDefaultTWDataById(String ID) throws Exception {

        String cacheKey = "A04WithDefaultTW_" + ID;

        Map<String, Object> tmp = (Map<String, Object>) LocalCache.get(cacheKey);
        if (tmp != null && !tmp.isEmpty()) {
            log.debug("[" + ID + "] A04WithDefaultTWDataById=" + tmp.entrySet());
            return tmp;
        }

        try {
            tmp = (Map<String, Object>) cathayLifeTypeAService.a04(ID, "TW");
            LocalCache.register(cacheKey, tmp, ExpiredTime._10mins);
            callApiService.localCache(cacheKey, tmp, ExpiredTime._10mins);
            log.debug("[" + ID + "] A04WithDefaultTWDataById=" + tmp.entrySet());
        } catch(Exception e) {
            log.debug("[" + ID + "] A04WithDefaultTWDataById error:" + e.getMessage());
        }

        return tmp;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getG06DataById(String ID) throws Exception {

        String cacheKey = "G06_" + ID;

        Map<String, Object> tmp = (Map<String, Object>) LocalCache.get(cacheKey);
        if (tmp != null && !tmp.isEmpty()) {
            log.debug("[" + ID + "] G06DataById=" + tmp.entrySet());
            return tmp;
        }

        tmp = (Map<String, Object>) cathayLifeTypeGService.g06WithNoException(ID);
        String g06ResultCode = MapUtils.getString(tmp, G06.resultCode.name());
        if ("000".equals(g06ResultCode)) {
            LocalCache.register(cacheKey, tmp, ExpiredTime._10mins);
            callApiService.localCache(cacheKey, tmp, ExpiredTime._10mins);
            log.debug("[" + ID + "] G06DataById=" + tmp.entrySet());
        } else {
            log.error("[" + ID + "] G06DataById resultCode:" + g06ResultCode);
        }

        return tmp;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getA30WithDefaultTWDataById(String ID) throws Exception {

        String cacheKey = "A30_" + ID;

        Map<String, Object> tmp = (Map<String, Object>) LocalCache.get(cacheKey);
        if (tmp != null && !tmp.isEmpty()) {
            log.debug("[" + ID + "] A30WithDefaultTWDataById=" + tmp.entrySet());
            return tmp;
        }

        try {
            tmp = (Map<String, Object>) cathayLifeTypeAService.a30(ID, "TW");
            String a30ResultCode = MapUtils.getString(tmp, A30.resultCode.name());
            if ("000".equals(a30ResultCode)) {
                LocalCache.register(cacheKey, tmp, ExpiredTime._10mins);
                callApiService.localCache(cacheKey, tmp, ExpiredTime._10mins);
                log.debug("[" + ID + "] A30WithDefaultTWDataById=" + tmp.entrySet());
            } else {
                log.error("[" + ID + "] A30WithDefaultTWDataById resultCode:" + a30ResultCode);
            }
        } catch(Exception e) {
            log.debug("[" + ID + "] A30WithDefaultTWDataById error:" + e.getMessage());
        }

        return tmp;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getAllBotTouristPlace() throws Exception {

        String cacheKey = "AllBotTouristPlace";

        Map<String, Object> tmp = (Map<String, Object>) LocalCache.get(cacheKey);
        if (tmp != null && !tmp.isEmpty()) {
            log.debug("AllBotTouristPlace=" + tmp.entrySet());
            return tmp;
        }

        tmp = new HashMap<String, Object>();

//        List<BotTouristPlace> placeData = botTouristPlaceRepository.findAll();
//        if (CollectionUtils.isNotEmpty(placeData)) {
//            for (BotTouristPlace place : placeData) {
//                if (place != null && place.getId() != null & StringUtils.isNotBlank(place.getPlace())) {
//                    // tmp.put(place.getPlace(), place.getId().getTypeId());
//                    tmp.put(place.getPlace().toLowerCase(), place.getId().getTypeId());
//                }
//            }
//        }
        
        if (MapUtils.isNotEmpty(tmp)) {
            LocalCache.register(cacheKey, tmp, ExpiredTime._1hr);
            callApiService.localCache(cacheKey, tmp, ExpiredTime._1hr);
            log.debug("AllBotTouristPlace=" + tmp.entrySet());
        } else {
            log.error("無法取得BotTouristPlace.");
        }

        return tmp;
    }

    @Override
    public String getPlacePattern() throws Exception {

        String cacheKey = "PlacePattern";

        String tmp = (String) LocalCache.get(cacheKey);
        if (StringUtils.isNotBlank(tmp)) {
            log.debug("PlacePattern=" + tmp);
            return tmp;
        }

        StringBuilder tmpPlace = new StringBuilder();

        Map<String, Object> placeMap = getAllBotTouristPlace();
        if (MapUtils.isNotEmpty(placeMap)) {
            
            List<String> keys = new ArrayList<>(placeMap.keySet());
            Collections.sort(keys, new Comparator<String>() {
                public int compare(String str1, String str2) {
                    return str1.length() < str2.length() ? 1 : (str1.length() == str2.length() ? 0 : -1);
                }
            });
            
            for (String place : keys) {
                if (tmpPlace.length() > 0) {
                    tmpPlace.append("|");
                }
                tmpPlace.append(place.trim());
            }
        }

        tmp = tmpPlace.toString();
        
        if (StringUtils.isNotBlank(tmp)) {
            LocalCache.register(cacheKey, tmp, ExpiredTime._1hr);
            callApiService.localCache(cacheKey, tmp, ExpiredTime._1hr);
            log.debug("PlacePattern=" + tmp);
        } else {
            log.error("無法取得BotTouristPlace.");
        }

        return tmp;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public String genG07PaymentJSON(Map tmpData, String STATUS) {

        /* {"A300":{ A300Data }, "Info":{ InfoData }, "A301":{ A301Data }} */

        String UPDATE_TIME = DateUtil.formatDate(DateUtil.getNow(), DateUtil.PATTERN_DASH_DATE_TIME_WITH_MS) + "000";
        
        Map<String, Object> payment = new HashMap<String, Object>();
        Map<String, Object> A300 = genG07PaymentA300Data(tmpData, UPDATE_TIME, STATUS);
        Map<String, Object> Info = genG07PaymentInfoData(tmpData, UPDATE_TIME);
        Map<String, Object> A301 = genG07PaymentA301Data(tmpData, UPDATE_TIME);

        payment.put("A300", A300);
        payment.put("Info", Info);
        payment.put("A301", A301);

        return new JSONObject(payment).toString();
    }

    @SuppressWarnings("rawtypes")
    private Map<String, Object> genG07PaymentA300Data(Map tmpData, String UPDATE_TIME, String STATUS) {

        // C02.data
        Map CONTRACT_DATA = MapUtils.getMap(tmpData, C02.CONTRACT_DATA.name());
        String TRANS_NO = MapUtils.getString(tmpData, C02.TRANS_NO.name());
        String SYS_NO = MapUtils.getString(tmpData, C02.CLIENT_ID.name());

        // C02.data.CONTRACT_DATA
        String ID = MapUtils.getString(CONTRACT_DATA, C02.APC_ID.name());
        String NAME = MapUtils.getString(CONTRACT_DATA, C02.APC_NAME.name());
        String BIRTHDAY = MapUtils.getString(CONTRACT_DATA, C02.APC_BRDY.name());
        String MOBIL_NO = MapUtils.getString(CONTRACT_DATA, C02.APC_MOBILE.name());
        String EMAIL = MapUtils.getString(CONTRACT_DATA, C02.APC_EMAIL.name());

        // NETINSR_LV 核心會員資格等級從A30來
        String NETINSR_LV = MapUtils.getString(tmpData, "NETINSR_LV");
        String IP = ApiLogUtils.getBotTpiCallLog().getIp();
        String TRANS_SRC = "B";
        String NATION_CODE = MapUtils.getString(tmpData, "NATION_CODE");
        
        String APLY_NO = null;
        String POLICY_NO = null;
        String SETUP_DATE = null;
        if ("5".equals(STATUS)) {
            APLY_NO = MapUtils.getString(CONTRACT_DATA, C02.APLY_NO.name());
            POLICY_NO = MapUtils.getString(CONTRACT_DATA, "POLICY_NO");
            SETUP_DATE = MapUtils.getString(CONTRACT_DATA, "SETUP_DATE");
        }

        /**{
            "A300":{
               "TRANS_NO":"M20181226000000551", // 訂單編號
               "SYS_NO":"CB00", // 商品代號:目前阿發可固定傳CB00
               "ID":"T18982746E", // 核心會員ID
               "NAME":"XXX", // 核心會員姓名
               "BIRTHDAY":"2018-10-10", // 核心會員生日
               "MOBIL_NO":"0911222333", // 核心會員手機
               "EMAIL":"XXX@cathaylife.com.tw", // 核心會員Email
               "NETINSR_LV":"4", // 核心會員資格等級
               "STATUS":"3", // 投保訂單編號狀態(3:資料填寫；5:完成投保)
               "IP":"127.0.0.1", // IP
               "TRANS_SRC":"B", // 投保管道來源，阿發固定傳B
               "UPDATE_TIME":"2019-03-27 16:08:48.284000", // 更新時間
               "APLY_NO":null, // 受理編號(等到核保試算API後會取到受編)
               "POLICY_NO":null, // 保單號碼(等到核保試算API後會取到受編)
               "SETUP_DATE":null, //  成立時間(給當天日期)
               "NATION_CODE":"TW" // 核心會員國籍
            },
            ...
         }*/

        Map<String, Object> A300 = new HashMap<String, Object>();
        A300.put("TRANS_NO", TRANS_NO);
        A300.put("SYS_NO", SYS_NO);
        A300.put("ID", ID);
        A300.put("NAME", NAME);
        A300.put("BIRTHDAY", BIRTHDAY);
        A300.put("MOBIL_NO", MOBIL_NO);
        A300.put("EMAIL", EMAIL);
        A300.put("NETINSR_LV", NETINSR_LV);
        A300.put("STATUS", STATUS);
        A300.put("IP", IP);
        A300.put("TRANS_SRC", TRANS_SRC);
        A300.put("UPDATE_TIME", UPDATE_TIME);
        A300.put("APLY_NO", APLY_NO != null ? APLY_NO : JSONObject.NULL);
        A300.put("POLICY_NO", POLICY_NO != null ? POLICY_NO : JSONObject.NULL);
        A300.put("SETUP_DATE", SETUP_DATE != null ? SETUP_DATE : JSONObject.NULL);
        A300.put("NATION_CODE", NATION_CODE);

        return A300;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> genG07PaymentInfoData(Map tmpData, String UPDATE_TIME) {

        // C02.data
        Map CONTRACT_DATA = MapUtils.getMap(tmpData, C02.CONTRACT_DATA.name());
        String TRANS_NO = MapUtils.getString(tmpData, C02.TRANS_NO.name());
        List INSD_DATA_list = (List) MapUtils.getObject(tmpData, C02.INSD_DATA.name());
        Map INSD_DATA = CollectionUtils.isNotEmpty(INSD_DATA_list) ? (Map) INSD_DATA_list.get(0) : null;

        // C02.data.CONTRACT_DATA
        String RGN_CODE = MapUtils.getString(CONTRACT_DATA, C02.RGN_CODE.name());
        String ISSUE_DATE = MapUtils.getString(CONTRACT_DATA, C02.ISSUE_DATE.name());
        String BGN_TIME = MapUtils.getString(CONTRACT_DATA, C02.BGN_TIME.name()) + ":00";
        // 未呼叫C02，尚未有TOT_PREM
        String PREM = MapUtils.getString(CONTRACT_DATA, C02.TOT_PREM.name());

        // END_DATE需要額外傳入
        String END_DATE = MapUtils.getString(CONTRACT_DATA, "END_DATE");
        
        String PARTNER_NO = MapUtils.getString(CONTRACT_DATA, "PARTNER_NO");

        // C02.data.INSD_DATA
        // MAIN_AMT 主約投保額度為C02死殘保額HD_AMT
        String MAIN_AMT = MapUtils.getString(INSD_DATA, C02.HD_AMT.name());
        String HP_AMT = MapUtils.getString(INSD_DATA, C02.HP_AMT.name());
        String HK_AMT = !"1".equals(RGN_CODE) ? MapUtils.getString(INSD_DATA, C02.HK_AMT.name()) : null;
        String ZIP_CODE = MapUtils.getString(INSD_DATA, C02.ZIP_CODE.name());
        String ADDR = MapUtils.getString(INSD_DATA, C02.ADDR.name());
        String EMAIL = MapUtils.getString(INSD_DATA, C02.EMAIL.name());
        
        String INSR_KIND = "1";
        String IS_GUARD = "N";
        String IS_MAIN_INCOME = "Y";
        String YEAR_INCOME = MapUtils.getString(INSD_DATA, "YEAR_INCOME", "1");
        String POLICY_KIND = "2";
        String PAY_KIND = "L";
        
        // CARD_NO、CARD_END_YM沒有先放null
        String CARD_NO = MapUtils.getString(CONTRACT_DATA, "CARD_NO");
        String CARD_END_YM = MapUtils.getString(CONTRACT_DATA, "CARD_END_YM");
        
        //是否有專機
        String IS_OMTP = MapUtils.getString(INSD_DATA, C02.OMTP_AMT.name());
        
        Object OTHER_RGN = null;
        if ("4".equals(RGN_CODE)) {
            List<String> RGN_NAMEs = (List<String>) MapUtils.getObject(CONTRACT_DATA, "RGN_NAMEs");
            if (CollectionUtils.isNotEmpty(RGN_NAMEs)) {
                StringBuilder names = new StringBuilder();
                for(String name : RGN_NAMEs) {
                    if (names.length() > 0)
                        names.append("、");
                    names.append(name);
                }
                OTHER_RGN = names.toString();
            }
        }
        if (OTHER_RGN == null || StringUtils.isBlank(OTHER_RGN.toString())) {
            OTHER_RGN = JSONObject.NULL;
        }
        
        /**{
            ...
            "Info":{
               "RGN_CODE":"1", // 旅遊地區
               "ISSUE_DATE":"2020-05-23", // 投保期間_起日
               "BGN_TIME":"15:30:00", // 投保期間_出發時間
               "END_DATE":"2020-05-23", // 投保期間_迄日
               "MAIN_AMT":"1500", // 主約投保額度
               "HP_AMT":"0", // 傷害醫療附約投保額度
               "HK_AMT":"0", // 海外突發疾病醫療附約投保額度
               "INSR_KIND":"1", // 投保對象(1:本人；2:本人及家人)  (固定本人)
               "IS_GUARD":"N", // 是否有監護宣告(Y/N)   (固定N)
               "ZIP_CODE":"106", // 郵遞區號
               "ADDR":"仁愛路四段２６９號", // 地址(不要有縣市跟行政區的文字)
               "IS_MAIN_INCOME":"Y", // 是否為家中主要經濟來源者(固定Y)
               "YEAR_INCOME":"1", // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上，固定1)
               "POLICY_KIND":"2", // 保單寄送形式(1:紙本 2:電子)(固定2)
               "EMAIL":"XXXXX@cathlife.com.tw", // 保單寄送EMAIL
               "PREM":"464", // 保費
               "PAY_KIND":null, // 繳費方式(L:信用卡 M:帳戶扣款)(固定L)
               "CARD_NO":null, // 信用卡卡號(給前8碼)
               "CARD_END_YM":null, // 信用卡到期日
               "BANK_NO":null, // 銀行代號
               "ACNT_NO":null, // 銀行帳號
               "REF_NO":null, // 推薦人序號(行銷活動資訊，阿發沒有的話都給null)
               "SEQ_NO":null, // 活動序號(行銷活動資訊，阿發沒有的話都給null)
               "PARTNER_NO":null, // 合作夥伴(行銷活動資訊，阿發沒有的話都給null)
               "PARTNER_ACCOUNT":null, // 合作夥伴帳號(行銷活動資訊，阿發沒有的話都給null)
               "UPDATE_TIME":"2019-01-02 09:11:38.195000", // 異動時間
               "BANK_NAME":null, // 銀行中文(使用帳號扣款才有)
               "FIRST_NAME":null, // 行銷活動資訊，阿發沒有的話都給null
               "LAST_NAME":null, // 行銷活動資訊，阿發沒有的話都給null
               "MILES":null, // 行銷活動資訊，阿發沒有的話都給null
               "MILES_CHECK":null, // 行銷活動資訊，阿發沒有的話都給null
               "COUPON":null, // 行銷活動資訊，阿發沒有的話都給null
               "ACNT_DATE":null, // 繳費完成日(扣款API會回傳的日期)
               "OTHER_RGN":null // 旅遊地點(旅遊地區選其他)，阿發應該沒有就傳null
            },
            ...
         }*/

        Map<String, Object> Info = new HashMap<String, Object>();
        Info.put("TRANS_NO", TRANS_NO);
        Info.put("RGN_CODE", RGN_CODE);
        Info.put("ISSUE_DATE", ISSUE_DATE);
        Info.put("BGN_TIME", BGN_TIME);
        Info.put("END_DATE", END_DATE);
        Info.put("MAIN_AMT", MAIN_AMT);
        Info.put("HP_AMT", HP_AMT);
        Info.put("HK_AMT", HK_AMT);
        Info.put("INSR_KIND", INSR_KIND);
        Info.put("IS_GUARD", IS_GUARD);
        Info.put("ZIP_CODE", ZIP_CODE);
        Info.put("ADDR", ADDR);
        Info.put("IS_MAIN_INCOME", IS_MAIN_INCOME);
        Info.put("YEAR_INCOME", YEAR_INCOME);
        Info.put("POLICY_KIND", POLICY_KIND);
        Info.put("EMAIL", EMAIL);
        Info.put("PREM", PREM);
        Info.put("PAY_KIND", PAY_KIND);
        Info.put("CARD_NO", CARD_NO);
        Info.put("CARD_END_YM", CARD_END_YM);
        Info.put("BANK_NO", JSONObject.NULL);
        Info.put("ACNT_NO", JSONObject.NULL);
        Info.put("REF_NO", JSONObject.NULL);
        Info.put("SEQ_NO", JSONObject.NULL);
        Info.put("PARTNER_NO", StringUtils.isNotBlank(PARTNER_NO) ? PARTNER_NO : JSONObject.NULL);
        Info.put("PARTNER_ACCOUNT", JSONObject.NULL);
        Info.put("UPDATE_TIME", UPDATE_TIME);
        Info.put("BANK_NAME", JSONObject.NULL);
        Info.put("FIRST_NAME", JSONObject.NULL);
        Info.put("LAST_NAME", JSONObject.NULL);
        Info.put("MILES", JSONObject.NULL);
        Info.put("MILES_CHECK", JSONObject.NULL);
        Info.put("COUPON", JSONObject.NULL);
        Info.put("ACNT_DATE", JSONObject.NULL);
        Info.put("OTHER_RGN", OTHER_RGN);
        Info.put("OMTP_AMT", IS_OMTP);

        return Info;
    }

    @SuppressWarnings("rawtypes")
    private Map<String, Object> genG07PaymentA301Data(Map tmpData, String UPDATE_TIME) {

        // C02.data
        Map CONTRACT_DATA = MapUtils.getMap(tmpData, C02.CONTRACT_DATA.name());
        List BENE_DATA_list = (List) MapUtils.getObject(tmpData, C02.BENE_DATA.name());
        Map BENE_DATA = CollectionUtils.isNotEmpty(BENE_DATA_list) ? (Map) BENE_DATA_list.get(0) : null;
        log.debug("[genG07PaymentA301Data] BENE_DATA=" + BENE_DATA.entrySet());
        
        String TRANS_NO = MapUtils.getString(tmpData, C02.TRANS_NO.name());
        
        // C02.data.CONTRACT_DATA
        String BENE_PHONE = MapUtils.getString(CONTRACT_DATA, C02.APC_MOBILE.name());
        String BENE_ZIP = MapUtils.getString(CONTRACT_DATA, C02.APC_ZIP_CODE.name());
        String BENE_ADDR = MapUtils.getString(CONTRACT_DATA, C02.APC_ADDR.name());
        
        // C02.data.BENE_DATA
        String INSD_ID = MapUtils.getString(BENE_DATA, C02.INSD_ID.name());
        
        String BENE_TYPE = MapUtils.getString(BENE_DATA, G06.BENE_TYPE.name());
        String BENE_SER = MapUtils.getString(BENE_DATA, G06.BENE_SER.name());

        // G07 RLTN(與被保人關係): 1法定繼承人 2配偶 3子女 4父母 5祖孫
        // C02 RELATION(與被保人關係): 5 配偶 6 子女 7 父母 8 袓孫 2 法定繼承人 1 親屬 3 朋友 4 其他
        String[] rltnMapping = { "", "", "1", "", "", "2", "3", "4", "5" };
        String c02RELATION = MapUtils.getString(BENE_DATA, C02.RELATION.name());
        int tmpRLTN = NumberUtils.toInt(c02RELATION);
        String RLTN = rltnMapping[tmpRLTN];
        if (StringUtils.isBlank(RLTN)) {
            log.error("genG07_A301發生錯誤，無法取得受益人與被保人關係，C02 RELATION=" + c02RELATION);
            RLTN = "1";
        }
        
        String ID_KIND = MapUtils.getString(BENE_DATA, G06.ID_KIND.name());
        String BENE_ID = MapUtils.getString(BENE_DATA, C02.BENE_ID.name());
        String BENE_NAME = MapUtils.getString(BENE_DATA, C02.BENE_NAME.name());
        
        String NATION_CODE = MapUtils.getString(BENE_DATA, G06.NATION_CODE.name());
        String BENE_BIRTH = MapUtils.getString(BENE_DATA, G06.BENE_BIRTH.name());
        String ADDR_AS_APC = MapUtils.getString(BENE_DATA, G06.ADDR_AS_APC.name());
        
        String BENE_RATO = "100";
        String BENE_PRIO = "0";
        
        /**{
            ...
            "A301":{ // 受益人資訊(如有前次訂單，則取前次訂單第一筆受益人資訊，若無阿發預設都是法繼)
               "A301List":[
                  {
                     "TRANS_NO":"M20181226000000551", // 訂單編號
                     "BENE_TYPE":"1", // 受益人種類(法繼固定傳1)
                     "BENE_SER":1, // 受益人序號(法繼固定傳1)
                     "INSD_ID":"T18982746E", // 被保人ID
                     "RLTN":"1", // 法繼固定傳1
                     "ID_KIND":"1", // 法繼固定傳1
                     "BENE_ID":"0000000000", // 法繼固定傳0000000000
                     "BENE_NAME":"法定繼承人", // 阿發固定傳法定繼承人
                     "NATION_CODE":null, // 法繼固定傳null
                     "BENE_BIRTH":null, // 法繼固定傳null
                     "ADDR_AS_APC":"Y", // 電話及地址同要保人(法繼固定傳Y)
                     "BENE_PHONE":"0911222333", // 核心會員手機
                     "BENE_ZIP":"106", // 郵遞區號
                     "BENE_ADDR":"仁愛路四段２６９號", // 電話及地址同要保人
                     "BENE_RATO":"100", // 獲得理賠方式_比例(固定100)
                     "BENE_PRIO":"0", // 獲得理賠方式_順位(固定傳0)
                     "UPDATE_TIME":"2019-05-07 08:59:31.530000" // 異動日期
                  }
               ]
            }
         }*/

        Map<String, Object> A301 = new HashMap<String, Object>();
        List<Map<String, Object>> A301List = new ArrayList<Map<String, Object>>();
        
        Map<String, Object> A301Map = new HashMap<String, Object>();
        A301Map.put("TRANS_NO", TRANS_NO);
        A301Map.put("BENE_TYPE", BENE_TYPE);
        A301Map.put("BENE_SER", BENE_SER);
        A301Map.put("INSD_ID", INSD_ID);
        A301Map.put("RLTN", RLTN);
        A301Map.put("ID_KIND", ID_KIND);
        A301Map.put("BENE_ID", BENE_ID);
        A301Map.put("BENE_NAME", BENE_NAME);
        A301Map.put("NATION_CODE", NATION_CODE != null ? NATION_CODE : JSONObject.NULL);
        A301Map.put("BENE_BIRTH", BENE_BIRTH);
        A301Map.put("ADDR_AS_APC", ADDR_AS_APC);
        A301Map.put("BENE_PHONE", BENE_PHONE);
        A301Map.put("BENE_ZIP", BENE_ZIP);
        A301Map.put("BENE_ADDR", BENE_ADDR);
        A301Map.put("BENE_RATO", BENE_RATO);
        A301Map.put("BENE_PRIO", BENE_PRIO);
        A301Map.put("UPDATE_TIME", UPDATE_TIME);
        
        A301List.add(A301Map);
        A301.put("A301List", A301List);

        return A301;
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public String genG08FinishInfoJSON(Map a30Data, Map d20Data, Map c03Data, String SYS_NO, String TRANS_NO) {
        
        String INFM_ID = null;
        String MOBIL_NO = null;
        String NAME = null;
        String EMAIL = null;
        String NATIONALITY = "TW";
        String IP = null;
        String TOT_PREM = null;
        String POLICY_NO = null;
        String RESPONSE_TM = null;
        
        if (MapUtils.isNotEmpty(a30Data)) {
            INFM_ID = MapUtils.getString(a30Data, A30.apc_id.name());
            MOBIL_NO = MapUtils.getString(a30Data, A30.mobl_no.name());
            NAME = MapUtils.getString(a30Data, A30.apc_name.name());
            EMAIL = MapUtils.getString(a30Data, A30.email.name());
        }
        
        IP = ApiLogUtils.getBotTpiCallLog().getIp();
        
        if (MapUtils.isNotEmpty(d20Data)) {
            TOT_PREM = MapUtils.getString(d20Data, "TOT_PREM");
            RESPONSE_TM = MapUtils.getString(d20Data, D20.RESPONSE_TM.name());
        }
        
        if (MapUtils.isNotEmpty(c03Data)) {
            Map c03CONTRACT_DATA = MapUtils.getMap(c03Data, C03.CONTRACT_DATA.name());
            POLICY_NO = MapUtils.getString(c03CONTRACT_DATA, C03.POLICY_NO.name());
        }
        
        /** {
            INFM_ID:核心會員ID (要保人ID)
            MOBIL_NO:核心會員手機 (要保人手機)
            NAME:核心會員姓名 (要保人姓名)
            EMAIL: 核心mail  (要保人mail) 
            NATIONALITY:核心國籍(固定TW)
            IP:        client IP 
            SYS_NO : 險種(固定CB00)
            TOT_PREM : 應繳保費
            POLICY_NO:保單號碼 
            RESPONSE_TM: 扣款成立時間
            TRANS_NO:訂單編號
            PREM_NAME : 商品名稱(可不傳) (阿發不用給)
        } */
        
        Map<String, Object> finishInfo = new HashMap<String, Object>();
        finishInfo.put("INFM_ID", INFM_ID);
        finishInfo.put("MOBIL_NO", MOBIL_NO);
        finishInfo.put("NAME", NAME);
        finishInfo.put("EMAIL", EMAIL);
        finishInfo.put("NATIONALITY", NATIONALITY);
        finishInfo.put("IP", IP);
        finishInfo.put("SYS_NO", SYS_NO);
        finishInfo.put("TOT_PREM", TOT_PREM);
        finishInfo.put("POLICY_NO", POLICY_NO);
        finishInfo.put("RESPONSE_TM", RESPONSE_TM);
        finishInfo.put("TRANS_NO", TRANS_NO);
        
        return new JSONObject(finishInfo).toString();
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public String genLineShareText(Map param, Map a30Data, Map c03Data) throws UnsupportedEncodingException {
        
        // String APC_NAME = null;
        // String ISSUE_DAYS = null;
        // String BGN_TIME = null;
        String MIN_DATE_TIME = null;
        String MAX_DATE_TIME = null;
        String RGN_NAME = null;
        // String RGN_PLACE = null;
        String POLICY_NO = null;
        
        /*if (MapUtils.isNotEmpty(a30Data)) {
            APC_NAME = MapUtils.getString(a30Data, A30.apc_name.name());
        }*/
        
        Map ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
        if (MapUtils.isNotEmpty(ISSUE_DAY)) {
             MIN_DATE_TIME = MapUtils.getString(ISSUE_DAY, "MIN_DATE_TIME").split(" ")[0];
             MAX_DATE_TIME = MapUtils.getString(ISSUE_DAY, "MAX_DATE_TIME").split(" ")[0];
             // ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS");
             // BGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME");
        }
        
        String RGN_CODE = MapUtils.getString(param, "RGN_CODE");
        List<String> RGN_NAMEs = (List<String>) MapUtils.getObject(param, "RGN_NAMEs");
        if (!"4".equals(RGN_CODE)) {
            RGN_NAME = getRGN_NAME(RGN_CODE);
        } else {
            StringBuilder names = new StringBuilder();
            for(String name : RGN_NAMEs) {
                if (names.length() > 0)
                    names.append("、");
                names.append(name);
            }
            RGN_NAME = getRGN_NAME(RGN_CODE) + "-" + names.toString();
        }
        
        List<String> RGN_PLACEs = (List<String>) MapUtils.getObject(param, "RGN_PLACEs");
        StringBuilder places = new StringBuilder();
        if (CollectionUtils.isNotEmpty(RGN_PLACEs)) {
            for(String place : RGN_PLACEs) {
                if (places.length() > 0)
                    places.append(",");
                places.append(place);
            }
        }
        // RGN_PLACE = places.length() > 0 ? places.toString() : RGN_NAME;
        
        if (MapUtils.isNotEmpty(c03Data)) {
            Map c03CONTRACT_DATA = MapUtils.getMap(c03Data, C03.CONTRACT_DATA.name());
            POLICY_NO = MapUtils.getString(c03CONTRACT_DATA, C03.POLICY_NO.name());
        }
        
        StringBuilder shareText1 = new StringBuilder();
        shareText1.append("嗨！我將於");
        shareText1.append(MIN_DATE_TIME);
        shareText1.append("~");
        shareText1.append(MAX_DATE_TIME);
        shareText1.append("前往");
        shareText1.append(RGN_NAME);
        shareText1.append("，在國泰人壽投保e悠遊旅行平安保險，保單號碼為");
        shareText1.append(POLICY_NO);
        shareText1.append("喔~");
        // shareText.append("%0D%0A");
        
        //String touristText = "CBA10001_5X1_S_" + RGN_PLACE + "_" + MIN_DATE_TIME + "_" + ISSUE_DAYS + "天_" + BGN_TIME + "!_!{\"flow_prefix\":\"cba\"}";
        //String shareUrl = CBALink.CBA10001_10_WEBSITE_URL + new String((new Base64()).encode(URLEncoder.encode(touristText, "UTF-8").getBytes()), "UTF-8");
        StringBuilder shareText2 = new StringBuilder();
        shareText2.append("【立即前往投保】" + CBALink.CBA10001_10_WEBSITE_URL);
        
        return StringUtils.replaceAll(URLEncoder.encode(StringUtils.replaceAll(shareText1.toString(), " ", "@@"), "UTF-8"), "%40%40", "%20")
                + "%0D%0A" 
                + StringUtils.replaceAll(URLEncoder.encode(StringUtils.replaceAll(shareText2.toString(), " ", "@@"), "UTF-8"), "%40%40", "%20");
    }

    @Override
    public String getCacheTRANS_NO(String cacheKey, String ID) throws Exception {
        String key = cacheKey + ID + "TRANS_NO";
        log.debug("[getCacheTRANS_NO] key=" + key);
        String value = (String) LocalCache.get(key);
        if (value == null) {
            value = (String) callApiService.getTpiCache(key, true);
        }
        String TRANS_NO = null;
        if (StringUtils.isNotBlank(value)) {
            Map<String, Object> tmp = JSONUtils.json2map(value);
            Date DUE_TIME = DateUtil.getDateByPattern(MapUtils.getString(tmp, "DUE_TIME"), DateUtil.PATTERN_DASH_DATE_TIME);
            if (DateUtil.getNow().before(DUE_TIME)) {
                TRANS_NO = MapUtils.getString(tmp, "TRANS_NO");
            } else {
                log.error("[getCacheTRANS_NO] TRANS_NO已經逾期.");
            }
        } else {
            log.error("[getCacheTRANS_NO] 無法取得TRANS_NO或已經逾期.");
        }
        log.debug("[getCacheTRANS_NO] TRANS_NO=" + TRANS_NO);
        return TRANS_NO;
    }

    @Override
    public void setCacheTRANS_NO(String cacheKey, String ID, String TRANS_NO, int expiredTime) throws Exception {
        String key = cacheKey + ID + "TRANS_NO";
        log.debug("[setCacheTRANS_NO] key=" + key);
        Map<String, String> value = new HashMap<String, String>();
        value.put("TRANS_NO", TRANS_NO);
        value.put("DUE_TIME", DateUtil.formatDate(DateUtil.addMinute(DateUtil.getNow(), expiredTime / ExpiredTime._1min), DateUtil.PATTERN_DASH_DATE_TIME));
        String tmp = JSONUtils.obj2json(value);
        LocalCache.register(key, tmp, expiredTime);
        callApiService.localCache(key, tmp, expiredTime, true);
    }
    
    @Override
    public void removeCacheTRANS_NO(String cacheKey, String ID) {
        String key = cacheKey + ID + "TRANS_NO";
        log.debug("[removeCacheTRANS_NO] key=" + key);
        LocalCache.remove(key);
        callApiService.removeCache(key, true);
    }

    @Override
    public String getCacheCompletedTRANS_NO(String cacheKey, String ID) {
        String key = cacheKey + ID + "COMPLETED_TRANS_NO";
        log.debug("[getCacheCompletedTRANS_NO] key=" + key);
        String value = (String) LocalCache.get(key);
        if (value == null) {
            value = (String) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheCompletedTRANS_NO] value=" + value);
        return value;
    }

    @Override
    public void setCacheCompletedTRANS_NO(String cacheKey, String ID, String TRANS_NO, int expiredTime) {
        String key = cacheKey + ID + "COMPLETED_TRANS_NO";
        log.debug("[setCacheCompletedTRANS_NO] key=" + key);
        LocalCache.register(key, TRANS_NO, expiredTime);
        callApiService.localCache(key, TRANS_NO, expiredTime, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getCacheTRANS_DATA(String cacheKey, String ID) {
        String key = cacheKey + ID + "TRANS_DATA";
        log.debug("[getCacheTRANS_DATA] key=" + key);
        Map<String, Object> value = (Map<String, Object>) LocalCache.get(key);
        if (value == null) {
            value = (Map<String, Object>) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheTRANS_DATA] value=" + value);
        return value;
    }
    
    @Override
    public void setCacheTRANS_DATA(String cacheKey, String ID, Map<String, Object> tmpData, int expiredTime) {
        String key = cacheKey + ID + "TRANS_DATA";
        log.debug("[setCacheTRANS_DATA] key=" + key);
        if (LocalCache.get(key) != null) {
            LocalCache.remove(key);
            callApiService.removeCache(key, true);
        }
        LocalCache.register(key, tmpData, expiredTime);
        callApiService.localCache(key, tmpData, expiredTime, true);
    }

    @Override
    public String getCacheZ01_KEY(String cacheKey, String ID) {
        String key = cacheKey + ID + "OTP_S";
        log.debug("[getCacheZ01_KEY] key=" + key);
        String value = (String) LocalCache.get(key);
        if (value == null) {
            value = (String) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheZ01_KEY] value=" + value);
        return value;
    }

    @Override
    public void setCacheZ01_KEY(String cacheKey, String ID, String Z01_KEY, int expiredTime) {
        String key = cacheKey + ID + "OTP_S";
        log.debug("[setCacheZ01_KEY] key=" + key);
        if (LocalCache.get(key) != null) {
            LocalCache.remove(key);
            callApiService.removeCache(key, true);
        }
        LocalCache.register(key, Z01_KEY, expiredTime);
        callApiService.localCache(key, Z01_KEY, expiredTime, true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getCacheCALL_ID_DATA(String cacheKey, String ID) {
        String key = cacheKey + ID + "CALL_ID_DATA";
        log.debug("[getCacheCALL_ID_DATA] key=" + key);
        Map<String, Object> value = (Map<String, Object>) LocalCache.get(key);
        if (value == null) {
            value = (Map<String, Object>) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheCALL_ID_DATA] value=" + value);
        return value;
    }

    @Override
    public void setCacheCALL_ID_DATA(String cacheKey, String ID, Map<String, Object> callIdData, int expiredTime) {
        String key = cacheKey + ID + "CALL_ID_DATA";
        log.debug("[setCacheCALL_ID_DATA] key=" + key);
        if (LocalCache.get(key) != null) {
            LocalCache.remove(key);
            callApiService.removeCache(key, true);
        }
        LocalCache.register(key, callIdData, expiredTime);
        callApiService.localCache(key, callIdData, expiredTime, true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getCacheC02_DATA(String cacheKey, String ID) {
        String key = cacheKey + ID + "C02_DATA";
        log.debug("[getCacheC02_DATA] key=" + key);
        Map<String, Object> value = (Map<String, Object>) LocalCache.get(key);
        if (value == null) {
            value = (Map<String, Object>) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheC02_DATA] value=" + value);
        return value;
    }

    @Override
    public void setCacheC02_DATA(String cacheKey, String ID, Map<String, Object> c02Data, int expiredTime) {
        String key = cacheKey + ID + "C02_DATA";
        log.debug("[setCacheC02_DATA] key=" + key);
        if (LocalCache.get(key) != null) {
            LocalCache.remove(key);
            callApiService.removeCache(key, true);
        }
        LocalCache.register(key, c02Data, expiredTime);
        callApiService.localCache(key, c02Data, expiredTime, true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getCacheD20_DATA(String cacheKey, String ID) {
        String key = cacheKey + ID + "D20_DATA";
        log.debug("[getCacheD20_DATA] key=" + key);
        Map<String, Object> value = (Map<String, Object>) LocalCache.get(key);
        if (value == null) {
            value = (Map<String, Object>) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheD20_DATA] value=" + value);
        return value;
    }

    @Override
    public void setCacheD20_DATA(String cacheKey, String ID, Map<String, Object> d20Data, int expiredTime) {
        String key = cacheKey + ID + "D20_DATA";
        log.debug("[setCacheD20_DATA] key=" + key);
        if (LocalCache.get(key) != null) {
            LocalCache.remove(key);
            callApiService.removeCache(key, true);
        }
        LocalCache.register(key, d20Data, expiredTime);
        callApiService.localCache(key, d20Data, expiredTime, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getCacheD21_DATA(String cacheKey) {
        String key = cacheKey + "D21_DATA";
        log.debug("[getCacheD21_DATA] key=" + key);
        Map<String, Object> value = (Map<String, Object>) LocalCache.get(key);
        if (value == null) {
            value = (Map<String, Object>) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheD21_DATA] value=" + value);
        return value;
    }
    
    @Override
    public void setCacheD21_DATA(String cacheKey, Map<String, Object> d21Data, int expiredTime) {
        String key = cacheKey + "D21_DATA";
        log.debug("[setCacheD21_DATA] key=" + key);
        if (LocalCache.get(key) != null) {
            LocalCache.remove(key);
            callApiService.removeCache(key, true);
        }
        LocalCache.register(key, d21Data, expiredTime);
        callApiService.localCache(key, d21Data, expiredTime, true);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getCacheTOURIST_DATA(String cacheKey) {
        String key = cacheKey + "TOURIST_DATA";
        log.debug("[getCacheTOURIST_DATA] key=" + key);
        Map<String, Object> value = (Map<String, Object>) LocalCache.get(key);
        if (value == null) {
            value = (Map<String, Object>) callApiService.getTpiCache(key);
        }
        log.debug("[getCacheTOURIST_DATA] value=" + value);
        return value;
    }
    
    @Override
    public void setCacheTOURIST_DATA(String cacheKey, Map<String, Object> touristData, int expiredTime) {
        String key = cacheKey + "TOURIST_DATA";
        log.debug("[setCacheTOURIST_DATA] key=" + key);
        if (LocalCache.get(key) != null) {
            LocalCache.remove(key);
            callApiService.removeCache(key, true);
        }
        LocalCache.register(key, touristData, expiredTime);
        callApiService.localCache(key, touristData, expiredTime, true);
    }
}
