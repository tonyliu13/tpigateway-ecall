package com.tp.tpigateway.service.life.flow.impl.cb;

import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.CathayBaseResponse;
import com.tp.tpigateway.model.life.CBV10001VO.CheckAmtVoiceData;
import com.tp.tpigateway.model.life.CBV10001VO.ConfirmVoiceData;
import com.tp.tpigateway.model.life.CBV10001VO.LastIssueData;
import com.tp.tpigateway.model.life.CBV10001VO.LawAgeData;
import com.tp.tpigateway.model.life.CBV10001VO.TempVoiceData;
import com.tp.tpigateway.service.life.flow.cb.CBV10001ApiService;

@Service
public class CBV10001ApiServiceImpl implements CBV10001ApiService{

	@Override
	public ResponseEntity<CathayBaseResponse<LawAgeData>> lawAge(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request
			) {
		return restTemplate.exchange(url,
                HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<LawAgeData>>() {});
	}

	@Override
	public ResponseEntity<CathayBaseResponse<LastIssueData>> lastIssueData(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request) {
		return restTemplate.exchange(url,
                HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<LastIssueData>>() {});
	}

	@Override
	public ResponseEntity<CathayBaseResponse<?>> allVipData(
			BotTpiCallLog3D botTpiCallLog3D, 
			String url,
			RestTemplate restTemplate,
			HttpEntity<Map<String, Object>> request) {
		return restTemplate.exchange(url,
                HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<?>>() {});
	}

	@Override
	public ResponseEntity<CathayBaseResponse<ConfirmVoiceData>> doConfirmVoice(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request) {
		return restTemplate.exchange(url,
                HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<ConfirmVoiceData>>() {});
	}

	@Override
	public ResponseEntity<CathayBaseResponse<TempVoiceData>> insertTempVoice(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request) {
		return restTemplate.exchange(url,
                HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<TempVoiceData>>() {});
	}

	@Override
	public ResponseEntity<CathayBaseResponse<CheckAmtVoiceData>> doCheckAmtVoice(
			BotTpiCallLog3D botTpiCallLog3D,
			String url,
			RestTemplate restTemplate, 
			HttpEntity<Map<String, Object>> request) {
		return restTemplate.exchange(url,
                HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<CheckAmtVoiceData>>() {});
	}

}
