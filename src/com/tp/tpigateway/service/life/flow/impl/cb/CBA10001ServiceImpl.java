package com.tp.tpigateway.service.life.flow.impl.cb;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;
import com.tp.tpigateway.model.common.BotMessageVO.ContentItem;
import com.tp.tpigateway.model.common.BotMessageVO.DataListItem;
import com.tp.tpigateway.model.common.CBALink;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.enumeration.A04;
import com.tp.tpigateway.model.common.enumeration.A30;
import com.tp.tpigateway.model.common.enumeration.C02;
import com.tp.tpigateway.model.common.enumeration.C03;
import com.tp.tpigateway.model.common.enumeration.C04;
import com.tp.tpigateway.model.common.enumeration.D20;
import com.tp.tpigateway.model.common.enumeration.D21;
import com.tp.tpigateway.model.common.enumeration.D22;
import com.tp.tpigateway.model.common.enumeration.G05;
import com.tp.tpigateway.model.common.enumeration.G06;
import com.tp.tpigateway.model.common.enumeration.Z04;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;
import com.tp.tpigateway.service.life.common.CathayLifeLoginService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001CardService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001DataService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001Service;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeCService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeDService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeGService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeHService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeZService;
import com.tp.tpigateway.util.ApiLogUtils;
import com.tp.tpigateway.util.DateUtil;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.LocalCache.ExpiredTime;
import com.tp.tpigateway.util.TPIStringUtil;

/**
 * CBA10001：旅平險投保服務實作
 */
@Service("cBA10001Service")
public class CBA10001ServiceImpl implements CBA10001Service {

    private static Logger log = LoggerFactory.getLogger(CBA10001ServiceImpl.class);

    @Autowired
    private BotMessageService botMessageService;
    @Autowired
    private CBA10001CardService cBA10001CardService;
    @Autowired
    private CBA10001DataService cBA10001DataService;
    @Autowired
    private BotTpiReplyTextService botTpiReplyTextService;
    @Autowired
    private CathayLifeTypeCService cathayLifeTypeCService;
    @Autowired
    private CathayLifeTypeDService cathayLifeTypeDService;
    @Autowired
    private CathayLifeTypeGService cathayLifeTypeGService;
    @Autowired
    private CathayLifeTypeHService cathayLifeTypeHService;
    @Autowired
    private CathayLifeTypeZService cathayLifeTypeZService;
    @Autowired
    private CathayLifeLoginService cathayLifeLoginService;

    /** 迴圈次數(1:CBA10001.5X1.1, 2:CBA10001.5X1.2, 3:CBA10001.5X1.3, 4:CBA10001.5X1.4, 5:CBA10001.5X1.5.1.3, 6:CBA10001.5X1.5.2) */
    private Map<String, Object> CBA_CYCLE = null;

    private String[] CALL_IDs = { "CBA10001_5X1_5_1", "CBA10001_5X1_5_2", "CBA10001_5X1_5_3", "CBA10001_5X1_5_4", "CBA10001_5X1_5_5" };
    private String[][] CALL_INSD_DATA = { { "900", "100", "100" }, { "1200", "120", "120" }, { "600", "60", "60" }, { "300", "30", "30" }, { "900", "150", "150"} };

    private final String CLIENT_ID = "CB00";
    private final String Z_SRC = "AT";
    private final String[] TRANS_NO_PARAM = { "MA", "M" };
    
    private final int TRANS_NO_EXPIRED_TIME = ExpiredTime._5mins;

    @Override
    public Object queryCBA10001(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001：怎麼投保旅平險##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1(source, reqVO);
        }
    }

    /**
     * CBA10001.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1(String source, BotLifeRequestVO reqVO) throws Exception {
        String IDNo = reqVO.getIDNo();
        if (StringUtils.isNotBlank(IDNo)) {
            return getChatWebForCBA10001_1_1(source, reqVO);
        } else {
            return getChatWebForCBA10001_1_2(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1 已核身
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1_1(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        // ID行銷資訊
        Map<String, Object> a04Result = cBA10001DataService.getA04WithDefaultTWDataById(reqVO.getIDNo());

        // 網投旅平險-不受歡迎旅遊地區
        Map<String, Object> c04Result = cBA10001DataService.getC04DataById(reqVO.getIDNo());

        // text{CBA10001.1.1.1}
        String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.1.1.1");
        botMessageService.setTextResult(vo, replyText1);

        // card {CBA10001.1.1.2}
        List<Map<String, Object>> cardList = cBA10001CardService.getCardsForCBA10001_1_1_2(vo, a04Result, c04Result);
        botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);

        return vo.getVO();
    }

    /**
     * CBA10001.1.2 未核身
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1_2(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        // text{CBA10001.1.2.1}
        String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.1.2.1");
        botMessageService.setTextResult(vo, replyText1);

        // card {CBA10001.1.2.2}
        List<Map<String, Object>> cardList = cBA10001CardService.getCardsForCBA10001_1_2_2(vo);
        botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);

        // text{CBA10001.1.2.3}
        String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.1.2.3");
        botMessageService.setTextResult(vo, replyText3);

        // quick reply{CBA10001.1.2.4}
        LifeLink[] links = { CBALink.CBA10001.看自己適合的管道, LifeLink.不了_結束對話 };
        botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));

        return vo.getVO();
    }

    @Override
    public Object queryCBA10001_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_1_1_1：試算投保旅平險##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        Map param = JSONUtils.json2map(reqVO.getJsonParameter());

        // N試算/Y修改
        String IS_FIX = MapUtils.getString(param, "isFixTourist", "N");
        
        if (StringUtils.equals("Y", IS_FIX)) {
            // text{CBA10001.1.1.1.1.2}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.1.1.1.1.2");
            botMessageService.setTextResult(vo, replyText1);
        } else {
            // text{CBA10001.1.1.1.1.1}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.1.1.1.1.1");
            botMessageService.setTextResult(vo, replyText1);
        }

        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        CBA_CYCLE = null;
        data.put("CBA_CYCLE", CBA_CYCLE);
        
        result.put("data", data);
        
        return result;
    }

    @Override
    public Object queryCBA10001_1_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1：輸入旅遊問句##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1.1.1.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {

        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();

        Map<String, Object> result = null;

        Map param = JSONUtils.json2map(reqVO.getJsonParameter());

        // 客戶輸入問句
        String CBA_ORIG_STR = MapUtils.getString(param, "CBA_ORIG_STR", "");
        // 旅遊地點
        List<Object> CBA_PLACEs = (List<Object>) MapUtils.getObject(param, "CBA_PLACEs");
        // 旅遊日期
        List<Object> CBA_DATEs = (List<Object>) MapUtils.getObject(param, "CBA_DATEs");
        // 旅遊時間
        String CBA_TIME = MapUtils.getString(param, "CBA_TIME");
        // 修正時間(當日投保，且修正為半小時後時間)
        String IS_FIX_TIME = MapUtils.getString(param, "isFixTime", "N");
        
        // 迴圈次數
        CBA_CYCLE = MapUtils.getMap(param, "CBA_CYCLE");
        if (CBA_CYCLE == null) {
            CBA_CYCLE = new HashMap<String, Object>();
        }

        Map<String, Object> RGN_RESULT = cBA10001DataService.getRGN_CODE(CBA_ORIG_STR, CBA_PLACEs);
        String RGN_CODE = MapUtils.getString(RGN_RESULT, "RGN_CODE");
        List<String> RGN_NAMEs = (List<String>) MapUtils.getObject(RGN_RESULT, "RGN_NAMEs");
        List<String> RGN_PLACEs = (List<String>) MapUtils.getObject(RGN_RESULT, "RGN_PLACEs");
        if (StringUtils.isBlank(RGN_CODE) && MapUtils.isNotEmpty(param)) {
            RGN_CODE = MapUtils.getString(param, "RGN_CODE", "");
            if (StringUtils.isNotBlank(RGN_CODE)) {
                RGN_NAMEs = (List) MapUtils.getObject(param, "RGN_NAMEs", null);
                RGN_PLACEs = (List) MapUtils.getObject(param, "RGN_PLACEs", null);
            }
        }
        
        // 日期重新處理，取得投保始期(經當日邏輯處理)日期、時間，及原訂起迄(原始日期)MIN_DATE、MAX_DATE
        Map<String, Object> ISSUE_DAY = cBA10001DataService.getISSUE_DAY(sessionId, CBA_DATEs);
        String ISSUE_DATE = MapUtils.getString(ISSUE_DAY, "ISSUE_DATE", null);
        String ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", null);
        String BGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", null);
        Date MIN_DATE = (Date) MapUtils.getObject(ISSUE_DAY, "MIN_DATE", null);
        Date MAX_DATE = (Date) MapUtils.getObject(ISSUE_DAY, "MAX_DATE", null);
        IS_FIX_TIME = MapUtils.getString(ISSUE_DAY, "IS_FIX_TIME", "N");
        if (StringUtils.isBlank(ISSUE_DAYS) && MapUtils.isNotEmpty(param)) {
            ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
            ISSUE_DAYS = MapUtils.isNotEmpty(ISSUE_DAY) ? MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", "") : "";
            if (StringUtils.isNotBlank(ISSUE_DAYS)) {
                String tmpISSUE_DATE = MapUtils.getString(ISSUE_DAY, "MIN_DATE", "");
                String tmpBGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", "");
                if (StringUtils.isBlank(tmpBGN_TIME)) {
                    MIN_DATE = DateUtil.getDateByPattern(tmpISSUE_DATE, DateUtil.PATTERN_DASH_DATE);
                } else {
                    MIN_DATE = DateUtil.getDateByPattern(tmpISSUE_DATE + " " + tmpBGN_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
                }
                MAX_DATE = DateUtil.getDateByPattern(MapUtils.getString(ISSUE_DAY, "MAX_DATE", null), DateUtil.PATTERN_DASH_DATE);
                
                Map<String, String> bgn = cBA10001DataService.getBGN_TIME(MIN_DATE);
                ISSUE_DATE = MapUtils.getString(bgn, "ISSUE_DATE");
                BGN_TIME = MapUtils.getString(bgn, "BGN_TIME");
                IS_FIX_TIME = MapUtils.getString(bgn, "IS_FIX_TIME", "N");
                
                ISSUE_DAY.put("ISSUE_DATE", ISSUE_DATE);
                ISSUE_DAY.put("BGN_TIME", BGN_TIME);
                ISSUE_DAY.put("MIN_DATE", MIN_DATE);
                ISSUE_DAY.put("MAX_DATE", MAX_DATE);
                ISSUE_DAY.put("IS_FIX_TIME", IS_FIX_TIME);
            }
        }
        
        // 出發時間
        if (MIN_DATE != null && StringUtils.isNotBlank(ISSUE_DAYS) && StringUtils.isNotBlank(CBA_TIME)) {
            Map<String, String> tmpDateTime = cBA10001DataService.getDateTime(MIN_DATE, ISSUE_DAYS, CBA_TIME);
            
            ISSUE_DATE = MapUtils.getString(tmpDateTime, "ISSUE_DATE");
            BGN_TIME = MapUtils.getString(tmpDateTime, "BGN_TIME");
            String tmpMIN_DATE_TIME = MapUtils.getString(tmpDateTime, "MIN_DATE_TIME");
            IS_FIX_TIME = MapUtils.getString(tmpDateTime, "IS_FIX_TIME");
            
            MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
            
            ISSUE_DAY.put("ISSUE_DATE", ISSUE_DATE);
            ISSUE_DAY.put("BGN_TIME", BGN_TIME);
            ISSUE_DAY.put("MIN_DATE", MIN_DATE);
            ISSUE_DAY.put("IS_FIX_TIME", IS_FIX_TIME);
        }
        
        // CBA10001.5X1.1 (旅遊地點取得、旅遊日期未取得)
        // CBA10001.5X1.2 (日期取得、地點未取得)
        // CBA10001.5X1.3 (地點及日期皆未取得)
        // CBA10001.5X1.4 (地點及日期皆取得，地點國內且投保期間>30天 或 地點國外且投保期間>180天 或 開始時間為今天以前)
        // CBA10001.5X1.5 (地點及日期皆取得)

        Integer nodeCycle = null;
        if (StringUtils.isNotBlank(RGN_CODE) && !(StringUtils.isNotBlank(ISSUE_DATE) && StringUtils.isNotBlank(ISSUE_DAYS))) {
            log.info("#############CBA10001.5X1.1：旅遊地點取得、旅遊日期未取得##############");
            nodeCycle = MapUtils.getInteger(CBA_CYCLE, "1", 0) + 1;
            CBA_CYCLE.put("1", nodeCycle);
            result = getChatWebForCBA10001_1_1_1_1_1_1(source, reqVO, nodeCycle, RGN_CODE, RGN_NAMEs);
        } else if ((StringUtils.isNotBlank(ISSUE_DATE) && StringUtils.isNotBlank(ISSUE_DAYS)) && StringUtils.isBlank(RGN_CODE)) {
            log.info("#############CBA10001.5X1.2：日期取得、地點未取得##############");
            nodeCycle = MapUtils.getInteger(CBA_CYCLE, "2", 0) + 1;
            CBA_CYCLE.put("2", nodeCycle);
            result = getChatWebForCBA10001_1_1_1_1_1_2(source, reqVO, nodeCycle, ISSUE_DATE, ISSUE_DAYS);
        } else if (StringUtils.isBlank(RGN_CODE) && !(StringUtils.isNotBlank(ISSUE_DATE) && StringUtils.isNotBlank(ISSUE_DAYS))) {
            log.info("#############CBA10001.5X1.3：地點及日期皆未取得##############");
            nodeCycle = MapUtils.getInteger(CBA_CYCLE, "3", 0) + 1;
            CBA_CYCLE.put("3", nodeCycle);
            result = getChatWebForCBA10001_1_1_1_1_1_3(source, reqVO, nodeCycle);
        } else {
            if (!cBA10001DataService.checkISSUE_Period(RGN_CODE, ISSUE_DATE, ISSUE_DAYS)) {
                log.info("#############CBA10001.5X1.4：地點及日期皆取得(期間不正確)##############");
                nodeCycle = MapUtils.getInteger(CBA_CYCLE, "4", 0) + 1;
                CBA_CYCLE.put("4", nodeCycle);
                result = getChatWebForCBA10001_1_1_1_1_1_4(source, reqVO, nodeCycle, RGN_CODE, RGN_NAMEs);
            } else {
                log.info("#############CBA10001.5X1.5：地點及日期皆取得##############");
                CBA_CYCLE = new HashMap<String, Object>();
                result = getChatWebForCBA10001_1_1_1_1_1_5(source, reqVO, sessionId, RGN_CODE, ISSUE_DAY);
            }
        }

        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (CBA_CYCLE != null) {
            data.put("CBA_CYCLE", CBA_CYCLE);
        }
        
        // 旅遊地點
        if (StringUtils.isNotBlank(RGN_CODE)) {
            data.put("RGN_CODE", RGN_CODE);
            data.put("RGN_NAMEs", RGN_NAMEs);
            data.put("RGN_PLACEs", RGN_PLACEs);
        }
        
        // 旅遊日期
        if ((StringUtils.isNotBlank(ISSUE_DATE) && StringUtils.isNotBlank(ISSUE_DAYS))) {
            Map<String, Object> tmp_ISSUE_DAY = new HashMap<String, Object>();
            tmp_ISSUE_DAY.put("ISSUE_DAYS", ISSUE_DAYS);
            if (StringUtils.isNotBlank(BGN_TIME)) {
                tmp_ISSUE_DAY.put("BGN_TIME", BGN_TIME);
            }
            tmp_ISSUE_DAY.put("MIN_DATE", DateUtil.formatDate(MIN_DATE, DateUtil.PATTERN_DASH_DATE));
            tmp_ISSUE_DAY.put("MAX_DATE", DateUtil.formatDate(MAX_DATE, DateUtil.PATTERN_DASH_DATE));
            data.put("ISSUE_DAY", tmp_ISSUE_DAY);
        }
        
        if (StringUtils.isNotBlank(CBA_TIME)) {
            data.put("CBA_TIME", CBA_TIME);
        }
        
        if (StringUtils.isNotBlank(IS_FIX_TIME)) {
            data.put("isFixTime", IS_FIX_TIME);
        } else {
            data.put("isFixTime", "N");
        }
        
        result.put("data", data);
        
        return result;
    }

    /**
     * CBA10001.5X1.1 (旅遊地點取得、旅遊日期未取得)
     * 
     * @param source
     * @param reqVO
     * @param nodeCycle
     * @param RGN_CODE
     * @param RGN_NAMEs
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_1(String source, BotLifeRequestVO reqVO, Integer nodeCycle, String RGN_CODE, List<String> RGN_NAMEs) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        boolean needTouristData = false;
        if (nodeCycle == null || nodeCycle.intValue() < 2) { // 迴圈第二次時不顯示
            // text{CBA10001.5X1.1.1}
            // 旅遊地點
            String RGN_NAME = cBA10001DataService.getRGN_NAME(RGN_CODE);
            Map<String, String> replaceParam = new HashMap<String, String>();
            replaceParam.put("TOURIST_PLACE", RGN_NAME);
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.1.1");
            botMessageService.setTextResultWithReplace(vo, replyText1, replaceParam);
            needTouristData = true;
        } else { // 迴圈第二次時才顯示
            // text{CBA10001.5X1.1.2}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.1.2");
            botMessageService.setTextResult(vo, replyText1);

            // quick reply{CBA10001.5X1.1.3}
            // LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保 };
            LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保(reqVO.getIDNo(), RGN_CODE, null, null, null, null, null) };
            botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
        }
        
        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (needTouristData) {
            data.put("needTouristData", "Y");
        } else {
            data.put("needTouristData", "N");
        }
        
        result.put("data", data);

        return result;
    }

    /**
     * CBA10001.5X1.2 (日期取得、地點未取得)
     * 
     * @param source
     * @param reqVO
     * @param nodeCycle
     * @param ISSUE_DATE
     * @param ISSUE_DAYS
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_2(String source, BotLifeRequestVO reqVO, Integer nodeCycle, String ISSUE_DATE, String ISSUE_DAYS) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        boolean needTouristData = false;
        if (nodeCycle == null || nodeCycle.intValue() < 2) { // 迴圈第二次時不顯示
            boolean inPeriod = cBA10001DataService.checkISSUE_Period(null, ISSUE_DATE, ISSUE_DAYS);
            if (inPeriod) {
                // text{CBA10001.5X1.2.1}
                String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.2.1");
                botMessageService.setTextResult(vo, replyText1);
            } else {
                // text{CBA10001.5X1.2.2}
                String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.2.2");
                botMessageService.setTextResult(vo, replyText2);

                // text{CBA10001.5X1.2.3}
                String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.2.3");
                botMessageService.setTextResult(vo, replyText3);
            }
            
            needTouristData = true;
        } else { // 迴圈第二次時才顯示
            // text{CBA10001.5X1.2.4}
            String replyText4 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.2.4");
            botMessageService.setTextResult(vo, replyText4);

            boolean inPeriod = cBA10001DataService.checkISSUE_Period(null, ISSUE_DATE, ISSUE_DAYS);
            
            // quick reply{CBA10001.5X1.1.5}
            if (inPeriod) {
                LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保(reqVO.getIDNo(), null, ISSUE_DAYS, ISSUE_DATE, null, null, null) };
                botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
            } else {
                LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保 };
                botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
            }
        }
        
        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (needTouristData) {
            data.put("needTouristData", "Y");
        } else {
            data.put("needTouristData", "N");
        }
        
        result.put("data", data);

        return result;
    }

    /**
     * CBA10001.5X1.3 (地點及日期皆未取得)
     * 
     * @param source
     * @param reqVO
     * @param nodeCycle
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_3(String source, BotLifeRequestVO reqVO, Integer nodeCycle) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        boolean needTouristData = false;
        if (nodeCycle == null || nodeCycle.intValue() < 2) { // 迴圈第二次時不顯示
            // text{CBA10001.5X1.3.1}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.3.1");
            botMessageService.setTextResult(vo, replyText1);
            needTouristData = true;
        } else { // 迴圈第二次時才顯示
            // text{CBA10001.5X1.3.2}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.3.2");
            botMessageService.setTextResult(vo, replyText1);

            // quick reply{CBA10001.5X1.3.3}
            LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保 };
            botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
        }
        
        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (needTouristData) {
            data.put("needTouristData", "Y");
        } else {
            data.put("needTouristData", "N");
        }
        
        result.put("data", data);

        return result;
    }

    /**
     * CBA10001.5X1.4 (地點及日期皆取得，地點國內且投保期間>30天 或 地點國外且投保期間>180天 或 開始時間為今天以前)
     * 
     * @param source
     * @param reqVO
     * @param nodeCycle
     * @param RGN_CODE
     * @param RGN_NAMEs
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_4(String source, BotLifeRequestVO reqVO, Integer nodeCycle, String RGN_CODE, List<String> RGN_NAMEs) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        boolean needTouristData = false;
        if (nodeCycle == null || nodeCycle.intValue() < 2) { // 迴圈第二次時不顯示
            // text{CBA10001.5X1.4.1}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.4.1");
            botMessageService.setTextResult(vo, replyText1);

            // text{CBA10001.5X1.4.2}
            // 旅遊地點
            String RGN_NAME = cBA10001DataService.getRGN_NAME(RGN_CODE);
            Map<String, String> replaceParam = new HashMap<String, String>();
            replaceParam.put("TOURIST_PLACE", RGN_NAME);
            String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.4.2");
            botMessageService.setTextResultWithReplace(vo, replyText2, replaceParam);
            
            needTouristData = true;
        } else { // 迴圈第二次時才顯示
            // text{CBA10001.5X1.4.3}
            String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.4.3");
            botMessageService.setTextResult(vo, replyText3);

            // quick reply{CBA10001.5X1.4.4}
            // LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保 };
            LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保(reqVO.getIDNo(), RGN_CODE, null, null, null, null, null) };
            botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
        }
        
        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (needTouristData) {
            data.put("needTouristData", "Y");
        } else {
            data.put("needTouristData", "N");
        }
        
        result.put("data", data);

        return result;
    }

    /**
     * CBA10001.5X1.5 (地點及日期皆取得)
     * 
     * @param source
     * @param reqVO
     * @param sessionId
     * @param RGN_CODE
     * @param ISSUE_DAY
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5(String source, BotLifeRequestVO reqVO, String sessionId, String RGN_CODE, Map<String, Object> ISSUE_DAY) throws Exception {
        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
        
        StringBuilder replyText = new StringBuilder();

        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        // 取消專機確認浮出視窗後要多一個問句
        boolean IS_CANCEL = MapUtils.getBoolean(param, "IS_CANCEL", false);
        if(IS_CANCEL) {
            // 抱歉啦！套餐方案有附加海外醫療專機運送服務需要同意重要告知事項喔，請幫我重新選擇投保套餐吧~
            replyText.append(botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.0.1.5"));
        }else {
        	
        	// text{CBA10001.5X1.5.0}
        	replyText.append(botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.0"));
        	
        	// 活動wording，有值時接續顯示
        	// text{CBA10001.5X1.5.ACT.1}
        	String act_1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.ACT.1");
        	if (StringUtils.isNotBlank(act_1)) {
        		replyText.append(act_1);
        	}
        }
        
        botMessageService.setTextResult(vo, replyText.toString());
        
        if(StringUtils.equals(RGN_CODE, "11"))
        	RGN_CODE = "7";
        
        return getChatWebForCBA10001_5X1_5(vo, reqVO, sessionId, RGN_CODE, ISSUE_DAY);
    }

    @SuppressWarnings({ "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_5X1_5(BotMessageVO vo, BotLifeRequestVO reqVO, String sessionId, String RGN_CODE, Map<String, Object> ISSUE_DAY) throws Exception {
    	
        Map<String, Object> result = new HashMap<String, Object>();

        Map<String, Object> a30Result = null;
        if (StringUtils.isNotBlank(reqVO.getIDNo())) {
            a30Result = cBA10001DataService.getA30WithDefaultTWDataById(reqVO.getIDNo());
        }

        String ISSUE_DATE = MapUtils.getString(ISSUE_DAY, "ISSUE_DATE", null);
        String BGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", null);
        String ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", null);
        Date MIN_DATE = (Date) MapUtils.getObject(ISSUE_DAY, "MIN_DATE", null);
//        Date MAX_DATE = (Date) MapUtils.getObject(ISSUE_DAY, "MAX_DATE", null);
        String IS_FIX_TIME = MapUtils.getString(ISSUE_DAY, "IS_FIX_TIME", "N");

        ConcurrentHashMap<String, Object> C01_map = new ConcurrentHashMap<String, Object>();
        
        boolean IS_OMTP = cBA10001DataService.getOMTP(RGN_CODE);

        try {
        	// 判斷請求試算保單數量變數
        	int callIDLen = 0;
        	
            // 契約內容單筆
            Map<String, String> CONTRACT_DATA = cBA10001DataService.genC01_CONTRACT_DATA(ISSUE_DATE, ISSUE_DAYS, BGN_TIME, RGN_CODE);

            // 套餐-熱門方案
            cathayLifeTypeCService.c01WithAsync(C01_map, CALL_IDs[0], CLIENT_ID, sessionId, CONTRACT_DATA, cBA10001DataService.genC01_INSD_DATA(RGN_CODE, CALL_INSD_DATA[0][0], CALL_INSD_DATA[0][1], CALL_INSD_DATA[0][2], IS_OMTP));
            callIDLen++;
            
            // 尊榮方案目前暫停
//            // 套餐-尊榮方案
//            cathayLifeTypeCService.c01WithAsync(C01_map, CALL_IDs[1], CLIENT_ID, sessionId, CONTRACT_DATA, cBA10001DataService.genC01_INSD_DATA(RGN_CODE, CALL_INSD_DATA[1][0], CALL_INSD_DATA[1][1], CALL_INSD_DATA[1][2], IS_OMTP));
//            callIDLen++;
            
            // 套餐-經濟方案
            cathayLifeTypeCService.c01WithAsync(C01_map, CALL_IDs[2], CLIENT_ID, sessionId, CONTRACT_DATA, cBA10001DataService.genC01_INSD_DATA(RGN_CODE, CALL_INSD_DATA[2][0], CALL_INSD_DATA[2][1], CALL_INSD_DATA[2][2], false));
            callIDLen++;
            // 套餐-大省方案
            cathayLifeTypeCService.c01WithAsync(C01_map, CALL_IDs[3], CLIENT_ID, sessionId, CONTRACT_DATA, cBA10001DataService.genC01_INSD_DATA(RGN_CODE, CALL_INSD_DATA[3][0], CALL_INSD_DATA[3][1], CALL_INSD_DATA[3][2], false));
            callIDLen++;
           
            if(StringUtils.equals(RGN_CODE, "7")) {
            	// 套餐-申根方案 (申根方案RGN_CODE=11，其餘都帶入非申根套餐)
            	cathayLifeTypeCService.c01WithAsync(C01_map, CALL_IDs[4], CLIENT_ID, sessionId, CONTRACT_DATA, cBA10001DataService.genC01_INSD_DATA("11", CALL_INSD_DATA[4][0], CALL_INSD_DATA[4][1], CALL_INSD_DATA[4][2], false));
            	callIDLen++;
            }

            // 等候C01_網投旅平險-保費試算API呼叫完成
            while (true) {
                if (C01_map.size() >= callIDLen) {
                    break;
                }
                Thread.sleep(10);
            }

        } catch (Exception c01Ex) {
            log.error("c01WithAsync發生錯誤：" + c01Ex.getMessage(), c01Ex);
        }

        Object callMap0 = C01_map.get(CALL_IDs[0]);

        // 尊榮方案目前暫停
//        Object callMap1 = C01_map.get(CALL_IDs[1]);
        
        Object callMap2 = C01_map.get(CALL_IDs[2]);
        Object callMap3 = C01_map.get(CALL_IDs[3]);
		Object callMap4 = C01_map.get(CALL_IDs[4]);
		
        // 套餐-熱門方案 總保費
        String TOT_PREM1 = cBA10001DataService.getTOT_PREM((callMap0 != null && callMap0 instanceof Map) ? (Map<String, Object>) callMap0 : null);
        
        // 尊榮方案目前暫停
//        // 套餐-尊榮方案 總保費
//        String TOT_PREM2 = cBA10001DataService.getTOT_PREM((callMap0 != null && callMap1 instanceof Map) ? (Map<String, Object>) callMap1 : null);
        
        // 套餐-經濟方案 總保費
        String TOT_PREM3 = cBA10001DataService.getTOT_PREM((callMap0 != null && callMap2 instanceof Map) ? (Map<String, Object>) callMap2 : null);
        // 套餐-大省方案 總保費
        String TOT_PREM4 = cBA10001DataService.getTOT_PREM((callMap0 != null && callMap3 instanceof Map) ? (Map<String, Object>) callMap3 : null);
        // 套餐-申根方案 總保費
        String TOT_PREM5 = "";
        if(StringUtils.equals(RGN_CODE, "7")) {
        	TOT_PREM5 = cBA10001DataService.getTOT_PREM((callMap0 != null && callMap4 instanceof Map) ? (Map<String, Object>) callMap4 : null);
        }

        // card {CBA10001.5X1.5.1}
        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        int MAX_TOT_PREM = 0;

        Map<String, String> tmpDateTime = cBA10001DataService.getDateTime(MIN_DATE, ISSUE_DAYS);
        String MIN_DATE_TIME = MapUtils.getString(tmpDateTime, "MIN_DATE_TIME");
        String MAX_DATE_TIME = MapUtils.getString(tmpDateTime, "MAX_DATE_TIME");
        
        // 套餐-申根方案 牌卡
        if(StringUtils.equals(RGN_CODE, "7")) {
        	// [總保費]≠正常則不顯示該張牌卡
        	if (StringUtils.isNotBlank(TOT_PREM5)) {
        		CardItem card = cBA10001CardService.getCardForCBA10001_5X1_5(vo, "套餐-申根方案", "11", ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, CALL_INSD_DATA[4], TOT_PREM5, CALL_IDs[4], false);
        		cardList.add(card.getCards());
        		int TOT_PREM = NumberUtils.toInt(TOT_PREM5);
        		if (TOT_PREM > MAX_TOT_PREM) {
        			MAX_TOT_PREM = TOT_PREM;
        		}
        	}
        }
        
        // 套餐-熱門方案 牌卡
        // [總保費]≠正常則不顯示該張牌卡
        if (StringUtils.isNotBlank(TOT_PREM1)) {
            CardItem card = cBA10001CardService.getCardForCBA10001_5X1_5(vo, "套餐-熱門方案", RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, CALL_INSD_DATA[0], TOT_PREM1, CALL_IDs[0], IS_OMTP);
            cardList.add(card.getCards());
            int TOT_PREM = NumberUtils.toInt(TOT_PREM1);
            if (TOT_PREM > MAX_TOT_PREM) {
                MAX_TOT_PREM = TOT_PREM;
            }
        }

        boolean over65 = cBA10001DataService.checkOver65(a30Result);

        // 尊榮方案目前暫停
//        // 套餐-尊榮方案 牌卡
//        // 已驗身=登入者年齡65歲以上不顯示此牌卡
//        // [總保費]≠正常則不顯示該張牌卡
//        if (StringUtils.isNotBlank(TOT_PREM2) && !over65) {
//            CardItem card = cBA10001CardService.getCardForCBA10001_5X1_5(vo, "套餐-尊榮方案", RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, CALL_INSD_DATA[1], TOT_PREM2, CALL_IDs[1], IS_OMTP);
//            cardList.add(card.getCards());
//            int TOT_PREM = NumberUtils.toInt(TOT_PREM2);
//            if (TOT_PREM > MAX_TOT_PREM) {
//                MAX_TOT_PREM = TOT_PREM;
//            }
//        }

        // 套餐-經濟方案 牌卡
        // [總保費]≠正常則不顯示該張牌卡
        if (StringUtils.isNotBlank(TOT_PREM3)) {
            CardItem card = cBA10001CardService.getCardForCBA10001_5X1_5(vo, "套餐-經濟方案", RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, CALL_INSD_DATA[2], TOT_PREM3, CALL_IDs[2], false);
            cardList.add(card.getCards());
            int TOT_PREM = NumberUtils.toInt(TOT_PREM3);
            if (TOT_PREM > MAX_TOT_PREM) {
                MAX_TOT_PREM = TOT_PREM;
            }
        }

        // 套餐-大省方案 牌卡
        // [總保費]≠正常則不顯示該張牌卡
        if (StringUtils.isNotBlank(TOT_PREM4)) {
            CardItem card = cBA10001CardService.getCardForCBA10001_5X1_5(vo, "套餐-大省方案", RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, CALL_INSD_DATA[3], TOT_PREM4, CALL_IDs[3], false);
            cardList.add(card.getCards());
            int TOT_PREM = NumberUtils.toInt(TOT_PREM4);
            if (TOT_PREM > MAX_TOT_PREM) {
                MAX_TOT_PREM = TOT_PREM;
            }
        }

        // 其他投保方案
        CardItem otherCard = cBA10001CardService.getCard2ForCBA10001_5X1_5(vo, reqVO.getIDNo(), RGN_CODE, ISSUE_DAYS, ISSUE_DATE, null, null, null);
        cardList.add(otherCard.getCards());

        // card{CBA10001.5X1.5.1}
        botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);

        StringBuilder replyText3 = new StringBuilder();
        
        // 上述牌卡其中之一[總保費]超過1,100元則顯示該內容
        // text(CBA10001.5X1.5.3)
        if (MAX_TOT_PREM >= 1100 && !"1".equals(RGN_CODE)) {
            replyText3.append(botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.3"));
        }
        replyText3.append(botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.3b"));
        botMessageService.setTextResult(vo, replyText3.toString());

        StringBuilder replyText2 = new StringBuilder();
        Map<String, String> replaceParam = new HashMap<String, String>();

        replyText2.append(botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.4"));
        
        // 投保開始時間為今天則顯示該文字，後端以半小時為基準進行保費試算(20分.50分為基準)。例如：19:50-20:20，投保開始時間為：20:30；19:20-19:49，投保開始時間為：20:00。
        // text{CBA10001.5X1.5.2}
        // if (cBA10001DataService.checkToday(MIN_DATE)) {
        if (StringUtils.equals("Y", IS_FIX_TIME)) { // 有修正時間才顯示
            // 時間(時:分)
            replaceParam.put("THIS_TIME", DateUtil.formatDate(DateUtil.getNow(), DateUtil.PATTERN_TIME_2));
            replaceParam.put("START_TIME", BGN_TIME);
            replyText2.append(botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.2"));
        }
        
        if (replyText2.length() > 0) {
            if (MapUtils.isEmpty(replaceParam)) {
                botMessageService.setTextResult(vo, replyText2.toString());
            } else {
                botMessageService.setTextResultWithReplace(vo, replyText2.toString(), replaceParam);
            }
        }
        
        if(StringUtils.equals(RGN_CODE, "7") && StringUtils.isNotBlank(TOT_PREM5)) {
        	StringBuilder replyText4 = new StringBuilder();
        	replyText4.append(botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.5"));
        	botMessageService.setTextResult(vo, replyText4.toString());
        }

        result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }

        // 套餐-熱門方案
        Map<String, Object> callIdData0 = new HashMap<String, Object>();
        callIdData0.put("TOT_PREM", TOT_PREM1);
        callIdData0.put("CALL_INSD_DATA_SEQ", 0);
        callIdData0.put("IS_OMTP", IS_OMTP);
        data.put(CALL_IDs[0], callIdData0);

        // 尊榮方案目前暫停
//        // 套餐-尊榮方案
//        Map<String, Object> callIdData1 = new HashMap<String, Object>();
//        callIdData1.put("TOT_PREM", TOT_PREM2);
//        callIdData1.put("CALL_INSD_DATA_SEQ", 1);
//        callIdData1.put("IS_OMTP", IS_OMTP);
//        data.put(CALL_IDs[1], callIdData1);

        Map<String, Object> callIdData2 = new HashMap<String, Object>();
        callIdData2.put("TOT_PREM", TOT_PREM3);
        callIdData2.put("CALL_INSD_DATA_SEQ", 2);
        callIdData2.put("IS_OMTP", false);
        data.put(CALL_IDs[2], callIdData2);

        Map<String, Object> callIdData3 = new HashMap<String, Object>();
        callIdData3.put("TOT_PREM", TOT_PREM4);
        callIdData3.put("CALL_INSD_DATA_SEQ", 3);
        callIdData3.put("IS_OMTP", false);
        data.put(CALL_IDs[3], callIdData3);

        if(StringUtils.equals(RGN_CODE, "7") && StringUtils.isNotBlank(TOT_PREM5)) {
        	Map<String, Object> callIdData4 = new HashMap<String, Object>();
        	callIdData4.put("TOT_PREM", TOT_PREM5);
        	callIdData4.put("CALL_INSD_DATA_SEQ", 4);
        	callIdData4.put("IS_OMTP", false);
        	data.put(CALL_IDs[4], callIdData4);
        }
        
        data.put("needTouristData", "N");
        
        result.put("data", data);

        return result;
    }
    
    @Override
    public Object queryCBA10001_1_1_1_1_1_5_1(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1_5_1：確認投保##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_1(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1.1.1.1.5.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1(String source, BotLifeRequestVO reqVO) throws Exception {

        Map<String, Object> result = new HashMap<String, Object>();

        String ID = reqVO.getIDNo();
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        String ivUser = MapUtils.getString(param, "ivUser");
        if (!(StringUtils.isNotBlank(ivUser) && !"Unauthenticated".equals(ivUser))) {
            reqVO.setLoginType("type2");
            return cathayLifeLoginService.doLogin(reqVO, source);
        }
        
        // 確認是否為專機，並跳出確認視窗 // 0 = 非專機、不用確認、 1 = 專機、未確認、 2 = 專機、已確認
        String confirmNotice = MapUtils.getString(param, "confirmNotice");
        if(confirmNotice == null || StringUtils.equals(confirmNotice, "1")) {
        	return getChatWebForCBA10001_1_1_1_1_1_5_1_5(source, reqVO);
        }
        
        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();

        // CBA10001.5X1.5.1.1 A04[IS_NETINSR]=Y、C04[resultCode]=000、G05[resultCode]=000、G06[resultCode]=000、C02[resultCode]=000
        // CBA10001.5X1.5.1.2 A04[IS_NETINSR]≠Y、C04、G05、G06、C02呼叫任一失敗
        // CBA10001.5X1.5.1.3 A04[IS_NETINSR]=Y、C04[resultCode]=000、G06[resultCode]≠000

        boolean checkResult = true;
        
        // ID行銷資訊
        Map<String, Object> a04Result = cBA10001DataService.getA04WithDefaultTWDataById(ID);
        String IS_NETINSR = MapUtils.getString(a04Result, A04.IS_NETINSR.name());
        checkResult = checkResult & "Y".equals(IS_NETINSR);
        
        // A04[IS_NETINSR]≠Y
        if (!checkResult) {
            log.info("#############CBA10001.5X1.5.1.4##############");
            result = getChatWebForCBA10001_1_1_1_1_1_5_1_4(source, reqVO);
        } else { // A04[IS_NETINSR]=Y
            // 網投旅平險-不受歡迎旅遊地區
            Map<String, Object> c04Result = cBA10001DataService.getC04DataById(ID);
            String c04ResultCode = MapUtils.getString(c04Result, C04.resultCode.name());
            checkResult = checkResult & "000".equals(c04ResultCode);
            
            Map ISSUE_DAY = null;
            String MIN_DATE_TIME = null;
            String MAX_DATE_TIME = null;
            
            if (checkResult) { // A04[IS_NETINSR]=Y、C04[resultCode]=000
                
                // 查詢網投旅平險最新訂單資訊
                Map<String, Object> g06Result = cBA10001DataService.getG06DataById(ID);
                String g06ResultCode = MapUtils.getString(g06Result, G06.resultCode.name());
                
                if (!"000".equals(g06ResultCode)) { // G06[resultCode]≠000
                    log.info("#############CBA10001.5X1.5.1.3##############");
                    
                    // 迴圈次數
                    CBA_CYCLE = MapUtils.getMap(param, "CBA_CYCLE");
                    if (CBA_CYCLE == null) {
                        CBA_CYCLE = new HashMap<String, Object>();
                    }
                    
                    log.debug("(" + sessionId + ") CBA_CYCLE=" + CBA_CYCLE.entrySet());
                    
                    Integer nodeCycle = 1; // MapUtils.getInteger(CBA_CYCLE, "5", 0) + 1;
                    CBA_CYCLE.put("5", nodeCycle);
                    
                    Map<String, Object> d21Result = cBA10001DataService.getCacheD21_DATA(sessionId);
                    if (d21Result != null) {
                        result = getChatWebForCBA10001_1_1_1_1_1_5_1_3(source, reqVO);
                    } else {
                        result = getChatWebForCBA10001_1_1_1_1_1_5_1_3(source, reqVO, nodeCycle);
                    }
                } else { // G06[resultCode]=000
                    Map g06Data = MapUtils.getMap(g06Result, G06.data.name());
                    
                    // 取得網投旅平險訂單編號
                    Map<String, Object> g05Result = cathayLifeTypeGService.g05WithNoException(TRANS_NO_PARAM[0], TRANS_NO_PARAM[1]);
                    String g05ResultCode = MapUtils.getString(g05Result, G05.resultCode.name());
                    checkResult = checkResult & "000".equals(g05ResultCode);
                    
//                String c02ResultCode = null;
//                Map c02Data = null;
                    String TRANS_NO = null;
                    Map<String, Object> CONTRACT_DATA = null;
                    List<Map<String, String>> INSD_DATA = null;
                    List<Map<String, Object>> BENE_DATA = null;
                    Map<String, Object> TRAL_TMP = null;
                    
                    Map<String, Object> tmpData = new HashMap<String, Object>();
                    
                    if ("000".equals(g05ResultCode)) {
                        
                        String RGN_CODE = MapUtils.getString(param, "RGN_CODE");
                        List<String> RGN_NAMEs = (List<String>) MapUtils.getObject(param, "RGN_NAMEs");
                        List<String> RGN_PLACEs = (List<String>) MapUtils.getObject(param, "RGN_PLACEs");
                        ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
                        String CALL_ID = MapUtils.getString(param, "CALL_ID");
                        // 自訂綁定TRANS_NO使用
                        String TOURIST_ID = TPIStringUtil.getUUIDNoSeparator();
                        
                        RGN_CODE = planRGN_CODEforSchengen(RGN_CODE,CALL_ID);
                        
                        log.debug("(" + sessionId + ") RGN_CODE=" + RGN_CODE + ", CALL_ID=" + CALL_ID + ", TOURIST_ID=" + TOURIST_ID + ", ISSUE_DAY=" + ISSUE_DAY.entrySet());
                        
                        Map callIdData = MapUtils.getMap(param, CALL_ID);
                        int TOT_PREM = NumberUtils.toInt(MapUtils.getString(callIdData, "TOT_PREM"));
                        int CALL_INSD_DATA_SEQ = MapUtils.getIntValue(callIdData, "CALL_INSD_DATA_SEQ");
                        boolean IS_OMTP = MapUtils.getBooleanValue(callIdData, "IS_OMTP");
                        
                        log.debug("(" + sessionId + ") TOT_PREM=" + TOT_PREM + ", CALL_INSD_DATA_SEQ=" + CALL_INSD_DATA_SEQ + ", callIdData=" + callIdData.entrySet() + ", IS_OMTP=" + IS_OMTP);
                        
                        String HD_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][0];
                        String HP_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][1];
                        String HK_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][2];
                        
                        Map g05Data = MapUtils.getMap(g05Result, G05.data.name());
                        TRANS_NO = MapUtils.getString(g05Data, G05.TRANS_NO.name());
//                    String TRANS_UUID = sessionId;
//                    String ZS_UUID = null;
//                    String contract_TRANS_NO = TRANS_NO;
                        
                        String ISSUE_DATE = null; // MapUtils.getString(ISSUE_DAY, "ISSUE_DATE", null);
                        String ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", null);
                        Date MIN_DATE = null;
                        String tmpBGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", "");
                        String tmpMIN_DATE = MapUtils.getString(ISSUE_DAY, "MIN_DATE", null);
                        if (StringUtils.isBlank(tmpBGN_TIME)) {
                            MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE, DateUtil.PATTERN_DASH_DATE);
                        } else {
                            MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE + " " + tmpBGN_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
                        }
                        
                        Map<String, String> tmpDateTime = cBA10001DataService.getDateTime(MIN_DATE, ISSUE_DAYS);
                        ISSUE_DATE = MapUtils.getString(tmpDateTime, "ISSUE_DATE");
                        String BGN_TIME = MapUtils.getString(tmpDateTime, "BGN_TIME");
                        MIN_DATE_TIME = MapUtils.getString(tmpDateTime, "MIN_DATE_TIME");
                        MAX_DATE_TIME = MapUtils.getString(tmpDateTime, "MAX_DATE_TIME");
                        
                        Map<String, Object> a30Result = cBA10001DataService.getA30WithDefaultTWDataById(ID);
                        String a30ResultCode = MapUtils.getString(a30Result, A30.resultCode.name());
                        checkResult = checkResult & "000".equals(a30ResultCode);
                        
                        if (checkResult) {
                            Map a30Data = MapUtils.getMap(a30Result, A30.data.name());
                            
                            CONTRACT_DATA = cBA10001DataService.genC02_CONTRACT_DATA(a30Data, g06Data, null, null, RGN_CODE, ISSUE_DATE, ISSUE_DAYS, BGN_TIME);
                            INSD_DATA = cBA10001DataService.genC02_INSD_DATA(a30Data, g06Data, null, null, RGN_CODE, HD_AMT, HP_AMT, HK_AMT, IS_OMTP);
                            BENE_DATA = cBA10001DataService.genC02_BENE_DATA(a30Data, g06Data, null, null);
                            TRAL_TMP = cBA10001DataService.genC02_TRAL_TMP(a30Data, g06Data);
                            
                            try {
                                // 公會資料取回(非同步)
                                Map<String, Object> z04Result = cathayLifeTypeZService.z04(ID, TRANS_NO, Z_SRC);
                                String z04ResultCode = MapUtils.getString(z04Result, Z04.resultCode.name());
                                checkResult = checkResult & "000".equals(z04ResultCode);
                            } catch(Exception z04Ex) {
                                log.error("呼叫公會資料取回(非同步)發生錯誤：" + z04Ex.getMessage());
                                checkResult = false;
                            }
                            
                            if (checkResult) {
                                Map INSD_DATA_map = new HashMap();
                                try {
                                    if (CollectionUtils.isNotEmpty(INSD_DATA)) {
                                        INSD_DATA_map = (Map) INSD_DATA.get(0);
                                    }
                                } catch (Exception e) {
                                    log.error("genC02_BENE_DATA發生錯誤，error:" + e.getMessage(), e);
                                }
                                // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上)
                                String APC_YEAR_INCOME = MapUtils.getString(INSD_DATA_map, "YEAR_INCOME_NAME");
                                
                                result = getChatWebForCBA10001_1_1_1_1_1_5_1_1(source, reqVO, RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, g06Data, CALL_INSD_DATA[CALL_INSD_DATA_SEQ], TOT_PREM, CALL_ID,
                                        TOURIST_ID, APC_YEAR_INCOME, IS_OMTP);
                                cBA10001DataService.setCacheTRANS_NO(TOURIST_ID, ID, TRANS_NO, TRANS_NO_EXPIRED_TIME);
                                
                                CONTRACT_DATA.put("RGN_NAMEs", RGN_NAMEs);
                                
                                tmpData.put("TRANS_NO", TRANS_NO);
                                tmpData.put("CLIENT_ID", CLIENT_ID);
                                tmpData.put("CONTRACT_DATA", CONTRACT_DATA);
                                tmpData.put("INSD_DATA", INSD_DATA);
                                tmpData.put("BENE_DATA", BENE_DATA);
                                tmpData.put("TRAL_TMP", TRAL_TMP);
                                tmpData.put("NATION_CODE", MapUtils.getString(g06Data, G06.NATION_CODE.name()));
                                tmpData.put("NETINSR_LV", MapUtils.getString(a30Data, A30.netinsr_lv.name()));
                                
                                cBA10001DataService.setCacheTRANS_DATA(TOURIST_ID, ID, tmpData, ExpiredTime._10mins);
                                cBA10001DataService.setCacheCALL_ID_DATA(TOURIST_ID, ID, callIdData, ExpiredTime._10mins);
                                
                                Map<String, Object> touristData = new HashMap<String, Object>();
                                if (MapUtils.isNotEmpty(ISSUE_DAY)) {
                                    if (StringUtils.isNotBlank(MIN_DATE_TIME)) {
                                        ISSUE_DAY.put("MIN_DATE_TIME", MIN_DATE_TIME);
                                    }
                                    if (StringUtils.isNotBlank(MAX_DATE_TIME)) {
                                        ISSUE_DAY.put("MAX_DATE_TIME", MAX_DATE_TIME);
                                    }
                                    touristData.put("ISSUE_DAY", ISSUE_DAY);
                                }
                                if (StringUtils.isNotBlank(RGN_CODE)) {
                                    touristData.put("RGN_CODE", RGN_CODE);
                                }
                                if (CollectionUtils.isNotEmpty(RGN_NAMEs)) {
                                    touristData.put("RGN_NAMEs", RGN_NAMEs);
                                }
                                if (CollectionUtils.isNotEmpty(RGN_PLACEs)) {
                                    touristData.put("RGN_PLACEs", RGN_PLACEs);
                                }
                                touristData.put("CALL_INSD_DATA_SEQ", CALL_INSD_DATA_SEQ);
                                cBA10001DataService.setCacheTOURIST_DATA(TOURIST_ID, touristData, ExpiredTime._10mins);
                            }
                        }
                        
                        
//                    // 取得網投旅平險訂單編號
//                    Map<String, Object> c02Result = cathayLifeTypeCService.c02WithNoException(TRANS_NO, TRANS_UUID, ZS_UUID, CLIENT_ID, contract_TRANS_NO, CONTRACT_DATA, INSD_DATA, BENE_DATA,
//                            TRAL_TMP);
//                    c02ResultCode = MapUtils.getString(c02Result, C02.resultCode.name());
//                    c02Data = MapUtils.getMap(c02Result, C02.data.name());
//                    checkResult = checkResult & "000".equals(c02ResultCode);
                        
                        // C04[resultCode]=000、G05[resultCode]=000、G06[resultCode]=000、C02[resultCode]=000
//                    if ("000".equals(c02ResultCode)) {
//                        log.info("#############CBA10001.5X1.5.1.1##############");
//                        result = getChatWebForCBA10001_1_1_1_1_1_5_1_1(source, reqVO, RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, g06Data, CALL_INSD_DATA[CALL_INSD_DATA_SEQ], TOT_PREM, CALL_ID,
//                                TOURIST_ID);
//                        // callApiService.sendDataToQueue(sessionId, TOURIST_ID + "TRANS_NO", TRANS_NO);
//                        cBA10001DataService.setCacheTRANS_NO(TOURIST_ID, TRANS_NO, TRANS_NO_EXPIRED_TIME);
//                    }
                    }
                    
                    if (checkResult) { // ***上面都完成後更新定單狀態
                        // G07_網投旅平險訂單狀態更新 (3)
                        try {
                            // 投保訂單編號狀態(3:資料填寫；5:完成投保)
                            String paymentJSON = cBA10001DataService.genG07PaymentJSON(tmpData, "3");
                            cathayLifeTypeGService.g07(paymentJSON);
                        } catch (Exception e07Exe) {
                            log.error("(" + sessionId + ") G07呼叫發生錯誤：" + e07Exe.getMessage());
                        }
                    }
                }
            }
            
            // C04、G05、G06、C02呼叫任一失敗
            if (!checkResult) {
                log.info("#############CBA10001.5X1.5.1.2##############");
                result = getChatWebForCBA10001_1_1_1_1_1_5_1_2(source, reqVO);
            }
        }

        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (CBA_CYCLE != null) {
            data.put("CBA_CYCLE", CBA_CYCLE);
        }
        
        String needAddr = MapUtils.getString(data, "needAddr");
        if (StringUtils.isBlank(needAddr)) {
            data.put("needAddr", "N");
        }
        
        // 確認方案後不再回存Flow，改以TOURIST_ID存入cache，確同意繳費牌卡後續操作取得同樣的旅遊行程資料
        /*if (MapUtils.isNotEmpty(ISSUE_DAY)) {
            if (StringUtils.isNotBlank(MIN_DATE_TIME)) {
                ISSUE_DAY.put("MIN_DATE_TIME", MIN_DATE_TIME);
            }
            if (StringUtils.isNotBlank(MAX_DATE_TIME)) {
                ISSUE_DAY.put("MAX_DATE_TIME", MAX_DATE_TIME);
            }
            data.put("ISSUE_DAY", ISSUE_DAY);
        }*/
        
        result.put("data", data);

        return result;
    }

    /**
     * CBA10001.5X1.5.1.1 繳費前確認
     * 
     * @param source
     * @param reqVO
     * @param RGN_CODE 旅遊地點
     * @param ISSUE_DAYS 旅遊天數
     * @param MIN_DATE_TIME 旅遊開始日期時間
     * @param MAX_DATE_TIME 旅遊結束日期時間
     * @param g06Data
     * @param INSD_DATA
     * @param TOT_PREM 總保費
     * @param CALL_ID
     * @param TOURIST_ID
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_1(String source, BotLifeRequestVO reqVO, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME, String MAX_DATE_TIME, Map g06Data,
            String[] INSD_DATA, int TOT_PREM, String CALL_ID, String TOURIST_ID, String APC_YEAR_INCOME, boolean IS_OMTP) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        String APC_NAME = MapUtils.getString(g06Data, G06.APC_NAME.name());
        List INSD_DATA_list = (List) MapUtils.getObject(g06Data, G06.INSD_DATA.name());
        String BENE_NAME = null;
        if (CollectionUtils.isNotEmpty(INSD_DATA_list)) {
            List BENE_DATA_list = (List) MapUtils.getObject((Map) INSD_DATA_list.get(0), G06.BENE_DATA.name());
            if (CollectionUtils.isNotEmpty(BENE_DATA_list)) {
                BENE_NAME = MapUtils.getString((Map) BENE_DATA_list.get(0), G06.BENE_NAME.name());
            }
        }
        
        // card {CBA10001.5X1.5.1.1.1}
        List<Map<String, Object>> cardList = cBA10001CardService.getCardsForCBA10001_1_1_1_1_1_5_1_1_1(vo, APC_NAME, RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, BENE_NAME, INSD_DATA, TOT_PREM, CALL_ID,
                TOURIST_ID, APC_YEAR_INCOME, IS_OMTP);
        botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);

        // text{CBA10001.5X1.5.1.1.2}
        if (TOT_PREM >= 1100 && !"1".equals(RGN_CODE)) {
            String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.1.2");
            botMessageService.setTextResult(vo, replyText2);
        }
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        // 確認是否為專機，並跳出確認視窗 // 0 = 非專機、不用確認、 1 = 專機、未確認、 2 = 專機、已確認
        String confirmNotice = MapUtils.getString(param, "confirmNotice");
        if(confirmNotice == null || StringUtils.equals(confirmNotice, "2")) {
        	// text{CBA10001.5X1.5.1.1.4}
        	String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.1.4");
        	botMessageService.setTextResult(vo, replyText3);
        }else {
        	// text{CBA10001.5X1.5.1.1.3}
        	String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.1.3");
        	botMessageService.setTextResult(vo, replyText3);
        }

        return vo.getVO();
    }

    /**
     * CBA10001.5X1.5.1.2 無法進行網路投保
     * 
     * @param source
     * @param reqVO
     * @param nodeCycle
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_2(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        // text{CBA10001.5X1.5.1.2.1}
        String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.2.1");
        botMessageService.setTextResult(vo, replyText);

        // quick reply{CBA10001.5X1.5.1.2.2}
        LifeLink[] links = { CBALink.查保單業務員(""), CBALink.不了_結束對話 };
        botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));

        return vo.getVO();
    }

    /**
     * CBA10001.5X1.5.1.3 無法取得會員前次投保訂單資訊
     * 
     * @param source
     * @param reqVO
     * @param nodeCycle
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_3(String source, BotLifeRequestVO reqVO, Integer nodeCycle) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        if (nodeCycle == null || nodeCycle.intValue() < 2) { // 迴圈第二次時不顯示
            // text{CBA10001.5X1.5.1.3.1}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.3.1");
            botMessageService.setTextResult(vo, replyText1);
        } else { // 迴圈第二次時才顯示
            // text{CBA10001.5X1.5.1.3.2}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.3.2");
            botMessageService.setTextResult(vo, replyText1);
        }
        
        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        data.put("needAddr", "Y");
        result.put("data", data);
        
        result.put("noAutoComplete", true);

        return result;
    }
    
    /**
     * CBA10001.5X1.5.1.5 投保地區專機浮出視窗確認
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_5(String source, BotLifeRequestVO reqVO) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();

        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
        
        String CALL_ID = MapUtils.getString(param, "CALL_ID");
        
        Map<String, String> dataMap = new HashMap<String, String>();
        
        
        List<Map<String, Object>> contentList = new ArrayList<Map<String, Object>>();
        //<b>海外醫療專機運送服務重要告知事項</b>
        String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.0.1.1");
        //【保障說明】....
        String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.0.1.2");
        //本人已清楚瞭解並確認
        String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.0.1.3");
        //(1)上述兩點重要告知事項。...
        String replyText4 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.0.1.4");
        ContentItem contentH = vo.new ContentItem();
        contentH.setType(12);
        contentH.setAnnoType("4");
        contentH.setWidget("doNotice");
        contentH.setAnnoTitle(replyText1);
        contentH.setAnnoText(replyText2);
        
        
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        DataListItem data = vo.new DataListItem();
        Map<String, Object> pData = new HashMap<String, Object>();
        // 準備要帶到畫面上的資料
        pData.put("cancelData", "CBA10001_1_1_1_1_1輸入旅遊問句|{\"needTouristData\" : true,\"IS_CANCEL\":true}!_!{\"flow_perfix\" : \"cbalogin\"}");// 取消按鈕
        pData.put("confirmData", "CBA10001_5X1_5_1確認方案|{\"CALL_ID\" : \"" + CALL_ID + "\", \"confirmNotice\" : \"2\"}!_!{\"flow_prefix\" : \"cbalogin\"}");// 確定按鈕
        pData.put("buttonText",replyText3);
        pData.put("underButtonText",replyText4);
        
        
        data.setPData(pData);
        dataList.add(data.getData());
        contentH.setDataList(dataList);
        
        contentList.add(contentH.getContent());
        vo.setContent(contentList);
        
        result = vo.getVO();
        
        // 準備要帶回Flow暫存的資料

        return result;
    }

    /**
     * CBA10001.5X1.5.1.4 未開通網路投保資格
     * 
     * @param source
     * @param reqVO
     * @param nodeCycle
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_4(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        // text{CBA10001.5X1.5.1.4.1}
        String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.4.1");
        botMessageService.setTextResult(vo, replyText);

        // quick reply{CBA10001.5X1.5.1.4.2}
        LifeLink[] links = { CBALink.CBA10001.前往網頁申請開通, CBALink.不了_結束對話 };
        botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));

        return vo.getVO();
    }

    @Override
    public Object queryCBA10001_1_1_1_1_1_5_2(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1_5_2：修改出發時間##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_2(source, reqVO);
        }
    }
    
    /**
     * CBA10001.1.1.1.1.1.5.2
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_2(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        // 迴圈次數
        CBA_CYCLE = MapUtils.getMap(param, "CBA_CYCLE");
        if (CBA_CYCLE == null) {
            CBA_CYCLE = new HashMap<String, Object>();
        }
        
        Integer nodeCycle = 0;
        CBA_CYCLE.put("6", nodeCycle);
        
        // text{CBA10001.5X1.5.2.1}
        String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.2.1");
        botMessageService.setTextResult(vo, replyText1);

        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (CBA_CYCLE != null) {
            data.put("CBA_CYCLE", CBA_CYCLE);
        }
        
        data.put("needTime", "Y");
        
        result.put("data", data);

        return result;
    }
    @Override
    public Object queryCBA10001_1_1_1_1_1_5_2_X(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1_5_2_X：輸入出發時間##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_2_X(source, reqVO);
        }
    }
    
    /**
     * CBA10001.1.1.1.1.1.5.2.X
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_2_X(String source, BotLifeRequestVO reqVO) throws Exception {
        
        Map<String, Object> result = new HashMap<String, Object>();
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        String CBA_TIME = MapUtils.getString(param, "CBA_TIME");
        if (StringUtils.isBlank(CBA_TIME)) {
            result = getChatWebForCBA10001_1_1_1_1_1_5_2_2(source, reqVO);
        } else {
            result = getChatWebForCBA10001_1_1_1_1_1_5_2_1(source, reqVO);
        }
        
        return result;
    }
    
    /**
     * CBA10001.1.1.1.1.1.5.2.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_2_1(String source, BotLifeRequestVO reqVO) throws Exception {
        
        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        String CBA_TIME = MapUtils.getString(param, "CBA_TIME");
        
        Map ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
        String ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", null);
        Date MIN_DATE = DateUtil.getDateByPattern(MapUtils.getString(ISSUE_DAY, "MIN_DATE", null), DateUtil.PATTERN_DASH_DATE);
        
        Map<String, String> tmpDateTime = cBA10001DataService.getDateTime(MIN_DATE, ISSUE_DAYS, CBA_TIME);
        String ISSUE_DATE = MapUtils.getString(tmpDateTime, "ISSUE_DATE");
        String BGN_TIME = MapUtils.getString(tmpDateTime, "BGN_TIME");
        String MIN_DATE_TIME = MapUtils.getString(tmpDateTime, "MIN_DATE_TIME");
        String MAX_DATE_TIME = MapUtils.getString(tmpDateTime, "MAX_DATE_TIME");
        
        ISSUE_DAY.put("ISSUE_DATE", ISSUE_DATE);
        ISSUE_DAY.put("ISSUE_DAYS", ISSUE_DAYS);
        ISSUE_DAY.put("BGN_TIME", BGN_TIME);
        ISSUE_DAY.put("MIN_DATE", DateUtil.getDateByPattern(MIN_DATE_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME));
        // ISSUE_DAY.put("MAX_DATE", DateUtil.getDateByPattern(MAX_DATE_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME));
        
        String RGN_CODE = MapUtils.getString(param, "RGN_CODE");
        
        log.info("#############CBA10001.5X1.5.2.1：出發時間取得##############");
        CBA_CYCLE = new HashMap<String, Object>();
        Map<String, Object> result = getChatWebForCBA10001_1_1_1_1_1_5(source, reqVO, sessionId, RGN_CODE, ISSUE_DAY);
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (CBA_CYCLE != null) {
            data.put("CBA_CYCLE", CBA_CYCLE);
        }
        
        if (MapUtils.isNotEmpty(ISSUE_DAY)) {
            if (StringUtils.isNotBlank(BGN_TIME)) {
                ISSUE_DAY.put("BGN_TIME", BGN_TIME);
            }
            if (MIN_DATE != null) {
                ISSUE_DAY.put("MIN_DATE", DateUtil.formatDate(MIN_DATE, DateUtil.PATTERN_DASH_DATE));
            }
            if (StringUtils.isNotBlank(MIN_DATE_TIME)) {
                ISSUE_DAY.put("MIN_DATE_TIME", MIN_DATE_TIME);
            }
            if (StringUtils.isNotBlank(MAX_DATE_TIME)) {
                ISSUE_DAY.put("MAX_DATE_TIME", MAX_DATE_TIME);
            }
            data.put("ISSUE_DAY", ISSUE_DAY);
        }
        
        result.put("data", data);
        
        data.put("needTime", "N");
        
        return result;
    }
    
    /**
     * CBA10001.1.1.1.1.1.5.2.2
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_2_2(String source, BotLifeRequestVO reqVO) throws Exception {
        
        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        String RGN_CODE = MapUtils.getString(param, "RGN_CODE", "");
        
        Map ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
        String ISSUE_DAYS = MapUtils.isNotEmpty(ISSUE_DAY) ? MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", "") : "";
        
        String ISSUE_DATE = null;
        Date MIN_DATE = null;
        if (StringUtils.isNotBlank(ISSUE_DAYS)) {
            String tmpISSUE_DATE = MapUtils.getString(ISSUE_DAY, "MIN_DATE", "");
            String tmpBGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", "");
            if (StringUtils.isBlank(tmpBGN_TIME)) {
                MIN_DATE = DateUtil.getDateByPattern(tmpISSUE_DATE, DateUtil.PATTERN_DASH_DATE);
            } else {
                MIN_DATE = DateUtil.getDateByPattern(tmpISSUE_DATE + " " + tmpBGN_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
            }
            
            Map<String, String> bgn = cBA10001DataService.getBGN_TIME(MIN_DATE);
            ISSUE_DATE = MapUtils.getString(bgn, "ISSUE_DATE");
        }
        
        // 迴圈次數
        CBA_CYCLE = MapUtils.getMap(param, "CBA_CYCLE");
        if (CBA_CYCLE == null) {
            CBA_CYCLE = new HashMap<String, Object>();
        }
        
        Integer nodeCycle = MapUtils.getInteger(CBA_CYCLE, "6", 0) + 1;
        CBA_CYCLE.put("6", nodeCycle);
        
        boolean needTime = false;
        if (nodeCycle == null || nodeCycle.intValue() < 2) { // 迴圈第二次時不顯示
            // text{CBA10001.5X1.5.2.2.1}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.2.2.1");
            botMessageService.setTextResult(vo, replyText1);
            needTime = true;
        } else { // 迴圈第二次時才顯示
            // text{CBA10001.5X1.5.2.2.2}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.2.2.2");
            botMessageService.setTextResult(vo, replyText1);
            
            // quick reply{CBA10001.5X1.5.2.2.3}
            LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保(reqVO.getIDNo(), RGN_CODE, ISSUE_DAYS, ISSUE_DATE, null, null, null) };
            botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
        }
        
        Map<String, Object> result = vo.getVO();
        
        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (CBA_CYCLE != null) {
            data.put("CBA_CYCLE", CBA_CYCLE);
        }
        
        if (needTime) {
            data.put("needTime", "Y");
        } else {
            data.put("needTime", "N");
        }
        
        result.put("data", data);
        
        return result;
    }

    @Override
    public Object queryCBA10001_1_1_1_1_1_5_1_3(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_1_1_1_1_1_5_1_3：輸入地址##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_1_3(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1.1.1.1.5.1.3
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_3(String source, BotLifeRequestVO reqVO) throws Exception {

        Map<String, Object> result = new HashMap<String, Object>();

        String ID = reqVO.getIDNo();

        Map param = JSONUtils.json2map(reqVO.getJsonParameter());

        // ZIPCODE非必填
        String ZIPCODE = null;
        String ADDRESS = MapUtils.getString(param, "ADDRESS");

        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();

        // CBA10001.1.1.1.1.1.5.1.3.1 D21[resultCode]=000、A30[resultCode]=000、C02[resultCode]=000
        // CBA10001.1.1.1.1.1.5.1.3.2 
        //                            1.D21[resultCode]≠000
        //                            2.D21[resultCode]=000、A30[resultCode]≠000
        //                            3.D21[resultCode]=000、A30[resultCode]=000、C02[resultCode]≠000
        // CBA10001.1.1.1.1.1.5.1.3.3 (地址無法識別)

        boolean checkResult = true;

        // 取得網投旅平險訂單編號
        Map<String, Object> g05Result = cathayLifeTypeGService.g05WithNoException(TRANS_NO_PARAM[0], TRANS_NO_PARAM[1]);
        String g05ResultCode = MapUtils.getString(g05Result, G05.resultCode.name());
        Map g05Data = MapUtils.getMap(g05Result, G05.data.name());

        // 訂單編號
        String TRANS_NO = MapUtils.getString(g05Data, G05.TRANS_NO.name());
        Map<String, Object> CONTRACT_DATA = null;
        List<Map<String, String>> INSD_DATA = null;
        List<Map<String, Object>> BENE_DATA = null;
        Map<String, Object> TRAL_TMP = null;
        
        Map<String, Object> tmpData = new HashMap<String, Object>();

        // 地址檢核
        Map<String, Object> d21Result = null;
        
        boolean getD21FromCache = false;
        if (StringUtils.isNotBlank(ADDRESS)) {
            d21Result = cathayLifeTypeDService.d21(ZIPCODE, ADDRESS, ID, TRANS_NO, sessionId, null);
        } else {
            d21Result = cBA10001DataService.getCacheD21_DATA(sessionId);
            if (d21Result != null) {
                getD21FromCache = true;
            }
        }
        String d21ResultCode = MapUtils.getString(d21Result, D21.resultCode.name());
        checkResult = checkResult & "000".equals(d21ResultCode);

//        Map<String, Object> c02Result = null;
        Map a30Data = null;
        Map ISSUE_DAY = null;
        String MIN_DATE_TIME = null;
        String MAX_DATE_TIME = null;

        if ("000".equals(d21ResultCode)) {
            
            Map d21Data = MapUtils.getMap(d21Result, D21.data.name());
            ZIPCODE = MapUtils.getString(d21Data, D21.ZIPCODE.name());
            ADDRESS = MapUtils.getString(d21Data, D21.ADDRESS.name());
            if (!getD21FromCache) {
                cBA10001DataService.setCacheD21_DATA(sessionId, d21Result, ExpiredTime._10mins);
            }

            Map<String, Object> a30Result = cBA10001DataService.getA30WithDefaultTWDataById(ID);
            String a30ResultCode = MapUtils.getString(a30Result, A30.resultCode.name());
            checkResult = checkResult & "000".equals(a30ResultCode);

            if ("000".equals(a30ResultCode)) {

                a30Data = MapUtils.getMap(a30Result, A30.data.name());

                checkResult = checkResult & "000".equals(g05ResultCode);

                if ("000".equals(g05ResultCode)) {

                    String RGN_CODE = MapUtils.getString(param, "RGN_CODE");
                    String CALL_ID = MapUtils.getString(param, "CALL_ID");
                    //如果是歐洲且選擇申根方案RGN_CODE帶11
                	RGN_CODE = planRGN_CODEforSchengen(RGN_CODE,CALL_ID);
					
                    List<String> RGN_NAMEs = (List<String>) MapUtils.getObject(param, "RGN_NAMEs");
                    List<String> RGN_PLACEs = (List<String>) MapUtils.getObject(param, "RGN_PLACEs");
                    ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
                    // 自訂綁定TRANS_NO使用
                    String TOURIST_ID = TPIStringUtil.getUUIDNoSeparator();

                    log.debug("(" + sessionId + ") RGN_CODE=" + RGN_CODE + ", CALL_ID=" + CALL_ID + ", TOURIST_ID=" + TOURIST_ID + ", ISSUE_DAY=" + ISSUE_DAY.entrySet());
                    
                    Map callIdData = MapUtils.getMap(param, CALL_ID);
                    int TOT_PREM = NumberUtils.toInt(MapUtils.getString(callIdData, "TOT_PREM"));
                    int CALL_INSD_DATA_SEQ = MapUtils.getIntValue(callIdData, "CALL_INSD_DATA_SEQ");
                    boolean IS_OMTP = MapUtils.getBooleanValue(callIdData, "IS_OMTP");
                    
                    log.debug("(" + sessionId + ") TOT_PREM=" + TOT_PREM + ", CALL_INSD_DATA_SEQ=" + CALL_INSD_DATA_SEQ + ", callIdData=" + callIdData.entrySet());
                    
                    String HD_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][0];
                    String HP_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][1];
                    String HK_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][2];

//                    String TRANS_UUID = ApiLogUtils.getBotTpiCallLog().getChatId();
//                    String ZS_UUID = null;
//                    String contract_TRANS_NO = TRANS_NO;

                    String ISSUE_DATE = null; // MapUtils.getString(ISSUE_DAY, "ISSUE_DATE", null);
                    String ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", null);
                    Date MIN_DATE = null;
                    String tmpBGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", "");
                    String tmpMIN_DATE = MapUtils.getString(ISSUE_DAY, "MIN_DATE", null);
                    if (StringUtils.isBlank(tmpBGN_TIME)) {
                        MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE, DateUtil.PATTERN_DASH_DATE);
                    } else {
                        MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE + " " + tmpBGN_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
                    }

                    Map<String, String> tmpDateTime = cBA10001DataService.getDateTime(MIN_DATE, ISSUE_DAYS);
                    ISSUE_DATE = MapUtils.getString(tmpDateTime, "ISSUE_DATE");
                    String BGN_TIME = MapUtils.getString(tmpDateTime, "BGN_TIME");
                    MIN_DATE_TIME = MapUtils.getString(tmpDateTime, "MIN_DATE_TIME");
                    MAX_DATE_TIME = MapUtils.getString(tmpDateTime, "MAX_DATE_TIME");
                    
                    CONTRACT_DATA = cBA10001DataService.genC02_CONTRACT_DATA(a30Data, null, ZIPCODE, ADDRESS, RGN_CODE, ISSUE_DATE, ISSUE_DAYS, BGN_TIME);
                    INSD_DATA = cBA10001DataService.genC02_INSD_DATA(a30Data, null, ZIPCODE, ADDRESS, RGN_CODE, HD_AMT, HP_AMT, HK_AMT, IS_OMTP);
                    BENE_DATA = cBA10001DataService.genC02_BENE_DATA(a30Data, null, ZIPCODE, ADDRESS);
                    TRAL_TMP = cBA10001DataService.genC02_TRAL_TMP(a30Data, null);

                    try {
                        // 公會資料取回(非同步)
                        Map<String, Object> z04Result = cathayLifeTypeZService.z04(ID, TRANS_NO, Z_SRC);
                        String z04ResultCode = MapUtils.getString(z04Result, Z04.resultCode.name());
                        checkResult = checkResult & "000".equals(z04ResultCode);
                    } catch(Exception z04Ex) {
                        log.error("呼叫公會資料取回(非同步)發生錯誤：" + z04Ex.getMessage());
                        checkResult = false;
                    }
                    
                    if (checkResult) {
                        Map INSD_DATA_map = new HashMap();
                        try {
                            if (CollectionUtils.isNotEmpty(INSD_DATA)) {
                                INSD_DATA_map = (Map) INSD_DATA.get(0);
                            }
                        } catch (Exception e) {
                            log.error("genC02_BENE_DATA發生錯誤，error:" + e.getMessage(), e);
                        }
                        // 個人年收入(1: 50萬元以下,2:50-100,3:101-200,4:200萬以上)
                        String APC_YEAR_INCOME = MapUtils.getString(INSD_DATA_map, "YEAR_INCOME_NAME");
                        
                        result = getChatWebForCBA10001_1_1_1_1_1_5_1_3_1(source, reqVO, RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, a30Data, CALL_INSD_DATA[CALL_INSD_DATA_SEQ], TOT_PREM, CALL_ID,
                                TOURIST_ID, APC_YEAR_INCOME, IS_OMTP);
                        cBA10001DataService.setCacheTRANS_NO(TOURIST_ID, ID, TRANS_NO, TRANS_NO_EXPIRED_TIME);
                        
                        CONTRACT_DATA.put("RGN_NAMEs", RGN_NAMEs);
                        
                        tmpData.put("TRANS_NO", TRANS_NO);
                        tmpData.put("CLIENT_ID", CLIENT_ID);
                        tmpData.put("CONTRACT_DATA", CONTRACT_DATA);
                        tmpData.put("INSD_DATA", INSD_DATA);
                        tmpData.put("BENE_DATA", BENE_DATA);
                        tmpData.put("TRAL_TMP", TRAL_TMP);
                        tmpData.put("NATION_CODE", MapUtils.getString(a30Data, A30.nation_code.name()));
                        tmpData.put("NETINSR_LV", MapUtils.getString(a30Data, A30.netinsr_lv.name()));
                        
						cBA10001DataService.setCacheTRANS_DATA(TOURIST_ID, ID, tmpData, ExpiredTime._10mins);
                        cBA10001DataService.setCacheCALL_ID_DATA(TOURIST_ID, ID, callIdData, ExpiredTime._10mins);
                        
                        Map<String, Object> touristData = new HashMap<String, Object>();
                        if (MapUtils.isNotEmpty(ISSUE_DAY)) {
                            if (StringUtils.isNotBlank(MIN_DATE_TIME)) {
                                ISSUE_DAY.put("MIN_DATE_TIME", MIN_DATE_TIME);
                            }
                            if (StringUtils.isNotBlank(MAX_DATE_TIME)) {
                                ISSUE_DAY.put("MAX_DATE_TIME", MAX_DATE_TIME);
                            }
                            touristData.put("ISSUE_DAY", ISSUE_DAY);
                        }
                        if (StringUtils.isNotBlank(RGN_CODE)) {
                            touristData.put("RGN_CODE", RGN_CODE);
                        }
                        if (CollectionUtils.isNotEmpty(RGN_NAMEs)) {
                            touristData.put("RGN_NAMEs", RGN_NAMEs);
                        }
                        if (CollectionUtils.isNotEmpty(RGN_PLACEs)) {
                            touristData.put("RGN_PLACEs", RGN_PLACEs);
                        }
                        touristData.put("CALL_INSD_DATA_SEQ", CALL_INSD_DATA_SEQ);
                        cBA10001DataService.setCacheTOURIST_DATA(TOURIST_ID, touristData, ExpiredTime._10mins);
                    }
                            
                    
//                    // 網投旅平險-核保試算(公會通算)
//                    c02Result = cathayLifeTypeCService.c02WithNoException(TRANS_NO, TRANS_UUID, ZS_UUID, CLIENT_ID, contract_TRANS_NO, CONTRACT_DATA, INSD_DATA, BENE_DATA, TRAL_TMP);
//                    String c02ResultCode = MapUtils.getString(c02Result, C02.resultCode.name());
//                    checkResult = checkResult & "000".equals(c02ResultCode);
//
//                    // C04[resultCode]=000、G05[resultCode]=000、G06[resultCode]=000、C02[resultCode]=000
//                    if ("000".equals(c02ResultCode)) {
//                        log.info("#############CBA10001.5X1.5.1.1##############");
//                        result = getChatWebForCBA10001_1_1_1_1_1_5_1_3_1(source, reqVO, RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, a30Data, CALL_INSD_DATA[CALL_INSD_DATA_SEQ], TOT_PREM, CALL_ID,
//                                TOURIST_ID);
//                        // callApiService.sendDataToQueue(sessionId, TOURIST_ID + "TRANS_NO", TRANS_NO);
//                        cBA10001DataService.setCacheTRANS_NO(TOURIST_ID, TRANS_NO, TRANS_NO_EXPIRED_TIME);
//                    }
                }
            }
        }

        // G05[resultCode]≠000 or C02[resultCode]≠000，視為API呼叫錯誤，阿發進修
        if (checkResult) { // ***上面都完成後更新定單狀態
            // G07_網投旅平險訂單狀態更新 (3)
            try {
                // 投保訂單編號狀態(3:資料填寫；5:完成投保)
                String paymentJSON = cBA10001DataService.genG07PaymentJSON(tmpData, "3");
                cathayLifeTypeGService.g07(paymentJSON);
            } catch (Exception e07Exe) {
                log.error("(" + sessionId + ") G07呼叫發生錯誤：" + e07Exe.getMessage());
            }
        } else { // 1.D21[resultCode]≠000, 2.D21[resultCode]=000、A30[resultCode]≠000, 3.D21[resultCode]=000、A30[resultCode]=000、C02[resultCode]≠000
            if (!"000".equals(d21ResultCode)) {
                // 迴圈次數
                CBA_CYCLE = MapUtils.getMap(param, "CBA_CYCLE");
                if (CBA_CYCLE == null) {
                    CBA_CYCLE = new HashMap<String, Object>();
                }
                
                Integer nodeCycle = MapUtils.getInteger(CBA_CYCLE, "5", 0) + 1;
                CBA_CYCLE.put("5", nodeCycle);
                
                if (nodeCycle.intValue() <= 2) {
                    log.info("#############CBA10001.5X1.5.1.3##############");
                    result = getChatWebForCBA10001_1_1_1_1_1_5_1_3(source, reqVO, nodeCycle);
                } else { // 迴圈第三次後才顯示
                    log.info("#############CBA10001.5X1.5.1.3.3##############");
                    result = getChatWebForCBA10001_1_1_1_1_1_5_1_3_3(source, reqVO);
                }
            } else {
                result = getChatWebForCBA10001_1_1_1_1_1_5_1_3_2(source, reqVO);   
            }
        }

        Map<String, Object> data = MapUtils.getMap(result, "data");
        if (MapUtils.isEmpty(data)) {
            data = new HashMap<String, Object>();
        }
        
        if (CBA_CYCLE != null) {
            data.put("CBA_CYCLE", CBA_CYCLE);
        }
        
        String needAddr = MapUtils.getString(data, "needAddr");
        if (StringUtils.isBlank(needAddr)) {
            data.put("needAddr", "N");
        }
        
        // 確認方案後不再回存Flow，改以TOURIST_ID存入cache，確同意繳費牌卡後續操作取得同樣的旅遊行程資料
        /*if (MapUtils.isNotEmpty(ISSUE_DAY)) {
            if (StringUtils.isNotBlank(MIN_DATE_TIME)) {
                ISSUE_DAY.put("MIN_DATE_TIME", MIN_DATE_TIME);
            }
            if (StringUtils.isNotBlank(MAX_DATE_TIME)) {
                ISSUE_DAY.put("MAX_DATE_TIME", MAX_DATE_TIME);
            }
            data.put("ISSUE_DAY", ISSUE_DAY);
        }*/
        
        result.put("data", data);

        return result;
    }

    /**
     * CBA10001.5X1.5.1.3.1 輸入地址後繳費前確認
     * 
     * @param source
     * @param reqVO
     * @param RGN_CODE 旅遊地點
     * @param ISSUE_DAYS 旅遊天數
     * @param MIN_DATE_TIME 旅遊開始日期時間
     * @param MAX_DATE_TIME 旅遊結束日期時間
     * @param a30Data
     * @param INSD_DATA
     * @param TOT_PREM 總保費
     * @param CALL_ID
     * @param TOURIST_ID
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_3_1(String source, BotLifeRequestVO reqVO, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME, String MAX_DATE_TIME, Map a30Data,
            String[] INSD_DATA, int TOT_PREM, String CALL_ID, String TOURIST_ID, String APC_YEAR_INCOME, boolean IS_OMTP) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        String APC_NAME = MapUtils.getString(a30Data, A30.apc_name.name());
        String BENE_NAME = "法定繼承人";

        // card {CBA10001.5X1.5.1.3.1.1}
        List<Map<String, Object>> cardList = cBA10001CardService.getCardsForCBA10001_1_1_1_1_1_5_1_3_1_1(vo, APC_NAME, RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, BENE_NAME, INSD_DATA, TOT_PREM,
                CALL_ID, TOURIST_ID, APC_YEAR_INCOME, IS_OMTP);
        botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);

        // text{CBA10001.5X1.5.1.3.1.2}
        if (TOT_PREM > 1100 && !"1".equals(RGN_CODE)) {
            String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.3.1.2");
            botMessageService.setTextResult(vo, replyText2);
        }
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        String confirmNotice = MapUtils.getString(param, "confirmNotice");
        if(confirmNotice == null || StringUtils.equals(confirmNotice, "2")) {
        	// text{CBA10001.5X1.5.1.3.1.4}
        	String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.3.1.4");
        	botMessageService.setTextResult(vo, replyText3);
        }else{
        	// text{CBA10001.5X1.5.1.3.1.3}
        	String replyText3 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.3.1.3");
        	botMessageService.setTextResult(vo, replyText3);
        }

        return vo.getVO();
    }

    /**
     * CBA10001.5X1.5.1.3.2 輸入地址後無法進行網路投保
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_3_2(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        // text{CBA10001.5X1.5.1.3.2.1}
        String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.3.2.1");
        botMessageService.setTextResult(vo, replyText);

        // quick reply{CBA10001.5X1.5.1.3.2.2}
        LifeLink[] links = { CBALink.查保單業務員(""), CBALink.不了_結束對話 };
        botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));

        return vo.getVO();
    }
    
    /**
     * CBA10001.5X1.5.1.3.3 輸入地址後(輸入2次仍無法識別)地址無法識別
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_3_3(String source, BotLifeRequestVO reqVO) throws Exception {
        
        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
        
        // text{CBA10001.5X1.5.1.3.3.1}
        String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.3.3.1");
        botMessageService.setTextResult(vo, replyText1);
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        String RGN_CODE = MapUtils.getString(param, "RGN_CODE", "");
        
        Map ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
        String ISSUE_DAYS = MapUtils.isNotEmpty(ISSUE_DAY) ? MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", "") : "";
        
        String ISSUE_DATE = null;
        Date MIN_DATE = null;
        if (StringUtils.isNotBlank(ISSUE_DAYS)) {
            String tmpISSUE_DATE = MapUtils.getString(ISSUE_DAY, "MIN_DATE", "");
            String tmpBGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", "");
            if (StringUtils.isBlank(tmpBGN_TIME)) {
                MIN_DATE = DateUtil.getDateByPattern(tmpISSUE_DATE, DateUtil.PATTERN_DASH_DATE);
            } else {
                MIN_DATE = DateUtil.getDateByPattern(tmpISSUE_DATE + " " + tmpBGN_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
            }
            
            Map<String, String> bgn = cBA10001DataService.getBGN_TIME(MIN_DATE);
            ISSUE_DATE = MapUtils.getString(bgn, "ISSUE_DATE");
        }
        
        // quick reply{CBA10001.5X1.5.1.3.3.2}
        LifeLink[] links = { CBALink.轉真人客服, CBALink.CBA10001.前往網頁投保(reqVO.getIDNo(), RGN_CODE, ISSUE_DAYS, ISSUE_DATE, null, null, null) };
        botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
        
        return vo.getVO();
    }

    @Override
    public Object queryCBA10001_1_1_1_1_1_5_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1_5_1_1_1：繳費步驟##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1.1.1.1.5.1.1.1.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {

        Map<String, Object> result = new HashMap<String, Object>();

        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        String TOURIST_ID = MapUtils.getString(param, "TOURIST_ID");
        String TRANS_NO = cBA10001DataService.getCacheTRANS_NO(TOURIST_ID, reqVO.getIDNo());
        
        if (StringUtils.isBlank(TRANS_NO)) {
            return getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1_1(source, reqVO);
        } else {
            
            BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
            
            String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
            
            String CALL_ID = MapUtils.getString(param, "CALL_ID");
            
            Map<String, Object> d22Result = cathayLifeTypeDService.d22(reqVO.getIDNo(), CLIENT_ID, TRANS_NO, sessionId);
            
            Map<String, String> dataMap = new HashMap<String, String>();
            dataMap.put("TOURIST_ID", TOURIST_ID);
            
            try {
                String d22ResultCode = MapUtils.getString(d22Result, D22.resultCode.name());
                if ("000".equals(d22ResultCode)) {
                    Map d22Data = MapUtils.getMap(d22Result, D22.data.name());
                    List<Map> d22CARD_LIST = (List<Map>) MapUtils.getObject(d22Data, D22.CARD_LIST.name());
                    if (CollectionUtils.isNotEmpty(d22CARD_LIST)) {
                        for (Map CARD : d22CARD_LIST) {
                            String TYPE = MapUtils.getString(CARD, D22.TYPE.name());
                            if ("M".equals(TYPE)) {
                                String CARD_NO = MapUtils.getString(CARD, D22.CARD_NO.name());
                                String CARD_VALD_YY = MapUtils.getString(CARD, D22.CARD_VALD_YY.name());
                                String CARD_VALD_MM = MapUtils.getString(CARD, D22.CARD_VALD_MM.name());
                                String BANK_NAME = MapUtils.getString(CARD, D22.BANK_NAME.name());
                                if (StringUtils.isNotBlank(CARD_NO) && CARD_NO.length() == 16 && StringUtils.isNotBlank(CARD_VALD_YY) && StringUtils.isNotBlank(CARD_VALD_MM)) {
                                    dataMap.put("card1", StringUtils.substring(CARD_NO, 0, 4));
                                    dataMap.put("card2", StringUtils.substring(CARD_NO, 4, 8));
                                    dataMap.put("card3", StringUtils.substring(CARD_NO, 8, 12));
                                    dataMap.put("yy", CARD_VALD_YY);
                                    dataMap.put("mm", CARD_VALD_MM);
                                    dataMap.put("bankName", BANK_NAME);
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception d22Ex) {
                log.error("(" + sessionId + ") 取得常用帳戶信用卡資料發生錯誤：" + d22Ex.getMessage());
            }
            
            List<Map<String, Object>> contentList = new ArrayList<Map<String, Object>>();
            
            ContentItem contentH = vo.new ContentItem();
            contentH.setType(12);
            contentH.setAction(5);
            contentH.setWidget("doCCPayment");
            
            List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
            DataListItem data = vo.new DataListItem();
            Map<String, Object> pData = new HashMap<String, Object>();
            // 準備要帶到畫面上的資料
            ObjectMapper ow = new ObjectMapper();
            String cardItem = MapUtils.isNotEmpty(dataMap) ? ow.writeValueAsString(dataMap) : null;
            pData.put("data", StringUtils.isNotBlank(cardItem) ? new String((new Base64()).encode(URLEncoder.encode(cardItem, "UTF-8").getBytes()), "UTF-8") : null);
            pData.put("text", "繳費完成");
            pData.put("reply", "CBA10001_5X1_5_5X1繳費完成|{\"CALL_ID\":\"" + CALL_ID + "\",\"TOURIST_ID\":\"" + TOURIST_ID + "\"}!_!{\"flow_prefix\":\"cbalogin\"}");
            
            data.setPData(pData);
            dataList.add(data.getData());
            contentH.setDataList(dataList);
            
            contentList.add(contentH.getContent());
            vo.setContent(contentList);
            
            result = vo.getVO();
            
            // 準備要帶回Flow暫存的資料
        }

        return result;
    }
    
    @Override
    public Object queryCBA10001_1_1_1_1_1_5_1_1_2(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1_5_1_1_2：修改投保資訊##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_1_1_2(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1.1.1.1.5.1.1.2
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_1_2(String source, BotLifeRequestVO reqVO) throws Exception {

        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
        
        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        // 確認方案後，改以TOURIST_ID取出cache，確同意繳費牌卡後續操作取得同樣的旅遊行程資料
        // String RGN_CODE = MapUtils.getString(param, "RGN_CODE");
        // Map ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
        
        String TOURIST_ID = MapUtils.getString(param, "TOURIST_ID");
        Map<String, Object> touristData = cBA10001DataService.getCacheTOURIST_DATA(TOURIST_ID);
        String RGN_CODE = MapUtils.getString(touristData, "RGN_CODE");
        Map ISSUE_DAY = MapUtils.getMap(touristData, "ISSUE_DAY");
        
        // String CALL_ID = MapUtils.getString(param, "CALL_ID");
        
        String ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", null);
        Date MIN_DATE = null;
        String tmpBGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", "");
        String tmpMIN_DATE = MapUtils.getString(ISSUE_DAY, "MIN_DATE", null);
        if (StringUtils.isBlank(tmpBGN_TIME)) {
            MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE, DateUtil.PATTERN_DASH_DATE);
        } else {
            MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE + " " + tmpBGN_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
        }

        Map<String, String> tmpDateTime = cBA10001DataService.getDateTime(MIN_DATE, ISSUE_DAYS);
        String ISSUE_DATE = MapUtils.getString(tmpDateTime, "ISSUE_DATE");
        
        // Map callIdData = MapUtils.getMap(param, CALL_ID);
        // int CALL_INSD_DATA_SEQ = MapUtils.getIntValue(callIdData, "CALL_INSD_DATA_SEQ");
        int CALL_INSD_DATA_SEQ = MapUtils.getIntValue(touristData, "CALL_INSD_DATA_SEQ");

        log.debug("(" + sessionId + ") CALL_INSD_DATA_SEQ=" + CALL_INSD_DATA_SEQ);
        
        String MAIN_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][0];
        String HP_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][1];
        String HK_AMT = CALL_INSD_DATA[CALL_INSD_DATA_SEQ][2];
        
        String url = CBALink.CBA10001.getCalculatorUrl(reqVO.getIDNo(), RGN_CODE, ISSUE_DAYS, ISSUE_DATE, MAIN_AMT, HP_AMT, HK_AMT);
        
        Map<String, String> replaceParam = new HashMap<String, String>();
        replaceParam.put("CAL_URL", url);
        
        // text{CBA10001.5X1.5.1.1.2.1}
        String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.1.1.2.1");
        botMessageService.setTextResultWithReplace(vo, replyText1, replaceParam);

        return vo.getVO();
    }
    
    @Override
    public Object queryCBA10001_1_1_1_1_1_5_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1_5_4X1：投保操作時間過久 or 被登出##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1_1(source, reqVO);
        }
    }
    
    /**
     * CBA10001.1.1.1.1.1.5.1.1.1.1.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {
        
        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        
        String ivUser = MapUtils.getString(param, "ivUser");
        String RGN_CODE = MapUtils.getString(param, "RGN_CODE");
        Map ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
        
        String ISSUE_DAYS = MapUtils.getString(ISSUE_DAY, "ISSUE_DAYS", null);
        Date MIN_DATE = null;
        String tmpBGN_TIME = MapUtils.getString(ISSUE_DAY, "BGN_TIME", "");
        String tmpMIN_DATE = MapUtils.getString(ISSUE_DAY, "MIN_DATE", null);
        if (StringUtils.isBlank(tmpBGN_TIME)) {
            MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE, DateUtil.PATTERN_DASH_DATE);
        } else {
            MIN_DATE = DateUtil.getDateByPattern(tmpMIN_DATE + " " + tmpBGN_TIME + ":00", DateUtil.PATTERN_DASH_DATE_TIME);
        }
        Date MAX_DATE = DateUtil.getDateByPattern(MapUtils.getString(ISSUE_DAY, "MAX_DATE", null), DateUtil.PATTERN_DASH_DATE);
        
        Map<String, String> tmpDateTime = cBA10001DataService.getDateTime(MIN_DATE, ISSUE_DAYS);
        String ISSUE_DATE = MapUtils.getString(tmpDateTime, "ISSUE_DATE");
        String BGN_TIME = MapUtils.getString(tmpDateTime, "BGN_TIME");
        
        ISSUE_DAY.put("ISSUE_DATE", ISSUE_DATE);
        ISSUE_DAY.put("BGN_TIME", BGN_TIME);
        ISSUE_DAY.put("MIN_DATE", MIN_DATE);
        ISSUE_DAY.put("MAX_DATE", MAX_DATE);
        
        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
        
        boolean hasIvUser = false;
        if (StringUtils.isNotBlank(ivUser) && !"Unauthenticated".equals(ivUser)) {
            hasIvUser = true;
        }

        if (hasIvUser) { // 投保操作時間過久
            // text{CBA10001.5X1.5.4X1.1}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.4X1.1");
            botMessageService.setTextResult(vo, replyText1);
        } else { // 被登出
            // text{CBA10001.5X1.5.4X1.2}
            String replyText1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.4X1.2");
            botMessageService.setTextResult(vo, replyText1);
        }
        
        // CBA10001.5X1.5：地點及日期皆取得
        log.info("#############CBA10001.5X1.5：地點及日期皆取得##############");
        CBA_CYCLE = new HashMap<String, Object>();
        Map<String, Object> result = getChatWebForCBA10001_5X1_5(vo, reqVO, sessionId, RGN_CODE, ISSUE_DAY);
        
        return result;
    }

    @Override
    public Object queryCBA10001_1_1_1_1_1_5_1_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {
        log.info("#############CBA10001_5X1_5_5X1：繳費完成##############");
        if (reqVO.isFaceBook()) {
            return null;
        } else {
            return getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1_1_1(source, reqVO);
        }
    }

    /**
     * CBA10001.1.1.1.1.1.5.1.1.1.1.1.1
     * 
     * @param source
     * @param reqVO
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Map<String, Object> getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1_1_1(String source, BotLifeRequestVO reqVO) throws Exception {

        BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());

        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
        
        Map param = JSONUtils.json2map(reqVO.getJsonParameter());
        String TOURIST_ID = MapUtils.getString(param, "TOURIST_ID", "");
        
        // 確認方案後，改以TOURIST_ID取出cache，確同意繳費牌卡後續操作取得同樣的旅遊行程資料
        // Map ISSUE_DAY = MapUtils.getMap(param, "ISSUE_DAY");
        // List<String> RGN_NAMEs = (List<String>) MapUtils.getObject(param, "RGN_NAMEs");
        
        Map<String, Object> touristData = cBA10001DataService.getCacheTOURIST_DATA(TOURIST_ID);
        Map ISSUE_DAY = MapUtils.getMap(touristData, "ISSUE_DAY");
        String RGN_CODE = MapUtils.getString(touristData, "RGN_CODE");
        List<String> RGN_NAMEs = (List<String>) MapUtils.getObject(touristData, "RGN_NAMEs");
        List<String> RGN_PLACEs = (List<String>) MapUtils.getObject(touristData, "RGN_PLACEs");
        param.put("ISSUE_DAY", ISSUE_DAY);
        param.put("RGN_CODE", RGN_CODE);
        param.put("RGN_NAMEs", RGN_NAMEs);
        param.put("RGN_PLACEs", RGN_PLACEs);
        
        String TRANS_NO = cBA10001DataService.getCacheCompletedTRANS_NO(TOURIST_ID, reqVO.getIDNo());
        Map<String, Object> c02Data = cBA10001DataService.getCacheC02_DATA(TOURIST_ID, reqVO.getIDNo());
        Map<String, Object> transData = cBA10001DataService.getCacheTRANS_DATA(TOURIST_ID, reqVO.getIDNo());
        Map<String, Object> d20Data = cBA10001DataService.getCacheD20_DATA(TOURIST_ID, reqVO.getIDNo());
        log.debug("(" + sessionId + ") d20Data=" + (d20Data != null ? d20Data.entrySet() : "null"));
        
        String MAX_DATE_TIME = MapUtils.getString(ISSUE_DAY, "MAX_DATE_TIME", "");
        String AUTH_CODE = MapUtils.getString(d20Data, D20.CARD_AUTH_CODE.name());
        String IS_NCCC = MapUtils.getString(d20Data, D20.IS_NCCC.name());
        String ACNT_DATE = MapUtils.getString(d20Data, D20.ACNT_DATE.name());
        String CARD_NO = MapUtils.getString(d20Data, "CARD_NO", "");
        String CARD12 = StringUtils.substring(CARD_NO, 0, 8);
        String CARD4 = StringUtils.substring(CARD_NO, 12, 16);
        String CARD_END_YM = MapUtils.getString(d20Data, "CARD_END_YM", "");
        String TOT_PREM = MapUtils.getString(d20Data, "TOT_PREM", "");

        Map<String, Object> contract = new HashMap<String, Object>();
        contract.put("CLIENT_ID", CLIENT_ID);
        contract.put("TRANS_NO", TRANS_NO); // 此訂單在專站之交易序號
        contract.put("AUTH_CODE", AUTH_CODE); // 授權碼,AC 保費扣款api回覆之AUTH_CODE
        contract.put("PAY_WAY_CODE", "L"); // 繳費方式, AC 保費扣款api回覆之PAY_WAY_CODE  L=信用卡, M=帳戶扣款 N=虛擬帳號
        contract.put("IS_NCCC", IS_NCCC); // NCC 由AC 保費扣款api回覆得知,是否為扣款行別NCCC,國泰,花旗 值要問二科
        contract.put("IS_EXTENDED", "N"); // 延保件 是否為延保件,Y=延保件,N=新件,阿發只做新投保
        contract.put("ACNT_DATE", ACNT_DATE); // CB意外回壓檔案需使用，AT新契約不用

        Map<String, Object> c03Result = cathayLifeTypeCService.c03(TRANS_NO, sessionId, contract);
        String c03ResultCode = MapUtils.getString(c03Result, C03.resultCode.name());

        if ("000".equals(c03ResultCode)) { // C03 [resultCode]=000
            
            Map c03Data = MapUtils.getMap(c03Result, C03.data.name());
            Map c03CONTRACT_DATA = MapUtils.getMap(c03Data, C03.CONTRACT_DATA.name());
            String POLICY_NO = MapUtils.getString(c03CONTRACT_DATA, C03.POLICY_NO.name());
            
            // C03的保單號碼要帶給G07
            Map<String, Object> a30Result = cBA10001DataService.getA30WithDefaultTWDataById(reqVO.getIDNo());
            String a30ResultCode = MapUtils.getString(a30Result, A30.resultCode.name());
            
            Map a30Data = null;
            if ("000".equals(a30ResultCode)) {
                a30Data = MapUtils.getMap(a30Result, A30.data.name());
                c02Data.put("NATION_CODE", MapUtils.getString(a30Data, A30.nation_code.name()));
                c02Data.put("NETINSR_LV", MapUtils.getString(a30Data, A30.netinsr_lv.name()));
            }
            
            boolean ACT_1_FLAG = false; // 保費超過1,100 即享蝦皮購物金 100元(1.活動時間2019/12/31止 2.保費達1,100元)
            Date ACT_1_END_DATE = DateUtil.getDateByPattern("2020-04-30 23:59:59", DateUtil.PATTERN_DASH_DATE_TIME);
            if (ACT_1_END_DATE.after(DateUtil.getNow()) && NumberUtils.toInt(TOT_PREM) >= 800) {
                ACT_1_FLAG = true;
            }
            
            // G07 paymentJSON
            try {
                Map c02CONTRACT_DATA = MapUtils.getMap(c02Data, C02.CONTRACT_DATA.name());
                c02CONTRACT_DATA.put("POLICY_NO", POLICY_NO);
                c02CONTRACT_DATA.put("SETUP_DATE", DateUtil.formatDate(DateUtil.getToday(), DateUtil.PATTERN_DASH_DATE));
                c02CONTRACT_DATA.put("END_DATE", MAX_DATE_TIME.split(" ")[0]);
                c02CONTRACT_DATA.put("CARD_NO", CARD12);
                c02CONTRACT_DATA.put("CARD_END_YM", CARD_END_YM);
                c02CONTRACT_DATA.put("RGN_NAMEs", RGN_NAMEs);
                if (ACT_1_FLAG) {
                    c02CONTRACT_DATA.put("PARTNER_NO", "9");
                }
                c02Data.put(C02.CONTRACT_DATA.name(), c02CONTRACT_DATA);
                
                List transINSD_DATA_list = (List) MapUtils.getObject(transData, C02.INSD_DATA.name());
                Map transINSD_DATA = CollectionUtils.isNotEmpty(transINSD_DATA_list) ? (Map) transINSD_DATA_list.get(0) : null;
                
                List c02INSD_DATA_list = (List) MapUtils.getObject(c02Data, C02.INSD_DATA.name());
                Map c02INSD_DATA = CollectionUtils.isNotEmpty(c02INSD_DATA_list) ? (Map) c02INSD_DATA_list.get(0) : null;
                c02INSD_DATA.put("YEAR_INCOME", MapUtils.getString(transINSD_DATA, G06.YEAR_INCOME.name()));
                c02INSD_DATA_list.add(0, c02INSD_DATA);
                c02Data.put(C02.INSD_DATA.name(), c02INSD_DATA_list);
                
                List transBENE_DATA_list = (List) MapUtils.getObject(transData, C02.BENE_DATA.name());
                c02Data.put(C02.BENE_DATA.name(), transBENE_DATA_list);
                log.debug("[getChatWebForCBA10001_1_1_1_1_1_5_1_1_1_1_1_1] c02Data=" + c02Data.entrySet());
                
                // 投保訂單編號狀態(3:資料填寫；5:完成投保)
                String paymentJSON = cBA10001DataService.genG07PaymentJSON(c02Data, "5");
                cathayLifeTypeGService.g07(paymentJSON);
            } catch (Exception g07Exe) {
                log.error("(" + sessionId + ") G07呼叫發生錯誤：" + g07Exe.getMessage(), g07Exe);
            }

            // G08 finishInfoJSON
            try {
                String finishInfoJSON = cBA10001DataService.genG08FinishInfoJSON(a30Data, d20Data, c03Data, CLIENT_ID, TRANS_NO);
                cathayLifeTypeGService.g08(finishInfoJSON);
            } catch (Exception g08Exe) {
                log.error("(" + sessionId + ") G08呼叫發生錯誤：" + g08Exe.getMessage(), g08Exe);
            }

            // text{CBA10001.5X1.5.6X1.1.0}
            String replyText0 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.6X1.1.0");
            botMessageService.setTextResult(vo, replyText0);
            
            // card {CBA10001.5X1.5.6X1.1.1}
            List<Map<String, Object>> cardList = cBA10001CardService.getCardsForCBA10001_5X1_5_6X1_1_1(vo, CARD4, TOT_PREM, a30Data, POLICY_NO);
            botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);

            // sticker 4.讚{CBA10001.5X1.5.6X1.1.2}
            botMessageService.setImageResult(vo, "sticker_praise.gif");

            // 活動wording，有值時顯示
            // text{CBA10001.5X1.5.6X1.ACT.1}
            String act_1 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.6X1.ACT.1");
            if (ACT_1_FLAG && StringUtils.isNotBlank(act_1)) {
                botMessageService.setTextResult(vo, act_1);
            }
            
            // text{CBA10001.5X1.5.6X1.1.3}
            String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.6X1.1.3");
            botMessageService.setTextResult(vo, replyText);

            // quick reply{CBA10001.5X1.5.6X1.1.4}
            String shareText = cBA10001DataService.genLineShareText(param, a30Data, c03Data);
            LifeLink[] links = { CBALink.CBA10001.LINE分享給親友(shareText), CBALink.不了_結束對話 };
            botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
        } else { // C03 [resultCode]≠000

            boolean h06Result = cathayLifeTypeHService.h06();
            if (h06Result) {
                // 週一至週五09:00-20:00顯示此節點
                // 【給客戶wording後，幫客戶直接轉接客服(S050)】
                // text{CBA10001.5X1.5.6X1.1.5}
                String replyText5 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.6X1.1.5");
                botMessageService.setTextResult(vo, replyText5);
                
                ContentItem contentH = vo.new ContentItem();
                contentH.setType(12);
                contentH.setAction(5);
                contentH.setWidget("doTransToAgent");
                
                vo.getContent().add(contentH.getContent());
            } else {
                // 非週一至週五09:00-20:00顯示此節點
                // text{CBA10001.5X1.5.6X1.1.6}
                String replyText6 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "CBA10001.5X1.5.6X1.1.6");
                botMessageService.setTextResult(vo, replyText6);

                // quick reply{CBA10001.5X1.5.6X1.1.7}
                LifeLink[] links = { CBALink.聯絡客服中心, CBALink.不了_結束對話 };
                botMessageService.setLifeLinkListResult(vo, Arrays.asList(links));
            }

        }

        return vo.getVO();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map<String, String> doCombinePAY(BotLifeRequestVO reqVO, String TOURIST_ID, String CARD_NO, String CARD_END_YM, String TRANS_NO) throws Exception {

        Map<String, String> result = new HashMap<String, String>();
        
        log.info("#############D20扣款##############");
        
        String PAY_WAY_CODE = "L";
        String USER_ID = reqVO.getIDNo();
        String AUTH_NAME = null;
        String POLICY_NO = null;
        String TRN_AMT = null;
        String PROD_ID = "0GS";
        String APLY_NO = null;
        String NEXT_PAY_AUTH = "N";
        String SAME_FIRST_PAY = "N";
        String SYS_NO = CLIENT_ID;
        String TRANS_UUID = ApiLogUtils.getBotTpiCallLog().getChatId();
        
        Map<String, Object> a30Result = cBA10001DataService.getA30WithDefaultTWDataById(reqVO.getIDNo());
        String a30ResultCode = MapUtils.getString(a30Result, A30.resultCode.name());
        if ("000".equals(a30ResultCode)) {
            Map a30Data = MapUtils.getMap(a30Result, A30.data.name());
            AUTH_NAME = MapUtils.getString(a30Data, A30.apc_name.name());
        }
        
        Map<String, Object>  c02Data = cBA10001DataService.getCacheC02_DATA(TOURIST_ID, reqVO.getIDNo());
        Map c02CONTRACT_DATA = MapUtils.getMap(c02Data, C02.CONTRACT_DATA.name());
        TRN_AMT = MapUtils.getString(c02CONTRACT_DATA, C02.TOT_PREM.name());
        APLY_NO = MapUtils.getString(c02CONTRACT_DATA, C02.APLY_NO.name());
        POLICY_NO = APLY_NO;

        Map<String, String> reqMap = new HashMap<String, String>();
        reqMap.put("PAY_WAY_CODE", PAY_WAY_CODE);
        reqMap.put("USER_ID", USER_ID);
        reqMap.put("AUTH_NAME", AUTH_NAME);
        reqMap.put("POLICY_NO", POLICY_NO);
        reqMap.put("TRN_AMT", TRN_AMT);
        reqMap.put("CARD_NO", CARD_NO);
        reqMap.put("CARD_END_YM", CARD_END_YM);
        reqMap.put("PROD_ID", PROD_ID);
        reqMap.put("APLY_NO", APLY_NO);
        reqMap.put("NEXT_PAY_AUTH", NEXT_PAY_AUTH);
        reqMap.put("SAME_FIRST_PAY", SAME_FIRST_PAY);
        reqMap.put("SYS_NO", SYS_NO);
        reqMap.put("TRANS_NO", TRANS_NO);
        reqMap.put("TRANS_UUID", TRANS_UUID);
        
        Map<String, Object> d20Result = cathayLifeTypeDService.d20(reqMap);
        
        String d20ResultCode = MapUtils.getString(d20Result, D20.resultCode.name());
        if (!"000".equals(d20ResultCode)) {
            String d20ResultMsg = MapUtils.getString(d20Result, D20.resultMsg.name());
            log.error("(" + TRANS_UUID + ") 呼叫D20扣款發生錯誤： (" + d20ResultCode + ")" + d20ResultMsg);
            result.put("error", "D20");
        } else {
            cBA10001DataService.removeCacheTRANS_NO(TOURIST_ID, reqVO.getIDNo());
            cBA10001DataService.setCacheCompletedTRANS_NO(TOURIST_ID, reqVO.getIDNo(), TRANS_NO, TRANS_NO_EXPIRED_TIME);
            Map<String, Object> d20Data = MapUtils.getMap(d20Result, D20.data.name());
            d20Data.put("CARD_NO", CARD_NO);
            d20Data.put("CARD_END_YM", CARD_END_YM);
            d20Data.put("TOT_PREM", TRN_AMT);
            cBA10001DataService.setCacheD20_DATA(TOURIST_ID, reqVO.getIDNo(), d20Data, ExpiredTime._5mins);
        }
        
        return result;
    }
    
    /*  
     * @param RGN_CODE
     * @param CALL_ID
     * @return
     * 判斷方案是否為申根以及是否為歐洲國家
     */
    public String planRGN_CODEforSchengen(String RGN_CODE,String CALL_ID) {
    	if(StringUtils.equals(RGN_CODE, "7") && StringUtils.equals(CALL_ID, "CBA10001_5X1_5_5")) {
    		RGN_CODE = "11";
    	}
    	return RGN_CODE;
    }
}
