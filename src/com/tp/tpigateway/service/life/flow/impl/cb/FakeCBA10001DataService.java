package com.tp.tpigateway.service.life.flow.impl.cb;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.repository.BotTouristPlaceRepository;
import com.tp.tpigateway.service.common.CallApiService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001DataService;
import com.tp.tpigateway.util.LocalCache;
import com.tp.tpigateway.util.LocalCache.ExpiredTime;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * 易call保，開發 "旅遊地點代碼" 需要呼叫 CBA10001DataService getRGN_CODE(String CBA_ORIG_STR, List<Object> CBA_PLACEs)
 * 因為原專案的實做有連結資料庫，而開發時無法連接國泰的測試環境
 * 所以使用 FakeCBA10001DataService 做為測試
 *
 * ps. 把 CBA10001DataServiceImpl 的 @Service 註解，讓 CBV10001ServiceImpl 能夠 autowired 這個 service
 *
 * @see CBV10001ServiceImpl
 */
@Service
public class FakeCBA10001DataService implements CBA10001DataService {

    @Autowired
    private BotTouristPlaceRepository botTouristPlaceRepository;
	
    @Autowired
    private CallApiService callApiService;
    
	private static Logger log = LoggerFactory.getLogger(CBA10001DataServiceImpl.class);

	@Override
	public Map<String, String> genC01_CONTRACT_DATA(String ISSUE_DATE, String ISSUE_DAYS, String BGN_TIME,
			String RGN_CODE) {
		return null;
	}

	@Override
	public List<Map<String, String>> genC01_INSD_DATA(String RGN_CODE, String HD_AMT, String HP_AMT, String HK_AMT,
			boolean IS_OMTP) {
		return null;
	}

	@Override
	public String getTOT_PREM(Map<String, Object> c01Result) {
		return null;
	}

	@Override
	public boolean checkOver65(Map<String, Object> a30Result) throws Exception {
		return false;
	}

	@Override
	public Map<String, Object> getRGN_CODE(String CBA_ORIG_STR, List<Object> CBA_PLACEs) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();

        List<String> RGN_PLACEs = new ArrayList<String>();
        Map<String, String> RGN_CODEs = new HashMap<String, String>();
        Map<String, String> RGN_NAMEs = new HashMap<String, String>();

        // 1. CBA_PLACEs，由NLU回傳結果組成
        if (CollectionUtils.isNotEmpty(CBA_PLACEs)) {
            for (Object obj : CBA_PLACEs) {
                if (obj != null && obj instanceof Map) {
                    Map CBA_PLACE = (Map) obj;
                    String entity = MapUtils.getString(CBA_PLACE, "entity");
                    String text = MapUtils.getString(CBA_PLACE, "text");
                    String info = MapUtils.getString(CBA_PLACE, "info");
                    if (StringUtils.isNotBlank(entity) && StringUtils.isNotBlank(text) && StringUtils.isNotBlank(info)) {
                        String tmp = null;
                        /*if (StringUtils.endsWith(entity, "台灣行政區")) {
                            tmp = "1";
                        } else */
                        if (StringUtils.endsWith(entity, "世界國家城市")) {
                            tmp = getCBA_CODE(info);
                        }
                        if (StringUtils.isNotBlank(tmp)) {
                            String RGN_NAME = getRGN_NAME(tmp);
                            if (StringUtils.isNotBlank(tmp) && !RGN_PLACEs.contains(text)) {
                                RGN_PLACEs.add(text);
                            }
                            RGN_CODEs.put(text, tmp);
                            RGN_NAMEs.put(text, RGN_NAME);
                        } else {
                            log.error("判斷旅遊地區發生錯誤：entity=" + entity + ", text=" + text + ", info=" + info);
                        }
                    } else {
                        log.error("取得entity發生錯誤：entity=" + entity + ", text=" + text + ", info=" + info);
                    }
                }
            }
        }

        // 2. 由tourist_place table維護比對
        Map<String, Object> placeMap = getAllBotTouristPlace();

        Pattern p = null;
        Matcher m = null;

        if (MapUtils.isNotEmpty(placeMap)) {
            String placePattern = getPlacePattern();
            if (StringUtils.isNotBlank(placePattern)) {
                p = Pattern.compile(placePattern);
                m = p.matcher(StringUtils.isNotBlank(CBA_ORIG_STR) ? CBA_ORIG_STR.toLowerCase() : "");
                while (m.find()) {
                    String RGN_PLACE = m.group();
                    if (validEngPlaceName(CBA_ORIG_STR, RGN_PLACE)) {
                        String tmp = placeMap.get(RGN_PLACE).toString();
                        String RGN_NAME = getRGN_NAME(tmp);
                        if (StringUtils.isNotBlank(tmp) && !RGN_PLACEs.contains(RGN_PLACE)) {
                            RGN_PLACEs.add(RGN_PLACE);
                        }
                        RGN_CODEs.put(RGN_PLACE, tmp);
                        RGN_NAMEs.put(RGN_PLACE, RGN_NAME);
                    }
                }
            } else {
                log.error("取得旅遊地點Pattern發生錯誤，PlacePattern為空！");
            }
        } else {
            log.error("取得Table旅遊地點發生錯誤，TouristPlace為空！");
        }

        log.debug("NLU、Table比對合併地點：" + RGN_PLACEs);
        log.debug("NLU、Table比對合併國家：" + RGN_NAMEs.entrySet());

        // 3. 重整RGN_PLACEs中互相涵蓋重疊部分，取字串最長
        String RGN_CODE = null;
        List<String> newRGN_PLACEs = new ArrayList<String>();
        List<String> newRGN_NAMEs = new ArrayList<String>();

        if (CollectionUtils.isNotEmpty(RGN_PLACEs)) {

            List<String> tmpPlaces = new ArrayList<String>(RGN_PLACEs);
            Collections.sort(tmpPlaces, new Comparator<String>() {
                @Override
                public int compare(String str1, String str2) {
                    return str1.length() < str2.length() ? 1 : (str1.length() == str2.length() ? 0 : -1);
                }
            });

            StringBuilder tmpPlace = new StringBuilder();
            for (String place : tmpPlaces) {
                if (tmpPlace.length() > 0) {
                    tmpPlace.append("|");
                }
                tmpPlace.append(place.trim());
            }

            p = Pattern.compile(tmpPlace.toString());
            m = p.matcher(CBA_ORIG_STR);
            while (m.find()) {
                String RGN_PLACE = m.group();
                String tmp = RGN_CODEs.get(RGN_PLACE);
                String RGN_NAME = RGN_NAMEs.get(RGN_PLACE);
                if (StringUtils.isBlank(RGN_CODE)) {
                    RGN_CODE = tmp;
                } else if (!StringUtils.equals(RGN_CODE, tmp)) {
                    RGN_CODE = "4";
                }
                if (StringUtils.isNotBlank(tmp) && !newRGN_PLACEs.contains(RGN_PLACE)) {
                    newRGN_PLACEs.add(RGN_PLACE);
                }
                if (!newRGN_NAMEs.contains(RGN_NAME)) {
                    newRGN_NAMEs.add(RGN_NAME);
                }
            }
        } else {
            log.error("無法辨視任何旅遊地點！");
        }

        result.put("RGN_CODE", RGN_CODE);
        result.put("RGN_PLACEs", newRGN_PLACEs);
        result.put("RGN_NAMEs", newRGN_NAMEs);

        return result;
	}

	@Override
	public String getRGN_NAME(String RGN_CODE) {
		if ("1".equals(RGN_CODE)) {
            return "國內";
        } else if ("2".equals(RGN_CODE)) {
            return "中國.港澳";
        } else if ("5".equals(RGN_CODE)) {
            return "日本";
        } else if ("6".equals(RGN_CODE)) {
            return "美加";
        } else if ("7".equals(RGN_CODE)) {
            return "歐洲";
        } else if ("11".equals(RGN_CODE)) {
        	return "歐洲-申根";
        } else if ("9".equals(RGN_CODE)) {
            return "紐澳";
        } else if ("14".equals(RGN_CODE)) {
            return "韓國";
        } else if ("15".equals(RGN_CODE)) {
            return "越南";
        } else if ("16".equals(RGN_CODE)) {
        	return "新加坡";
        } else if ("17".equals(RGN_CODE)) {
        	return "菲律賓";
        } else if ("18".equals(RGN_CODE)) {
        	return "印尼";
        } else if ("19".equals(RGN_CODE)) {
        	return "馬來西亞";
        } else if ("20".equals(RGN_CODE)) {
        	return "緬甸";
        } else if ("21".equals(RGN_CODE)) {
        	return "泰國";
        } else if ("22".equals(RGN_CODE)) {
        	return "寮國";
        } else if ("23".equals(RGN_CODE)) {
        	return "柬埔寨";
        } else { // 4: 「其他」顯示暫時調整為「國外」
            // return "其他";
            return "國外";
        }
	}

	@Override
	public Map<String, Object> getISSUE_DAY(String sessionId, List<Object> CBA_DATEs) {
		return null;
	}

	@Override
	public Map<String, String> getBGN_TIME(Date MIN_DATE) {
		return null;
	}

	@Override
	public Map<String, String> getDateTime(Date MIN_DATE, String ISSUE_DAYS) {
		return null;
	}

	@Override
	public Map<String, String> getDateTime(Date MIN_DATE, String ISSUE_DAYS, String MIN_TIME) {
		return null;
	}

	@Override
	public boolean checkISSUE_Period(String RGN_CODE, String ISSUE_DATE, String ISSUE_DAYS) {
		return false;
	}

	@Override
	public Map<String, Object> genC02_CONTRACT_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS,
			String RGN_CODE, String ISSUE_DATE, String ISSUE_DAYS, String BGN_TIME) {
		return null;
	}

	@Override
	public List<Map<String, String>> genC02_INSD_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS,
			String RGN_CODE, String HD_AMT, String HP_AMT, String HK_AMT, boolean IS_OMTP) {
		return null;
	}

	@Override
	public List<Map<String, Object>> genC02_BENE_DATA(Map a30Data, Map g06Data, String ZIPCODE, String ADDRESS) {
		return null;
	}

	@Override
	public Map<String, Object> genC02_TRAL_TMP(Map a30Data, Map g06Data) {
		return null;
	}

	@Override
	public boolean checkToday(Date date) {
		return false;
	}

	@Override
	public Map<String, Object> getC04DataById(String ID) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> getA04WithDefaultTWDataById(String ID) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> getG06DataById(String ID) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> getA30WithDefaultTWDataById(String ID) throws Exception {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllBotTouristPlace() throws Exception {
		Map<String, Object> result = new HashMap<>();
		
		String places = 
				"台北	1\r\n" + 
				"高雄	1\r\n" + 
				"小人國	1\r\n" + 
				"合歡山	1\r\n" + 
				"阿里山	1\r\n" + 
				"埔里	1\r\n" + 
				"南投	1\r\n" + 
				"國內	1\r\n" + 
				"內灣	1\r\n" + 
				"十三行博物館	1\r\n" + 
				"小烏來天空步道	1\r\n" + 
				"新屋鄉綠色走廊	1\r\n" + 
				"海洋科技博物館	1\r\n" + 
				"十分老街	1\r\n" + 
				"日月潭	1\r\n" + 
				"石門	1\r\n" + 
				"大溪老街	1\r\n" + 
				"淡水	1\r\n" + 
				"八里	1\r\n" + 
				"陽明山	1\r\n" + 
				"梨山	1\r\n" + 
				"武陵農場	1\r\n" + 
				"蘭嶼	1\r\n" + 
				"綠島	1\r\n" + 
				"澎湖	1\r\n" + 
				"旗津	1\r\n" + 
				"清水斷崖	1\r\n" + 
				"太魯閣	1\r\n" + 
				"琵琶湖	1\r\n" + 
				"知本溫泉	1\r\n" + 
				"礁溪	1\r\n" + 
				"羅東	1\r\n" + 
				"頭城	1\r\n" + 
				"墾丁	1\r\n" + 
				"墾丁國家公園	1\r\n" + 
				"後壁湖	1\r\n" + 
				"赤崁樓	1\r\n" + 
				"七股鹽山	1\r\n" + 
				"曾文水庫	1\r\n" + 
				"奧萬大	1\r\n" + 
				"妖怪村	1\r\n" + 
				"司馬庫斯	1\r\n" + 
				"勝興車站	1\r\n" + 
				"龜山島	1\r\n" + 
				"明池	1\r\n" + 
				"東石漁港	1\r\n" + 
				"谷關	1\r\n" + 
				"Taipei	1\r\n" + 
				"全台環島	1\r\n" + 
				"峇里島	18\r\n" + 
				"普吉島	21\r\n" + 
				"菲律賓	17\r\n" + 
				"吳哥窟	23\r\n" + 
				"宿霧	17\r\n" + 
				"蘇梅島	21\r\n" + 
				"愛妮島	17\r\n" + 
				"巴拉望	17\r\n" + 
				"長灘島	17\r\n" + 
				"沙巴	19\r\n" + 
				"馬新	19\r\n" + 
				"下龍灣	15\r\n" + 
				"帛琉	4\r\n" + 
				"馬丘比丘	4\r\n" + 
				"非洲	4\r\n" + 
				"玻利維亞	4\r\n" + 
				"環遊世界	4\r\n" + 
				"關帛	4\r\n" + 
				"南極	4\r\n" + 
				"復活節島	4\r\n" + 
				"全球	4\r\n" + 
				"日本	5\r\n" + 
				"九州	5\r\n" + 
				"北九州	5\r\n" + 
				"琉球	5\r\n" + 
				"四國	5\r\n" + 
				"北海道	5\r\n" + 
				"箱根	5\r\n" + 
				"輕井澤	5\r\n" + 
				"Hokkaido	5\r\n" + 
				"關東	5\r\n" + 
				"博多	5\r\n" + 
				"Tokyo	5\r\n" + 
				"Nagoya	5\r\n" + 
				"京阪	5\r\n" + 
				"京阪神	5\r\n" + 
				"白川鄉	5\r\n" + 
				"合掌村	5\r\n" + 
				"關西	5\r\n" + 
				"美國	6\r\n" + 
				"加拿大	6\r\n" + 
				"美加	6\r\n" + 
				"歐洲	7\r\n" + 
				"北歐	7\r\n" + 
				"奧捷	7\r\n" + 
				"捷奧	7\r\n" + 
				"德瑞	7\r\n" + 
				"德比荷	7\r\n" + 
				"德瑞法	7\r\n" + 
				"德瑞義	7\r\n" + 
				"英法	7\r\n" + 
				"英比荷	7\r\n" + 
				"法義	7\r\n" + 
				"法比荷	7\r\n" + 
				"法瑞義	7\r\n" + 
				"義瑞法	7\r\n" + 
				"荷比盧	7\r\n" + 
				"荷比法	7\r\n" + 
				"荷比英	7\r\n" + 
				"荷比	7\r\n" + 
				"南法	7\r\n" + 
				"東歐	7\r\n" + 
				"紐西蘭	9\r\n" + 
				"澳洲	9\r\n" + 
				"紐澳	9\r\n" + 
				"韓國	14\r\n" + 
				"濟州島	14\r\n" + 
				"大邱	14\r\n" + 
				"港澳	2\r\n" + 
				"香港	2\r\n" + 
				"澳門	2"
				;
		
		List<String> placesList = Arrays.asList(places.split("\\r\\n"));
		placesList.stream().forEach(place -> {
			String[] item = place.split("\\t");
			result.put(item[0].trim(), item[1].trim());
		});
        return result;
	}

	@Override
	public String getPlacePattern() throws Exception {
		String cacheKey = "PlacePattern";

        String tmp = (String) LocalCache.get(cacheKey);
        if (StringUtils.isNotBlank(tmp)) {
            log.debug("PlacePattern=" + tmp);
            return tmp;
        }

        StringBuilder tmpPlace = new StringBuilder();

        Map<String, Object> placeMap = getAllBotTouristPlace();
        if (MapUtils.isNotEmpty(placeMap)) {
            
            List<String> keys = new ArrayList<>(placeMap.keySet());
            Collections.sort(keys, new Comparator<String>() {
                public int compare(String str1, String str2) {
                    return str1.length() < str2.length() ? 1 : (str1.length() == str2.length() ? 0 : -1);
                }
            });
            
            for (String place : keys) {
                if (tmpPlace.length() > 0) {
                    tmpPlace.append("|");
                }
                tmpPlace.append(place.trim());
            }
        }

        tmp = tmpPlace.toString();
        
        if (StringUtils.isNotBlank(tmp)) {
            LocalCache.register(cacheKey, tmp, ExpiredTime._1hr);
            callApiService.localCache(cacheKey, tmp, ExpiredTime._1hr);
            log.debug("PlacePattern=" + tmp);
        } else {
            log.error("無法取得BotTouristPlace.");
        }

        return tmp;
	}

	@Override
	public String genG07PaymentJSON(Map tmpData, String STATUS) {
		return null;
	}

	@Override
	public String genG08FinishInfoJSON(Map a30Data, Map d20Data, Map c03Data, String SYS_NO, String TRANS_NO) {
		return null;
	}

	@Override
	public String genLineShareText(Map param, Map a30Data, Map c03Data) throws UnsupportedEncodingException {
		return null;
	}

	@Override
	public String getCacheTRANS_NO(String cacheKey, String ID) throws Exception {
		return null;
	}

	@Override
	public void setCacheTRANS_NO(String cacheKey, String ID, String TRANS_NO, int expiredTime) throws Exception {

	}

	@Override
	public void removeCacheTRANS_NO(String cacheKey, String ID) {

	}

	@Override
	public String getCacheCompletedTRANS_NO(String cacheKey, String ID) {
		return null;
	}

	@Override
	public void setCacheCompletedTRANS_NO(String cacheKey, String ID, String TRANS_NO, int expiredTime) {

	}

	@Override
	public Map<String, Object> getCacheTRANS_DATA(String cacheKey, String ID) {
		return null;
	}

	@Override
	public void setCacheTRANS_DATA(String cacheKey, String ID, Map<String, Object> tmpData, int expiredTime) {

	}

	@Override
	public Map<String, Object> getCacheCALL_ID_DATA(String cacheKey, String ID) {
		return null;
	}

	@Override
	public void setCacheCALL_ID_DATA(String cacheKey, String ID, Map<String, Object> callIdData, int expiredTime) {

	}

	@Override
	public Map<String, Object> getCacheC02_DATA(String cacheKey, String ID) {
		return null;
	}

	@Override
	public void setCacheC02_DATA(String cacheKey, String ID, Map<String, Object> c02Data, int expiredTime) {

	}

	@Override
	public Map<String, Object> getCacheD20_DATA(String cacheKey, String ID) {
		return null;
	}

	@Override
	public void setCacheD20_DATA(String cacheKey, String ID, Map<String, Object> d20Data, int expiredTime) {

	}

	@Override
	public Map<String, Object> getCacheD21_DATA(String cacheKey) {
		return null;
	}

	@Override
	public void setCacheD21_DATA(String cacheKey, Map<String, Object> d21Data, int expiredTime) {

	}

	@Override
	public String getCacheZ01_KEY(String cacheKey, String ID) {
		return null;
	}

	@Override
	public void setCacheZ01_KEY(String cacheKey, String ID, String Z01_KEY, int expiredTime) {

	}

	@Override
	public Map<String, Object> getCacheTOURIST_DATA(String cacheKey) {
		return null;
	}

	@Override
	public void setCacheTOURIST_DATA(String cacheKey, Map<String, Object> touristData, int expiredTime) {

	}

	@Override
	public boolean getOMTP(String RGN_CODE) throws Exception {
		return false;
	}

	private String getCBA_CODE(String infos) {
        if (StringUtils.isBlank(infos)) {
            return "4";
        }

        String info = infos.split(";")[0];

        if (StringUtils.contains(info, "歐洲非申根-土耳其") || StringUtils.contains(info, "歐洲非申根-以色列") || StringUtils.contains(info, "歐洲非申根-白俄羅斯")
                || StringUtils.contains(info, "歐洲非申根-約旦") || StringUtils.contains(info, "歐洲非申根-烏克蘭") || StringUtils.contains(info, "歐洲非申根-喬治亞")
                || StringUtils.contains(info, "歐洲非申根-摩爾多瓦") || StringUtils.contains(info, "歐洲非申根-黎巴嫩")) {
            return "4";
        }

        if(StringUtils.contains(info, "國內")) {
        	return "1";
        }else if(StringUtils.contains(info, "中國大陸") || StringUtils.contains(info, "香港") || StringUtils.contains(info, "澳門")) {
        	return "2";
        }else if(StringUtils.contains(info, "日本")) {
        	return "5";
        } else if (StringUtils.contains(info, "美國") || StringUtils.contains(info, "加拿大")) {
        	return "6";
        }else if(StringUtils.contains(info, "歐洲")) {
        	return "7";// 預設7(歐洲非申根)
        } else if (StringUtils.contains(info, "紐西蘭") || StringUtils.contains(info, "澳洲")) {
        	return "9";
        }else if(StringUtils.contains(info, "韓國")) {
        	return "14";
        }else if(StringUtils.contains(info, "越南")) {
        	return "15";
        }else if(StringUtils.contains(info, "新加坡")) {
        	return "16";
        }else if(StringUtils.contains(info, "菲律賓")) {
        	return "17";
        }else if(StringUtils.contains(info, "印尼")) {
        	return "18";
        }else if(StringUtils.contains(info, "馬來西亞")) {
        	return "19";
        }else if(StringUtils.contains(info, "緬甸")) {
        	return "20";
        }else if(StringUtils.contains(info, "泰國")) {
        	return "21";
        }else if(StringUtils.contains(info, "寮國")) {
        	return "22";
        }else if(StringUtils.contains(info, "柬埔寨")) {
        	return "23";
        }else {
        	return "4";
        }
    }

	/**
	 * 檢核英文地點有效性(獨立的英文單字)<br>
	 * 1.輸入參數為空，無效<br>
	 * 2.PLACE不是純英文，有效(不需檢核)<br>
	 * 3.獨立的英文單字，有效<br>
	 * 4.其他，無效
	 *
	 * @param STR
	 * @param PLACE
	 * @return true:有效, false:無效
	 */
	private boolean validEngPlaceName(String STR, String PLACE) {
	    if (StringUtils.isBlank(STR) || StringUtils.isBlank(PLACE))
	        return false;

	    if (!PLACE.matches("^[A-Za-z]+$"))
	        return true;

	    boolean result = false;

	    try {
	        Pattern p = Pattern.compile("(^|[^a-z])" + PLACE + "($|[^a-z])");
	        Matcher m = p.matcher(StringUtils.isNotBlank(STR) ? STR.toLowerCase() : "");
	        while (m.find()) {
	            String RGN_PLACE = m.group();
	            if (StringUtils.isNotBlank(RGN_PLACE)) {
	                result = true;
	            }
	        }
	    } catch(Exception e) {

	    }

	    return result;
	}

}
