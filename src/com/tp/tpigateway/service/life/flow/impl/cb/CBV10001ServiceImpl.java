package com.tp.tpigateway.service.life.flow.impl.cb;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.tp.tpigateway.exception.CBV10001DataException;
import com.tp.tpigateway.model.life.CBV10001VO.AllVipData;
import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.CathayBaseResponse;
import com.tp.tpigateway.model.life.CBV10001VO.CathayResponseDetail;
import com.tp.tpigateway.model.life.CBV10001VO.CheckAmtVoiceData;
import com.tp.tpigateway.model.life.CBV10001VO.ConfirmVoiceData;
import com.tp.tpigateway.model.life.CBV10001VO.ContentAllVipData;
import com.tp.tpigateway.model.life.CBV10001VO.ContentCheckAmtVoice;
import com.tp.tpigateway.model.life.CBV10001VO.ContentConfirmVoice;
import com.tp.tpigateway.model.life.CBV10001VO.ContentGetRgnCode;
import com.tp.tpigateway.model.life.CBV10001VO.ContentLastIssueData;
import com.tp.tpigateway.model.life.CBV10001VO.ContentLawAge;
import com.tp.tpigateway.model.life.CBV10001VO.ContentTempVoice;
import com.tp.tpigateway.model.life.CBV10001VO.FlowCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.FlowRequest;
import com.tp.tpigateway.model.life.CBV10001VO.FlowRequest.Places;
import com.tp.tpigateway.model.life.CBV10001VO.GatewayResponse;
import com.tp.tpigateway.model.life.CBV10001VO.GatewayResponseData;
import com.tp.tpigateway.model.life.CBV10001VO.IdMappingEasycall;
import com.tp.tpigateway.model.life.CBV10001VO.LastIssueData;
import com.tp.tpigateway.model.life.CBV10001VO.LawAgeData;
import com.tp.tpigateway.model.life.CBV10001VO.TempVoiceData;
import com.tp.tpigateway.service.life.flow.cb.CBA10001DataService;
import com.tp.tpigateway.service.life.flow.cb.CBV10001ApiService;
import com.tp.tpigateway.service.life.flow.cb.CBV10001DataService;
import com.tp.tpigateway.service.life.flow.cb.CBV10001Service;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.PropUtils;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

@Service("cBV10001Service")
public class CBV10001ServiceImpl implements CBV10001Service {
    private static final Logger LOGGER = LoggerFactory.getLogger(CBV10001ServiceImpl.class);

    private static final String CATHAY_SYSTEM_ERROR = "099";
    private static final String API_RESULT_SUCCESS = "000";

    private String ECALL_API_URL_PREFIX;
    private String ECALL_API_XIBM_CLIENT_ID;

    private RestTemplate restTemplate;

    @Autowired
    private CBA10001DataService cba10001DataService;
    
    @Autowired
    private CBV10001DataService cbv10001DataService; 
    
    @Autowired
    private CBV10001ApiService cbv10001ApiService;
    
    /**
     * 由 tpigateway-${profile}.properties 讀取 e-call API 的 URL 和 application ID
     *
     * 初始化 RestTemplate，使用 OkHttp3
     * (因為要交付原始碼給國壽，在設定不方便更改的情況，故不在 Spring configuration 設定 bean)
     */
    public CBV10001ServiceImpl() {
    	ECALL_API_URL_PREFIX = PropUtils.getProperty("cathaylife.CBV10001.website.url.prefix");
        ECALL_API_XIBM_CLIENT_ID = PropUtils.getProperty("cathaylife.CBV10001.api.x-ibm-client-id");
        if (StringUtils.isEmpty(ECALL_API_URL_PREFIX) || StringUtils.isEmpty(ECALL_API_XIBM_CLIENT_ID)) {
            throw new RuntimeException("Initial Cathay API properties fail!");
        }

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        ConnectionPool okHttpConnectionPool = new ConnectionPool(5, 30, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);
        builder.connectionPool(okHttpConnectionPool);
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.retryOnConnectionFailure(false);
        restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new OkHttp3ClientHttpRequestFactory(builder.build()));
    }


    /**
     * == 取得會員法定年齡 (http://${host}:${port}/${context}/ecall/law-age) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "issueDate" : "2020-02-20",
     *       "vipBrdy" : ["1970-01-01", "1980-08-08"]
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/lawAge
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {"ISSUE_DATE":"2020-02-20", "VIP_BRDY":"1970-01-01"}
     *
     *   response ->
     *   {
     *       "returnCode": "0",
     *       "detail": {
     *           "resultMsg": "",
     *           "shortMsg": "查詢成功",
     *           "resultCode": "000",
     *           "data": {
     *               "AGE":"50",
     *               "VIP_BRDY":"1970-01-01",
     *               "ISSUE_DATE":"2020-02-20"
     *           },
     *           "returnMessage": {"m_desc":[""], "m_msgid":"", "m_sysid":"", "m_type":"", "m_url":"", "m_idx":0, "m_code":0}
     *       },
     *       "returnMsg": "API success",
     *       "UUID": "CB_API1331_20200221_DEC228F4C2_T01",
     *       "hostname": "cxlcsxat01"
     *   }
     *
     *   ps. 任一筆查詢不成功，直接回傳結果至 Flow
     *
     *
     * 3. response to Chat Flow
     *
     *   {
     *       "msg": "success",
     *       "code": 0,
     *       "data": {
     *           "from": "bot",
     *           "showtime": "2020-03-05 11:40:20",
     *           "content": {
     *               "RTN_CODE": "000",
     *               "AGE": [12, 50],
     *               "ISSUE_DATE": "2020-02-20",
     *               "VIP_BRDY": ["1970-01-01", "1980-08-08"]
     *           },
     *           "channel":"cathaylife",
     *           "source":"cathaylife",
     *           "role":"cathaylife",
     *           "fb":false,
     *           "fbIntent":null,
     *           "sbIntent":null
     *       }
     *   }
     *
     * @author Tony Liu
     * @author Wayne Tsai
     */
    @Override
    public GatewayResponse<ContentLawAge> lawAge(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D) {
        /* 2. */
        for (String vipBrdy : req.getVipBrdy()) {
            if (StringUtils.isBlank(vipBrdy)) {
                return GatewayResponse.buildFail(ContentLawAge.class, "parameter: vipBrdy is required!");
            }
        }
        HttpHeaders apiHeaders = buildApiHttpHeader();
        String url = ECALL_API_URL_PREFIX + "/lawAge";

        List<String> vipBrdys = req.getVipBrdy();
        List<String> ages = new ArrayList<>(vipBrdys.size());
        String resultCode = null;
        String shortMsg = null;
        for (String vipBrdy : vipBrdys) {
            Map<String, Object> apiParams = new HashMap<>();
            apiParams.put("ISSUE_DATE", req.getIssueDate());
            apiParams.put("VIP_BRDY", vipBrdy);
            HttpEntity<Map<String, Object>> request = new HttpEntity<>(apiParams, apiHeaders);

            CathayBaseResponse<LawAgeData> apiResponse = null;
            try {
                ResponseEntity<CathayBaseResponse<LawAgeData>> responseEntity = 
                		cbv10001ApiService.lawAge(botTpiCallLog3D, url, restTemplate, request);
                apiResponse = responseEntity.getBody();
            } catch (ResourceAccessException | CBV10001DataException ex) {
            	throw ex;
            } catch (Exception ex) {
                LOGGER.error("Invoke cathay e-call API lawAge fail", ex);
                GatewayResponse<ContentLawAge> failResult = getFailResult(
                        ContentLawAge.class, CATHAY_SYSTEM_ERROR);
                ContentLawAge content = failResult.getData().getContent();
                content.setIssueDate(req.getIssueDate());
                content.setVipBirthday(vipBrdys);
                return failResult;
            }
            CathayResponseDetail<LawAgeData> detail = apiResponse.getDetail();
            resultCode = detail.getResultCode();
            shortMsg = detail.getShortMsg();

            if (!API_RESULT_SUCCESS.equals(detail.getResultCode())) {
                GatewayResponse<ContentLawAge> failResult = 
                		getFailResult(ContentLawAge.class, apiResponse, resultCode);
            
                ContentLawAge content = failResult.getData().getContent();
                content.setIssueDate(req.getIssueDate());
                content.setVipBirthday(vipBrdys);
                return failResult;
            }
            ages.add(detail.getData().getAge());
        }

        /* 3. */
        ContentLawAge resContent = new ContentLawAge(resultCode, ages, req.getIssueDate(), vipBrdys);
        GatewayResponseData<ContentLawAge> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        GatewayResponse<ContentLawAge> response = GatewayResponse.buildSuccess(ContentLawAge.class, shortMsg);
        response.setData(resData);
        return response;
    }

    /**
     * == 取得上一次投保紀錄  (http://${host}:${port}/${context}/ecall/last-issue-data)==
     *
     *1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A29091346D"
     *   }
     *
     *2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/last_issue_data
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {" VIP_ID ":"A123456789"}
     *
     *   response ->
     *   {
     *      "returnCode":"0",
     *      "detail":{
     *          "resultMsg":"",
     *          "shortMsg":"查詢成功",
     *          "resultCode":"000",
     *          "data":{
     *              "BIRTHDAY":"0741222",
     *              "LOCAL_MEDICAL":"0",
     *              "AGE":"34",
     *              "IS_EMPLOYEE":"Y",
     *              "AGENT_QUALIFICATION":"Y",
     *              "OVERSEA_MEDICAL":"10",
     *              "OVERSEA_ILLNESS_TYPE":"3",
     *              "IS_MEMBER":"Y",
     *              "CREDITCARD_VALIDDATE":"202010",
     *              "LOCAL_MAIN":"100",
     *              "MEMBER_TYPE":"MAIN",
     *              "OVERSEA_ILLNESS":"20",
     *              "OVERSEA_MAIN":"100"
     *          },
     *              "returnMessage":{
     *                  "m_desc":[
     *                  ""
     *                  ],
     *                  "m_msgid":"",
     *                  "m_sysid":"",
     *                  "m_type":"",
     *                  "m_url":"",
     *                  "m_idx":0,
     *                  "m_code":0
     *              }
     *      },
     *      "returnMsg":"API success",
     *      "UUID":"CB_API1328_20200221_866D181AD1_T01",
     *      "hostname":"cxlcsxat01"
     *   }
     *3. response to Chat Flow
     *  3.1 成功範例
     *      {
     *          "msg": "success",
     *          "code": 0,
     *          "data": {
     *              "from": "bot",
     *              "showtime": "2020-03-05 11:40:20",
     *              "content": {
     *                  "RTN_CODE": "000",
     *                  "LOCAL_MAIN": "100",
     *                  "LOCAL_MEDICAL" : "0",
     *                  "OVERSEA_MAIN" : "100",
     *                  "OVERSEA_MEDICAL": "10",
     *                  "OVERSEA_ILLNESS": "20",
     *                  "OVERSEA_ILLNESS_TYPE": "3"
     *              },
     *              "channel": "cathaylife",
     *              "source": "cathaylife",
     *              "role": "cathaylife",
     *              "fb": false,
     *              "fbIntent": null,
     *              "sbIntent": null
     *
     *          }
     *      }
     *
     *   3.2 失敗範例 (note: 一筆錯誤就視為系統錯誤)
     *      {
     *          "msg": "error",
     *          "code": -1,
     *          "data": {
     *              "from": "bot",
     *              "showtime": "2020-03-05 11:40:20",
     *              "content": {
     *                  "RTN_CODE": "099",
     *                  "LOCAL_MAIN": "",
     *                  "LOCAL_MEDICAL" : "",
     *                  "OVERSEA_MAIN" : "",
     *                  "OVERSEA_MEDICAL": "",
     *                  "OVERSEA_ILLNESS": "",
     *                  "OVERSEA_ILLNESS_TYPE": ""
     *              },
     *              "channel": "cathaylife",
     *              "source": "cathaylife",
     *              "role": "cathaylife",
     *              "fb": false,
     *              "fbIntent": null,
     *              "sbIntent": null
     *
     *          }
     *      }
     */
    @Override
    public GatewayResponse<ContentLastIssueData> lastIssueData(FlowRequest req,BotTpiCallLog3D botTpiCallLog3D) {
        List<String> errorFields = new ArrayList<>();
        if (StringUtils.isEmpty(req.getVipId())) {
            errorFields.add("vipId");
        }
        if (!errorFields.isEmpty()) {
            return GatewayResponse.buildFail(ContentLastIssueData.class, "parameter: vipId is required!");
        }

        HttpHeaders headers = buildApiHttpHeader();
        String url = ECALL_API_URL_PREFIX + "/last_issue_data";
        
        Map<String, Object> params = new HashMap<>();
        params.put("VIP_ID", req.getVipId());
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(params, headers);

        CathayBaseResponse<LastIssueData> response = null;
        try {
            ResponseEntity<CathayBaseResponse<LastIssueData>> responseEntity = 
            		cbv10001ApiService.lastIssueData(botTpiCallLog3D, url, restTemplate, request);
            response = responseEntity.getBody();
            LOGGER.info("Response from lastIssueData:\n" + JSONUtils.obj2json(response));
        } catch (ResourceAccessException | CBV10001DataException ex) {
        	throw ex;
        } catch (Exception ex) {
            LOGGER.error("Calling Ecall api lastIssueData occurs error:", ex);
            return getFailResult(ContentLastIssueData.class, CATHAY_SYSTEM_ERROR);
        }
        CathayResponseDetail<LastIssueData> detail = response.getDetail();
        String resultCode = detail.getResultCode();
        if (!API_RESULT_SUCCESS.equals(detail.getResultCode())) {
            return getFailResult(ContentLastIssueData.class, response, resultCode);
        }

        LastIssueData lastIssueData = detail.getData();
        ContentLastIssueData contentLastIssueData =
                new ContentLastIssueData(
                		resultCode,
                        lastIssueData.getLocalMain(),
                        lastIssueData.getLocalMedical(),
                        lastIssueData.getOverseaMain(),
                        lastIssueData.getOverseaMedical(),
                        lastIssueData.getOverseaIllness(),
                        lastIssueData.getOverseaIllnessType());

        GatewayResponse<ContentLastIssueData> res =
                GatewayResponse.buildSuccess(ContentLastIssueData.class, response.getReturnMsg());
        GatewayResponseData<ContentLastIssueData> resData = GatewayResponse.buildDefaultData();
        resData.setContent(contentLastIssueData);
        res.setData(resData);
        return res;
    }

    /**
     * == 整戶資料查詢  (http://${host}:${port}/${context}/ecall/all-vip-data)==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A29091346D"
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/all_vip_data
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {" VIP_ID ":"A123456789"}
     *
     *   2.1 回傳代碼為097, 098時, data欄位為字串
     *   response ->
     *   {
     *      "returnCode":"0",
     *      "detail":{
     *          "resultMsg":"傳入參數:會員 ID 格式有誤",
     *          "shortMsg":"未通過輸入檢核",
     *          "resultCode":"097",
     *          "data":"A12345",
     *          "returnMessage":{
     *               "m_desc":[
     *              ""
     *              ],
     *              "m_msgid":"",
     *              "m_sysid":"",
     *              "m_type":"",
     *              "m_url":"",
     *              "m_idx":0,
     *              "m_code":-3
     *          }
     *      },
     *      "returnMsg":"API success",
     *      "UUID":"CB_API0467_20200130_74C2D87F54_S02",
     *      "hostname":"cxl1csas01"
     *   }
     *   2.2 查詢成功
     *   response -> 參考國泰api文件
     *
     *   2.3
     *   CathayBaseResponse<?> 泛型沒有指定物件，因此查詢成功時，需要在此處再轉換為AllVipData物件
     *
     * 3. response to Chat Flow
     *  3.1 成功範例
     *  {
     *    "msg": "success",
     *    "code": 0,
     *    "data": {
     *         "from": "bot",
     *         "showtime": "2020-03-05 11:40:20",
     *         "content": {
     *             "RTN_CODE": "000",
     *             "checkAGNT_ID": "00",
     *             "PROD_ID": "Y",
     *             "TRVL_PROD_ID": "M",
     *             "onlyVIP": "N",
     *             "newCASE_NO": "Y",
     *             "newCASE_NO_INS": "Y",
     *             "notWelcome": "00",
     *             "a190_bo": {
     *                 "vip_id": "H29376308F",
     *                 "vip_name": "謝曉玲",
     *                 "vip_brdy": "1955-01-01",
     *                 "mobile": "0922123456",
     *                 "email": "XXXXX@cathlife.com.tw",
     *                 "card_no": "4284306866737233",
     *                 "card_name_ym": "202206",
     *                 "case_no_ins": "E0810236241"
     *             },
     *             "a191_boList": [
     *                 {
     *                     "vip_id": "H29376308F",
     *                     "rela_id": "A13042355K",
     *                     "rela_name": "傅承熙",
     *                     "rela_brdy": "1957-01-01",
     *                     "case_no_ins": "E0810236241"
     *                 },
     *                 {
     *                     "vip_id": "H29376308F",
     *                     "rela_id": "A71912213K",
     *                     "rela_name": "傅小明",
     *                     "rela_brdy": "1990-01-01",
     *                     "case_no_ins": "E0810236241"
     *                  }
     *               ]
     *          },
     *      "channel": "cathaylife",
     *      "source": "cathaylife",
     *      "role": "cathaylife",
     *      "fb": false,
     *      "fbIntent": null,
     *      "sbIntent": null
     *   }
     * }
     *   3.2 失敗範例
     *  {
     *     "msg": "error",
     *     "code": -1,
     *     "data": {
     *          "from": "bot",
     *          "showtime": "2020-03-05 11:40:20",
     *          "content": {
     *              "RTN_CODE": "098",
     *              "checkAGNT_ID": "",
     *              "PROD_ID": "",
     *              "TRVL_PROD_ID": "",
     *              "onlyVIP": "",
     *              "newCASE_NO": "",
     *              "newCASE_NO_INS": "",
     *              "notWelcome": "",
     *              "a190_bo": {
     *                  "vip_id": "",
     *                  "vip_name": "",
     *                  "vip_brdy": "",
     *                  "mobile": "",
     *                  "email": "",
     *                  "card_no": "",
     *                  "card_name_ym": "",
     *                  "case_no_ins": ""
     *               },
     *               "a191_boList": []
     *           },
     *           "channel": "cathaylife",
     *           "source": "cathaylife",
     *           "role": "cathaylife",
     *           "fb": false,
     *           "fbIntent": null,
     *           "sbIntent": null
     *      }
     *  }
     *  
     *5. 整戶資料成功後，將sessionId存進資料表 IdMappingEasyCall，aplyNo設為空字串。
     */
    @Override
    public GatewayResponse<ContentAllVipData> allVipData(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D) {
    	//不檢查參數是否正確，交由國泰api檢查，以便回傳國泰錯誤代碼，讓flow使用
//        List<String> errorFields = new ArrayList<>();
//        if (StringUtils.isEmpty(req.getVipId())) {
//            errorFields.add("vipId");
//        }
//        if (!errorFields.isEmpty()) {
//            return GatewayResponse.buildFail(ContentAllVipData.class, "parameter: " + errorFields + " is required!");
//        }

        HttpHeaders headers = buildApiHttpHeader();
        String url = ECALL_API_URL_PREFIX + "/all_vip_data";
        
        Map<String, Object> params = new HashMap<>();
        params.put("VIP_ID", req.getVipId());
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(params, headers);

        String resultCode = null;
        AllVipData allVipData = null;
        CathayBaseResponse<?> response = null;
        try {
            ResponseEntity<CathayBaseResponse<?>> responseEntity = 
            		cbv10001ApiService.allVipData(botTpiCallLog3D, url, restTemplate, request);
            response = responseEntity.getBody();
            LOGGER.info("Response from allVipData:\n" + JSONUtils.obj2json(response));

            CathayResponseDetail<?> detail = response.getDetail();
            resultCode = detail.getResultCode();
            if (!API_RESULT_SUCCESS.equals(detail.getResultCode())) {
                return getFailResult(ContentAllVipData.class, response, resultCode);
            }
            /* 2.3 */
            allVipData = JSONUtils.obj2pojo(detail.getData(), AllVipData.class);
        } catch (ResourceAccessException | CBV10001DataException ex) {
        	throw ex;
        } catch (Exception ex) {
            LOGGER.error("Calling Ecall api lastIssueData occurs error:\n");
            return getFailResult(ContentAllVipData.class, CATHAY_SYSTEM_ERROR);
        }
        
        /* 5. */
        saveOrUpdateIdMappingEasyCall(req);

        ContentAllVipData contentAllVipData = new ContentAllVipData();
        contentAllVipData.setReturnCode(resultCode);
        contentAllVipData.setCheckAgntId(allVipData.getCheckAgntId());
        contentAllVipData.setProdId(allVipData.getProdId());
        contentAllVipData.setTrvlProdId(allVipData.getTrvlProdId());
        contentAllVipData.setOnlyVip(allVipData.getOnlyVip());
        contentAllVipData.setNewCaseNo(allVipData.getNewCaseNo());
        contentAllVipData.setNewCaseNoIns(allVipData.getNewCaseNoIns());
        contentAllVipData.setNotWelcome(allVipData.getNotWelcome());
        contentAllVipData.setMainMember(allVipData.getMainMember());
        contentAllVipData.setSubMemberList(allVipData.getSubMemberList());

        GatewayResponse<ContentAllVipData> res =
                GatewayResponse.buildSuccess(ContentAllVipData.class, response.getReturnMsg());
        GatewayResponseData<ContentAllVipData> resData = GatewayResponse.buildDefaultData();
        resData.setContent(contentAllVipData);
        res.setData(resData);
        return res;
    }

    /**
     * == 契約成立 (http://${host}:${port}/${context}/ecall/do-confirm-voice) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "APLY_NO": "0010220330"
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/doConfirm_voice
     *   認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {"APLY_NO": "0010700001"}
     *
     *   response ->
     *   {
     *       "returnCode":"0",
     *       "detail":{
     *           "resultMsg": "",
     *           "shortMsg": "契約成立",
     *           "resultCode": "000",
     *           "data": {
     *               "APLY_NO": "0010700001"
     *           },
     *           "returnMessage": {"m_desc":[""], "m_msgid":"", "m_sysid":"", "m_type":"", "m_url":"", "m_idx":0, "m_code":0}
     *       },
     *       "returnMsg": "API success",
     *       "UUID": "CB_API1330_20200318_620B18880A_T01",
     *       "hostname": "cxlcsxat01"
     *   }
     *
     *   必填參數: VIP_ID, INSD_ID, INSD_BRDY
     *
     *
     * 3. response to Chat Flow
     *   {
     *       "msg": "success",
     *       "code": 0,
     *       "data": {
     *           "from": "bot",
     *           "showtime": "2020-03-05 11:40:20",
     *           "content": {
     *               "RTN_CODE":"000",
     *               "APLY_NO":"0010220330"
     *           },
     *           "channel": "cathaylife",
     *           "source": "cathaylife",
     *           "role": "cathaylife",
     *           "fb": false,
     *           "fbIntent": null,
     *           "sbIntent": null
     *       }
     *   }
     *
     * @author Tony Liu
     */
    @Override
    public GatewayResponse<ContentConfirmVoice> doConfirmVoice(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D) {
        /* 2. */
        if (StringUtils.isBlank(req.getAplyNo())) {
            GatewayResponse<ContentConfirmVoice> response = GatewayResponse.buildFail(
                    ContentConfirmVoice.class, "parameter: aplyNo is required!");
            return response;
        }

        HttpHeaders apiHeaders = buildApiHttpHeader();
        String url = ECALL_API_URL_PREFIX + "/doConfirm_voice";
        
        Map<String, Object> apiParams = new HashMap<>();
        apiParams.put("APLY_NO", req.getAplyNo());
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(apiParams, apiHeaders);

        CathayBaseResponse<ConfirmVoiceData> apiResponse = null;
        try {
            ResponseEntity<CathayBaseResponse<ConfirmVoiceData>> responseEntity = 
            		cbv10001ApiService.doConfirmVoice(botTpiCallLog3D, url, restTemplate, request);
            apiResponse = responseEntity.getBody();
        } catch (ResourceAccessException | CBV10001DataException ex) {
        	throw ex;
        } catch (Exception ex) {
            LOGGER.error("Invoke cathay e-call API insertTemp_voice fail", ex);
            return getFailResult(ContentConfirmVoice.class, CATHAY_SYSTEM_ERROR);
        }
        CathayResponseDetail<ConfirmVoiceData> apiResponseDetail = apiResponse.getDetail();
        String rtnCode = apiResponseDetail.getResultCode();

        /* 3. */
        ContentConfirmVoice resContent = new ContentConfirmVoice(rtnCode, apiResponseDetail.getData());
        GatewayResponseData<ContentConfirmVoice> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        resData.setResultMsg(apiResponseDetail.getResultMsg());
        resData.setShortMsg(apiResponseDetail.getShortMsg());
        GatewayResponse<ContentConfirmVoice> response = GatewayResponse.buildSuccess(
                ContentConfirmVoice.class, apiResponseDetail.getShortMsg());
        response.setData(resData);
        return response;
    }

    /**
     * == 暫存 (http://${host}:${port}/${context}/ecall/insert-temp-voice) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A123456789",
     *       "aplyNo": "",
     *       "issueDate": "2020-03-20",
     *       "bgnTime":"08:00:00",
     *       "issueDays":"3",
     *       "rgnCode":"3",
     *       "insdList":[
     *           {
     *               "insdId": "A123456700",
     *               "insdBrdy": "1987-01-25",
     *               "hdAmt": "1000",
     *               "hpAmt": "100",
     *               "hkType": "2",
     *               "hkAmt": "10",
     *               "omtpAmt": "1",
     *               "trvlNotConv": "C"
     *           }
     *       ]
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/insertTemp_voice
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter ->
     *     {
     *         "VIP_ID":"N26648942K",
     *         "APLY_NO":"0010700001",
     *         "ISSUE_DATE":"2020-03-20",
     *         "BGN_TIME":"08:00:00",
     *         "ISSUE_DAYS":"3",
     *         "RGN_CODE":"10",
     *         "SRC":"G",
     *         "INSD_LIST":[
     *             {
     *                 "INSD_ID":"N26648942K",
     *                 "INSD_BRDY":"1990-07-29",
     *                 "HD_AMT":"100",
     *                 "HP_AMT":"10",
     *                 "HK_TYPE":"3",
     *                 "HK_AMT":"10",
     *                 "OMTP_AMT":"1",
     *                 "TRVL_NOT_CONV":"N"
     *             },
     *             ...
     *         ]
     *     }
     *
     *     必填參數: VIP_ID, INSD_ID, INSD_BRDY
     *
     *
     * 3. response to Chat Flow
     *
     *   {
     *       "msg":"success",
     *       "code":0,
     *       "data":{
     *           "from":"bot",
     *           "showtime":"2020-03-05 11:40:20",
     *           "content":{
     *               "RTN_CODE":"000",
     *               "APLY_NO":"0010220330",
     *               "ISSUE_DATE":"2020-03-20",
     *               "BGN_TIME":"08:00:00",
     *               "ISSUE_DAYS":"3",
     *               "RGN_CODE":"3",
     *               "INSD_LIST": [
     *                   {
     *                       "INSD_ID":"A123456789",
     *                       "INSD_BRDY":"1990-07-29",
     *                       "HD_AMT":"1000",
     *                       "HP_AMT":"100",
     *                       "HK_TYPE":"1",
     *                       "HK_AMT":"10",
     *                       "OMTP_AMT":"2",
     *                       "TRVL_NOT_CONV":"C"
     *                   }
     *               ]
     *           },
     *           "channel":"cathaylife",
     *           "source":"cathaylife",
     *           "role":"cathaylife",
     *           "fb":false,
     *           "fbIntent":null,
     *           "sbIntent":null
     *       }
     *   }
     *4. 呼叫暫存成功後，更新 idMappingEasycall 中的 aplyNo
     */
    @Override
    public GatewayResponse<ContentTempVoice> insertTempVoice(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D) {
        /* 2. */
        Set<String> invalidFields = new HashSet<>();
        if (StringUtils.isBlank(req.getVipId())) {
            invalidFields.add("vipId");
        }
        if (req.getInsdList() == null) {
            invalidFields.add("insdList");
        } else {
            for (com.tp.tpigateway.model.life.CBV10001VO.FlowRequest.InsdList insd : req.getInsdList()) {
                if (StringUtils.isBlank(insd.getInsdId())) {
                    invalidFields.add("insdId");
                }
                if (StringUtils.isBlank(insd.getInsdBrdy())) {
                    invalidFields.add("insdBrdy");
                }
            }
        }
        if (!invalidFields.isEmpty()) {
            GatewayResponse<ContentTempVoice> response = GatewayResponse.buildFail(
                    ContentTempVoice.class, "parameter: " + invalidFields + " is required!");
            return response;
        }

        HttpHeaders apiHeaders = buildApiHttpHeader();
        String url = ECALL_API_URL_PREFIX + "/insertTemp_voice";
        Map<String, Object> apiParams = new HashMap<>();
        apiParams.put("VIP_ID", req.getVipId());
        apiParams.put("APLY_NO", req.getAplyNo());
        apiParams.put("ISSUE_DATE", req.getIssueDate());
        apiParams.put("BGN_TIME", req.getBgnTime());
        apiParams.put("ISSUE_DAYS", req.getIssueDays());
        apiParams.put("RGN_CODE", req.getRgnCode());
        apiParams.put("SRC", "G"); // 固定傳 "G"
        List<Map<String, String>> insdList = new ArrayList<>();
        for (com.tp.tpigateway.model.life.CBV10001VO.FlowRequest.InsdList insd : req.getInsdList()) {
            Map<String, String> insdMap = new HashMap<>();
            insdMap.put("INSD_ID", insd.getInsdId());
            insdMap.put("INSD_BRDY", insd.getInsdBrdy());
            insdMap.put("HD_AMT", insd.getHdAmt());
            insdMap.put("HP_AMT", insd.getHpAmt());
            insdMap.put("HK_TYPE", insd.getHkType());
            insdMap.put("HK_AMT", insd.getHkAmt());
            insdMap.put("OMTP_AMT", insd.getOmtpAmt());
            insdMap.put("TRVL_NOT_CONV", insd.getTrvlNotConv());
            insdList.add(insdMap);
        }
        apiParams.put("INSD_LIST", insdList);

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(apiParams, apiHeaders);
        CathayBaseResponse<TempVoiceData> apiResponse = null;
        try {
            ResponseEntity<CathayBaseResponse<TempVoiceData>> responseEntity = 
            		cbv10001ApiService.insertTempVoice(botTpiCallLog3D, url, restTemplate, request);
            apiResponse = responseEntity.getBody();
        } catch (ResourceAccessException | CBV10001DataException ex) {
        	throw ex;
        } catch (Exception ex) {
            LOGGER.error("Invoke cathay e-call API insertTemp_voice fail", ex);
            GatewayResponse<ContentTempVoice> response = getFailResult(ContentTempVoice.class, CATHAY_SYSTEM_ERROR);
            return response;
        }
        CathayResponseDetail<TempVoiceData> detail = apiResponse.getDetail();
        String resultCode = detail.getResultCode();

        /* 4. */
        if(API_RESULT_SUCCESS.equals(resultCode)) {
        	updateIdMappingEasyCall(req.getSessionId(), detail.getData().getAplyNo());
        }
        
        /* 3. */
        ContentTempVoice resContent = new ContentTempVoice(resultCode, detail.getData());
        GatewayResponseData<ContentTempVoice> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        resData.setResultMsg(detail.getResultMsg());
        resData.setShortMsg(detail.getShortMsg());
        GatewayResponse<ContentTempVoice> response = GatewayResponse.buildSuccess(ContentTempVoice.class, detail.getShortMsg());
        response.setData(resData);
        return response;
    }

    /**
     * == 核保試算 (http://${host}:${port}/${context}/ecall/do-checkamt-voice) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A123456789",
     *       "aplyNo": "",
     *       "issueDate": "2020-02-20",
     *       "bgnTime":"08:00:00",
     *       "issueDays":"3",
     *       "rgnCode":"3",
     *       "insdList":[
     *           {
     *               "insdId": "A123456700",
     *               "insdBrdy": "1987-01-25",
     *               "hdAmt": "1000",
     *               "hpAmt": "100",
     *               "hkType": "2",
     *               "hkAmt": "10",
     *               "omtpAmt": "1",
     *               "trvlNotConv": "C"
     *           }
     *       ]
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathdoCheckAmt_voiceayholdings-uat/cxl-voice-travel-insr-api/v1/
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter ->
     *     {
     *         "VIP_ID":"N26648942K",
     *         "APLY_NO":"0010700001",
     *         "ISSUE_DATE":"2020-03-20",
     *         "BGN_TIME":"08:00:00",
     *         "ISSUE_DAYS":"3",
     *         "RGN_CODE":"10",
     *         "SRC":"G",
     *         "INSD_LIST":[
     *             {
     *                 "INSD_ID":"N26648942K",
     *                 "INSD_BRDY":"1990-07-29",
     *                 "HD_AMT":"100",
     *                 "HP_AMT":"10",
     *                 "HK_TYPE":"3",
     *                 "HK_AMT":"10",
     *                 "OMTP_AMT":"1",
     *                 "TRVL_NOT_CONV":"N"
     *             },
     *             ...
     *         ]
     *     }
     *
     * 3. response to Chat Flow
     *
     *   {
     *       "msg":"success",
     *       "code":0,
     *       "data":{
     *           "from":"bot",
     *           "showtime":"2020-03-05 11:40:20",
     *           "content":{
     *               "RTN_CODE":"000",
     *               "RGN_CODE":"3",
     *               "ISSUE_DATE":"2020-02-20",
     *               "BGN_TIME":"08:00:00",
     *               "ISSUE_DAYS":"3",
     *               "APLY_NO":"0010700001",
     *               "TOT_PREM":"2000",
     *               "INSD_LIST":[
     *                   {
     *                       "INSD_ID":"A123456789",
     *                       "INSD_BRDY":"1990-07-29",
     *                       "HD_AMT":"1000",
     *                       "HP_AMT":"100",
     *                       "HK_TYPE":"1",
     *                       "HK_AMT":"10",
     *                       "OMTP_AMT":"2",
     *                       "TRVL_NOT_CONV":"C",
     *                       "SELF_PREM":"1000"
     *                   }
     *               ]
     *           },
     *           "channel":"cathaylife",
     *           "source":"cathaylife",
     *           "role":"cathaylife",
     *           "fb":false,
     *           "fbIntent":null,
     *           "sbIntent":null
     *       }
     *   }
     */
    @Override
    public GatewayResponse<ContentCheckAmtVoice> doCheckAmtVoice(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D) {
    	
    	/* 2. */
        HttpHeaders apiHeaders = buildApiHttpHeader();
        String url = ECALL_API_URL_PREFIX + "/doCheckAmt_voice";
        
        Map<String, Object> apiParams = new HashMap<>();
        apiParams.put("VIP_ID", req.getVipId());
        apiParams.put("APLY_NO", req.getAplyNo());
        apiParams.put("ISSUE_DATE", req.getIssueDate());
        apiParams.put("BGN_TIME", req.getBgnTime());
        apiParams.put("ISSUE_DAYS", req.getIssueDays());
        apiParams.put("RGN_CODE", req.getRgnCode());
        apiParams.put("SRC", "G"); // 固定傳 "G"
        List<Map<String, String>> insdList = new ArrayList<>();
        for (com.tp.tpigateway.model.life.CBV10001VO.FlowRequest.InsdList insd : req.getInsdList()) {
            Map<String, String> insdMap = new HashMap<>();
            insdMap.put("INSD_ID", insd.getInsdId());
            insdMap.put("INSD_BRDY", insd.getInsdBrdy());
            insdMap.put("HD_AMT", insd.getHdAmt());
            insdMap.put("HP_AMT", insd.getHpAmt());
            insdMap.put("HK_TYPE", insd.getHkType());
            insdMap.put("HK_AMT", insd.getHkAmt());
            insdMap.put("OMTP_AMT", insd.getOmtpAmt());
            insdMap.put("TRVL_NOT_CONV", insd.getTrvlNotConv());
            insdList.add(insdMap);
        }
        apiParams.put("INSD_LIST", insdList);

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(apiParams, apiHeaders);
        CathayBaseResponse<CheckAmtVoiceData> apiResponse = null;
        try {
            ResponseEntity<CathayBaseResponse<CheckAmtVoiceData>> responseEntity = 
            		cbv10001ApiService.doCheckAmtVoice(botTpiCallLog3D, url, restTemplate, request);
            apiResponse = responseEntity.getBody();
        } catch (ResourceAccessException | CBV10001DataException ex) {
        	throw ex;
        } catch (Exception ex) {
            LOGGER.error("Invoke cathay e-call API doCheckAmt_voice fail", ex);
            GatewayResponse<ContentCheckAmtVoice> response = 
            		getFailResult(ContentCheckAmtVoice.class, CATHAY_SYSTEM_ERROR);
            return response;
        }
        CathayResponseDetail<CheckAmtVoiceData> detail = apiResponse.getDetail();
        String resultCode = detail.getResultCode();

        /* 3. */
        ContentCheckAmtVoice resContent = new ContentCheckAmtVoice(resultCode, detail.getData());
        GatewayResponseData<ContentCheckAmtVoice> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        resData.setResultMsg(detail.getResultMsg());
        resData.setShortMsg(detail.getShortMsg());
        GatewayResponse<ContentCheckAmtVoice> response = GatewayResponse.buildSuccess(ContentCheckAmtVoice.class, detail.getShortMsg());
        response.setData(resData);
        
        /* 4. */
        boolean aplyNoNotNull = 
        		API_RESULT_SUCCESS.equals(detail.getResultCode()) && 
        		detail.getData() != null &&
        		StringUtils.isNotEmpty(detail.getData().getAplyNo());
        
        if(aplyNoNotNull) {
        	updateIdMappingEasyCall(req.getSessionId(), detail.getData().getAplyNo());
        }
        
        return response;
    }

	/**
     * == 旅遊地點代碼 (http://${host}:${port}/${context}/ecall/get-rgn-code) ==
     *
     * 1. Chat Flow request
     * {
     *     "channel": "cathaylife",
     *     "source": "cathaylife",
     *     "role": "cathaylife",
     *     "fbIntent": "",
     *     "sbIntent": "",
     *     "origStr": "泰國",
     *     "places": [
     *         {
     *             "match_user_phrase": false,
     *             "start": 0,
     *             "entity_id": 3685,
     *             "info": "東南亞-泰國;Thailand",
     *             "end": 2,
     *             "tokenized_text": "泰 國",
     *             "text": "泰國",
     *             "is_builtin": true,
     *             "key_user_phrase": null,
     *             "entity": "sys.世界國家城市"
     *         }
     *     ]
     * }
     *
     * 2. 呼叫 CBA10001DataService getRGN_CODE(String CBA_ORIG_STR, List<Object> CBA_PLACEs)
     */
	@SuppressWarnings("unchecked")
	@Override
    public GatewayResponse<ContentGetRgnCode> getRgnCode(FlowRequest req, BotTpiCallLog3D botTpiCallLog3D) {
    	List<Places> places = req.getPlaces();

		List<Object> placeMap;
		try {
			placeMap = JSONUtils.obj2list(places, Object.class);
		} catch (Exception e) {
			LOGGER.error("Json deserialize fail:Error Object:" + places);
            return getFailResult(ContentGetRgnCode.class, CATHAY_SYSTEM_ERROR);
		}
        Map<String, Object> rgnCodeMap = null;
        try {
            rgnCodeMap = cba10001DataService.getRGN_CODE(req.getOrigStr(), placeMap);
        } catch (Exception ex) {
            LOGGER.error("Calling Cathay CBA10001DataService getRGN_CODE() fail!");
            return getFailResult(ContentGetRgnCode.class, CATHAY_SYSTEM_ERROR);
        }

        List<String> rgnNams = (List<String>)rgnCodeMap.get("RGN_NAMEs");
        List<String> rgnPlaces = (List<String>)rgnCodeMap.get("RGN_PLACEs");

        ContentGetRgnCode resContent = new ContentGetRgnCode();
        resContent.setReturnCode("000");
        resContent.setRgnCode((String)rgnCodeMap.get("RGN_CODE"));
        resContent.setRgnName(rgnNams.isEmpty() ? "" : rgnNams.get(0));
        resContent.setRgnPlace(rgnPlaces.isEmpty() ? "" : rgnPlaces.get(0));
        GatewayResponseData<ContentGetRgnCode> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        GatewayResponse<ContentGetRgnCode> response = GatewayResponse.buildSuccess(ContentGetRgnCode.class, "success");
        response.setData(resData);
        return response;
    }

	@Override
	public GatewayResponse<?> saveFlowCallLog3D(FlowCallLog3D flowCallLog3D) {
		cbv10001DataService.saveFlowCallLog3D(flowCallLog3D);
		try {
			LOGGER.info(JSONUtils.obj2json(flowCallLog3D.getTimeLog()));
		} catch (Exception e) {
		}
    	return GatewayResponse.buildSuccess(Object.class, "save flowCall log success");
	}

	private void saveOrUpdateIdMappingEasyCall(FlowRequest req) {
		IdMappingEasycall idMappingEasycall = new IdMappingEasycall();
		idMappingEasycall.setSessionId(req.getSessionId());
		String connectionId = (String) req.getConnectionId();
        if(connectionId == null) {
        	connectionId = (String) req.getSessionId();
        }
		idMappingEasycall.setConnectionId(connectionId);
		idMappingEasycall.setCustId(req.getVipId());
		idMappingEasycall.setAplyNo("");
		
		try {
			cbv10001DataService.saveOrUpdateIdMappingEasyCall(idMappingEasycall);
		} catch (Exception e) {
			throw new CBV10001DataException(e.getMessage());
		}
		
	}
	
	private void updateIdMappingEasyCall(String sessionId, String aplyNo) {
		IdMappingEasycall idMappingEasycall = cbv10001DataService.findIdMappingEasycallBySessionId(sessionId);
		idMappingEasycall.setAplyNo(aplyNo);
		
		cbv10001DataService.updateIdMappingEasyCall(idMappingEasycall);
	}


	private HttpHeaders buildApiHttpHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("x-ibm-client-id", ECALL_API_XIBM_CLIENT_ID);
        return headers;
    }


    /*
     * _建立 fail response，參數 contentType 為 GatewayResponse 的 Data 的 Content 的泛形類別
     * _使用 reflection 建立 content 物件，並對 return code 設值
     *
     * *************************************************************************
     * _如果 contentType 的類別沒有宣告 returnCode 這個 field，則回傳的 JSON 在 content 內的 RTN_CODE 會是 null
     * _並在 JSON 的 msg 顯示， 類別aa.bb.Ccc呼叫setRreturnCode()失敗
     * *************************************************************************
     *
     * @param contentType    - 回傳 flow 的 JSON，content 對應的 Java 類別
     * @param flowReturnCode - 回傳 flow 的 JSON，content 物件內的 RTN_CODE 的值
     */
    private <T> GatewayResponse<T> getFailResult(
    		Class<T> flowContent, String flowReturnCode) {
        T content;
        try {
            content = flowContent.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(("初始化" + flowContent.getName() + "失敗"), ex);
        }

        String errMsg = "error";
        PropertyDescriptor pd;
        try {
            pd = new PropertyDescriptor("returnCode", content.getClass());
            pd.getWriteMethod().invoke(content, flowReturnCode);
        } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            errMsg = "類別 " + flowContent.getName() + " 無法設定 RTN_CODE";
        }

        GatewayResponse<T> failResult = GatewayResponse.buildFail(flowContent, errMsg);
        GatewayResponseData<T> failResData = GatewayResponse.buildDefaultData();
        failResData.setContent(content);
        failResult.setData(failResData);
        return failResult;
    }
    
    /*
     * F: 回傳到flow的Content型別, ex. ContentAllVipData, ContentConfirmVoice
     * D: 國泰api回傳的的data型別, ex. LastIssueData, LawAgeData
     */
    private <F, D> GatewayResponse<F> getFailResult(
    		Class<F> flowContent, CathayBaseResponse<D> response, String resultCode) {
    	
    	GatewayResponse<F> failResult = getFailResult(flowContent, resultCode);
    	GatewayResponseData<F> failResData = failResult.getData();
    	CathayResponseDetail<D> failDetail = response.getDetail();
    	if(failDetail != null) {
    		failResData.setResultMsg(failDetail.getResultMsg());
    		failResData.setShortMsg(failDetail.getShortMsg());
    	}
    	return failResult;
    }
}
