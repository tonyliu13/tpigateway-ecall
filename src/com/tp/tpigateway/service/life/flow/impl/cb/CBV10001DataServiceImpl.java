package com.tp.tpigateway.service.life.flow.impl.cb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.dao.flow.BotTpiCallLog3DDAO;
import com.tp.tpigateway.dao.flow.FlowCallLog3DDAO;
import com.tp.tpigateway.dao.flow.IdMappingEasycallDAO;
import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.FlowCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.IdMappingEasycall;
import com.tp.tpigateway.service.life.flow.cb.CBV10001DataService;

@Transactional
@Service
public class CBV10001DataServiceImpl implements CBV10001DataService {

	@Autowired
    private IdMappingEasycallDAO idMappingEasycallDAO;
    
    @Autowired
    private FlowCallLog3DDAO flowCallLog3DDAO;
    
    @Autowired
    BotTpiCallLog3DDAO botTpiCallLog3DDAO;
	
	@Override
	public void saveFlowCallLog3D(FlowCallLog3D flowCallLog3D) {
		flowCallLog3DDAO.saveOrUpdate(flowCallLog3D);
	}
	
	@Override
	public void saveIdMappingEasyCall(IdMappingEasycall idMappingEasyCall) {
		idMappingEasycallDAO.save(idMappingEasyCall);
	}

	@Override
	public void updateIdMappingEasyCall(IdMappingEasycall idMappingEasyCall) {
		idMappingEasycallDAO.update(idMappingEasyCall);
	}
	
	@Override
	public void saveOrUpdateIdMappingEasyCall(IdMappingEasycall idMappingEasyCall) {
		idMappingEasycallDAO.saveOrUpdate(idMappingEasyCall);
	}

	@Override
	public IdMappingEasycall findIdMappingEasycallBySessionId(String sessionId) {
		return idMappingEasycallDAO.findBySessionId(sessionId);
	}
	
	@Override
	public void saveBotTpiCallLog3D(BotTpiCallLog3D botTpiCallLog3D) {
		botTpiCallLog3DDAO.save(botTpiCallLog3D);
	}
}
