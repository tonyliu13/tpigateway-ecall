package com.tp.tpigateway.service.life.flow.impl.cb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CImageDataItem;
import com.tp.tpigateway.model.common.BotMessageVO.CTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;
import com.tp.tpigateway.model.common.CBALink;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.enumeration.A04;
import com.tp.tpigateway.model.common.enumeration.A30;
import com.tp.tpigateway.model.common.enumeration.C04;
import com.tp.tpigateway.model.common.enumeration.ECallQueueInfo;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001CardService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001DataService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeOTSService;
import com.tp.tpigateway.util.DataUtils;
import com.tp.tpigateway.util.PropUtils;

/**
 * CBA10001：旅平險投保牌卡組裝服務實作
 */
@Service("cBA10001CardService")
public class CBA10001CardServiceImpl implements CBA10001CardService {

    private static Logger log = LoggerFactory.getLogger(CBA10001CardServiceImpl.class);

    @Autowired
    private BotMessageService botMessageService;
    @Autowired
    private CBA10001DataService cBA10001DataService;
    @Autowired
    private CathayLifeTypeOTSService cathayLifeTypeOTSService;

    private String[] CALL_IDs = { "CBA10001_5X1_5_1", "CBA10001_5X1_5_2", "CBA10001_5X1_5_3", "CBA10001_5X1_5_4", "CBA10001_5X1_5_5" };
    
    @Override
    public List<Map<String, Object>> getCardsForCBA10001_1_1_2(BotMessageVO vo, Map<String, Object> a04Result, Map<String, Object> c04Result) {

        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        String IS_NETINSR = MapUtils.getString(a04Result, A04.IS_NETINSR.name());
        String IS_EASY_CALL = MapUtils.getString(a04Result, A04.IS_EASY_CALL.name());
        String resultCode = MapUtils.getString(c04Result, C04.resultCode.name());

        // 網路投保
        // A04[網路投保]=Y
        // →才顯示該張牌卡
        // C04[resultCode]=000
        if ("Y".equals(IS_NETINSR) && "000".equals(resultCode)) {
            CardItem card1 = getCard1ForCBA10001_1_X_2(vo, "1");
            cardList.add(card1.getCards());
        }

        // 易CALL保
        // A04[易CALL保]=Y
        // →才顯示該張牌卡
        if ("Y".equals(IS_EASY_CALL)) {
            CardItem card2 = getCard2ForCBA10001_1_X_2(vo, "1");
            cardList.add(card2.getCards());
        }

        // 立即預約機場投保
        CardItem card3 = getCard3ForCBA10001_1_X_2(vo);
        cardList.add(card3.getCards());

        // 傳真投保
        CardItem card4 = getCard4ForCBA10001_1_X_2(vo);
        cardList.add(card4.getCards());

        return cardList;
    }

    @Override
    public List<Map<String, Object>> getCardsForCBA10001_1_2_2(BotMessageVO vo) {

        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        // 網路投保
        CardItem card1 = getCard1ForCBA10001_1_X_2(vo, "2");
        cardList.add(card1.getCards());

        // 易CALL保
        CardItem card2 = getCard2ForCBA10001_1_X_2(vo, "2");
        cardList.add(card2.getCards());

        // 立即預約機場投保
        CardItem card3 = getCard3ForCBA10001_1_X_2(vo);
        cardList.add(card3.getCards());

        // 傳真投保
        CardItem card4 = getCard4ForCBA10001_1_X_2(vo);
        cardList.add(card4.getCards());

        return cardList;
    }

    /**
     * 網路投保
     * 
     * @param vo
     * @return
     */
    private CardItem getCard1ForCBA10001_1_X_2(BotMessageVO vo, String X) {

        /**
         * (圖片-巴黎鐵塔拿相機)<br>
         * ---------------------------<br>
         * 網路投保<br>
         * ●適用<br>
         * 國泰人壽會員本人<br>
         * ●特色<br>
         * ①3分鐘快速完成投保<br>
         * ②以個人投保為主<br>
         * ③保額上限1500萬元<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        cImageData.setCImageUrl("card_channel_network.png");

        cards.setCImageData(cImageData.getCImageDatas());

        // 一般牌卡2
        cards.setCTextType("7");
        // cards.setCTitle("網路投保");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("0");
        cText0.setCLabel("<div style='font-size:17px;color:#FF9A00;'>網路投保</div>");
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("1");
        cText1.setCText("<b>●適用</b><br/>國泰人壽會員本人");
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCTextClass("1");
        cText3.setCText("<b>●特色</b><br/>①3分鐘快速完成投保<br/>②以個人投保為主<br/>③保額上限1100萬元");

        cTextList.add(cText0.getCTexts());
        cTextList.add(cText1.getCTexts());
        cTextList.add(cText3.getCTexts());

        cards.setCTexts(cTextList);

        // link
        cards.setCLinkType(1);
        List<Map<String, Object>> cLinkList = new ArrayList<Map<String, Object>>();
        if ("1".equals(X)) {
            LifeLink[] links = { CBALink.CBA10001.我要試算投保 };
            cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        } else if ("2".equals(X)) {
            LifeLink[] links = { CBALink.CBA10001.我要試算投保, CBALink.CBA10001.加入國泰人壽會員 };
            cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        }
        cards.setCLinkList(cLinkList);

        log.info("cards=" + new JSONObject(cards).toString());

        return cards;
    }

    /**
     * 易CALL保
     * 
     * @param vo
     * @return
     */
    private CardItem getCard2ForCBA10001_1_X_2(BotMessageVO vo, String X) {

        /**
         * (圖片-巴黎鐵塔拿相機)<br>
         * ---------------------------<br>
         * 易CALL保<br>
         * ●適用<br>
         * 須易CALL保資格(1人/多人)<br>
         * ●特色<br>
         * ①加入會員，全家人可電話投保<br>
         * ②會員可加購旅遊不便險<br>
         * ③保額上限2000萬元<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        cImageData.setCImageUrl("card_channel_easycall.png");

        cards.setCImageData(cImageData.getCImageDatas());

        // 一般牌卡2
        cards.setCTextType("7");
        // cards.setCTitle("易CALL保");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("0");
        cText0.setCLabel("<div style='font-size:17px;color:#FF9A00;'>易CALL保</div>");
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("1");
        cText1.setCText("<b>●適用</b><br/>須易CALL保資格(1人/多人)");
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCTextClass("1");
        cText3.setCText("<b>●特色</b><br/>①加入會員，全家人可電話投保<br/>②會員可加購旅行綜合保險<br/>③保額上限2000萬元");

        cTextList.add(cText0.getCTexts());
        cTextList.add(cText1.getCTexts());
        cTextList.add(cText3.getCTexts());

        cards.setCTexts(cTextList);

        // API : getECallQueueInfo
        // (1)“result”為1 → 檢查”func_show”， ”func_show”為Y則顯示預約投保功能，”func_show”為N不顯示
        // (2) “result”不為1，不顯示預約投保功能
        // (3) 呼叫API出現Exception或http status code不為200，不顯示預約投保功能

        boolean toWeb = false;
        try {
            Map<String, Object> ecResult = cathayLifeTypeOTSService.eCallQueueInfo();
            String result = MapUtils.getString(ecResult, ECallQueueInfo.result.name());
            String func_show = MapUtils.getString(ecResult, ECallQueueInfo.func_show.name());
            if (StringUtils.equals("1", result) && StringUtils.equals("Y", func_show)) {
                toWeb = true;
            }
        } catch (Exception e) {
            log.error("呼叫eCallQueueInfo發生錯誤：" + e.getMessage());
            log.debug("", e);
        }
        
        // link
        cards.setCLinkType(1);
        List<Map<String, Object>> cLinkList = new ArrayList<Map<String, Object>>();
        if ("1".equals(X)) {
            LifeLink[] links = { (toWeb ? CBALink.CBA10001.前往網頁預約電話投保 : CBALink.CBA10001.立即撥打電話投保(PropUtils.CUSTOMER_PHONE)) };
            cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        } else if ("2".equals(X)) {
            LifeLink[] links = { (toWeb ? CBALink.CBA10001.前往網頁預約電話投保 : CBALink.CBA10001.立即撥打電話投保(PropUtils.CUSTOMER_PHONE)), CBALink.CBA10001.申請易CALL保會員 };
            cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        }
        cards.setCLinkList(cLinkList);

        log.info("cards=" + new JSONObject(cards).toString());

        return cards;
    }

    /**
     * 立即預約機場投保
     * 
     * @param vo
     * @return
     */
    private CardItem getCard3ForCBA10001_1_X_2(BotMessageVO vo) {

        /**
         * (圖片-巴黎鐵塔拿相機)<br>
         * ---------------------------<br>
         * 立即預約機場投保<br>
         * ●適用<br>
         * 至國泰人壽機場櫃檯投保<br>
         * ●特色<br>
         * ①節省機場櫃檯投保時間<br>
         * ②可額外加購旅遊不便險<br>
         * ③保額上限3000萬元<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        cImageData.setCImageUrl("card_channel_airport.png");

        cards.setCImageData(cImageData.getCImageDatas());

        // 一般牌卡2
        cards.setCTextType("7");
        // cards.setCTitle("機場投保");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("0");
        cText0.setCLabel("<div style='font-size:17px;color:#FF9A00;'>機場投保</div>");
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("1");
        cText1.setCText("<b>●適用</b><br/>至國泰人壽機場櫃檯投保");
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCTextClass("1");
        cText3.setCText("<b>●特色</b><br/>①節省機場櫃檯投保時間<br/>②可額外加購旅行綜合保險<br/>③保額上限3000萬元");

        cTextList.add(cText0.getCTexts());
        cTextList.add(cText1.getCTexts());
        cTextList.add(cText3.getCTexts());

        cards.setCTexts(cTextList);

        // link
        cards.setCLinkType(1);
        LifeLink[] links = { CBALink.CBA10001.前往網頁預約機場投保, CBALink.CBA10001.查看機場櫃檯資訊 };
        List<Map<String, Object>> cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        cards.setCLinkList(cLinkList);

        log.info("cards=" + new JSONObject(cards).toString());

        return cards;
    }

    /**
     * 傳真投保
     * 
     * @param vo
     * @return
     */
    private CardItem getCard4ForCBA10001_1_X_2(BotMessageVO vo) {

        /**
         * (圖片-巴黎鐵塔拿相機)<br>
         * ---------------------------<br>
         * 傳真投保<br>
         * ●適用<br>
         * 無需簽約或加入會員可用傳真投保<br>
         * ●特色<br>
         * ①寫好要保文件傳真即可<br>
         * ②不須加入會員，可多人同時投保<br>
         * ③保額上限2000萬元<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        cImageData.setCImageUrl("card_channel_fax.png");

        cards.setCImageData(cImageData.getCImageDatas());

        // 一般牌卡2
        cards.setCTextType("7");
        // cards.setCTitle("傳真投保");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("0");
        cText0.setCLabel("<div style='font-size:17px;color:#FF9A00;'>傳真投保</div>");
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("1");
        cText1.setCText("<b>●適用</b><br/>無需簽約或加入會員可傳真投保");
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCTextClass("1");
        cText3.setCText("<b>●特色</b><br/>①寫好要保文件傳真即可<br/>②不需加入會員，可多人投保<br/>③保額上限2000萬元");

        cTextList.add(cText0.getCTexts());
        cTextList.add(cText1.getCTexts());
        cTextList.add(cText3.getCTexts());

        cards.setCTexts(cTextList);

        // link
        cards.setCLinkType(1);
        LifeLink[] links = { CBALink.CBA10001.前往網頁線上傳真投保, CBALink.CBA10001.投保相關申請書及範例 };
        List<Map<String, Object>> cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        cards.setCLinkList(cLinkList);

        log.info("cards=" + new JSONObject(cards).toString());

        return cards;
    }

    @Override
    public CardItem getCardForCBA10001_5X1_5(BotMessageVO vo, String PLAN_NAME, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME, String MAX_DATE_TIME, String[] INSD_DATA,
            String TOT_PREM, String CALL_ID, boolean IS_OMTP) {

        /**
         * (圖片)<br>
         * ---------------------------<br>
         * 套餐-XX方案<br>
         * 旅遊地點           [旅遊地點]( [旅遊天數]天)<br>
         * 保險期間                    [旅遊開始日期]XX:XX起<br>
         *                 [旅遊結束日期]XX:XX止<br>
         * 意外身故、失能                                    1,000萬元<br>
         * 傷害醫療保險金                                          100萬元<br>
         * 突發疾病醫療保險金                                100萬元<br>
         * 總保費<br>
         *                  NTD[總保費]<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        for (int idI = 0; idI < CALL_IDs.length; idI++) {
            String callId = CALL_IDs[idI];
            if (StringUtils.equals(callId, CALL_ID)) {
                if (idI == 0) {
                    cImageData.setCImageUrl("card_plan_popular.png");
                } else if (idI == 1) {
                    cImageData.setCImageUrl("card_plan_honor.png");
                } else if (idI == 2) {
                    cImageData.setCImageUrl("card_plan_economy.png");
                } else if (idI == 3) {
                    cImageData.setCImageUrl("card_plan_budget.png");
	            } else if (idI == 4) {
	            	cImageData.setCImageUrl("card_plan_schengen.png");
	            }
            }
        }
        
        if (StringUtils.isBlank(cImageData.getCImageUrl())) {
            cImageData.setCImageUrl("card-b38.jpg");
        }

        cards.setCImageData(cImageData.getCImageDatas());

        // (1+2)混合牌卡
        cards.setCTextType("7");
        // cards.setCTitle(PLAN_NAME);
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("0");
        cText0.setCLabel("<div style='font-size:17px;color:#FF9A00;'>" + PLAN_NAME + "</div>");
        cText0.setCText("");
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("0");
        cText1.setCLabel("地點天數");
        cText1.setCText(cBA10001DataService.getRGN_NAME(RGN_CODE) + "/" + ISSUE_DAYS + "天");
        CTextItem cText21 = vo.new CTextItem();
        cText21.setCTextClass("0");
        cText21.setCLabel("保險期間");
        cText21.setCText(MIN_DATE_TIME + "起");
        CTextItem cText22 = vo.new CTextItem();
        cText22.setCTextClass("0");
        cText22.setCText(MAX_DATE_TIME + "止");
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCTextClass("0");
        cText3.setCLabel("意外身故、失能");
        cText3.setCText(DataUtils.formatDollar(INSD_DATA[0]) + "萬元");
        CTextItem cText4 = vo.new CTextItem();
        cText4.setCTextClass("0");
        cText4.setCLabel("傷害醫療保險金");
        cText4.setCText(DataUtils.formatDollar(INSD_DATA[1]) + "萬元");
        CTextItem cText5 = vo.new CTextItem();
        cText5.setCTextClass("0");
        cText5.setCLabel("突發疾病醫療保險金");
        cText5.setCText(DataUtils.formatDollar(INSD_DATA[2]) + "萬元");
        CTextItem cText6 = vo.new CTextItem();
        cText6.setCTextClass("0");
        cText6.setCLabel("附加海外醫療專機運送服務");
        CTextItem cText7 = vo.new CTextItem();
        cText7.setCTextClass("2");
        cText7.setCLabel("總保費");
        cText7.setCUnit("NTD");
        cText7.setCAmount(" " + DataUtils.formatDollar(TOT_PREM));

        cTextList.add(cText0.getCTexts());
        cTextList.add(cText1.getCTexts());
        cTextList.add(cText21.getCTexts());
        cTextList.add(cText22.getCTexts());
        cTextList.add(cText3.getCTexts());
        cTextList.add(cText4.getCTexts());
        // [突發疾病醫療保險金]=旅遊地點為國內則不顯示
        if (!"1".equals(RGN_CODE)) {
            cTextList.add(cText5.getCTexts());
        }
        if(IS_OMTP) {
        	cTextList.add(cText6.getCTexts());
        }
        cTextList.add(cText7.getCTexts());

        cards.setCTexts(cTextList);

        // link
        cards.setCLinkType(1);
        LifeLink[] links = { CBALink.CBA10001.確認方案(PLAN_NAME, CALL_ID,IS_OMTP), CBALink.CBA10001.修改地點或期間 };
        List<Map<String, Object>> cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        cards.setCLinkList(cLinkList);

        log.info("cards=" + new JSONObject(cards).toString());

        return cards;
    }

    @Override
    public CardItem getCard2ForCBA10001_5X1_5(BotMessageVO vo, String IDNo, String RGN_CODE, String ISSUE_DAYS, String ISSUE_DATE, String MAIN_AMT, String HP_AMT, String HK_AMT) {

        /**
         * (圖片)
        * ---------------------------
        * 其他投保方案
        * 套餐方案不喜歡嗎？可以前往網頁自助選擇想要投保的保險金額喔~<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        cImageData.setCImageUrl("card_plan_other.png");

        cards.setCImageData(cImageData.getCImageDatas());

        // 一般牌卡2
        cards.setCTextType("7");
        // cards.setCTitle("其他投保方案");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("0");
        cText0.setCLabel("<div style='font-size:17px;color:#FF9A00;'>" + "其他投保方案" + "</div>");
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("1");
        cText1.setCText("套餐方案不喜歡嗎？可以前往網頁自助選擇想要投保的保險金額喔~");
        
        cTextList.add(cText0.getCTexts());
        cTextList.add(cText1.getCTexts());

        cards.setCTexts(cTextList);

        // link
        cards.setCLinkType(1);
        LifeLink[] links = { CBALink.CBA10001.前往網頁投保其他方案(IDNo, RGN_CODE, ISSUE_DAYS, ISSUE_DATE, MAIN_AMT, HP_AMT, HK_AMT) };
        List<Map<String, Object>> cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        cards.setCLinkList(cLinkList);

        log.info("cards=" + new JSONObject(cards).toString());

        return cards;
    }

    @Override
    public List<Map<String, Object>> getCardsForCBA10001_1_1_1_1_1_5_1_1_1(BotMessageVO vo, String APC_NAME, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME,
            String MAX_DATE_TIME, String BENE_NAME, String[] INSD_DATA, int TOT_PREM, String CALL_ID, String TOURIST_ID, String APC_YEAR_INCOME,boolean IS_OMTP) {

        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        /**
         * (圖片)<br>
         * ---------------------------<br>
         * e悠遊 旅行平安險<br>
         * 要/被保人姓名                                              [要保人姓名]<br>
         * 目前是否受監護宣告                                                           否<br>
         * 旅遊地點                          [旅遊地點]( [旅遊天數]天)<br>
         * 保險期間                                    [旅遊開始日期]XX:XX起<br>
         *                      [旅遊結束日期]XX:XX止<br>
         * 意外身故、失能                        [意外身故、失能]萬元<br>
         * 傷害醫療保險金                        [傷害醫療保險金]萬元<br>
         * 突發疾病醫療保險金    [突發疾病醫療保險金]萬元<br>
         * 受益人                                                          [受益人](100%)<br>
         * 總保費<br>
         *                               NTD[總保費]<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        cImageData.setCImageUrl("card-b36.jpg");

        cards.setCImageData(cImageData.getCImageDatas());

        // (1+2)混合牌卡
        cards.setCTextType("7");
        // cards.setCTitle("e悠遊 旅行平安險");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("0");
        cText0.setCLabel("<div style='font-size:17px;color:#FF9A00;'>e悠遊旅行平安保險</div>");
        
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("0");
        cText1.setCLabel("要/被保人姓名");
        cText1.setCText(APC_NAME);

        // [目前受監護宣告] 預設為否
        CTextItem cText2 = vo.new CTextItem();
        cText2.setCTextClass("0");
        cText2.setCLabel("目前是否受監護宣告");
        cText2.setCText("否");

        CTextItem cText3 = vo.new CTextItem();
        cText3.setCTextClass("0");
        cText3.setCLabel("地點天數");
        cText3.setCText(cBA10001DataService.getRGN_NAME(RGN_CODE) + "/" + ISSUE_DAYS + "天");

        CTextItem cText4 = vo.new CTextItem();
        cText4.setCTextClass("0");
        cText4.setCLabel("保險期間");
        cText4.setCText(MIN_DATE_TIME + "起");
        CTextItem cText5 = vo.new CTextItem();
        cText5.setCTextClass("0");
        cText5.setCText(MAX_DATE_TIME + "止");

        CTextItem cText6 = vo.new CTextItem();
        cText6.setCTextClass("0");
        cText6.setCLabel("受益人");
        cText6.setCText(BENE_NAME + "(100%)");

        CTextItem cText7 = vo.new CTextItem();
        cText7.setCTextClass("0");
        cText7.setCLabel("意外身故、失能");
        cText7.setCText(DataUtils.formatDollar(INSD_DATA[0]) + "萬元");
        CTextItem cText8 = vo.new CTextItem();
        cText8.setCTextClass("0");
        cText8.setCLabel("傷害醫療保險金");
        cText8.setCText(DataUtils.formatDollar(INSD_DATA[1]) + "萬元");
        CTextItem cText9 = vo.new CTextItem();
        cText9.setCTextClass("0");
        cText9.setCLabel("突發疾病醫療保險金");
        cText9.setCText(DataUtils.formatDollar(INSD_DATA[2]) + "萬元");
        CTextItem cText10 = vo.new CTextItem();
        cText10.setCTextClass("0");
        cText10.setCLabel("附加海外醫療專機運送服務");
        CTextItem cText11 = vo.new CTextItem();
        cText11.setCTextClass("2");
        cText11.setCLabel("總保費");
        cText11.setCUnit("NTD");
        cText11.setCAmount(" " + DataUtils.formatDollar("" + TOT_PREM));
        
        CTextItem cText12 = vo.new CTextItem();
        cText12.setCTextClass("0");
        cText12.setCLabel("年收入");
        cText12.setCText(APC_YEAR_INCOME);

        cTextList.add(cText0.getCTexts());
        cTextList.add(cText1.getCTexts());
        cTextList.add(cText2.getCTexts());
        cTextList.add(cText12.getCTexts());
        cTextList.add(cText3.getCTexts());
        cTextList.add(cText4.getCTexts());
        cTextList.add(cText5.getCTexts());
        cTextList.add(cText7.getCTexts());
        cTextList.add(cText8.getCTexts());
        // [突發疾病醫療保險金]=旅遊地點為國內則不顯示
        if (!"1".equals(RGN_CODE)) {
            cTextList.add(cText9.getCTexts());
        }
        if(IS_OMTP) {
        	cTextList.add(cText10.getCTexts());
        }
        cTextList.add(cText6.getCTexts());
        cTextList.add(cText11.getCTexts());

        cards.setCTexts(cTextList);

        // link
        cards.setCLinkType(1);
        LifeLink[] links = { CBALink.CBA10001.同意投保資訊並前往繳費(CALL_ID, TOURIST_ID), CBALink.CBA10001.修改投保資訊(CALL_ID, TOURIST_ID) };
        List<Map<String, Object>> cLinkList = botMessageService.getCLinkList(vo, Arrays.asList(links));
        cards.setCLinkList(cLinkList);

        log.info("cards=" + new JSONObject(cards).toString());

        cardList.add(cards.getCards());

        return cardList;
    }

    @Override
    public List<Map<String, Object>> getCardsForCBA10001_1_1_1_1_1_5_1_3_1_1(BotMessageVO vo, String APC_NAME, String RGN_CODE, String ISSUE_DAYS, String MIN_DATE_TIME,
            String MAX_DATE_TIME, String BENE_NAME, String[] INSD_DATA, int TOT_PREM, String CALL_ID, String TOURIST_ID, String APC_YEAR_INCOME,boolean IS_OMTP) {
        return getCardsForCBA10001_1_1_1_1_1_5_1_1_1(vo, APC_NAME, RGN_CODE, ISSUE_DAYS, MIN_DATE_TIME, MAX_DATE_TIME, BENE_NAME, INSD_DATA, TOT_PREM, CALL_ID, TOURIST_ID,
                APC_YEAR_INCOME, IS_OMTP);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<Map<String, Object>> getCardsForCBA10001_5X1_5_6X1_1_1(BotMessageVO vo, String CARD4, String TOT_PREM, Map a30Data, String POLICY_NO) {

        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

        /**
         * (圖片)-投保完成<br>
         * --------------------------------<br>
         * 保單幫你寄送到 <br>    
         * [手機]<br>
         * [EMAIL]<br>
         * 繳費方式       信用卡末四碼[XXXX]<br>
         * 繳交保費<br>
         *                NTD[繳交保費]<br>
         */

        CardItem cards = vo.new CardItem();
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB);
        cImageData.setCImageUrl("card38.jpg");

        cards.setCImageData(cImageData.getCImageDatas());

        // (1+2)混合牌卡
        cards.setCTextType("7");
        //cards.setCTitle("保單幫你寄送到");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText0 = vo.new CTextItem();
        cText0.setCTextClass("1");
        cText0.setCLabel("保單幫你寄送到");
        CTextItem cText1 = vo.new CTextItem();
        cText1.setCTextClass("1");
        // phone
        String phone = MapUtils.getString(a30Data, A30.mobl_no.name());
        cText1.setCText("<img src='resources/images/icon_phone@2x.png' style='width: 24px;height: 24px;display: inline-table;'> " + phone);
        CTextItem cText2 = vo.new CTextItem();
        cText2.setCTextClass("1");
        // EMAIL
        String EMAIL = MapUtils.getString(a30Data, A30.email.name());;
        cText2.setCText("<img src='resources/images/icon_mail@2x.png' style='width: 24px;height: 24px;display: inline-table;'> " + EMAIL);
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCTextClass("0");
        cText3.setCLabel("保單號碼");
        cText3.setCText(POLICY_NO);
        CTextItem cText4 = vo.new CTextItem();
        cText4.setCTextClass("0");
        cText4.setCLabel("繳費方式");
        cText4.setCText("信用卡末四碼" + CARD4);
        CTextItem cText5 = vo.new CTextItem();
        cText5.setCTextClass("2");
        cText5.setCLabel("繳交保費");
        cText5.setCUnit("NTD");
        cText5.setCAmount(DataUtils.formatDollar(TOT_PREM));

        // [手機][EMAIL]沒有值不顯示
        cTextList.add(cText0.getCTexts());
        if (StringUtils.isNotBlank(phone)) {
            cTextList.add(cText1.getCTexts());
        }
        if (StringUtils.isNotBlank(EMAIL)) {
            cTextList.add(cText2.getCTexts());
        }
        cTextList.add(cText3.getCTexts());
        cTextList.add(cText4.getCTexts());
        cTextList.add(cText5.getCTexts());
        
        cards.setCTexts(cTextList);

        log.info("cards=" + new JSONObject(cards).toString());

        cardList.add(cards.getCards());

        return cardList;
    }
}
