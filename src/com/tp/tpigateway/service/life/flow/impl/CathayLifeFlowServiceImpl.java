package com.tp.tpigateway.service.life.flow.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.tpigateway.configuration.BotCardShowData;
import com.tp.tpigateway.configuration.BotMessageSetting;
import com.tp.tpigateway.dao.common.BotCardImageStyleDAO;
import com.tp.tpigateway.dao.common.BotCardImageTemplateDAO;
import com.tp.tpigateway.dao.common.BotCardImageTextTemplateDAO;
import com.tp.tpigateway.dao.common.BotCardStyleDAO;
import com.tp.tpigateway.dao.common.BotCardTemplateDAO;
import com.tp.tpigateway.dao.common.BotCardTextStyleDAO;
import com.tp.tpigateway.dao.common.BotCardTextTemplateDAO;
import com.tp.tpigateway.dao.common.BotReplyCardsDao;
import com.tp.tpigateway.dao.common.BotReplyContentDao;
import com.tp.tpigateway.model.common.ABBLink;
import com.tp.tpigateway.model.common.APIResult;
import com.tp.tpigateway.model.common.BotCardImageStyle;
import com.tp.tpigateway.model.common.BotCardImageStyleId;
import com.tp.tpigateway.model.common.BotCardImageTemplate;
import com.tp.tpigateway.model.common.BotCardImageTextTemplate;
import com.tp.tpigateway.model.common.BotCardImageTextTemplateId;
import com.tp.tpigateway.model.common.BotCardStyle;
import com.tp.tpigateway.model.common.BotCardStyleId;
import com.tp.tpigateway.model.common.BotCardTemplate;
import com.tp.tpigateway.model.common.BotCardTemplateId;
import com.tp.tpigateway.model.common.BotCardTextStyle;
import com.tp.tpigateway.model.common.BotCardTextTemplate;
import com.tp.tpigateway.model.common.BotCardTextTemplateId;
import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CImageDataItem;
import com.tp.tpigateway.model.common.BotMessageVO.CImageTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;
import com.tp.tpigateway.model.common.BotReplyCards;
import com.tp.tpigateway.model.common.BotReplyContent;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.enumeration.D12;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotLinkService;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.impl.CathayLifeBotMessageServiceImpl.Card.CardWidth;
import com.tp.tpigateway.service.common.impl.CathayLifeBotMessageServiceImpl.Card.CarouselCard;
import com.tp.tpigateway.service.life.flow.CathayLifeFlowService;
import com.tp.tpigateway.util.LocalCache;

@Service("cathayLifeFlowService")
public class CathayLifeFlowServiceImpl implements CathayLifeFlowService {

	private static Logger log = LoggerFactory.getLogger(CathayLifeFlowServiceImpl.class);

	private boolean isInfo = log.isInfoEnabled();

	@Autowired
	private BotReplyContentDao botReplyContentDao;

	@Autowired
	private BotReplyCardsDao botReplyCardsDao;

	@Autowired
	private BotCardStyleDAO botCardStyleDAO;

	@Autowired
	private BotCardTemplateDAO botCardTemplateDAO;

	@Autowired
	private BotCardImageStyleDAO botCardImageStyleDAO;

	@Autowired
	private BotCardImageTemplateDAO botCardImageTemplateDAO;

	@Autowired
	private BotCardImageTextTemplateDAO botCardImageTextTemplateDAO;

	@Autowired
	private BotCardTextStyleDAO botCardTextStyleDAO;

	@Autowired
	private BotCardTextTemplateDAO botCardTextTemplateDAO;

	@Autowired
	protected BotLinkService botLinkService;

	@Autowired
	private BotMessageService botMessageService;

	private BotCardImageTemplate content;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tp.tpigateway.service.life.flow.CathayLifeFlowService#genResponse(java.lang.String,
	 * com.tp.tpigateway.model.life.BotLifeRequestVO, java.lang.String)
	 */
	@Override
	public BotMessageVO genResponse(String source, BotLifeRequestVO reqVO, APIResult apiResult) throws Exception {
		String respNodeID = apiResult.getRespNodeID();
		List<BotReplyContent> repContents = botReplyContentDao.findByRespNodeId(respNodeID);

		log.debug("repContents size:" + repContents.size());
		BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), source, reqVO.getRole());
		for (BotReplyContent content : repContents) {
			String uiType = content.getUiType();
			String respNodSeq = content.getRespNodeSeq();
			Boolean isShow = true;
			Map apiResultDataMap = null;
			if (StringUtils.isNotBlank(content.getDataKey())) {
				apiResultDataMap = (Map) apiResult.getApiResult(content.getDataKey());
				isShow = MapUtils.getBoolean(apiResultDataMap, BotMessageSetting.IS_SHOW, true);
			}
			log.debug("[BotReplyContent]:" + content);
			log.debug("isShow:" + isShow);
			if (isShow) {
				if ("10".equals(uiType)) {
					// 10.靜態文字
					botMessageService.setTextResult(vo, content.getReplyText());
				} else if ("11".equals(uiType)) {
					// 11動態文字
					botMessageService.setTextResultWithReplace(vo, content.getReplyText(), apiResultDataMap);
				} else if ("21".equals(uiType)) {
					// 21靜態文字牌卡
					List<BotReplyCards> cards = botReplyCardsDao.findByRespNodeSeq(respNodSeq);
					log.debug("Card size:" + cards.size());
					CarouselCard[] txtCards = new CarouselCard[cards.size()];
					for (int i = 0; i < cards.size(); i++) {
						BotReplyCards card = cards.get(i);
						log.debug("[BotReplyCards]:" + card);
						LifeLink[] links = botLinkService.getOptionLinks(card.getOptionId(), apiResult);
						log.debug("[LifeLinks]:" + Arrays.asList(links));
						// new LifeLink[] { ABBLink.ABB00004.ABB00004_1_1}
						CarouselCard c1 = botMessageService.createCarouselCard(card.getTitle(), new String[] { card.getText() },
								links);
						if (card.getText().length() > 10) {
							c1.setWidth(CardWidth._250);
						}
						txtCards[i] = c1;
						log.debug("CarouselCard:" + c1);
					}
					botMessageService.setCarouselCards(vo, null, txtCards);
				} else if ("22".equals(uiType)) {
					// 22.靜態PDF牌卡
					List<BotReplyCards> cards = botReplyCardsDao.findByRespNodeSeq(respNodSeq);

					List<Map<String, Object>> pdfCards = new ArrayList<Map<String, Object>>();
					for (int i = 0; i < cards.size(); i++) {
						BotReplyCards card = cards.get(i);
						LifeLink[] links = botLinkService.getOptionLinks(card.getOptionId(), apiResult);
						Map<String, Object> pdf = botMessageService.getCardForDownloadPDF(vo, card.getPdfName(), Arrays.asList(links));
						pdfCards.add(pdf);
					}
					botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, pdfCards);

				} else if ("23".equals(uiType)) {
					// 23一般牌卡、
					List<Map<String, Object>> cardList;
					if (content.getReplyText() != null && "common:service_location".equals(content.getReplyText())) {
						// 服務據點牌卡
						// cardList = botMessageService.getCardsForI8_1_4_1(vo);
						cardList = getServiceLocationCards(reqVO, vo, apiResult, content.getOptionId());
					} else {
						cardList = this.getCardsByDatabaseConfig(vo, apiResult, content);

						if (cardList == null || cardList.isEmpty()) {
							cardList = this.getCustomCards(reqVO, source, vo, apiResult, content.getOptionId());
						}
					}
					botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
				} else if ("24".equals(uiType)) {
					Map pdfResult = apiResultDataMap;
					// 回覆PDF牌卡
					String downloadFileFullPath = MapUtils.getString(pdfResult, D12.downloadFileFullPath.name());
					String downloadFileName = MapUtils.getString(pdfResult, D12.downloadFileName.name());
					String lts = MapUtils.getString(pdfResult, D12.lts.name());

					if (StringUtils.isBlank(downloadFileFullPath) || StringUtils.isBlank(downloadFileName)) {
						log.error("動態PDF下載參數錯誤:" + pdfResult);
					} else {
						if (StringUtils.isNotBlank(lts)) {
							botMessageService.setPDFCard(vo,
									ABBLink.downloadFileLink(downloadFileFullPath, downloadFileName, lts, "開啓"),
									content.getReplyText());
						} else {
							botMessageService.setPDFCard(vo, ABBLink.downloadFileLink(downloadFileFullPath, downloadFileName, "開啓"),
									content.getReplyText());
						}
					}
				} else if ("25".equals(uiType)) {
                    // 25.靜態EDM牌卡
                    List<BotReplyCards> cards = botReplyCardsDao.findByRespNodeSeq(respNodSeq);

                    List<Map<String, Object>> pdfCards = new ArrayList<Map<String, Object>>();
                    for (int i = 0; i < cards.size(); i++) {
                        BotReplyCards card = cards.get(i);
                        LifeLink[] links = botLinkService.getOptionLinks(card.getOptionId(), apiResult);
                        Map<String, Object> pdf = botMessageService.getCardForDownloadEDM(vo, card.getPdfName(), Arrays.asList(links));
                        pdfCards.add(pdf);
                    }
                    botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, pdfCards);

                } else if ("40".equals(uiType)) {
					// 40.快速回復
					LifeLink[] links = null;
					if (apiResultDataMap != null && apiResultDataMap.containsKey(BotMessageSetting.REPLACE_OPTION_WITH_LINKID)) {
						links = botLinkService.getReplaceOptionLinks(content.getOptionId(), apiResult, content.getDataKey());
					} else {
						links = botLinkService.getOptionLinks(content.getOptionId(), apiResult);
					}
					botMessageService.setQuickReply(vo, links);
				} else if ("50".equals(uiType)) {
					// 50.靜態圖卡
					botMessageService.setSticker(vo, content.getImageUrl());
				} else if ("61".equals(uiType)) {
					// 61.靜態注意事項
					botMessageService.setNoteResult(vo, "注意事項", content.getReplyText());
				} else if ("62".equals(uiType)) {
					// 62.動態注意事項
					botMessageService.setNoteResultWithReplace(vo, "注意事項", content.getReplyText(), apiResultDataMap);
				} else if ("63".equals(uiType)) {
					// 63.橫幅靜態注意事項
					botMessageService.setHNoteResult(vo, content.getReplyText());
				} else if ("64".equals(uiType)) {
					// 64..橫幅動態注意事項
					botMessageService.setHNoteResultWithReplace(vo, content.getReplyText(), apiResultDataMap);
				}
			}
		}

		return vo;

	}

	private List<Map<String, Object>> getServiceLocationCards(BotLifeRequestVO reqVO, BotMessageVO vo, APIResult apiResult,
			String optionID) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		String[] data_recipient = { "世界服務中心", "桃園服務中心", "育仁服務中心", "逢甲服務中心", "高雄服務中心" };
		String[] data_image = { "service01.jpg", "service02.jpg", "service03.jpg", "service04.jpg", "service05.jpg" };
		String[] data_add = { "105 台北市松山區南京東路四段１２６號４樓", "330 桃園市桃園區中山路８４５號４樓", "404 台中市北區進化路５８１號２樓", "700 台南市中西區西門路一段４９６號４樓",
				"801 高雄市前金區中華三路１４６號３樓Ａ室" };

		for (int i = 0; i < data_add.length; i++) {

			CardItem cards = vo.new CardItem();// 創一個牌卡物件
			// cards.setCName("地址");
			cards.setCWidth(BotMessageVO.CWIDTH_200);

			CImageDataItem cImageData = vo.new CImageDataItem();// 設定牌卡圖片
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl(data_image[i]);
			cards.setCImageData(cImageData.getCImageDatas());
			cards.setCTextType("3"); // 文字與邊框的間距

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();// 設定牌卡圖片區塊的文字
			CTextItem cText1 = vo.new CTextItem();
			cText1.setCLabel("收件人");
			cText1.setCText(data_recipient[i]);
			cTextList.add(cText1.getCTexts());

			CTextItem cText2 = vo.new CTextItem();
			cText2.setCLabel("收件地址");
			cText2.setCText(data_add[i]);
			cTextList.add(cText2.getCTexts());

			cards.setCTexts(cTextList);

			log.info("cards=" + new JSONObject(cards).toString());

			cardList.add(cards.getCards());
		}

		return cardList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tp.tpigateway.service.life.flow.CathayLifeFlowService#getRequireParams(com.tp.tpigateway.model.life.BotLifeRequestVO,
	 * java.lang.String)
	 */
	@Override
	public Map<String, String> getRequireParams(BotLifeRequestVO reqVO, String nodeID) throws Exception {

		Map<String, String> requireParams = getDefaultRequireParams(reqVO);
		// 各情境流程自行實作
		this.setCustomParams(reqVO, nodeID, requireParams);

		return requireParams;

	}

	/**
	 * 定義界面讓各情境流程,依據NodeID自行實作必要的檢核參數
	 * 
	 * @param reqVO
	 * @param nodeID
	 * @param requireParams
	 */
	protected void setCustomParams(BotLifeRequestVO reqVO, String nodeID, Map<String, String> requireParams) {
		// 核身一律需要保單號碼 / 身份證字號 或其他需要的參數
		// requireParams.put("PolicyNo", reqVO.getPolicyNo());
		// requireParams.put("IDNo", reqVO.getIDNo());
		//

	}

	/**
	 * 預設的必要建核參數
	 * 
	 * @param reqVO
	 * @return
	 */
	private Map<String, String> getDefaultRequireParams(BotLifeRequestVO reqVO) {
		Map<String, String> requireParams = new HashMap<>();
		requireParams.put("Channel", reqVO.getChannel());
		requireParams.put("Role", reqVO.getRole());
		requireParams.put("NodeID", reqVO.getNodeID());
		return requireParams;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tp.tpigateway.service.life.flow.CathayLifeFlowService#getData(com.tp.tpigateway.model.life.BotLifeRequestVO,
	 * java.lang.String)
	 */
	@Override
	public APIResult getData(BotLifeRequestVO reqVO, String source, String nodeID) throws Exception {
		APIResult result = new APIResult();
		result.setRespNodID(nodeID);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tp.tpigateway.service.life.flow.CathayLifeFlowService#getCustomCards(com.tp.tpigateway.model.life.BotLifeRequestVO,
	 * java.lang.String, com.tp.tpigateway.model.common.APIResult)
	 */

	@Override
	public List<Map<String, Object>> getCustomCards(BotLifeRequestVO reqVO, String source, BotMessageVO vo, APIResult apiResult,
			String optionID) {
		return Collections.EMPTY_LIST;
	}

	@Override
	public List<Map<String, Object>> getCardsByDatabaseConfig(BotMessageVO botMessageVO, APIResult apiResult,
			BotReplyContent botReplyContent) {

		String cardId = botReplyContent.getCardId();

		if (isInfo) {
			log.info("cardId:" + cardId);
		}

		String cacheKey = "botCardTemplates_" + cardId;
		List<BotCardTemplate> botCardTemplates = (List<BotCardTemplate>) LocalCache.get(cacheKey);
		if (botCardTemplates == null) {
			botCardTemplates = botCardTemplateDAO.findByCardId(cardId);
		}

		if (isInfo) {
			log.info("botCardTemplates:" + botCardTemplates);
		}

		if (botCardTemplates == null || botCardTemplates.isEmpty()) {
			return Collections.EMPTY_LIST;
		}

		LocalCache.register(cacheKey, botCardTemplates);

		int count = botCardTemplates.size();
		if (isInfo) {
			log.info("botCardTemplates.size:" + count);
		}

		// 如果只有一個Template，則檢查bot_reply_content是否有設定data_key
		List<Map> dataList = null;
		Map dataMap = null;
		String botReplyContent_dataKey = botReplyContent.getDataKey();
		if (count == 1) {
			if (StringUtils.isNotBlank(botReplyContent_dataKey)) {
				Object obj = apiResult.getApiResult(botReplyContent_dataKey);
				if (obj != null) {
					if (obj instanceof Map) {
						dataMap = (Map) obj;
						if (dataMap.containsKey(BotMessageSetting.cardTemplateData)) {
							obj = dataMap.get(BotMessageSetting.cardTemplateData);
							if (obj instanceof Map) {
								dataMap = (Map) obj;
							} else if (obj instanceof List) {
								dataList = (List<Map>) obj;
							}
						}
					} else if (obj instanceof List) {
						dataList = (List<Map>) obj;
					}
				}
			}
		}

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		String botReplyContent_optionId = botReplyContent.getOptionId();
		StringBuffer sbf = new StringBuffer();

		if (dataList != null && !dataList.isEmpty()) {
			for (Map data : dataList) {
				createCard(botMessageVO, apiResult, cardId, botCardTemplates, data, botReplyContent_optionId, botReplyContent_dataKey,
						cardList, sbf);
			}
		} else {
			createCard(botMessageVO, apiResult, cardId, botCardTemplates, dataMap, botReplyContent_optionId, botReplyContent_dataKey,
					cardList, sbf);
		}

		if (isInfo) {
			log.info("cardList:" + new JSONArray(cardList).toString());
		}

		return cardList;
	}

	private void createCard(BotMessageVO botMessageVO, APIResult apiResult, String cardId, List<BotCardTemplate> botCardTemplates,
			Map data, String botReplyContent_optionId, String botReplyContent_dataKey, List<Map<String, Object>> cardList,
			StringBuffer sbf) {
		if (isInfo) {
			log.info("data:" + data);
		}

		boolean noData = data == null || data.isEmpty();
		Map botCard_dataMap = null;
		boolean hasBotCard_dataMap = false;
		BotCardShowData botCardShowData = null;
		BotCardShowData botCardShowData_default = new BotCardShowData();

		if (!noData) {
			botCard_dataMap = data;
			hasBotCard_dataMap = true;
			botCardShowData = (BotCardShowData) botCard_dataMap.get(BotCardShowData.name);

			if (isInfo) {
				log.info("botCardShowData >>> " + botCardShowData);
			}

			if (botCardShowData == null) {
				botCardShowData = botCardShowData_default;
			}
		}

		List<LifeLink> lifeLinks = new ArrayList<LifeLink>();

		for (BotCardTemplate botCardTemplate : botCardTemplates) {
			if (isInfo) {
				log.info("bot_card_template:" + botCardTemplate);
			}

			CardItem card = botMessageVO.new CardItem();

			int cardStyle = botCardTemplate.getCardStyle();
			String cacheKey = "botCardStyle_" + cardStyle;

			BotCardStyle botCardStyle = (BotCardStyle) LocalCache.get(cacheKey);
			if (botCardStyle == null) {
				botCardStyle = botCardStyleDAO.findByStyleId(botCardTemplate.getCardStyle());

				if (botCardStyle != null) {
					LocalCache.register(cacheKey, botCardStyle);
				}
			}

			if (isInfo) {
				log.info("bot_card_style:" + botCardStyle);
			}

			if (botCardStyle != null) {

				BotCardStyleId botCardStyleId = botCardStyle.getId();

				card.setCWidth(CardAttribute.getCWidth(botCardStyleId.getWidth()));

				String textType = CardAttribute.getCTextType(botCardStyleId.getTextType());
				if (StringUtils.isNotBlank(textType)) {
					card.setCTextType(textType);
				}

				Integer linkType = CardAttribute.getCLinkType(botCardStyleId.getLinkType());
				if (linkType != null) {
					card.setCLinkType(linkType);
				}
			}

			if (noData) {
				botCardShowData = null;
				botCard_dataMap = null;
				hasBotCard_dataMap = false;

				String botCardTemplate_dataKey = botCardTemplate.getDataKey();
				if (StringUtils.isNotBlank(botCardTemplate_dataKey)) {
					Object obj = apiResult.getApiResult(botCardTemplate_dataKey);
					if (obj != null && obj instanceof Map && !((Map) obj).isEmpty()) {
						botCard_dataMap = (Map) obj;
						hasBotCard_dataMap = true;
						botCardShowData = (BotCardShowData) botCard_dataMap.get(BotCardShowData.name);

						if (isInfo) {
							log.info("botCardShowData >>> " + botCardShowData);
						}
					}
				}

				if (botCardShowData == null) {
					botCardShowData = botCardShowData_default;
				}
			}

			String cardName = botCardTemplate.getCardName();
			if (StringUtils.isNotBlank(cardName)) {
				card.setCName(getText(cardName, hasBotCard_dataMap, botCard_dataMap));
			}

			if (botCardShowData.showBotCardTemplate_message()) {
				String message = botCardTemplate.getMessage();
				if (StringUtils.isNotBlank(message)) {
					card.setCMessage(getText(message, hasBotCard_dataMap, botCard_dataMap));
				}
			}

			if (botCardShowData.showBotCardTemplate_title()) {
				String title = botCardTemplate.getTitle();
				if (StringUtils.isNotBlank(title)) {
					card.setCTitle(getText(title, hasBotCard_dataMap, botCard_dataMap));
				}
			}

			if (botCardShowData.showBotCardTemplate_breakLine()) {
				if (botCardTemplate.isBreakLine()) {
					card.setCBreakLine("Y");
				}
			}

			boolean hasImageTemplate = botCardTemplate.isImageTemplate();
			boolean hasTextTemplate = botCardTemplate.isTextTemplate();
			boolean hasText2Template = botCardTemplate.isText2Template();
			String templateId = null;
			if (hasImageTemplate || hasTextTemplate || hasText2Template) {
				// get id for table(bot_card_image_template, bot_card_image_text_template, bot_card_text_template)
				sbf.append(cardId);
				BotCardTemplateId botCardTemplateId = botCardTemplate.getId();
				short seq = botCardTemplateId.getSeq();
				if (seq >= 0) {
					sbf.append('@').append(seq);
				}
				templateId = sbf.toString();
				sbf.setLength(0);
			}

			if (hasImageTemplate) {
				cacheKey = "botCardImageTemplate_" + templateId;

				BotCardImageTemplate botCardImageTemplate = (BotCardImageTemplate) LocalCache.get(cacheKey);
				if (botCardImageTemplate == null) {
					botCardImageTemplate = botCardImageTemplateDAO.findByImageId(templateId);

					if (botCardImageTemplate != null) {
						LocalCache.register(cacheKey, botCardImageTemplate);
					}
				}

				if (isInfo) {
					log.info("bot_card_image_template:" + botCardImageTemplate);
				}

				if (botCardImageTemplate != null) {

					CImageDataItem cImageData = botMessageVO.new CImageDataItem();

					if (noData) {
						botCardShowData = null;
						botCard_dataMap = null;
						hasBotCard_dataMap = false;

						String botCardImageTemplate_dataKey = botCardImageTemplate.getDataKey();
						if (StringUtils.isNotBlank(botCardImageTemplate_dataKey)) {
							Object obj = apiResult.getApiResult(botCardImageTemplate_dataKey);
							if (obj != null && obj instanceof Map && !((Map) obj).isEmpty()) {
								botCard_dataMap = (Map) obj;
								hasBotCard_dataMap = true;
								botCardShowData = (BotCardShowData) botCard_dataMap.get(BotCardShowData.name);

								if (isInfo) {
									log.info("botCardShowData >>> " + botCardShowData);
								}
							}
						}

						if (botCardShowData == null) {
							botCardShowData = botCardShowData_default;
						}
					}

					int imageStyle = botCardImageTemplate.getImageStyle();
					if (imageStyle >= 0) {
						cacheKey = "botCardImageStyle_" + imageStyle;

						BotCardImageStyle botCardImageStyle = (BotCardImageStyle) LocalCache.get(cacheKey);
						if (botCardImageStyle == null) {
							botCardImageStyle = botCardImageStyleDAO.findByStyleId(imageStyle);

							if (botCardImageStyle != null) {
								LocalCache.register(cacheKey, botCardImageStyle);
							}
						}

						if (isInfo) {
							log.info("bot_card_image_style:" + botCardImageStyle);
						}

						if (botCardImageStyle != null) {

							BotCardImageStyleId botCardImageStyleId = botCardImageStyle.getId();

							String image = botCardImageStyleId.getImage();
							if (StringUtils.isNotBlank(image)) {
								cImageData.setCImage(getText(image, hasBotCard_dataMap, botCard_dataMap));
							}

							Integer imageTextType = ImageDataAttribute.getCImageTextType(botCardImageStyleId.getImageTextType());
							if (imageTextType != null) {
								cImageData.setCImageTextType(imageTextType);
							}
						}
					}

					String imageUrl = botCardImageTemplate.getImageUrl();
					if (StringUtils.isNotBlank(imageUrl)) {
						cImageData.setCImageUrl(getText(imageUrl, hasBotCard_dataMap, botCard_dataMap));
					}

					if (botCardShowData.showBotCardImageTemplate_imageTag()) {
						String imageTag = botCardImageTemplate.getImageTag();
						if (StringUtils.isNotBlank(imageTag)) {
							cImageData.setCImageTag(getText(imageTag, hasBotCard_dataMap, botCard_dataMap));
						}
					}

					boolean hasImageTextTemplate = botCardImageTemplate.isImageTextTemplate();
					if (hasImageTextTemplate) {

						cacheKey = "botCardImageTextTemplates_" + templateId;

						List<BotCardImageTextTemplate> botCardImageTextTemplates = (List<BotCardImageTextTemplate>) LocalCache
								.get(cacheKey);
						if (botCardImageTextTemplates == null) {
							botCardImageTextTemplates = botCardImageTextTemplateDAO.findByImageTextId(templateId);

							if (botCardImageTextTemplates != null && !botCardImageTextTemplates.isEmpty()) {
								LocalCache.register(cacheKey, botCardImageTextTemplates);
							}
						}

						if (isInfo) {
							log.info("bot_card_image_text_template:" + botCardImageTextTemplates);
						}

						if (botCardImageTextTemplates != null && !botCardImageTextTemplates.isEmpty()) {

							List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();

							for (BotCardImageTextTemplate botCardImageTextTemplate : botCardImageTextTemplates) {
								if (noData) {
									botCardShowData = null;
									botCard_dataMap = null;
									hasBotCard_dataMap = false;

									String botCardImageTextTemplate_dataKey = botCardImageTextTemplate.getDataKey();
									if (StringUtils.isNotBlank(botCardImageTextTemplate_dataKey)) {
										Object obj = apiResult.getApiResult(botCardImageTextTemplate_dataKey);
										if (obj != null && obj instanceof Map && !((Map) obj).isEmpty()) {
											botCard_dataMap = (Map) obj;
											hasBotCard_dataMap = true;
											botCardShowData = (BotCardShowData) botCard_dataMap.get(BotCardShowData.name);

											if (isInfo) {
												log.info("botCardShowData >>> " + botCardShowData);
											}
										}
									}

									if (botCardShowData == null) {
										botCardShowData = botCardShowData_default;
									}
								}

								Set<BotCardImageTextTemplateId> botCardImageTexts_hide = botCardShowData.getBotCardImageText_hide();
								if (botCardImageTexts_hide.contains(botCardImageTextTemplate.getId())) {
									continue;
								}

								CImageTextItem cImageText = botMessageVO.new CImageTextItem();
								String imageTextTitle = botCardImageTextTemplate.getImageTextTitle();
								if (botCardShowData.showBotCardImageTextTemplate_imageTextTitle()
										&& StringUtils.isNotBlank(imageTextTitle)) {
									cImageText.setCImageTextTitle(getText(imageTextTitle, hasBotCard_dataMap, botCard_dataMap));
								}

								String imageText = botCardImageTextTemplate.getImageText();
								if (botCardShowData.showBotCardImageTextTemplate_imageText() && StringUtils.isNotBlank(imageText)) {
									cImageText.setCImageText(getText(imageText, hasBotCard_dataMap, botCard_dataMap));
								}

								cImageTexts.add(cImageText.getCImageTexts());
							}

							cImageData.setCImageTexts(cImageTexts);
						}
					}

					card.setCImageData(cImageData.getCImageDatas());
				}
			}

			if (hasTextTemplate) {
				createCardText(false, templateId, botMessageVO, apiResult, noData, botCard_dataMap, hasBotCard_dataMap,
						botCardShowData, botCardShowData_default, card);
			}

			if (hasText2Template) {
				createCardText(true, templateId + ".2", botMessageVO, apiResult, noData, botCard_dataMap, hasBotCard_dataMap,
						botCardShowData, botCardShowData_default, card);
			}

			String cardOptionId = botCardTemplate.getOptionId();
			String optionDataKey = botCardTemplate.getOptionDataKey();
			LifeLink[] linkInfo = null;
			if (StringUtils.isBlank(cardOptionId)) {
				cardOptionId = botReplyContent_optionId;
			} else if (!noData && data.containsKey(cardOptionId)) {
				Object tmp = data.get(cardOptionId);
				if (tmp != null && tmp instanceof LifeLink[]) {
					linkInfo = (LifeLink[]) tmp;
				}
			}

			if (StringUtils.isNotBlank(cardOptionId) && linkInfo == null) {
				if (StringUtils.isNotBlank(optionDataKey) && apiResult.getApiResult(optionDataKey) == null) {
					apiResult.putApiResult(optionDataKey, data);
				}
				// 2018-08-31修改 李思穎，若沒有動態選項代入，要取預設所有optionDataKey的選項link
				Map apiResultDataMap = (Map) apiResult.getApiResult(botReplyContent_dataKey);
				if (apiResultDataMap != null && apiResultDataMap.containsKey(BotMessageSetting.REPLACE_OPTION_WITH_LINKID)) {
					if (StringUtils.isNotEmpty(optionDataKey)) {
						linkInfo = botLinkService.getReplaceOptionLinks(cardOptionId, data, content.getDataKey(), optionDataKey);
					} else {
						linkInfo = botLinkService.getReplaceOptionLinks(cardOptionId, apiResult, content.getDataKey());
					}
				} else {
					if (StringUtils.isNotEmpty(optionDataKey)) {
						if (data != null) {
							linkInfo = botLinkService.getOptionLinks(cardOptionId, data, optionDataKey);
						}
					} else {
						linkInfo = botLinkService.getOptionLinks(cardOptionId, apiResult);
					}
				}
			}

			if (linkInfo != null && linkInfo.length > 0) {
				lifeLinks.clear();
				for (LifeLink link : linkInfo) {
					lifeLinks.add(link);
				}
				card.setCLinkList(botMessageService.getCLinkList(botMessageVO, lifeLinks));
			}

			cardList.add(card.getCards());
		}
	}

	private void createCardText(boolean isTexts2, String templateId, BotMessageVO botMessageVO, APIResult apiResult, boolean noData,
			Map botCard_dataMap, boolean hasBotCard_dataMap, BotCardShowData botCardShowData, BotCardShowData botCardShowData_default,
			CardItem card) {

		String cacheKey = "botCardTextTemplates_" + templateId;

		List<BotCardTextTemplate> botCardTextTemplates = (List<BotCardTextTemplate>) LocalCache.get(cacheKey);
		if (botCardTextTemplates == null) {
			botCardTextTemplates = botCardTextTemplateDAO.findByCardTextId(templateId);

			if (botCardTextTemplates != null && !botCardTextTemplates.isEmpty()) {
				LocalCache.register(cacheKey, botCardTextTemplates);
			}
		}

		if (isInfo) {
			log.info("bot_card_text_template:" + botCardTextTemplates);
		}

		if (botCardTextTemplates != null && !botCardTextTemplates.isEmpty()) {

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			for (BotCardTextTemplate botCardTextTemplate : botCardTextTemplates) {
				if (noData) {
					botCardShowData = null;
					botCard_dataMap = null;
					hasBotCard_dataMap = false;

					String botCardTextTemplate_dataKey = botCardTextTemplate.getDataKey();
					if (StringUtils.isNotBlank(botCardTextTemplate_dataKey)) {
						Object obj = apiResult.getApiResult(botCardTextTemplate_dataKey);
						if (obj != null && obj instanceof Map && !((Map) obj).isEmpty()) {
							botCard_dataMap = (Map) obj;
							hasBotCard_dataMap = true;
							botCardShowData = (BotCardShowData) botCard_dataMap.get(BotCardShowData.name);

							if (isInfo) {
								log.info("botCardShowData >>> " + botCardShowData);
							}
						}
					}

					if (botCardShowData == null) {
						botCardShowData = botCardShowData_default;
					}
				}

				Set<BotCardTextTemplateId> botCardTexts_hide = botCardShowData.getBotCardText_hide();
				if (botCardTexts_hide.contains(botCardTextTemplate.getId())) {
					continue;
				}

				CTextItem cText = botMessageVO.new CTextItem();

				Integer cardTextStyle = botCardTextTemplate.getCardTextStyle();
				if (cardTextStyle != null) {
					cacheKey = "botCardTextStyle_" + cardTextStyle;

					BotCardTextStyle botCardTextStyle = (BotCardTextStyle) LocalCache.get(cacheKey);
					if (botCardTextStyle == null) {
						botCardTextStyle = botCardTextStyleDAO.findByStyleId(cardTextStyle);

						if (botCardTextStyle != null) {
							LocalCache.register(cacheKey, botCardTextStyle);
						}
					}

					if (isInfo) {
						log.info("bot_card_text_style:" + botCardTextStyle);
					}

					if (botCardTextStyle != null) {
						String textClass = TextAttribute.getTextClass(botCardTextStyle.getTextClass());
						if (StringUtils.isNotBlank(textClass)) {
							cText.setCTextClass(textClass);
						}
					}
				}

				String cardTextLabel = botCardTextTemplate.getCardTextLabel();
				if (botCardShowData.showBotCardTextTemplate_cardTextLabel() && StringUtils.isNotBlank(cardTextLabel)) {
					cText.setCLabel(getText(cardTextLabel, hasBotCard_dataMap, botCard_dataMap));
				}

				String text = botCardTextTemplate.getText();
				if (botCardShowData.showBotCardTextTemplate_text() && StringUtils.isNotBlank(text)) {
					cText.setCText(getText(text, hasBotCard_dataMap, botCard_dataMap));
				}

				String unit = botCardTextTemplate.getUnit();
				if (botCardShowData.showBotCardTextTemplate_unit() && StringUtils.isNotBlank(unit)) {
					cText.setCUnit(getText(unit, hasBotCard_dataMap, botCard_dataMap));
				}

				String amount = botCardTextTemplate.getAmount();
				if (botCardShowData.showBotCardTextTemplate_amount() && StringUtils.isNotBlank(amount)) {
					cText.setCAmount(getText(amount, hasBotCard_dataMap, botCard_dataMap));
				}

				cTextList.add(cText.getCTexts());
			}

			if (isTexts2) {
				card.setCTexts2(cTextList);
			} else {
				card.setCTexts(cTextList);
			}
		}
	}

	private static class CardAttribute {
		/**
		 * 寬度
		 * 
		 * @param width
		 * @return
		 */
		private static String getCWidth(short width) {
			switch (width) {
			case 200:
				return BotMessageVO.CWIDTH_200;
			default:
				return BotMessageVO.CWIDTH_250;
			}
		}

		/**
		 * 牌卡內容顯示格式
		 * 
		 * @param textType
		 * @return
		 */
		private static String getCTextType(short textType) {
			switch (textType) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				return String.valueOf(textType);
			default:
				return null;
			}
		}

		/**
		 * 連結顯示類型
		 * 
		 * @param linkType
		 * @return
		 */
		private static Integer getCLinkType(short linkType) {
			switch (linkType) {
			case 1:
			case 2:
				return Integer.valueOf(linkType);
			default:
				return null;
			}
		}
	}

	private static class ImageDataAttribute {
		/**
		 * 圖像文字顯示定位格式
		 * 
		 * @param imageTextType
		 * @return
		 */
		private static Integer getCImageTextType(short imageTextType) {
			switch (imageTextType) {
			case 1:
			case 2:
				return Integer.valueOf(imageTextType);
			default:
				return null;
			}
		}
	}

	private static class TextAttribute {
		/**
		 * (1+2)混合牌卡內容顯示類型
		 * 
		 * @param textClass
		 * @return
		 */
		private static String getTextClass(short textClass) {
			switch (textClass) {
			case 1:
			case 2:
				return String.valueOf(textClass);
			default:
				return null;
			}
		}
	}

	/**
	 * 取得文字
	 * 
	 * @param value
	 * @param hasData ApiResult是否有資料
	 * @param data
	 * @return
	 */
	private String getText(String value, boolean hasData, Map<String, String> data) {
		if (hasData) {
			return botMessageService.replaceTextByParamMap(value, data);
		}

		return value;
	}
}
