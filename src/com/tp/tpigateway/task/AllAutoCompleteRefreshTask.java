package com.tp.tpigateway.task;

import com.tp.tpigateway.service.common.AllAutoCompleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
@PropertySource(value = { "classpath:tpigateway.properties" })
@Transactional
public class AllAutoCompleteRefreshTask {

	@Autowired
	private AllAutoCompleteService allAutoCompleteService;
	
	@Scheduled(cron = "${allAutoComplete.refresh.cache.cron}")
	public void schRefreshAllAutoComplete() {
		allAutoCompleteService.refreshAllAutoComplete();
	}
}
