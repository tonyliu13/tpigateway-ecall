package com.tp.tpigateway.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.service.common.BotTpiReplyTextService;

/**
 * 排程處理 bot tpi 回覆清單 cache 更新
 */
@Component
@PropertySource(value = { "classpath:tpigateway.properties" })
@Transactional
public class BotTpiReplyTextRefreshTask {

	//private static Logger log = LoggerFactory.getLogger(BotTpiReplyTextRefreshTask.class);
	
	@Autowired
	private BotTpiReplyTextService botTpiReplyTextService;
	
	/**
	 * 排程更新 bot tpi 回覆清單 cache
	 */
	@Scheduled(cron = "${reply.refresh.cache.cron}")
	public void schRefreshReplyText() {
		botTpiReplyTextService.refreshReplyText();
	}
}
