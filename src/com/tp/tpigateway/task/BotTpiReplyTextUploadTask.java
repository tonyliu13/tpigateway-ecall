//package com.tp.tpigateway.task;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import org.apache.commons.codec.digest.DigestUtils;
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.commons.collections.MapUtils;
//import org.apache.commons.io.IOUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Profile;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.tp.tpigateway.dao.common.BotTpiReplyTextDao;
//import com.tp.tpigateway.model.common.BotTpiReplyText;
//import com.tp.tpigateway.util.PropUtils;
//import com.tp.tpigateway.util.ReadExcelUtils;
//
///**
// * 排程處理 bot tpi 回覆清單上傳更新
// * cluster server 有一台必須啟動設定(-Dspring.profiles.active=scheduled)
// */
//@Component
//@PropertySource(value = { "classpath:tpigateway.properties" })
//@Transactional
//@Profile({"scheduled"})
//public class BotTpiReplyTextUploadTask {
//
//	private static Logger log = LoggerFactory.getLogger(BotTpiReplyTextUploadTask.class);
//	
//	@Autowired
//	private BotTpiReplyTextDao botTpiReplyTextDao;
//	
//	private final static StringBuilder fileMD5 = new StringBuilder("");
//	
//	/**
//	 * 排程讀取 bot tpi 回覆清單 excel並寫入資料庫
//	 */
//	@Scheduled(cron = "${reply.update.excel.cron}")
//	public void schReadBotTpiReplyTextTask() {
//
//		log.info("Scheduling ReadBotTpiReplyTextTask start");
//		
//		List<BotTpiReplyText> botTpiReplyTexts = null;
//		
//		try {
//			// 讀取excel
//			String filepath = PropUtils.REPLY_EXCEL_PATH;
//			
//			String md5 = null;
//			FileInputStream fis = null;
//			try {
//				String innerPath = BotTpiReplyTextUploadTask.class.getResource("/" + filepath).getFile();
//				if (new File(innerPath).exists()) {
//					filepath = innerPath;
//				}
//				
//				log.debug("[schReadBotTpiReplyTextTask] filepath=" + filepath);
//				fis = new FileInputStream(filepath);
//				md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
//			} catch (Exception fex) {
//			} finally {
//				if (md5 == null) {
//					md5 = "";
//				}
//				
//				if (fis != null) {
//					IOUtils.closeQuietly(fis);
//				}
//			}
//			
//			// 調整為檢查完excel實際筆數再決定是否更新
////			if (StringUtils.isNotBlank(fileMD5.toString()) && StringUtils.equals(fileMD5.toString(), md5)) {
////				log.debug("wording is the same, not need upload.");
////				return;
////			}
//			
//			ReadExcelUtils excelReader = new ReadExcelUtils(filepath);
//			
//			// 角色清單, 數量及順序 與 excel 的 sheet 相對應
//			//String[] roleNames = StringUtils.split(PropUtils.REPLY_SHEET_ROLENAMES, ",");
//			
//			// <sheetId, <rowId, <columnId, wording>>>
//			Map<Integer, Map<Integer, Map<Integer, Object>>> results = new HashMap<Integer, Map<Integer, Map<Integer, Object>>>();
//			
//			// 開始讀取sheet資料
//			int count = PropUtils.REPLY_SHEET_COUNT;
//			for (int sheetId = 0; sheetId < count; sheetId++) {
//				try {
//					Map<Integer, Map<Integer, Object>> result = excelReader.readExcelContent(sheetId);
//					results.put(sheetId, result);
//				} catch (Exception e) {
//					log.error(e.getMessage(), e);
//				}
//			}
//			
//			// 原檢查excel file md5，再加強比對excel內容筆數
//			String resultSize = (MapUtils.isNotEmpty(results) && MapUtils.isNotEmpty(results.get(0))) ? results.get(0).size() + "" : "0";
//			if (StringUtils.isNotBlank(fileMD5.toString()) && StringUtils.equals(fileMD5.toString(), md5 + resultSize)) {
//				log.debug("wording is the same, not need upload.");
//				return;
//			}
//			
//			log.error("[schReadBotTpiReplyTextTask] results size=" + resultSize);
//			// 轉置為寫入資料庫的資料
//            int id = 1;
//			if (MapUtils.isNotEmpty(results)) {
//				botTpiReplyTexts = new ArrayList<BotTpiReplyText>();
//				for (Entry<Integer,Map<Integer,Map<Integer,Object>>> roles : results.entrySet()) {
//					for (Entry<Integer,Map<Integer,Object>> rows : roles.getValue().entrySet()) {
//						if (this.checkRowData(rows)) {
//							BotTpiReplyText replyText = new BotTpiReplyText();
//							replyText.setId(id++);
//							replyText.setNodeId((String) rows.getValue().get(0));
//							replyText.setRoleId((String) rows.getValue().get(1));
//							replyText.setTextId((String) rows.getValue().get(2));
//							replyText.setText((String) rows.getValue().get(3));
//							botTpiReplyTexts.add(replyText);
//						}
//					}
//				}
//			}
//			
//			// 清空並寫入資料庫
//			if (CollectionUtils.isNotEmpty(botTpiReplyTexts)) {
//				botTpiReplyTextDao.removeAll();
//				botTpiReplyTextDao.batchInsert(botTpiReplyTexts);
//			}
//			
//			fileMD5.setLength(0);
//			fileMD5.append(md5 + resultSize);
//		} catch (Exception e) {
//			log.error("ReadBotTpiReplyTextTask error:" + e.getMessage(), e);
//		}
//		
//		log.info("Scheduling ReadBotTpiReplyTextTask end");
//	}
//
//	private boolean checkRowData(Entry<Integer, Map<Integer, Object>> rows) {
//		boolean result = true;
//		rows.getKey();
//		/*
//		if (StringUtils.isBlank((String) rows.getValue().get(0))) {
//			log.error("rows.getKey()= " + rows.getKey() + ":ReadBotTpiReplyTextTask checkRowData error: nodeId is Empty" );
//			result = false;
//		}*/
//		if (StringUtils.isBlank((String) rows.getValue().get(1))) {
//			log.error("rows.getKey()= " + rows.getKey() +":ReadBotTpiReplyTextTask checkRowData error: roleId is Empty" );
//			result = false;
//		}
//		if (StringUtils.isBlank((String) rows.getValue().get(2))) {
//			log.error("rows.getKey()= " + rows.getKey() + ":ReadBotTpiReplyTextTask checkRowData error: textId is Empty" );
//			result = false;
//		}
//
//		return result;
//	}
//
//}
