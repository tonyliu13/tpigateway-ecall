package com.tp.tpigateway.task;

import com.tp.tpigateway.dao.common.AllAutoCompleteDao;
import com.tp.tpigateway.model.common.AllAutoComplete;
import com.tp.tpigateway.util.PropUtils;
import com.tp.tpigateway.util.ReadExcelUtils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Component
@PropertySource(value = {"classpath:tpigateway.properties"})
@Transactional
@Profile({"scheduled"})
public class AllAutoCompleteUploadTask {

    private static Logger log = LoggerFactory.getLogger(AllAutoCompleteUploadTask.class);

    @Autowired
    private AllAutoCompleteDao allAutoCompleteDao;
    
    private final static StringBuilder fileMD5 = new StringBuilder("");

    @Scheduled(cron = "${allAutoComplete.update.excel.cron}")
    public void schReadAllAutoCompleteTask() {

        log.info("Scheduling read AllAutoComplete task start");

        List<AllAutoComplete> allAutoCompleteList = null;
        try {

            // 讀取Excel
            String filepath = PropUtils.ALL_AUTO_COMPLETE_EXCEL_PATH;
            
			String md5 = null;
			FileInputStream fis = null;
			try {
				String innerPath = AllAutoCompleteUploadTask.class.getResource("/" + filepath).getFile();
				if (new File(innerPath).exists()) {
					filepath = innerPath;
				}

				log.debug("[schReadAllAutoCompleteTask] filepath=" + filepath);
				fis = new FileInputStream(filepath);
				md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
			} catch (Exception fex) {
			} finally {
				if (md5 == null) {
					md5 = "";
				}

				if (fis != null) {
					IOUtils.closeQuietly(fis);
				}
			}
			
            ReadExcelUtils excelReader = new ReadExcelUtils(filepath);

            Map<Integer, Map<Integer, Map<Integer, Object>>> results = new HashMap<>();

            // 開始讀取Excel資料
            int count = PropUtils.ALL_AUTO_COMPLETE_SHEET_COUNT;

            for (int sheetId = 0; sheetId < count; sheetId++) {
                try {
                    Map<Integer, Map<Integer, Object>> result = excelReader.readExcelContent(sheetId);
                    results.put(sheetId, result);
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
            
            // 原檢查excel file md5，再加強比對excel內容筆數
 			String resultSize = (MapUtils.isNotEmpty(results) && MapUtils.isNotEmpty(results.get(0))) ? results.get(0).size() + "" : "0";
			if (StringUtils.isNotBlank(fileMD5.toString())
					&& StringUtils.equals(fileMD5.toString(), md5 + resultSize)) {
				log.debug("auto complete excel is the same, not need upload.");
				return;
			}
         		
 			log.error("[schReadBotTpiReplyTextTask] results size=" + resultSize);
            // 轉置為寫入資料庫的資料
            if (MapUtils.isNotEmpty(results)) {
                allAutoCompleteList = new ArrayList<>();
                String display_string, hidden_string;
                Integer string_Order;
                int id = 1;
                for (Entry<Integer, Map<Integer, Map<Integer, Object>>> roles : results.entrySet()) {
                    for (Entry<Integer, Map<Integer, Object>> rows : roles.getValue().entrySet()) {
                        try {
                            if (this.checkRowData(rows)) {
                                display_string = getRowValue(rows, 0);
                                string_Order = MapUtils.getInteger(rows.getValue(), 1);
                                hidden_string = getRowValue(rows, 2);

                                AllAutoComplete allAutoComplete = new AllAutoComplete();
                                allAutoComplete.setId(id++);
                                allAutoComplete.setDisplayString(display_string);
                                if (string_Order != null) {
                                    allAutoComplete.setStringOrder(string_Order);
                                } else {
                                    allAutoComplete.setStringOrder(999);
                                }
                                if (StringUtils.isNotEmpty(hidden_string)) {
                                    allAutoComplete.setHiddenString(hidden_string);
                                } else {
                                    allAutoComplete.setHiddenString(display_string);
                                }
                                allAutoCompleteList.add(allAutoComplete);
                            }
                        } catch (Exception e) {
                            log.error(e.getMessage());
                            log.debug("", e);
                        }
                    }
                }
            }
            // 清空並寫入資料庫
            if (CollectionUtils.isNotEmpty(allAutoCompleteList)) {
                allAutoCompleteDao.removeAll();
                allAutoCompleteDao.batchInsert(allAutoCompleteList);
            }
            
            fileMD5.setLength(0);
			fileMD5.append(md5 + resultSize);
        } catch (Exception e) {
            log.error("Read AllAutoComplete task error:" + e.getMessage());
        }
        log.info("Scheduling read AllAutoComplete task end");
    }

    private boolean checkRowData(Entry<Integer, Map<Integer, Object>> rows) {
        boolean result = true;
        try {
            // Just verify PK
            if (StringUtils.isBlank((String) rows.getValue().get(0))) {
                result = false;
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    private String getRowValue(Entry<Integer, Map<Integer, Object>> rows, int index) {
        String str = null;
        try {
            str = (String) rows.getValue().get(index);
        } catch (Exception e) {
        } finally {
            if (str == null) {
                str = "";
            }
        }
        return str;
    }
}
