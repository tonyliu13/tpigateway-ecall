package com.tp.tpigateway.util.exception;

import static com.tp.tpigateway.util.exception.StatusType.LIFE_API;

public interface LifeApiStatusConstants {
	
    StatusCode HTTP_STATUS_ERROR = new StatusCode(LIFE_API, "0001", "statusCode = {0}");
	
    StatusCode RETURN_CODE_ERROR = new StatusCode(LIFE_API, "0002", "returnCode = {0}");
    
    StatusCode RETURN_BODY_IS_EMPTY = new StatusCode(LIFE_API, "0003", "URL = {0} : return body is empty");
    
    StatusCode NO_DATA_FOUND = new StatusCode(LIFE_API, "0004", "no data found");
    
    StatusCode NOT_OWN_POLICYNO = new StatusCode(LIFE_API, "0005", "not own policyNo");
}
