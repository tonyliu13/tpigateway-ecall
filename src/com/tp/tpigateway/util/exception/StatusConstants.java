package com.tp.tpigateway.util.exception;

import static com.tp.tpigateway.util.exception.StatusType.COMMON;

public interface StatusConstants {
    StatusCode OK = new StatusCode("0", "OK.");
    StatusCode UNEXPECTED_EXCEPTION = new StatusCode(COMMON, "0001", "Unexpected exception.");
    
    StatusCode BOT_CHAT_LOG_NOT_FOUND = new StatusCode(COMMON, "0002", "The BotChatLog {0} is not found.");
}
