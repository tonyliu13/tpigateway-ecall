package com.tp.tpigateway.util.exception;

public enum StatusType {
    COMMON(10), LIFE_API(11), FLOW_CALL_LOG(12);
    
    private int value;
    
    private StatusType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
