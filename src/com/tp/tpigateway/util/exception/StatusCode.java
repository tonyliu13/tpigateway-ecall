package com.tp.tpigateway.util.exception;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class StatusCode implements Serializable {
    private static final long serialVersionUID = -8233232483342499951L;

    private String code;
    private String message;

    public StatusCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public StatusCode(StatusType statusType, String code, String message) {
        this.code = statusType.getValue() + code;
        this.message = message;
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof StatusCode)) {
            return false;
        }
        StatusCode otherStatusCode = (StatusCode) obj;

        EqualsBuilder builder = new EqualsBuilder();
        builder.append(getCode(), otherStatusCode.getCode());
        builder.append(getMessage(), otherStatusCode.getMessage());
        return builder.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getCode());
        builder.append(getMessage());
        return builder.hashCode();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("code:").append(getCode()).append(", message:")
                .append(getMessage()).toString();
    }
}
