package com.tp.tpigateway.util;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * properties 共用工具類
 */
public class PropUtils {

	private static final Logger log = LoggerFactory.getLogger(PropUtils.class);
	
	// 相關properties設定來源
	private static final String TPIGATEWAY_PROPERTIES = "tpigateway.properties";
	private static final String APPLICATION_PROPERTIES = "application.properties";
		
	private static Map<String, String> propMap = new ConcurrentHashMap<String, String>();
	
	// 系統參數
	public final static String SYSTEM_ACTIVE = StringUtils.defaultString(System.getProperty("system.active"), "");
	//public final static String SYSTEM_SOURCE = getProperty("system.source");
	public final static String SYSTEM_SOURCE = "cathaylife";
	public final static String SYSTEM_ID = UUID.randomUUID().toString();

	public final static String APP_ID = getProperty("app.id");

	/** API認證用key for kitt */
	public final static String KITT_KEY = getApiKey("kitt");
	public static long TOKEN_EXPIRE_TIME = NumberUtils.toLong(getProperty("token.expire.time")); // millisecond
	
	/** 限制連入ip */
	public static String SYSTEM_RUN_IP = getProperty("system.run.ip");
	
	public static String APIM_INTRA_BANK = "013";
	/** POC用gateway參數 end */
	
	public static int OK_CONNECT_TIMEOUT = NumberUtils.toInt(getProperty("connect.timeout"), 10);
	public static int OK_READ_TIMEOUT = NumberUtils.toInt(getProperty("read.timeout"), 10);
	public static int OK_WRITE_TIMEOUT = NumberUtils.toInt(getProperty("write.timeout"), 10);
	
	/** 測試環境加強顯示api錯誤原因 for 阿發進修 */
	public static boolean CALL_API_ERROR_SHOW_INFO = BooleanUtils.toBoolean(getProperty("call.api.error.show.info"));
	
	/** Gateway組成回覆用wording相關設定 */
	public static int REPLY_SHEET_COUNT = NumberUtils.toInt(getProperty("reply.sheet.count"), 1);
	public static String REPLY_SHEET_ROLENAMES = getProperty("reply.sheet.rolenames");
	public static String REPLY_EXCEL_PATH = getProperty("reply.excel.path");
	public static String REPLY_UPDATE_EXCEL_CRON = getProperty("reply.update.excel.cron");
	public static String REPLY_REFRESH_CACHE_CRON = getProperty("reply.refresh.cache.cron");

	public static int ALL_AUTO_COMPLETE_SHEET_COUNT = NumberUtils.toInt(getProperty("allAutoComplete.sheet.count"), 1);
	public static String ALL_AUTO_COMPLETE_EXCEL_PATH = getProperty("allAutoComplete.excel.path");
	public static String ALL_AUTO_COMPLETE_UPDATE_EXCEL_CRON = getProperty("allAutoComplete.update.excel.cron");
	public static String ALL_AUTO_COMPLETE_REFRESH_CACHE_CRON = getProperty("allAutoComplete.refresh.cache.cron");

	public static int BANKCODE_SHEET_COUNT = NumberUtils.toInt(getProperty("bankcode.sheet.count"), 1);
	public static String BANKCODE_SHEET_ROLENAMES = getProperty("bankcode.sheet.rolenames");
	public static String BANKCODE_EXCEL_PATH = getProperty("bankcode.excel.path");
	public static String BANKCODE_UPDATE_EXCEL_CRON = getProperty("bankcode.update.excel.cron");
	public static String BANKCODE_REFRESH_CACHE_CRON = getProperty("bankcode.refresh.cache.cron");
	
	public static int BANKERRORCODE_SHEET_COUNT = NumberUtils.toInt(getProperty("bankerrorcode.sheet.count"), 1);
	public static String BANKERRORCODE_SHEET_ROLENAMES = getProperty("bankerrorcode.sheet.rolenames");
	public static String BANKERRORCODE_EXCEL_PATH = getProperty("bankerrorcode.excel.path");
	public static String BANKERRORCODE_UPDATE_EXCEL_CRON = getProperty("bankerrorcode.update.excel.cron");
	public static String BANKERRORCODE_REFRESH_CACHE_CRON = getProperty("bankerrorcode.refresh.cache.cron");
	
	/** Hibernate setting */
	public static int HIBERNATE_JDBC_BATCH_SIZE = NumberUtils.toInt(getProperty("hibernate.jdbc.batch_size"), 1);
	
	public static boolean CATHAYLIFE_PASS_G03 = BooleanUtils.toBoolean(getProperty("cathaylife.pass.g03"));
	
	public static String CATHAYLIFE_A04_URL = "cathaylife.a04";
	public static String CATHAYLIFE_A05_URL = "cathaylife.a05";
	public static String CATHAYLIFE_A12_URL = "cathaylife.a12";
	public static String CATHAYLIFE_A20_URL = "cathaylife.a20";
	public static String CATHAYLIFE_A21_URL = "cathaylife.a21";
	public static String CATHAYLIFE_A22_URL = "cathaylife.a22";
	public static String CATHAYLIFE_A23_URL = "cathaylife.a23";
	public static String CATHAYLIFE_A24_URL = "cathaylife.a24";
	public static String CATHAYLIFE_A29_URL = "cathaylife.a29";
	public static String CATHAYLIFE_A25_URL = "cathaylife.a25";
	public static String CATHAYLIFE_A27_URL = "cathaylife.a27";
	public static String CATHAYLIFE_A17_URL = "cathaylife.a17";
	public static String CATHAYLIFE_A13_URL = "cathaylife.a13";
	public static String CATHAYLIFE_A18_URL = "cathaylife.a18";
	public static String CATHAYLIFE_A14_URL = "cathaylife.a14";
	public static String CATHAYLIFE_A19_URL = "cathaylife.a19";
	public static String CATHAYLIFE_A11_URL = "cathaylife.a11";
	public static String CATHAYLIFE_A15_URL = "cathaylife.a15";
	public static String CATHAYLIFE_A30_URL = "cathaylife.a30";
	public static String CATHAYLIFE_A31_URL = "cathaylife.a31";
	public static String CATHAYLIFE_A32_URL = "cathaylife.a32";
	public static String CATHAYLIFE_A33_URL = "cathaylife.a33";
	
	public static String CATHAYLIFE_C01_URL = "cathaylife.c01";
	public static String CATHAYLIFE_C02_URL = "cathaylife.c02";
	public static String CATHAYLIFE_C03_URL = "cathaylife.c03";
	public static String CATHAYLIFE_C04_URL = "cathaylife.c04";
	
	public static String CATHAYLIFE_D01_URL = "cathaylife.d01";
	public static String CATHAYLIFE_D05_URL = "cathaylife.d05";
	public static String CATHAYLIFE_D02_URL = "cathaylife.d02";
	public static String CATHAYLIFE_D06_URL = "cathaylife.d06";
	public static String CATHAYLIFE_D07_URL = "cathaylife.d07";
	public static String CATHAYLIFE_D08_URL = "cathaylife.d08";
	public static String CATHAYLIFE_D09_URL = "cathaylife.d09";
	public static String CATHAYLIFE_D11_URL = "cathaylife.d11";
	public static String CATHAYLIFE_D12_URL = "cathaylife.d12";
	public static String CATHAYLIFE_D13_URL = "cathaylife.d13";
	public static String CATHAYLIFE_D14_URL = "cathaylife.d14";
	public static String CATHAYLIFE_D15_URL = "cathaylife.d15";
	public static String CATHAYLIFE_D16_URL = "cathaylife.d16";
	public static String CATHAYLIFE_D18_URL = "cathaylife.d18";
	public static String CATHAYLIFE_D19_URL = "cathaylife.d19";
	public static String CATHAYLIFE_D20_URL = "cathaylife.d20";
	public static String CATHAYLIFE_D21_URL = "cathaylife.d21";
	public static String CATHAYLIFE_D22_URL = "cathaylife.d22";
	public static String CATHAYLIFE_D25_URL = "cathaylife.d25";
	public static String CATHAYLIFE_D26_URL = "cathaylife.d26";
	public static String CATHAYLIFE_D27_URL = "cathaylife.d27";
	
	public static String CATHAYLIFE_E01_URL = "cathaylife.e01";
	public static String CATHAYLIFE_E02_URL = "cathaylife.e02";
	public static String CATHAYLIFE_E07_URL = "cathaylife.e07";
	public static String CATHAYLIFE_E08_URL = "cathaylife.e08";

    public static String CATHAYLIFE_G01_URL = "cathaylife.g01";
    public static String CATHAYLIFE_G02_URL = "cathaylife.g02";
    public static String CATHAYLIFE_G03_URL = "cathaylife.g03";
    public static String CATHAYLIFE_G04_URL = "cathaylife.g04";
    public static String CATHAYLIFE_G05_URL = "cathaylife.g05";
    public static String CATHAYLIFE_G06_URL = "cathaylife.g06";
    public static String CATHAYLIFE_G07_URL = "cathaylife.g07";
    public static String CATHAYLIFE_G08_URL = "cathaylife.g08";

	public static String CATHAYLIFE_H01_URL = "cathaylife.h01";
	public static String CATHAYLIFE_H02_URL = "cathaylife.h02";
	public static String CATHAYLIFE_H03_URL = "cathaylife.h03";
	public static String CATHAYLIFE_H05_URL = "cathaylife.h05";
	public static String CATHAYLIFE_H06_URL = "cathaylife.h06";
	public static String CATHAYLIFE_H07_URL = "cathaylife.h07";

	public static String CATHAYLIFE_I01_URL = "cathaylife.i01";
	public static String CATHAYLIFE_I02_URL = "cathaylife.i02";

	public static String CATHAYLIFE_J01_URL = "cathaylife.j01";
	public static String CATHAYLIFE_J02_URL = "cathaylife.j02";
	public static String CATHAYLIFE_J03_URL = "cathaylife.j03";
	public static String CATHAYLIFE_J04_URL = "cathaylife.j04";
	public static String CATHAYLIFE_J05_URL = "cathaylife.j05";
	
	public static String CATHAYLIFE_S01_URL = "cathaylife.s01";
	public static String CATHAYLIFE_S02_URL = "cathaylife.s02";
	public static String CATHAYLIFE_S03_URL = "cathaylife.s03";
	public static String CATHAYLIFE_S04_URL = "cathaylife.s04";
	public static String CATHAYLIFE_S06_URL = "cathaylife.s06";
	public static String CATHAYLIFE_S07_URL = "cathaylife.s07";
	public static String CATHAYLIFE_S08_URL = "cathaylife.s08";
	
	public static String CATHAYLIFE_K01_URL = "cathaylife.k01";	
	public static String CATHAYLIFE_K02_URL = "cathaylife.k02";
	public static String CATHAYLIFE_K03_URL = "cathaylife.k03";
	public static String CATHAYLIFE_K04_URL = "cathaylife.k04";
	public static String CATHAYLIFE_K05_URL = "cathaylife.k05";
	public static String CATHAYLIFE_K06_URL = "cathaylife.k06";
	
	public static String CATHAYLIFE_Z01_URL = "cathaylife.z01";
	public static String CATHAYLIFE_Z02_URL = "cathaylife.z02";
	public static String CATHAYLIFE_Z04_URL = "cathaylife.z04";
	public static String CATHAYLIFE_Z05_URL = "cathaylife.z05";
	
	public static String CATHAYLIFE_ECALLQUEUEINFO_URL = "cathaylife.eCallQueueInfo";
	
	public static String CATHAYLIFE_DOWNLOAD_FILE_URL = getProperty("cathaylife.download.file.url");

	public static String MAIL_SUBJECT = getProperty("mail.subject");
	public static String MAIL_BODY = getProperty("mail.body");
	
	public static String CATHAYLIFE_MONGODB_URL = getProperty("cathaylife.mongodb.url");
	
	public final static String RESULT_CODE_SUCCESS = "1";
	public final static String RESULT_CODE_FAIL = "0";
	
	public final static String RESULT_DESCRIPTION_SUCCESS = "成功";
	public final static String RESULT_DESCRIPTION_SYSTEM_ERROR = "系統錯誤";
	public final static String RESULT_DESCRIPTION_PARAMETER_ERROR = "參數錯誤";
	public final static String RESULT_DESCRIPTION_TOKEN_ERROR = "permission denied(Token error)";
	public final static String RESULT_DESCRIPTION_TOKEN_OVERDUE = "permission denied(Token overdue)";
	
	public final static String CALL_LIFE_API_ERROR = getProperty("call.life.api.error");
	public final static String CALL_LIFE_API_ERROR2 = getProperty("call.life.api.error2");
	
	public final static String GOOGLE_MAP_SEARCH_PLACE_URL = getProperty("google.map.search.place.url");
	
	//S052.b 客服電話
	public final static String CUSTOMER_PHONE = PropUtils.getProperty("cathaylife.customer.phone");

	public final static String CUSTOMER_URL = PropUtils.getProperty("cathaylife.customer.url");

	/** 檔案上傳設定 */
	public final static long MULTIPART_MAX_FILE_SIZE = NumberUtils.toLong(getProperty("multipart.maxFileSize"));
	
	public static String getApiKey(String source) {
		return getProperty("apikey." + source);
	}

	/*session_stage*/
	public final static String IDLE_STATUS_SCORE = getProperty("idle.status.score");
	public final static String IDLE_STATUS_50 = getProperty("idle.status.50");
	public final static String IDLE_STATUS_80 = getProperty("idle.status.80");

	public final static int DEFAULT_SESSION_UPD_TIME = NumberUtils.toInt(getProperty("default.session.upd.min"), 20);

	/**
     * 取得一般參數
     * @param key
     * @return
     */
    public static String getProperty(String key) {
    	
    	if (propMap.isEmpty()) {
    		synchronized(propMap) {
    			if (propMap.isEmpty()) {
    				readProperties();
    			}
    		}
    	}
    	
    	return propMap.get(key);
    }
    
    /**
     * 讀取 properties
     */
    public static void readProperties() {
    	Properties prop = new Properties();
    	
    	try {
    		Map<String, String> tempMap = new ConcurrentHashMap<String, String>();
    		prop.load(PropUtils.class.getResourceAsStream("/" + TPIGATEWAY_PROPERTIES));
    		for (Entry<Object, Object> entry : prop.entrySet()) {
    			String key = (String) entry.getKey();
    			String value = (String) entry.getValue();
    			tempMap.put(key, value);
    		}
    		prop.load(PropUtils.class.getResourceAsStream("/" + APPLICATION_PROPERTIES));
    		for (Entry<Object, Object> entry : prop.entrySet()) {
    			String key = (String) entry.getKey();
    			String value = (String) entry.getValue();
    			tempMap.put(key, value);
    		}
    		final String active = StringUtils.defaultIfBlank(SYSTEM_ACTIVE, tempMap.get("system.active"));
    		if (StringUtils.isNotBlank(active)) {
    			InputStream is = null;
    			final String activeTPIGatewayFileName = "tpigateway-"+active+".properties";
    			is = PropUtils.class.getResourceAsStream("/" + activeTPIGatewayFileName);
    			if (is != null) {
    				prop.load(is);
    				for (Entry<Object, Object> entry : prop.entrySet()) {
    					String key = (String) entry.getKey();
    					String value = (String) entry.getValue();
    					tempMap.put(key, value);
    				}
    			} else {
    				log.warn(activeTPIGatewayFileName + " is not exist.");
    			}
    			is = null;
    			final String activeApplicationFileName = "application-"+active+".properties";
    			is = PropUtils.class.getResourceAsStream("/" + activeApplicationFileName);
    			if (is != null) {
    				prop.load(is);
    				for (Entry<Object, Object> entry : prop.entrySet()) {
    					String key = (String) entry.getKey();
    					String value = (String) entry.getValue();
    					tempMap.put(key, value);
    				}
    			} else {
    				log.warn(activeApplicationFileName + " is not exist.");
    			}
    		}
    		
    		propMap.clear();
    		propMap.putAll(tempMap);
    	} catch (IOException ex) {
    		log.error(ex.getMessage());
    	}
    }
}
