package com.tp.tpigateway.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Http(SSL)
 */
public class DummySecureProtocolSocketFactory {
	
	private static Logger log = LoggerFactory.getLogger(DummySecureProtocolSocketFactory.class);
	
    private static SSLContext sslcontext = null;
   
    /**
     * SSLContext
     * @return SSLContext
     */
    private static SSLContext createSSLContext() {
        SSLContext sslcontext = null;
        try {
            sslcontext = SSLContext.getInstance("TLSv1.2");
            sslcontext.init(null,  new TrustManager[] { new TrustAllCerts() }, new SecureRandom());
        } catch (NoSuchAlgorithmException e) {
        	log.error(e.getMessage());
        } catch (KeyManagementException e) {
        	log.error(e.getMessage());
        }
        return sslcontext;
    }
   
    private static SSLContext getSSLContext() {
        if (sslcontext == null) {
            sslcontext = createSSLContext();
        }
        return sslcontext;
    }
   
    public static class TrustAllCerts implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {return new X509Certificate[0];}
    }

    public static class TrustAllHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    public static SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory ssfFactory = null;

        try {
            SSLContext sc = getSSLContext();
            ssfFactory = sc.getSocketFactory();
        } catch (Exception e) {
        }

        return ssfFactory;
    }
}
