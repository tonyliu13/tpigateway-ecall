package com.tp.tpigateway.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.collections.MapUtils;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class XmlUtil {

	private static Logger log = LoggerFactory.getLogger(XmlUtil.class);
	
	public HashMap XMLToMap(String xml) throws Exception{
		int a = xml.indexOf("<?xml");
		int b = xml.indexOf("?>");
		if(a>-1 && b>-1){
			xml = "<?xml version=\"1.0\" encoding=\"BIG5\" ?>"+xml.substring(b+2);
		}
		log.debug("xml=" + xml);
//		xml = this.fullToHalf(xml);
		HashMap map = new HashMap();
		InputStream in = new ByteArrayInputStream(xml.getBytes("BIG5"));
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(in);
		Element root = doc.getRootElement();
		this.elementToMap(root.getChildren(), map);
//		this.attributeToMap(root.getChildren(), map);
		return settingMap(map);
	}
	private HashMap settingMap(HashMap map) {
		HashMap newMap = new HashMap();
		for (Object key : map.keySet()) {
			log.trace("key="+key.toString());
			if (map.containsKey(key)) {
				if (map.get(key) instanceof Map) {
					newMap.putAll((Map) map.get(key));
				} else {
					newMap.put(key, map.get(key));
				}
			}
		}
		return newMap;
	}
	private void elementToMap(List list, HashMap map) {
		for(int i=0; i<list.size(); i++){
			Element emt = (Element)list.get(i);
			if(emt.getTextTrim()!=null && emt.getTextTrim().length()>0){
				map.put(emt.getName(), this.replaceChara(emt.getTextTrim()));
			}
			List listChildren = emt.getChildren();
			if(listChildren.size()>0){
//				this.elementToMap(listChildren, map);
				HashMap tempMap = new HashMap();
				this.elementToMap(listChildren, tempMap);
				if (StringUtils.equals("Record", emt.getName())) {
					if (MapUtils.getObject(map, emt.getName()) == null) {
						map.put(emt.getName(), new ArrayList());
					}
					((List) MapUtils.getObject(map, emt.getName())).add(tempMap);
				} else {
					map.put(emt.getName(), tempMap);
				}
			}
		}
	}
	private void attributeToMap(List list, HashMap map) throws Exception{
		StringBuilder name = new StringBuilder("");
		StringBuilder value = new StringBuilder("");
		for(int i=0; i<list.size(); i++){
			Element emt = (Element)list.get(i);
			List attributeList = emt.getAttributes();
			if(attributeList.size()>0 && emt.getChildren().size()>0){
				Attribute attribute = (Attribute)attributeList.get(0);
				value.append(attribute.getValue());
				ArrayList subList = new ArrayList();
				map.put(value, subList);
				this.subattributeToMap(emt.getChildren(), subList);
			}else{
				for(int j=0; j<attributeList.size(); j++){
					Attribute attribute = (Attribute)attributeList.get(j);
					name.append(attribute.getName());
					value.append(attribute.getValue());
				}
				if(value.length() != 0){
					map.put(value,  this.replaceChara(emt.getTextTrim()));
				}
				List listChildren = emt.getChildren();
				if(listChildren.size()>0){
					this.attributeToMap(listChildren, map);
				}
			}
			value.setLength(0);
		}
	}
	private void subattributeToMap(List list, List subList) throws Exception{
		StringBuilder value = new StringBuilder("");
		for(int i=0; i<list.size(); i++){
			HashMap subMap = new HashMap();
			Element emt = (Element)list.get(i);
			List attributeList = emt.getAttributes();
			
			for(int j=0; j<attributeList.size(); j++){
				Attribute attribute = (Attribute)attributeList.get(j);
				value.append(attribute.getValue());
			}
			if(value.length() != 0){
				subMap.put(value,  this.replaceChara(emt.getTextTrim()));
			}
			List listChildren = emt.getChildren();
			if(listChildren.size()>0){
				this.attributeToMap(listChildren, subMap);
			}
			subList.add(subMap);
			value.setLength(0);
		}
	}
	public String replaceChara(String text){
		text = text.replaceAll("&amp;", "&");
		text = text.replaceAll("&lt;", "<");
		text = text.replaceAll("&gt;", ">");
		return text;
	}
}
