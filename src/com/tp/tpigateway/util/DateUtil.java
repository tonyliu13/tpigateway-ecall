package com.tp.tpigateway.util;


import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;


/**
 * 日期公用工具
 */
public final class DateUtil {

	public static final String[] MONTH_TW = {"01", "02", "03", "04", "05",
		"06", "07", "08", "09", "10", "11", "12"};
	public static final String[] NUMBER_TW = {"一", "二", "三", "四", "五", "六",
		"七", "八", "九"};
	public static final String[] DIGIT_TW = {"", "十", "百", "千", "萬"};
	public static final String SYMBOL_SLASH = "/";
	public static final String SYMBOL_DASH = "-";
	public static final String PATTERN_DATE = "yyyyMMdd";
	public static final String PATTERN_DATE_TIME = "yyyyMMdd HH:mm:ss";
	public static final String PATTERN_DATE_TIME_2 = "yyyyMMddHHmmss";
	public static final String PATTERN_SLASH_DATE = "yyyy/MM/dd";
	public static final String PATTERN_TW_DATE = "yyyy年MM月dd日";
	public static final String PATTERN_SLASH_DATE_TIME = "yyyy/MM/dd HH:mm:ss";
	public static final String PATTERN_DASH_DATE = "yyyy-MM-dd";
	public static final String PATTERN_DASH_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final String PATTERN_DASH_DATE_TIME_A = "yyyy/MM/dd a hh:mm:ss";
	public static final String PATTERN_TIME = "HH:mm:ss";
	public static final String PATTERN_TIME_2 = "HH:mm";
	public static final String PATTERN_DASH_DATE_TIME_WITH_MS = "yyyy-MM-dd HH:mm:ss.SSS";

	/**
	 * 將數字字串轉成中文數字(99→九十九)
	 *
	 * @param num
	 * @return
	 */
	public static String formatTwNumber(String num) {
		if (num == null) {
			return "";
		}
		return formatTwNumber(Integer.parseInt(num));
	}

	/**
	 * 將數字轉成中文數字(99→九十九)
	 *
	 * @param num
	 * @return
	 */
	public static String formatTwNumber(Integer num) {
		if (num == null) {
			return "";
		}

		String twNum = num.toString();
		StringBuffer buffer = new StringBuffer();
		// 位數>0
		if (twNum.length() > 0) {
			int b = 0; // startIndex
			int e = 1; // endIndex
			Integer temp;
			while (twNum.length() >= e) { // endIndex不超過數字長度時
				temp = Integer.parseInt(twNum.substring(b, e));
				buffer.append(NUMBER_TW[temp - 1]);
				buffer.append(DIGIT_TW[twNum.substring(b, twNum.length())
						.length() - 1]);
				b++;
				e++;
			}
		}

		return buffer.toString();
	}

	/**
	 * 將民國年轉成西元年
	 *
	 * @param taiwanYear
	 * @return
	 */
	public static int getYear(String taiwanYear) {
		if (StringUtils.isBlank(taiwanYear)) {
			throw new IllegalArgumentException(
					"taiwanYear can not null or empty.");
		}

		return Integer.valueOf(taiwanYear).intValue() + 1911;
	}

	/**
	 * 依民國年月取得該月份的第一天(2009-01-01 00:00:00)
	 *
	 * @param taiwanYear
	 * @param month
	 * @return
	 */
	public static Date getFirstDayOfMonth(String taiwanYear, String month) {
		if (StringUtils.isBlank(taiwanYear)) {
			throw new IllegalArgumentException(
					"taiwanYear can not null or empty.");
		} else if (StringUtils.isBlank(month)) {
			throw new IllegalArgumentException("month can not null or empty.");
		}

		return getDate(getYear(taiwanYear), Integer.valueOf(month), 1);
	}

	/**
	 * 依民國年月取得該月份的最後一天(2009-01-31 23:59:59)
	 *
	 * @return
	 */
	@SuppressWarnings("static-access")
	public static Date getLastDayOfMonth(String taiwanYear, String month) {
		if (StringUtils.isBlank(taiwanYear)) {
			throw new IllegalArgumentException(
					"taiwanYear can not null or empty.");
		} else if (StringUtils.isBlank(month)) {
			throw new IllegalArgumentException("month can not null or empty.");
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getFirstDayOfMonth(taiwanYear, month));
		int dayOfMonth = calendar.get(calendar.DAY_OF_MONTH);
		int monthMaxDay = calendar.getActualMaximum(calendar.DAY_OF_MONTH);
		calendar.add(calendar.DATE, monthMaxDay - dayOfMonth);

		return getDate(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
	}

	/**
	 * 取得目前月份的第一天。
	 *
	 * @return
	 */
	public static Date getFirstDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, 1);
		return calendar.getTime();
	}

	/**
	 * 依西元年月取得該月份的第一天(2009-01-01 00:00:00)
	 *
	 * @param year
	 * @param month
	 * @return
	 */
	public static Date getFirstDayOfMonth(int year, int month) {
		return getDateOfStart(getDate(year, month, 1));
	}
	
	/**
	 * 依西元年月取得該月份的最後一天 (2009-01-31 23:59:59)
	 *
	 * @param year
	 * @param month
	 * @return
	 */
	public static Date getLastDayOfMonth(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getDate(year, month, 1));
		calendar.set(Calendar.DATE,
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return getDateOfEnd(calendar.getTime());
	}

	/**
	 * 取得月份最後最一天，例如:28、29、30、31
	 *
	 * @param date
	 * @return
	 */
	public static int getMaxDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 取得日期的起始時間 2009-01-01 00:00:00
	 *
	 * @param date
	 * @return
	 */
	public static Date getDateOfStart(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("date can not null.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return getDate(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
	}
	
	/**
	 * 取得該日期月份的1號 (不含時間)2009-01-01 00:00:00
	 *
	 * @param date
	 * @return
	 */
	public static Date getDateOfFirstMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("date can not null.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return getDate(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH) + 1,
				1, 0, 0, 0);
	}

	/**
	 * 取得日期的結束時間 2009-01-01 23:59:59
	 *
	 * @param date
	 * @return
	 */
	public static Date getDateOfEnd(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("date can not null.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return getDate(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
	}

	/**
	 * 取得指定日期的民國年
	 *
	 * @param date
	 * @return 99
	 */
	public static int getTwYear(Date date) {
		return DateUtil.getYear(date) - 1911;
	}

	/**
	 * 取得指定日期的民國年
	 *
	 * @param date
	 * @return 099
	 */
	public static String getTWYearPattern(Date date) {
		return DateUtil.getTWYearPattern(DateUtil.getTwYear(date));
	}

	/**
	 * 取得 指定日期的日歷年
	 *
	 * @param date
	 * @return 2010
	 */
	public static int getYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * 取得 指定日期的月份
	 *
	 * @param date
	 * @return 9
	 */
	public static int getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * 取得 指定日期的月份
	 *
	 * @param date
	 * @return 09
	 */
	public static String getMonthPattern(Date date) {
		return TPIStringUtil.numberFormat(getMonth(date), "00");
	}

	/**
	 * 取得 指定日期的天
	 *
	 * @param date
	 * @return
	 */
	public static int getDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 取得今日日期(午夜零時的時間)
	 *
	 * @return
	 */
	public static Date getToday() {
		Calendar dateTime = Calendar.getInstance();
		Calendar date = new GregorianCalendar(dateTime.get(Calendar.YEAR),
				dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DATE));
		return date.getTime();
	}

	/**
	 * 取得目前的時間
	 *
	 * @return
	 */
	public static Date getNow() {
		return new Date();
	}

	/**
	 * 取得僅含有"年月日"的日期物件
	 *
	 * @param date
	 */
	public static Date getDate(Date date) {
		Calendar calendarDateTime = Calendar.getInstance();
		calendarDateTime.setTime(date);
		Calendar calendarDate = new GregorianCalendar(
				calendarDateTime.get(Calendar.YEAR),
				calendarDateTime.get(Calendar.MONTH),
				calendarDateTime.get(Calendar.DATE));
		return calendarDate.getTime();
	}

	/**
	 * 根據指定的 參數產生 {@link Date} 實體
	 *
	 * @param year 年份
	 * @param month 月份, 1-12
	 * @param day 日期, 1-31
	 * @return 指定的時間 {@link Date} 實體
	 */
	public static Date getDate(int year, int month, int day) {
		return getDate(year, month, day, 0, 0, 0);
	}

	/**
	 * 根據指定的 參數產生 {@link Date} 實體
	 *
	 * @param year 年份
	 * @param month 月份, 1-12
	 * @param day 日期, 1-31
	 * @param hour 小時，0-23
	 * @param minute 分鐘 0-59
	 * @param second 秒 0-59
	 * @return 指定的時間 {@link Date} 實體
	 */
	public static Date getDate(int year, int month, int day, int hour,
			int minute, int second) {
		Calendar c = Calendar.getInstance();
		c.set(year, month - 1, day, hour, minute, second);
		c.set(Calendar.MILLISECOND, 0);

		return c.getTime();
	}

	/**
	 * add year to specified date instance.
	 *
	 * @param date
	 * @param year
	 * @return
	 */
	public static Date addYear(Date date, int year) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.YEAR, year);
		return c.getTime();
	}

	/**
	 * add day to specified date instance.
	 *
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date addDay(Date date, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, day);

		return calendar.getTime();
	}

	/**
	 * add month to specified date instance.
	 *
	 * @param date
	 * @param month
	 * @return
	 */
	public static Date addMonth(Date date, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);

		return calendar.getTime();
	}

	/**
	 * add hour to specified date instance.
	 *
	 * @param date
	 * @param hour
	 * @return
	 */
	public static Date addHour(Date date, int hour) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, hour);

		return calendar.getTime();
	}

	/**
	 * add minute to specified date instance.
	 *
	 * @param date
	 * @param minute
	 * @return
	 */
	public static Date addMinute(Date date, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minute);

		return calendar.getTime();
	}

	/**
	 * 取得上個月
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 2
	 */
	public static int getPreviousMonth(String taiwanYear, String month) {
		return getMonth(addMonth(getFirstDayOfMonth(taiwanYear, month), -1));
	}

	/**
	 * 取得上個月
	 *
	 * @param taiwanYear
	 * @param month
	 * @return "02"
	 */
	public static String getPreviousMonthPattern(String taiwanYear, String month) {
		return TPIStringUtil.numberFormat(getPreviousMonth(taiwanYear, month),
				"00");
	}

	/**
	 * 取得下個月
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 2
	 */
	public static int getNextMonth(String taiwanYear, String month) {
		return getMonth(addMonth(getFirstDayOfMonth(taiwanYear, month), 1));
	}

	/**
	 * 取得下個月
	 *
	 * @param taiwanYear
	 * @param month
	 * @return "02"
	 */
	public static String getNextMonthPattern(String taiwanYear, String month) {
		return TPIStringUtil.numberFormat(getNextMonth(taiwanYear, month), "00");
	}

	/**
	 * 取得上一年
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 2010
	 */
	public static int getPreviousYear(String taiwanYear, String month) {
		return getYear(addYear(getFirstDayOfMonth(taiwanYear, month), -1));
	}

	/**
	 * 取得上一年
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 99
	 */
	public static int getPreviousTwYear(String taiwanYear, String month) {
		int twYear = new Integer(taiwanYear).intValue();
		return getPreviousMonth(taiwanYear, month) == 12 ? twYear - 1 : twYear;
	}

	/**
	 * 取得上一年
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 099
	 */
	public static String getPreviousTwYearPattern(String taiwanYear,
			String month) {
		return DateUtil.getTWYearPattern(getPreviousTwYear(taiwanYear, month));
	}

	/**
	 * 取得下一年
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 2010
	 */
	public static int getNextYear(String taiwanYear, String month) {
		return getYear(addYear(getFirstDayOfMonth(taiwanYear, month), 1));
	}

	/**
	 * 取得下一年
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 99
	 */
	public static int getNextTwYear(String taiwanYear, String month) {
		int twYear = new Integer(taiwanYear).intValue();
		int twMonth = new Integer(month).intValue();
		return twMonth == 12 ? twYear + 1 : twYear;
	}

	/**
	 * 取得下一年
	 *
	 * @param taiwanYear
	 * @param month
	 * @return 099
	 */
	public static String getNextTwYearPattern(String taiwanYear, String month) {
		return DateUtil.getTWYearPattern(getNextTwYear(taiwanYear, month));
	}

	/**
	 * 判斷兩個日期年份是否相同
	 *
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameYear(Date date1, Date date2) {
		if (date1 == null) {
			throw new IllegalArgumentException("date1 can not null.");
		} else if (date2 == null) {
			throw new IllegalArgumentException("date2 can not null.");
		}
		int date1Year = getYear(date1);
		int date2Year = getYear(date2);
		if (date1Year == date2Year) {
			return true;
		}
		return false;
	}

	/**
	 * 判斷兩個日期月份是否相同
	 *
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameMonth(Date date1, Date date2) {
		if (date1 == null) {
			throw new IllegalArgumentException("date1 can not null.");
		} else if (date2 == null) {
			throw new IllegalArgumentException("date2 can not null.");
		}
		int date1Month = getMonth(date1);
		int date2Month = getMonth(date2);
		if (date1Month == date2Month) {
			return true;
		}
		return false;
	}

	/**
	 * 判斷兩個日期年份與月份是否都相同
	 *
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameYearAndMonth(Date date1, Date date2) {
		if (DateUtil.isSameYear(date1, date2)
				&& DateUtil.isSameMonth(date1, date2)) {
			return true;
		}
		return false;
	}

	/**
	 * 判斷 date1的年月 是否小於 date2的年月
	 *
	 * @param date1
	 * @param standardDate
	 * @return
	 */
	public static boolean isLessYearAndMonth(Date date1, Date standardDate) {
		int date1Year = getYear(date1);
		int date1Month = getMonth(date1);
		int date2Year = getYear(standardDate);
		int date2Month = getMonth(standardDate);

		if (date1Year < date2Year) {
			return true;
		} else if (date1Year == date2Year && date1Month < date2Month) {
			return true;
		}
		return false;
	}

	/**
	 * 取得指定範圍內的民國年
	 *
	 * @param start 預選年份起始 89
	 * @param end 預選年份結束 99
	 * @return 089,090,091,092,093,094,095,096,097,098,099
	 */
	public static String[] getRangeTwYear(int start, int end) {
		StringBuffer sb = new StringBuffer();
		if (end >= start) {
			createTwYear(start, end, sb);
			return sb.toString().split(",");
		} else {
			return new String[]{};
		}
	}

	/**
	 * 取得範圍內的民國年
	 *
	 * @param start 預選年份的前幾年 4
	 * @param end 預選年份的後幾年 4
	 * @param date 預選日期 98
	 * @return 094,095,096,097,98,099,100,101,102
	 */
	public static String[] getRangeTwYear(int start, int end, Date date) {
		int now = getTwYear(date);
		return DateUtil.getRangeTwYear(start, end, now);
	}

	/**
	 * 取得範圍內的民國年
	 *
	 *
	 * @param start 預選年份的前幾年 4
	 * @param end 預選年份的後幾年 4
	 * @param now 預選的民國年日期 98
	 * @return 094,095,096,097,98,099,100,101,102
	 */
	public static String[] getRangeTwYear(int start, int end, int now) {
		StringBuffer sb = new StringBuffer();
		createTwYear(now - start, now - 1, sb);
		if (end != 0) {
			sb.append(",");
			sb.append(getTWYearPattern(now));
		}
		createTwYear(now + 1, now + end, sb);
		return sb.toString().split(",");
	}

	/**
	 * 建立日期範圍格式
	 *
	 * @param start 開始日期
	 * @param end 結束日期
	 * @param range 放入的範圍字串buffer
	 */
	private static void createTwYear(int start, int end, StringBuffer range) {
		int i = end - 1;
		if (start < end) {
			DateUtil.createTwYear(start, i, range);
		}
		if (range.length() > 0) {
			range.append(",");
		}
		range.append(DateUtil.getTWYearPattern(end));
	}

	/**
	 * 取得格式化後的民國年 ex : twYear :98 , return: 098
	 *
	 * @param twYear 民國年
	 * @return
	 */
	public static String getTWYearPattern(int twYear) {
		return TPIStringUtil.numberFormat(twYear, "000");
	}

	/**
	 * 取得格式化後的月 ex : month :2 , return: 02
	 *
	 * @param month 月份
	 * @return
	 */
	public static String getMonthPattern(int month) {
		return TPIStringUtil.numberFormat(month, "00");
	}

	/**
	 * 將民國年月日(yyyMMdd)字串，依傳入符號轉換其格式
	 *
	 * @param twDate (yyyMMdd)
	 * @param symbol (SYMBOL_SLASH, SYMBOL_DASH)
	 * @return yyy/MM/dd, yyy-MM-dd
	 */
	public static String formatTwDate(String twDateStr, String symbol) {
		if (StringUtils.isBlank(twDateStr) || twDateStr.length() != 7) {
			return "";
		}
		StringBuffer sb = new StringBuffer();
		sb.append(getYear(twDateStr.substring(0, 3)));
		sb.append(symbol);
		sb.append(twDateStr.substring(3, 5));
		sb.append(symbol);
		sb.append(twDateStr.substring(5));
		return sb.toString();
	}

	/**
	 * 將日期轉為民國格式
	 *
	 * @param date
	 * @param patternOfTw
	 * @return
	 */
	public static String formatTwDate(Date date, String pattern) {
		if (date == null || StringUtils.isBlank(pattern)) {
			return "";
		}
		pattern = StringUtils.remove(pattern, "y");
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return getTWYearPattern(getTwYear(date)) + sdf.format(date);
	}

	/**
	 * 將日期轉為民國(yyy年MM月dd日)格式。
	 *
	 * @param date
	 * @return 民國年月日(yyy年MM月dd日)，date為null則回傳空字串。
	 */
	public static String formatTwDateFmt(Date date) {
		String temp = formatTwDate(date, PATTERN_SLASH_DATE);
		if (temp == null) {
			return null;
		}
		String[] temps = temp.split(SYMBOL_SLASH);
		if (temps.length != 3) {
			return null;
		}
		return temps[0] + "年" + temps[1] + "月" + temps[2] + "日";
	}

	/**
	 * 依傳入格式轉換日期物件
	 *
	 * @param date
	 * @param pattern
	 * @return 日期字串(ex:yyyy/MM/dd)，date為null則回傳空字串
	 */
	public static String formatDate(Date date, String pattern) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}

	/**
	 * 將輸入的字串依格式轉為Date
	 *
	 * @param dateStr
	 * @param pattern
	 * @return
	 * @throws Exception
	 */
	public static Date getDateByPattern(String dateStr, String pattern) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.parse(dateStr);
		} catch (NullPointerException npe) {
			return null;
		} catch (IllegalArgumentException ilae) {
			return null;
		} catch (ParseException pe) {
			return null;
		}
	}

	/**
	 * 將輸入的西元日期字串，依輸入的格式，轉為民國 yyy/mm/dd
	 *
	 * @param dateStr
	 * @param pattern
	 * @return
	 */
	public static String getTwDate(String dateStr, String pattern) {
		Date tmpDate = getDateByPattern(dateStr, pattern);

		return formatTwDate(tmpDate, pattern);
	}

	/**
	 * 將輸入的西元日期字串，依輸入的格式，轉為民國 yyy年mm月dd日
	 *
	 * @param dateStr
	 * @param pattern
	 * @return
	 */
	public static String getTwDateFmt(String dateStr, String pattern) {
		Date tmpDate = getDateByPattern(dateStr, pattern);

		return formatTwDate(tmpDate, pattern);
	}

	/**
	 * 取得兩日期相差月數
	 *
	 * @param date1
	 * @param Date2
	 * @return BigDecimal
	 */
	public static int getSubtractMonths(Date startDate, Date endDate) {
		CheckUtil.isNotNull("startDate", startDate);
		CheckUtil.isNotNull("endDate", endDate);

		Calendar startCalendar = Calendar.getInstance();
		Calendar endCalendar = Calendar.getInstance();
		startCalendar.setTime(startDate);
		endCalendar.setTime(endDate);

		BigDecimal satrtTime = new BigDecimal(
				Long.toString(startDate.getTime()));
		BigDecimal endTime = new BigDecimal(Long.toString(endDate.getTime()));

		return endTime.subtract(satrtTime)
				.divide(new BigDecimal("24"), 0, BigDecimal.ROUND_HALF_UP)
				.divide(new BigDecimal("60"), 0, BigDecimal.ROUND_HALF_UP)
				.divide(new BigDecimal("60"), 0, BigDecimal.ROUND_HALF_UP)
				.divide(new BigDecimal("1000"), 0, BigDecimal.ROUND_HALF_UP)
				.divide(new BigDecimal("30"), 0, BigDecimal.ROUND_HALF_UP)
				.intValue();
	}

	/**
	 * 依使用年限計算兩日期相差月數
	 *
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int getSubtractMonthsByUseYear(Date startDate, Date endDate) {
		CheckUtil.isNotNull("startDate", startDate);
		CheckUtil.isNotNull("endDate", endDate);

		Calendar startCalendar = Calendar.getInstance();
		Calendar endCalendar = Calendar.getInstance();
		startCalendar.setTime(startDate);
		endCalendar.setTime(endDate);

		int startDay = startCalendar.get(Calendar.DATE);
		int endDay = endCalendar.get(Calendar.DATE);

		if (startDay != 1) {
			throw new IllegalArgumentException("起始日(startDate)需由1日開始。");
		} else if (endDay != 1
				&& endDay != endCalendar
				.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			throw new IllegalArgumentException("結束日(startDate)需由1日或當月最後一天。");
		}

		int year = endCalendar.get(Calendar.YEAR)
				- startCalendar.get(Calendar.YEAR);
		int month = endCalendar.get(Calendar.MONTH)
				- startCalendar.get(Calendar.MONTH);

		if (startDay == 1 && endDay == 1) {
			return year * 12 + month;
		} else {
			return year * 12 + month + 1;
		}
	}

	/**
	 * date1>date2 => return true. 以"日"為計算基準
	 *
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isMore(Date date1, Date date2) {
		if (getDiffDays(date1, date2) > 0) {
			return true;
		}

		return false;
	}

	/**
	 * date1<date2 => return true. 以"日"為計算基準
	 *
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isLess(Date date1, Date date2) {
		if (getDiffDays(date1, date2) < 0) {
			return true;
		}
		return false;
	}

	/**
	 * 取得日期相差天數，只計算日期差，不包含時間差
	 *
	 * @param date1
	 * @param date2
	 * @return date1 - date2 的日期差，可能為正數或負數
	 */
	public static Long getDiffDays(Date date1, Date date2) {
		CheckUtil.isNotNull("date1", date1);
		CheckUtil.isNotNull("date2", date2);

		long timeSpan = date1.getTime() - date2.getTime();
		if (Math.abs(timeSpan) < (1000 * 60 * 60 * 24)) {
			return 0L;
		}

		long days = timeSpan / 1000 / 60 / 60 / 24;
		return days;
	}
	
	/**
	 * 將西元年月日(yyyyMMdd)字串，依傳入符號轉換其格式
	 *
	 * @param date (yyyyMMdd)
	 * @param symbol (SYMBOL_SLASH, SYMBOL_DASH)
	 * @return yyyy/MM/dd, yyyy-MM-dd
	 */
	public static String formatDate(String dateStr, String symbol) {
		if (StringUtils.isBlank(dateStr) || dateStr.length() != 8) {
			return "";
		}
		StringBuffer sb = new StringBuffer();
		sb.append(dateStr.substring(0, 4));
		sb.append(symbol);
		sb.append(dateStr.substring(4, 6));
		sb.append(symbol);
		sb.append(dateStr.substring(6, 8));
		
		return sb.toString();
	}
	
	
	public static SimpleDateFormat getFormat(String patten){
		SimpleDateFormat format = new SimpleDateFormat(patten);
		return format;
	}
	
	/**
	 * 將傳入的timestamp轉換為指定格式的日期字串
	 * @param patten
	 * @param timestamp
	 * @return
	 */
	public static String formatTamp(String patten, Date timestamp){
		String result = "";
		if(timestamp != null){
			result = getFormat(patten).format(timestamp);
		}else{
			result = getFormat(patten).format(new Date());
		}
		
		return result;
	}
	
}
