package com.tp.tpigateway.util;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * 檢核工具
 */
public final class CheckUtil<T> {

	public static Boolean isNotNull(String objectName, Object object) {
		if (object == null) {
			throw new IllegalArgumentException(TPIStringUtil.join(objectName,
					" can not null."));
		}
		return Boolean.TRUE;
	}

	public static Boolean isNotNull(String objectName, Long object) {
		if (object == null) {
			throw new IllegalArgumentException(TPIStringUtil.join(objectName,
					" can not null."));
		}
		return Boolean.TRUE;
	}

	public static Boolean isNotNull(String objectName, Long... objects) {
		if (objects == null || objects.length == 0
				|| (objects.length == 1 && objects[0] == null)) {
			throw new IllegalArgumentException(TPIStringUtil.join(objectName,
					" can not null."));
		}
		return Boolean.TRUE;
	}

	@SuppressWarnings("unchecked")
	public static Boolean isNotEmpty(String objectName, List objects) {
		if (objects == null || objects.size() == 0) {
			throw new IllegalArgumentException(TPIStringUtil.join(objectName,
					" can not null or empty."));
		}
		for (int i = 0; i < objects.size(); i++) {
			if (objects.get(i) == null) {
				throw new IllegalArgumentException(TPIStringUtil.join(objectName,
						".get(", i, ") can not null."));
			}
		}
		return Boolean.TRUE;
	}

	@SuppressWarnings("unchecked")
	public static Boolean isNotEmpty(String objectName, Map objects) {
		if (objects == null || objects.size() == 0) {
			throw new IllegalArgumentException(TPIStringUtil.join(objectName,
					" can not null or empty."));
		}
		return Boolean.TRUE;
	}

	public static Boolean isNotEmpty(String objectName, Object[] objects) {
		if (objects == null || objects.length == 0) {
			throw new IllegalArgumentException(TPIStringUtil.join(objectName,
					" can not null or empty."));
		}
		for (int i = 0; i < objects.length; i++) {
			if (objects[i] == null) {
				throw new IllegalArgumentException(TPIStringUtil.join(objectName,
						"[", i, "] can not null."));
			}
		}
		return Boolean.TRUE;
	}

	public static Boolean isNotEmpty(String objectName, String object) {
		if (StringUtils.isBlank(object)) {
			throw new IllegalArgumentException(TPIStringUtil.join(objectName,
					" can not null or empty."));
		}
		return Boolean.TRUE;
	}

	public static Boolean isEquals(String object1Name, String object2Name,
			Object object1, Object object2) {
		if (object1 == null && object2 == null) {
			return Boolean.TRUE;
		} else if (isNotNull(object1Name, object1)
				&& isNotNull(object2Name, object2)) {
			if (!object1.equals(object2)) {
				throw new IllegalArgumentException(TPIStringUtil.join(object1Name,
						object1.toString(), " is not equal to ", object2Name,
						object2.toString(), "."));
			}
		}
		return Boolean.TRUE;
	}

	public static <T> T runtimeNotNull(String objectName, T object) {
		if (object == null) {
			throw new RuntimeException(TPIStringUtil.join(objectName,
					" can not be found."));
		}
		return object;
	}

	public static <T> T runtimeNotNull(String paramName, Object paramValue,
			String objectName, T object) {
		if (object == null) {
			throw new RuntimeException(TPIStringUtil.join(objectName, "(",
					paramName, ":", paramValue, ") can not be found."));
		}
		return object;
	}

	public static <T> List<T> runtimeNotEmpty(String objectName, List<T> objects) {
		if (objects.isEmpty()) {
			throw new RuntimeException(TPIStringUtil.join(objectName,
					" can not empty."));
		}
		return objects;
	}

	public static <T> List<T> runtimeNotEmpty(String paramName,
			Object paramValue, String objectName, List<T> objects) {
		if (objects.isEmpty()) {
			throw new RuntimeException(TPIStringUtil.join(objectName, "(",
					paramName, ":", paramValue, ") can not empty."));
		}
		return objects;
	}
	
	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if( !isNum.matches() ){
			return false;
		}
		return true;
	}
}
