package com.tp.tpigateway.util;

import java.security.MessageDigest;

/**
 * Token產生公用程式
 */
public class PasswordUtils {
	static final char[] DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	private static String DIGEST_ALGORITHM = "SHA";

	public static String encodePassword(String plainTextPassword) {
		if (plainTextPassword == null) {
			return null;
		}

		try {
			MessageDigest md = MessageDigest.getInstance(DIGEST_ALGORITHM);
			md.update(plainTextPassword.getBytes());
			return hexEncode(md.digest());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static String hexEncode(byte[] bytes) {
		StringBuffer s = new StringBuffer(bytes.length * 2);

		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];
			s.append(DIGITS[(b & 0xf0) >> 4]);
			s.append(DIGITS[b & 0x0f]);
		}

		return s.toString();
	}
}
