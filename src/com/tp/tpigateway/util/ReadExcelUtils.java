package com.tp.tpigateway.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 讀取Excel
 */
public class ReadExcelUtils {
	
	private Logger logger = LoggerFactory.getLogger(ReadExcelUtils.class);
	
	private Workbook wb;

	public ReadExcelUtils(String filepath) {
		if (filepath == null) {
			return;
		}
		String ext = filepath.substring(filepath.lastIndexOf("."));
		try {
			InputStream is = new FileInputStream(filepath);
			if (".xls".equals(ext)) {
				wb = new HSSFWorkbook(is);
			} else if (".xlsx".equals(ext)) {
				wb = new XSSFWorkbook(is);
			} else {
				wb = null;
			}
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException");
		} catch (IOException e) {
			logger.error("IOException:" + e.getMessage());
			logger.debug("", e);
		}
	}

	/**
	 * 讀取Excel表格表頭的內容
	 * 
	 * @param InputStream
	 * @return String 表頭內容的數組
	 */
	public String[] readExcelTitle(int sheetId) throws Exception {
		if (wb == null) {
			throw new Exception("Workbook對象為空！");
		}
		Sheet sheet = wb.getSheetAt(sheetId);
		Row row = sheet.getRow(0);
		// 標題總列數
		int colNum = row.getPhysicalNumberOfCells();
		System.out.println("colNum:" + colNum);
		String[] title = new String[colNum];
		for (int i = 0; i < colNum; i++) {
			// title[i] = getStringCellValue(row.getCell((short) i));
			title[i] = row.getCell(i).getCellFormula();
		}
		return title;
	}

	/**
	 * 讀取Excel資料內容
	 * 
	 * @param InputStream
	 * @return Map 包含單元格資料內容的Map對象
	 */
	public Map<Integer, Map<Integer, Object>> readExcelContent(int sheetId) throws Exception {
		if (wb == null) {
			throw new Exception("Workbook對象為空！");
		}
		Map<Integer, Map<Integer, Object>> content = new HashMap<Integer, Map<Integer, Object>>();

		Sheet sheet = wb.getSheetAt(sheetId);
		// 得到總行數
		int rowNum = sheet.getLastRowNum();
		Row row = sheet.getRow(0);
		int colNum = row.getPhysicalNumberOfCells();
		// 正文內容應該從第二行開始,第一行為表頭的標題
		for (int i = 1; i <= rowNum; i++) {
			row = sheet.getRow(i);
			int j = 0;
			Map<Integer, Object> cellValue = new HashMap<Integer, Object>();
			while (j < colNum) {
				Object obj = getCellFormatValue(row.getCell(j));
				//Object obj = getCellStringValue(row.getCell(j));
				cellValue.put(j, obj);
				j++;
			}
			content.put(i, cellValue);
		}
		return content;
	}

	/**
	 * 以字串類型設置資料
	 * 
	 * @param cell
	 * @return
	 */
	@SuppressWarnings({ "unused" })
	private Object getCellStringValue(Cell cell) {
		Object cellvalue = "";
		if (cell != null) {
			cellvalue = cell.getRichStringCellValue().getString();
		}
		return cellvalue;
	}
	
	/**
	 * 根據Cell類型設置資料
	 * 
	 * @param cell
	 * @return
	 */
	@SuppressWarnings({ "deprecation" })
	private Object getCellFormatValue(Cell cell) {
		Object cellvalue = "";
		if (cell != null) {
			// 判斷當前Cell的Type
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_NUMERIC:// 如果當前Cell的Type為NUMERIC
			case Cell.CELL_TYPE_FORMULA: {
				// 判斷當前的cell是否為Date
				if (DateUtil.isCellDateFormatted(cell)) {
					// 如果是Date類型則，轉化為Data格式
					// data格式是帶時分秒的：2013-7-10 0:00:00
					// cellvalue = cell.getDateCellValue().toLocaleString();
					// data格式是不帶帶時分秒的：2013-7-10
					Date date = cell.getDateCellValue();
					cellvalue = date;
				} else {// 如果是純數字

					// 取得當前Cell的數值
					//cellvalue = String.valueOf(cell.getNumericCellValue());
					cellvalue = String.valueOf(new Double(cell.getNumericCellValue()).intValue());
				}
				break;
			}
			case Cell.CELL_TYPE_STRING:// 如果當前Cell的Type為STRING
				// 取得當前的Cell字符串
				cellvalue = cell.getRichStringCellValue().getString();
				break;
			default:// 默認的Cell值
				cellvalue = "";
			}
		} else {
			cellvalue = "";
		}
		return cellvalue;
	}

	public static void main(String[] args) {
		try {
			String filepath = "D:\\temp\\wording.xlsx";
			ReadExcelUtils excelReader = new ReadExcelUtils(filepath);
			// 對讀取Excel表格標題測試
			// String[] title = excelReader.readExcelTitle();
			// System.out.println("獲得Excel表格的標題:");
			// for (String s : title) {
			// System.out.print(s + " ");
			// }

			// 對讀取Excel表格內容測試
			Map<Integer, Map<Integer, Object>> map = excelReader.readExcelContent(0);
			System.out.println("獲得Excel表格的內容:");
			for (int i = 1; i <= map.size(); i++) {
				System.out.println(map.get(i));
			}
		} catch (FileNotFoundException e) {
		} catch (Exception e) {
		}
	}
}