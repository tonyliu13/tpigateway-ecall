package com.tp.tpigateway.util;

import java.util.HashMap;
import java.util.Map;

import com.tp.tpigateway.model.common.BotTpiCallLog;

public class ApiLogUtils {

	private static ThreadLocal<BotTpiCallLog> apiLog = new ThreadLocal<BotTpiCallLog>();
	
	private static ThreadLocal<Map<String, Object>> paramLog = new ThreadLocal<Map<String, Object>>();
	
	private static final String PARAM_NODEID = "nodeID";
	private static final String PARAM_SOURCEIP = "sourceIP";

//	public static ThreadLocal<BotTpiCallLog> getApiLog() {
//		return apiLog;
//	}
//
//	public static void setApiLog(ThreadLocal<BotTpiCallLog> apiLog) {
//		ApiLogUtils.apiLog = apiLog;
//	}

	public static BotTpiCallLog getBotTpiCallLog() {
		return apiLog.get();
	}
	
	public static void addBotTpiCallLog(BotTpiCallLog botTpiCallLog) {
		apiLog.set(botTpiCallLog);
	}
	
	public static String getNodeID() {
		return (null != paramLog && null != paramLog.get() && null != paramLog.get().get(PARAM_NODEID))
				? paramLog.get().get(PARAM_NODEID).toString()
				: "";
	}
	
	public static void setNodeID(String nodeID) {
		if (null == paramLog.get()) {
			paramLog.set(new HashMap<String, Object>());
		}
		paramLog.get().put(PARAM_NODEID, nodeID);
	}
	
	public static String getSourceIP() {
		return (null != paramLog && null != paramLog.get() && null != paramLog.get().get(PARAM_SOURCEIP))
				? paramLog.get().get(PARAM_SOURCEIP).toString()
						: "";
	}
	
	public static void setSourceIP(String sourceIP) {
		if (null == paramLog.get()) {
			paramLog.set(new HashMap<String, Object>());
		}
		paramLog.get().put(PARAM_SOURCEIP, sourceIP);
	}
}
