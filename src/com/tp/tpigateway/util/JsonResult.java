package com.tp.tpigateway.util;

import java.util.HashMap;
import java.util.Map;

public class JsonResult {

	private int code; //0:成功   1:失敗
	
	private String msg;
	
	private Map<String, Object> extend = new HashMap<>();

	public static JsonResult success(){
		JsonResult result = new JsonResult();
		result.setCode(0);
		result.setMsg("處理成功");
		return result;
	}
	
	public static JsonResult success(String msg){
		JsonResult result = new JsonResult();
		result.setCode(0);
		result.setMsg(msg);
		return result;
	}
	
	public static JsonResult fail(){
		JsonResult result = new JsonResult();
		result.setCode(1);
		result.setMsg("處理失敗");
		return result;
	}
	
	public static JsonResult fail(String msg){
		JsonResult result = new JsonResult();
		result.setCode(1);
		result.setMsg(msg);
		return result;
	}
	
	public JsonResult add(String key, Object value){
		this.getExtend().put(key, value);
		return this;
	}
	
	public boolean isSuucess(){
		return code == 0;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map<String, Object> getExtend() {
		return extend;
	}

	public void setExtend(Map<String, Object> extend) {
		this.extend = extend;
	}
	
}
