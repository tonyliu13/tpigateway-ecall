package com.tp.tpigateway.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 資料格式驗證工具
 */
public class ValidUtils {

	private static Logger log = LoggerFactory.getLogger(ValidUtils.class);
	
	private static final char[] FIRSTWORD = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	private static final int[] FIRSTWORDNUMBER = {1,10,19,28,37,46,55,64,39,73,82,2,11,20,48,29,38,47,56,65,74,83,21,3,12,30};
	 
	/**
	 * 身分證字號(臺灣)驗證
	 * 
	 * @param id
	 * @return true:pass, false:fail
	 */
	public static boolean isIDValid(String id) {
		
		log.debug("id=" + id);
		
		if (id == null) {
			return false;
		}
		
		String personId = id.toUpperCase();
		if (personId.length() == 0) {
			return false;
		}
		
		Pattern pattern = Pattern.compile("^\\p{Upper}{1}\\d{9}", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(personId);
		if (matcher.matches()) {
			int verifyNumber = 0;
			// 英文字母的轉碼數字
			verifyNumber = FIRSTWORDNUMBER[Arrays.binarySearch(FIRSTWORD, personId.charAt(0))];
			// 身份證字號第1～8個數字依次取出乘以8～1
			for (int i = 0; i < 8; i++) {
				verifyNumber += Character.digit(personId.charAt(i + 1), 10) * (8 - i);
			}
			// 10 - 上述總和除10之餘數
			verifyNumber = 10 - (verifyNumber % 10);
			// 若驗證碼與身份證字號最後一個數字相同即為正確
			if (verifyNumber == Character.digit(personId.charAt(9), 10) || verifyNumber == 10) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 日期格式驗證
	 * 
	 * @param date
	 * @param format
	 * @return true:pass, false:fail
	 */
	public static boolean isDateValid(String date, String format) {
		
		try {
			// 以SimpleDateFormat來檢核日期
			SimpleDateFormat dFormat = new SimpleDateFormat(format);
			dFormat.setLenient(false);
			// 如果成功就是正確的日期，失敗就是有錯誤的日期。
			dFormat.parse(date);
			return true;
		} catch (ParseException e) {
			// 這個日期不是一個正確的日期"
		}
		
		return false;
	}
}
