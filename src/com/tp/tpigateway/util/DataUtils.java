package com.tp.tpigateway.util;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.cglib.beans.BeanMap;

import java.beans.FeatureDescriptor;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Stream;

public class DataUtils {

	public static String convertToCardDateStyle(String s) {
		return s == null ? "" : s.replaceAll("-", "/");
	}

	/*
	 * 1.去掉小數點後面多餘的零
	 * 2.將數字格式化為千分位表示法。
	 */
	public static String formatDollar(String s) {
		
		if (StringUtils.isBlank(s) || !NumberUtils.isNumber(s)) {
			return "";
		}
		
		DecimalFormat decimalFormat = new DecimalFormat(",###");
		
		if (s.indexOf(".") > 0) {
			s = s.replaceAll("0+?$", ""); // 去掉多餘的零
			s = s.replaceAll("[.]$", ""); // 如最後一位是.則去掉
		}
		
		String intPart = "";
		String dotPart = "";
		intPart = s.split("\\.")[0];
		if (s.indexOf(".") > 0) {
			dotPart = "." + s.split("\\.")[1];
		}
		
		s = decimalFormat.format(Double.parseDouble(intPart)) + dotPart;  
		
		return s;
	}
	
    /**
     * 將物件轉為 BigDecimal 型態，若為 null 、長度為 0  的空字串或是半型空白時，則回傳指定預設值
     * @param obj
     * @param defaultValue
     * @return
     */
	public static BigDecimal objToBigDecimal(Object obj, BigDecimal defaultValue) {
        if (obj == null) {
            return defaultValue;
        }

        if (BigDecimal.class.isInstance(obj)) {
            return (BigDecimal) obj;
        }

        String str = obj.toString();

        if ("".equals(str) || "".equals(str.trim())) {
            return defaultValue;
        }

        return new BigDecimal(str);
    }
    
	public static <T> Map<String, Object> beanToMap(T bean) {
		Map<String, Object> map = new HashMap<>();
		if (bean != null) {
			BeanMap beanMap = BeanMap.create(bean);
			for (Object key : beanMap.keySet()) {
				map.put(key+"", beanMap.get(key));
			}
		}
		return map;
	}

	public static <T> T mapToBean(Map<String, Object> map,T bean) {
		BeanMap beanMap = BeanMap.create(bean);
		beanMap.putAll(map);
		return bean;
	}

	public static <T> List<Map<String, Object>> objectsToMaps(List<T> objList) {
		List<Map<String, Object>> list = new ArrayList<>();
		if (objList != null && objList.size() > 0) {
			Map<String, Object> map = null;
			T bean = null;
			for (int i = 0,size = objList.size(); i < size; i++) {
				bean = objList.get(i);
				map = beanToMap(bean);
				list.add(map);
			}
		}
		return list;
	}

	public static <T> List<T> mapsToObjects(List<Map<String, Object>> maps, Class<T> clazz) throws InstantiationException, IllegalAccessException {
		List<T> list = new ArrayList<>();
		if (maps != null && maps.size() > 0) {
			Map<String, Object> map = null;
			T bean = null;
			for (int i = 0,size = maps.size(); i < size; i++) {
				map = maps.get(i);
				bean = clazz.newInstance();
				mapToBean(map, bean);
				list.add(bean);
			}
		}
		return list;
	}

	/*
	public static String[] getNullPropertyNames (Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for(java.beans.PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null) emptyNames.add(pd.getName());
		}
		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}*/

	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
		return Stream.of(wrappedSource.getPropertyDescriptors())
				.map(FeatureDescriptor::getName)
				.filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
				.toArray(String[]::new);
	}

	public static void main(String[] args) {
		String s = "12345.12300";
		System.out.println(DataUtils.formatDollar(s));
	}
}
