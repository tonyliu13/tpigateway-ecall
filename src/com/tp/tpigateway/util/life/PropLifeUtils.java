package com.tp.tpigateway.util.life;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * properties 共用工具類 for life
 */
public class PropLifeUtils {

	private static final Logger log = LoggerFactory.getLogger(PropLifeUtils.class);
	
	private static Map<String, String> intentMap = new ConcurrentHashMap<String, String>();
	private static Map<String, String> scriptMap = new ConcurrentHashMap<String, String>();

	public static String addIntent(String wording) {
		String[] scriptContent = {"base_day", "salesman", "revival_way", "change_validity_credit_card", "foreign_exchange", 
				"payReciTime", "ATM", "MyBill", "MyBank", "ibon", "FamiPort", "postal_giro", "bank_transfer", "service_counter", 
				"vip_counter", "another", "deduct", "fuyue", "deductplace", "paychannel", "auto_pay_way", 
				"entry_time", "history", "query", "turnToreal", "exit", "login"};
		String result = wording;
		for (int i = 0; i <= scriptContent.length - 1; i ++) {
			if (StringUtils.equals(getScript(scriptContent[i]), wording)) {
				result = getIntent(scriptContent[i]) + "@" + wording;
				break;
			}
		}
		return result;
//		if (StringUtils.equals(getScript("another"), wording)) {
//			return getIntent("another") + "@" + wording;
//		} else if (StringUtils.equals(getScript("paychannel"), wording)) {
//			return getIntent("paychannel") + "@" + wording;
//		} else if (StringUtils.equals(getScript("auto_pay_way"), wording)) {
//			return getIntent("auto_pay_way") + "@" + wording;
//		} else if (StringUtils.equals(getScript("query"), wording)) {
//			return getIntent("query") + "@" + wording;
//		} else if (StringUtils.equals(getScript("deduct"), wording)) {
//			return getIntent("deduct") + "@" + wording;
//		} else if (StringUtils.equals(getScript("deductplace"), wording)) {
//			return getIntent("deductplace") + "@" + wording;
//		} else if (StringUtils.equals(getScript("history"), wording)) {
//			return getIntent("history") + "@" + wording;
//		} else if (StringUtils.equals(getScript("exit"), wording)) {
//			return getIntent("exit") + "@" + wording;
//		} else if (StringUtils.equals(getScript("entry_time"), wording)) {
//			return getIntent("entry_time") + "@" + wording;
//		} else if (StringUtils.equals(getScript("salesman"), wording)) {
//			return getIntent("salesman") + "@" + wording;
//		} else {
//			return wording;
//		}
	}
	
    /**
     * 取得intent
     * @param key
     * @return
     */
    public static String getIntent(String key) {
    	
    	if (intentMap.isEmpty()) {
    		synchronized(intentMap) {
    			if (intentMap.isEmpty()) {
    				readIntentProperties();
    			}
    		}
    	}
    	
    	return intentMap.get(key);
    }
    
    /**
     * 讀取 intent properties
     */
    public static void readIntentProperties() {
    	Properties prop = new Properties();
    	
    	try {
			prop.load(PropLifeUtils.class.getResourceAsStream("/nlu_intent.life.properties"));
    		
    		Map<String, String> tempMap = new ConcurrentHashMap<String, String>();
    		for (Entry<Object, Object> entry : prop.entrySet()) {
    			String key = (String) entry.getKey();
    			String value = (String) entry.getValue();
    			tempMap.put(key, value);
    		}
    		
    		intentMap.clear();
    		intentMap.putAll(tempMap);
    	} catch (IOException ex) {
    		log.error(ex.getMessage());
    	}
    }
    
    /**
     * 取得腳本固定文字
     * @param keyitem
     * @return
     */
    public static String getScript(String keyitem) {
    	return getScript(null, keyitem);
    }
    	
	/**
	 * 取得腳本固定文字
	 * @param keyid
	 * @param keyitem
	 * @return
	 */
	public static String getScript(String keyid, String keyitem) {
    	
    	if (scriptMap.isEmpty()) {
    		synchronized(scriptMap) {
    			if (scriptMap.isEmpty()) {
    				readScriptProperties();
    			}
    		}
    	}
    	
    	if (keyid == null) {
    		return scriptMap.get("common." + keyitem);
    	} else {
    		return scriptMap.get(keyid + "." + keyitem);
    	}
    }
    
    /**
     * 讀取 script properties
     */
    public static void readScriptProperties() {
    	Properties prop = new Properties();
    	
    	try {
    		prop.load(PropLifeUtils.class.getResourceAsStream("/script.life.properties"));
    		
    		Map<String, String> tempMap = new ConcurrentHashMap<String, String>();
    		for (Entry<Object, Object> entry : prop.entrySet()) {
    			String key = (String) entry.getKey();
    			String value = (String) entry.getValue();
    			tempMap.put(key, value);
    		}
    		
    		scriptMap.clear();
    		scriptMap.putAll(tempMap);
    	} catch (IOException ex) {
    		log.error(ex.getMessage());
    	}
    }
}
