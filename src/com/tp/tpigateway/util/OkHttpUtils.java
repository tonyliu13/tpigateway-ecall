package com.tp.tpigateway.util;

import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tp.tpigateway.exception.RRException;
import com.tp.tpigateway.model.common.BotTpiCallLog;
import com.tp.tpigateway.service.common.BotTpiCallLogService;
import com.tp.tpigateway.util.exception.LifeApiStatusConstants;

import okhttp3.FormBody;
import okhttp3.FormBody.Builder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Http連線呼叫公用程式<br/>
 * (採用OkHttp且以singleton實作, 現只有post)
 */
public class OkHttpUtils {

	private static Logger log = LoggerFactory.getLogger(OkHttpUtils.class);
			
	public final static String STATUS_CODE = "statusCode";
	public final static String MESSAGE = "message";
	public final static String BODY = "body";
	
	private static OkHttpClient singleton;

	private OkHttpUtils() { }
	
	public static OkHttpClient getInstance() {
		if (singleton == null) {
			synchronized (OkHttpUtils.class) {
				if (singleton == null) {
					
					OkHttpClient.Builder mBuilder = new OkHttpClient.Builder();
		            // mBuilder.sslSocketFactory(DummySecureProtocolSocketFactory.createSSLSocketFactory());
					// mBuilder.sslSocketFactory(DummySecureProtocolSocketFactory.createSSLSocketFactory(), new DummySecureProtocolSocketFactory.TrustAllCerts());
		            // mBuilder.hostnameVerifier(new DummySecureProtocolSocketFactory.TrustAllHostnameVerifier());
		            mBuilder.connectTimeout(PropUtils.OK_CONNECT_TIMEOUT, TimeUnit.SECONDS);
		            mBuilder.readTimeout(PropUtils.OK_READ_TIMEOUT, TimeUnit.SECONDS);
		            mBuilder.writeTimeout(PropUtils.OK_WRITE_TIMEOUT, TimeUnit.SECONDS);
		            
					singleton = mBuilder.build();
				}
			}
		}
		return singleton;
	}

	/**
	 * get
	 */
	/*public static void requestGet() {
		// Request
		final Request request = new Request.Builder().url(req.getRequestUrl()).build();
		OkHttpUtils.getInstance().newCall(request);
	}*/

	/**
	 * post
	 */
	public static Map<String, Object> requestPost(final String url, final Map<String, String> headers, final RequestBody body) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		Response response = null;
		try {
			Request.Builder builder = new Request.Builder();
			
			// url
			builder.url(url);
			
			// header
			for (Entry<String, String> mEntry : headers.entrySet()) {
				String mEntryKey = mEntry.getKey();
				Object mEntryValue = mEntry.getValue();
				if (StringUtils.isBlank(mEntryKey)) {
					continue;
				}
				builder.addHeader(mEntryKey, mEntryValue.toString());
			}
			
			/*
			 * 發送國壽API時，須在header代入以下三個參數
			 * 1. SRC_HOSTIP 來源者IP，優先從hostname.properties設定的資訊，次為InetAddress API
			 * 2. SRC_HOSTNAME來源者主機名稱，優先從hostname.properties設定的資訊，InetAddress API
			 * 3. SRC_CLASSNAME 來源者發送請求的類別名稱
			 */
			try{
            	String name = null;
                StackTraceElement[] stes = Thread.currentThread().getStackTrace();
                String className = OkHttpUtils.class.getName();
                for(int i = 0; i < stes.length; i++){
                    if(stes[i].getClassName().equals(className)){
                        String nextClassName = stes[i+1].getClassName();
                        if(className.equals(nextClassName)){
                            continue;
                        }
                        if(nextClassName.contains("life")){
                        	name = stes[i+2].getClassName();
                        }else {
                        	name = stes[i+1].getClassName();
                        }
                        builder.addHeader("SRC_CLASSNAME", name);
                        break;
                    }
                }
                String hostName = null;
                String ip = null;
                try {
                	hostName = ResourceBundle.getBundle("hostname").getString("hostname");
                	ip = ResourceBundle.getBundle("hostname").getString("ip");
                }catch(Exception e) {
                	log.info("無設定hostname.properties，取得SRC_HOSTIP, SRC_HOSTNAME失敗：" + e.getMessage());
                	try {
                		ip = InetAddress.getLocalHost().getHostAddress();
                		hostName = InetAddress.getLocalHost().getHostName();
                	}catch(Exception ee) {
                		log.info("InetAddress取得SRC_HOSTIP, SRC_HOSTNAME失敗：" + e.getMessage());
                	}
                }
                builder.addHeader("SRC_HOSTIP", ip);
            	builder.addHeader("SRC_HOSTNAME", hostName);
            }catch(Exception e){
                log.info("取得SRC_CLASSNAME, SRC_HOSTIP, SRC_HOSTNAME失敗：" + e.getMessage());
            }
			
			// body
			builder.post(body);
			
			Request request = builder.build();
			log.debug("===============================request info start==============================");
			log.debug("request url=" + org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(request.url()));
			log.debug("request method=" + request.method());
			log.debug("request headers=" + org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(request.headers()));
			log.debug("===============================request info end================================");
			
			// call
			response = OkHttpUtils.getInstance().newCall(request).execute();
			
			int statusCode = response.code();
			String message = response.message();
			String bodyStr = response.body().string();
			
			result.put(STATUS_CODE, statusCode);
			result.put(MESSAGE, message);
			result.put(BODY, bodyStr);
			
			log.debug("statusCode=" + statusCode);
			log.debug("message=" + message);
			//log.debug("bodyStr=" + bodyStr);
			
		} catch (Exception e) {
			log.error("error:" + e.getMessage());
			log.debug("", e);
			
			if (result.isEmpty()) {
				result.put(MESSAGE, e.getMessage());
			}
		} finally {
			if (response != null) {
				try {
					response.body().close();
				} catch(Exception ex) { }
			}
		}
		
		return result;
	}
    
    /**
     * get
     * @throws Exception 
     */
    public static Map<String, Object> requestGet(final String url, final Map<String, String> headers) throws Exception {
        
        Map<String, Object> result = new HashMap<String, Object>();
        
        Response response = null;
        try {
            Request.Builder builder = new Request.Builder();
            
            // url
            builder.url(url);
            
            // header
            for (Entry<String, String> mEntry : headers.entrySet()) {
                String mEntryKey = mEntry.getKey();
                Object mEntryValue = mEntry.getValue();
                if (StringUtils.isBlank(mEntryKey)) {
                    continue;
                }
                builder.addHeader(mEntryKey, mEntryValue.toString());
            }
            
            // body
            builder.get();
            
            Request request = builder.build();
            log.debug("===============================request info start==============================");
            log.debug("request url=" + org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(request.url()));
            log.debug("request method=" + request.method());
            log.debug("request headers=" + org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(request.headers()));
            log.debug("===============================request info end================================");
            
            // call
            response = OkHttpUtils.getInstance().newCall(request).execute();
            
            int statusCode = response.code();
            String message = response.message();
            String bodyStr = response.body().string();
            
            result.put(STATUS_CODE, statusCode);
            result.put(MESSAGE, message);
            result.put(BODY, bodyStr);
            
            log.debug("statusCode=" + statusCode);
            log.debug("message=" + message);
            log.debug("bodyStr=" + bodyStr);
            
        } catch (Exception e) {
            String errorMsg = e.getMessage();
            log.error("error:" + errorMsg);
            
            if (result.isEmpty()) {
                result.put(MESSAGE, errorMsg);
            }
            
            if ((e.getCause() != null && e.getCause().equals(SocketTimeoutException.class))
                    || StringUtils.contains(errorMsg.toLowerCase(), "timed out")
                    || StringUtils.contains(errorMsg.toLowerCase(), "timeout")) {
                throw e;
            }
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch(Exception ex) { }
            }
        }
        
        return result;
    }
	
	/**
	 * 檢查是否呼叫成功並包含回傳結果
	 * 
	 * @param respResult
	 * @return
	 */
	public static boolean isSuccessHasBody(Map<String, Object> respResult) {
		
		boolean result = false;
		
		if (MapUtils.isNotEmpty(respResult)) {
			int code = MapUtils.getIntValue(respResult, STATUS_CODE);
			String body = MapUtils.getString(respResult, BODY);
			if (HttpServletResponse.SC_OK == code && StringUtils.isNotBlank(body)) {
				result = true;
			}
		}
		
		return result;
	}
	
	/**
	 * 取得呼叫status code
	 * 
	 * @param respResult
	 * @return
	 */
	public static int getCode(Map<String, Object> respResult) {
		return MapUtils.getIntValue(respResult, STATUS_CODE);
	}
	
	/**
	 * 取得呼叫http message
	 * 
	 * @param respResult
	 * @return
	 */
	public static String getMessage(Map<String, Object> respResult) {
		return MapUtils.getString(respResult, MESSAGE);
	}
	
	/**
	 * 取得呼叫回覆結果
	 * 
	 * @param respResult
	 * @return
	 */
	public static String getBody(Map<String, Object> respResult) {
		return MapUtils.getString(respResult, BODY);
	}
	
	public static <T> T callLifeApi(String url, Map<String, String> param, Class<T> rtnType) throws Exception {
		return OkHttpUtils.callLifeApi(url, param, null, rtnType, HashMap.class);
	}
	
	public static <T> T callLifeApi(String url, Map<String, String> param, String jsonString, Class<T> rtnType) throws Exception {
		return OkHttpUtils.callLifeApi(url, param, jsonString, rtnType, HashMap.class);
	}
	
	/**
	 * 呼叫人壽API
	 */
	public static <T> T callLifeApi(String urlKey, Map<String, String> param, Class<T> rtnType, Class<?> bodyResultType) throws Exception {
		return OkHttpUtils.callLifeApi(urlKey, param, null, rtnType, bodyResultType);
	}
	
	/**
	 * 呼叫人壽API<br>
	 * 1. param、jsonString擇一做為呼叫API參數集<br>
	 * 2. jsonString有值優先使用<br>
	 * 3. 若有呼叫API參數檢核限制，則param必須傳入
	 * 
	 * @param urlKey
	 * @param param
	 * @param jsonString
	 * @param rtnType
	 * @param bodyResultType
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked" })
	public static <T> T callLifeApi(String urlKey, Map<String, String> param, String jsonString, Class<T> rtnType, Class<?> bodyResultType) throws Exception {
		
		String url = PropUtils.getProperty(urlKey + ".url");
		
		if (param == null) {
			param = new HashMap<String, String>();
		}
		
		// 呼叫API參數檢核
		CallApiCheckUtil.baseParamCheck(urlKey, param);
		
		long costTimeStart = System.currentTimeMillis();
		
		T result = null;
		boolean apiResult = true;
		BotTpiCallLog apiLog = null;
		String bodyResult = null;
		String respStatus = null;
		String returnCode = null;
		
		try {
			log.debug("param=" + param);
			if (StringUtils.isNotBlank(jsonString)) {
			    log.debug("jsonString=" + jsonString);
			}

			String apiid = StringUtils.substringAfterLast(url, "/");
			
			RequestBody body = null;
			
			// 20190529 by Todd, 支援直接傳入jsonString
			/*Builder builder = new FormBody.Builder();
			
			for (Entry<String, String> entry : param.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue() == null ? "" : entry.getValue();
				builder.add(key, value);
			}
			body = builder.build();*/
			
			if (StringUtils.isBlank(jsonString)) {
				Builder builder = new FormBody.Builder();
				
				for (Entry<String, String> entry : param.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue() == null ? "" : entry.getValue();
					builder.add(key, value);
				}
				body = builder.build();
			} else {
				MediaType JSON = MediaType.parse("application/json; charset=utf-8");
				// put your json here
				body = RequestBody.create(JSON, jsonString);
			}
			
			Map<String, StringBuilder> infoMap = new HashMap<String, StringBuilder>();
			infoMap.put("apiError",  new StringBuilder());
			
			Map<String, Object> respResult = OkHttpUtils.requestPost(url, new HashMap<String, String>(), body);
			log.debug("respResult=" + Arrays.toString(respResult.entrySet().toArray()));
			
			if (StringUtils.equalsIgnoreCase("timeout", OkHttpUtils.getMessage(respResult))) {
				if (infoMap.get("apiError").length() == 0) infoMap.get("apiError").append(apiid + " API " + OkHttpUtils.getMessage(respResult));
			}
			
			
			int statusCode = OkHttpUtils.getCode(respResult);
			bodyResult = OkHttpUtils.getBody(respResult);
			
			respStatus = "" + statusCode;
			
			if (statusCode < 200 || statusCode >= 400) {
				log.error("url=" + url + ", param=" + param + ", statusCode=" + statusCode);
				if (infoMap.get("apiError").length() == 0) infoMap.get("apiError").append(apiid + " API statusCode=" + statusCode);
				apiResult = false;
				throw new RRException(LifeApiStatusConstants.HTTP_STATUS_ERROR, infoMap, statusCode);
			}

			if (StringUtils.isNotBlank(bodyResult)) {
				//log.debug(bodyResult);
				
				if (bodyResultType == HashMap.class) {
					Map<String, Object> mapResult = new ObjectMapper().readValue(bodyResult, HashMap.class);
					returnCode = MapUtils.getString(mapResult, "returnCode");
					
					if (StringUtils.isNotBlank(returnCode)) { //目前人壽api除了i0外都有returnCode
						if (StringUtils.equals("0", returnCode)) {
							if (MapUtils.isNotEmpty(mapResult) && mapResult.get("detail") != null) {
								result = (T) mapResult.get("detail");
							}
						} 
						else {
							log.error("url=" + url + ", param=" + param + ", returnCode=" + returnCode);
							String detail = MapUtils.getObject(mapResult, "detail").toString();
							String returnMsg = MapUtils.getString(mapResult, "returnMsg");
							infoMap.put("detail", new StringBuilder(detail));
							infoMap.put("returnMsg", new StringBuilder(returnMsg));
							if (infoMap.get("apiError").length() == 0) infoMap.get("apiError").append(apiid + " API returnCode=" + returnCode);
							throw new RRException(LifeApiStatusConstants.RETURN_CODE_ERROR, infoMap, returnCode);
						}
					} else {
						if (infoMap.get("apiError").length() == 0) infoMap.get("apiError").append(apiid + " API no returnCode");
					}
				}
				else if (bodyResultType == String.class && rtnType == String.class) { //目前人壽api除了i0外都有returnCode i0回String
					if (infoMap.get("apiError").length() == 0) infoMap.get("apiError").append(apiid + " API response error");
					result = (T) bodyResult;
				}

			} else {
				if (infoMap.get("apiError").length() == 0) infoMap.get("apiError").append(apiid + " API response nothing");
				throw new RRException(LifeApiStatusConstants.RETURN_BODY_IS_EMPTY, infoMap, url);
			}
		} catch (Exception e) {
			/*
			String errorMsg = e.getMessage();
			log.error(e.getMessage(), e);
			
			if ((e.getCause() != null && e.getCause().equals(SocketTimeoutException.class))
					|| StringUtils.contains(errorMsg.toLowerCase(), "timed out")
					|| StringUtils.contains(errorMsg.toLowerCase(), "timeout")) {
				throw e;
			}*/
			log.error(e.getMessage());
			throw e;
		} finally {
			
			try {
				
				String inputParam = null;
				if (StringUtils.isBlank(jsonString)) {
					inputParam = new JSONObject(param).toString();
				} else {
					inputParam = jsonString;
				}
				
				//ApiLog
				BotTpiCallLog commonApiLog = new BotTpiCallLog();
				BeanUtils.copyProperties(commonApiLog, ApiLogUtils.getBotTpiCallLog());
				
				if (commonApiLog != null) {
					apiLog = new BotTpiCallLog();
					BeanUtils.copyProperties(apiLog, commonApiLog);
					apiLog.setApiUrl(url);
					apiLog.setType(BotTpiCallLog.LogType.QUERY.toString());
//					apiLog.setInput(new JSONObject(param).toString());
//					apiLog.setOutput(bodyResult);
					// apiLog.setInput(StringUtils.substring(new JSONObject(param).toString(), 0, 30000));
					apiLog.setInput(StringUtils.substring(inputParam, 0, 30000));
					apiLog.setOutput(StringUtils.substring(bodyResult, 0, 30000));
					apiLog.setResult(apiResult);
					apiLog.setInsertTime(new Timestamp(System.currentTimeMillis()));
					apiLog.setCostTime(System.currentTimeMillis() - costTimeStart);
//					apiLog.setRespStatus(respStatus + (StringUtils.isBlank(returnCode) ? "" : ":" + returnCode));
					apiLog.setRespStatus(respStatus);
				}
				
				if (apiLog != null) {
					BotTpiCallLogService botTpiCallLogService = SpringUtil.getBean(BotTpiCallLogService.class);
					botTpiCallLogService.save(apiLog);
				}
			} catch(Exception e) {
				log.error("botTpiCallLogService.save error:" + e.getMessage());
			}
		}
		return result;
	}
}
