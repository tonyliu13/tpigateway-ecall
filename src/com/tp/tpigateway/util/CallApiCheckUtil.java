package com.tp.tpigateway.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.tp.tpigateway.exception.RRException;
import com.tp.tpigateway.model.common.enumeration.A05;
import com.tp.tpigateway.model.common.enumeration.H01;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeAService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeHService;
import com.tp.tpigateway.util.exception.LifeApiStatusConstants;

public class CallApiCheckUtil {

	/**
	 * 呼叫API前置參數檢核
	 * @param param
	 * @throws Exception
	 */
	public static void baseParamCheck(String urlKey, Map<String, String> param) throws Exception {
		
		ownPolicyNoCheck(urlKey, param);
	}
	
	/**
	 * 依情境代碼檢核有效保單
	 * @param param
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static void ownPolicyNoCheck(String urlKey, Map<String, String> param) throws Exception {
		
		String paramName = PropUtils.getProperty(urlKey + ".policy.param");
		if (StringUtils.isNotBlank(paramName)) {
			String policyNo = param.get(paramName);
			if (StringUtils.isNotBlank(policyNo)) {
				
				String scenarioCode = PropUtils.getProperty(urlKey + ".policy.scenario");
				List<Object> h01Result = CallApiCheckUtil.getH01DataByScenarioCode(StringUtils.isNotBlank(scenarioCode) ? scenarioCode : "S001");
				
				boolean ownPolicyNo = false;
				for (int i = 0; i < h01Result.size(); i++) {
					String policy_no = MapUtils.getString((Map) h01Result.get(i), H01.POLICY_NO.name());
					if (StringUtils.equals(policyNo, policy_no)) {
						ownPolicyNo = true;
						break;
					}
				}
				
				if (!ownPolicyNo) {
					Map<String, StringBuilder> infoMap = new HashMap<String, StringBuilder>();
					infoMap.put("apiError", new StringBuilder("not own " + policyNo));
					throw new RRException(LifeApiStatusConstants.NOT_OWN_POLICYNO, infoMap, "before api check.");
				}
			}
		}
	}
	
	/**
	 * 依情境代碼查詢H01結果
	 * @param ScenarioCode
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private static List<Object> getH01DataByScenarioCode(String ScenarioCode) throws Exception {
		
		String ID = ApiLogUtils.getBotTpiCallLog().getCustId();
		
		// 取得國籍
		CathayLifeTypeAService cathayLifeTypeAService = SpringUtil.getBean(CathayLifeTypeAService.class);
		// 20181025 by todd, 增加條件A05無法正常取得客戶國籍時，帶預設值TW
		// Map<String, Object> a05Result = cathayLifeTypeAService.a05(ID);
		Map<String, Object> a05Result = null;
		try {
    		a05Result = cathayLifeTypeAService.a05(ID);
		} catch (Exception e) {
		} finally {
			if (a05Result == null) a05Result = new HashMap<String, Object>();
			if (StringUtils.isBlank(MapUtils.getString(a05Result, "NATION_CODE"))) {
				a05Result.put("NATION_CODE", "TW");
			}
		}
		String nationCode = MapUtils.getString(a05Result, A05.NATION_CODE.name());
				
		String cacheKey = "I1_" + ID + "_" + nationCode + "_" + ScenarioCode;

		List<Object> tmp = (List<Object>) LocalCache.get(cacheKey);
		if (tmp != null && !tmp.isEmpty()) {
			return tmp;
		}

		CathayLifeTypeHService cathayLifeTypeHService = SpringUtil.getBean(CathayLifeTypeHService.class);
		tmp = (List<Object>) cathayLifeTypeHService.h01(ID, nationCode, ScenarioCode);
		LocalCache.register(cacheKey, tmp);
		return tmp;
	}
}
