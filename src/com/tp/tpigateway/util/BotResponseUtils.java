package com.tp.tpigateway.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CImageDataItem;
import com.tp.tpigateway.model.common.BotMessageVO.CImageTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CLinkListItem;
import com.tp.tpigateway.model.common.BotMessageVO.CTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;

public class BotResponseUtils {
	private static final Logger log = LoggerFactory.getLogger(BotResponseUtils.class);

	/**
	 * 純文字
	 */
	public static class Text {

		/**
		 * 純文字
		 * 
		 * @param botMessageService
		 * @param botTpiReplyTextService
		 * @param vo
		 * @param role
		 * @param textIdsOrTexts
		 */
		public static void setText(BotMessageService botMessageService, BotTpiReplyTextService botTpiReplyTextService,
				BotMessageVO vo, String role, String... textIdsOrTexts) {
			setText(botMessageService, botTpiReplyTextService, vo, role, true, textIdsOrTexts);
		}

		/**
		 * 純文字
		 * 
		 * @param botMessageService
		 * @param botTpiReplyTextService
		 * @param vo
		 * @param role
		 * @param useWording
		 * @param textIdsOrTexts
		 */
		public static void setText(BotMessageService botMessageService, BotTpiReplyTextService botTpiReplyTextService,
				BotMessageVO vo, String role, boolean useWording, String... textIdsOrTexts) {
			for (String textIdOrText : textIdsOrTexts) {
				if (useWording) {
					textIdOrText = getText(botTpiReplyTextService, role, textIdOrText);
				}
				botMessageService.setTextResult(vo, textIdOrText);
			}
		}

		/**
		 * 純文字(有替代變數)
		 * 
		 * @param botMessageService
		 * @param botTpiReplyTextService
		 * @param vo
		 * @param role
		 * @param replaceParams
		 * @param textIds
		 */
		public static void setTextWithReplace(BotMessageService botMessageService,
				BotTpiReplyTextService botTpiReplyTextService, BotMessageVO vo, String role,
				Map<String, String> replaceParams, String textId) {

			boolean doReplace = replaceParams != null && !replaceParams.isEmpty();

			if (doReplace) {
				String replyText = getText(botTpiReplyTextService, role, textId);
				botMessageService.setTextResultWithReplace(vo, replyText, replaceParams);
			} else {
				setText(botMessageService, botTpiReplyTextService, vo, role, textId);
			}
		}

		/**
		 * 多組純文字(有替代變數)
		 * 
		 * @param botMessageService
		 * @param botTpiReplyTextService
		 * @param vo
		 * @param role
		 * @param multiReplaceParams
		 * @param textIds
		 */
		public static void setTextWithReplace(BotMessageService botMessageService,
				BotTpiReplyTextService botTpiReplyTextService, BotMessageVO vo, String role,
				Map<String, Map<String, String>> multiReplaceParams, String... textIds) {

			boolean doReplace = multiReplaceParams != null && !multiReplaceParams.isEmpty();

			for (String textId : textIds) {
				setTextWithReplace(botMessageService, botTpiReplyTextService, vo, role,
						doReplace ? multiReplaceParams.get(textId) : null, textId);
			}
		}
	}

	/**
	 * PDF牌卡
	 */
	public static class PDFCard {
		
        
		/**
		 * Carousel Card
		 * 
		 * @param botMessageService
		 * @param vo
		 * @param cardList
		 * @param title
		 * @param texts
		 * @param lifeLinks
		 * @return
		 */
		public static void setPDFCards(BotMessageService botMessageService, BotMessageVO vo,
				Map<String, Object>... carouselCards) {
			setPDFCards(botMessageService, vo, null, carouselCards);
		}
		
		/**
		 * Carousel Card
		 * 
		 * @param botMessageService
		 * @param vo
		 * @param cardList
		 * @param title
		 * @param texts
		 * @param lifeLinks
		 * @return
		 */
		public static void setPDFCards(BotMessageService botMessageService, BotMessageVO vo,
				List<Map<String, Object>> cardList, Map<String, Object>... pdfCards) {

			boolean onlyOneData = cardList == null;

			if (onlyOneData) {
				cardList = new ArrayList<Map<String, Object>>();
			}
			for(Map<String, Object> pdfCard: pdfCards) {
				cardList.add(pdfCard);	
			}

			botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
		}		
		
		//下載PDF牌卡
	    public static Map<String, Object> createPDFCard(BotMessageVO vo, LifeLink link, String imageText) {
			//操作流程圖PDF
			CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_250);
			
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CB06);
			//
			cImageData.setCImageTextType(3);
			
			List<Map<String,Object>> cImageTextList = new ArrayList<>();
			CImageTextItem cImageText = vo.new CImageTextItem();
			cImageText.setCImageText(imageText);
			cImageTextList.add(cImageText.getCImageTexts());
			cImageData.setCImageTexts(cImageTextList);
			
			cards.setCImageData(cImageData.getCImageDatas());
			
			//link
			cards.setCLinkType(1);
			List<Map<String, Object>> cLinkList = getCLinkList(vo, link);
			cards.setCLinkList(cLinkList);
			
			log.info("cards=" + new JSONObject(cards).toString());
			
			return cards.getCards();
		}
		
		private static List<Map<String, Object>> getCLinkList(BotMessageVO vo, LifeLink link) {
			List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
			cLinkContentList.add(getCLinkListItem(vo, link).getCLinkList());
			return cLinkContentList;
		}
		

		
		private static CLinkListItem getCLinkListItem(BotMessageVO vo, LifeLink link) {
			CLinkListItem cLinkContent = vo.new CLinkListItem();
			cLinkContent.setClAction(link.getAction());
			cLinkContent.setClText(link.getText());
			cLinkContent.setClAlt(link.getAlt());
			cLinkContent.setClReply(link.getReply());
			cLinkContent.setClUrl(link.getUrl());
			return cLinkContent;
		}			
		
		
		

	}
	
	/**
	 * 牌卡
	 */
	public static class Card {
		public static CarouselCard createCarouselCard(String title, String[] texts, LifeLink[] lifeLinks) {
			return new BotResponseUtils.Card().new CarouselCard(title, texts, lifeLinks);
		}

		/**
		 * 文字牌卡
		 */
		public class CarouselCard {
			private String title;

			private String[] texts;

			private LifeLink[] lifeLinks;

			private CardWidth width = CardWidth._200;

			
			private TextType textType = TextType.一般牌卡2;

			public CarouselCard(String title, String[] texts, LifeLink[] lifeLinks) {
				this.title = title;
				this.texts = texts;
				this.lifeLinks = lifeLinks;
			}

			public String getTitle() {
				return title;
			}

			public void setTitle(String title) {
				this.title = title;
			}

			public String[] getTexts() {
				return texts;
			}

			public void setTexts(String[] texts) {
				this.texts = texts;
			}

			public LifeLink[] getLifeLinks() {
				return lifeLinks;
			}

			public void setLifeLinks(LifeLink[] lifeLinks) {
				this.lifeLinks = lifeLinks;
			}

			public CardWidth getWidth() {
				return this.width;
			}

			public void setWidth(CardWidth width) {
				this.width = width;
			}
			
			public void setWidthByText() {
				boolean wideCard = false;
				for(String text: texts) {
					if(StringUtils.isNotBlank(text) && text.length()>10) {
						wideCard = true;
					}	
				}
				
				if(wideCard) {
					this.width = CardWidth._250;
				} else {
					this.width = CardWidth._200;
				} 				
			}

			public TextType getTextType() {
				return textType;
			}

			public void setTextType(TextType textType) {
				this.textType = textType;
			}
		}

		/**
		 * 連結顯示類型
		 */
		public enum LinkType {
			連結(1), 按鈕(2);

			private int value;

			private LinkType(int value) {
				this.value = value;
			}

			public int getValue() {
				return this.value;
			}
		}

		/**
		 * 牌卡內容顯示格式
		 */
		public enum TextType {
			/** 左標題cLabel+右項目值cText */
			一般牌卡1("1"), 金額牌卡("2"), 多標題牌卡("3"),
			/** (1+2)Highlight標題 */
			混合牌卡("4"),
			/** 大標題cTitle +內容cText */
			一般牌卡2("5");

			private String value;

			private TextType(String value) {
				this.value = value;
			}

			public String getValue() {
				return this.value;
			}
		}

		/**
		 * 牌卡寬度
		 */
		public enum CardWidth {
			_200(BotMessageVO.CWIDTH_200), _250(BotMessageVO.CWIDTH_250);

			private String value;

			private CardWidth(String value) {
				this.value = value;
			}

			public String getValue() {
				return this.value;
			}
		}

		/**
		 * Carousel Card
		 * 
		 * @param botMessageService
		 * @param vo
		 * @param cardList
		 * @param title
		 * @param texts
		 * @param lifeLinks
		 * @return
		 */
		public static void setCarouselCards(BotMessageService botMessageService, BotMessageVO vo,
				CarouselCard... carouselCards) {
			setCarouselCards(botMessageService, vo, null, carouselCards);
		}

		/**
		 * Carousel Card
		 * 
		 * @param botMessageService
		 * @param vo
		 * @param cardList
		 * @param title
		 * @param texts
		 * @param lifeLinks
		 * @return
		 */
		public static void setCarouselCards(BotMessageService botMessageService, BotMessageVO vo,
				List<Map<String, Object>> cardList, CarouselCard... carouselCards) {

			boolean onlyOneData = cardList == null;

			if (onlyOneData) {
				cardList = new ArrayList<Map<String, Object>>();
			}

			for (CarouselCard carouselCard : carouselCards) {
				CardItem cards = vo.new CardItem();
				cards.setCWidth(carouselCard.getWidth().getValue());
				cards.setCTextType(carouselCard.getTextType().getValue());

				String title = carouselCard.getTitle();
				if (StringUtils.isNotBlank(title)) {
					cards.setCTitle(title);
				}

				String[] texts = carouselCard.getTexts();
				if (texts != null && texts.length > 0) {
					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
					for (String text : texts) {
						CTextItem cText = vo.new CTextItem();
						cText.setCText(text);
						cTextList.add(cText.getCTexts());
					}
					cards.setCTexts(cTextList);
				}

				LifeLink[] lifeLinks = carouselCard.getLifeLinks();
				if (lifeLinks != null && lifeLinks.length > 0) {
					cards.setCLinkType(Card.LinkType.連結.getValue());
					cards.setCLinkList(getCLinkList(vo, lifeLinks));
				}

				cardList.add(cards.getCards());
			}

			if (onlyOneData) {
				botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
			}
		}

		/**
		 * Carousel Card
		 * 
		 * @param botMessageService
		 * @param vo
		 * @param titles
		 * @param texts
		 * @param lifeLinks
		 */
		public static void setCarouselCards(BotMessageService botMessageService, BotMessageVO vo,
				List<CarouselCard> carouselCards) {
			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

			for (CarouselCard carouselCard : carouselCards) {
				setCarouselCards(botMessageService, vo, cardList, carouselCard);
			}

			botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CAROUSEL, cardList);
		}

		private static List<Map<String, Object>> getCLinkList(BotMessageVO vo, LifeLink... lifeLinks) {
			List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();

			for (LifeLink lifeLink : lifeLinks) {
				cLinkContentList.add(getCLinkListItem(vo, lifeLink).getCLinkList());
			}
			return cLinkContentList;
		}

		private static CLinkListItem getCLinkListItem(BotMessageVO vo, LifeLink link) {
			CLinkListItem cLinkContent = vo.new CLinkListItem();
			cLinkContent.setClAction(link.getAction());
			cLinkContent.setClText(link.getText());
			cLinkContent.setClAlt(link.getAlt());
			cLinkContent.setClReply(link.getReply());
			cLinkContent.setClUrl(link.getUrl());
			return cLinkContent;
		}
	}

	public static class QuickReply {

		/**
		 * Quick Reply
		 * 
		 * @param botMessageService
		 * @param vo
		 * @param lifeLinks
		 * 
		 */
		public static void setQuickReply(BotMessageService botMessageService, BotMessageVO vo, LifeLink... lifeLinks) {
			botMessageService.setLifeLinkListResult(vo, Arrays.asList(lifeLinks));
		}
	}

	/**
	 * 貼圖
	 */
	public static class Sticker {

		/**
		 * 貼圖
		 * 
		 * @param botMessageService
		 * @param vo
		 * @param imageName
		 */
		public static void setSticker(BotMessageService botMessageService, BotMessageVO vo, String imageName) {
			botMessageService.setImageResult(vo, imageName);
		}
	}

	/**
	 * 注意事項
	 */
	public static class Note {

		/**
		 * 注意事項(使用預設標題)
		 * 
		 * @param botMessageService
		 * @param botTpiReplyTextService
		 * @param vo
		 * @param role
		 * @param textId
		 */
		public static void setNote(BotMessageService botMessageService, BotTpiReplyTextService botTpiReplyTextService,
				BotMessageVO vo, String role, String textId) {
			setNote(botMessageService, botTpiReplyTextService, vo, role, null, textId);
		}

		/**
		 * 注意事項
		 * 
		 * @param botMessageService
		 * @param botTpiReplyTextService
		 * @param vo
		 * @param role
		 * @param title
		 * @param textId
		 */
		public static void setNote(BotMessageService botMessageService, BotTpiReplyTextService botTpiReplyTextService,
				BotMessageVO vo, String role, String title, String textId) {
			String replyText = getText(botTpiReplyTextService, role, textId);
			botMessageService.setNoteResult(vo, title == null ? "注意事項" : title, replyText);
		}

		/**
		 * 多組注意事項
		 * 
		 * @param botMessageService
		 * @param botTpiReplyTextService
		 * @param vo
		 * @param role
		 * @param titleAndTextIdArray
		 */
		public static void setNote(BotMessageService botMessageService, BotTpiReplyTextService botTpiReplyTextService,
				BotMessageVO vo, String role, String[]... titleAndTextIdArray) {

			for (int i = 0; i < titleAndTextIdArray.length; i++) {
				String[] titleAndTextId = titleAndTextIdArray[i];
				String title = titleAndTextId[0];
				if (title == null) {
					title = "注意事項";
					if (i > 0) {
						title += (i + 1);
					}
				}
				String textId = titleAndTextId[1];

				setNote(botMessageService, botTpiReplyTextService, vo, role, title, textId);
			}
		}
	}

	/**
	 * 從wording取得文字
	 * 
	 * @param botTpiReplyTextService
	 * @param role
	 * @param textId
	 * @return
	 */
	private static String getText(BotTpiReplyTextService botTpiReplyTextService, String role, String textId) {
		return botTpiReplyTextService.getReplyText(role, textId);
	}
}