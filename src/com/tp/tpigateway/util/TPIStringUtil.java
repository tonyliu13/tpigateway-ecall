package com.tp.tpigateway.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.UUID;

/**
 * 字串處理工具
 */
public final class TPIStringUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(TPIStringUtil.class);

	public static final String getUUID() {
		return UUID.randomUUID().toString();
	}
	
	public static final String getUUIDNoSeparator() {
	    return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 將參數串接成字串後回傳
	 *
	 * @param values
	 * @return
	 */
	public static String join(Object... values) {
		StringBuffer buf = new StringBuffer();
		for (Object value : values) {
			if (value != null) {
				buf.append(value);
			}
		}
		return buf.toString();
	}

	/**
	 * 將財產編號(分號)數字格式化
	 *
	 * @param number
	 * @return
	 */
	public static String formatPpSemNo(Number number) {
		return TPIStringUtil.numberFormat(number, "0000000");
	}

	/**
	 * 將數字補成所要的模式型態<br> ex : number :123 , pattern :"0000", return "0123"
	 *
	 * @param number
	 * @param pattern
	 * @return
	 */
	public static String numberFormat(Number number, String pattern) {
		DecimalFormat nf = new DecimalFormat(pattern);
		return nf.format(number);
	}
	
	/**
	 * 將數字補成所要的模式型態<br> ex : number :123 , pattern :"0000", return "0123"
	 *
	 * @param number
	 * @param pattern
	 * @return
	 */
	public static String numberFormat(Number number, String pattern, int decimalLen) {
		DecimalFormat nf = new DecimalFormat(pattern);
		String strNum = nf.format(number);
		
		String strFmt = formatStrToDecimal(strNum, decimalLen);
		
		return strFmt;
	}

	/**
	 * 將數字格式化為千分位表示法。
	 *
	 * @param value
	 * @return
	 */
	public static String formatNumber(Number value) {
		
		NumberFormat nf = NumberFormat.getInstance(); 
		return new String(nf.format(value));
	}
	
	private final static String[] hex = {"00", "01", "02", "03", "04", "05",
		"06", "07", "08", "09", "0A", "0B", "0C", "0D", "0E", "0F", "10",
		"11", "12", "13", "14", "15", "16", "17", "18", "19", "1A", "1B",
		"1C", "1D", "1E", "1F", "20", "21", "22", "23", "24", "25", "26",
		"27", "28", "29", "2A", "2B", "2C", "2D", "2E", "2F", "30", "31",
		"32", "33", "34", "35", "36", "37", "38", "39", "3A", "3B", "3C",
		"3D", "3E", "3F", "40", "41", "42", "43", "44", "45", "46", "47",
		"48", "49", "4A", "4B", "4C", "4D", "4E", "4F", "50", "51", "52",
		"53", "54", "55", "56", "57", "58", "59", "5A", "5B", "5C", "5D",
		"5E", "5F", "60", "61", "62", "63", "64", "65", "66", "67", "68",
		"69", "6A", "6B", "6C", "6D", "6E", "6F", "70", "71", "72", "73",
		"74", "75", "76", "77", "78", "79", "7A", "7B", "7C", "7D", "7E",
		"7F", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89",
		"8A", "8B", "8C", "8D", "8E", "8F", "90", "91", "92", "93", "94",
		"95", "96", "97", "98", "99", "9A", "9B", "9C", "9D", "9E", "9F",
		"A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "AA",
		"AB", "AC", "AD", "AE", "AF", "B0", "B1", "B2", "B3", "B4", "B5",
		"B6", "B7", "B8", "B9", "BA", "BB", "BC", "BD", "BE", "BF", "C0",
		"C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "CA", "CB",
		"CC", "CD", "CE", "CF", "D0", "D1", "D2", "D3", "D4", "D5", "D6",
		"D7", "D8", "D9", "DA", "DB", "DC", "DD", "DE", "DF", "E0", "E1",
		"E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "EA", "EB", "EC",
		"ED", "EE", "EF", "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7",
		"F8", "F9", "FA", "FB", "FC", "FD", "FE", "FF"};
	private final static byte[] val = {0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x00, 0x01,
		0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
		0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F};

	/**
	 * 編碼
	 *
	 * @param s
	 * @return
	 */
	public static String escape(String s) {
		StringBuffer sbuf = new StringBuffer();
		int len = s.length();
		for (int i = 0; i < len; i++) {
			int ch = s.charAt(i);
			if ('A' <= ch && ch <= 'Z') {
				sbuf.append((char) ch);
			} else if ('a' <= ch && ch <= 'z') {
				sbuf.append((char) ch);
			} else if ('0' <= ch && ch <= '9') {
				sbuf.append((char) ch);
			} else if (ch == '-' || ch == '_' || ch == '.' || ch == '!'
					|| ch == '~' || ch == '*' || ch == '\'' || ch == '('
					|| ch == ')') {
				sbuf.append((char) ch);
			} else if (ch <= 0x007F) {
				sbuf.append('%');
				sbuf.append(hex[ch]);
			} else {
				sbuf.append('%');
				sbuf.append('u');
				sbuf.append(hex[(ch >>> 8)]);
				sbuf.append(hex[(0x00FF & ch)]);
			}
		}
		return sbuf.toString();
	}

	/**
	 * 解碼 說明：本方法保證 不論參數s是否經過escape()編碼，均能得到正確的「解碼」結果
	 *
	 * @param s
	 * @return
	 */
	public static String unescape(String s) {
		StringBuffer sbuf = new StringBuffer();
		int i = 0;
		int len = s.length();
		while (i < len) {
			int ch = s.charAt(i);
			if ('A' <= ch && ch <= 'Z') {
				sbuf.append((char) ch);
			} else if ('a' <= ch && ch <= 'z') {
				sbuf.append((char) ch);
			} else if ('0' <= ch && ch <= '9') {
				sbuf.append((char) ch);
			} else if (ch == '-' || ch == '_' || ch == '.' || ch == '!'
					|| ch == '~' || ch == '*' || ch == '\'' || ch == '('
					|| ch == ')') {
				sbuf.append((char) ch);
			} else if (ch == '%') {
				int cint = 0;
				if ('u' != s.charAt(i + 1)) {
					cint = (cint << 4) | val[s.charAt(i + 1)];
					cint = (cint << 4) | val[s.charAt(i + 2)];
					i += 2;
				} else {
					cint = (cint << 4) | val[s.charAt(i + 2)];
					cint = (cint << 4) | val[s.charAt(i + 3)];
					cint = (cint << 4) | val[s.charAt(i + 4)];
					cint = (cint << 4) | val[s.charAt(i + 5)];
					i += 5;
				}
				sbuf.append((char) cint);
			} else {
				sbuf.append((char) ch);
			}
			i++;
		}
		return sbuf.toString();
	}

	/**
	 * mark掉指定長度(後幾碼)
	 *
	 * @param unmaskData 欲mark變數
	 * @param maskLength mark長度
	 * @return
	 */
	public static String maskPartData(String unmaskData, int maskLength) {
		if (StringUtils.isBlank(unmaskData)) {
			return unmaskData;
		}
		if (maskLength < 1) {
			return unmaskData;
		}
		StringBuilder sbResult = new StringBuilder();
		for (int i = 0; i < unmaskData.length(); i++) {
			if (i >= (unmaskData.length() - maskLength)) {
				sbResult.append("x");
			} else {
				sbResult.append(unmaskData.charAt(i));
			}
		}
		return sbResult.toString();
	}
	
	/**
	 * 字串格式化，長度不夠向右補空白
	 * @param fmtStr － 字串
	 * @param strLen - 字串長度，不夠向右補空白
	 * @return
	 */
	public static String fmtStringAddBlank(String fmtStr, int len) {
		String pattern = "%1$-"+len+"s";
		String returnStr = String.format(pattern,fmtStr);
		
		return returnStr;
	}
	
	public static final String ADD_RIGHT = "R";
	public static final String ADD_LEFT = "L";
	
	/**
	 * 字串格式化，長度不夠向左補0
	 * @param str － 要格式化的字串
	 * @param strLength - 回傳字串的長度
	 * @param strRL － L：向左補, R：向右補
	 * @param symbol － 希望補上的字串ex. 0或空白
	 * @return
	 */
	public static String addZeroForNum(String str, int strLength, String strRL, String symbol) {
	    int strLen = str.length();
	    if (strLen < strLength) {
	        while (strLen < strLength) {
	            StringBuffer sb = new StringBuffer();
	            
	            if(ADD_RIGHT.equals(strRL)){
		            sb.append(str).append(symbol);//右補
		            str = sb.toString();
		            strLen = str.length();
		            
	            }else{
		            sb.append(symbol).append(str);// 左補
		            str = sb.toString();
		            strLen = str.length();
	            }

	        }
	    }

	    return str;
	}
	
	/**
	 * 補上小數 ex. fmtStr=100.0, decimalLen=4 ==> 100.0000
	 * @param fmtStr
	 * @param decimalLen：小數位長度
	 * @return
	 */
	public static String formatStrToDecimal(String fmtStr, int decimalLen){
		String str = fmtStr;
		
		if(decimalLen > 0){
			if(!str.contains(".")){ //傳入fmtStr為整數,先補成有1位小數
				str = fmtStr+".0";
			}
			
			String[] strArr = str.split("\\.");
			
			if(strArr != null && strArr.length > 1){
				String strDecimal = strArr[1]; //小數位
				
				int iStrDecimalLen = strDecimal.length();//小數位數
				
				while (iStrDecimalLen < decimalLen) {
		            StringBuffer sb = new StringBuffer();
		            sb.append(str).append("0");//右補0
		            str = sb.toString();
		            
		            strArr = str.split("\\.");
		            iStrDecimalLen = strArr[1].length();
		        }
			}
		}
		
		return str;
	}
	
	/**
	 * 轉換成16進位字串
	 * @param b
	 * @return
	 */
	public static String byte2HexStr(byte[] b) {
		String stmp="";
	    StringBuilder sb = new StringBuilder("");
	    for (int n=0;n<b.length;n++) {
	    	stmp = Integer.toHexString(b[n] & 0xFF);
	        sb.append((stmp.length()==1)? "0"+stmp : stmp);
	        //sb.append(" ");
	    }
	    return sb.toString().toUpperCase().trim();
	 }
	/**
	 * 將map string轉成json string
	 * @param str
	 * @return
	 */
	public static String mapStrToJsonStr(String str){
		str = "{\""+str.substring(1,str.length()-1)+"\"}";
		str = str.replaceAll("=", "\":\"").replaceAll(", ", "\", \"");
		return str;
	}
	
	/**
	 * Ex. 3,000.30 -> 3000.30 ; 600,000 -> 600000
	 * 
	 */
	public static String stripeNumStrWithComman(String numStr){
		
		return numStr.replaceAll(",", "");
		
	}
	
	/*
	 * Ex. 4000 -> 4,000, 5000.000 -> 5,000.00 (when input dcimalLen=2)
	 */
	public static String formatNumStrWithCommaAndDecimal(String numStr , int decimalLen){
		
		numStr = formatNumber(new Double(numStr));
		numStr = formatStrToDecimal(numStr,decimalLen);
		
		return numStr;
	}
	
	public static String clearDollarsString(String str) {
		if (StringUtils.isNotBlank(str) && str.length() > 0) {
			final String char1 = StringUtils.substring(str, 0, 1);
			if (StringUtils.equalsAny(char1, "+", "-")) {
				int f = 1;
				if (StringUtils.equals(char1, "-")) {
					f = -1;
				}
				final String dollars = StringUtils.substring(str, 1);
				Long lDollars = Long.parseLong(dollars);
				lDollars *= f;
				return lDollars.toString();
			} else {
				// 沒有正負號，直接轉型
				Long lDollars = Long.parseLong(str);
				return lDollars.toString();
			}
		}
		return "";
	}
	
	/**
	 * 轉全形+base64編碼
	 * @param string
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String formatText(String string) throws UnsupportedEncodingException{
		if(StringUtils.isNotBlank(string)){
			String half = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~`!@#$%^&*()-_+={[}]|\\:;\"'<,>.?/ ";
			char full[] = new char[]{'０','１','２','３','４','５','６','７','８','９','Ａ','Ｂ','Ｃ','Ｄ','Ｅ','Ｆ','Ｇ','Ｈ','Ｉ','Ｊ','Ｋ','Ｌ','Ｍ','Ｎ','Ｏ','Ｐ','Ｑ','Ｒ','Ｓ','Ｔ','Ｕ','Ｖ','Ｗ','Ｘ','Ｙ','Ｚ','ａ','ｂ','ｃ','ｄ','ｅ','ｆ','ｇ','ｈ','ｉ','ｊ','ｋ','ｌ','ｍ','ｎ','ｏ','ｐ','ｑ','ｒ','ｓ','ｔ','ｕ','ｖ','ｗ','ｘ','ｙ','ｚ','～','‘','！','＠','＃','＄','％','︿','＆','＊','（','）','－','＿','＋','＝','｛','〔','｝','〕','｜','＼','：','；','”','’','＜','，','＞','．','？','／','　'};
			for (int i = 0; i < string.length(); i++){
				char ch = string.charAt(i);
				if (half.indexOf(String.valueOf(ch))>=0){
					string = string.replace(ch, full[half.indexOf(String.valueOf(ch))]);
				}
			}
			char[] ca = string.toCharArray();
			for(int i = 0; i < ca.length; i++){
				if(ca[i] >= 33 && ca[i] <= 126){
					ca[i] = (char) (ca[i] + 65248);
				}
			}
			return DESUtil.toBase64String((new String(ca)).getBytes("utf-8"));
		}else{
			return "";
		}
	}
	public static HashMap mapStringtoMap(String mapStr){
		HashMap map = new HashMap();
		mapStr = mapStr.substring(1,mapStr.length()-1);
		String[] entryArr = mapStr.split(", ");
		for(int i = 0; i < entryArr.length; i++){
			String[] entry = entryArr[i].split("=",2);
			map.put(entry[0], entry[1]);
		}
		return map;
	}
	
	/**
	 * 信用卡卡號末四碼
	 * (取得字串最後四個字)
	 * @return
	 */
	public static String hideCardNoKeepLastFour(String cardNo) {
		int length = cardNo.length();
		if (StringUtils.isNotBlank(cardNo) && length >= 4) {
			return StringUtils.substring(cardNo, length-4);
		}
		return "";
	}
	
	/**
	 * 信用卡卡號format格式與隱碼
	 * (1234-****-****-1234)
	 * @return
	 */
	public static String cardNoFormat(String cardNo) {
		if (StringUtils.isNotBlank(cardNo) && cardNo.length() == 16) {
			//cardNo = cardNo.substring(0,4)+"-****-****-"+cardNo.substring(12);
			cardNo = cardNo.substring(0,4)+"********"+cardNo.substring(12);
			return cardNo;
		}
		return "";
	}
	
	/**
	 * 節取字串長度
	 * @param str
	 * @param length
	 * @return
	 */
	public static String cutString(String str, int length) {
		if (StringUtils.isNotBlank(str)) {
			int len = str.length();
			if (len <= length) {
				return StringUtils.substring(str, 0);
			} else {
				return StringUtils.substring(str, 0, length);
			}
		} else {
			return str;
		}
	}
	
	public static String trimParameterString(String param) {
		param = StringUtils.trim(param);
		logger.debug("<trimParameterString> param="+param);
		return param;
	}

	/**
	 * 姓名遮罩
	 * @param custName
	 * @return
	 */
	public static String custNameMark(String custName) {
		StringBuilder strBuilder = new StringBuilder();
		
		if(StringUtils.isNotBlank(custName)) {
			String name = StringUtils.trim(custName);
			int firstInt = 0;
			int endInt = 0;
			
			//英文
			if(name.matches("^[a-zA-Z]{1,}[a-zA-Z\\.\\-\\, ]*[a-zA-Z]{1,}$")) {
				if(name.length()>8) {
					firstInt = 4;
					endInt = 4;
				}else {
					firstInt = 2;
					endInt = 2;
				}
				//中文
			}else {
				if(name.length()==3) {
					firstInt = 1;
					endInt = 1;
				}else if(name.length()==2) {
					firstInt = 1;
					endInt = 0;
				}else {
					firstInt = 2;
					endInt = 1;
				}
			}
			
			String firstStr = name.substring(0, firstInt);
			String endStr = name.substring(name.length()-endInt, name.length());
			String subStr = name.substring(firstInt, name.length()-endInt);
			
			strBuilder.append(firstStr);
			for(int i=0;i<subStr.length();i++) {
				strBuilder.append("*");
			}
			strBuilder.append(endStr);
		}
		
		return strBuilder.toString();
	}
}
