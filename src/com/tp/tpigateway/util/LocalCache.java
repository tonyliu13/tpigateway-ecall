package com.tp.tpigateway.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalCache {
	private static Logger log = LoggerFactory.getLogger(LocalCache.class);

	private static boolean isInfo = log.isInfoEnabled();

	private static final int autoRemoveInterval = ExpiredTime._5mins;

	private static final int defaultExpiredTime = ExpiredTime._5mins;

	private static Map<Object, Object> cache = new ConcurrentHashMap<Object, Object>();

	private static Timer timer;

	static {
		autoRemoveExpiredData();
	}

	/**
	 * 註冊
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public static synchronized <T> T register(Object key, T value) {
		return register(key, value, defaultExpiredTime);
	}

	/**
	 * <pre>
	 * 註冊
	 * 
	 * @param key
	 * @param value
	 * @param expiredTime
	 *            有效時間(ms)，預設為5mins
	 *            <=0表示不做Cache
	 * @return
	 * </pre>
	 */
	public static synchronized <T> T register(Object key, T value, int expiredTime) {
		if (expiredTime <= 0) {
			return value;
		}

		if (key == null) {
			log.error("key:" + key + " can't be null");
			return value;
		}

		if (value == null) {
			log.error("value:" + value + " can't be null");
			return value;
		}

		if (cache.containsKey(key)) {
			log.error("key:" + key + " already exists");
			return (T) get(key);
		}

		Map<String, Object> tmp = new HashMap<String, Object>();
		tmp.put("value", value);
		tmp.put("insertTime", new Date().getTime());
		tmp.put("expiredTime", expiredTime);
		cache.put(key, tmp);

		return value;
	}

	/**
	 * 取得資料
	 * 
	 * @param key
	 * @return
	 */
	public static synchronized Object get(Object key) {
		if (cache.containsKey(key)) {
			Map<String, Object> tmp = (Map<String, Object>) cache.get(key);
			return tmp.get("value");
		}

		return null;
	}

	/**
	 * 移除
	 * 
	 * @param key
	 */
	public static void remove(Object key) {
		if (cache.containsKey(key)) {
			cache.remove(key);
		}
	}

	/**
	 * 定期移除過期的資料
	 */
	private static void autoRemoveExpiredData() {
		timer = new Timer(LocalCache.class.getName(), true);
		timer.schedule(new TimerTask() {
			public void run() {
				long currentTime = new Date().getTime();

				if (isInfo) {
					log.info("=== [autoRemoveExpiredData] START ===");
				}

				int count = 0;
				for (Entry<Object, Object> e : cache.entrySet()) {
					Map<String, Object> tmp = (Map<String, Object>) e.getValue();
					if (currentTime - (Long) tmp.get("insertTime") > (Integer) tmp.get("expiredTime")) {
						remove(e.getKey());
						count++;
					}
				}

				if (isInfo) {
					log.info("=== [autoRemoveExpiredData] remove " + count + " data");
					log.info("=== [autoRemoveExpiredData] END ===");
				}
			}
		}, autoRemoveInterval, autoRemoveInterval);
	}

	/**
	 * 常用時間
	 */
	public class ExpiredTime {
		public static final int _1sec = 1000;
		public static final int _10secs = 10 * _1sec;
		public static final int _30secs = 30 * _1sec;

		public static final int _1min = 60 * _1sec;
		public static final int _5mins = 5 * _1min;
		public static final int _10mins = 10 * _1min;

		public static final int _1hr = 60 * _1min;
		public static final int _12hrs = 12 * _1hr;

		public static final int _1day = 24 * _1hr;
	}
}
