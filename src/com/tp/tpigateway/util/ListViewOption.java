package com.tp.tpigateway.util;

import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.CImageDataItem;
import com.tp.tpigateway.model.common.BotMessageVO.CTextItem;
import com.tp.tpigateway.model.common.BotMessageVO.CardItem;
import com.tp.tpigateway.service.life.lifeapi.impl.CathayLifeTypeIServiceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * for S007,S008共用
 */
public enum ListViewOption {

    尚未擁有的保障項目("尚未擁有的保障項目"),
    癌症醫療("癌症醫療"),
    失能給付("失能給付"),
    重疾給付("重疾給付"),
    疾病醫療("疾病醫療"),
    身故給付("身故給付"),
    長照給付("長照給付"),
    意外醫療("意外醫療"),
    實支實付需收據("實支實付需收據"),
    手術醫療("手術醫療");

    private static Logger log = LoggerFactory.getLogger(ListViewOption.class);

    private final String value;

    private static final Map<String, ListViewOption> valMap;

    static {
        ListViewOption[] options = ListViewOption.values();
        valMap = new HashMap<>();
        for (ListViewOption option : options) {
            valMap.put(option.getValue(), option);
        }
    }

    private ListViewOption(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ListViewOption getValue(String value) {
        return valMap.get(value);
    }

    public static Map<String, Boolean> getInitDataMap() {
        Map<String, Boolean> dataMap = new HashMap<>();
        for (ListViewOption e : ListViewOption.values()) {
            //Boolean: [保障金額]有資料的項目 -> true 反之 false
            dataMap.put(e.name(), false);
        }

        return dataMap;
    }

    public static Map<String, Boolean> getDataMap(List<CathayLifeTypeIServiceImpl.CoverageItem> coverageItems) {
        Map<String, Boolean> dataMap = ListViewOption.getInitDataMap();

        if (CollectionUtils.isNotEmpty(coverageItems)) {
            for (CathayLifeTypeIServiceImpl.CoverageItem coverageItem : coverageItems) {
                String mainCoverage = coverageItem.getMainCoverage();
                String amount = coverageItem.getAmount();

                if (dataMap.get(mainCoverage) != null && dataMap.containsKey(mainCoverage)) {
                    if (StringUtils.isNotEmpty(amount) && !"0".equals(amount)) {
                        dataMap.put(mainCoverage, true);
                    }
                }
            }
        }

        dataMap.put(ListViewOption.尚未擁有的保障項目.name(), isShowNoItem(dataMap));

        return dataMap;
    }

    private static boolean isShowNoItem(Map<String, Boolean> dataMap) {
        //是否要秀出[尚未擁有的保障項目]這個選項(如果其他9個保障項都秀的話就不應該再顯示[尚未擁有的保障項目])
        boolean isShowNoItem = false;

        for(ListViewOption option : ListViewOption.values()) {
            if (ListViewOption.尚未擁有的保障項目 != option) {
                boolean isShow = dataMap.get(option.name()) == null ? false : dataMap.get(option.name());
                if (!isShow) {
                    isShowNoItem = true;
                }
            }
        }

        return isShowNoItem;
    }

    //保障金額是否皆為零
    public static boolean isAllAmountZero(List<Map<String, Object>> showCoverageItem) {
        //只含有"尚未擁有的保障項目"
        return showCoverageItem.size() == 1;
    }

    public static List<Map<String, Object>> getShowCoverageItems(List<CathayLifeTypeIServiceImpl.CoverageItem> coverageItems) {
        Map<String, Boolean> dataMap = ListViewOption.getDataMap(coverageItems);

        List<Map<String, Object>> list = new ArrayList<>();
        for (ListViewOption e : ListViewOption.values()) {
            if (dataMap.get(e.name())) {
                Map<String, Object> map = new HashMap<>();
                map.put("mainCoverage", e.name());
                list.add(map);
            }
        }

        return list;
    }

    public static List<ListViewOption> getAmountZeroCoverageItems(List<CathayLifeTypeIServiceImpl.CoverageItem> coverageItems) {
        Map<String, Boolean> dataMap = ListViewOption.getDataMap(coverageItems);

        List<ListViewOption> list = new ArrayList<>();
        for (ListViewOption e : ListViewOption.values()) {
            if (!dataMap.get(e.name())) {
                if (ListViewOption.尚未擁有的保障項目 != e)
                    list.add(e);
            }
        }

        return list;
    }

    //取得"保障項目"對應的圖片URL
    public static String getImageUrlByType(ListViewOption type, boolean isAmountZero) {
        String imageUrl = isAmountZero ? "card35-" : "card34-";
        switch (type) {
            case 癌症醫療:
                imageUrl = imageUrl + "02.jpg";
                break;
            case 失能給付:
                imageUrl = imageUrl + "03.jpg";
                break;
            case 重疾給付:
                imageUrl = imageUrl + "04.jpg";
                break;
            case 疾病醫療:
                imageUrl = imageUrl + "05.jpg";
                break;
            case 身故給付:
                imageUrl = imageUrl + "06.jpg";
                break;
            case 長照給付:
                imageUrl = imageUrl + "08.jpg";
                break;
            case 意外醫療:
                imageUrl = imageUrl + "07.jpg";
                break;
            case 實支實付需收據:
                imageUrl = imageUrl + "09.jpg";
                break;
            case 手術醫療:
                imageUrl = imageUrl + "01.jpg";
                break;
            case 尚未擁有的保障項目:
                imageUrl = "";
                break;
            default:
                break;
        }

        return imageUrl;
    }

    public static List<Map<String, Object>> getCommonCards(BotMessageVO vo, CathayLifeTypeIServiceImpl.I02Data i02Data, ListViewOption type) {
        List<Map<String, Object>> cardList = new ArrayList<>();

        List<CathayLifeTypeIServiceImpl.CoverageItem> coverageItems = i02Data.getCoverageItems();
        if (type != null) {

            if (type == ListViewOption.尚未擁有的保障項目) {
                List<ListViewOption> listViewOptions = ListViewOption.getAmountZeroCoverageItems(coverageItems);

                for (ListViewOption listViewOption : listViewOptions) {
                    CardItem cards = vo.new CardItem();
                    cards.setCWidth(BotMessageVO.CWIDTH_250);

                    CImageDataItem cImageData = vo.new CImageDataItem();
                    cImageData.setCImage(BotMessageVO.IMG_CB);
                    cImageData.setCImageUrl(ListViewOption.getImageUrlByType(listViewOption, true));
                    cards.setCImageData(cImageData.getCImageDatas());
                    cards.setCTextType("5");
                    cards.setCTitle("還沒擁有此保障項目喔～");
                    cardList.add(cards.getCards());
                }
            } else {
                List<List<Map<String, Object>>> list = new ArrayList<>(); //存放每張牌卡的cTextList  list.size()就是需要的卡片數

                List<Map<String, Object>> cTextList = new ArrayList<>();
                int cnt = 0;
                for (int i = 0; i < coverageItems.size(); i++) {
                    CathayLifeTypeIServiceImpl.CoverageItem coverageItem = coverageItems.get(i);
                    String mainCoverage = coverageItem.getMainCoverage();
                    String amount = coverageItem.getAmount();
                    //[保障名稱]僅顯示有值欄位
                    if (StringUtils.isEmpty(amount) || "0".equals(amount)) {
                        continue;
                    }
                    if (type.name().equals(mainCoverage)) {
                        CTextItem cText1 = vo.new CTextItem();
                        cText1.setCLabel(coverageItem.getSubCoverageElem());
                        CTextItem cText2 = vo.new CTextItem();
                        cText2.setCText(amount);

                        cTextList.add(cText1.getCTexts());
                        cTextList.add(cText2.getCTexts());
                        cnt++;

                        if (cnt > 0 && cnt % 5 == 0) {  //一張牌卡最多顯示五個[保障名稱]
                            list.add(cTextList);
                            cTextList = new ArrayList<>();
                        }
                    }
                }

                if (cnt % 5 != 0) { //加最後一張卡
                    list.add(cTextList);
                }

                for (List<Map<String, Object>> textList : list) {
                    CardItem cards = vo.new CardItem();
                    cards.setCWidth(BotMessageVO.CWIDTH_250);

                    CImageDataItem cImageData = vo.new CImageDataItem();
                    cImageData.setCImage(BotMessageVO.IMG_CB);
                    cImageData.setCImageUrl(ListViewOption.getImageUrlByType(type, false));
                    cards.setCImageData(cImageData.getCImageDatas());
                    cards.setCTexts(textList);
                    cardList.add(cards.getCards());
                }

            }
        }

        return cardList;
    }

    public static List<Map<String, Object>> getCommonCards(BotMessageVO vo, CathayLifeTypeIServiceImpl.I02Data i02Data, List<ListViewOption> types) {
        List<Map<String, Object>> cardList = new ArrayList<>();

        List<CathayLifeTypeIServiceImpl.CoverageItem> coverageItems = i02Data.getCoverageItems();
        if (CollectionUtils.isNotEmpty(types)) {
            for (ListViewOption type : types) {
                List<Map<String, Object>> cards = ListViewOption.getCommonCards(vo, i02Data, type);
                cardList.addAll(cards);
            }
        }

        return cardList;
    }

    public static List<ListViewOption> getListViewOptionList(String jsonParameter) {
        List<ListViewOption> list = new ArrayList<>();

        if (StringUtils.isNotBlank(jsonParameter)) {
            String showItem = null;
            Map param = null;

            try {
                param = JSONUtils.json2map(jsonParameter);
                showItem = MapUtils.getString(param, "showItem");

                String[] items = showItem.split(",");
                if (items != null) {
                    for (String item : items) {
                        list.add(ListViewOption.getValue(item));
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }

        return list;
    }

}
