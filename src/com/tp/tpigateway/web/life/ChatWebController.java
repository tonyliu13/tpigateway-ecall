package com.tp.tpigateway.web.life;

import com.tp.tpigateway.exception.RRException;
import com.tp.tpigateway.model.common.AllAutoComplete;
import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotRequestAndResponse;
import com.tp.tpigateway.model.common.BotTpiCallLog;
import com.tp.tpigateway.model.life.BotChatLog;
import com.tp.tpigateway.service.common.AllAutoCompleteService;
import com.tp.tpigateway.service.common.BotChatflowMappingService;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiCallLogService;
import com.tp.tpigateway.service.common.ChatHistoryService;
import com.tp.tpigateway.service.life.common.BotChatLogService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeGService;
import com.tp.tpigateway.util.PropUtils;
import com.tp.tpigateway.web.common.BaseController;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 提供 ChatWeb Server 使用之內部 API
 */
@Controller
@RequestMapping("/Life/ChatWeb")
public class ChatWebController extends BaseController {

	private static Logger log = LoggerFactory.getLogger(ChatWebController.class);

	@Autowired
	private CathayLifeTypeGService cathayLifeTypeGService;
	
	@Autowired
	private BotChatLogService botChatLogService;

	@Autowired
	private BotMessageService botMessageService;

	@Autowired
	private AllAutoCompleteService allAutoCompleteService;
	
	@Autowired
	private BotTpiCallLogService botTpiCallLogServiceImpl;
	
	@Autowired
	private ChatHistoryService chatHistoryService;
	
	@Autowired
	private BotChatflowMappingService botChatflowMappingService;
	

	/**
	 * 查詢客戶是否有保單驗證 G03
	 *
	 * @param ID
     * @param IP
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "queryContractCheck")
	public Map<String, Object> queryContractCheck(@RequestParam(value = "ID", required = false) String ID,
                                                  @RequestParam(value = "IP", required = false) String IP) {

		log.info("ID=" + ID);

		Map<String, Object> result = null;
		try {
			Map<String, Object> g03Result = new HashMap<>();
			//if (!PropUtils.CATHAYLIFE_PASS_G03) {
				g03Result = cathayLifeTypeGService.g03(ID, IP);
			//}
			log.info("g03Result=" + g03Result);
			result = getSuccessResult(g03Result, false);
		} catch (RRException rre) {
			if (rre.getInfo() != null && rre.getInfo().get("detail") != null) {
				log.error(rre.getMessage() + ", " + (rre.getInfo() != null ? rre.getInfo().get("detail") : ""));
				result = getErrorResult(rre.getMessage() + ", " + rre.getInfo().get("detail"));
			} else {
				result = getErrorResult(rre.getMessage());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;
	}

	/**
	 * OTP保單驗證
	 *
	 * @param ID
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "doContractCheck")
	public Map<String, Object> doContractCheck(@RequestParam(value = "ID", required = false) String ID,
                                               @RequestParam(value = "IP", required = false) String IP) throws Exception {

		log.info("ID={}", ID);

		Map<String, Object> result = null;
		try {
			Map<String, Object> checkInfo = cathayLifeTypeGService.g01(ID, IP);
			log.info("info=" + Arrays.toString(checkInfo.entrySet().toArray()));
			result = getSuccessResult(checkInfo, false);
		} catch (Exception e) {
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;
	}

    /**
     * 檢核OTP保單驗證結果
     *
     * @param ID
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "verifyContractCheck")
    public Map<String, Object> verifyContractCheck(@RequestParam(value = "ID", required = false) String ID,
                                                   @RequestParam(value = "OTP_SNO", required = false) String OTP_SNO,
                                                   @RequestParam(value = "OTP_VNO", required = false) String OTP_VNO) throws Exception {

        log.info("ID={}, OTP_SNO={}, OTP_VNO={}", ID, OTP_SNO, OTP_VNO);

        Map<String, Object> result = null;
        try {
            Map<String, Object> checkInfo = cathayLifeTypeGService.g02(ID, OTP_SNO, OTP_VNO);
            log.info("info=" + Arrays.toString(checkInfo.entrySet().toArray()));
            result = getSuccessResult(checkInfo, false);
        } catch (Exception e) {
            result = getErrorResult(e.getMessage());
        } finally {
            if (result == null) {
                result = getSystemErrorResult();
            }
        }

        return result;
    }

    /**
     * 進行保單驗證(無手機資料)
     *
     * @param ID
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "doContractCheckNoPhone")
    public Map<String, Object> doContractCheckNoPhone(@RequestParam(value = "ID", required = false) String ID,
                                                   @RequestParam(value = "POLICY_NO", required = false) String POLICY_NO,
                                                   @RequestParam(value = "VERIFY_TYPE", required = false) String VERIFY_TYPE) throws Exception {

        log.info("ID={}, POLICY_NO={}, VERIFY_TYPE={}", ID, POLICY_NO, VERIFY_TYPE);

        Map<String, Object> result = null;
        try {
            Map<String, Object> checkInfo = cathayLifeTypeGService.g04(ID, POLICY_NO, VERIFY_TYPE);
            log.info("info=" + Arrays.toString(checkInfo.entrySet().toArray()));
            result = getSuccessResult(checkInfo, false);
        } catch (Exception e) {
            result = getErrorResult(e.getMessage());
        } finally {
            if (result == null) {
                result = getSystemErrorResult();
            }
        }

        return result;
    }

	/**
	 * 轉真人時寫chatLog至DB
	 */
	@ResponseBody
	@RequestMapping(value = "botChatLog", method = RequestMethod.POST)
	public Map<String, Object> addBotChatLog(
			@RequestParam(value = "session_id", required = true) String sessionId,
			@RequestParam(value = "chat_log", required = true) String chatLog) throws Exception {

		log.info("session_id=" + sessionId + ", chat_log=" + chatLog);
		Map<String, Object> result = null;
		try {
			botChatLogService.addBotChatLog(new BotChatLog(sessionId, chatLog));
			result = getSuccessResult(null, false);
		} catch (Exception e) {
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;
	}

	/**
	 * 查詢聊天紀錄
	 */
	@ResponseBody
	@RequestMapping(value = "botChatLog/sessionId/{sessionId}", method = RequestMethod.GET)
	public Map<String, Object> getBotChatLogBySessionId(@PathVariable String sessionId) throws Exception {
		log.info("sessionId=" + sessionId);

		Map<String, Object> result = null;
		try {
			BotChatLog botChatLog = botChatLogService.getBotChatLogBySessionId(sessionId);
			result = getSuccessResult(botChatLog.getChatLog(), false);
		} catch (Exception e) {
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;
	}

	@ResponseBody
	@RequestMapping(value="/getAllAutoComplete", produces="application/json; charset=UTF-8")
	public Map<String, Object> getAllAutoComplete() {

		Map<String, Object> result = null;
		try {
			List<AllAutoComplete> data = allAutoCompleteService.getAllAutoComplete();
			BotMessageVO botMsg = botMessageService.getAutoCompleteBotMessageVO(data);
			Map<String, Object> returnData = (Map<String, Object>) CollectionUtils.get(botMsg.getContent(), 0);
			result = getSuccessResult(returnData, false);
		} catch (Exception e) {
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;

	}
	
		
	/**
	 * 查詢資料，依據chatID 及 insertTime
	 */
	@ResponseBody
	@RequestMapping(value = "botChatLog/custId/{custId}/chatId/{chatId}/start/{insertTimeStart}/end/{insertTimeEnd}", method = RequestMethod.GET)
	public Map<String, Object> getBotChatLogByChatId(
				@PathVariable String custId,
				@PathVariable String chatId,
				@PathVariable String insertTimeStart,
				@PathVariable String insertTimeEnd
	)throws Exception {
		
		log.info("getBotChatLogByChatId REQUEST custId=" + custId);
		log.info("getBotChatLogByChatId REQUEST chatId=" + chatId);
		log.info("getBotChatLogByChatId REQUEST insertTimeStart=" + insertTimeStart);
		log.info("getBotChatLogByChatId REQUEST insertTimeEnd=" + insertTimeEnd);
		
		Timestamp insertTimeStartTs;
		Timestamp insertTimeEndTs;
		insertTimeStart = URLDecoder.decode(insertTimeStart, "UTF-8");
		insertTimeEnd = URLDecoder.decode(insertTimeEnd, "UTF-8");
		log.info("After decode start=" + insertTimeStart);
		log.info("After decode end=" + insertTimeEnd);
		if (insertTimeStart == null || "NULL".equals(insertTimeStart.trim().toUpperCase())) {
			insertTimeStartTs = null;
		} else {
			insertTimeStartTs = Timestamp.valueOf(insertTimeStart);
		}
		
		if (insertTimeEnd == null || "NULL".equals(insertTimeEnd.trim().toUpperCase())) {
			insertTimeEndTs = null;
		} else {
			insertTimeEndTs = Timestamp.valueOf(insertTimeEnd);
		}
		
		Map<String, Object> result = null;

		try {
			BotRequestAndResponse botChatLog = botTpiCallLogServiceImpl.queryByChatID(custId, chatId, insertTimeStartTs, insertTimeEndTs);
			replaceApiIdWithChatflowName(botChatLog);
																																						 
			result = getSuccessResult(botChatLog, false);
		} catch (Exception e) {
			e.printStackTrace();
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "ChatflowHistory/sessionId/{sessionId}", method = RequestMethod.GET)
	public Map<String, Object> getChatflowHistory(
				@PathVariable String sessionId
	) throws Exception {
		Map<String, Object> result = null;
		try {
			List<Object> historyList = chatHistoryService.getSessionMessageList(sessionId);
			result = getSuccessResult(historyList, false);
		} catch (Exception e) {
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "NLUIntent/sessionId/{sessionId}/msgId/{msgId}", method = RequestMethod.GET)
	public Map<String, Object> getMessageIntentList(
				@PathVariable String sessionId,
				@PathVariable String msgId
	) throws Exception {
		
		Map<String, Object> result = null;
		try {
			List<Object> intentList = chatHistoryService.getMessageIntentList(sessionId, msgId);
			Map<String, Object> intentMap = new HashMap<>();
			intentMap.put("intents", intentList);
			result = getSuccessResult(intentMap, false);
		} catch (Exception e) {
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "PathNode/sessionId/{sessionId}/msgId/{msgId}", method = RequestMethod.GET)
	public Map<String, Object> getPathNodeList(
				@PathVariable String sessionId,
				@PathVariable String msgId
	) throws Exception {
		
		Map<String, Object> result = null;
		try {
			List<Map<String, String>> pathNodeList = chatHistoryService.getMessagePathNodeList(sessionId, msgId);
			Map<String, Object> pathMap = new HashMap<>();
			pathMap.put("path", pathNodeList);
			result = getSuccessResult(pathMap, false);
		} catch (Exception e) {
			result = getErrorResult(e.getMessage());
		} finally {
			if (result == null) {
				result = getSystemErrorResult();
			}
		}
		return result;
	}
	
	private void replaceApiIdWithChatflowName(BotRequestAndResponse botChatLog) {
		Map<String, String> botChatflowMap = botChatflowMappingService.getChatflowMapping();
		List<BotTpiCallLog> callLogList = botChatLog.getCallLogList();
		for (BotTpiCallLog callLog: callLogList) {
			if (botChatflowMap.containsKey(callLog.getApiId())) {
				callLog.setApiId(botChatflowMap.get(callLog.getApiId()));
			}
		}
	}
	
}
