package com.tp.tpigateway.web.life;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tp.tpigateway.util.OkHttpUtils;
import com.tp.tpigateway.util.PropUtils;

import okhttp3.MediaType;

/**
 * 提供 ChatWeb Server 使用之測試 DummyAPI
 */
@Controller
@RequestMapping("/Life/DummyAPI")
public class DummyController {
	private static Logger log = LoggerFactory.getLogger(DummyController.class);

	/**
	 * Dummy for A20:ID查詢可借金額
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A20", produces = "application/json; charset=UTF-8")
	public Object a20(@RequestParam(value = "ID", required = false) String ID) {
		log.info("ID:" + ID);

		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		Map<String, Object> detail;
		if ("A28856650C".equals(ID)) { // A20查無資料
			detail = new HashMap<String, Object>();
		} else {
			detail = new generateA20TestData().getData();
		}

		returnResult.put("detail", detail);
		return returnResult;
	}

	private class generateA20TestData {
		// 幣別
		String[] currs = { "NTD", "USD", "AUD", "NZD", "CNY" };

		// 幣別對映的匯率
		String[] rates = { "1", "29.1715286", "22.4798716", "21.0650525", "4.60764294" }; // 2018-03-20

		String[] ids = { "J18871663D", "J28665911J", "F28675049E", "E18737469B" };

		Map<String, BigDecimal> tmp4Rate = new HashMap<String, BigDecimal>();

		public generateA20TestData() {
			for (int i = 0; i < currs.length; i++) {
				tmp4Rate.put(currs[i], new BigDecimal(rates[i]));
			}
		}

		/**
		 * 取得A20的測試資料
		 * 
		 * @return
		 */
		private Map<String, Object> getData() {
			int times = 10;

			long cash_random_seed = new Date().getTime() / 10000000;

			Map<String, Object> returnData = new HashMap<String, Object>();

			List<Map> dataList = new ArrayList<Map>();
			returnData.put("DETAIL", dataList); // TODO 欄位名稱暫定

			for (int i = 1; i <= times; i++) {
				Map m = new HashMap();
				dataList.add(m);

				// 保單資料
				String serNo = StringUtils.leftPad(String.valueOf(i), 3, '0');

				m.put("POLICY_NO", "POLICY_NO" + serNo); // 保單號碼
				m.put("PROD_NAME", "險別" + serNo); // 險別名稱

				String CURR = getRandomCurr();
				m.put("CURR", CURR); // 保單幣別

				BigDecimal LOAN_BAL = getRandomLOAN(cash_random_seed);
				BigDecimal LOAN_ABLE = getRandomLOAN(cash_random_seed, LOAN_BAL);
				BigDecimal LOAN_ABLE2NTD = LOAN_ABLE.multiply(getExchangeRate(CURR)).setScale(2,
						BigDecimal.ROUND_HALF_UP);
				m.put("LOAN_BAL", LOAN_BAL); // 原貸金額
				m.put("LOAN_RATE", getRandomLOAN_RATE()); // 借款利率
				m.put("LOAN_ABLE", LOAN_ABLE); // 本次可貸金額
				m.put("APC_NAME", "大樹_" + serNo); // 要保人姓名
				m.put("INSD_NAME", "種子" + serNo); // 被保人姓名
				m.put("LOAN_ABLE2NTD", LOAN_ABLE2NTD); // TODO 尚可借金額換算為台幣金額(欄位名稱暫定)

				// 合計(約當台幣)
				if (returnData.containsKey("TOTAL_ABLE_AMT")) {
					returnData.put("TOTAL_ABLE_AMT",
							((BigDecimal) returnData.get("TOTAL_ABLE_AMT")).add(LOAN_ABLE2NTD));
				} else {
					returnData.put("TOTAL_ABLE_AMT", LOAN_ABLE2NTD);
				}

				// 保單尚可貸金額
				String LOAN_ABLE_CURR = "LOAN_ABLE_" + CURR;
				if (returnData.containsKey(LOAN_ABLE_CURR)) {
					returnData.put(LOAN_ABLE_CURR, ((BigDecimal) returnData.get(LOAN_ABLE_CURR)).add(LOAN_ABLE));
				} else {
					returnData.put(LOAN_ABLE_CURR, LOAN_ABLE);
				}

				// 保單張數
				String CURR_POL_CNT = CURR + "_POL_CNT";
				if (returnData.containsKey(CURR_POL_CNT)) {
					returnData.put(CURR_POL_CNT, ((BigDecimal) returnData.get(CURR_POL_CNT)).add(BigDecimal.ONE));
				} else {
					returnData.put(CURR_POL_CNT, BigDecimal.ONE);
				}
			}

			Collections.sort(dataList, new ListComparator());

			return returnData;
		}

		class ListComparator implements Comparator<Map> {
			@Override
			public int compare(Map m1, Map m2) {
				return ((BigDecimal) m1.get("LOAN_RATE")).compareTo((BigDecimal) m2.get("LOAN_RATE"));
			}
		}

		private BigDecimal getExchangeRate(String curr) {
			return tmp4Rate.get(curr);
		}

		private String getRandomCurr() {
			return currs[(int) (Math.random() * currs.length)];
		}

		private BigDecimal getRandomLOAN_RATE() {
			return new BigDecimal(Math.random()).setScale(5, BigDecimal.ROUND_HALF_UP);
		}

		private BigDecimal getRandomLOAN(long cash_random_seed) {
			return new BigDecimal(Math.random() * cash_random_seed).setScale(2, BigDecimal.ROUND_HALF_UP);
		}

		private BigDecimal getRandomLOAN(long cash_random_seed, BigDecimal maxLimit) {
			BigDecimal tmp = null;
			do {
				tmp = getRandomLOAN(cash_random_seed);
			} while (tmp.compareTo(maxLimit) > 0);

			return tmp;
		}

		private String getRandomAPC_ID() {
			return ids[(int) (Math.random() * ids.length)];
		}
	}

	/**
	 * Dummy for A29查詢ATM借還款金額
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A29", produces = "application/json; charset=UTF-8")
	public Object a29(@RequestParam(value = "APC_ID", required = false) String APC_ID) {
		log.info("APC_ID:" + APC_ID);

		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		Map<String, Object> detail = new HashMap<String, Object>();
		returnResult.put("detail", detail);
		if ("G28745129F".equals(APC_ID)) {// A29[狀態表示]≠Y A29[回覆訊息代碼] =I38.1.1
			detail.put("STS_CODE", "N");
			detail.put("MSG_TXT", "因為你是保單的被保人，目前ATM保單借還款的服務資格為：");
			detail.put("MSG_CD", "I38.1.1");
		} else if ("J18871663D".equals(APC_ID)) {// A29[狀態表示]≠Y A29[回覆訊息代碼] =I38.1.2
			detail.put("STS_CODE", "N");
			detail.put("MSG_TXT", "發現你還沒有申辦ATM保單借還款服務，所以沒有辦法查詢哦~");
			detail.put("MSG_CD", "I38.1.2");
		} else {// A29[狀態表示]=成功
			detail.put("LOAN_ABLE_AMT", "1000");
			detail.put("LOAN_BAL_AMT", "100");
			detail.put("STS_CODE", "Y");
		}
		return returnResult;
	}

	/**
	 * Dummy for A23查詢ATM借還款金額
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A23", produces = "application/json; charset=UTF-8")
	public Object a23(@RequestParam(value = "POLICY_NO", required = false) String POLICY_NO) {
		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		if("5506122963".endsWith(POLICY_NO)) {			
			ArrayList al = new ArrayList();
			returnResult.put("detail", al);
		}else {
			Map<String, Object> detail = new HashMap<String, Object>();
			Map<String, Object> detail2 = new HashMap<String, Object>();
			Map<String, Object> detail3 = new HashMap<String, Object>();
			Map<String, Object> detail4 = new HashMap<String, Object>();
			Map<String, Object> detail5= new HashMap<String, Object>();
			Map<String, Object> detail6 = new HashMap<String, Object>();
			ArrayList al = new ArrayList();
			detail.put("POLICY_NO", POLICY_NO);
			detail.put("PAY_DATE", "2013-01-03");
			detail.put("PAY_NET", "123544");
			detail.put("PAY_TYPE", "匯撥");
			detail.put("ACPT_ACNT_NO", "135790246899 ");
			detail.put("ACPT_BANK_NO", "國泰世仁愛");
			al.add(detail);
			detail2.put("POLICY_NO", POLICY_NO);
			detail2.put("PAY_DATE", "2015-04-05");
			detail2.put("PAY_NET", "895512");
			detail2.put("PAY_TYPE", "電話繳費");
			detail2.put("ACPT_ACNT_NO", "135790246899 ");
			detail2.put("ACPT_BANK_NO", "國泰松山行");
			al.add(detail2);
			detail3.put("POLICY_NO", POLICY_NO);
			detail3.put("PAY_DATE", "2017-11-22");
			detail3.put("PAY_NET", "45621");
			detail3.put("PAY_TYPE", "網銀繳費");
			detail3.put("ACPT_ACNT_NO", "135798246899 ");
			detail3.put("ACPT_BANK_NO", "國泰大安分");
			al.add(detail3);
			detail4.put("POLICY_NO", POLICY_NO);
			detail4.put("PAY_DATE", "2014-10-22");
			detail4.put("PAY_NET", "895512");
			detail4.put("PAY_TYPE", "ATM");
			detail4.put("ACPT_ACNT_NO", "135790246899 ");
			detail4.put("ACPT_BANK_NO", "國泰中山行");
			al.add(detail4);
			detail5.put("POLICY_NO", POLICY_NO);
			detail5.put("PAY_DATE", "2016-01-29");
			detail5.put("PAY_NET", "895512");
			detail5.put("PAY_TYPE", "點數卡");
			detail5.put("ACPT_ACNT_NO", "135790246699 ");
			detail5.put("ACPT_BANK_NO", "國泰淡水銀");
			al.add(detail5);
			detail6.put("POLICY_NO", POLICY_NO);
			detail6.put("PAY_DATE", "2018-01-21");
			detail6.put("PAY_NET", "895512");
			detail6.put("PAY_TYPE", "郵局劃撥");
			detail6.put("ACPT_ACNT_NO", "135790247799 ");
			detail6.put("ACPT_BANK_NO", "國泰高市分");
			al.add(detail6);
			returnResult.put("detail", al);
		}
		return returnResult;
	}
	
	/**
	 * Dummy for A25 保全系統/查詢作業/B滿期金領取記錄
	 * 
	 * @param POLICY_NO
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A25", produces = "application/json; charset=UTF-8")
	public Object a25(@RequestParam(value = "POLICY_NO", required = false) String POLICY_NO) {

		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		
		List detailList = new ArrayList();
		
		Map<String, Object> detail1 = new HashMap<String, Object>();
		detail1.put("ACPT_NAME", "劉O廷");
		detail1.put("TACNT_DATE", "1991-10-06");
		detail1.put("PAY_NET", "35000");
		detail1.put("ACPT_BANK_NO", "國泰世華");
		detail1.put("ACPT_ACNT_NO", "135790246899    ");
		detail1.put("PAY_TYPE", "自動轉帳");
		//returnResult.put("detail", detail1);
		detailList.add(detail1);
		
		Map<String, Object> detail2 = new HashMap<String, Object>();
		detail2.put("ACPT_NAME", "劉O志");
		detail2.put("TACNT_DATE", "1992-01-28");
		detail2.put("PAY_NET", "47000");
		detail2.put("ACPT_BANK_NO", "國泰彰化");
		detail2.put("ACPT_ACNT_NO", "135790241233    ");
		detail2.put("PAY_TYPE", "自動轉帳");
		//returnResult.put("detail", detail2);
		detailList.add(detail2);
		
		returnResult.put("detail", detailList);
		
		return returnResult;
	}
	
	/**
	 * Dummy for A17 保全系統/保全給付試算輸入/C解約試算輸入
	 * 
	 * @param POLICY_NO
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A17", produces = "application/json; charset=UTF-8")
	public Object a17(@RequestParam(value = "POLICY_NO", required = false) String POLICY_NO) {

		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
			
		Map<String, Object> detail1 = new HashMap<String, Object>();
		detail1.put("MESSAGE_ID", "S033.d.2");
		detail1.put("PAY_NET", "123");
		returnResult.put("detail", detail1);
		
		return returnResult;
	}
	
	/**
	 * Dummy for A04 ID行銷資訊
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A04", produces = "application/json; charset=UTF-8")
	public Object a04(@RequestParam(value = "ID", required = false) String POLICY_NO) {
		
		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
			
		Map<String, Object> detail1 = new HashMap<String, Object>();
		detail1.put("ONLINE_CHANGE", "Y");
		returnResult.put("detail", detail1);
		
		return returnResult;
	}
	
	/**
	 * Dummy for A18  單一保單試算繳清資訊
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A18", produces = "application/json; charset=UTF-8")
	public Object a18(@RequestParam(value = "POLICY_NO", required = false) String POLICY_NO,
							 @RequestParam(value = "IS_RD_VALD", required = false) String IS_RD_VALD,
							 @RequestParam(value = "INTENT", required = false) String INTENT) {
		
		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		
		Map<String, Object> detail1 = new HashMap<String, Object>();
		
		detail1.put("AUTO_PREM_INT", "0");
		detail1.put("AUTO_PREM_PRM", "0");
		detail1.put("DVD_AMT", "0");
		detail1.put("LOAN_AMT", "0");
		detail1.put("LOAN_INT", "0");
		detail1.put("PAUP_BGN_DATE", "2019-02-01");
		detail1.put("PAUP_FACE_AMT", "286950");
		detail1.put("RTN_ADD_AMT", "0");
		detail1.put("RTN_ADD_IND", "0");
		
		if("9015077201".equals(POLICY_NO)) {
			detail1.put("MSG_ID", "A");
		} else if("9038630722".equals(POLICY_NO)) {
			detail1.put("MSG_ID", "B");
		} else if("7709604427".equals(POLICY_NO)) {
			detail1.put("MSG_ID", "E");
		} else if("9126306820".equals(POLICY_NO)) {
			detail1.put("MSG_ID", "Z");
		}
		
		returnResult.put("detail", detail1);
		
		return returnResult;
	}
	
	/**
	 * Dummy for A11 單一保單可附加附約(最多11筆)
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A11", produces = "application/json; charset=UTF-8")
	public Object a11(@RequestParam(value = "POLICY_NO", required = false) String POLICY_NO) {
		
		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		
		Map<String, Object> detail = new HashMap<String, Object>();
		if("1001241158".equals(POLICY_NO)) {
			detail.put("RESN", "不好意思！[險別中文名稱]沒有提供附加附約服務，所以無法辦理喔！");//回覆訊息中文
			detail.put("MSG_ID", "A");
		}else {
			detail.put("RESN", "繳清保額=0，不得辦理");//回覆訊息中文
			detail.put("MSG_ID", "B");
		}
		
		List detailList = new ArrayList();
		
		Map<String, Object> detail1 = new HashMap<String, Object>();
		detail1.put("PROD_SID", "66666");//附約險別
		detail1.put("PROD_SNAME", "意外險");//附約險別中文名稱
		detailList.add(detail1);
		
		Map<String, Object> detail2 = new HashMap<String, Object>();
		detail2.put("PROD_SID", "55555");//附約險別
		detail2.put("PROD_SNAME", "壽險");//附約險別中文名稱
		detailList.add(detail2);
		
		Map<String, Object> detail3 = new HashMap<String, Object>();
		detail3.put("PROD_SID", "55555");//附約險別
		detail3.put("PROD_SNAME", "123險");//附約險別中文名稱
		detailList.add(detail3);
		
		Map<String, Object> detail4 = new HashMap<String, Object>();
		detail4.put("PROD_SID", "55555");//附約險別
		detail4.put("PROD_SNAME", "456險");//附約險別中文名稱
		detailList.add(detail4);
		
		Map<String, Object> detail5 = new HashMap<String, Object>();
		detail5.put("PROD_SID", "55555");//附約險別
		detail5.put("PROD_SNAME", "QQQ險");//附約險別中文名稱
		detailList.add(detail5);
		
		Map<String, Object> detail6 = new HashMap<String, Object>();
		detail6.put("PROD_SID", "55555");//附約險別
		detail6.put("PROD_SNAME", "WWW險");//附約險別中文名稱
		detailList.add(detail6);
		
		Map<String, Object> detail7 = new HashMap<String, Object>();
		detail7.put("PROD_SID", "55555");//附約險別
		detail7.put("PROD_SNAME", "ZZZ險");//附約險別中文名稱
		detailList.add(detail7);
		
		Map<String, Object> detail8 = new HashMap<String, Object>();
		detail8.put("PROD_SID", "55555");//附約險別
		detail8.put("PROD_SNAME", "CCC險");//附約險別中文名稱
		detailList.add(detail8);
		
		detail.put("PROD_List", detailList);
		returnResult.put("detail", detail);
		
		return returnResult;
	}
	
	/**
	 * Dummy for A19  單一保單試算縮小保額
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A19", produces = "application/json; charset=UTF-8")
	public Object a19(@RequestParam(value = "POLICY_NO", required = false) String POLICY_NO,
							 @RequestParam(value = "PREM", required = false) String PREM,
							 @RequestParam(value = "PROD_KIND", required = false) String PROD_KIND,
							 @RequestParam(value = "INTENT", required = false) String INTENT){
		
		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		
		Map<String, Object> detail1 = new HashMap<String, Object>();
		
		detail1.put("NEW_MAIN_PREM", "4554646940");
		detail1.put("NEW_SPEC_PREM", "45646");
		detail1.put("LOAN_AMT", "4564654");
		detail1.put("LOAN_INT", "16546546");
		detail1.put("LOAN_BAL", "5468480");
		detail1.put("EFT_DATE", "2019-02-01");
		detail1.put("BOTTOM_LIMIT", "286950");
		detail1.put("SHOW_ERROR_MSG", "");
		detail1.put("MSG_ID", "");
		detail1.put("RTN_ADD_TOTAL_AMT", "59898");
				
		
		returnResult.put("detail", detail1);
		
		return returnResult;
	}
	
	/**
	 * Dummy for A14 險別查詢保全主約設定
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "A14", produces = "application/json; charset=UTF-8")
	public Object a14(@RequestParam(value = "PROD_ID", required = false) String PROD_ID)
							 {
		
		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");
		
		Map<String, Object> detail1 = new HashMap<String, Object>();
		
		detail1.put("IS_GRUP_IND", "Y");
		detail1.put("IS_PAUP", "Y");
		detail1.put("IS_DISC_AMT", "Y");
		detail1.put("IS_INCR_AMT", "Y");
		detail1.put("IS_PAY_FREQ", "Y");
		detail1.put("IS_INCR_PRD", "Y");
		detail1.put("IS_DISC_PRD", "Y");
		detail1.put("IS_DVD_KIND", "Y");
		detail1.put("IS_ROCR", "Y");
		detail1.put("IS_GRUP_IND", "Y");
		
		returnResult.put("detail", detail1);
		
		return returnResult;
	}
	
	/**
	 * Dummy for K01 查詢客戶契約及VIP等級
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "K01", produces = "application/json; charset=UTF-8")
	public Object k01(@RequestParam(value = "ID", required = false) String POLICY_NO) {

		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");

		String vip_lvl = null;
		int i = (int) (Math.random() * 4 + 1);
		switch (i) {
		case 1:
			vip_lvl = "0";
			break;
		case 2:
			vip_lvl = "5";
			break;
		case 3:
			vip_lvl = "6";
			break;
		case 4:
			vip_lvl = "7";
			break;
		default:
			break;
		}
		Map<String, Object> detail = new HashMap<String, Object>();
		detail.put("VIP_LVL", vip_lvl);
		returnResult.put("detail", detail);

		return returnResult;
	}

	/**
	 * Dummy for K02 健檢資格查詢
	 * 
	 * @param ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "K02", produces = "application/json; charset=UTF-8")
	public Object k02(@RequestParam(value = "ID", required = false) String POLICY_NO) {

		Map<String, Object> returnResult = new HashMap<String, Object>();
		returnResult.put("returnCode", "0");
		returnResult.put("returnMsg", "API success.");

		String checkup = null, requrest = null ;
		int i = (int) (Math.random() * 4 + 1);
		switch (i % 2) {
		case 0:
			checkup = "Y";
			break;
		case 1:
			checkup = "N";
			break;
		default:
			break;
		}
		
		switch (i % 3) {
		case 0:
			requrest = "Y";
			break;
		case 1:
			requrest = "N";
			break;
		default:
			break;
		}
		
		Map<String, Object> detail = new HashMap<String, Object>();
		detail.put("HealthType", checkup);
		detail.put("Reissue", requrest);
		returnResult.put("detail", detail);

		return returnResult;
	}

	/**
     * Dummy for C03 網投旅平險-契約成立
     * 
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "C03", produces = "application/json; charset=UTF-8")
    public Object c03(){
        
        Map<String, Object> returnResult = new HashMap<String, Object>();
        returnResult.put("returnCode", "0");
        returnResult.put("returnMsg", "API success.");
        
        Map<String, Object> detail1 = new HashMap<String, Object>();
        
        detail1.put("resultMsg", "");
        detail1.put("resultCode", "099");
        
        returnResult.put("detail", detail1);
        
        return returnResult;
    }
    
    private static String askCSRtnCode = "1";
    private static String askCSFailTime = "5";
    
    /**
     * Dummy for 請求客服系統派線
     * 
     * @param reqMap
     * @param req
     * @return
     * @throws ServletException
     */
    @RequestMapping(value="/askCS"/*, method=RequestMethod.POST*/, produces="application/json; charset=UTF-8")
    public @ResponseBody Object askCustomerService (
            @RequestBody Map<String, Object> reqMap, HttpServletRequest req) throws ServletException {
        
        long startTime = System.currentTimeMillis();
        log.info("askCustomerService enter.");
        
        Object resultObject = null;
        Map<String, Object> rspMap = new HashMap<String, Object>();
        try {
            log.info(Arrays.toString(reqMap.entrySet().toArray()));
            
            final String chatID = MapUtils.getString(reqMap, "chatID");
            
            rspMap.put("rtnCode", askCSRtnCode);
            
            if (StringUtils.equals("1", MapUtils.getString(rspMap, "rtnCode"))) {
                rspMap.put("rtnMsg", "成功");
            } else {
                Thread.sleep(NumberUtils.toInt(askCSFailTime) * 1000);
                rspMap.put("rtnMsg", "失敗");
            }
            resultObject = rspMap;
            
            if (StringUtils.equals("1", MapUtils.getString(rspMap, "rtnCode"))) {
                try {
                    Thread receiverThread = new Thread(new Runnable() {
                        
                        @Override
                        public void run() {
                            
                            agentSendMsg(chatID, "system", "客服將盡快提供您服務...");
                            try { Thread.sleep(500); } catch (Exception e) { }
                            agentSendMsg(chatID, "system", "客服將盡快提供您服務...");
                            try { Thread.sleep(500); } catch (Exception e) { }
                            agentSendMsg(chatID, "system", "客服將盡快提供您服務...");
                            try { Thread.sleep(1000); } catch (Exception e) { }
                            
                            agentSendMsg(chatID, "M0001", "AgentConnect");
                            try { Thread.sleep(2 * 1000); } catch (Exception e) { }
                            
                            agentSendMsg(chatID, "M0001", "您好，我是客服人員編號3761，請問有什麼需要為您服務的呢？");
                        }
                    });
                    
                    receiverThread.start();
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultObject = new Object();
        }
        
        log.info("askCustomerService exit.");
        log.info("The API process time : " +(System.currentTimeMillis() - startTime) + "ms.");
        
        return resultObject;
    }
    
    private void agentSendMsg(String chatID, String agentId, String msg) {
        agentSendMsg(chatID, agentId, msg, null);
    }
    
    private void agentSendMsg(String chatID, String agentId, String msg, String customerMsgId) {
        Map<String, String> bo = new HashMap<String, String>();
        bo.put("chatID", chatID);
        bo.put("agentId", agentId);
        if (StringUtils.isNotBlank(customerMsgId)) {
            bo.put("customerMsgId", customerMsgId);
        }
        bo.put("chatMessage", msg);
        
        JSONObject requestJSON = new JSONObject(bo);
        log.debug("requestJSON=" + requestJSON.toString());
        
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("accept", "application/json");
        
        okhttp3.RequestBody body = okhttp3.RequestBody.create(MediaType.parse("application/json"), requestJSON.toString());
        
        String env = StringUtils.contains(PropUtils.getProperty("chat.reply.url"), "10.95.42.34") ? "2" : 
            (StringUtils.contains(PropUtils.getProperty("chat.reply.url"), "10.95.33.30") ? "3" : "1");
        
        String url = "http://" + ("2".equals(env) ? "10.95.42.34" : ("3".equals(env) ? "10.95.33.30" : "127.0.0.1:8080")) + "/ChatWeb/Agent/sendMsg";
        Map<String, Object> respResult = OkHttpUtils.requestPost(url, headers, body);
        
        log.info(Arrays.toString(respResult.entrySet().toArray()));
    }
    
    /**
     * Dummy for 真人客服
     * 
     * @param reqMap
     * @param req
     * @return
     * @throws ServletException
     */
    @RequestMapping(value="/sendCSMsg"/*, method=RequestMethod.POST*/, produces="application/json; charset=UTF-8")
    public @ResponseBody Object sendAgentMsg (
            @RequestBody Map<String, Object> reqMap, HttpServletRequest req) throws ServletException {
        
        long startTime = System.currentTimeMillis();
        log.info("sendCustomerServiceMsg enter.");
        
        Object resultObject = null;
        Map<String, Object> rspMap = new HashMap<String, Object>();
        try {
            log.info(Arrays.toString(reqMap.entrySet().toArray()));
            
            final String chatID = MapUtils.getString(reqMap, "chatID");
            final String customerMsgId = MapUtils.getString(reqMap, "customerMsgId");
            String chatMessage = MapUtils.getString(reqMap, "chatMessage");
            String faqAnsList = MapUtils.getString(reqMap, "faqAnsList");
            
            final String agentReply = StringUtils.contains(chatMessage, "不需要") ? "感謝請打分" : "Agent收到(chatMessage:" + chatMessage + ")(faqAnsList:" + faqAnsList + ")";
            
            rspMap.put("rtnCode", "1");
            rspMap.put("rtnMsg", "發送訊息完成");
            resultObject = rspMap;
            
            if (StringUtils.equals("1", MapUtils.getString(rspMap, "rtnCode"))) {
                
                try {
                    Thread receiverThread = new Thread(new Runnable() {
                        
                        @Override
                        public void run() {
                            
                            // agent 回覆
                            try { Thread.sleep(3 * 1000); } catch (Exception e) { }
                            agentSendMsg(chatID, "M0001", agentReply, customerMsgId);
                        }
                    });
                    
                    receiverThread.start();
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultObject = new Object();
        }
        
        log.info("sendCustomerServiceMsg exit.");
        log.info("The API process time : " +(System.currentTimeMillis() - startTime) + "ms.");
        
        return resultObject;
    }
}
