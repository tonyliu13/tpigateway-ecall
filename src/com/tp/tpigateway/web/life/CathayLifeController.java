package com.tp.tpigateway.web.life;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tp.tpigateway.dao.common.BotLinkDao;
import com.tp.tpigateway.model.common.BotLink;
import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.BotMessageVO.ContentItem;
import com.tp.tpigateway.model.common.LifeLink;
import com.tp.tpigateway.model.common.ZZALink;
import com.tp.tpigateway.model.common.enumeration.A30;
import com.tp.tpigateway.model.common.enumeration.C02;
import com.tp.tpigateway.model.common.enumeration.D19;
import com.tp.tpigateway.model.common.enumeration.D28;
import com.tp.tpigateway.model.common.enumeration.Z02;
import com.tp.tpigateway.model.common.enumeration.Z05;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.common.BotMessageService;
import com.tp.tpigateway.service.common.BotTpiReplyTextService;
import com.tp.tpigateway.service.common.RightsInformationService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001DataService;
import com.tp.tpigateway.service.life.flow.cb.CBA10001Service;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeAService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeCService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeDService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeHService;
import com.tp.tpigateway.service.life.lifeapi.CathayLifeTypeZService;
import com.tp.tpigateway.util.ApiLogUtils;
import com.tp.tpigateway.util.JSONUtils;
import com.tp.tpigateway.util.LocalCache;
import com.tp.tpigateway.util.LocalCache.ExpiredTime;
import com.tp.tpigateway.util.PropUtils;
import com.tp.tpigateway.util.TPIStringUtil;
import com.tp.tpigateway.web.common.BaseController;

@Controller
@RequestMapping("/CathayLife")
public class CathayLifeController extends BaseController {

	private static Logger log = LoggerFactory.getLogger(CathayLifeController.class);

	@Autowired
	private CathayLifeTypeAService cathayLifeTypeAService;
	@Autowired
	private CathayLifeTypeCService cathayLifeTypeCService;
	@Autowired
	private CathayLifeTypeDService cathayLifeTypeDService;
	@Autowired
	private CathayLifeTypeHService cathayLifeTypeHService;
	@Autowired
	private CathayLifeTypeZService cathayLifeTypeZService;
	@Autowired
	private BotMessageService botMessageService;
	@Autowired
	private BotTpiReplyTextService botTpiReplyTextService;
	@Autowired
	private BotLinkDao botLinkDao;
	@Autowired
	private RightsInformationService rightsInformationService;
	@Autowired
    private CBA10001Service cBA10001Service;
	@Autowired
	private CBA10001DataService cBA10001DataService;

	private static final String SOURCE = PropUtils.SYSTEM_SOURCE;
	
	private static final String S050_TURN_TO_REAL_INTENT = "baodan.S050.turnToreal";
	private static final String S051_TURN_TO_REAL_INTENT = "baodan.S051.turnToreal";
	private static final String S052_TURN_TO_REAL_INTENT = "S052";
	private static final String S053_TURN_TO_REAL_INTENT = "baodan.S053.turnToreal";
	
	private final String CLIENT_ID = "CB00";
	private final String Z_SRC = "AT";
	private final String TRNID = "OIA30400";

	@RequestMapping(value="/A05")
	@ResponseBody
	public Map<String, Object> a05(String ID) {
		Map<String, Object> result;
		try {
			result = cathayLifeTypeAService.a05(ID);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	/*
	@RequestMapping(value="/H06")
	@ResponseBody
	public Map<String, Object> h06() {
		Map<String, Object> result = null;
		try {
			boolean h06Result = cathayLifeTypeHService.h06();
			result = this.getSuccessResult(h06Result, false);
		} catch (Exception e) {
			result = getSystemErrorResult();
		}
		return result;
	}*/

	@RequestMapping(value="/H06")
	@ResponseBody
	public boolean h06() {
		boolean result = false;
		try {
			result = cathayLifeTypeHService.h06();
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	//S051~S053下班時間回應訊息
	@RequestMapping(value="/getNotServiceTimeMsg")
	@ResponseBody
	public Map<String, Object> getNotServiceTimeMsg(HttpServletRequest req) {
		BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);

		// 20181204 by todd, 轉真人上下班時間調整在ChatFlow判斷
		String intent = req.getParameter("intent");
		String passBot = req.getParameter("passBot");

		BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), SOURCE, reqVO.getRole());

		boolean IS_ENG = false;
		// 回覆內容
		if (StringUtils.contains(intent, S050_TURN_TO_REAL_INTENT)) {
			String replyTextId1 = "S050.2.1.1";
			String replyTextId2 = "S050.2.1.2";
			try {
				Map param = JSONUtils.json2map(reqVO.getJsonParameter());
				IS_ENG = MapUtils.getBoolean(param, "IS_ENG", false);
				if(IS_ENG) {
					replyTextId1 = replyTextId1 + ".ENG";
					replyTextId2 = replyTextId2 + ".ENG";
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.error("判斷英文客服失敗 : " + e.getMessage());
			}
			
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), replyTextId1);
			botMessageService.setTextResult(vo, replyText);
			String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), replyTextId2);
			botMessageService.setTextResult(vo, replyText2);
		} else if (StringUtils.contains(intent, S051_TURN_TO_REAL_INTENT)) {
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "S051.2.1");
			botMessageService.setTextResult(vo, replyText);
			String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "S051.2.2");
			botMessageService.setTextResult(vo, replyText2);
		} else if (StringUtils.contains(intent, S052_TURN_TO_REAL_INTENT)) {
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "S052.2.1");
			botMessageService.setTextResult(vo, replyText);
			String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "S052.2.2");
            botMessageService.setTextResult(vo, replyText2);
		} else if (StringUtils.contains(intent, S053_TURN_TO_REAL_INTENT)) {
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "S053.2.1");
			botMessageService.setTextResult(vo, replyText);
			String replyText2 = botTpiReplyTextService.getReplyText(reqVO.getRole(), "S053.2.2");
			botMessageService.setTextResult(vo, replyText2);
		} else if (!StringUtils.equals("true", passBot)) {
			String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "S050.2.1");
			botMessageService.setTextResult(vo, replyText);
		}

		List<Map<String, Object>> cardList = this.getNotServiceTimeCardList(vo, IS_ENG);
		botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);

		return vo.getVO();
	}

	public List<Map<String, Object>> getNotServiceTimeCardList(BotMessageVO vo, boolean IS_ENG) {
		/*
		  carousel 文字牌卡
		 */
		List<Map<String, Object>> cardList = new ArrayList<>();
		String[] textInfo = {
				"客服中心信箱:寄送E-mail<br/>在兩個工作日內回覆", "國泰人壽官網:預約諮詢時間<br/><br/>", "來電客服中心:撥打免付費電話<br/><br/>"
		};
		
		String alt = LifeLink.getDefaultMailAlt("service@cathaylife.com.tw");
		LifeLink[] linkInfo = {
				new LifeLink(17,"立即發送",alt,"",""),
				new LifeLink(16,"立即前往","","",PropUtils.CUSTOMER_URL),
				new LifeLink(18,"立即撥號",PropUtils.CUSTOMER_PHONE,"","")
		};
		if(IS_ENG) {
			textInfo = new String[] {
					"Contact center Email:Send an E-mail<br/>Reply within 2 business days",
					"Call contact center:Call a toll free number<br/><br/>"};
			linkInfo = new LifeLink[] { 
					new LifeLink(17,"Send now",alt,"",""),
					new LifeLink(18,"Call us",PropUtils.CUSTOMER_PHONE,"","")
			};
		}

		for(int i=0; i<textInfo.length; i++) {
			String[] info = textInfo[i].split(":");
			String title = info[0];
			String desc = info[1];
			LifeLink link = linkInfo[i];

			BotMessageVO.CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);

			cards.setCTextType("5");
			cards.setCTitle(title);

			List<Map<String, Object>> cTextList = new ArrayList<>();
			BotMessageVO.CTextItem cText1 = vo.new CTextItem();
			cText1.setCText(desc);
			cTextList.add(cText1.getCTexts());
			cards.setCTexts(cTextList);

			//link
			cards.setCLinkType(1);
			cards.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link)));

			cardList.add(cards.getCards());
		}
		return cardList;
	}

	@RequestMapping(value="/repeatAgentMsg")
	@ResponseBody
	public Object repeatAgentMsg(HttpServletRequest req) {
		BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);
		
		BotMessageVO vo = botMessageService.initBotMessage(reqVO.getChannel(), SOURCE, reqVO.getRole());

		boolean IS_ENG = false;
		
		String replytext = "抱歉啦！真人客服還在忙線中，如果想減少等待時間，可以透過以下方式留下聯絡資訊，我們會盡快回覆你喔~";
		try {
			Map param = JSONUtils.json2map(reqVO.getJsonParameter());
			IS_ENG = MapUtils.getBoolean(param, "IS_ENG", false);
			if(IS_ENG) {
				replytext = new String("Sorry! Our live chat agents are still busy. If you want to reduce the waiting time, please leave your contact information and we will get back to you as soon as possible..");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("判斷英文客服失敗 : " + e.getMessage());
		}

		// 回覆內容
		botMessageService.setTextResult(vo, replytext);

		List<Map<String, Object>> cardList = this.getCardList(vo,IS_ENG);
		botMessageService.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);

		return vo.getVO();
	}

	public List<Map<String, Object>> getCardList(BotMessageVO vo, boolean IS_ENG) {
		/*
		  carousel 文字牌卡
		 */
		List<Map<String, Object>> cardList = new ArrayList<>();
		String[] textInfo = {
		        "客服中心信箱:寄送E-mail<br/>在兩個工作日內回覆", "國泰人壽官網:預約諮詢時間<br/><br/>"
		};

		String alt = LifeLink.getDefaultMailAlt("service@cathaylife.com.tw");
		LifeLink[] linkInfo = {
            //先呼叫人壽真人客服斷線，再將alt以html mailto方式執行
            new LifeLink(17,"立即發送",alt,"",""),
			//先呼叫人壽真人客服斷線，再執行內/外部連結(開啟新頁面)
	        new LifeLink(16,"立即前往","","",PropUtils.CUSTOMER_URL)
		};
		
		if(IS_ENG) {
			textInfo = new String[]{ "Contact center Email:Send an E-mail<br/>Reply within 2 business days" };

			linkInfo = new LifeLink[] {new LifeLink(17,"Send now",alt,"","")};
		}

		for(int i=0; i<textInfo.length; i++) {
			String[] info = textInfo[i].split(":");
			String title = info[0];
			String desc = info[1];
			LifeLink link = linkInfo[i];

			BotMessageVO.CardItem cards = vo.new CardItem();
			cards.setCWidth(BotMessageVO.CWIDTH_200);

			cards.setCTextType("5");
			cards.setCTitle(title);

			List<Map<String, Object>> cTextList = new ArrayList<>();
			BotMessageVO.CTextItem cText1 = vo.new CTextItem();
			cText1.setCText(desc);
			cTextList.add(cText1.getCTexts());
			cards.setCTexts(cTextList);

			//link
			cards.setCLinkType(1);
			cards.setCLinkList(botMessageService.getCLinkList(vo, Arrays.asList(link)));

			cardList.add(cards.getCards());
		}
		return cardList;
	}

	@RequestMapping(value="/getStickerHelloImageName")
	@ResponseBody
	public String getStickerHelloImageName(@RequestParam(value = "role", required = true) String role) {
		String result = "";
		try {
			BotLink botLink= botLinkDao.findByLinkID("I0");
			result = botLink.getUrl();
		} catch (Exception e) {
			log.error("取得打招呼貼圖檔名發生錯誤：" + e.getMessage());
		}
		if (StringUtils.isBlank(result)) {
			log.error("無法取得打招呼貼圖檔名");
		}
		return result;
	}

	@RequestMapping(value="/getBirthdayGift")
	@ResponseBody
	public Object getBirthdayGift(HttpServletRequest req) {
		
		BotMessageVO vo = null;
		try {
			BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);

			if (StringUtils.isNotBlank(reqVO.getIDNo())) {
				if (rightsInformationService.checkBirthday(ApiLogUtils.getBotTpiCallLog().getChatId(), reqVO)) {
					log.error("[getBirthdayGift][" + ApiLogUtils.getBotTpiCallLog().getChatId() + "] 符合生日禮");
					vo = botMessageService.initBotMessage(reqVO.getChannel(), SOURCE, reqVO.getRole());

					// sticker 探頭出來貼圖
					botMessageService.setImageResult(vo, "sticker_recall.gif");
					
					// 回覆內容{RIGHTS.INFO.1}
					String replyText = botTpiReplyTextService.getReplyText(reqVO.getRole(), "RIGHTS.INFO.1");
					botMessageService.setTextResult(vo, replyText);

					// 執行動作：作法一，給予生日禮wording，當觸發權益懶人包時若只有生日禮，則不再主動提示權益
					ContentItem contentH = vo.new ContentItem();
					contentH.setType(12);
					contentH.setAction(5);
					contentH.setWidget("doWay1BirthdayGift");
					vo.getContent().add(contentH.getContent());
				}
			}
		} catch (Exception e) {
			log.error("取得生日禮發生錯誤：" + e.getMessage());
		}

		return vo != null ? vo.getVO() : null;
	}
	
	@RequestMapping(value="/queryRightsInformation")
	@ResponseBody
	public Object queryRightsInformation(HttpServletRequest req) {
		
		boolean result = false;
		try {
			BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);
			
			if (StringUtils.isNotBlank(reqVO.getIDNo())) {
				rightsInformationService.queryRightsInformationWithAsync(ApiLogUtils.getBotTpiCallLog().getChatId(), reqVO);
			}
			
			result = true;
		} catch (Exception e) {
			log.error("取得權益懶人包發生錯誤：" + e.getMessage());
		}
		
		return result;
	}
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value="/D19")
    @ResponseBody
    public Object D19(HttpServletRequest req) {
        
        String D19_error = "信用卡檢核錯誤。";
        String C02_error = "抱歉，目前無法進行網路投保，若需要投保旅平險，可以向業務人員聯繫，或前往<a href='https://campaigns.cathaylife.com.tw/OAWeb/html/OA/C8/index.html' onclick='App.handleCustomerAction(\"recommend_link\", this)' target='_blank'>我的保險好麻吉</a>尋找適合你的專業顧問喔！";
        String Z05_error = "很抱歉，目前公會取得資料異常，請稍後再試";
        String API_noresp = "很抱歉，系統繁忙中，請稍後再試，謝謝！";
        
        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);
            
            String CARD_NO = req.getParameter("CARD_NO");
            String CARD_END_YM = req.getParameter("CARD_END_YM");
            String TOURIST_ID = req.getParameter("TOURIST_ID");
            String USER_ID = reqVO.getIDNo();
            String REMEMBER_CARD = req.getParameter("REMEMBER_CARD");
            String TRANS_NO = cBA10001DataService.getCacheTRANS_NO(TOURIST_ID, USER_ID);
            if (StringUtils.isBlank(TRANS_NO)) {
                result.put("resultMsg", "投保操作時間過久");
                result.put("resultCode", "-a2");
            } else {
                boolean checkResult = true;
                
                String TRANS_UUID = sessionId;
                Map<String, Object> d19Result = cathayLifeTypeDService.d19(CARD_NO, CARD_END_YM, USER_ID, TRANS_NO, TRANS_UUID);
                result.putAll(d19Result);
                String d19ResultCode = MapUtils.getString(d19Result, D19.resultCode.name());
                checkResult = checkResult & "000".equals(d19ResultCode);
                
                if (!checkResult) {
                    log.error("(" + sessionId + ") 信用卡卡號檢核發生錯誤：TOURIST_ID=" + TOURIST_ID);
                    result.put("resultMsg", D19_error);
                    result.put("resultCode", "-1");
                }
                
                if (checkResult) {
                	if(StringUtils.equals("Y", REMEMBER_CARD)) {
                		try {
                			Map<String, Object> d28Result = cathayLifeTypeDService.d28("L", REMEMBER_CARD, "N", USER_ID, TRANS_NO, CARD_NO, CARD_END_YM, TRANS_UUID);
                			String d28ResultCode = MapUtils.getString(d28Result, D28.resultCode.name());
                			boolean checkD28Result = checkResult & "000".equals(d28ResultCode);
                			if(!checkD28Result) {
                				String d28ResultMsg = MapUtils.getString(d28Result, D28.resultMsg.name());
                				log.error("(" + sessionId + ") TRANS_NO=" + TRANS_NO + "信用卡號儲存失敗，ErrorMsg ： " + d28ResultMsg);
                			}
						} catch (Exception e) {
							log.error("(" + sessionId + ") TRANS_NO=" + TRANS_NO + ", D28呼叫失敗.");
						}
                	}
                	
                    Map<String, Object> z05Result = cathayLifeTypeZService.z05(reqVO.getIDNo(), TRANS_NO, Z_SRC);
                    String z05ResultCode = MapUtils.getString(z05Result, Z05.resultCode.name());
                    checkResult = checkResult & "000".equals(z05ResultCode);
                    
                    if (checkResult) {
                        Map<String,Object> TRANS_DATA = cBA10001DataService.getCacheTRANS_DATA(TOURIST_ID, reqVO.getIDNo());
                        
                        String ZS_UUID = null;
                        Map<String, Object> CONTRACT_DATA = (Map<String, Object>) MapUtils.getMap(TRANS_DATA, "CONTRACT_DATA");
                        List<Map<String, String>> INSD_DATA = (List<Map<String, String>>) MapUtils.getObject(TRANS_DATA, "INSD_DATA");
                        List<Map<String, Object>> BENE_DATA = (List<Map<String, Object>>) MapUtils.getObject(TRANS_DATA, "BENE_DATA");
                        Map<String, Object> TRAL_TMP = (Map<String, Object>) MapUtils.getMap(TRANS_DATA, "TRAL_TMP");
                        
                        Map<String, Object> c02Result = cathayLifeTypeCService.c02(TRANS_NO, TRANS_UUID, ZS_UUID, CLIENT_ID, TRANS_NO, CONTRACT_DATA, INSD_DATA, BENE_DATA, TRAL_TMP);
                        String c02ResultCode = MapUtils.getString(c02Result, C02.resultCode.name());
                        checkResult = checkResult & "000".equals(c02ResultCode);
                        if (checkResult) {
                            Map c02Data = MapUtils.getMap(c02Result, C02.data.name());
                            Map c02CONTRACT_DATA = MapUtils.getMap(c02Data, C02.CONTRACT_DATA.name());
                            String c02TOT_PREM = MapUtils.getString(c02CONTRACT_DATA, C02.TOT_PREM.name());
                            
                            Map<String, Object> callIdData = cBA10001DataService.getCacheCALL_ID_DATA(TOURIST_ID, reqVO.getIDNo());
                            String c01TOT_PREM = MapUtils.getString(callIdData, "TOT_PREM");
                            checkResult = checkResult & StringUtils.equals(c02TOT_PREM, c01TOT_PREM);
                            if (!checkResult) {
                                log.error("(" + sessionId + ") TRANS_NO=" + TRANS_NO + ", C01(" + c01TOT_PREM + ")試算金額與C02(" + c02TOT_PREM + ")試算金額不一致.");
                                result.put("resultMsg", C02_error);
                                result.put("resultCode", "-1");
                            } else {
                                cBA10001DataService.setCacheC02_DATA(TOURIST_ID, reqVO.getIDNo(), c02Data, ExpiredTime._5mins);
                            }
                        } else {
                            log.error("(" + sessionId + ") TRANS_NO=" + TRANS_NO + ", C02呼叫失敗.");
                            result.put("resultMsg", C02_error);
                            result.put("resultCode", "-1");
                        }
                    } else {
                        log.error("(" + sessionId + ") TRANS_NO=" + TRANS_NO + ", Z05呼叫失敗.");
                        result.put("resultMsg", Z05_error);
                        result.put("resultCode", "-1");
                    }
                }
            }
        } catch (Exception e) {
            log.error("(" + sessionId + ") 信用卡卡號檢核發生錯誤：" + e.getMessage());
            result.put("resultMsg", API_noresp);
            result.put("resultCode", "-1");
        } finally {
            if (result.isEmpty()) {
                result.put("resultMsg", API_noresp);
                result.put("resultCode", "-1");
            }
        }
        
        return result;
    }
    
    @SuppressWarnings({ "rawtypes" })
    @RequestMapping(value="/doGetCCOTP")
    @ResponseBody
    public Object doGetCCOTP(HttpServletRequest req) {
        
        String Z01_error = "OTP驗證碼發送錯誤，請重新發送。";
        String API_noresp = "很抱歉，系統繁忙中，請稍後再試，謝謝！";
        
        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);
            
            String TOURIST_ID = req.getParameter("TOURIST_ID");
            String infmId = reqVO.getIDNo();
            // 交易代號
            String trnId = TRNID;
            
            String TRANS_NO = cBA10001DataService.getCacheTRANS_NO(TOURIST_ID, reqVO.getIDNo());
            if (StringUtils.isBlank(TRANS_NO)) {
                result.put("returnMsg", "投保操作時間過久");
                result.put("returnCode", "-a2");
            } else {
                boolean checkResult = true;
                
                // mobileNo, infmName, email
                String mobileNo = null;
                String infmName = null;
                String email = null;
                if (checkResult) {
                    Map<String, Object> a30Result = cBA10001DataService.getA30WithDefaultTWDataById(reqVO.getIDNo());
                    String a30ResultCode = MapUtils.getString(a30Result, A30.resultCode.name());
                    checkResult = checkResult & "000".equals(a30ResultCode);
                    if ("000".equals(a30ResultCode)) {
                        Map a30Data = MapUtils.getMap(a30Result, A30.data.name());
                        mobileNo = MapUtils.getString(a30Data, A30.mobl_no.name());
                        String infmNameOrigin = MapUtils.getString(a30Data, A30.apc_name.name());
                        //中英文姓名遮罩
                        infmName = TPIStringUtil.custNameMark(infmNameOrigin);
                        email = MapUtils.getString(a30Data, A30.email.name());
                    }
                }
                
                if (checkResult) {
                    String z01Result = cathayLifeTypeZService.z01(trnId, mobileNo, infmId, infmName, email);
                    if (StringUtils.isNotBlank(z01Result)) {
                        cBA10001DataService.setCacheZ01_KEY(TOURIST_ID, reqVO.getIDNo(), z01Result, LocalCache.ExpiredTime._10mins);
                        result.put("returnCode", "0");
                        if (StringUtils.isNotBlank(mobileNo)) {
                            result.put("MOBL_NO", mobileNo);
                        }
                        if (StringUtils.isNotBlank(email)) {
                            result.put("EMAIL", email);
                        }
                    } else {
                        log.error("(" + sessionId + ") TRANS_NO=" + TRANS_NO + ", Z01呼叫失敗.");
                        result.put("returnMsg", Z01_error);
                        result.put("returnCode", "-1");
                    }
                }
            }
        } catch (Exception e) {
            log.error("(" + sessionId + ") 取得一次性碼(發OTP)發生錯誤：" + e.getMessage());
        } finally {
            if (result.isEmpty()) {
                result.put("returnMsg", API_noresp);
                result.put("returnCode", "-1");
            }
        }
        
        return result;
    }
    
    @RequestMapping(value="/doCheckOtpCodeAndPay")
    @ResponseBody
    public Object doCheckOtpCodeAndPay(HttpServletRequest req) {
        
        String Z02_error = "OTP驗證碼錯誤。";
        String D20_error = "交易失敗，請洽銀行確認交易資料，或卡片額度後再次輸入。";
        String API_noresp = "很抱歉，系統繁忙中，請稍後再試，謝謝！";
        
        String sessionId = ApiLogUtils.getBotTpiCallLog().getChatId();
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);
            
            String TOURIST_ID = req.getParameter("TOURIST_ID");
            String CARD_NO = req.getParameter("CARD_NO");
            String CARD_END_YM = req.getParameter("CARD_END_YM");
            
            String TRANS_NO = cBA10001DataService.getCacheTRANS_NO(TOURIST_ID, reqVO.getIDNo());
            if (StringUtils.isBlank(TRANS_NO)) {
                result.put("RTN_MSG", "投保操作時間過久");
                result.put("RTN_CODE", "-a2");
            } else {
                // 交易代號
                String trnId = TRNID;
                String s = cBA10001DataService.getCacheZ01_KEY(TOURIST_ID, reqVO.getIDNo());
                String otp = req.getParameter("otp");
                
                String RTN_CODE = null;
                String RTN_MSG = null;
                
                Map<String,Object> z02Result = cathayLifeTypeZService.z02(trnId, s, otp);
                if (MapUtils.isNotEmpty(z02Result)) {
                    RTN_CODE = MapUtils.getString(z02Result, Z02.RTN_CODE.name());
                    RTN_MSG = MapUtils.getString(z02Result, Z02.RTN_MSG.name());
                    
                    Map<String, String> combinePAYResult = null;
                    if ("0".equals(RTN_CODE)) {
                        combinePAYResult = cBA10001Service.doCombinePAY(reqVO, TOURIST_ID, CARD_NO, CARD_END_YM, TRANS_NO);
                    }
                    
                    String error = MapUtils.getString(combinePAYResult, "error");
                    if (StringUtils.equals("D20", error)) {
                        result.put("RTN_MSG", D20_error);
                        result.put("RTN_CODE", "-3");
                    } else if (!"0".equals(RTN_CODE)) {
                        result.put("RTN_CODE", RTN_CODE);
                        result.put("RTN_MSG", Z02_error);
                    } else {
                        result.put("RTN_CODE", RTN_CODE);
                        result.put("RTN_MSG", RTN_MSG);
                    }
                }
            }
            
        } catch (Exception e) {
            log.error("(" + sessionId + ") 驗證一次性密碼(驗證OTP)或扣款發生錯誤：" + e.getMessage());
        } finally {
            if (result.isEmpty()) {
                result.put("RTN_MSG", API_noresp);
                result.put("RTN_CODE", "-3");
            }
        }
        
        return result;
    }
}
