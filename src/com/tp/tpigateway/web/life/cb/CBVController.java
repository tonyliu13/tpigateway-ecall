package com.tp.tpigateway.web.life.cb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.tpigateway.dao.flow.BotTpiCallLog3DDAO;
import com.tp.tpigateway.dao.flow.FlowCallLog3DDAO;
import com.tp.tpigateway.dao.flow.IdMappingEasycallDAO;
import com.tp.tpigateway.exception.CBV10001DataException;
import com.tp.tpigateway.model.life.CBV10001VO.BotTpiCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.ContentAllVipData;
import com.tp.tpigateway.model.life.CBV10001VO.ContentCheckAmtVoice;
import com.tp.tpigateway.model.life.CBV10001VO.ContentConfirmVoice;
import com.tp.tpigateway.model.life.CBV10001VO.ContentGetRgnCode;
import com.tp.tpigateway.model.life.CBV10001VO.ContentLastIssueData;
import com.tp.tpigateway.model.life.CBV10001VO.ContentLawAge;
import com.tp.tpigateway.model.life.CBV10001VO.ContentTempVoice;
import com.tp.tpigateway.model.life.CBV10001VO.FlowCallLog3D;
import com.tp.tpigateway.model.life.CBV10001VO.FlowRequest;
import com.tp.tpigateway.model.life.CBV10001VO.GatewayResponse;
import com.tp.tpigateway.model.life.CBV10001VO.IdMappingEasycall;
import com.tp.tpigateway.service.life.flow.cb.CBV10001Service;

@RestController
@RequestMapping("ecall")
public class CBVController {

//    private static final Logger LOGGER = LoggerFactory.getLogger(CBVController.class);

	@Autowired
	private CBV10001Service cbv10001Service;

	@Autowired
	private FlowCallLog3DDAO flowCallLog3DDAO;

	@Autowired
	private BotTpiCallLog3DDAO botTpiCallLog3DDAO;

	@Autowired
	private IdMappingEasycallDAO idMappingEasycallDAO;

	@RequestMapping(value = "/law-age", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentLawAge> lawAge(@RequestBody FlowRequest req, BotTpiCallLog3D botTpiCallLog3D) {
		return cbv10001Service.lawAge(req, botTpiCallLog3D);
	}

	@RequestMapping(value = "/last-issue-data", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentLastIssueData> lastIssueData(@RequestBody FlowRequest req,
			BotTpiCallLog3D botTpiCallLog3D) {
		return cbv10001Service.lastIssueData(req, botTpiCallLog3D);
	}

	@RequestMapping(value = "/all-vip-data", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentAllVipData> allVipData(@RequestBody FlowRequest req,
			BotTpiCallLog3D botTpiCallLog3D) {
		return cbv10001Service.allVipData(req, botTpiCallLog3D);
	}

	@RequestMapping(value = "/do-confirm-voice", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentConfirmVoice> doConfirmVoice(@RequestBody FlowRequest req,
			BotTpiCallLog3D botTpiCallLog3D) {
		return cbv10001Service.doConfirmVoice(req, botTpiCallLog3D);
	}

	@RequestMapping(value = "/insert-temp-voice", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentTempVoice> insertTempVoice(@RequestBody FlowRequest req,
			BotTpiCallLog3D botTpiCallLog3D) {
		return cbv10001Service.insertTempVoice(req, botTpiCallLog3D);
	}

	@RequestMapping(value = "/do-checkamt-voice", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentCheckAmtVoice> doCheckAmtVoice(@RequestBody FlowRequest req,
			BotTpiCallLog3D botTpiCallLog3D) {
		return cbv10001Service.doCheckAmtVoice(req, botTpiCallLog3D);
	}

	@RequestMapping(value = "/get-rgn-code", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentGetRgnCode> getRgnCode(@RequestBody FlowRequest req,
			BotTpiCallLog3D botTpiCallLog3D) {
		return cbv10001Service.getRgnCode(req, botTpiCallLog3D);
	}

	@RequestMapping(value = "/saveOrUpdateFlowCallLog3D", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<?> saveFlowCallLog3D(@RequestBody FlowCallLog3D flowCallLog3D) {
		return cbv10001Service.saveFlowCallLog3D(flowCallLog3D);
	}

	// TODO 正式上線後須刪除
	@Transactional
	@RequestMapping(value = "/showFlowCallLog3D/{dataNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FlowCallLog3D> showFlowCallLog3D(@PathVariable(name = "dataNumber", required = false) int dataNumber) {
		dataNumber = dataNumber == 0 ? 10 : dataNumber;
		return flowCallLog3DDAO.showFlowCallLog3D(dataNumber);
	}

	// TODO 正式上線後須刪除
	@Transactional
	@RequestMapping(value = "/showBotTpiCallLog3D/{dataNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BotTpiCallLog3D> showBotTpiCallLog3D(
			@PathVariable(name = "dataNumber", required = false) int dataNumber) {
		dataNumber = dataNumber == 0 ? 10 : dataNumber;
		return botTpiCallLog3DDAO.showBotTpiCallLog3D(dataNumber);
	}

	// TODO 正式上線後須刪除
	@Transactional
	@GetMapping(value = "/showIdMappingEasycall/{sessionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public IdMappingEasycall showIdMappingEasycall(@PathVariable("sessionId") String sessionId) {
		return idMappingEasycallDAO.findBySessionId(sessionId);
	}

}
