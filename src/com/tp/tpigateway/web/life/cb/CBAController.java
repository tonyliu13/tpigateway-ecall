package com.tp.tpigateway.web.life.cb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tp.tpigateway.exception.RRException;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.life.flow.cb.CBA10001Service;
import com.tp.tpigateway.util.PropUtils;
import com.tp.tpigateway.web.common.BaseController;

/**
 * 提供 Bot 呼叫關於「旅平險投保」方面的回傳結果API
 */
@Controller
@RequestMapping("/Life/Bot")
public class CBAController extends BaseController {

	private static Logger log = LoggerFactory.getLogger(CBAController.class);

	private static final String SOURCE = PropUtils.SYSTEM_SOURCE;

	@Autowired
	private CBA10001Service cBA10001Service;

	@ResponseBody
	@RequestMapping("{nodeID:CBA\\d{5}(?:_{1}\\w+)?}")
	public Object CBA(HttpServletRequest req, @PathVariable String nodeID) throws RRException, Exception {
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;

		try {
			// 取得參數
			BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);
			log.info("reqVO=" + getRequestString(reqVO));

			// 設定必要參數
			Map<String, String> requireParams = this.getRequireParams(reqVO, nodeID);

			// 必要參數檢核
			if (checkParams(requireParams)) {
				switch (nodeID) {
				case "CBA10001": // 怎麼投保旅平險
					result = getSuccessResult(cBA10001Service.queryCBA10001(SOURCE, reqVO), false);
					break;
				case "CBA10001_1_1_1": // 試算投保旅平險
					result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1(SOURCE, reqVO), false);
					break;
				case "CBA10001_1_1_1_1_1": // 輸入旅遊問句
					result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1(SOURCE, reqVO), false);
					break;
				case "CBA10001_1_1_1_1_1_5_1": // 確認投保
					result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_1(SOURCE, reqVO), false);
					break;
				case "CBA10001_1_1_1_1_1_5_2": // 修改出發時間
				    result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_2(SOURCE, reqVO), false);
				    break;
				case "CBA10001_1_1_1_1_1_5_2_X": // 輸入出發時間
				    result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_2_X(SOURCE, reqVO), false);
				    break;
				case "CBA10001_1_1_1_1_1_5_1_3": // 輸入地址
					result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_1_3(SOURCE, reqVO), false);
					break;
				case "CBA10001_1_1_1_1_1_5_1_1_1": // 繳費步驟
					result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_1_1_1(SOURCE, reqVO), false);
					break;
				case "CBA10001_1_1_1_1_1_5_1_1_2": // 修改投保資訊
				    result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_1_1_2(SOURCE, reqVO), false);
				    break;
				case "CBA10001_1_1_1_1_1_5_1_1_1_1": // 投保操作時間過久 or 被登出
					result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_1_1_1_1(SOURCE, reqVO), false);
					break;
				case "CBA10001_1_1_1_1_1_5_1_1_1_1_1": // 繳費完成
				    result = getSuccessResult(cBA10001Service.queryCBA10001_1_1_1_1_1_5_1_1_1_1_1(SOURCE, reqVO), false);
				    break;
				default:
					break;
				}
			} else {
				result = getErrorResult(getErrorDescription());
			}

		} finally {
			log.error("[" + nodeID + "] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;
	}

	private Map<String, String> getRequireParams(BotLifeRequestVO reqVO, String nodeID) {
		if (StringUtils.isEmpty(nodeID))
			nodeID = "";

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<>();
		requireParams.put("Channel", reqVO.getChannel());
		requireParams.put("Role", reqVO.getRole());
		requireParams.put("NodeID", reqVO.getNodeID());

		switch (nodeID) {
		case "CBA10001":
			// requireParams.put("IDNo", reqVO.getIDNo());
			break;
		default:
			break;
		}

		return requireParams;
	}
}
