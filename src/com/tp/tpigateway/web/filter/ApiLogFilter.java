package com.tp.tpigateway.web.filter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tp.tpigateway.model.common.BotTpiCallLog;
import com.tp.tpigateway.util.ApiLogUtils;
import com.tp.tpigateway.util.PropUtils;

//@WebFilter("/apiLogFilter")
public class ApiLogFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private boolean isDebug = logger.isDebugEnabled();
	
	@Override
	public void destroy() {
		logger.info("[destroy]");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain)
			throws IOException, ServletException {
		logger.info("[doFilter]");
		
		boolean systemRunIp = false;
		
		try {
			Map<String, Object> reqMap = requestToMap(req);
			ApiLogUtils.addBotTpiCallLog(genBotTpiCallLog(reqMap));
			ApiLogUtils.setNodeID(MapUtils.getString(reqMap, "NodeID", ""));
			ApiLogUtils.setSourceIP(getIpAddress((HttpServletRequest)req));
			
			if (StringUtils.isBlank(PropUtils.SYSTEM_RUN_IP)) {
				systemRunIp = true;
			} else if (StringUtils.contains(PropUtils.SYSTEM_RUN_IP, ApiLogUtils.getSourceIP())) {
				systemRunIp = true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.debug("", e);
			logger.error("systemRunIp:" + systemRunIp);
		} finally {
			if (!systemRunIp) {
				HttpServletResponse httpResp = (HttpServletResponse) resp;
				ServletOutputStream out = httpResp.getOutputStream();
				
				Map<String, Object> mapResult = new LinkedHashMap<String, Object>();
				mapResult.put("ReturnCode", PropUtils.RESULT_CODE_FAIL);
				mapResult.put("ReturnDesc", "ip error:" + ApiLogUtils.getSourceIP());
				
				out.write((new JSONObject(mapResult)).toString().getBytes());
				out.flush();
			} else {
				// to next step
				filterChain.doFilter(req, resp);
			}
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("[init]");

	}
	
	private BotTpiCallLog genBotTpiCallLog(Map<String, Object> objMap) {
		BotTpiCallLog botTpiCallLog = new BotTpiCallLog();
		botTpiCallLog.setCookieId(MapUtils.getString(objMap, BotTpiCallLog.COLUMN_NAME_COOKIE_ID, ""));
		botTpiCallLog.setIp(MapUtils.getString(objMap, BotTpiCallLog.COLUMN_NAME_IP, ""));
		botTpiCallLog.setChatId(MapUtils.getString(objMap, BotTpiCallLog.COLUMN_NAME_CHAT_ID, ""));
		String custID = MapUtils.getString(objMap, "IDNo", "");
		if (StringUtils.isBlank(custID) || StringUtils.equalsIgnoreCase("NULL", custID)) {
			botTpiCallLog.setCustId("NOID");
		} else {
			botTpiCallLog.setCustId(custID);
		}
		botTpiCallLog.setApiId(MapUtils.getString(objMap, BotTpiCallLog.COLUMN_NAME_API_ID, ""));
		botTpiCallLog.setTpiHostName(getHostName());
		return botTpiCallLog;
	}
	
	private Map<String, Object> requestToMap(ServletRequest req) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		
		try {
			Enumeration<String> paramNames = req.getParameterNames();
			String paramName, paramValue;
			while (paramNames.hasMoreElements()) {
				paramName = paramNames.nextElement();
				paramValue = req.getParameter(paramName);
				if (StringUtils.equals("errorlog", paramName)) {
					logger.error("<requestToMap> paramName=" + paramName + ", paramValue=" + paramValue);
				} else if (StringUtils.equals("infolog", paramName)) {
					logger.info("<requestToMap> paramName=" + paramName + ", paramValue=" + paramValue);
				} else {
					logger.debug("<requestToMap> paramName=" + paramName + ", paramValue=" + paramValue);
				}
				rtnMap.put(paramName, paramValue);
			}
			
			try {
				String requestApiId = StringUtils.substringAfterLast(StringUtils.substringBeforeLast(((HttpServletRequest)req).getRequestURL().toString(), "?"), "/");
				logger.debug("requestApiId=" + requestApiId);
				if ("queryContractListByScenario".equals(requestApiId)) {
					rtnMap.put(BotTpiCallLog.COLUMN_NAME_API_ID, "i1");
				} else if ("I1_4".equals(requestApiId)) {
					rtnMap.put(BotTpiCallLog.COLUMN_NAME_API_ID, "i3");
				} else {
					rtnMap.put(BotTpiCallLog.COLUMN_NAME_API_ID, requestApiId);
				}
			} catch (Exception urle) { }
		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
		}
		logger.debug("<requestToMap> rtnMap size="+rtnMap.size());
		return rtnMap;
	}
	
	private String getHostName() {
		String hostname = null;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (Exception e) {
			logger.error("getHostName error:" + e.getMessage());
		} finally {
			if (null == hostname) hostname = "NoHostName";
		}
		return StringUtils.substring(hostname, 0, 20);
	}
	
	private String getIpAddress(HttpServletRequest request) {
    		String ipAddress = null;//getIP(request);
        if (ipAddress == null) {
            ipAddress = getClientIP(request);
        }
        return ipAddress;
    }
	
	private String getIP(HttpServletRequest req) {
		boolean reqNotExist = req == null;
		if (reqNotExist) {
			return null;
		}

		String strIpAddress = req.getHeader("OrigClientAddr");
		if (isDebug) {
			logger.debug("********** getHeader('OrigClientAddr'):" + strIpAddress);
		}
		if (strIpAddress == null || strIpAddress.length() == 0) {
			strIpAddress = req.getHeader("iv-remote-address");
			if (isDebug) {
				logger.debug("********** getHeader('iv-remote-address'):" + strIpAddress);
			}
		}

		if ("127.0.0.1".equals(strIpAddress) || "0:0:0:0:0:0:0:1".equals(strIpAddress) || "::1".equals(strIpAddress)) {
			try {
				strIpAddress = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				logger.error(e.getMessage());
				logger.debug("", e);
			}
		}
		return strIpAddress;
	}

	private String getClientIP(HttpServletRequest request) {
		try {
			String ip = request.getHeader("x-forwarded-for");
			if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}
			if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			}
			if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Cdn-Src-Ip");
			}
			if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}
			logger.debug("<getIpAddr> ip=" + ip);
			return ip;
		} catch (Exception e) {
			return request.getRemoteAddr();
		}
	}
}
