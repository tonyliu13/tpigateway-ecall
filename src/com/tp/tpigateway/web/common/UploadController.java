//package com.tp.tpigateway.web.common;
//
//import java.io.File;
//import java.io.IOException;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.multipart.MultipartFile;
//
//import com.tp.tpigateway.util.PropUtils;
//
//@Controller
//@RequestMapping("/upload")
//public class UploadController {
//
//	private static Logger log = LoggerFactory.getLogger(UploadController.class);
//	
//	@RequestMapping("/init")
//	public String chat(HttpServletRequest request, HttpServletResponse response) throws IOException {
//		return "upload";
//	}
//
//	@RequestMapping(value = "/doUpload", method = RequestMethod.POST)
//	public String doUpload(Model model, MultipartFile file) throws IOException {
//		if (file == null || StringUtils.isEmpty(file.getOriginalFilename())) {
//			model.addAttribute("code", "-1");
//			model.addAttribute("msg", "file is empty..");
//			return "uploadResult";
//		}
//		
//		File destFile = new File(UploadController.class.getResource("/" + PropUtils.REPLY_EXCEL_PATH).getFile());
//		FileUtils.copyToFile(file.getInputStream(), destFile);
//		
//		model.addAttribute("code", "0");
//		return "uploadResult";
//	}
//
//	/*
//	@ResponseBody
//	@RequestMapping(value = "doUpload", method = RequestMethod.POST)
//	public String doUpload(HttpServletResponse response, MultipartFile file) throws IOException {
//		if (file != null)
//			System.out.println("getOriginalFilename = " + file.getOriginalFilename());
//		
//		//避免用@ResponseBody ie8下上傳文件後返回json格式會提示下载
//		PrintWriter pw = null;
//		try {
//			pw = response.getWriter();
//			pw.print(JSONUtils.obj2json(JsonResult.success()));
//			pw.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}*/
//
//}
