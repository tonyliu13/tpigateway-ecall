package com.tp.tpigateway.web.common;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tp.tpigateway.model.common.APIResult;
import com.tp.tpigateway.model.common.BotMessageVO;
import com.tp.tpigateway.model.common.TPIGatewayResult;
import com.tp.tpigateway.model.life.BotLifeRequestVO;
import com.tp.tpigateway.service.life.flow.CathayLifeFlowService;
import com.tp.tpigateway.util.PasswordUtils;
import com.tp.tpigateway.util.PropUtils;

/**
 * 基礎Controller
 * (提供輸入參數驗證及API呼叫認證)
 */
public class BaseController {

	private static Logger log = LoggerFactory.getLogger(BaseController.class);
	
	protected static final String SOURCE = PropUtils.SYSTEM_SOURCE;
	
	@Autowired
	protected CathayLifeFlowService cathayLifeFlowService;	
	
	private String errorDescription = null;
	
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	public Map<String, Object> getSuccessResult(Object info, boolean breakUpInfo) {
		return new TPIGatewayResult(PropUtils.RESULT_CODE_SUCCESS, PropUtils.RESULT_DESCRIPTION_SUCCESS, info).getRegroupResult(breakUpInfo);
	}
	
	public Map<String, Object> getSuccessResult(Object info, String result, boolean breakUpInfo) {
		return new TPIGatewayResult(PropUtils.RESULT_CODE_SUCCESS, PropUtils.RESULT_DESCRIPTION_SUCCESS, info, result).getRegroupResult(breakUpInfo);
	}
	
	public Map<String, Object> getErrorResult(String error) {
		return new TPIGatewayResult(PropUtils.RESULT_CODE_FAIL, error, null).getRegroupResult(true);
	}
	
	public Map<String, Object> getSystemErrorResult() {
		return new TPIGatewayResult(PropUtils.RESULT_CODE_FAIL, PropUtils.RESULT_DESCRIPTION_SYSTEM_ERROR, null).getRegroupResult(true);
	}

	/**
	 * 輸入參數驗證
	 * 
	 * @param params
	 * @return true:pass, false:fail
	 */
	public boolean checkParams(Map<String, String> params) {
		
		boolean result = false;
		try {
			if(params != null && !params.isEmpty()) {
				
				StringBuilder paramSB = new StringBuilder();
				
				Set<String> keySet = params.keySet();
				for(String key : keySet) {
					if(StringUtils.isBlank(params.get(key))) {
						paramSB.append("[" + key + "]");
					}
				}
				
				if(paramSB.length() == 0) {
					result = true;
				} else {
					this.setErrorDescription(PropUtils.RESULT_DESCRIPTION_PARAMETER_ERROR + " - " + paramSB.toString());
				}
			} else {
				result = true;
			}
		} catch (Exception e) {
			log.error("error:" + e.getMessage());
			log.debug("", e);
			this.setErrorDescription(PropUtils.RESULT_DESCRIPTION_SYSTEM_ERROR);
		}
		
		return result;
	}
	
	/**
	 * API呼叫認證
	 * 
	 * @param source 呼叫來源
	 * @param token
	 * @param sysTime
	 * @return true:pass, false:fail
	 */
	public boolean checkToken(String source, String token, String sysTime) {
		
		boolean result = false;
		try {
			String sourceKey = null;
			if (StringUtils.isNotBlank(source)) {
				sourceKey = PropUtils.getApiKey(source);
			} else {
				sourceKey = PropUtils.KITT_KEY;
			}
			
			String tpiToken = PasswordUtils.encodePassword(sourceKey + sysTime);
			if(NumberUtils.isParsable(sysTime) && StringUtils.equals(tpiToken, token)) {
				long kikiTime = NumberUtils.toLong(sysTime);
				long nowTime = System.currentTimeMillis();
				if((kikiTime - (10*1000)) <= nowTime && (kikiTime + PropUtils.TOKEN_EXPIRE_TIME) >= nowTime) {
					result = true;
				} else {
					this.setErrorDescription(PropUtils.RESULT_DESCRIPTION_TOKEN_OVERDUE);
				}
			} else {
				this.setErrorDescription(PropUtils.RESULT_DESCRIPTION_TOKEN_ERROR);
			}
		} catch (Exception e) {
			log.error("source=" + source + ", error:" + e.getMessage());
			log.debug("", e);
			this.setErrorDescription(PropUtils.RESULT_DESCRIPTION_SYSTEM_ERROR);
		}
		
		return result;
		
		// return true;
	}
	
	public String getRequestString(BotLifeRequestVO reqVO) {
		try {
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			return ow.writeValueAsString(reqVO);
		} catch (Exception e) {
			log.error(e.getMessage());
			log.debug("", e);
			return "";
		}
	}
	
	public String trimParam(String param) {
		return StringUtils.trim(param);
	}
	
    /**
     * 依據NodeID 取得FlowService實作
     * @param nodeID
     * @return
     */
    public CathayLifeFlowService getFlowService(String nodeID) {
    	return cathayLifeFlowService;
    }


	/**
	 * 依據回覆設定檔產生回答内容
	 * @param req
	 * @param nodeID
	 * @return
	 * @throws Exception
	 */
    public Object genResponseByNodeID(HttpServletRequest req, String nodeID) throws Exception {
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
		CathayLifeFlowService flowService = getFlowService(nodeID);
		try {
			
			// 取得參數
			BotLifeRequestVO reqVO = BotLifeRequestVO.getParam(req);
			log.info("reqVO=" + getRequestString(reqVO));

			
			// 設定必要參數
			Map<String, String> requireParams = flowService.getRequireParams(reqVO, nodeID);

			// 必要參數檢核
			if (checkParams(requireParams)) {
				log.debug("flowService:"+flowService);
				APIResult apiResult = flowService.getData(reqVO,  SOURCE, nodeID);

		        log.debug("RespNodeID:"+apiResult.getRespNodeID());
				BotMessageVO botMsg = flowService.genResponse(SOURCE, reqVO, apiResult);
				result = getSuccessResult(botMsg.getVO(), false);
                
			} else {
				result = getErrorResult(getErrorDescription());
			}

		} finally {
			log.error("[" + nodeID + "] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
			if (result == null) {
				result = getSystemErrorResult();
			}
		}

		return result;
	}		
}
