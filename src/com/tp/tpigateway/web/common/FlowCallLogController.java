package com.tp.tpigateway.web.common;


import com.tp.tpigateway.model.common.FlowCallLog;
import com.tp.tpigateway.service.common.FlowCallLogService;
import com.tp.tpigateway.util.ApiLogUtils;
import com.tp.tpigateway.util.JSONUtils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/FlowCallLog/")
public class FlowCallLogController extends BaseController {

    private static Logger log = LoggerFactory.getLogger(FlowCallLogController.class);

    @Autowired
    private FlowCallLogService flowCallLogService;

    /*
    @PostMapping(value = "/add")
    public Map<String, Object> addFlowLog(FlowCallLog flowCallLog) {
        flowCallLogService.add(flowCallLog);
        return getSuccessResult("", false);
    }

    @PostMapping(value = "/update")
    public Map<String, Object> updateFlowLog(FlowCallLog flowCallLog) {
        flowCallLogService.update(flowCallLog);
        return getSuccessResult("", false);
    }
    */

	@PostMapping(value = "/saveOrUpdate")
	public Map<String, Object> saveOrUpdateFlowLog(FlowCallLog flowCallLog, HttpServletRequest req) throws IOException {
		if (null == flowCallLog || StringUtils.isBlank(flowCallLog.getSessionId())) {
			log.debug("FlowCallLog getInputStream.");
			byte[] body = IOUtils.toByteArray(req.getInputStream());
			log.debug("[saveOrUpdateFlowLog] body stream:" + (new String(body)));
			flowCallLog = (new ObjectMapper()).readValue(body, FlowCallLog.class);
		}
		flowCallLog.setFlowHostName(ApiLogUtils.getSourceIP());
//		flowCallLogService.saveOrUpdate(flowCallLog);
		flowCallLogService.saveOrUpdate(handleValueLength(flowCallLog));
		printLog(flowCallLog);
		return getSuccessResult("", false);
	}

    //輸出除nlu結果、faq結果以外參數的java log
    private void printLog(FlowCallLog flowCallLog) {
        try {
            flowCallLog.setNluResult(null);
            flowCallLog.setFaqResult(null);
            log.info("flowCallLog = " + JSONUtils.obj2json(flowCallLog));
        } catch (Exception e) {

        }
    }

    /**
     * 處理可能會過長的欄位值
     * 
     * @param flowCallLog
     * @return
     */
    private FlowCallLog handleValueLength(FlowCallLog flowCallLog) {
    		if (flowCallLog != null) {
    			flowCallLog.setMsg(StringUtils.substring(flowCallLog.getMsg(), 0, 30000));
    			flowCallLog.setNluResult(StringUtils.substring(flowCallLog.getNluResult(), 0, 30000));
    			flowCallLog.setNluIntent(StringUtils.substring(flowCallLog.getNluIntent(), 0, 30000));
    			flowCallLog.setFaqResult(StringUtils.substring(flowCallLog.getFaqResult(), 0, 30000));
    			flowCallLog.setFaqAnswer(StringUtils.substring(flowCallLog.getFaqAnswer(), 0, 30000));
    			flowCallLog.setNodeId(StringUtils.substring(flowCallLog.getNodeId(), 0, 20));
    			flowCallLog.setFlowHostName(StringUtils.substring(flowCallLog.getFlowHostName(), 0, 20));
    			flowCallLog.setSubChannel(StringUtils.substring(flowCallLog.getSubChannel(), 0, 10));
    			flowCallLog.setFlowAppId(StringUtils.substring(flowCallLog.getFlowAppId(), 0, 30));
    			flowCallLog.setCelebrusCookie(StringUtils.substring(flowCallLog.getCelebrusCookie(), 0, 75));
    		}
    		return flowCallLog;
    }
}
