package com.tp.tpigateway.web.common;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tp.tpigateway.model.common.DailySession;
import com.tp.tpigateway.model.common.SessionStage;
import com.tp.tpigateway.model.common.monitor.CacheData;
import com.tp.tpigateway.model.common.monitor.ChatLogData;
import com.tp.tpigateway.model.common.monitor.MonitorData;
import com.tp.tpigateway.model.common.monitor.SessionLightData;
import com.tp.tpigateway.model.common.monitor.SessionStatusData;
import com.tp.tpigateway.service.common.MonitorService;
import com.tp.tpigateway.util.LocalCache;
import com.tp.tpigateway.util.PropUtils;

@RestController
@RequestMapping("/api/monitor/")
public class MonitorController extends BaseController {

    private static Logger log = LoggerFactory.getLogger(MonitorController.class);

    @Autowired
    private MonitorService monitorService;

    /*
        2.提供對話紀錄查詢API(by session_id)

        TPIGateway開API可查出以下資訊(如寫入table:bot_chat_log中，ChatFlow提供的對話紀錄)
        依session_id查詢對話紀錄(mongodb)。

        待連線上公司mongodb後可以先看看結構，看怎麼兜出對話紀錄，
        有問題再問我或小麥，或是不行我們再請原廠進行說明。

        ChatWeb中chatLog呈現對話紀錄功能待上述功能完成後改接，並將chatLog頁面標頭加上顯示session_id。
    */
    @GetMapping(value = "/chatLog")
    public Map<String, Object> getChatLog(
            @RequestParam(value = "sessionId", required = true) String sessionId,
            @RequestParam(value = "startDateTime", required = false) Long startDateTime) {
        ChatLogData result = monitorService.findChatLogBySessionIdAndStartDateTime(sessionId, startDateTime);
        return getSuccessResult(result, false);
    }

    /*
       4.依時間區段查詢(對話起始時間)session的下列相關資訊(mongodb)：

      TPIGateway開API給真人客服呼叫，提供以下資訊(json即可)：
      .1「對話unique ID」{session_id}
      .2「登入ID」{customerData.customer_id}
      .3「時間 」{對話起始時間}
      .4「管道」{channel}、「次管道」{subChannel}
      .5「是否申訴」{intent包含<baodan.S051.turnToreal>或對話紀錄中(bot回覆)有isAppeal=true}
      .6「該客戶曾詢問哪些intent」{對話紀錄中NLU命中到的intent清單}
     */
    @RequestMapping(value = "/chatLogByTime")
    public Map<String, Object> getChatLogByTime(
            @RequestParam(value = "sessionId", required = false) String sessionId,
            @RequestParam(value = "startDateTime", required = true) long startDateTime,
            @RequestParam(value = "endDateTime", required = true) long endDateTime) {

        List<MonitorData> result = monitorService.findChatLogBySessionIdAndDateTime(sessionId, startDateTime, endDateTime);
        return getSuccessResult(result, false);
    }

    @PostMapping(value = "/saveDailySession")
    public Map<String, Object> saveDailySession(@RequestBody DailySession dailySession){
    		log.debug("do saveDailySession dailySession:" + new JSONObject(dailySession).toString());
        monitorService.saveDailySession(dailySession);
        return getSuccessResult("", false);
    }

    /* 對話session取件 */
    @RequestMapping(value = "/sessionStatusData")
    public Map<String, Object> getSessionStatusData(@RequestParam(value = "agentId", required = true) String agentId) {
        SessionStatusData result = monitorService.findSessionStatusData(agentId);
        return getSuccessResult(result, false);
    }

    /*查詢對話狀態(監控燈號)數量API*/
    @RequestMapping(value = "/sessionLight")
    public Map<String, Object> getSessionLight(@RequestParam(value = "sessionUpdMin", required = false) Integer sessionUpdMin){
        SessionLightData result = monitorService.countSessionLight(sessionUpdMin);
        return getSuccessResult(result, false);
    }

    @PostMapping(value = "/updateCache")
    public Map<String, Object> updateCache(@RequestBody DailySession dailySession){
        if (!StringUtils.equalsIgnoreCase(dailySession.getSystemID(), PropUtils.SYSTEM_ID)) {
        		log.debug("do updateCache dailySession:" + new JSONObject(dailySession).toString());
        		monitorService.updateCache(dailySession);
        } else {
        		log.debug("bypass updateCache dailySession:" + new JSONObject(dailySession).toString());
        }
        return getSuccessResult("", false);
    }
    
    /**
     * 查詢對話狀態API
     */
    @RequestMapping(value = "/getSessionStage")
	public Map<String, Object> getSessionStage(@RequestParam(value = "sessionId", required = true) String sessionId) {
		SessionStage result = monitorService.findSessionStageBySessionId(sessionId);
		return getSuccessResult(result, false);
	}

    @RequestMapping(value = "/localCache")
    public Map<String, Object> localCache(@RequestBody CacheData cacheData) throws JsonParseException, JsonMappingException, IOException {

        if ("-1".equals(cacheData.getType())) {
            log.debug("do removeCache:(" + cacheData.getKey() + ")");
            LocalCache.remove(cacheData.getKey());
        } else {
            if (LocalCache.get(cacheData.getKey()) == null) {

                log.debug("do localCache:(" + cacheData.getKey() + ")" + cacheData.getValue());
                Object tmp = null;
                if ("1".equals(cacheData.getType())) {
                    tmp = cacheData.getValue();
                } else if ("2".equals(cacheData.getType())) {
                    tmp = new ObjectMapper().readValue(cacheData.getValue(), Collection.class);
                } else if ("3".equals(cacheData.getType())) {
                    tmp = new ObjectMapper().readValue(cacheData.getValue(), Map.class);
                }
                LocalCache.register(cacheData.getKey(), tmp, cacheData.getExpiredTime());
            }
        }
        return getSuccessResult("", false);
    }
}
